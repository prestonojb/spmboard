<div class="modal fade" id="generateExamPaperModal" tabindex="-1" role="dialog" aria-labelledby="generateExamPaperModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        {{-- Form --}}
        <form action="{{ route('exam_builder.sheets.store') }}" method="POST" id="generateExamPaperForm">
            @csrf
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Generate Exam Paper</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    {{-- Hidden fields --}}
                    <input type="hidden" name="item_ids" value="{{ json_encode( $items->pluck('id')->toArray() ) }}">
                    <input type="hidden" name="subject" value="{{ $subject->id }}">
                    <input type="hidden" name="chapters" value="{{ json_encode( $chapters->pluck('id')->toArray() ) }}">
                    <input type="hidden" name="paper_number" value="{{ $paper_number }}">

                    {{-- Name --}}
                    <div class="form-field">
                        <label for="">Name</label>
                        <input type="text" name="name" value="{{ old('name') ?? $subject->name.' Paper '.$paper_number }}" required>
                        @error('name')
                            <div class="error-message">{{ $message }}</div>
                        @enderror
                    </div>

                    <hr>

                    <div class="mb-4">
                        {{-- Options --}}
                        <label for="" class="mb-2">Options</label>
                        {{-- Question Label --}}
                        <div class="mb-2">
                            <input type="checkbox" name="show_label" id="show_label" {{ old('show_label') ? 'checked' : '' }}>
                            <label for="show_label">{{ __('Show Question Label') }}</label>
                        </div>

                        {{-- Show Watermark --}}
                        <div class="mb-2">
                            <input type="checkbox" name="hide_watermark" id="hide_watermark" {{ old('hide_watermark') ? 'checked' : '' }}
                            @cannot('hide_watermark', \App\ExamBuilderSheet::class) disabled @endcannot>
                            <label for="hide_watermark">{{ __('Hide Watermark') }}</label>
                        </div>
                    </div>

                    <p class="gray2">Your worksheet will take <b>5-10 seconds</b> to be generated.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="button--primary--inverse" data-dismiss="modal">Close</button>
                    <button type="button" class="button--primary loadOnSubmitButton" style="width: 94px;">Generate</button>
                </div>
            </div>
        </form>
    </div>
</div>
