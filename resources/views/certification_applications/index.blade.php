@extends('main.layouts.app')

@section('meta')
<title>Certified {{ ucfirst(auth()->user()->account_type) }} | {{ config('app.name') }}</title>
@endsection

@section('content')
@php
    $user = auth()->user();
@endphp
@include('main.navs.header')
<div class="py-3"></div>

<div class="container">
{{-- Teacher --}}
@if($user->is_teacher())
    {{-- Certified values --}}
    <div class="">
        <div class="mb-3">
            <h1 class="font-size9 mb-2">Teacher Certification Programme</h1>
            <p class="mb-0 gray2 font-size4">Boost your reputation with a certification from Board!</p>
        </div>

        <div class="panel p-0">
            <div class="panel-header">
                Exclusive Benefits
            </div>
            <div class="panel-body">
                <div class="row">
                    @php
                        $data = [
                            ['icon' => 'ic:baseline-verified', 'title' => 'Certified Badge', 'description' => 'Having a tick beside your name makes you a more credible and trustworthy teacher.'],
                            ['icon' => 'uil:chart-growth', 'title' => 'Boost Exposure', 'description' => 'Get exclusive opportunities to be featured on Board\'s Facebook, Youtube channel and Weekly Newsletter!'],
                        ];
                    @endphp
                    @foreach($data as $datum)
                        <div class="col-12 col-md-6 col-lg-4">
                            <div class="main__icon-list">
                                <span class="iconify blue5" data-inline="false" data-icon="{{ $datum['icon'] }}"></span>
                                <h6 class="title">{{ $datum['title'] }}</h6>
                                <p class="description">{{ $datum['description'] }}</p>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

    <div class="py-3"></div>
    <hr>
    <div class="py-1"></div>

    {{-- My Application --}}
    <section>
        <div class="d-flex flex-wrap justify-content-between align-items-center mb-1">
            <h2 class="font-size4 font-weight-normal">My Applications</h2>
            {{-- Create Application --}}
            @if($user->is_certified)
                <a href="javascript:void(0)" class="button--primary disabled">
                    <span class="iconify" data-icon="ant-design:check-circle-outlined" data-inline="false"></span> Certified</a>
            @else
                @can('create_store', \App\CertificationApplication::class)
                    <a href="{{ route('certification_applications.create') }}" class="button--primary">
                        <span class="iconify" data-icon="ant-design:check-circle-outlined" data-inline="false"></span> Get Certified</a>
                @else
                    <a href="javascript:void(0)" class="button--primary disabled">
                        <span class="iconify" data-icon="ant-design:check-circle-outlined" data-inline="false"></span> Get Certified</a>
                @endcan
            @endif
        </div>
        @component('certification_applications.teacher.index', ['teacher' => $user->teacher, 'applications' => $applications])
        @endcomponent
    </section>

    <div class="py-3"></div>

    {{-- Metrics --}}
    <section>
        <div class="d-flex flex-wrap justify-content-between align-items-center mb-1">
            <h2 class="font-size4 font-weight-normal">Metrics</h2>
        </div>
        @component('certification_applications.teacher.metrics', ['teacher' => $user->teacher])
        @endcomponent
    </section>
@endif
</div>
@endsection

@section('scripts')
@endsection
