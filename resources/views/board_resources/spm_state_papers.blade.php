@extends('forum.layouts.app')
@section('meta')
@endsection

@section('content')
@include('main.navs.header')
<section class="state-papers py-3">
<div class="container">
    <div class="text-center py-4">
        <h1 class="heading">SPM State Papers</h1>
        <h2 class="sub-heading">Download your Mark Scheme here.</h2>
    </div>

    @php
        $state_papers = [
            '2019 Math' => [
                'Johor' => 'https://drive.google.com/file/d/1xMU3CynRc8xO5Al7-A8uucKKFvxHZsA0/view?usp=sharing',
                'MRSM' => 'https://drive.google.com/file/d/1_VU0Umb2wenmxJhyn5rIUmBFtWLmHSSN/view?usp=sharing',
                'Kelantan' => 'https://drive.google.com/file/d/1vJzneSoqAvNpSEdOfUhtAhX0QjahG8K_/view?usp=sharing',
                'Pahang' => 'https://drive.google.com/file/d/1YAs5VTTk2_AcpmnImMlnafALE8ItSC_f/view?usp=sharing',
                'Penang' => 'https://drive.google.com/file/d/1F7v1Zkgq-HALXQtB1SmTKpD6SUYf8Mst/view?usp=sharing',
                'Perlis' => 'https://drive.google.com/file/d/1XpOVfoxFbfiPjnImnUy43kStKGE5OI0r/view?usp=sharing',
                'Selangor' => 'https://drive.google.com/file/d/1kzrrSbxkM-WpRlAX2M8maBX1HdwiyvXE/view?usp=sharing',
                'Terengganu' => 'https://drive.google.com/file/d/1VIXl1Vrl-uNXVRgQ2LaSFWf9i8Zko-8j/view?usp=sharing',
            ],
            '2019 Add Math' => [
                'Perak' => 'https://drive.google.com/file/d/1vH1HhMIZuKizcqmHSxy5C-HQzEiR_6br/view?usp=sharing',
                'MRSM' => 'https://drive.google.com/file/d/1l1KJJmBCLmkNzRzCwry-MCb7cCqOLb-l/view?usp=sharing',
                'Terengganu' => 'https://drive.google.com/file/d/10aJW_tT03a_NqPh64Y5XyNVR5IJ9-2bT/view?usp=sharing',
                'N9' => 'https://drive.google.com/file/d/1gNYbXt0FkQ56nRjet8obQU_zmFvp2qPj/view?usp=sharing',
                'Kelantan' => 'https://drive.google.com/file/d/16CkzsgZyYsZt1zKJzlaInqX0gnx2WLlC/view?usp=sharing',
                'Johor' => 'https://drive.google.com/file/d/1-TjQA8eNAqoxSY3hhXx40sI_jqItrnpi/view?usp=sharing',
                'Selangor' => 'https://drive.google.com/file/d/1JAW-oIjTgpcfbtRAUQ6bd8-nFFv3ijXU/view?usp=sharing',
                'Penang' => 'https://drive.google.com/file/d/1fmeItJeigEfyKvEZXWeShq6J2LF1Do5c/view?usp=sharing',
            ],
            '2019 Biology' => [
                'Johor' => 'https://drive.google.com/file/d/11lqfNazuNjY2SIpp5orQtghlWhNedlmg/view?usp=sharing',
                'MRSM' => 'https://drive.google.com/file/d/15zlPH0SCpDyuol6ALF-b-qzRrr1BO4vq/view?usp=sharing',
                'Kelantan' => 'https://drive.google.com/file/d/1WjOVrL7vDIvw7wAiowHuGWgBaT0G6Nu4/view?usp=sharing',
                'N9' => 'https://drive.google.com/file/d/1gmlL4pEeGu3MeBMEFu3c1pD2yEI8Nl0r/view?usp=sharing',
                'Pahang' => 'https://drive.google.com/file/d/1LQ4iDPwS_qt80QK0I9TOa6kupB1XTGLC/view?usp=sharing',
                'Selangor' => 'https://drive.google.com/file/d/1AYdUEPRw4mgb0D1-TSjSDc5QnH524K9J/view?usp=sharing',
                'Penang' => 'https://drive.google.com/file/d/1azMyu5gI-I_JsS9Zgy1q65pqR_IdwzIP/view?usp=sharing',
            ],
            '2019 Physics' => [
                'Melaka' => 'https://drive.google.com/file/d/1jQIwpvsd1JxCCCuEl9vNaoaG1cLPRlqj/view?usp=sharing',
                'MRSM' => 'https://drive.google.com/file/d/1hjJooJ_DSJyceoZ00mxY1b5tyl6Tf3_7/view?usp=sharing',
                'Kelantan' => 'https://drive.google.com/file/d/1dkfm9uZFnVwe9YPxks-i7vflMf-T3kY1/view?usp=sharing',
                'Terengganu' => 'https://drive.google.com/file/d/18TnWd4PT_bFSNDRgLlnS6iIdbw6Dz12F/view?usp=sharing',
                'Pahang' => 'https://drive.google.com/file/d/1tREnHJPFIgQTc-Eot08lm6MpVEh7tsSp/view?usp=sharing',
                'Selangor' => 'https://drive.google.com/file/d/1zSr4G4ioGlMQ1bPNhboqMh4qFBfxq2ie/view?usp=sharing',
                'Penang' => 'https://drive.google.com/file/d/1FjBI0iRraouskOCN9Cu_ZV7lqlaKDy34/view?usp=sharing',
            ],
            '2019 Chemistry' => [
                'Penang' => 'https://drive.google.com/file/d/1nuBhPf0R8PL_ott9Rhi0cu9fjyQj8cmS/view?usp=sharing',
                'MRSM' => 'https://drive.google.com/file/d/1IysNljZOrRYTc-OaDg75ni9T-UWapLqL/view?usp=sharing',
                'Terengganu' => 'https://drive.google.com/file/d/1GBW7OerrJHyP1_0_UqN2Xp-yuaThJQyf/view?usp=sharing',
                'Perlis' => 'https://drive.google.com/file/d/1wOYXmwm0ZbVCTUUY-l1vIuQL83X6TJph/view?usp=sharing',
                'Johor' => 'https://drive.google.com/file/d/1vO2rwVZpQqSalFZIJ90dK6Ql_1Acjdo6/view?usp=sharing',
                'Selangor' => 'https://drive.google.com/file/d/1hok_hb58g8u5WbXJO6R2Ox2toL8kAOjp/view?usp=sharing',
                'Pahang' => 'https://drive.google.com/file/d/1eOhRUZc1hwTTfakNrsDaUvRD9ha3jOM_/view?usp=sharing',
            ],
            '2019 Prinsip Perakaunan' => [
                'Johor' => 'https://drive.google.com/file/d/1bO2icwnA0V2p1IX0DmI0u_e5NSyy9956/view?usp=sharing',
                'Kelantan' => 'https://drive.google.com/file/d/1HO6n8NphX8xoJ9LLsWSwPnqFgLHAkHuP/view?usp=sharing',
                'MRSM' => 'https://drive.google.com/file/d/1jtOI31W8j9CV_YgK-9bJ0YUejgWo1X4O/view?usp=sharing',
                'Pahang' => 'https://drive.google.com/file/d/1JjRBlOHXTt08fVEzg2StiUu6ZIxkcIO_/view?usp=sharing',
                'Penang' => 'https://drive.google.com/file/d/1VNxysczCq71khDPbYh1sF-0a9dU82sp7/view?usp=sharing',
                'Perlis' => 'https://drive.google.com/file/d/1WHwtkz01I0aznVVx1PWPOPgAUqkjXf5v/view?usp=sharing',
                'Terengganu' => 'https://drive.google.com/file/d/1Qwd_svb7LZXX-9D5v1XpKzwkxsE0vov0/view?usp=sharing'
            ],
            '2019 Ekonomi Asas' => [
                'Johor' => 'https://drive.google.com/file/d/1dmB98q9HrTdZZN5Tpjos3ESSzTORF3Dq/view?usp=sharing',
                'Melaka' => 'https://drive.google.com/file/d/1a9anvVkH7A-DN-Ks7iVbhPpixPLr1lBd/view?usp=sharing',
                'N9' => 'https://drive.google.com/file/d/1wwxCf_IMR6u9XFwV5lhlJI0xDpL8rET3/view?usp=sharing',
                'Pahang' => 'https://drive.google.com/file/d/1sKjemCkKYdvdsFoHDak57phOTMhqu3Dd/view?usp=sharing',
                'Penang' => 'https://drive.google.com/file/d/1griCQktpUGVuwJpyM4w2C11hg5oLThGY/view?usp=sharing',
                'Perlis' => 'https://drive.google.com/file/d/1TOichLL81FD7A_AEkwtjn-wIPr5Ihx_j/view?usp=sharing',
                'Terengganu' => 'https://drive.google.com/file/d/1z8jrTchmXq3DAKD9qL5Tgz1jIVdi5Qis/view?usp=sharing'
            ]
        ];
    @endphp

    @foreach($state_papers as $state => $papers)
        <div class="mb-3">
            <h3 class="sub-heading">{{ $state }}</h3>
            <div class="row">
                @foreach($papers as $state => $url)
                    <div class="col-12 col-md-4 col-lg-3">
                        <div class="panel text-center mb-2 p-3">
                            <h3 class="h2 py-2">{{ $state }}</h3>
                            <a href="{{ $url }}" class="button--primary w-100 downloadButton" target="_blank">Mark Scheme</a>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>

        <hr>
    @endforeach

</div>
</section>
@endsection

@section('scripts')
<script>
$(function(){
    $('.downloadButton').
});
</script>
@endsection
