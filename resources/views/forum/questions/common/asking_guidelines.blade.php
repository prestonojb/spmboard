@php
    $guidelines = [
        'Question should be typed out' => 'Instead of attaching the image of the question, we encourage students to retype this question out in the description box so that it can be found on google search thereby attract more users to answer your questions.',
        'Question should be well written' => 'Questions should be clear and concise with correct spelling, punctuation, grammar and suitable capitalisation. You can bold or underlines to emphasise certain important words. A well written question will assist the teachers or your peers to answer your question correctly according to the syllabus.',
        'Choose an interesting title!' => 'This will allow the particular questions to be spotted easily among all other questions!',
        'Question should comply with the site policy' => 'We do not encourage any questions or statements that are hurtful or likely to make others felt uncomfortable. We would like to create a friendly community where teachers and students can form a community to learn together happily.',
        'Ask for politely' => 'End your question by saying thank you would be really helpful! Be a part of the community that encourage learning, exchange of knowledge and academic improvements!',
        'Offer reward points' => 'By offering reward points you earn from daily login, you will be able to attract more peers or teachers to answer your question. Do remember to select the best answers to reward the users with reward points! This will encourage the users to provide quality answers.',
    ];
@endphp

<div class="panel">
    <h3 class="block-title">Asking Guidelines</h3>
    <div id="guidelineAccordion">
        @foreach($guidelines as $title => $description)
            <div class="">
                <div class="mb-1" id="headingOne">
                    <h5 class="mb-0">
                        <button class="w-100 text-left border-bottom" data-toggle="collapse" data-target="#{{ str_slug($title) }}_guideline" aria-expanded="true" aria-controls="collapseOne">
                            {{ $loop->index + 1 }}. {{ $title }}
                        </button>
                    </h5>
                </div>
                <div id="{{ str_slug($title) }}_guideline" class="collapse" aria-labelledby="headingOne" data-parent="#guidelineAccordion">
                    <div class="pl-3 gray2 font-size1 mb-1">
                        {{ $description }}
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>
