<div class="coin-block">
    <div class="coin-block__img-container">
        {!! $product->img !!}
    </div>
    <div class="coin-block__text-container">
        <p class="coin-block__coins-in-return">
            <img src="{{ asset('img/essential/coin.svg') }}" alt="coin" width="25" height="25" class="d-inline-block mr-1">
            <span>{{ $product->coins_in_return }}</span>
        </p>
        <div class="mb-3">
            <h4 class="font-size5">{{ $product->name }}</h4>
            <h5 class="font-weight-normal font-size3">{{ $product->description }}</h5>
        </div>
        <div class="coin-block__price">
            @auth
                <form action="{{ route('products.purchase', $product->id) }}" method="post">
                    @csrf
                    <input type="hidden" name="password">
                    <button type="submit" class="button--danger purchaseWithCashButton mr-1" data-product="{{ $product }}">
                        {{ $product->price_with_currency }}
                    </button>
                    @auth
                        <a class="font-size2 inactive blue4 underline-on-hover" href="{{ route('payment') }}">{{ auth()->user()->hasDefaultPaymentMethod() ? 'Update payment method' : 'Setup payment method' }}</a>
                    @endauth
                </form>
            @else
                <a href="{{ route('login') }}" class="button--danger">{{ $product->price_with_currency }}</a>
            @endauth
        </div>
    </div>
</div>
