<div class="libraryListItem border-bottom py-2">
    <input type="hidden" name="resource_id" value="{{ $resource->id }}">
    <div class="d-flex justify-content-between">
        {{-- Toggle Save Library --}}
        <form action="{{ route('libraries.toggle_resource', [$library->id, $resource->id]) }}" method="POST" class="">
            @csrf
            <input type="checkbox" class="styled-checkbox--green toggleSaveToLibraryCheckBox" data-resource="{{ $resource }}" data-library="{{ $library }}" name="library" id="library_{{ $library->id }}" @if($library->hasResource($resource)) checked @endif @if($library->isDisabled()) disabled @endif>
            <label for="library_{{ $library->id }}">{{ $library->name }}</label>
        </form>
    </div>
</div>
