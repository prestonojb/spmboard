<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateBoardPassSubscriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('board_pass_subscriptions', function (Blueprint $table) {
            $table->dropColumn('coin_product_type_id');
            $table->dropColumn('end_at');

            $table->unsignedInteger('duration_in_month');
            $table->timestamp('activated_at')->nullable();
            $table->timestamp('activation_code')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('board_pass_subscriptions', function (Blueprint $table) {
            $table->unsignedInteger('coin_product_type_id');
            $table->timestamp('end_at');

            $table->dropColumn('duration_in_months');
            $table->dropColumn('activated_at');
            $table->dropColumn('activation_code');
        });
    }
}
