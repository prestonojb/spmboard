<div class="panel question--block position-relative mb-3">

    <div class="question-block__header align-items-center mb-1">
        <ul class="question-block__tags">
            @if( $question->chapters->count() )
                @foreach($question->chapters as $chapter)
                    <li>
                        {{ $chapter->name }}
                    </li>
                @endforeach
            @else
                <li>
                    General
                </li>
            @endif
        </ul>
        <ul class="nav question-block__icon-group mb-1">
            <button class="question-block__check @if($question->answered) checked @endif m-0" data-toggle="popover" data-trigger="focus" data-placement="top" data-content="Lights up when question has an accepted answer.">
                <i class="far fa-check-circle"></i>
            </button>
        </ul>
    </div>

    <div class="question-block__user-info">
        <a href="{{ route('users.show', $question->user->username) }}">
            <img class="avatar" src="{{ $question->user->avatar ?? asset('img/essential/no-profile-picture.jpg') }}">
        </a>
        <div class="mt-1 text">
            <div class="user-info"><a class="username" href="{{ route('users.show', $question->user->username) }}">{{ $question->user->username }}</a></div>
            <span class="time">Asked {{ $question->created_at->longRelativeDiffForHumans() }}</span>
        </div>
    </div>
    <h1 class="question-block__title">
        <a href="{{ route('classroom.questions.show', [$classroom->id, $question->id]) }}" class="stretched-link"></a>
        <strong>{{ $question->question }}</strong>
    </h1>

    @if(!is_null($question->img))
        <div class="question-block__image-container">
            <img src="{{ $question->img }}" class="question-block__image">
        </div>
    @endif

    <div class="question-block__button-group">
        <button type="button" class="answer-button" data-url="{{ route('classroom.questions.show', [$classroom->id, $question->id]) }}"><span class="icon"></span> <span class="text">Answer</span> · <span class="answer-count">{{ $question->get_answers_from_classroom_count( $classroom ) }}</span></button>
    </div>
</div>
