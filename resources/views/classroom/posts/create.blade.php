@extends('classroom.layouts.app')

@section('meta')
    <title>{{ $classroom->name }} - Create Post | {{ config('app.name') }} Classroom</title>
@endsection

@section('content')

@component('classroom.common.breadcrumbs')
    <a href="{{ route('classroom.classrooms.show', $classroom->id) }}">Dashboard</a> /
    <a href="{{ route('classroom.posts.index', $classroom->id) }}">Posts</a> /
    Add
@endcomponent

@component('classroom.common.page_heading', ['title' => 'Add Post'])
@endcomponent


<div class="ask-wrapper">
    @component('classroom.posts.common.form', ['classroom' => $classroom])
    @endcomponent
</div>

<div class="py-2"></div>
@endsection

@section('scripts')
<script>
$(function(){
});
</script>
@endsection
