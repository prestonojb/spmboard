@extends('forum.layouts.app')

@section('meta')
<title>Reset Password | {{ config('app.name') }}</title>
@endsection

@section('content')
@include('main.navs.header')
<div class="wrapper">
    <div class="text-center">
        <h1 class="heading">Reset Password</h1>
        <p class="mb-3">Don't lose your password again!</p>
    </div>

    <form method="POST" action="{{ route('password.update') }}">
        @csrf

        <input type="hidden" name="token" value="{{ $token }}">

        <div class="form-field">

            <label>{{ __('Email') }}</label>
            <input type="email" name="email" value="{{ $email }}" required readonly>

            @error('email')
                <div class="error-message" role="alert">
                    {{ $message }}
                </div>
            @enderror

        </div>

        <div class="form-field">

            <label>{{ __('New Password') }}</label>
            <input type="password" name="password" required autofocus autocomplete="off">

            @error('password')
                <div class="error-message" role="alert">
                    {{ $message }}
                </div>
            @enderror

        </div>

        <div class="form-field">

            <label>{{ __('Confirm New Password') }}</label>
            <input type="password" name="password_confirmation" required autocomplete="off">

            @error('password_confirmation')
                <div class="error-message" role="alert">
                    {{ $message }}
                </div>
            @enderror

        </div>

        <div class="text-center">
            <button type="submit" class="button--primary">{{ __('Set New Password') }}</button>
        </div>

    </form>
</div>


@endsection
