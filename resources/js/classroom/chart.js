Chart.defaults.global.legend.display = false;

var colors = '#2174D3';

if( typeof noOfResourcePostsTimeLabels !== 'undefined' && typeof noOfResourcePostsTimeData !== 'undefined' ) {
    var ctx = document.getElementById('noOfResourcePostTime').getContext('2d');
    var labels = noOfResourcePostsTimeLabels;
    var data = noOfResourcePostsTimeData;
    var chart = new Chart(ctx, {
        type: 'line',
        data: {
            // Blade directive 'json' converts Laravel array to Javascript array
            labels: labels,
            datasets: [{
                borderColor: colors,
                fill: false,
                lineTension: 0,
                data: data,
            }]
        },
    });
};

if( typeof noOfHomeworksTimeLabels !== 'undefined' && typeof noOfHomeworksTimeData !== 'undefined' ) {
    var ctx = document.getElementById('noOfHomeworkTime').getContext('2d');
    var labels = noOfHomeworksTimeLabels;
    var data = noOfHomeworksTimeData;
    var chart = new Chart(ctx, {
        type: 'line',
        data: {
            // Blade directive 'json' converts Laravel array to Javascript array
            labels: labels,
            datasets: [{
                borderColor: colors,
                fill: false,
                lineTension: 0,
                data: data,
            }]
        },
    });
}

if( typeof homeworkMarksTimeLabels !== 'undefined' && typeof homeworkMarksTimeData !== 'undefined' ) {
    var ctx = document.getElementById('homeworkMarksTime').getContext('2d');
    var colors = '#198dc2';
    var data = homeworkMarksTimeData;
    var chart = new Chart(ctx, {
        // The type of chart we want to create
        type: 'line',
        // The data for our dataset
        data: {
            // Blade directive 'json' converts Laravel array to Javascript array
            labels: homeworkMarksTimeLabels,
            datasets: [{
                borderColor: colors,
                fill: false,
                lineTension: 0,
                data: data,
            }]
        },
    });
}
