@php
    $reward = [10, 20, 30, 40, 50, 50, 50];
    $coin_reward_for_day_7 = 5;
@endphp

<section class="qi__right-col">
    <div class="panel qi__login-rewards">
        <h3 class="block-title">Daily Login Rewards</h3>
        @auth
            <div class="countdown">
                <div class="number">{{ auth()->user()->consecutive_login_days }} <span>day{{ auth()->user()->consecutive_login_days > 1 ? 's':'' }}</span></div>
                <div class="text">Login Streak</div>
            </div>
        @endauth

        @auth
            @if( auth()->user()->hasRewardPoints() || auth()->user()->hasCoins() )
                {{-- From Day 1 to 7 --}}
                @foreach( range(1,7) as $i )
                    <input type="checkbox" id="Day {{ $i }}" {{ auth()->user()->reward_day >= $i ? 'checked' : '' }} disabled>
                    <label for="Day {{ $i }}" class="auth-login-reward">
                        Day {{ $i }}
                        <div class="coin">
                            @if($i != 7)
                                {{ $reward[$i-1] }} points
                            @else
                                @if( auth()->user()->hasCoins() )
                                    5 coins
                                @else
                                    {{ $reward[$i-1] }} points
                                @endif
                            @endif
                        </div>
                    </label>
                @endforeach

                <p class="caption">
                    Log in consecutively and receive exclusive rewards!<br>
                    <small><i>*Login rewards reset every 7 days.</i></small>
                </p>
            @else
                {{-- Account type not eligible for login rewards     --}}
                <div class="py-1 gray2">{{ ucfirst(auth()->user()->account_type) }} account is not eligible for login rewards for now.</div>
            @endif

        @else
            @foreach(range(1, 7) as $i)
                <input type="checkbox" id="Day {{ $i }}" disabled>
                <label for="Day {{ $i }}" class="guest-login-reward">
                    Day {{ $i }}
                    <div class="coin">@if($i == 7) Online class @elseif($i !== 7) {{ $reward[$i-1] }} points @endif</div>
                </label>
            @endforeach

            <p class="caption">
                Log in consecutively and receive exclusive rewards!<br>
                <small><i>*Login rewards reset every 7 days.</i></small>
            </p>
        @endauth
    </div>
</section>
