<?php

namespace App\Http\Controllers\Voyager;

use App\CoinAction;
use App\Notifications\WithdrawalRequestResponded;
use App\WithdrawalRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;

class VoyagerWithdrawalRequestController extends \TCG\Voyager\Http\Controllers\VoyagerBaseController
{
    /**
     * List all withdrawal requests
     */
    public function index(Request $request)
    {
        $withdrawal_requests = WithdrawalRequest::latest()->get();
        return view('vendor.voyager.withdrawal_requests.index', compact('withdrawal_requests'));
    }

    public function complete(WithdrawalRequest $withdrawal_request)
    {
        // Validation
        request()->validate([
            'receipt' => 'required|file|mimes:pdf|max:20000',
            'amount_transferred_in_usd' => 'required|regex:/^\d+(\.\d{1,2})?$/',
            'comment' => 'nullable',
        ]);

        // Check user balance
        if($withdrawal_request->user->coin_balance < $withdrawal_request->withdrawal_amount_in_coin) {
            return redirect()->back()->with(['alert-type' => 'error', 'message' => 'User coin balance exceeds withdrawal amount!']);
        }

        // Update request
        $withdrawal_request->update([
            'status' => 'completed',
            'amount_transferred_in_usd' => request('amount_transferred_in_usd'),
            'comment' => request('comment'),
        ]);

        // Upload receipt
        if(request()->has('receipt') && !is_null(request('receipt'))) {
            $withdrawal_request->saveReceipt(request('receipt'));
        }

        // Deduct coins from user
        $withdrawal_request->user->deductCoins($withdrawal_request->withdrawal_amount_in_coin);
        $withdrawal_request->user->coin_actions()->create([
            'name' => 'Coin Withdrawal',
            'coin_actionable_type' => WithdrawalRequest::class,
            'coin_actionable_id' => $withdrawal_request->id,
            'amount' => -($withdrawal_request->withdrawal_amount_in_coin),
        ]);

        $withdrawal_request->user->notify(new WithdrawalRequestResponded($withdrawal_request));

        return redirect()->back()->with(['alert-type' => 'success', 'message' => 'Withdrawal Request is completed!']);
    }

    public function cancel(WithdrawalRequest $withdrawal_request)
    {
        // Validation
        request()->validate([
            'comment' => 'nullable',
        ]);

        // Update request
        $withdrawal_request->update([
            'status' => 'cancelled',
            'comment' => request('comment'),
        ]);

        $withdrawal_request->user->notify(new WithdrawalRequestResponded($withdrawal_request));

        return redirect()->back()->with(['alert-type' => 'success', 'message' => 'Withdrawal Request is cancelled!']);
    }
}
