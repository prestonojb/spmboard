<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BlogAuthor;
use App\Blog;
use App\BlogCategory;

class BlogAuthorController extends Controller
{
    public function create()
    {
        return view('blog.authors.create');
    }
    
    public function store()
    {
        request()->validate([
            'name' => 'required|unique:blog_authors',
            'description' => 'required',
            'img' => 'required|mimes:jpeg,png,jpg,gif,svg|max:2000'
        ]);

        $author = new BlogAuthor;
        
        $author->name = request()->name;
        $author->description = request()->description;
        if(request()->guest){
            $author->guest = true;
        } else {
            $author->guest = false;
        }
    
        $path = request()->file('img')->store('public/author_img');
        $filename = request()->file('img')->hashName();
        $author->img = $filename;
        
        $author->save();
        
        $blogs = Blog::orderByDesc('created_at')->paginate(10);
        $blog_categories = BlogCategory::all();
        request()->session()->flash('success', 'Author added successfully!');
        return view('blog.index', ['blogs' => $blogs, 'blog_categories' => $blog_categories]);
    }
    

    public function show(BlogAuthor $blog_author)
    {

        $blogs = Blog::where('blog_author_id', $blog_author->id)->take(6)->get();
        return view('blog.authors.show', ['blog_author' => $blog_author, 'blogs' => $blogs]);
    
    }

    public function edit(BlogAuthor $blog_author)
    {        
        return view('blog.authors.edit', ['blog_author' => $blog_author]);
    }

    public function update(BlogAuthor $blog_author)
    {
        request()->validate([
            'name' => 'required',
            'description' => 'required',
            'img' => 'nullable|mimes:jpeg,png,jpg,gif,svg|max:2000'
        ]);

        $blog_author->name = request()->name;
        $blog_author->description = request()->description;

        if(request()->file('img')){
            $author_img_path = storage_path('app/public/author_img/'. $blog_author->img);
            unlink($author_img_path);

            $path = request()->file('img')->store('public/author_img');
            $filename = request()->file('img')->hashName();
            $blog_author->img = $filename;
        }
        
        $blog_author->save();

        $blogs = Blog::orderByDesc('created_at')->paginate(10);
        $blog_categories = BlogCategory::all();
        request()->session()->flash('success', 'Author updated successfully!');
        return view('blog.index', ['blogs' => $blogs, 'blog_categories' => $blog_categories]);

    }


    
}
