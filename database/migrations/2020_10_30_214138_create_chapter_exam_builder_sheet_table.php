<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChapterExamBuilderSheetTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chapter_exam_builder_sheet', function (Blueprint $table) {
            $table->unsignedInteger('chapter_id');
            $table->unsignedInteger('exam_builder_sheet_id');

            $table->primary(['chapter_id', 'exam_builder_sheet_id'], 'chapter_sheet_primary');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chapter_exam_builder_sheet');
    }
}
