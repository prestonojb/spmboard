<?php

namespace App\Notifications\Classroom;

use App\Lesson;
use App\Student;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class LessonAdded extends Notification implements ShouldQueue
{
    use Queueable;

    public $student;
    public $lesson;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Student $student, Lesson $lesson)
    {
        $this->student = $student;
        $this->lesson = $lesson;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    // /**
    //  * Get the mail representation of the notification.
    //  *
    //  * @param  mixed  $notifiable
    //  * @return \Illuminate\Notifications\Messages\MailMessage
    //  */
    // public function toMail($notifiable)
    // {
    //     return (new MailMessage)
    //                 ->line('The introduction to the notification.')
    //                 ->action('Notification Action', url('/'))
    //                 ->line('Thank you for using our application!');
    // }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $student = $this->student;
        $lesson = $this->lesson;
        $classroom = $lesson->classroom;
        $teacher = $classroom->teacher;

        if( $notifiable->account_type == 'student' ) {
            $title = "<b>{$teacher->name}</b> added <b>{$lesson->name}</b> Lesson to <b>{$classroom->name}</b> class.";
        } elseif( $notifiable->account_type == 'parent' ) {
            $title = "<b>{$teacher->name}</b> added a lesson to <b>{$classroom->name}</b> class for {$student->name}.";
        }
        return [
            'type' => Lesson::class,
            'target' => $lesson,
            'title' => $title,
        ];
    }
}
