@extends('classroom.layouts.app')

@section('meta')
    <title>Edit Classroom | {{ config('app.name') }} Classroom</title>
@endsection

@section('content')
@component('classroom.common.breadcrumbs')
    <a href="{{ route('classroom.landing') }}">Home</a> /
    Edit
@endcomponent

@component('classroom.common.page_heading', ['title' => 'Edit Classroom'])
@endcomponent

<div class="maxw-460px mx-auto my-5">
    <div class="panel mb-2">
        <form action="{{ route('classroom.classrooms.update', $classroom->id) }}" method="POST">
            @csrf
            @method('PATCH')
            <div class="form-field">
                <label>{{ __('Class Name') }}</label>
                <input type="text" name="name" value="{{ old('name') ?? $classroom->name }}" required autofocus autocomplete="name" placeholder="Class Name">
                @error('name')
                    <span class="error-message">{{ $message }}</span>
                @enderror
            </div>

            <div class="text-center">
                <button type="submit" class="button--primary disableOnSubmitButton mb-2">Update Classroom</button>
            </div>
        </form>
    </div>
</div>

@endsection
