@extends('voyager::master')

@section('page_title', 'Chapters')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_header')
    <h1>Chapters</h1>
@endsection

@section('content')
@if(auth()->user()->isAdmin())
    <div style="width: 90vw;">
        {{-- Subject Filter --}}
        <div class="">
            <form action="{{ url()->current() }}" method="get">
                @csrf
                <div class="form-field">
                    <label for="">Chapters</label>

                    <select name="subject" id="">
                        <option value="" disabled selected>Select a subject</option>
                        @foreach($subjects as $subject)
                            <option value="{{ $subject->id }}" @if(!is_null($selected_subject) && $subject->id == $selected_subject->id) selected @endif>{{ $subject->name_with_exam_board }}</option>
                        @endforeach
                        @error('subject')
                            <div class="text-danger">{{ $error }}</div>
                        @enderror
                    </select>

                </div>
                <button type="submit" class="btn btn-primary">Apply</button>
                <a href="{{ route('voyager.chapters.create') }}" class="btn btn-warning">
                    <i class="fas fa-plus"></i> Add Chapter
                </a>
                <a href="{{ route('voyager.chapters.bulk_create') }}" class="btn btn-danger">
                    <i class="fas fa-plus"></i> Bulk Add
                </a>
            </form>
        </div>

        @if(!is_null($selected_subject))
            <!-- Simple List -->
            <div id="chapterList" class="list-group">
                @foreach($selected_subject->chapters as $chapter)
                    <div class="list-group-item border-bottom d-flex justify-content-between mb-1" data-chapter-id="{{ $chapter->id }}">
                        <div>
                            <b>{{ $chapter->name }}</b> <br>
                            <small>{{ $chapter->getSubchaptersCount() }} subchapter(s)</small>
                        </div>
                        <div>
                            <a class="btn btn-primary" href="{{ route('voyager.chapters.show', $chapter->id) }}">View</a>
                            <a class="btn btn-warning" href="{{ route('voyager.chapters.edit', $chapter->id) }}">Edit</a>
                            <form class="d-inline-block" action="{{ route('voyager.chapters.destroy', $chapter->id) }}" method="post">
                                @csrf
                                @method('DELETE')
                                <button class="btn btn-danger delete" href="">Delete</button>
                            </form>
                        </div>
                    </div>
                @endforeach
            </div>

            <form action="{{ route('voyager.chapters.reorder') }}" method="post" id="orderChapterForm">
                @csrf
                <input type="hidden" name="chapter_order">
                <div class="text-right">
                    <button type="submit" class="btn btn-primary">Save Order</button>
                </div>
            </form>
        @else
            <h3 class="gray2 text-center">Please select a subject first.</h3>
        @endif

        <div class="py-3"></div>
    </div>
@endif
@stop

@section('javascript')
<!-- Latest Sortable -->
<script src="https://raw.githack.com/SortableJS/Sortable/master/Sortable.js"></script>
<script>
$(function(){
    // Simple list
    Sortable.create(chapterList, {
        scroll: true, // Enable the plugin. Can be HTMLElement.
        scrollSensitivity: 30, // px, how near the mouse must be to an edge to start scrolling.
        scrollSpeed: 10, // px, speed of the scrolling
        bubbleScroll: true // apply autoscroll to all parent elements, allowing for easier movement
    });

    var $chapterList = $('#chapterList');

    $('#orderChapterForm button[type=submit]').click(function(e){
        e.preventDefault();
        $(this).prop('disabled', true);

        // Fill chapterOrder data
        var chapterOrderArr = [];

        $.each($chapterList.find('.list-group-item'), function (idx, item) {
            chapterOrderArr.push($(this).data('chapter-id'));
        });

        $('[name=chapter_order]').val( JSON.stringify(chapterOrderArr) );
        $(this).closest('form').submit();
    });
});
</script>
@stop
