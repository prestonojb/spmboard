@extends('resources.layouts.app')
@section('meta')
    <title>{{ $subject->name }} - Notes | {{ $exam_board->name }} {{ config('app.name') }} </title>
@endsection

@section('header')
    @component('resources.common.header', ['type' => 'note', 'exam_board' => $exam_board])
    @endcomponent
@endsection

@section('breadcrumbs')
    @component('resources.common.breadcrumbs')
        <a href="{{ route('notes.home', $exam_board->slug) }}">Home</a> /
        {{ $subject->name }}
    @endcomponent
@endsection

@section('context_panel')
    @component('resources.common.context_panel', ['exam_board' => $exam_board->name, 'title' => $subject->name, 'description' => $subject->description])
        <h3 class="font-size3">
            Part of <a href="{{ route('topical_past_papers.home', [$subject->exam_board->slug]) }}" class="font-size-inherit blue-underline">{{ $subject->exam_board->name }}</a>
        </h3>
    @endcomponent
@endsection

@section('body')
    {{-- Filter --}}
    @include('resources.notes.common.filter')

    @if(count($notes ?? []))
        <p class="font-size2 gray2">Showing {{ $notes->firstItem() }}-{{ $notes->lastItem() }} of {{ $notes->total() }} results</p>

        <div class="form-row">
            @foreach($notes as $note)
                <div class="col-6 col-md-4 col-lg-3 mb-2">
                    @component('resources.common.panel', ['resource' => $note->resource])
                    @endcomponent
                </div>
            @endforeach
        </div>

        {{ $notes->links() }}
    @else
        <div class="py-3 text-center">
            <img src="{{ asset('img/essential/empty.svg') }}" alt="" width="300" height="300">
            <p class="mb-0">No notes available</p>
        </div>
    @endif
@endsection
