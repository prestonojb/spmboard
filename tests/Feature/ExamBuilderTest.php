<?php

namespace Tests\Feature;

use App\Chapter;
use App\ExamBuilderItem;
use App\Subject;
use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Http\UploadedFile;
use Tests\TestCase;
use UsersTableSeeder;

class ExamBuilderTest extends TestCase
{
    use DatabaseTransactions, WithoutMiddleware;

    public function setUp() : void
    {
        parent::setUp();

        $this->valid_file_formats = ['png'];

        $this->admin = User::whereHas('role', function($q){
                            return $q->where('name', 'admin');
                        })->first();

        // Subject
        $this->subject = Subject::first();
        // Chapter
        $this->chapter = $this->subject->chapters()->first();
        $this->exam_board = $this->subject->exam_board;
    }

    /**
     * @group exam_builder
     * @return void
     */
    public function testStoreItems()
    {
        ExamBuilderItem::truncate();

        Storage::fake('s3');
        $image = UploadedFile::fake()->image('Algebra_MJ_2009_04_Q1.png');

        // Simulate file upload
        $response = $this->actingAs($this->admin)
                    ->post('admin/exam-builder-items', [
                        'subject_id' => $this->subject->id,
                        'chapter_id' => $this->chapter->id,
                        'imgs' => [$image],
                    ]);

        $response->assertRedirect();

        // Record is correctly uploaded in database
        $this->assertTrue(
            ExamBuilderItem::where('name', 'Algebra MJ 2009 04 Q1')
                            ->where('label_number', 1)
                            ->where('subject_id', $this->subject->id)
                            ->where('chapter_id', $this->chapter->id)
                            ->where('year', 2009)
                            ->where('season', 'MJ')
                            ->where('paper_number', 4)
                            ->whereJsonContains('question_imgs', "exam-builder/{$this->exam_board->slug}/{$this->subject->name}/{$image->getClientOriginalName()}")
                            ->whereNull('answer_imgs')
                            ->exists()
        );

        // Image uploaded to storage
        Storage::disk('s3')->assertExists( "exam-builder/{$this->exam_board->slug}/{$this->subject->name}/{$image->getClientOriginalName()}" );
    }

    /**
     * @group exam_builder
     * Test Item format
     * @return void
     */
    public function testItemFormats()
    {
        $formats = ['png', 'PNG', 'jpeg', 'JPEG', 'jpg', 'JPG', 'gif', 'GIF', 'pdf', 'PDF'];
        for($i=0; $i < count($formats); $i++) {
            // Remove all items before download
            ExamBuilderItem::truncate();
            $init_count = ExamBuilderItem::count();

            $image = UploadedFile::fake()->image('Algebra_MJ_2009_04_Q1.'.$formats[$i]);

            // Simulate file upload
            $response = $this->actingAs($this->admin)
                        ->post('admin/exam-builder-items', [
                            'subject_id' => $this->subject->id,
                            'chapter_id' => $this->chapter->id,
                            'imgs' => [$image],
                        ]);

            $response->assertRedirect();

            // Successful upload
            if( $i == 0 ) {
                // Assert item added
                $this->assertEquals($init_count + 1, ExamBuilderItem::count());
            } else {
                // Assert item not added
                $this->assertEquals($init_count, ExamBuilderItem::count());
            }
        }
    }

    /**
     * @group exam_builder
     * Test Item no duplicate files
     * @return void
     */
    public function testItemUpdateImages()
    {
        Storage::fake('s3');
        // Remove all items before download
        ExamBuilderItem::truncate();

        $init_image = UploadedFile::fake()->image('Algebra_MJ_2009_04_Q1.png', 300);

        // Simulate file upload
        $response = $this->actingAs($this->admin)
                    ->post('admin/exam-builder-items', [
                        'subject_id' => $this->subject->id,
                        'chapter_id' => $this->chapter->id,
                        'imgs' => [$init_image],
                    ]);

        $new_image = UploadedFile::fake()->image('Algebra_MJ_2009_04_Q1.png', 600);
        // File Uploaded
        $response = $this->actingAs($this->admin)
                            ->post('admin/exam-builder-items', [
                                'subject_id' => $this->subject->id,
                                'chapter_id' => $this->chapter->id,
                                'imgs' => [$new_image],
                            ]);

        $image_size = Storage::disk('s3')->size("exam-builder/{$this->exam_board->slug}/{$this->subject->slug}/Algebra_MJ_2009_04_Q1.png");
        $this->assertEquals( $image_size, $new_image->getSize() );

        $item = ExamBuilderItem::first();
        // Assert question image DB column updated correctly
        $this->assertEquals($item->question_img_urls, ["exam-builder/{$this->exam_board->slug}/{$this->subject->slug}/Algebra_MJ_2009_04_Q1.png"]);
    }
}
