@extends('main.layouts.app')

@section('meta')
<title>Refer a Friend | {{ config('app.name') }}</title>
@endsection

@section('content')
@include('main.navs.header')
<div class="py-2"></div>

<div class="container">
    <div class="row">
        <div class="maxw-740px mx-auto">
            <h1 class="font-size7 blue6">Refer a Friend</h1>
            <h2 class="font-size3 font-weight-normal">
                Share the Board platform with your family, friends and teachers for <b>{{ setting('marketing.referral_rp_amount') }} RP</b> for both of you! <br>
            </h2>
            <p class="gray2 font-size2"><i>*Parent accounts are not eligible for Reward Points.</i></p>

                @if($user->hasReferralLink())
                    <div class="panel">
                        <div>
                            <div class="d-flex justify-content-between align-items-start">
                                <label for="">Share your referral link!</label>
                                <button class="button-link blue4 underline-on-hover copyButton" data-input-id="referralLink">Copy</button>
                            </div>
                            <input type="text" readonly value="{{ $user->referral_link }}" id="referralLink">
                        </div>
                        <p class="gray2 font-size2">Tip: After your referee registers a Board account with your referral link, both of you will be entitled to extra Reward Points!</p>
                    </div>
                @else
                    <form action="{{ route('referral.generate_referral_link') }}" method="post">
                        @csrf
                        <button class="button--danger">Generate my Referral Link!</button>
                    </form>
                @endif

            </div>
        </div>
    </div>
</div>

<div class="py-2"></div>

@endsection

@section('scripts')
@endsection
