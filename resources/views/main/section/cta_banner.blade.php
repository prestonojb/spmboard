<section class="main__cta-banner">
    <div class="container">
        <h4 class="h2 text-white mb-4"><b>{{ $title }}</b></h4>
        @if(isset($align_buttons) && $align_buttons)
            <div class="d-flex flex-wrap justify-content-center align-items-center button main__cta-banner-btn-link-group">
                {{ $slot }}
            </div>
        @else
            <div class="mx-auto">
                {{ $slot }}
            </div>
        @endif
    </div>
</section>
