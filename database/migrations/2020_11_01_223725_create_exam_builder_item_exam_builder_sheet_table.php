<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExamBuilderItemExamBuilderSheetTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exam_builder_item_exam_builder_sheet', function (Blueprint $table) {
            $table->unsignedInteger('exam_builder_item_id');
            $table->unsignedInteger('exam_builder_sheet_id');

            $table->primary(['exam_builder_item_id', 'exam_builder_sheet_id'], 'item_sheet_primary');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exam_builder_item_exam_builder_sheet');
    }
}
