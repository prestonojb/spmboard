<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Subject;
use App\TopicalPastPaper;
use Faker\Generator as Faker;

$subjects = Subject::all();

$factory->define(TopicalPastPaper::class, function (Faker $faker) use ($subjects) {
    $subject = $subjects->random();
    $chapter = $subject->chapters->random();
    return [
        'subject_id' => $subject->id,
        'chapter_id' => $chapter->id,
        'name' => $faker->sentence(3),
        'paper_number' => rand(1, 6),
        'qp_url' => 'https://www.w3.org/WAI/ER/tests/xhtml/testfiles/resources/pdf/dummy.pdf',
        'ms_url' => 'https://www.w3.org/WAI/ER/tests/xhtml/testfiles/resources/pdf/dummy.pdf',
    ];
});
