<nav class="navbar header">
    <!-- Hamburger-icon -->
    <button class="hamburger-button" type="button" aria-expanded="false" aria-label="Toggle navigation">
        <i class="fa fa-bars" aria-hidden="true"></i>
    </button>
    <a class="d-none d-lg-inline header2__logo" href="{{ route('questions.index', $exam_board) }}">
        <img class="" src="{{ asset('img/logo/logo_light_'.$exam_board->slug.'.png') }}">
    </a>
    <ul class="header-right-menu">
        <li class="d-sm-none ">
            <button type="button" class="header-icon search-icon"><span class="iconify" data-icon="bx:bx-search" data-inline="false"></span></button>
        </li>

        <form class="d-sm-none mobile-search-form" method="GET" action="{{ route('search.index', $exam_board->slug) }}">
            <i class="d-none d-sm-inline fa fa-search" aria-hidden="true"></i>
            <input class="mobile-search-input-header" type="text" name="q" placeholder="Search" autocomplete="off" required>
            <button class="d-none" type="submit"></button>
        </form>

        <form class="d-none d-sm-inline-block header2__search-form" method="GET" action="{{ route('search.index', $exam_board->slug) }}">
            <span class="iconify" data-icon="bx:bx-search" data-inline="false"></span>
            <input type="text" name="q" placeholder="Search" autocomplete="off" required>
            <button class="d-none" type="submit"></button>
        </form>

        @auth
            @component('common.header.notification_dropdown', ['user' => auth()->user()])
            @endcomponent
        @endauth

        @component('common.header.profile_dropdown', ['hide_rp_coins' => false])
        @endcomponent

    </ul>
</nav>

