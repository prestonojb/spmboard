@extends('resources.layouts.app')
@section('meta')
    <title>{{ $note->name }} - Notes | {{ $exam_board->name }} {{ config('app.name') }} </title>
@endsection

@section('header')
    @component('resources.common.header', ['type' => 'note', 'exam_board' => $exam_board])
    @endcomponent
@endsection

@section('breadcrumbs')
    @component('resources.common.breadcrumbs')
        <a href="{{ route('notes.home', $exam_board->slug) }}">Home</a> /
        <a href="{{ route('notes.subject_show', [$exam_board->slug, $note->subject_id]) }}">{{ $note->subject->name }}</a> /
        {{ str_limit($note->name, 30) }}
    @endcomponent
@endsection

@section('context_panel')
    @component('resources.common.context_panel', ['exam_board' => $note->subject->exam_board->name, 'title' => $note->name, 'description' => $note->description, 'locked' => !Gate::allows('view_unlocked', $note->resource)])
        {{-- Ratings --}}
        <div class="position-relative d-inline-flex flex-wrap align-items-center mb-2">
            <div class="Stars sm mb-1 mr-2" style="--rating: {{ $note->resource->rating }};"></div>
            <a href="#reviews" class="blue5 underline-on-hover">{{ $note->resource->getTotalReviews() }} reviews</a>
        </div>

        <h3 class="font-size3">Part of <a href="{{ route('notes.subject_show', [$note->subject->exam_board->slug, $note->subject->id]) }}" class="font-size-inherit blue-underline">{{ $note->subject->name }}</a>
            @if(!is_null($note->chapter))
                | <a href="javascript:void(0)" class="font-size-inherit blue-underline">{{ $note->chapter->name }}</a>
            @endif
        </h3>

        {{-- Toolbar --}}
        <div class="py-1 my-1 border-left-gray5-3px pl-2">
            {{-- Download --}}
            @can('download_pdf', $note)
                <a href="{{ $note->url }}" class="gray-button sm mr-2" target="_blank">
                    <span class="iconify" data-icon="carbon:generate-pdf" data-inline="false"></span>Download</a>
            @else
                <a href="javascript:void(0)" class="gray-button disabled sm mr-2">
                    <span class="iconify" data-icon="carbon:generate-pdf" data-inline="false"></span>Download</a>
            @endcan

            {{-- Save --}}
            <button type="button" class="gray-button sm" data-toggle="modal" data-target="#saveToLibraryModal">
                <span class="iconify" data-icon="cil:playlist-add" data-inline="false"></span>Save</button>

            @component('resources.common.save_to_library', ['resource' => $note->resource])
            @endcomponent
        </div>
    @endcomponent
@endsection

@section('side')
    @if(!is_null($note->chapter))
        <h4 class="mb-3 pb-2 border-bottom">More from {{ $note->chapter->name }}</h4>
        @if(count($note->getRelatedNotesByChapter() ?? []) > 0)
            <div class="form-row">
                @foreach($note->getRelatedNotesByChapter() as $related_note)
                    <div class="col-6 col-lg-12 mb-2">
                        @component('resources.common.panel', ['resource' => $related_note->resource])
                        @endcomponent
                    </div>
                @endforeach
            </div>
        @else
            <div class="gray2 text-center ">No suggestion</div>
        @endif
    @endif
@endsection

@section('main')
    {{-- Edit --}}
    @can('edit', $note->resource)
        <a href="{{ route('notes.edit', [$exam_board->slug, $note->id]) }}" class="d-inline-block underline-on-hover mr-1 mb-2"><span class="iconify" data-icon="bx:bx-edit" data-inline="false"></span> Edit</a>
    @endcan

    {{-- Delete --}}
    @can('delete', $note->resource)
        <form action="{{ route('notes.delete', [$exam_board->slug, $note->id]) }}" method="POST" class="d-inline">
            @csrf
            @method('delete')
            <button class="red button-link underline-on-hover deleteButton">
                <span class="iconify" data-icon="carbon:delete" data-inline="false"></span>
                Delete
            </button>
        </form>
    @endcan

    {{-- Share & Report toolbar --}}
    @php
        $tag = 'resource';
        $description = '[Note ID: '.$note->id.'] '.$note->name.' is not working as expected.';
        $email = auth()->check() ? auth()->user()->email : '';
    @endphp
    @component('resources.common.share_and_report_toolbar', ['tag' => $tag, 'description' => $description, 'email' => $email])
    @endcomponent

    <div class="py-1"></div>

    {{-- Tags --}}
    <div class="mb-1">
        {!! $note->tags_html !!}
        @if($note->isFree())
            <a href="javascript:void(0)" class="status-tag red--inverse mr-1">Free</a>
        @endif
        <a href="javascript:void(0)" class="status-tag {{ $note->type_color }}">{{ $note->display_type }}</a>
    </div>

    {{-- Info --}}
    @component('resources.sections.info', ['resource' => $note->resource])
    @endcomponent

    <div class="py-1"></div>


    @can('view_unlocked', $note->resource)
        {{-- Full --}}
        <div class="w-100 custom_pdf_viewer" data-url="{{ $note->url }}"></div>
    @else
        <div class="position-relative">
            {{-- Preview --}}
            <div class="w-100 custom_pdf_viewer_preview" data-max-page-preview="1" data-url="{{ $note->url }}"></div>
            <div class="d-flex justify-content-between align-items-center text-white bg-blue6 custom-pdf-preview-prompt-block">
                <div>
                    <p class="mb-2 font-size2">End of preview</p>
                    <p class="font-size6 font-weight-bold">Unlock this study material with your coins!</p>
                    @if(auth()->user()->is_student())
                        <p class="font-size3"><i>You can save <b>{{ setting('board_pass.resource_discount_in_percentage') }}%</b> per unlock with Board Pass! <a target="_blank" href="{{ route('board_pass') }}" class="text-white"><u>Learn More</u></a></i></p>
                    @elseif(auth()->user()->is_teacher())
                        <p class="font-size3"><i>You can save <b>{{ setting('board_premium.resource_discount_in_percentage') }}%</b> per unlock with Board Premium! <a target="_blank" href="{{ route('classroom.prices.index') }}" class="text-white"><u>Learn More</u></a></i></p>
                    @endif
                </div>

                <div class="d-flex justify-content-center align-items-center">
                    <form action="{{ route('notes.unlock', [$exam_board->slug, $note->id]) }}" method="post">
                        @csrf
                        <button type="submit" class="button--primary--inverse confirmButton" data-title="Are you sure?" data-text="You are unlocking {{ $note->name }} with {{ $note->getAdjustedCoinPrice(auth()->user()) }} coins!">

                            @if($note->getAdjustedCoinPrice(auth()->user()) != $note->coin_price)
                                <span class="d-inline-flex flex-column flex-md-row align-items-center">
                                    <img src="{{ asset('img/essential/coin.svg') }}" class="inline-image">
                                    <span>
                                        <span class="blue6 d-inline-flex flex-column mx-1">
                                            <span>
                                                <b class="font-size7">{{ $note->getAdjustedCoinPrice(auth()->user()) }}</b>
                                                <s>{{ $note->coin_price }}</s>
                                            </span>
                                            <span class="blue6">coins</span>
                                        </span>
                                    </span>
                                </span>
                            @else
                                <img src="{{ asset('img/essential/coin.svg') }}" class="inline-image">
                                <span class="blue6">
                                    <b>{{ $note->coin_price }} coins</b>
                                </span>
                            @endif

                        </button>
                    </form>
                </div>
            </div>
        </div>
    @endcan

    {{-- Share & Report --}}
    @component('resources.common.share_and_report_toolbar', ['tag' => $tag, 'description' => $description, 'email' => $email])
    @endcomponent

    <div class="py-3"></div>

    {{-- Author --}}
    <div class="border-top border-bottom">
        @component('resources.common.author_details', ['user' => $note->owner])
        @endcomponent
    </div>

    <div class="py-4"></div>

    {{-- Write a Review --}}
    @component('resources.sections.review', ['resource' => $note->resource, 'exam_board' => $exam_board])
    @endcomponent
@endsection
