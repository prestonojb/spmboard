<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\SoftDeletes;

class Guide extends Model
{
    use SoftDeletes;

    protected $guarded = [];

    public function exam_board()
    {
        return $this->subject->exam_board;
    }

    public function subject()
    {
        return $this->belongsTo(Subject::class, 'subject_id', 'id');
    }

    public function chapter()
    {
        return $this->belongsTo(Chapter::class);
    }

    public function topic()
    {
        return $this->belongsTo(Chapter::class);
    }

    public function resource()
    {
        return $this->morphOne('App\Resource', 'resourceable');
    }

    /**
     * Returns author of guides (fallback to user with admin resource email)
     * @return App\User
     */
    public function getAuthor()
    {
        return !is_null($this->user_id) ? User::find($this->user_id)
                                        : User::where('email', setting('admin.resource_email'))->first();
    }

    /**
     * -------------
     * Accessors
     * -------------
     */

    public function getNameAttribute()
    {
        return $this->title;
    }

    public function getImgAttribute($value)
    {
        if($value){
            if(strpos($value, 'amazonaws')){
                //Amazon link for image
                return $value;
            } else{
                //Amazon S3
                $updated_value = str_replace('\\', '/', $value);
                return Storage::cloud()->url($updated_value);
            }
        } else {
            return null;
        }
    }

    public function getPrimaryAttribute()
    {
        return $this->title;
    }

    public function getSecondaryAttribute()
    {
        return $this->description;
    }

    /**
     * Return show url
     * @return string
     */
    public function getShowUrlAttribute()
    {
        try {
            return route('guides.show', [$this->subject->exam_board->slug, $this->id]);
        } catch(\Exception $e) {
            return null;
        }
    }

    public function getUrlAttribute()
    {
        return $this->show_url;
    }

    /**
     * Get previous guide by ID
     * @return App\Guide $guide
     */
    public function prev()
    {
        // get previous user id
        $previous_id = self::where('id', '<', $this->id)->where('chapter_id', $this->chapter_id)->max('id');
        return self::find($previous_id);
    }

    /**
     * Get next guide by ID
     * @return App\Guide $guide
     */
    public function next()
    {
        $next_id = self::where('id', '>', $this->id)->where('chapter_id', $this->chapter_id)->min('id');
        return self::find($next_id);
    }

    /**
     * Get guides from same subject (max 5)
     * @param int $take Number of guides to retrieve
     * @return Collection
     */
    public function similarBySubject($take = 5)
    {
        return self::where('id', '!=', $this->id)->where('subject_id', $this->subject->id)->inRandomOrder()->take($take)->get();
    }

    /**
     * Get guides from same chapter (max 5)
     * @param int $take Number of guides to retrieve
     * @return Collection
     */
    public function similarByChapter($take = 5)
    {
        return self::where('id', '!=', $this->id)->where('chapter_id', $this->chapter->id)->inRandomOrder()->take($take)->get();
    }

    /**
     * Returns true if guide is general (non-chapter-specific)
     * @return boolean
     */
    public function isGeneral()
    {
        return is_null($this->chapter_id);
    }
}
