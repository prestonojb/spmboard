<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\BoardPassSubscription;
use App\CoinProductType;
use Carbon\Carbon;
use App\User;
use Faker\Generator as Faker;

$factory->define(BoardPassSubscription::class, function (Faker $faker) {
    return [
        'user_id' => function() {
            return factory(User::class)->states('student')->create()->id;
        }, // Can be overriden
        'coin_product_type_id' => CoinProductType::first(),
        'end_at' => Carbon::now()->addMonth(),
    ];
});
