@extends('voyager::master')

@section('page_title', 'Topical Past Papers')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_header')
<div class="container-fluid">
    <h1 class="page-title">
        Topical Past Papers
    </h1>
</div>
@stop

@section('content')
    <div class="page-content container-fluid">
        <form class="form-edit-add" role="form"
              action="{{ route('voyager.topical-past-papers.store') }}"
              method="POST" enctype="multipart/form-data" autocomplete="off">
            {{ csrf_field() }}

            <div class="panel panel-bordered">
                <div class="panel">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <div class="panel-body">
                        {{-- Images --}}
                        <div class="form-group">
                            <label for="files">Files</label>
                            <input type="file" multiple class="form-control" name="file" placeholder="Files">
                            @error('file')
                                <div class="error-message">{{ $message }}</div>
                            @enderror
                            <div class="form-group"></div>
                            <p class="gray2">Download Excel template <a target="_blank" href="https://docs.google.com/spreadsheets/d/172TFr9m46zOYKf8ktOBnDsA5LKs8frYN3MFXQTmKuDw/edit?usp=sharing">here</a><br>
                                Refer to the reference IDs to fill into the template accordingly.</p>
                        </div>

                        <div class="form-group">
                            <label for="">References</label>
                            <p class="gray2">
                                <a target="_blank" href="{{ route('subject_chapter_id_reference') }}">Subject Chapter IDs</a><br>
                                <a target="_blank" href="https://www.loom.com/share/4ebe2931fd9e43ea8bcde77a9488fcb9">Video Tutorial</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>

            <button type="submit" class="btn btn-primary pull-right save">
                {{ __('voyager::generic.save') }}
            </button>
        </form>
    </div>
@stop
