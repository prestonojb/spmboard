@component('mail::message')
# Thank you for your participation in 2020 Math Board Quiz!
## We appreciate your effort in completing the quiz. Here's what's next!

1. Like our Facebook Page to stay updated with the winners of the contest! We will update the results of the contests on both our pages!
<div class="text-center">
    <a class="d-inline-block mr-1 button button-facebook" href="https://facebook.com/spmboardmy">SPM Board</a>
    <a class="d-inline-block button button-facebook" href="https://facebook.com/igcseboardmy">IGCSE Board</a>
</div>


2. Share this contest with your friends!
<div class="text-center">
    <a class="d-inline-block mr-1" href="{{ $facebook_share_url }}">
        <img src="{{ asset('img/essential/rounded_fb.png') }}" alt="" width="50" height="50">
    </a>
    <a class="d-inline-block mr-1" href="{{ $twitter_share_url }}">
        <img src="{{ asset('img/essential/rounded_twitter.png') }}" alt="" width="50" height="50">
    </a>
    <a class="d-inline-block mr-1" href="{{ $whatsapp_share_url }}">
        <img src="{{ asset('img/essential/rounded_whatsapp.png') }}" alt="" width="50" height="50">
    </a>
</div>
{{-- <div class="row">
    <span class="d-inline-block mr-2">
        <a class="button button-facebook" href="{{ $facebook_share_url }}">Facebook</a>
    </span>
    <span class="d-inline-block mr-2">
        <a class="button button-twitter" href="{{ $twitter_share_url }}">Twitter</a>
    </span>
    <span class="d-inline-block">
        <a class="button button-whatsapp" href="{{ $whatsapp_share_url }}">Whatsapp</a>
    </span>
</div> --}}

@if(!$is_existing_user)
3. Start asking questions on Board!<br>
**Ask questions, get answered, get rewarded.*
@component('mail::button', ['url' => $register_url, 'color' => 'primary'])
Join for Free
@endcomponent
@else
3. Log in on Board and start collecting Reward Points(RP) now!<br>
**You can spend Reward Points on getting help for your homework, redeem coins for resources and many more!*
@component('mail::button', ['url' => $login_url, 'color' => 'primary'])
Log In
@endcomponent
@endif

We'll be sure to update you for the next quiz! Till next time ;)<br>

Regards,<br>
The Board Team
@endcomponent
