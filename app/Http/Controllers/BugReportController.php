<?php

namespace App\Http\Controllers;

use App\BugReport;
use App\Notifications\BugReported;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;

class BugReportController extends Controller
{
    public $admin_email;

    public function __construct()
    {
        $this->admin_email = setting('admin.bug_report_to_email');
    }

    /**
     * Store bug report
     * AJAX
     */
    public function store()
    {
        // Validation
        request()->validate([
            'description' => 'required|max:200',
            'tag' => 'nullable',
            'img' => 'nullable',
            'email' => 'nullable|email',
        ]);

        // Store Bug Report
        $bug_report = BugReport::create([
                            'tag' => request('tag'),
                            'description' => request('description'),
                            'email' => request('email'),
                        ]);

        // Save image
        if(request()->has('img') && !is_null(request('img'))) {
            $bug_report->saveImage(request('img'));
        }

        // Send notification to admin email
        Notification::route('mail', $this->admin_email)->notify(new BugReported($bug_report));
        
        return 'OK';
    }
}
