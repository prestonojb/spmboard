@extends('main.layouts.app')

@section('meta')
<title>{{ $blog->title }} | {{ config('app.name') }}</title>
<meta name="description" content="{{ $blog->description }}">

<meta property="og:image" content="{{ $blog->img }}" />
<meta property="og:type" content="article" />
@endsection

@section('content')
<div class="py-2"></div>

<div class="container">
    <div class="maxw-740px mx-auto">

        <div class="py-1"></div>
        {{-- Back Button --}}
        <a href="{{ route('blog.index') }}" class="d-inline-block gray2 underline-on-hover mb-2">< Back to Blogs</a>

        <div class="panel">
            <img class="blog__cover-img" src="{{ $blog->img }}" alt="" class="cover-img mb-2">

            <div class="blog-content">

                <div class="mb-3">
                    <h1 class="font-size11"><b>{{ $blog->title }}</b></h1>

                    <div class="py-1"></div>

                    {{-- Share Buttons --}}
                    <div class="shareIcons"></div>
                </div>
            </div>

            {{-- Author --}}
            <div class="d-flex justify-content-start align-items-start border-top border-bottom py-3">
                <a href="{{ $blog->blog_author->show_url }}">
                    <img src="{{ $blog->blog_author->avatar }}" class="avatar mr-2" alt="Author Avatar">
                </a>
                <div>
                    <h4 class="font-size3 font-weight500 mb-1">
                        <a href="javascript:void(0)" class="font-size-inherit color-inherit">
                            {{ $blog->blog_author->name }}
                        </a>
                    </h4>
                    <p class="gray2 font-size3 mb-0">
                        {{ $blog->updated_at->toFormattedDateString() }}
                    </p>
                </div>
            </div>


            <div class="py-2"></div>

            {{-- Body --}}
            <article class="tinymce-editor-contents">
                {!! $blog->body !!} <br>
            </article>

            {{-- Published By --}}
            <div class="border-top pt-2">
                {{-- Share Buttons --}}
                <div class="shareIcons"></div>

                <div class="py-2"></div>

                <p class="gray2 text-uppercase font-size3">Written By</p>

                <div class="d-flex justify-content-start align-items-start">
                    <a href="javascript:void(0)">
                        <img src="{{ $blog->blog_author->img }}" class="avatar-lg mr-1" alt="Author Avatar">
                    </a>
                    <div>
                        <h4 class="font-size5 font-weight500">
                            <a href="javascript:void(0)" class="font-size-inherit color-inherit">
                                {{ $blog->blog_author->name }}
                            </a>
                        </h4>

                        @if(!is_null($blog->blog_author->description))
                            <p class="font-size3 gray2">{!! nl2br($blog->blog_author->description) !!}</p>
                        @endif
                    </div>
                </div>
            </div>

            <div class="py-1"></div>
        </div>

        <div class="py-4"></div>

        @if($recent_blogs->count())
            <div class="recommended-articles mb-1">
                <h6 class="font-size5 border-bottom pb-2 mb-2"><b>Recent Blogs</b></h6>
                <div class="row">
                    @foreach($recent_blogs as $blog)
                        <div class="col-12 col-lg-4 mb-2">
                            @include('blog.common.block')
                        </div>
                    @endforeach
                </div>
            </div>
        @endif

        @if($related_blogs->count())
            <div class="recommended-articles mb-1">
                <h6 class="font-size5 border-bottom pb-2 mb-2"><b>Related Blogs</b></h6>
                <div class="row">
                    @foreach($recent_blogs as $blog)
                        <div class="col-12 col-lg-4 mb-2">
                            @include('blog.common.block')
                        </div>
                    @endforeach
                </div>
            </div>
        @endif
</div>
@endsection
