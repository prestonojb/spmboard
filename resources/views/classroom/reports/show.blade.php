@extends('classroom.layouts.app')

@section('meta')
    <title>Report #{{ $report->id }} | {{ config('app.name') }} Classroom</title>
@endsection

@section('content')

{{-- Breadcrumbs --}}
@component('classroom.common.breadcrumbs')
    <a href="{{ route('classroom.landing') }}">Home</a> /
    <a href="{{ route('classroom.reports.index') }}">Reports</a> /
    {{ $report->id }}
@endcomponent

{{-- Page Heading --}}
@component('classroom.common.page_heading', ['title' => $report->title, 'subtitle' => $report->subtitle])
    @slot('button_group')
        <form class="mr-1" action="{{ route('classroom.reports.download_pdf', $report->id) }}" method="post">
            @csrf
            <button class="button--primary--inverse" type="submit" @cannot('download_pdf', $report) disabled @endcannot><i class="fas fa-arrow-down"></i> Download PDF</button>
        </form>
        @can('delete', $report)
            <form action="{{ route('classroom.reports.delete', $report->id) }}" method="post">
                @csrf
                @method('DELETE')
                <button type="button" class="button--danger deleteButton"><span class="iconify" data-icon="emojione-monotone:cross-mark" data-inline="false"></span> Delete</button>
            </form>
        @endcan
    @endslot
@endcomponent


@php
    $data = [
        'Teacher Remark' => $report->teacher_remark ?? 'N/A',
        'Prepared At' => $report->created_at->toDayDateTimeString(),
        'Updated At' => $report->updated_at->toDayDateTimeString(),
    ];
@endphp
@component('classroom.common.details', ['data' => $data])
@endcomponent

{{-- Lessons --}}
@component('classroom.common.page_subheading', ['title' => 'Lessons'])
@endcomponent

@foreach($report->lesson_items as $item)
    @component('classroom.reports.common.lesson_item', ['report' => $report, 'item' => $item])
    @endcomponent
@endforeach

<div class="py-2"></div>
@endsection
