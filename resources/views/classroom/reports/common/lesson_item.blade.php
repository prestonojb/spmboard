<div class="mb-3">
    <div class="panel position-relative">
        <a class="stretched-link" data-toggle="collapse" href="#lesson_item_{{ $item->id }}" role="button"></a>
        <h3 class="font-weight500 mb-1">{{ $item->lesson->primary }}</h3>
        <p class="mb-0 gray2 font-size1">{!! $item->lesson->secondary !!}</p>
    </div>

    <div class="collapse" id="lesson_item_{{ $item->id }}">
        <div class="pl-5 mt-3 border-left border-blue5 border-width-3px">
            <h4 class="mb-1 font-size4"><b>Lesson Summary</b></h4>
            <p class="gray2 mb-3 font-size1">*This summary is auto-generated and the fields will remain updated even after this report is already generated.</p>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th scope="col" class="border-right">Type</th>
                        <th scope="col">Title</th>
                        <th scope="col">Metric</th>
                    </tr>
                </thead>
                <tbody>
                    @if( $item->lesson->resource_post_count || $item->lesson->homework_count )
                        @if( $item->lesson->resource_post_count )
                            @foreach( $item->lesson->resource_posts as $post )
                                <tr class="item">
                                    @if($loop->first)
                                        <td rowspan="{{ $item->lesson->resource_post_count }}">Resource Posts</td>
                                    @endif
                                    <td>{{ $post->title }}</td>
                                    <td>{{ $post->metric( $report->student ) ?? 'N/A' }}</td>
                                </tr>
                            @endforeach
                        @endif

                        @if( $item->lesson->homework_count )
                            @foreach( $item->lesson->homeworks as $homework )
                                <tr class="item">
                                    @if($loop->first)
                                        <td rowspan="{{ $item->lesson->homework_count }}">Homework</td>
                                    @endif
                                    <td>{{ $homework->title }}</td>
                                    <td>
                                        @if($homework->getStudentSubmission( $report->student ) && $homework->getStudentSubmission( $report->student )->marks)
                                            {{ $homework->getStudentSubmission( $report->student )->marks.'/'.$homework->max_marks ?? 'N/A' }}
                                        @else
                                            N/A
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                    @else
                        <tr>
                            <td colspan="3">
                                <div class="text-center">No data found</div>
                            </td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
</div>
