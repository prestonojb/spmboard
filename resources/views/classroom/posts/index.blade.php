@extends('classroom.layouts.app')

@section('meta')
    <title>{{ $classroom->name }} - Posts | {{ config('app.name') }}</title>
@endsection


@section('content')

@component('classroom.common.breadcrumbs')
    <a href="{{ route('classroom.classrooms.show', $classroom->id) }}">Dashboard</a> /
    Discussion
@endcomponent

@component('classroom.common.page_heading', ['title' => 'Discussion'])
    @can('store_post', $classroom)
        @slot('button_group')
            <a href="{{ route('classroom.posts.create', $classroom->id) }}" class="button--primary sm"><i class="fas fa-plus"></i> Add Post</a>
        @endslot
    @endcan
@endcomponent


<div class="maxw-740px mx-auto">
    @if( count($posts ?? []) )
        @foreach($posts as $post)
            @component('classroom.posts.common.block', ['classroom' => $classroom, 'post' => $post])
            @endcomponent
        @endforeach
        {{ $posts->links() }}
    @else
        <div class="text-center my-4">
            <img src="{{ asset('img/essential/empty.svg') }}" alt="No Post" width="350" height="350" class="mb-2">
            <h4 class="font-size4">No Posts</h4>
        </div>
    @endif
</div>

<div class="py-2"></div>
@endsection

