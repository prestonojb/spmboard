<?php

namespace App\Http\Controllers;

use App\Lesson;
use App\Notifications\Classroom\ReportPrepared;
use App\Student;
use App\StudentReport;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use PDF;

class ClassroomStudentReportController extends Controller
{
    /**
     * List all student reports
     */
    public function index()
    {
        $this->authorize('index', StudentReport::class);
        $user = auth()->user();

        // Get reports and students
        if( $user->is_teacher() ) {
            // Teacher
            $teacher = $user->teacher;
            $reports = StudentReport::where('teacher_id', $teacher->user_id);
            $students = $teacher->students;
        } elseif( $user->is_parent() ) {
            // Parent
            $parent = $user->parent;
            $reports = StudentReport::whereIn( 'student_id', $parent->children_ids );
            $students = $parent->children;
        }

        // Student Selected
        if( request()->student ) {
            $reports = $reports->where('student_id', request()->student);
        }

        // Prepared days ago
        if( request()->prepared_days_ago ) {
            $days_ago = request()->prepared_days_ago;
            $from = Carbon::today()->subDays( $days_ago );
            $reports = $reports->where('created_at', '>', $from );
        }

        $reports = $reports->get();

        return view('classroom.reports.index', compact('reports', 'students'));
    }

    /**
     * Create Student Report
     */
    public function create()
    {
        $this->authorize('store', StudentReport::class);

        $teacher = auth()->user()->teacher;
        $students = $teacher->students;

        return view('classroom.reports.create', compact('teacher', 'students'));
    }

    /**
     * Store Student Report
     */
    public function store()
    {
        $this->authorize('store', StudentReport::class);
        request()->validate([
            'student' => 'required',
            'start_date' => 'required',
            'end_date' => 'required',
        ]);

        $user = auth()->user();
        $student = Student::find( request()->student );

        // Create Report
        $report = StudentReport::create([
            'student_id' => request('student'),
            'teacher_id' => $user->teacher->user_id,
            'teacher_remark' => request('teacher_remark'),
            'start_date' => Carbon::createFromFormat( 'd/m/Y', request('start_date') ),
            'end_date' => Carbon::createFromFormat( 'd/m/Y', request('end_date') ),
        ]);

        // Decode Report Item JSON
        $report_items = json_decode( request('report_items') );
        
        // Add report items if any
        if( !is_null($report_items) ) {
            $report->addItems( $report_items );
        }

        // Generate invoice PDF and send to parent
        if( $student->has_parent() ) {
            $student->parent->notify( new ReportPrepared( $report, $report->getPdf() ) );
        }

        Session::flash('success', 'Report prepared successfully!');
        return redirect()->route('classroom.reports.show', $report->id);
    }

    /**
     * Show Student Report
     */
    public function show( StudentReport $report )
    {
        $this->authorize('show', $report);
        return view('classroom.reports.show', compact('report'));
    }

    /**
     * Delete Student Report
     */
    public function delete( StudentReport $report )
    {
        $this->authorize('delete', $report);

        // Delete report and related models
        $report->items()->delete();
        $report->delete();

        return redirect()->route('classroom.reports.index')->with('error', 'Report deleted successfully!');
    }

    public function download_pdf( StudentReport $report )
    {
        $this->authorize('download_pdf', $report);
        $data = [
            'report' => $report
        ];
        $pdf = PDF::loadView('classroom.pdf.student_report', $data);
        return $pdf->download('report_'.$report->id.'.pdf');
    }
}
