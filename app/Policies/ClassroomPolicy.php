<?php

namespace App\Policies;

use App\User;
use App\Classroom;
use Illuminate\Auth\Access\HandlesAuthorization;

class ClassroomPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Permission to view the classroom list
     */
    public function index(User $user)
    {
        return $user->teacher || $user->student;
    }

    /**
     * Permission to create classroom
     */
    public function create(User $user)
    {
        return $user->is_teacher() && $user->teacher->hasExtraClassroom();
    }

    /**
     * Permission to store classroom
     */
    public function store(User $user)
    {
        return $user->is_teacher() && $user->teacher->hasExtraClassroom();
    }

    /**
     * Permission to view classroom
     */
    public function show(User $user, Classroom $classroom)
    {
        if( $user->is_teacher() ) {
            return $user->id == $classroom->teacher_id;
        }
        if( $user->is_student() ) {
            return $classroom->students->contains( $user->id );
        }
        return false;
    }

    /**
     * Permission to update classroom
     */
    public function update(User $user, Classroom $classroom)
    {
        return $user->is_teacher() && $classroom->hasTeacher($user->teacher);
    }

    /**
     * Permission to upload custom file in classroom
     */
    public function custom_file_upload(User $user)
    {
        return $user->is_teacher();
    }

    /**
     * Permission to join classroom
     */
    public function join(User $user)
    {
        return $user->is_student();
    }

    /**
     * Permission to view classroom analytics
     */
    public function view_analytics(User $user, Classroom $classroom)
    {
        return $user->is_teacher() && $classroom->hasTeacher($user->teacher);
    }

    /**
     * Permission to add student into classroom
     */
    public function add_student(User $user, Classroom $classroom)
    {
        return $user->is_teacher() && $classroom->hasTeacher($user->teacher) && $user->teacher->hasExtraStudentSlot($classroom);
    }

    /**
     * Permission to remove student from classroom
     */
    public function remove_student(User $user, Classroom $classroom)
    {
        return $user->is_teacher() && $classroom->hasTeacher($user->teacher);
    }

    /**
     * Permission to view students in classroom
     */
    public function view_students(User $user, Classroom $classroom)
    {
        return $user->is_teacher() && $classroom->hasTeacher($user->teacher);
    }

    /**
     * Permission to view calendar
     */
    public function view_calendar(User $user)
    {
        return $user->is_teacher() || $user->is_student();
    }

    /**
     * Permission to view classroom billing
     */
    public function view_classroom_billing(User $user)
    {
        return $user->is_teacher();
    }

    /**
     * Permission to view students in classroom
     */
    public function view_student(User $user, Classroom $classroom)
    {
        return $user->is_teacher() && $classroom->hasTeacher($user->teacher);
    }

    /**
     * Permission to view posts in classroom
     */
    public function index_post(User $user, Classroom $classroom)
    {
        if( $user->is_teacher() ) {
            return $user->id == $classroom->teacher_id;
        }
        if( $user->is_student() ) {
            return $classroom->students->contains( $user->id );
        }
        return false;
    }

    /**
     * Permission to store post in classroom
     */
    public function store_post(User $user, Classroom $classroom)
    {
        return $user->teacher;
    }

    /**
     * Permission to create lesson in classroom
     */
    public function create_lesson(User $user, Classroom $classroom)
    {
        return $user->is_teacher() && $classroom->hasTeacher($user->teacher) && $user->teacher->hasExtraLesson($classroom);
    }
}
