<?php

use App\BlogCategory;
use Illuminate\Database\Seeder;

class BlogCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $blog_categories = array(
            0 => array('id' => '1', 'name' => 'Post-SPM', 'slug' => 'post-spm'),
            1 => array('id' => '2', 'name' => 'Self-Improvement', 'slug' => 'self-improvement'),
            2 => array('id' => '3', 'name' => 'Academics', 'slug' => 'academics'),
            3 => array('id' => '4', 'name' => 'About Us', 'slug' => 'about-us'),
        );

        foreach ($blog_categories as $value) {
            $data = array(
                'id' => $value['id'],
                'name' => $value['name'],
                'slug' => $value['slug'],
            );

            $data_exists = BlogCategory::find($value['id']);
            if (!is_null($data_exists)) {
                $data['updated_at'] = date("Y-m-d H:i:s");
                BlogCategory::where('id', $value['id'])
                        ->update($data); //UPDATE
            } else {
                $data['created_at'] = date("Y-m-d H:i:s");
                $data['updated_at'] = date("Y-m-d H:i:s");
                BlogCategory::create($data); //CREATE
            }
        }
    }
}
