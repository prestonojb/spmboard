<?php

namespace App\Policies;

use App\PastPaper;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class PastPaperPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Edit past paper
     */
    public function edit(?User $user, PastPaper $past_paper)
    {
        if(!is_null($user)) {
            return $user->isAdmin();
        }
        return false;
    }

    /**
     * Delete past paper
     */
    public function delete(?User $user, PastPaper $past_paper)
    {
        if(!is_null($user)) {
            return $user->isAdmin();
        }
        return false;
    }
}
