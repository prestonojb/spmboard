@extends('classroom.layouts.app')

@section('meta')
    <title>Subscription | {{ config('app.name') }} Classroom</title>
@endsection

@section('content')
@component('classroom.common.page_heading', ['title' => 'Subscription'])
@endcomponent


@if( auth()->user()->is_teacher() )
    @component('classroom.navs.teacher.billing')
    @endcomponent

    @include('classroom.subscriptions.main.teacher')
@endif

@endsection

@section('scripts')
@endsection
