<?php

namespace App\Policies;

use App\Lesson;
use App\User;
use Carbon\Carbon;
use Illuminate\Auth\Access\HandlesAuthorization;

class LessonPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function show(User $user, Lesson $lesson)
    {
        if( $user->teacher ) {
            return $user->id == $lesson->classroom->teacher_id;
        } elseif( $user->student ) {
            return !Carbon::parse($lesson->scheduled_at)->isPast();
        }
    }

    /**
     * Permission to update lesson
     */
    public function update(User $user, Lesson $lesson)
    {
        return $user->is_teacher() && $lesson->classroom->hasTeacher($user->teacher);
    }

    /**
     * Permission to delete lesson
     */
    public function delete(User $user, Lesson $lesson)
    {
        return $user->is_teacher() && $lesson->classroom->hasTeacher($user->teacher);
    }
}
