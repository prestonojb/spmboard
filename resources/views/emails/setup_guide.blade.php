@component('mail::message')
# Setup Guide
## We tried to setup your Board account but noticed you already have an existing account on Board.

@if( $account_type == $user->account_type )
You can continue logging in on Board with your existing credentials.

@component('mail::button', ['url' => url('login'), 'color' => 'primary'])
Log In Now
@endcomponent

@else

You may need to switch from your existing {{ $user->account_type }} account to a new {{ $account_type }} account if you wish to continue using the same email for your Board account.

@component('mail::button', ['url' => url('settings/account'), 'color' => 'primary'])
Switch Account Type
@endcomponent

@endif


Happy learning!


Regards,<br>
The Board Team


**This email is auto-generated. If you need support, please email to hello@boardedu.org.*

@endcomponent
