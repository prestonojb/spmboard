<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateResourcePostRatingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('resource_post_ratings', function (Blueprint $table) {
            $table->unsignedInteger('resource_post_id');
            $table->unsignedInteger('student_id');
            $table->unsignedInteger('rating');

            $table->timestamps();

            $table->primary(['resource_post_id', 'student_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('resource_post_ratings');
    }
}
