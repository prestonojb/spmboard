<?php

use App\StudentReport;
use App\StudentReportItem;
use App\Teacher;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class StudentReportsTableSeeder extends Seeder
{
    protected $NO_OF_REPORTS = 10;
    protected $MIN_NO_OF_REPORT_ITEMS = 1;
    protected $MAX_NO_OF_REPORT_ITEMS = 4;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        StudentReport::truncate();
        StudentReportItem::truncate();
        $faker = \Faker\Factory::create();

        for($i = 0; $i < $this->NO_OF_REPORTS; $i++) {

            // Get random teacher, and a random student from the teacher
            $teacher = Teacher::inRandomOrder()->first();
            $student = $teacher->students->random();

            $lesson_ids = $teacher->getUnreportedLessonIds( $student );

            // Stop generating reports if all lessons are reported
            if( count($lesson_ids ?? []) ) {
                // Create report and report items
                $start_date = Carbon::createFromTimeStamp($faker->dateTimeBetween('-1 years', 'now')->getTimestamp());
                $end_date = $start_date->copy()->addDays( rand(7, 90) );

                // Create report
                $report = StudentReport::create([
                            'teacher_id' => $teacher->user_id,
                            'student_id' => $student->user_id,
                            'teacher_remark' => $faker->optional()->paragraph,
                            'start_date' => $start_date->format('Y-m-d'),
                            'end_date' => $end_date->format('Y-m-d'),
                        ]);

                // Generate report items and attach to report
                $no_of_lessons = rand($this->MIN_NO_OF_REPORT_ITEMS, $this->MAX_NO_OF_REPORT_ITEMS);
                for( $i=0; $i < $no_of_lessons; $i++ ) {
                    $lesson_id = $lesson_ids[ array_rand($lesson_ids) ];
                    $report->items()->create([
                        'lesson_id' => $lesson_id, // Pop last value from lesson IDs array
                        'description' => $faker->optional()->paragraph,
                    ]);
                }
            }
        }
    }
}
