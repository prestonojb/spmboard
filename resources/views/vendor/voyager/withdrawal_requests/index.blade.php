@extends('voyager::master')

@section('page_title', 'Withdrawal Request')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_header')
    <h1>Withdrawal Request</h1>
@endsection

@section('content')

@component('vendor.voyager.withdrawal_requests.common.table', ['withdrawal_requests' => $withdrawal_requests, 'is_admin_view' => true])
@endcomponent

@stop
