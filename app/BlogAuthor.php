<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Blog;
use Illuminate\Support\Facades\Storage;

class BlogAuthor extends Model
{
    //Set identifier for the model
    public function getRouteKeyName()
    {
        return 'slug';
    }
    public function blog()
    {
        return $this->hasMany(Blog::class);
    }

    public function setNameAttribute($value)
    {
        $this->attributes['name'] = $value;
        $this->attributes['slug'] = str_slug($value);
    }   

    public function getImgAttribute($value)
    {        
        if($value){
            if(strpos($value, 'amazonaws')){
                //Amazon link for image
                return $value;
            } else{
                //Amazon S3
                $updated_value = str_replace('\\', '/', $value);
                return Storage::cloud()->url($updated_value);
            }
        } else {
            return null;
        }
    }
}
