<?php

namespace App\Http\Controllers;

use App\Question;
use App\ExamBoard;
use App\Http\Requests\Question\StoreRequest;
use App\Http\Requests\Question\UpdateRequest;
use App\Notifications\QuestionAsked;
use App\Notifications\QuestionRecommended;
use App\RewardPointAction;
use Illuminate\Support\Facades\Storage;
use Amplitude;

class QuestionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['index', 'show']);
    }

    public $valid_sorts = array('hot', 'answered');

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(ExamBoard $exam_board)
    {
        $sort = request('sort');
        if(!is_null($sort) && !in_array($sort, Question::$valid_sorts)) {
            $sort = 'hot';
        }

        $questions = Question::sortByExamBoard($exam_board);
        return view('forum.questions.index', compact('exam_board', 'questions', 'sort'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(ExamBoard $exam_board)
    {
        $this->authorize('create', Question::class);
        $subjects = $exam_board->subjects;

        return view('forum.questions.create', compact('exam_board', 'subjects'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        $this->authorize('create', Question::class);
        $user = auth()->user();

        if (auth()->user()->reward_point_balance < request()->reward_points) {
            return redirect()->back()->withInput(request()->input())->with('error', 'Insufficient reward point balance!');
        }

        //Create new question
        $question = Question::create([
            'user_id' => $user->id,
            'question' => $request->question,
            'description' => $request->description,
            'subject_id' => $request->subject,
            'reward_points' => $request->reward_points,
            'flagged_as' => $request->flagged_as,
            'anonymous' => $request->has('anonymous'),
        ]);

        if($request->has('img') && !empty(request()->file('img')) ){
            // Save Image to DB and S3
            $question->saveImage(request()->file('img'));
        }

        // Save question chapter relationship in question_chapter table
        $question->chapters()->attach($request->chapter);

        // Log reward point action
        if($question->reward_points != 0) {
            RewardPointAction::create([
                'user_id' => $user->id,
                'name' => 'Ask question',
                'amount' => -($question->reward_points),
                'reward_point_actionable_id' => $question->id,
                'reward_point_actionable_type' => Question::class
            ]);

            $user->deductRewardPoints($question->reward_points);
        }

        $user->notify(new QuestionAsked($question));

        // Log event on Amplitude
        Amplitude::setUserId($user->id);
        Amplitude::queueEvent('ask_question', ['question_id' => $question->id]);

        return redirect('/'.$question->subject->exam_board->slug.'/questions/'.$question->id.'/'.$question->slug)->withSuccess('Question asked successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(ExamBoard $exam_board, $id, $slug=null)
    {
        // Redirect to question URL with slug if only ID is provided
        if ( is_null($slug) ) {
            if( Question::where('id', $id)->exists() ) {
                $slug = Question::find($id)->slug;
                return redirect()->route('questions.show', [$exam_board->slug, $id, $slug]);
            } else {
                abort(404);
            }
        }

        $question = Question::find($id);
        if (!$question) {
            abort(404);
        }
        // Record views
        views($question)->record();

        //Get sorted answers to question
        $answers = $question->sortedAnswers();

        return view('forum.questions.show', ['question' => $question, 'answers' => $answers]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(ExamBoard $exam_board, Question $question)
    {
        $this->authorize('edit', $question);

        $subjects = $exam_board->subjects;
        return view('forum.questions.edit', compact('exam_board', 'question', 'subjects'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, ExamBoard $exam_board, Question $question)
    {
        $this->authorize('edit', $question);

        $question->update([
            'question' => $request->question,
            'description' => $request->description,
            'subject_id' => $request->subject,
            'flagged_as' => $request->flagged_as,
        ]);

        $question->save();

        //Detach and attach new relationship between question and chapters
        $question->chapters()->detach();
        $question->chapters()->attach($request->chapter);

        return redirect('/'.$question->subject->exam_board->slug.'/questions/'.$question->id.'/'.$question->slug)->withSuccess('Question updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(ExamBoard $exam_board, Question $question)
    {
        $this->authorize('delete', $question);

        if($question->img){
            if(strpos($question->img, 'amazonaws.com')){
                //Delete file from S3
                $img_dir = substr($question->img, strpos($question->img, 'questions/'));
                Storage::cloud()->delete($img_dir);

            } else {
                //Delete file from local
                $img_path = storage_path('app/public/questions/'. $question->img);
                unlink($img_path);
            }
        }

        $answers = $question->answers;

        foreach($answers as $answer){

            if($answer->img){
                if(strpos($answer->img, 'amazonaws.com')){
                    //Delete file from S3
                    $img_dir = substr($answer->img, strpos($answer->img, 'answers/'));
                    Storage::cloud()->delete($img_dir);
                } else {
                    //Delete file from local
                    $img_path = storage_path('app/public/answers/'. $answer->img);
                    unlink($img_path);
                }
            }

        }

        $question->delete();

        return redirect()->route('questions.index', $exam_board)->withError('Question deleted');

    }

    public function recommend(Question $question)
    {
        $this->authorize('recommend', $question);

        // Moderator
        $mod = auth()->user();

        if(!$question->recommended) {
            // Only reward users RP for question recommended for the first time
            if ( is_null($question->recommended_by_user_id) && is_null($question->recommended_at) ) {
                $reward_amount = 10;

                $question->user->addRewardPoints($reward_amount);

                RewardPointAction::create([
                    'user_id' => $question->user->id,
                    'name' => 'Recommended Question',
                    'reward_point_actionable_type' => Question::class,
                    'reward_point_actionable_id' => $question->id,
                    'amount' => $reward_amount,
                ]);

                $question->recommended_by_user_id = $mod->id;
                $question->recommended_at = now();

                $question->save();

                // Notify user about his/her RP!
                $question->user->notify(new QuestionRecommended($question));

            } else {
                $question->recommended_by_user_id = $mod->id;
                $question->recommended_at = now();

                $question->save();
            }

            return 'recommended';
        } else {
            $question->recommended_by_user_id = $mod->id;
            $question->recommended_at = null;

            $question->save();

            return 'unrecommended';
        }
    }
}
