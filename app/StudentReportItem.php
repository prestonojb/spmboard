<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudentReportItem extends Model
{
    protected $guarded = [];

    public function report()
    {
        return $this->belongsTo(StudentReport::class);
    }

    public function lesson()
    {
        return $this->belongsTo(Lesson::class);
    }
}
