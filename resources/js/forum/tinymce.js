// Reference: https://www.tiny.cloud/docs/plugins/image/
function image_upload_handler (blobInfo, success, failure, progress) {
    var xhr, formData;

    xhr = new XMLHttpRequest();
    xhr.withCredentials = false;
    xhr.open('POST', '/editor/upload');

    xhr.upload.onprogress = function (e) {
    progress(e.loaded / e.total * 100);
    };

    xhr.onload = function() {
    var json;

    if (xhr.status === 403) {
        failure('HTTP Error: ' + xhr.status, { remove: true });
        return;
    }

    if (xhr.status < 200 || xhr.status >= 300) {
        failure('HTTP Error: ' + xhr.status);
        return;
    }

    json = JSON.parse(xhr.responseText);

    if (!json || typeof json.location != 'string') {
        failure('Invalid JSON: ' + xhr.responseText);
        return;
    }

    success(json.location);
    };

    xhr.onerror = function () {
    failure('Image upload failed due to a XHR Transport error. Code: ' + xhr.status);
    };

    formData = new FormData();
    formData.append('file', blobInfo.blob(), blobInfo.filename());

    xhr.send(formData);
};

// Configs
var plugins = [
    'advlist autoresize autolink charmap lists link image charmap print preview anchor',
    'searchreplace visualblocks code fullscreen hr',
    'insertdatetime media table paste code wordcount'
];
var questionToolbar = 'bold italic underline | superscript subscript charmap | table link | bullist numlist removeformat';
var answerToolbar = 'formatselect | bold italic underline | superscript subscript charmap | table link image | bullist numlist hr removeformat';
var resourceToolbar = 'bold italic underline | superscript subscript charmap | link | bullist numlist removeformat';
var articleToolbar = 'formatselect | bold italic underline | superscript subscript charmap | table link image | bullist numlist hr removeformat';

if (typeof tinymce !== 'undefined') {
    tinymce.init({
        selector: 'textarea.question-tinymce-editor',
        height: 500,
        block_formats: 'Paragraph=p; Wrapper=wrapper;',
        object_resizing : 'td, tr, th',
        body_class: 'tinymce-editor-contents',
        content_css: '/css/app.css',
        content_style: "@import url('https://fonts.googleapis.com/css?family=Poppins:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i'); html,body {background-color: white;}",
        menubar: false,
        statusbar: false,
        default_link_target: '_blank',
        link_default_protocol: 'https',
        remove_linebreaks : true,
        setup: function(editor) {
            editor.on('init', function(e) {
                $('.loading-icon').hide();
                var content = editor.getContent();;
                editor.setContent(content);
            });
        },
        max_height: 600,
        table_toolbar: "tableprops tabledelete | tableinsertrowbefore tableinsertrowafter tabledeleterow | tableinsertcolbefore tableinsertcolafter tabledeletecol tabledeletecol",
        plugins: plugins,
        toolbar: questionToolbar,
        images_upload_handler: image_upload_handler,
    });

    tinymce.init({
        selector: 'textarea.answer-tinymce-editor',
        height: 500,
        block_formats: 'Header=h3; Paragraph=p; Wrapper=wrapper;',
        formats: {
            'wrapper': {block: 'div', classes: 'content-wrapper', wrapper: true},
        },
        object_resizing : 'td, tr, th',
        body_class: 'tinymce-editor-contents',
        content_css: '/css/app.css',
        content_style: "@import url('https://fonts.googleapis.com/css?family=Poppins:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i'); html,body {background-color: white;}",
        menubar: false,
        statusbar: false,
        default_link_target: '_blank',
        link_default_protocol: 'https',
        remove_linebreaks : true,
        setup: function(editor) {
            editor.on('init', function(e) {
                $('.loading-icon').hide();
                var content = editor.getContent();;
                editor.setContent(content);
            });
        },
        max_height: 600,
        table_toolbar: "tableprops tabledelete | tableinsertrowbefore tableinsertrowafter tabledeleterow | tableinsertcolbefore tableinsertcolafter tabledeletecol tabledeletecol",
        plugins: plugins,
        toolbar: answerToolbar,
        images_upload_handler: image_upload_handler,
    });

    tinymce.init({
        selector: 'textarea.article-tinymce-editor',
        height: 500,
        block_formats: 'Header 2=h2; Header 3=h3; Header 4=h4; Paragraph=p;',
        formats: {
            'wrapper': {block: 'div', classes: 'content-wrapper', wrapper: true},
        },
        object_resizing : 'td, tr, th',
        body_class: 'tinymce-editor-contents',
        content_css: '/css/app.css',
        content_style: "@import url('https://fonts.googleapis.com/css?family=Poppins:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i'); html,body {background-color: white;}",
        menubar: false,
        statusbar: false,
        default_link_target: '_blank',
        link_default_protocol: 'https',
        remove_linebreaks : true,
        setup: function(editor) {
            editor.on('init', function(e) {
                $('.loading-icon').hide();
                var content = editor.getContent();;
                editor.setContent(content);
            });
        },
        max_height: 600,
        table_toolbar: "tableprops tabledelete | tableinsertrowbefore tableinsertrowafter tabledeleterow | tableinsertcolbefore tableinsertcolafter tabledeletecol tabledeletecol",
        plugins: plugins,
        toolbar: answerToolbar,
        images_upload_handler: image_upload_handler,
    });

    tinymce.init({
        selector: 'textarea.resource-tinymce-editor',
        height: 500,
        block_formats: 'Paragraph=p;',
        body_class: 'tinymce-editor-contents',
        content_css: '/css/app.css',
        content_style: "@import url('https://fonts.googleapis.com/css?family=Poppins:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i'); html,body {background-color: white;}",
        menubar: false,
        statusbar: false,
        default_link_target: '_blank',
        link_default_protocol: 'https',
        remove_linebreaks : true,
        setup: function(editor) {
            editor.on('init', function(e) {
                $('.loading-icon').hide();
                var content = editor.getContent();;
                editor.setContent(content);
            });
        },
        max_height: 600,
        plugins: plugins,
        toolbar: resourceToolbar,
    });
}
