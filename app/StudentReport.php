<?php

namespace App;

use PDF;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\Model;
use Exception;

class StudentReport extends Model
{
    protected $guarded = [];
    protected $dates = ['start_date', 'end_date', 'created_at', 'updated_at'];

    /**
     * ---------------
     * Relationships
     * ---------------
     */
    public function student()
    {
        return $this->belongsTo(Student::class, 'student_id', 'user_id');
    }

    public function teacher()
    {
        return $this->belongsTo(Teacher::class, 'teacher_id', 'user_id');
    }

    public function items()
    {
        return $this->hasMany(StudentReportItem::class);
    }

    public function lesson_items()
    {
        return $this->items()->whereNotNull('lesson_id')->get();
    }

    public function non_lesson_items()
    {
        return $this->items()->whereNull('lesson_id')->get();
    }

    /**
     * ---------------
     * Accessors
     * ---------------
     */
    public function getLessonItemsAttribute()
    {
        return $this->lesson_items();
    }

    public function getNoOfLessonsAttribute()
    {
        return $this->lesson_items()->count();
    }

    public function getPeriodAttribute()
    {
        return "{$this->start_date->toFormattedDateString()} - {$this->end_date->toFormattedDateString()}";
    }

    public function getTitleAttribute()
    {
        return $this->student->name;
    }

    public function getSubtitleAttribute()
    {
        return "for {$this->period}";
    }

    public function getPrimaryAttribute()
    {
        return "{$this->title} {$this->subtitle}";
    }

    public function getSharedToParentAttribute()
    {
        return !is_null( $this->shared_to_parent_at );
    }

    /**
     * -----------------
     * PDF
     * -----------------
     */

    public function getPdf()
    {
        $data = [ 'report' => $this ];
        return PDF::loadView('classroom.pdf.student_report', $data);
    }

    /**
     * --------------
     * Report Items
     * --------------
     */

    /**
     * Add report items to report
     * @param array $items Report Items Data Array
     */
    public function addItems( $items )
    {
        if( !is_array($items) ) {
            throw new Exception("Passed argument must be of type array!");
        }
        if( count($items) == 0 ) {
            throw new Exception("Array passed must not be empty!");
        }
        // Add report items
        foreach( $items as $item ) {
            $this->items()->create([
                'lesson_id' => $item->lesson_id
            ]);
        }
    }



}
