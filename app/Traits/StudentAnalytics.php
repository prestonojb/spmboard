<?php

namespace App\Traits;
use App\Question;
use App\Subject;
use App\Chapter;
use App\Classroom;
use App\ExamBoard;
use App\HomeworkSubmission;
use App\Homework;
use App\User;
use Carbon\Carbon;
use Carbon\CarbonPeriod;

trait StudentAnalytics
{
    // public function question_asked_data($classroom = null)
    // {
    //     $userId = $this->user_id;
    //     $questions_asked_data = [];
    //     foreach( $classroom->subject->chapters as $chapter ) {
    //         $chapterId = $chapter->id;

    //         //Questions from this chapter
    //         $no_of_questions = Question::where('user_id', $userId )->whereHas('chapters', function($query) use ($chapterId) {
    //             $query->where('chapters.id', '=', $chapterId);
    //         })->count();

    //         $questions_asked_data[ $chapter->name ] = $no_of_questions;
    //     }

    //     return $questions_asked_data;
    // }

    // public function question_answered_data($classroom)
    // {
    //     $userId = $this->user_id;
    //     $questions_answered_data = [];
    //     foreach( $classroom->subject->chapters as $chapter ) {
    //         $chapterId = $chapter->id;

    //         //Questions from this chapter with answers from user
    //         $no_of_questions = Question::whereHas('chapters', function($query) use ($chapterId) {
    //             $query->where('chapters.id', '=', $chapterId);
    //         })->whereHas('answers', function($query) use ($userId){
    //             $query->where('answers.user_id', $userId);
    //         })
    //         ->count();

    //         $questions_answered_data[ $chapter->name ] = $no_of_questions;
    //     }
    //     return $questions_answered_data;
    // }

    // Returns homework data in the form [['Jan 2020' => 80], ['Feb 2020', => 85], ...]
    public function getHomeworkMarksData( Classroom $classroom = null )
    {
        $homework_marks_data = collect();

        $startDt = Carbon::now()->subMonths(6);
        $endDt = Carbon::now();

        $period = CarbonPeriod::create($startDt, '1 month', $endDt);

        foreach ($period as $date) {
            // $month_of_year = 'Jan 2020'
            $month_of_year = $date->copy()->format('M Y');
            $avg_homework_marks = $this->getAverageHomeworkMarksByMonth( $month_of_year, $classroom );

            $homework_marks_data->put(
                $month_of_year, $avg_homework_marks
            );
        }

        return $homework_marks_data->toArray();
    }
}
