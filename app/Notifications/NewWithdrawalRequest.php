<?php

namespace App\Notifications;

use App\WithdrawalRequest;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class NewWithdrawalRequest extends Notification implements ShouldQueue
{
    use Queueable;

    public $withdrawal_request;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(WithdrawalRequest $withdrawal_request)
    {
        $this->withdrawal_request = $withdrawal_request;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $request = $this->withdrawal_request;
        return (new MailMessage)
                    ->greeting('New Withdrawal Request')
                    ->line($request->user->username.' requested for a withdrawal with a total of '. $request->withdrawal_amount_in_coin. ' coins.')
                    ->action('Learn More', route('voyager.withdrawal-requests.index'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
