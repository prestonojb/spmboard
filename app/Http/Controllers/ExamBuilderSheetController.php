<?php

namespace App\Http\Controllers;

use App\Chapter;
use App\ExamBoard;
use App\Subject;
use App\ExamBuilderItem;
use App\ExamBuilderSheet;
use App\Jobs\LoadExamBuilderSheet;
use Carbon\Carbon;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Amplitude;

class ExamBuilderSheetController extends Controller
{
    /**
     * Store Exam Builder Sheet
     * Generates only QP PDF here (for perfomance)
     */
    public function store()
    {
        $this->authorize('generate_qp', ExamBuilderSheet::class);

        // Validation
        request()->validate([
            'item_ids' => 'required',
            'name' => 'required',
        ]);
        $user = auth()->user();
        // Get items
        $item_ids = json_decode( request('item_ids') );
        $items = ExamBuilderItem::find($item_ids);

        // Get values from requests
        $subject = Subject::find( request('subject') );
        $chapters = Chapter::find( json_decode( request('chapters') ) );

        // Check number of items within limit
        if($user->getExamBuilderQuestionLimit() < count($items)) {
            return redirect()->back()->with('error', 'Number of items exceeded limit!');
        }

        // Check permission of user for subchapter access
        if( $user->cannot('generate_with_subchapter', ExamBuilderSheet::class) ) {
            foreach($chapters as $chapter) {
                if($chapter->isSubChapter()) {
                    return redirect()->back()->with('error', 'No permission to generate worksheets with subchapters!');
                }
            }
        }

        // Store Sheet on DB
        $sheet = $user->exam_builder_sheets()->create([
                    'name' => request('name'),
                    'subject_id' => $subject->id,
                    'paper_number' => request('paper_number'),
                    'qp_status' => 'loading',
                ]);
        // Save chapters to sheet
        $sheet->chapters()->attach( $chapters->pluck('id')->toArray() );
        $sheet->items()->attach( $item_ids );

        // Resolve PDF options
        list($show_label, $hide_watermark) = $this->resolvePdfOptions( request()->except(['item_ids', 'name', 'subject', 'chapters']) );

        // ADD QP PDF generation to queue
        LoadExamBuilderSheet::dispatch($sheet->id, true, $show_label, $hide_watermark);

        // Log event on Amplitude
        Amplitude::setUserId($user->id);
        Amplitude::queueEvent('create_exam_builder_qp_sheet', ['exam_builder_sheet_id' => $sheet->id]);

        // Flash toast message
        Session::flash('success', 'Worksheet question paper is under construction!');
        return redirect()->route('exam_builder.sheets.show', $sheet->id);
    }

    public function generateAndStoreMsPdf(ExamBuilderSheet $exam_builder_sheet)
    {
        $this->authorize('generate_ms', ExamBuilderSheet::class);

        $user = auth()->user();
        $sheet = $exam_builder_sheet;

        // Resolve PDF options
        list($show_label, $hide_watermark) = $this->resolvePdfOptions( request()->except(['item_ids', 'name', 'subject', 'chapters']) );

        // Update sheet
        $sheet->update([
            'ms_status' => 'loading',
            'ms_generated_at' => Carbon::now(),
        ]);

        // ADD MS PDF generation to queue
        LoadExamBuilderSheet::dispatch($sheet->id, false, $show_label, $hide_watermark);

        // Log event on Amplitude
        Amplitude::setUserId($user->id);
        Amplitude::queueEvent('create_exam_builder_ms_sheet', ['exam_builder_sheet_id' => $sheet->id]);

        // Flash toast message
        Session::flash('success', 'Worksheet mark scheme is under construction!');
        return redirect()->route('exam_builder.sheets.show_ms',$sheet->id);
    }

    /**
     * Show Exam Builder Sheet QP
     */
    public function show(ExamBuilderSheet $exam_builder_sheet)
    {
        $this->authorize('show', $exam_builder_sheet);

        $is_question = true;
        $sheet = $exam_builder_sheet;
        $subject = $sheet->subject;
        $exam_board = $subject->exam_board;
        return view('exam_builder.sheets.show', compact('sheet', 'exam_board', 'subject', 'is_question'));
    }

    /**
     * Show EB Sheet MS
     */
    public function showMs(ExamBuilderSheet $exam_builder_sheet)
    {
        $this->authorize('show', $exam_builder_sheet);

        $is_question = false;

        $sheet = $exam_builder_sheet;
        $subject = $sheet->subject;
        $exam_board = $subject->exam_board;
        return view('exam_builder.sheets.show', compact('sheet', 'exam_board', 'subject', 'is_question'));
    }

    /**
     * Delete Exam Builder Sheet
     */
    public function delete(ExamBuilderSheet $exam_builder_sheet)
    {
        $this->authorize('delete', $exam_builder_sheet);

        $exam_builder_sheet->deletePdfs();
        $exam_builder_sheet->delete();

        return redirect()->back()->with('error', 'Exam Builder Worksheet deleted successfully!');
    }

    /**
     * Download QP PDF file
     */
    public function downloadQp(ExamBuilderSheet $exam_builder_sheet)
    {
        $this->authorize('download_qp', $exam_builder_sheet);

        $user = auth()->user();
        // Log event on Amplitude
        Amplitude::setUserId($user->id);
        Amplitude::queueEvent('download_exam_builder_qp_sheet', ['exam_builder_sheet_id' => $exam_builder_sheet->id]);

        return Storage::download($exam_builder_sheet->qp_relative_url, "{$exam_builder_sheet->name}.pdf");
    }

    /**
     * Download MS PDF file
     */
    public function downloadMs(ExamBuilderSheet $exam_builder_sheet)
    {
        $this->authorize('download_ms', $exam_builder_sheet);

        $user = auth()->user();
        // Log event on Amplitude
        Amplitude::setUserId($user->id);
        Amplitude::queueEvent('download_exam_builder_ms_sheet', ['exam_builder_sheet_id' => $exam_builder_sheet->id]);

        return Storage::download($exam_builder_sheet->ms_relative_url, "{$exam_builder_sheet->name}_MS.pdf");
    }

    /**
     * Get Exam Board from request
     * @return \App\ExamBoard
     */
    public function getExamBoard()
    {
        return ExamBoard::where('slug', request('exam_board') )->first() ?? ExamBoard::first();
    }

    /**
     * Resolve options of PDF to be generated (applies for both QP and MS)
     * @return array (@boolean Show Label, @boolean Hide Watermark)
     */
    public function resolvePdfOptions()
    {
        $user = auth()->user();
        $hide_watermark = $user->canHideExamBuilderSheetWatermark() && isset(request()->hide_watermark);
        $show_label = isset(request()->show_label);

        return array($show_label, $hide_watermark);
    }
}
