<?php

namespace App\Http\Controllers;

use App\Classroom;
use App\Lesson;
use App\Notifications\Classroom\LessonAdded;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class ClassroomLessonController extends Controller
{
    public function index(Classroom $classroom)
    {
        $this->authorize('show', $classroom);

        $lessons = $classroom->lessons;
        return view('classroom.lessons.index', compact('classroom', 'lessons'));
    }

    public function create(Classroom $classroom)
    {
        $this->authorize('create_lesson', $classroom);
        $is_edit = false;
        $lesson = null;
        return view('classroom.lessons.create_edit', compact('classroom', 'lesson', 'is_edit'));
    }

    /**
     * Create lesson in classroom
     */
    public function store(Classroom $classroom)
    {
        $this->authorize('create_lesson', $classroom);
        request()->validate([
            'name' => 'required',
            'description' => 'nullable',
            'start_at' => 'required',
            'end_at' => 'required',
        ]);

        // Create lesson
        $lesson = Lesson::create([
                    'name' => request()->name,
                    'description' => request()->description,
                    'classroom_id' => $classroom->id,
                    'start_at' => Carbon::createFromFormat('d/m/Y g:i A', request()->start_at), // 15/10/2020 02:00 AM
                    'end_at' => Carbon::createFromFormat('d/m/Y g:i A', request()->end_at),
                ]);

        // Lesson Added Notification
        foreach( $classroom->students as $student ) {
            // Notify student
            $student->notify( new LessonAdded( $student, $lesson ) );
            // Notify student's parent if any
            if( $student->parent ){
                $student->parent->notify( new LessonAdded( $student, $lesson ) );
            }
        }

        Session::flash('success', 'Lesson created successfully!');
        return redirect()->route('classroom.lessons.show', [$classroom->id, $lesson->id]);
    }


    /**
     * Show lesson in classroom
     */
    public function show(Classroom $classroom, Lesson $lesson)
    {
        $this->authorize('show', $classroom);

        $classroom = $lesson->classroom;
        $resource_posts = $lesson->resource_posts;
        $homeworks = $lesson->homeworks;

        return view('classroom.lessons.show', compact('classroom', 'lesson', 'resource_posts', 'homeworks'));
    }

    /**
     * Edit lesson
     */
    public function edit(Classroom $classroom, Lesson $lesson)
    {
        $this->authorize('update', $lesson);
        $is_edit = true;
        return view('classroom.lessons.create_edit', compact('classroom', 'lesson', 'is_edit'));
    }

    /**
     * Update lesson
     */
    public function update(Classroom $classroom, Lesson $lesson)
    {
        $this->authorize('update', $lesson);
        // Validation
        request()->validate([
            'name' => 'required',
            'description' => 'nullable',
        ]);

        // Update lesson
        $lesson->update([
            'name' => request('name'),
            'description' => request('description'),
        ]);

        Session::flash('success', 'Lesson updated successfully!');
        return redirect()->route('classroom.lessons.show', [$classroom->id, $lesson->id]);
    }

    /**
     * Delete lesson
     */
    public function destroy(Classroom $classroom, Lesson $lesson)
    {
        $this->authorize('delete', $lesson);

        $lesson->delete();

        Session::flash('error', 'Lesson deleted successfully!');
        return $this->index( $classroom );
    }
}
