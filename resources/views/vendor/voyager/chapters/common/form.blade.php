@php
    // Declare variables
    if( !is_null($chapter) ) {
        $name = old('name') ?? $chapter->name;
        $description = old('description') ?? $chapter->description;
        $subject_id = old('subject') ?? $chapter->subject_id;
        $parent_chapter_id = old('parent_chapter') ?? $chapter->parent_chapter_id;
    } else {
        $name = old('name');
        $description = old('description');
        $subject_id = old('subject');
        $parent_chapter_id = old('parent_chapter');
    }
    $url = $is_edit ? route('voyager.chapters.update', $chapter->id) : route('voyager.chapters.store');
@endphp

<form class="form-edit-add" role="form"
        action="{{ $url }}"
        method="POST" enctype="multipart/form-data" autocomplete="off">
        {{ csrf_field() }}
    @if($is_edit)
        @method('PATCH')
    @endif

    <div class="panel" style="max-width: 740px; padding: 12px; margin: 0 auto;">
        {{-- Name --}}
        <div class="form-group">
            <label for="">Name</label>
            <input type="text" name="name" class="form-control" placeholder="Name" id="" required value="{{ $name }}">
            @error('name')
                <div class="text-danger">{{ $message }}</div>
            @enderror
        </div>

        {{-- Description --}}
        <div class="form-group">
            <label for="">Description</label>
            <textarea name="description" class="form-control" id="" rows="3" required placeholder="Description">{{ $description }}</textarea>
            @error('description')
                <div class="text-danger">{{ $message }}</div>
            @enderror
        </div>

        {{-- Subject --}}
        <div class="form-group">
            <label>Subject</label>
            <select class="subject-select" name="subject">
                <option data-placeholder="true"></option>
                @foreach($subjects as $subject)
                    <option value="{{ $subject->id }}" {{ $subject_id == $subject->id ? 'selected="selected"' : '' }}>{{ $subject->name_with_exam_board }}</option>
                @endforeach
            </select>
            @error('subject')
                <span class="error-message">{{ $message }}</span>
            @enderror
        </div>

        {{-- Parent Chapter --}}
        <div class="form-group">
            <label>Parent Chapter</label>
            <select class="chapter-select" name="parent_chapter" disabled>
                <option data-placeholder="true" value=""></option>
                @foreach($subjects as $subject)
                    <optgroup label="{{ $subject->name_with_exam_board }}">
                        @foreach($subject->chapters as $parent_chapter)
                            <option value="{{ $parent_chapter->id }}"
                                {{ $parent_chapter_id == $parent_chapter->id ? 'selected="selected"' : '' }}>
                                {{ $parent_chapter->name }}
                            </option>
                        @endforeach
                    </optgroup>
                @endforeach
            </select>
            <p class="gray2">Leave empty if chapter is main chapter.</p>
        </div>

        <div class="form-group"></div>

        <div class="text-right">
            <button type="submit" class="btn btn-primary">
                Save
            </button>
        </div>
    </div>
</form>

<hr>

@if($is_edit && $chapter->hasSubchapters())
    <h2>Subchapters</h2>
        <div id="subchapterList" class="list-group">
            @foreach($chapter->subchapters as $subchapter)
                <div class="list-group-item border-bottom d-flex justify-content-between mb-1" data-chapter-id="{{ $subchapter->id }}">
                    <div>
                        {{ $subchapter->name }}
                    </div>
                    <div>
                        <a class="btn btn-primary" href="{{ route('voyager.chapters.show', $subchapter->id) }}">View</a>
                        <a class="btn btn-warning" href="{{ route('voyager.chapters.edit', $subchapter->id) }}">Edit</a>
                        <form class="d-inline-block" action="{{ route('voyager.chapters.destroy', $subchapter->id) }}" method="post">
                            @csrf
                            @method('DELETE')
                            <button class="btn btn-danger delete" href="">Delete</button>
                        </form>
                    </div>
                </div>
            @endforeach
        </div>
        <form action="{{ route('voyager.chapters.reorder') }}" id="orderSubchapterForm" method="post" class="text-right">
            @csrf
            <input type="hidden" name="chapter_order">
            <button type="submit" class="btn btn-primary">
                Save Order
            </button>
        </form>
@endif
