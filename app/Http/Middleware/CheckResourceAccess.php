<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Session;
use Closure;

class CheckResourceAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if( !auth()->check() ) {
            return $next($request);
        }
        if( auth()->user()->parent ) {
            Session::flash('error', 'You cannot access resources with a parent account!');
            return redirect()->back();
        }
        return $next($request);
    }
}
