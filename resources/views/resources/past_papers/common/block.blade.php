<div class="position-relative pp">
    <div class="row">
        <div class="col-12 col-sm-9">
            <h3 class="h3 resource__title">{{ $paper->name }}</h3>
            @if($exam_board->name == 'SPM')
                <h4 class="pp__description">Year {{ $paper->year }}, Paper {{ $paper->paper_number }}</h4>
            @elseif($exam_board->name == 'IGCSE')
                <h4 class="pp__description">{{ $paper->code }}</h4>
            @endif
            <span class="tag free mb-1">FREE</span>
        </div>

        <div class="col-12 col-sm-3 pp__cta">
            <a class="button--primary" href="{{ $paper->qp_route }}">Question</a>
            @if($paper->ms_url)
                <a class="button--primary--inverse" href="{{ $paper->ms_route }}">Answer</a>
            @endif
        </div>
    </div>
</div>
