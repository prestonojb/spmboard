<?php

namespace App\Console\Commands;

use App\User;
use Illuminate\Console\Command;
use Newsletter;

class SyncAllUsersToNewsletter extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync_all:newsletter';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Sync all users' emails to Mailchimp audience";

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $users = User::all();
        // Create progress bar for UI
        $bar = $this->output->createProgressBar(count($users));
        $bar->start();

        foreach ($users as $user) {
            // Get relevant tag names
            $account_type = $user->getUserAccountType();

            // Update user's email to Mailchimp's contact list and append group tag to the contact
            Newsletter::subscribeOrUpdate( $user->email, [], 'users' );
            Newsletter::addTags( [$account_type], $user->email, 'users' );

            $bar->advance();
        }

        $bar->finish();

        $this->info('Mailchimp contacts synced successfully!');
    }
}
