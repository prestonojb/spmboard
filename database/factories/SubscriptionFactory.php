<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use Faker\Generator as Faker;
use Laravel\Cashier\Subscription;

$factory->define(Subscription::class, function (Faker $faker) {
    return [
        'user_id' => 0, // To be overriden when make() is called
        'name' => 'classrooms', // To be overriden when make() is called
        'stripe_id' => 'sub_IBs0CvuCYpFTOC', // An active subscription ID on Stripe
        'stripe_status' => 'active',
        'stripe_plan' => 'price_1HGj3yHo5qT4aBLAXiugvgZ2', // An active price ID on Stripe
        'quantity' => 1
    ];
});
