@extends('classroom.layouts.app')

@section('meta')
    <title>Students | {{ config('app.name') }} Classroom</title>
@endsection

@section('content')

@component('classroom.common.breadcrumbs')
    <a href="{{ route('classroom.landing') }}">Home</a> /
    Students
@endcomponent

@component('classroom.common.page_heading', ['title' => 'Students'])
@endcomponent

@component('classroom.students.common.filter')
@endcomponent

@component('classroom.students.common.table', ['students' => $students, 'class_selected' => $class_selected])
@endcomponent

@endsection
