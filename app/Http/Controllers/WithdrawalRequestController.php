<?php

namespace App\Http\Controllers;

use App\Notifications\NewWithdrawalRequest;
use App\WithdrawalRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;

class WithdrawalRequestController extends Controller
{
    public function __construct()
    {
        $this->withdrawal_request_to_email = setting('admin.withdrawal_request_to_email');
    }

    /**
     * Store withdraw request
     */
    public function store()
    {
        $user = auth()->user();

        // Check if user type can withdraw coins
        if(!$user->canWithdraw()) {
            return redirect()->back()->withError('You are ineligible to withdraw coins!');
        }
        // Check if user has phone number setup (DuitNow)
        if(is_null($user->phone_number)) {
            return redirect()->back()->withError('You need to setup your phone number first under "Settings" before proceeding to request withdrawal!');
        }
        // Check if user is eligible for withdrawal
        if(!$user->canRequestWithdrawal()) {
            return redirect()->back()->withError('You have reached your withdrawal limit or there is still pending withdrawal requests. Check our withdrawal policy for more information.');
        }

        // Create withdrawal request
        $withdrawal_request = $user->withdrawal_requests()->create([
            'status' => 'pending',
            'withdrawal_amount_in_coin' => $user->coin_balance,
        ]);

        // Admin notified of withdrawal request
        Notification::route('mail', $this->withdrawal_request_to_email)->notify(new NewWithdrawalRequest($withdrawal_request));

        return redirect()->back()->withSuccess('Withdrawal requested successfully!');
    }
}
