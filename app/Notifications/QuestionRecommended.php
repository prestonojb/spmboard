<?php

namespace App\Notifications;

use App\Question;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\HtmlString;

class QuestionRecommended extends Notification implements ShouldQueue
{
    use Queueable;

    public $question;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Question $question)
    {
        $this->question = $question;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $question = $this->question;

        $line = "The Board community needs more amazing question askers like you! <b>Click on the button below</b> to ask the Board community more questions!";

        return (new MailMessage)
                    ->subject("🎉 Hooray! Your question is recommended by Board!")
                    ->greeting("Your question \"{$question->question}\" is recommended by Board. You are awarded 10 reward points! 🤩")
                    ->line(new HtmlString($line))
                    ->action('Ask More Questions', route('questions.create', $question->subject->exam_board->slug))
                    ->line("See you on Board! 🚀");
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $question = $this->question;

        return [
            'type' => Question::class,
            'target' => $this->question,
            'title' => "<b>Hooray!</b> Your question \"<b>{$question->question}</b>\" is recommended by Board. You are awarded <b>10 reward points</b>!",
        ];
    }
}
