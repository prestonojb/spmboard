/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 1);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/js/forum.js":
/*!*******************************!*\
  !*** ./resources/js/forum.js ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

$(function () {
  __webpack_require__(/*! ./forum/tinymce */ "./resources/js/forum/tinymce.js");

  __webpack_require__(/*! ./forum/select */ "./resources/js/forum/select.js");
});

/***/ }),

/***/ "./resources/js/forum/select.js":
/*!**************************************!*\
  !*** ./resources/js/forum/select.js ***!
  \**************************************/
/*! no static exports found */
/***/ (function(module, exports) {

var chapter_selectHTML = $('.chapter-select').html();
/*
* Initialise subject select
*/

new SlimSelect({
  select: '.subject-select',
  placeholder: 'e.g. Biology',
  allowDeselectOption: true,
  onChange: function onChange() {
    var subject_selected = $('.subject-select :selected').text();
    var $chapter_select = $('.chapter-select');
    $chapter_select.prop('disabled', false); // restore the full select list first.

    $chapter_select.html(chapter_selectHTML);
    var optGroup = $('optgroup[label="' + subject_selected + '"]').html();
    $chapter_select.html(optGroup);
    new SlimSelect({
      select: '.chapter-select',
      placeholder: 'Select Chapter',
      limit: 2,
      allowDeselectOption: true
    });
  }
});
/*
* For failed requests
*/

var subject_selected = $('.subject-select :selected').text();
var $chapter_select = $('.chapter-select'); //Enable chapter select if old subject is selected

if (subject_selected) {
  $chapter_select.prop('disabled', false);
} //Remove unrelated chapters


var optGroup = $('optgroup[label="' + subject_selected + '"]').html();
$chapter_select.html(optGroup);
/*
 * Inititialise chapter select
 */

new SlimSelect({
  select: '.chapter-select',
  placeholder: 'e.g. Respiration',
  limit: 2,
  allowDeselectOption: true
});
var preferred_subjectsHTML = $('.preferred-subjects-select').html();
/*
 * Initialise subject select
 */

new SlimSelect({
  select: '.exam-board-select',
  placeholder: 'Select Exam Board',
  allowDeselectOption: true,
  onChange: function onChange() {
    var exam_board_selected = $('.exam-board-select :selected').text();
    var $preferred_sujects_select = $('.preferred-subjects-select');
    $preferred_sujects_select.prop('disabled', false); // restore the full select list first.

    $preferred_sujects_select.html(preferred_subjectsHTML);
    var optGroup = $('optgroup[label="' + exam_board_selected + '"]').html();
    $preferred_sujects_select.html(optGroup);
    new SlimSelect({
      select: '.preferred-subjects-select',
      placeholder: 'Select Preferred Subjects',
      limit: 3,
      allowDeselectOption: true
    });
  }
});
/*
* For failed requests
*/

var subject_selected = $('.exam-board-select :selected').text();
var $preferred_sujects_select = $('.preferred-subjects-select'); //Enable chapter select if old subject is selected

if (subject_selected) {
  $preferred_sujects_select.prop('disabled', false);
} //Remove unrelated chapters


var optGroup = $('optgroup[label="' + subject_selected + '"]').html();
$preferred_sujects_select.html(optGroup);
/*
* Inititialise chapter select
*/

new SlimSelect({
  select: '.preferred-subjects-select',
  placeholder: 'e.g. Respiration',
  limit: 3,
  allowDeselectOption: true
});

/***/ }),

/***/ "./resources/js/forum/tinymce.js":
/*!***************************************!*\
  !*** ./resources/js/forum/tinymce.js ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports) {

// Reference: https://www.tiny.cloud/docs/plugins/image/
function image_upload_handler(blobInfo, success, failure, progress) {
  var xhr, formData;
  xhr = new XMLHttpRequest();
  xhr.withCredentials = false;
  xhr.open('POST', '/editor/upload');

  xhr.upload.onprogress = function (e) {
    progress(e.loaded / e.total * 100);
  };

  xhr.onload = function () {
    var json;

    if (xhr.status === 403) {
      failure('HTTP Error: ' + xhr.status, {
        remove: true
      });
      return;
    }

    if (xhr.status < 200 || xhr.status >= 300) {
      failure('HTTP Error: ' + xhr.status);
      return;
    }

    json = JSON.parse(xhr.responseText);

    if (!json || typeof json.location != 'string') {
      failure('Invalid JSON: ' + xhr.responseText);
      return;
    }

    success(json.location);
  };

  xhr.onerror = function () {
    failure('Image upload failed due to a XHR Transport error. Code: ' + xhr.status);
  };

  formData = new FormData();
  formData.append('file', blobInfo.blob(), blobInfo.filename());
  xhr.send(formData);
}

; // Configs

var plugins = ['advlist autoresize autolink charmap lists link image charmap print preview anchor', 'searchreplace visualblocks code fullscreen hr', 'insertdatetime media table paste code wordcount'];
var questionToolbar = 'bold italic underline | superscript subscript charmap | table link | bullist numlist removeformat';
var answerToolbar = 'formatselect | bold italic underline | superscript subscript charmap | table link image | bullist numlist hr removeformat';
var resourceToolbar = 'bold italic underline | superscript subscript charmap | link | bullist numlist removeformat';
var articleToolbar = 'formatselect | bold italic underline | superscript subscript charmap | table link image | bullist numlist hr removeformat';

if (typeof tinymce !== 'undefined') {
  tinymce.init({
    selector: 'textarea.question-tinymce-editor',
    height: 500,
    block_formats: 'Paragraph=p; Wrapper=wrapper;',
    object_resizing: 'td, tr, th',
    body_class: 'tinymce-editor-contents',
    content_css: '/css/app.css',
    content_style: "@import url('https://fonts.googleapis.com/css?family=Poppins:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i'); html,body {background-color: white;}",
    menubar: false,
    statusbar: false,
    default_link_target: '_blank',
    link_default_protocol: 'https',
    remove_linebreaks: true,
    setup: function setup(editor) {
      editor.on('init', function (e) {
        $('.loading-icon').hide();
        var content = editor.getContent();
        ;
        editor.setContent(content);
      });
    },
    max_height: 600,
    table_toolbar: "tableprops tabledelete | tableinsertrowbefore tableinsertrowafter tabledeleterow | tableinsertcolbefore tableinsertcolafter tabledeletecol tabledeletecol",
    plugins: plugins,
    toolbar: questionToolbar,
    images_upload_handler: image_upload_handler
  });
  tinymce.init({
    selector: 'textarea.answer-tinymce-editor',
    height: 500,
    block_formats: 'Header=h3; Paragraph=p; Wrapper=wrapper;',
    formats: {
      'wrapper': {
        block: 'div',
        classes: 'content-wrapper',
        wrapper: true
      }
    },
    object_resizing: 'td, tr, th',
    body_class: 'tinymce-editor-contents',
    content_css: '/css/app.css',
    content_style: "@import url('https://fonts.googleapis.com/css?family=Poppins:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i'); html,body {background-color: white;}",
    menubar: false,
    statusbar: false,
    default_link_target: '_blank',
    link_default_protocol: 'https',
    remove_linebreaks: true,
    setup: function setup(editor) {
      editor.on('init', function (e) {
        $('.loading-icon').hide();
        var content = editor.getContent();
        ;
        editor.setContent(content);
      });
    },
    max_height: 600,
    table_toolbar: "tableprops tabledelete | tableinsertrowbefore tableinsertrowafter tabledeleterow | tableinsertcolbefore tableinsertcolafter tabledeletecol tabledeletecol",
    plugins: plugins,
    toolbar: answerToolbar,
    images_upload_handler: image_upload_handler
  });
  tinymce.init({
    selector: 'textarea.article-tinymce-editor',
    height: 500,
    block_formats: 'Header 2=h2; Header 3=h3; Header 4=h4; Paragraph=p;',
    formats: {
      'wrapper': {
        block: 'div',
        classes: 'content-wrapper',
        wrapper: true
      }
    },
    object_resizing: 'td, tr, th',
    body_class: 'tinymce-editor-contents',
    content_css: '/css/app.css',
    content_style: "@import url('https://fonts.googleapis.com/css?family=Poppins:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i'); html,body {background-color: white;}",
    menubar: false,
    statusbar: false,
    default_link_target: '_blank',
    link_default_protocol: 'https',
    remove_linebreaks: true,
    setup: function setup(editor) {
      editor.on('init', function (e) {
        $('.loading-icon').hide();
        var content = editor.getContent();
        ;
        editor.setContent(content);
      });
    },
    max_height: 600,
    table_toolbar: "tableprops tabledelete | tableinsertrowbefore tableinsertrowafter tabledeleterow | tableinsertcolbefore tableinsertcolafter tabledeletecol tabledeletecol",
    plugins: plugins,
    toolbar: answerToolbar,
    images_upload_handler: image_upload_handler
  });
  tinymce.init({
    selector: 'textarea.resource-tinymce-editor',
    height: 500,
    block_formats: 'Paragraph=p;',
    body_class: 'tinymce-editor-contents',
    content_css: '/css/app.css',
    content_style: "@import url('https://fonts.googleapis.com/css?family=Poppins:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i'); html,body {background-color: white;}",
    menubar: false,
    statusbar: false,
    default_link_target: '_blank',
    link_default_protocol: 'https',
    remove_linebreaks: true,
    setup: function setup(editor) {
      editor.on('init', function (e) {
        $('.loading-icon').hide();
        var content = editor.getContent();
        ;
        editor.setContent(content);
      });
    },
    max_height: 600,
    plugins: plugins,
    toolbar: resourceToolbar
  });
}

/***/ }),

/***/ 1:
/*!*************************************!*\
  !*** multi ./resources/js/forum.js ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\laragon\www\spmboard\resources\js\forum.js */"./resources/js/forum.js");


/***/ })

/******/ });