<?php

namespace App\Policies;

use App\TopicalPastPaper;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class TopicalPastPaperPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    public function browse(User $user, TopicalPastPaper $topical_past_paper)
    {
        return $user->isAdmin();
    }

    public function read(User $user, TopicalPastPaper $topical_past_paper)
    {
        return $user->isAdmin();
    }

    public function edit(User $user, TopicalPastPaper $topical_past_paper)
    {
        return $user->isAdmin();
    }

    public function add(User $user, TopicalPastPaper $topical_past_paper)
    {
        return $user->isAdmin();
    }

    /**
     * Permission to view question
     */
    public function question_show(?User $user, TopicalPastPaper $topical_past_paper)
    {
        if($user->isAdmin()) {
            return true;
        }
        if(!is_null($user)) {
            if($topical_past_paper->isFree()) {
                return true;
            }
            return $user->hasResource($topical_past_paper->resource);
        }
        return false;
    }

    /**
     * Permission to view answer
     */
    public function answer_show(?User $user, TopicalPastPaper $topical_past_paper)
    {
        if($user->isAdmin()) {
            return true;
        }
        if(!is_null($user)) {
            if($topical_past_paper->isFree()) {
                return true;
            }
            return $user->hasResource($topical_past_paper->resource);
        }
        return false;
    }

    /**
     * Permission to view unlocked topical paper
     */
    public function download_pdf(?User $user, TopicalPastPaper $topical_past_paper)
    {
        // Guest
        if(!is_null($user)) {
            return $topical_past_paper->isFree() || $user->hasResource($topical_past_paper->resource);
        } else {
            return false;
        }
    }
}
