<div class="modal fade prebuilt-resources-modal prebuilt-tpp-modal show" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-xl">
      <div class="modal-content">
          <div class="modal-header">
              Topical Past Papers
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
          </div>

          <div class="modal-body">
                <div class="row">
                    @foreach($chapters as $chapter)
                        @if(count($chapter->free_topical_past_papers ?? []))
                            <div class="col-12">
                                <div class="mb-4">
                                    <h4 class="h3 font-weight500 border-bottom border-gray3 pb-1 mb-3">{{ $chapter->name }}</h4>
                                    <div class="form-row">
                                        @foreach($chapter->free_topical_past_papers as $past_paper)
                                            <div class="col-12 col-md-6">
                                                @component('classroom.common.teacher.resources.topical_past_paper_item', ['past_paper' => $past_paper])
                                                @endcomponent
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        @endif
                    @endforeach
                </div>
          </div>

          <div class="modal-footer">
            <button class="button--primary--inverse" data-dismiss="modal">Close</button>
            <button class="button--primary addResourceButton">Add</button>
        </div>
      </div>
    </div>
  </div>
