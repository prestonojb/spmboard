{{-- All Recommendations Modal --}}
<div class="modal fade" id="allRecommendationsModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Recommendations ({{ count($user->recommendations ?? []) }})</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                @foreach($user->recommendations as $recommendation)
                    <div class="py-1"></div>

                    @component('forum.users.profile.recommendations.common.block', ['recommendation' => $recommendation])
                    @endcomponent

                    @if(!$loop->last) <div class="border-bottom"></div> @endif
                @endforeach
            </div>
            <div class="modal-footer">
                <button type="button" class="button--primary--inverse" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
