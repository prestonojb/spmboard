@php
    $is_edit = isset($edit) && $edit;
    $post = isset($post) ? $post : null;
    // Declare variables
    if( !is_null($post) ) {
        $title = old('title') ?? $post->title;
        $description = old('description') ?? $post->description;
        $type = old('type') ?? $post->type;
    } else {
        $title = old('title');
        $description = old('description');
        $type = old('type');
    }
    $url = $is_edit ? route('classroom.posts.update', [$classroom->id, $post->id]) : route('classroom.posts.store', $classroom->id);
@endphp

<form action="{{ $url }}" method="post" id="">
    @csrf
    @if($is_edit) @method('PATCH') @endif
    {{-- Title --}}
    <div class="form-field">
        <label for="">Title</label>
        <input type="text" name="title" id="" value="{{ $title }}" placeholder="Title">
        @error('title')
            <div class="error-message">{{ $message }}</div>
        @enderror
    </div>

    {{-- Description --}}
    <div class="form-field">
        <label for="">Description</label>
        <div class="loading-icon loading in-editor blue"></div>
        <textarea class="tinymce-editor" name="description">{!! $description !!}</textarea>
        @error('description')
            <div class="error-message">{{ $message }}</div>
        @enderror
    </div>

    {{-- Categories --}}
    <div class="form-field">
        <label for="">Category</label>
        @foreach(['lesson', 'resource', 'homework', 'others'] as $category)
            <div>
                <input class="styled-checkbox--green" type="checkbox" name="categories[]" value="{{ $category }}" id="{{ $category }}_checkbox"
                    @if( old('categories') )
                        @if(is_array(old('categories')) && in_array($category, old('categories'))) checked @endif
                    @else
                        @if(!is_null($post))
                            @if(is_array($post->categories) && in_array($category, $post->categories)) checked @endif
                        @endif
                    @endif>
                <label for="{{ $category }}_checkbox" class="{{ App\ClassroomPost::getCategoryColor($category) }}">{{ ucfirst($category) }}</label>
            </div>
        @endforeach
        @error('categories')
            <div class="error-message">{{ $message }}</div>
        @enderror
    </div>

    {{-- Type --}}
    <div class="form-field">
        <label for="">Type</label>
        <select name="type" id="">
            <option value="" disabled selected>Select Type</option>
            <option value="announcement" @if($type == 'announcement') selected @endif>Announcement</option>
        </select>
        @error('type')
            <div class="error-message">{{ $message }}</div>
        @enderror
    </div>

    @if(!$is_edit)
        <div class="d-flex align-items-center justify-content-start mb-3">
            <input type="checkbox" name="notify_parents" id="notify_parents" {{ old('notify_parents') ? 'checked' : '' }}>
            <label for="notify_parents">{{ __('Notify Parents') }}</label>
        </div>
    @endif

    <div class="text-center">
        <button type="submit" class="button--primary loadOnSubmitButton" style="width: 129px;">{{ $is_edit ? 'Update' : 'Post' }}</button>
    </div>
</form>
