<form method="POST" action="{{ route('student.update') }}" enctype="multipart/form-data">
    @csrf
    @method('PATCH')
    @if( isset($getting_started) && $getting_started )
        <input type="hidden" name="getting_started" value="1">
    @endif

    <div class="mt-2 mb-4">
        <h3 class="">Profile</h3>
        <p class="gray2">Let the community know you better!</p>
    </div>

    <div class="form-field">
        <label>Avatar</label>
        <div class="avatar-settings-container">
            <img class="file-input-preview profile-img" src="{{ $student->avatar }}">
            <input class="file-input" name="profile_img" type="file">
            <div class="ml-2">
                <div class="file-drop-area">
                    <span class="fake-btn">Choose files</span>
                    <span class="file-msg">or drag and drop files here</span>
                    <input class="file-input" name="profile_img" type="file">
                </div>

                @error('profile_img')
                    <div class="error-message">{{ $message }}</div>
                @enderror
            </div>
        </div>
    </div>

    <div class="form-field">
        <label>Headline</label>
        <input type="text" name="headline"  value="{{ old('headline') ?? $student->headline }}" placeholder="Headline" />
        @error('headline')
            <div class="error-message">{{ $message }}</div>
        @enderror
    </div>

    <div class="form-field">
        <label>State</label>
        <select name="state">
            <option disabled selected>Select State</option>
            @if( old('state') )
                @foreach($states as $state)
                    <option value="{{ $state->id }}" @if( old('state') == $state->id ) selected @endif>{{ $state->name }}</option>
                @endforeach
            @else
                @foreach($states as $state)
                    <option value="{{ $state->id }}" @if( $student->state && $student->state->id == $state->id ) selected @endif>{{ $state->name }}</option>
                @endforeach
            @endif
        </select>
        @error('state')
            <div class="error-message">{{ $message }}</div>
        @enderror
    </div>

    <div class="form-field">
        <label>School</label>
        <input type="text" name="school" placeholder="e.g. Methodist Boys School" value="{{ old('school') ?? $student->school }}">
        @error('school')
            <div class="error-message">{{ $message }}</div>
        @enderror
    </div>

    <hr>

    <div class="mt-2 mb-4">
        <h3 class="">Syllabus</h3>
        <p class="gray2">Help us personalise your experience on Board.</p>
    </div>

    {{-- Exam Board --}}
    <div class="form-field">
        <label for="exam_board">{{ __('Exam Board') }}</label>
        <select class="" name="exam_board" id="exam_board">
            <option disabled selected value="">Select Exam Board</option>
            @if( old('exam_board') )
                @foreach($exam_boards as $board)
                    <option value="{{ $board->id }}" @if(old('exam_board') == $board->id) selected @endif>{{ $board->name }}</option>
                @endforeach
            @else
                @foreach($exam_boards as $board)
                    <option value="{{ $board->id }}" @if( $student->exam_board_id && $student->exam_board_id == $board->id ) selected @endif>{{ $board->name }}</option>
                @endforeach
            @endif

        </select>
        @error('exam_board')
            <span class="error-message">{{ $message }}</span>
        @enderror
    </div>

    <hr>

    <div class="mt-2 mb-4">
        <h3 class="">Join Board on Discord 🤗</h3>
        <p class="gray2">Meet new people and share your thoughts about Board on our Discord server.</p>

        <div class="py-1"></div>

        <a href="https://discord.gg/xBME4zxKEy" target="_blank" class="button--primary lg" style="background-color: #5865F2;">Join Discord Server 🚀</a>
    </div>



    <div class="text-center">
        <button type="submit" class="button--primary mb-1 disableOnSubmitButton">
            Update Profile
        </button>
    </div>
</form>
