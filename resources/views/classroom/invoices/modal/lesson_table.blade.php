<div class="modal fade" id="lessonTableModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Lessons to Bill</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <p>Select lessons that you want to bill for this invoice.</p>
            <table class="table lessonTable">
                <thead>
                    <tr>
                        <th scope="col"><input type="checkbox" class="selectAllCheckbox"></th>
                        <th scope="col">Name</th>
                        <th scope="col">Date</th>
                        <th scope="col">Duration(hours)</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
        <div class="modal-footer">
          <button type="button" class="button--primary" data-dismiss="modal">Confirm</button>
        </div>
      </div>
    </div>
</div>

<div class="d-none">
    <table>
        <tr class="lessonRowHelper">
            <td><input type="checkbox" class="lessonCheckbox" value="" id=""></td>
            <td class="lesson_name"></td>
            <td class="lesson_start_at"></td>
            <td class="lesson_duration_in_hours durationInHoursData"></td>
        </tr>
    </table>
</div>
