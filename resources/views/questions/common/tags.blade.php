<div class="d-flex">
    @if($question->reward_points > 0)
        <button class="d-inline-block tag--success mr-1" data-toggle="popover" data-trigger="focus" data-placement="top" data-content="Receive {{ $question->reward_points }} points as reward if your answer to this question gets accepted.">
            <b>+ {{ $question->reward_points }} RP</b>
        </button>
    @endif
    <a class="tag--light mr-1" href="{{ route('subject', [$question->subject->exam_board->slug, $question->subject->id]) }}">{{ $question->subject->name }}</a>
    @foreach($question->chapters as $chapter)
        <a class="d-none d-lg-inline-block tag--light mr-1" href="{{ route('chapter', [$question->subject->exam_board->slug, $chapter->id]) }}">{{ $chapter->name }}</a>
    @endforeach
</div>
