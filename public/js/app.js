/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/js/app.js":
/*!*****************************!*\
  !*** ./resources/js/app.js ***!
  \*****************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
// window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */
// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));
// Vue.component('example-component', require('./components/ExampleComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
// const app = new Vue({
//     el: '#app',
// });
// require('./bootstrap');
$(function () {
  var _toastr$options;

  // Get URL query string value by key
  window.getParameterByName = function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
  }; // Show loader in element to indicate that it is loading


  window.showLoader = function (elem, type, loaderColor) {
    var loader = '<div class="loading-icon loading ' + type + ' ' + loaderColor + '"></div>';
    elem.html(loader);
    elem.prop('disabled', true);
  }; // Replace loader with new HTML


  window.hideLoader = function (elem, newHtml) {
    elem.html(newHtml);
    elem.prop('disabled', false);
  }; //Loader


  $('html').css('visibility', '1');
  $('.loader').hide();
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  }); // Bootstrap Modal

  $(document).on('show.bs.modal', '.modal', function () {
    var zIndex = 1040 + 10 * $('.modal:visible').length;
    $(this).css('z-index', zIndex);
    setTimeout(function () {
      $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
    }, 0);
  });
  toastr.options = (_toastr$options = {
    'timeOut': '1500',
    "showMethod": "fadeIn"
  }, _defineProperty(_toastr$options, "showMethod", "fadeIn"), _defineProperty(_toastr$options, 'preventDuplicates', true), _defineProperty(_toastr$options, 'positionClass', 'toast-top-right'), _toastr$options); // Prevent closing from click inside dropdown

  $(document).on('click', '.dropdown-menu', function (e) {
    e.stopPropagation();
  });
  $('.disableOnSubmitButton').click(function (e) {
    e.preventDefault();
    $(this).attr('disabled', true);
    $(this).closest('form').submit();
  });
  $('.loadOnSubmitButton').click(function (e) {
    var $t = $(this);
    var initHtml = $(this).html();
    window.showLoader($t, 'in-button', 'white'); // Validate Form

    var $form = $t.closest('form'); // Validate inputs with required property

    $form.find('input').each(function () {
      // Adds error message if input is required but empty length
      if ($(this).prop('required')) {
        if (!$.trim($(this).val()).length) {
          // Prevent adding more than one error message after input
          if ($(this).next('.error-message').length == 0) {
            $(this).after('<span class="error-message">This field is required.</span>');
          }
        } else {
          $(this).next('.error-message').remove();
        }
      }
    }); // Disable modal if exists

    var $modal = $t.closest('.modal');

    if ($modal.length > 0) {
      // Reference: https://stackoverflow.com/a/38502417
      $modal.data('bs.modal')._config.backdrop = 'static';
      $modal.data('bs.modal')._config.keyboard = false;
    }

    if ($form.find('.error-message').length > 0) {
      // Form is invalid
      window.hideLoader($t, initHtml); // Re-enable modal

      if ($modal.length > 0) {
        $modal.data('bs.modal')._config.backdrop = true;
        $modal.data('bs.modal')._config.keyboard = true;
      }
    } else {
      // Form is valid
      $t.closest('form').submit();
    }
  });
  $('[data-toggle="popover"]').popover({
    trigger: 'focus'
  });
  $('[data-toggle="tooltip"]').tooltip();
  $('.hamburger-button').click(function (e) {
    var $overlay = $('.mobile-body-overlay');
    $overlay.show();
    e.stopPropagation();
  });
  $('.mobile-body-overlay').click(function () {
    $('.mobile-body-overlay').hide();
  });
  $('.mobile-nav-container').click(function (e) {
    e.stopPropagation();
  });
  $('.mobile-nav-item, .mobile-nav-dropdown-menu-item').click(function (e) {
    $(this).find('.mobile-nav-dropdown-arrow').click();
  });
  $('.mobile-nav-dropdown-arrow').click(function (e) {
    e.stopPropagation(); //Get child chapter menu

    var $dropdownMenu = $(this).parent().next('.mobile-nav-dropdown-menu, .mobile-nav-dropdown-menu__level-2');

    if ($dropdownMenu.css('display') == 'block') {
      //Hide if menu is on but is clicked again
      $dropdownMenu.hide();
    } else {
      //Reset ALL subject menu//dropdown arrow
      $dropdownMenu.show();
    } //Toggle up and down arrow of clicked list item


    $(this).find('i').toggleClass('fa-chevron-right fa-chevron-down');
  });
  $('.search-icon').click(function () {
    $('.mobile-search-form').toggle();
    $('.mobile-search-input-header').focus();
  });
  $('.notification-badge').click(function () {
    $('.bell-icon').click();
  });
  $('.clearFilterButton').click(function (e) {
    e.preventDefault();
    var form = $(this).closest('form');
    form.find('select option:disabled').prop('selected', true);
  });
  $('.confirmButton').click(function (e) {
    e.preventDefault();
    e.stopPropagation();
    var title = $(this).data('title');
    var text = $(this).data('text');
    var type = $(this).data('type') || 'warning';
    var form = $(this).closest('form');
    Swal.fire({
      title: title,
      text: text,
      type: type,
      showCancelButton: true,
      reverseButtons: true,
      confirmButtonText: 'Confirm'
    }).then(function (result) {
      if (result.value) {
        form.submit();
      }
    });
  });
  $('.deleteButton').click(function (e) {
    e.preventDefault();
    e.stopPropagation();
    var form = $(this).closest('form');
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      reverseButtons: true,
      confirmButtonText: 'Confirm'
    }).then(function (result) {
      if (result.value) {
        form.submit();
      }
    });
  });
  $('.unlockResourceButton').click(function (e) {
    e.preventDefault();
    e.stopPropagation();
    var resource_name = $(this).data('resource-name');
    var resource_coin_price = $(this).data('resource-coin-price');
    var form = $(this).closest('form');
    Swal.fire({
      title: 'Are you sure?',
      text: "You are unlocking " + resource_name + " with " + resource_coin_price + "!",
      type: 'warning',
      showCancelButton: true,
      reverseButtons: true,
      confirmButtonText: 'Confirm'
    }).then(function (result) {
      if (result.value) {
        form.submit();
      }
    });
  }); // Switch Exam Board Select

  $('.switch-exam-board-select').change(function (e) {
    e.preventDefault(); // Redirect to url

    var url = $(this).find(':selected').data('url');
    window.location.href = url;
  });
  $(".shareIcons").jsSocials({
    showLabel: false,
    showCount: false,
    shareIn: "popup",
    shares: ["facebook", "twitter", "whatsapp"]
  });
  $('.copyButton').click(function () {
    var inputId = $(this).data('input-id');
    var $input = $('#' + inputId); // Get input by ID

    $input.select();
    document.execCommand("copy");
    toastr.success('Copied to clipboard!');
  });
  $('.notification-dropdown-menu').click(function (e) {
    e.preventDefault();
    e.stopPropagation();
  });
});

__webpack_require__(/*! ./global/header2 */ "./resources/js/global/header2.js");

__webpack_require__(/*! ./global/file_input */ "./resources/js/global/file_input.js");

__webpack_require__(/*! ./global/smart_bar */ "./resources/js/global/smart_bar.js");

/***/ }),

/***/ "./resources/js/global/file_input.js":
/*!*******************************************!*\
  !*** ./resources/js/global/file_input.js ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function readURL(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function (e) {
      $('.file-input-preview').attr('src', e.target.result);
    };

    reader.readAsDataURL(input.files[0]);
  }
}

$(function () {
  var $fileInput = $('.file-input');
  var $fileInputPreview = $('.file-input-preview');
  var $droparea = $('.file-drop-area'); // highlight drag area

  $fileInput.on('dragenter focus click', function () {
    $droparea.addClass('is-active');
  }); // back to normal state

  $fileInput.on('dragleave blur drop', function () {
    $droparea.removeClass('is-active');
  }); // change inner text

  $fileInput.on('change', function (e) {
    e.preventDefault();
    var filesCount = $(this)[0].files.length;
    var $textContainer = $(this).prev();
    var fileExt = $(this).val().split('.').pop();
    var imageExt = ['jpg', 'jpeg', 'png', 'gif', 'svg']; //Check file format

    if (!imageExt.includes(fileExt)) {
      if (fileExt !== '') {
        $(this).val('');
        $textContainer.text('or drag and drop files here');
        alert('Only .jpg, .jpeg, .png, .gif, .svg image files can be uploaded!');
      } else {
        $textContainer.text('or drag and drop files here');
      }

      $fileInputPreview.attr('src', '');
    } else {
      if (filesCount === 1) {
        // if single file is selected, show file name
        var fileName = $(this).val().split('\\').pop();
        $textContainer.text(fileName);
      } else {
        // otherwise show number of files
        $textContainer.text(filesCount + ' files selected');
      }

      readURL(this);
    }
  });
  $('.clearFileInputButton').click(function (e) {
    e.preventDefault();

    if ($fileInput.val()) {
      $fileInput.val('');
      $fileInput.trigger('change');

      if (logoUrl) {
        $fileInputPreview.attr('src', logoUrl);
      } else {
        $fileInputPreview.attr('src', logoPlaceholderUrl);
      }
    }
  });
  $('.removeFileButton').click(function (e) {
    e.preventDefault();
    $('[name=remove_file]').val(1);
    $fileInput.val('');
    $fileInput.trigger('change');
    $fileInputPreview.attr('src', logoPlaceholderUrl);
  });
});

/***/ }),

/***/ "./resources/js/global/header2.js":
/*!****************************************!*\
  !*** ./resources/js/global/header2.js ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

$('.header2__hamburger-button').click(function (e) {
  var $overlay = $('.overlay');
  $overlay.show();
  e.stopPropagation();
});
$('.header2__mobile-search-icon').click(function () {
  $('.header2__mobile-search-form').toggleClass('show');
});
$('.overlay').click(function () {
  $('.overlay').hide();
});
$('.header2__sidenav-dropdown-arrow').click(function (e) {
  e.stopPropagation(); //Get child chapter menu

  var $chapterMenu = $(this).parent().next('.header2__sidenav-dropdown-menu');

  if ($chapterMenu.css('display') == 'block') {
    //Hide if menu is on but is clicked again
    $chapterMenu.hide();
  } else {
    //Reset ALL subject menu//dropdown arrow
    $chapterMenu.show();
  } //Toggle up and down arrow of clicked list item


  $(this).find('i').toggleClass('fa-chevron-right fa-chevron-down');
}); //Show sidenav dropdown menu

$('.header2__sidenav-item.active').next('.header2__sidenav-dropdown-menu').show(); //Change arrow sign

$('.header2__sidenav-item.active').find('.header2__sidenav-dropdown-arrow i').toggleClass('fa-chevron-right fa-chevron-down'); //Add active class to parent of active link

$('.header2__sidenav-dropdown-menu').each(function () {
  var hasActiveLink = $(this).has('.header2__sidenav-dropdown-menu-item.active').length > 0;

  if (hasActiveLink) {
    $(this).prev('.header2__sidenav-item').addClass('active');
    $(this).show();
  }
}); //Point mobile dropdown arrow to right direction

$('.header2__sidenav-item').each(function () {
  var siblingHasActiveLink = $(this).next('.header2__sidenav-dropdown-menu').has('.mobile-nav-dropdown-menu-item.active').length > 0;

  if (siblingHasActiveLink) {
    $(this).find('.header2__sidenav-dropdown-arrow i').toggleClass('fa-chevron-right fa-chevron-down');
  }
}); //Point dropdown arrow to right direction

$('.header2__sidenav-item.active').next('.header2__sidenav-dropdown-menu').show();
$('.header2__sidenav-item.active').find('.header2__sidenav-dropdown-arrow i').toggleClass('fa-chevron-right fa-chevron-down'); //Show active subject when chapter is active

$('.header2__sidenav-dropdown-menu').each(function () {
  var hasActiveLink = $(this).has('.header2__sidenav-dropdown-menu-item.active').length > 0;

  if (hasActiveLink) {
    $(this).prev('.header2__sidenav-item').addClass('active');
    $(this).show();
  }
}); //Dropdown menu arrow point to the right direction

$('.header2__sidenav-item').each(function () {
  var siblingHasActiveLink = $(this).next('.header2__sidenav-dropdown-menu').has('.header2__sidenav-dropdown-menu-item.active').length > 0;

  if (siblingHasActiveLink) {
    $(this).find('.header2__sidenav-dropdown-arrow i').toggleClass('fa-chevron-right fa-chevron-down');
  }
});
$('.notification-badge').click(function () {
  $('.bell-icon').click();
});

/***/ }),

/***/ "./resources/js/global/smart_bar.js":
/*!******************************************!*\
  !*** ./resources/js/global/smart_bar.js ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

// Reference: https://stackoverflow.com/questions/10730362/get-cookie-by-name
function getCookie(name) {
  var value = "; ".concat(document.cookie);
  var parts = value.split("; ".concat(name, "="));
  if (parts.length === 2) return parts.pop().split(';').shift();
} // Clone and show smart bar


$smartBarContainer = $('.smart-bar-container.mathBoardQuiz').clone().removeClass('d-none');
$smartBarContainer.prependTo($('main'));

if (!loggedIn) {
  // Show smart bar if user has not dismissed it
  if (!getCookie('dismissed_rp_promotion_bar')) {
    // Clone and show smart bar
    $smartBarContainer = $('.smart-bar-container.rpPromotion').clone().removeClass('d-none');
    $smartBarContainer.prependTo($('main'));
  } // Show smart bar if user has not dismissed it


  if (!getCookie('dismissed_ebooks_promotion_bar')) {
    // Clone and show smart bar
    $smartBarContainer = $('.smart-bar-container.ebooksPromotion').clone().removeClass('d-none');
    $smartBarContainer.prependTo($('main'));
  } // Set dismiss state as cookie and hide bar


  $('.dismissButton.rpPromotion').click(function () {
    document.cookie = "dismissed_rp_promotion_bar=true;";
    $(this).parents('.smart-bar-container').hide();
  }); // Set dismiss state as cookie and hide bar

  $('.dismissButton.ebooksPromotion').click(function () {
    document.cookie = "dismissed_ebooks_promotion_bar=true;";
    $(this).parents('.smart-bar-container').hide();
  });
}

/***/ }),

/***/ "./resources/sass/app.scss":
/*!*********************************!*\
  !*** ./resources/sass/app.scss ***!
  \*********************************/
/*! no static exports found */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ "./resources/sass/classroom/app.scss":
/*!*******************************************!*\
  !*** ./resources/sass/classroom/app.scss ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ "./resources/sass/forum/app.scss":
/*!***************************************!*\
  !*** ./resources/sass/forum/app.scss ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ "./resources/sass/main/app.scss":
/*!**************************************!*\
  !*** ./resources/sass/main/app.scss ***!
  \**************************************/
/*! no static exports found */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ "./resources/sass/voyager/app.scss":
/*!*****************************************!*\
  !*** ./resources/sass/voyager/app.scss ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ 0:
/*!**************************************************************************************************************************************************************************************************!*\
  !*** multi ./resources/js/app.js ./resources/sass/app.scss ./resources/sass/voyager/app.scss ./resources/sass/main/app.scss ./resources/sass/forum/app.scss ./resources/sass/classroom/app.scss ***!
  \**************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! C:\laragon\www\spmboard\resources\js\app.js */"./resources/js/app.js");
__webpack_require__(/*! C:\laragon\www\spmboard\resources\sass\app.scss */"./resources/sass/app.scss");
__webpack_require__(/*! C:\laragon\www\spmboard\resources\sass\voyager\app.scss */"./resources/sass/voyager/app.scss");
__webpack_require__(/*! C:\laragon\www\spmboard\resources\sass\main\app.scss */"./resources/sass/main/app.scss");
__webpack_require__(/*! C:\laragon\www\spmboard\resources\sass\forum\app.scss */"./resources/sass/forum/app.scss");
module.exports = __webpack_require__(/*! C:\laragon\www\spmboard\resources\sass\classroom\app.scss */"./resources/sass/classroom/app.scss");


/***/ })

/******/ });