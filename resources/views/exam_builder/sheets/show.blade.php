@extends('resources.layouts.app')
@section('meta')
    <title>{{ $sheet->name }} | {{ $exam_board->name }} {{ config('app.name') }} </title>
@endsection

@section('header')
    @component('resources.common.header', ['type' => 'exam_builder', 'exam_board' => $exam_board])
    @endcomponent
@endsection

@section('breadcrumbs')
    @component('resources.common.breadcrumbs')
        <a href="{{ route('exam_builder.landing').'?exam_board='.$exam_board->slug }}">Home</a> /
        {{ $sheet->name }}
    @endcomponent
@endsection

@section('context_panel')
    @component('resources.common.context_panel', ['exam_board' => $exam_board->name, 'title' => $sheet->name, 'description' => $sheet->description])
        {{-- Toolbar --}}
        <div class="py-1 my-1 border-left-gray5-3px pl-2">
            {{-- Download --}}
            @if($is_question)
                <a href="@can('download_qp', $sheet) {{ $sheet->qp_download_route }} @else javascript:void(0) @endcan" class="gray-button sm mr-2 @cannot('download_qp', $sheet) disabled @endcannot">
                    <span class="iconify" data-icon="carbon:generate-pdf" data-inline="false"></span>Download</a>
            @else
                <a href="@can('download_ms', $sheet) {{ $sheet->ms_download_route }} @else javascript:void(0) @endcan" class="gray-button sm mr-2 @cannot('download_ms', $sheet) disabled @endcannot">
                    <span class="iconify" data-icon="carbon:generate-pdf" data-inline="false"></span>Download</a>
            @endif
        </div>
    @endcomponent
@endsection

@section('side')
    <div>
        <h3 class="font-weight500 mb-3">Chapters</h3>
        {!! $sheet->chapterTagsHtml !!}
    </div>
@endsection

@section('body')
    <div class="d-flex flex-wrap justify-content-between align-items-center mb-2">
        <span class="tag--primary lg text-uppercase mb-1">{{ $is_question ? 'Question Paper' : 'Mark Scheme' }}</span>
        @if($is_question)
            <a href="{{ $sheet->ms_route }}" class="button--primary lg mb-1">Switch to Mark Scheme
                <span class="iconify" data-icon="codicon:arrow-right" data-inline="false"></span></a>
        @else
            <a href="{{ $sheet->qp_route }}" class="button--primary--inverse lg mb-1">Switch to Question Paper
                <span class="iconify" data-icon="codicon:arrow-right" data-inline="false"></span></a>
        @endif
    </div>
@endsection

@section('main')
    @if($is_question)
        {{-- QP --}}
        @switch($sheet->qp_status)
            @case('loading')
                <div class="text-center">
                    <img src="{{ asset('img/essential/under_construction.svg') }}" alt="Under Construction" width="220" height="220" class="mb-2">
                    <p class="py-2">
                        Your worksheet will be ready in <b>5-10 seconds</b>. <br>
                        Try refreshing the page if the worksheet is not showing up.
                    </p>
                </div>
                @break
            @case('available')
                <div class="w-100 custom_pdf_viewer" data-url="{{ $sheet->qp_url }}"></div>
                @break
        @endswitch
    @else
        {{-- MS --}}
        @switch($sheet->ms_status)
            @case('unavailable')
                <div class="py-2"></div>
                @include('exam_builder.sheets.common.generate_ms')
                @break
            @case('loading')
                <div class="text-center">
                    <img src="{{ asset('img/essential/under_construction.svg') }}" alt="Under Construction" width="220" height="220" class="mb-2">
                    <p class="py-2">
                        Your worksheet will be ready in <b>5-10 seconds</b>. <br>
                        Try refreshing the page if the worksheet is not showing up.
                    </p>
                </div>
                @break
            @case('available')
                <div class="w-100 custom_pdf_viewer" data-url="{{ $sheet->ms_url }}"></div>
                @break
        @endswitch
    @endif
@endsection
