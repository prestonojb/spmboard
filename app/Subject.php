<?php

namespace App;

use App\Chapter;
use App\Question;
use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    protected $guarded = [];

    public $additional_attributes = ['name_with_exam_board'];

    /**
     * ----------------
     * Relationships
     * ----------------
     */

    public function questions()
    {
        return $this->hasMany(Question::class);
    }

    public function chapters()
    {
        return $this->hasMany(Chapter::class)->whereNull('parent_chapter_id')->orderBy('chapter_number');
    }

    public function notes()
    {
        return $this->hasMany(Notes::class);
    }

    public function past_papers()
    {
        return $this->hasMany(PastPaper::class);
    }

    public function topical_past_papers()
    {
        return $this->hasMany(TopicalPastPaper::class);
    }

    public function exam_board()
    {
        return $this->belongsTo(ExamBoard::class);
    }

    public function user()
    {
        return $this->belongsToMany(User::class);
    }

    public function guides()
    {
        return $this->hasMany(Guide::class);
    }

    public function general_guides()
    {
        return $this->guides()->whereNull('chapter_id');
    }

    public function general_notes()
    {
        return $this->notes()->whereNull('chapter_id');
    }

    public function classrooms()
    {
        return $this->hasMany(Classroom::class);
    }

    public function exam_builder_items()
    {
        return $this->hasMany(ExamBuilderItem::class);
    }

    /**
     * --------------
     * Accessors
     * --------------
     */

    /**
     * Returns index URL
     * @return string
     */
    public function getIndexUrlAttribute()
    {
        return route('subject', [$this->exam_board->slug, $this->id]);
    }

    public function getNameWithExamBoardAttribute()
    {
        return "[{$this->exam_board->name}] {$this->name}";
    }

    public function getExamBoardAndNameAttribute()
    {
        return "{$this->exam_board->name} {$this->name}";
    }

    public function setNameAttribute($value){
        $this->attributes['name'] = $value;
        $this->attributes['slug'] = str_slug($value);
    }

    public function getPastPapersByYear($year)
    {
        return $this->past_papers()->where('year', $year)->get();
    }

    /**
     * -----------------
     * Helper functions
     * -----------------
     */

    /**
     * Returns array of paper numbers available for this subject
     * @return array
    */
    public function getAvailableExamBuilderPaperNumbers()
    {
        return $this->exam_builder_items()->groupBy('paper_number')->pluck('paper_number')->toArray();
    }


    /**
     * Returns chapters with at least 1 exam builder item, including chapters that has subchapters with exam builder item
     */
    public function getChaptersWithItems()
    {
        return $this->chapters()->where(function($q){
            $q->has('exam_builder_items')
              ->orWhereHas('subchapters', function($q) {
                return $q->has('exam_builder_items');
              });
        })->get();
    }

    /**
     * Returns true if subject has guides, else false
     * @return boolean
     */
    public function hasGuides()
    {
        return $this->whereHas('guides')->count() > 0;
    }

    /**
     * Returns true if subject has guides, else false
     * @return boolean
     */
    public function hasNotes()
    {
        return $this->whereHas('notes')->count() > 0;
    }

    /**
     * Returns true if subject has past papers, else false
     * @return boolean
     */
    public function hasPastPapers()
    {
        return $this->whereHas('past_papers')->count() > 0;
    }

    /**
     * Returns true if subject has topical papers, else false
     * @return boolean
     */
    public function hasTopicalPastPapers()
    {
        return $this->whereHas('topical_past_papers')->count() > 0;
    }

    /**
     * Returns max chapter number of subject
     * @return int
     */
    public function getMaxChapterNumber()
    {
        return $this->chapters()->whereNull('parent_chapter_id')->max('chapter_number');
    }

    /**
     * Returns next chapter number of subject
     * @return int
     */
    public function getNextChapterNumber()
    {
        return $this->getMaxChapterNumber() + 1;
    }

    /**
     * Returns true if subject has general guides
     * @return boolean
     */
    public function hasGeneralGuides()
    {
        return count($this->general_guides ?? []) > 0;
    }

    /**
     * Returns true if subject has general notes
     * @return boolean
     */
    public function hasGeneralNotes()
    {
        return count($this->general_notes ?? []) > 0;
    }

    /**
     * Returns general notes by type
     * @return Collection
     */
    public function getGeneralNotes($type = null)
    {
        if(!is_null($type)) {
            return $this->general_notes()->where('type', $type)->get();
        } else {
            return $this->general_notes;
        }
    }
}
