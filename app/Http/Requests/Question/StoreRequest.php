<?php

namespace App\Http\Requests\Question;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'question' => 'required|max:191',
            'description' => 'required',
            'subject' => 'required',
            'chapter' => 'required',
            'reward_points' => Rule::in(['0', '5', '10', '15', '20', '25', '30']),
            'img' => 'nullable|mimes:jpeg,png,jpg,gif,svg'
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'question.unique' => 'This question has already been asked.',
        ];
    }
}
