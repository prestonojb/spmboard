@php
switch($notification->data['type']) {
    case 'App\Question':
        $url = route('classroom.questions.show', [$notification->data['classroom_id'], $notification->data['target']['id']]);
        break;
    case 'App\Answer':
        $url = route('classroom.questions.show', [$notification->data['classroom_id'], $notification->data['target']['question_id']] );
        break;
    case 'App\ResourcePost':
        $url = route('classroom.lessons.resource_posts.show', [$notification->data['classroom_id'], $notification->data['target']['question']['id']]);
        break;
    case 'App\Homework':
        $url = route('classroom.lessons.homeworks.show', [$notification->data['classroom_id'], $notification->data['target']['id']]);
        break;
    case 'App\HomeworkSubmission':
        $url = route('classroom.lessons.homeworks.submissions.show', [$notification->data['classroom_id'], $notification->data['target']['id']]);
        break;
    default:
        $url = 'javascript:void(0)';
}
@endphp

<a class="dropdown-item d-flex align-items-center" href="{{ $url }}">
    <div class="mr-3">
        <div class="icon-circle bg-primary">
            <i class="fas fa-file-alt text-white"></i>
        </div>
    </div>
    <div>
        <div class="small text-gray-500">{{ $notification->created_at->diffForHumans() }}</div>
        <span class="font-weight-bold">{{ strip_tags($notification->data['title']) }}</span>
    </div>
</a>

