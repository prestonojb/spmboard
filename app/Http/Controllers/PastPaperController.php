<?php

namespace App\Http\Controllers;

use App\PastPaper;
use App\ExamBoard;
use App\Subject;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Session;
use Amplitude;

class PastPaperController extends Controller
{
    public function getHomeView(ExamBoard $exam_board)
    {
        $subjects = $exam_board->subjects()->whereHas('past_papers')->get();
        return view('resources.past_papers.home', compact('exam_board', 'subjects'));
    }

    public function subjectShow(ExamBoard $exam_board, Subject $subject)
    {
        $user = auth()->user();

        $past_papers = PastPaper::getPastPaperByExamBoard($exam_board);

        $past_papers = $past_papers->where('subject_id', $subject->id);
        $chapters = $subject->chapters;

        if(!is_null(request('year'))) {
            $past_papers = $past_papers->whereIn('year', request('year'));
        }

        // For SPM
        if(!is_null(request('paper_number'))) {
            $past_papers = $past_papers->whereIn('paper_number', request('paper_number'));
        }

        // For IGCSE
        if(!is_null(request('paper'))) {
            $papers = request('paper');

            // Reference of LIKE with WHEREIN: https://stackoverflow.com/questions/34329886/laravel-querybuilder-how-to-use-like-in-wherein-function
            $past_papers = $past_papers->where(function($q) use ($papers) {
                foreach($papers as $paper) {
                    $q->orWhere('paper_number', 'like', $paper.'%');
                }
            });
        }

        if(!is_null(request('season'))) {
            $past_papers = $past_papers->whereIn('season', request('season'));
        }

        if(!is_null(request('variant'))) {
            $variants = request('variant');

            // Reference of LIKE with WHEREIN: https://stackoverflow.com/questions/34329886/laravel-querybuilder-how-to-use-like-in-wherein-function
            $past_papers = $past_papers->where(function($q) use ($variants) {
                foreach($variants as $variant) {
                    $q->orWhere('paper_number', 'like', '%'.$variant);
                }
            });
        }
        // For IGCSE

        if($exam_board->slug == 'igcse') {
            $past_papers = $past_papers->orderByDesc('year')->orderBy('season')->orderBy('paper_number');
        } else {
            $past_papers = $past_papers->orderByDesc('year')->orderBy('paper_number');
        }
        $past_papers = $past_papers->paginate(100);

        return view('resources.past_papers.subject_show', compact('exam_board', 'past_papers', 'subject', 'chapters'));
    }

    public function questionShow(ExamBoard $exam_board, PastPaper $past_paper)
    {
        // Record views
        if(!is_null($past_paper->resource)) {
            views($past_paper->resource)->record();
        }

        $is_question = true;
        $subject = $past_paper->subject;
        return view('resources.past_papers.show', compact('exam_board', 'past_paper', 'is_question', 'subject'));
    }

    public function answerShow(ExamBoard $exam_board, PastPaper $past_paper)
    {
        // Record views
        if(!is_null($past_paper->resource)) {
            views($past_paper->resource)->record();
        }

        $is_question = false;
        $subject = $past_paper->subject;
        return view('resources.past_papers.show', compact('exam_board', 'past_paper', 'is_question', 'subject'));
    }

    /**
     * Download Question Paper File
     */
    public function downloadQpFile(ExamBoard $exam_board, PastPaper $past_paper)
    {
        $user = auth()->user();
        // Log event on Amplitude
        Amplitude::setUserId($user->id);
        Amplitude::queueEvent('download_resource', ['type' => "Past Paper", 'resourceable_id' => $past_paper->id, 'others' => "QP"]);

        return Storage::disk('s3')->download( $past_paper->qp_url_relative_path );
    }

    /**
     * Download Mark Scheme File
     */
    public function downloadMsFile(ExamBoard $exam_board, PastPaper $past_paper)
    {
        $user = auth()->user();
        // Log event on Amplitude
        Amplitude::setUserId($user->id);
        Amplitude::queueEvent('download_resource', ['type' => "Past Paper", 'resourceable_id' => $past_paper->id, 'others' => "MS"]);

        return Storage::disk('s3')->download( $past_paper->ms_url_relative_path );
    }

    /**
     * Delete past paper
     */
    public function delete(ExamBoard $exam_board, PastPaper $past_paper)
    {
        $subject = $past_paper->subject;
        $past_paper->delete();

        Session::flash('success', 'Past Paper deleted successfully!');
        return redirect()->route('past_papers.subject_show', [$exam_board->slug, $subject->id]);
    }
}
