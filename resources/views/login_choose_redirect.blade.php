@extends('forum.layouts.app')

@section('meta')
    <title>Redirect Login | {{ config('app.name') }}</title>
@endsection

@section('content')
@include('main.navs.header')

<div class="py-4"></div>

<div class="container">
    <h1 class="text-center mb-4"><b>Where do you want to go?</b></h1>

    <div class="row justify-content-center">

        <div class="col-12 col-sm-6 col-lg-4 mb-2">
            <div class="card h-100">
                <img class="p-5" src="{{ asset('img/classroom/discussion.svg') }}">
                <div class="card-body">
                    <h2 class="card-title"><b>Q&A and Resources</b></h2>
                    <p class="card-text gray2">A great place to learn interactively in a helpful community and high-quality study resources.</p>
                </div>
                <div class="card-footer border-0 bg-white">
                    <a href="/forum" class="card-link stretched-link underline-on-hover">Learn now <i class="fas fa-arrow-right"></i></a>
                </div>
            </div>
        </div>

        <div class="col-12 col-sm-6 col-lg-4 mb-2">
            <div class="card h-100">
                <img class="p-5" src="{{ asset('img/classroom/exam_builder.svg') }}">
                <div class="card-body">
                    <h2 class="card-title"><b>Exam Builder</b> <span class="badge badge-danger font-size5">NEW</span></h2>
                    <p class="card-text gray2">Build customised worksheets for your revision in <b>10 seconds</b>!</p>
                </div>
                <div class="card-footer border-0 bg-white">
                    <a href="{{ route('exam_builder.landing') }}" class="card-link stretched-link underline-on-hover">Try now <i class="fas fa-arrow-right"></i></a>
                </div>
            </div>
        </div>

        @if( Gate::allows('access-classroom') )
            <div class="col-12 col-sm-6 col-lg-4 mb-2">
                <div class="card h-100">
                    <img class="p-5" src="{{ asset('img/classroom/teaching.svg') }}">
                    <div class="card-body">
                        <h2 class="card-title"><b>Classroom</b></h2>
                        <p class="card-text gray2">Connecting educators and learners in a virtual space!</p>
                    </div>
                    <div class="card-footer border-0 bg-white">
                        <a href="/classroom" class="card-link stretched-link underline-on-hover">Walk me there <i class="fas fa-arrow-right"></i></a>
                    </div>
                </div>
            </div>
        @endif
    </div>
</div>

<div class="py-4"></div>

@endsection
