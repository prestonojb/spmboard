@extends('classroom.layouts.app')

@section('meta')
    <title>Invoicing | {{ config('app.name') }} Classroom</title>
@endsection

@section('content')

@component('classroom.common.breadcrumbs')
    <a href="{{ route('classroom.landing') }}">Home</a> /
    Invoices
@endcomponent

@component('classroom.common.page_heading', ['title' => 'Invoicing'])
    @can('store', App\TeacherInvoice::class)
        @slot('button_group')
            <a href="{{ route('classroom.invoices.create') }}" class="button--primary sm"><i class="fas fa-plus"></i> Create Invoice</a>
        @endslot
    @endcan
@endcomponent

@include('classroom.invoices.common.filter')

@component('classroom.invoices.common.table', ['invoices' => $invoices, 'show_students' => true])
@endcomponent

<div class="py-2"></div>
@endsection
