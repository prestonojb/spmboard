<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateTeachersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('teachers', function (Blueprint $table) {
            $table->renameColumn('teaching_experience', 'years_of_teaching_experience');

            $table->string('qualifications')->nullable();
            $table->text('teaching_approach')->nullable();
            $table->json('social_links')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('teachers', function (Blueprint $table) {
            $table->renameColumn('years_of_teaching_experience', 'teaching_experience');

            $table->dropColumn('qualifications');
            $table->dropColumn('teaching_approach');
            $table->dropColumn('social_links');
        });
    }
}
