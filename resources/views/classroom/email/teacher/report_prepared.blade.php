@component('mail::message')
# Report Generated
## Hi {{ $report->student->parent->name }}, kindly check the attached report for your child {{ $report->student->name }}.

Regards,

{{ $report->teacher->name }}
@endcomponent
