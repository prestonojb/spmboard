<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\DB;
use App\Console\Commands\GenerateDailyReport;
use App\Console\Commands\GenerateWeeklyReport;
use App\Console\Commands\SendLoginReminderEmail;
use App\Console\Commands\SyncAllUsersToNewsletter;
use App\Console\Commands\SyncRecentUsersToNewsletter;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        SyncAllUsersToNewsletter::class,
        SyncRecentUsersToNewsletter::class,
        SendLoginReminderEmail::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('sync:newsletter')->dailyAt('04:00');
        $schedule->command('login_reminder:send')->dailyAt('10:00');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
