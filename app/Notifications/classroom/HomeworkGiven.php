<?php

namespace App\Notifications\Classroom;

use App\Student;
use App\Homework;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\HtmlString;

class HomeworkGiven extends Notification implements ShouldQueue
{
    use Queueable;

    public $student;
    public $homework;
    public $lesson;
    public $classroom;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Student $student, Homework $homework)
    {
        $this->student = $student;
        $this->homework = $homework;
        $this->lesson = $homework->lesson;
        $this->classroom = $homework->lesson->classroom;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $student = $this->student;
        $homework = $this->homework;
        $lesson = $this->lesson;
        $classroom = $this->classroom;

        if( $notifiable->account_type == 'student' ) {
            $lineHtml = "Homework is given for <b>{$lesson->name}</b> Lesson in <b>{$classroom->name}</b>.";
        } elseif( $notifiable->account_type == 'parent' ) {
            $lineHtml = "<b>{$student->name}</b> has been assigned homework in <b>{$classroom->name}</b>.";
        }

        $line2Html = "Homework Title: <b>{$homework->title}</b>";

        // Homework details
        $line3Html = "";
        if( !is_null($homework->instruction) && !is_null($homework) ) {
            if( !is_null($homework->instruction) ) {
                $line3Html .= str_limit( html_entity_decode( strip_tags( $homework->instruction ) ), 200);
            }
            if( !is_null($homework) ) {
                $line3Html .= "<br>";
                $line3Html .= "<b>{$homework->due_at->toFormattedDateString()}</b>";
            }
        }

        return (new MailMessage)
                ->line(new HtmlString($lineHtml))
                ->line(new HtmlString($line2Html))
                ->line(new HtmlString($line3Html))
                ->action('Read on Board', route('classroom.lessons.homeworks.show', [$lesson->id, $homework->id]));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $student = $this->student;
        $homework = $this->homework;
        $lesson = $this->lesson;
        $classroom = $this->classroom;

        if( $notifiable->account_type == 'student' ) {
            $title = "Homework is given for <b>{$lesson->name}</b> Lesson in <b>{$classroom->name}</b>.";
        } elseif( $notifiable->account_type == 'parent' ) {
            $title = "<b>{$student->name}</b> has been assigned homework in <b>{$classroom->name}</b>.";
        }

        return [
            'type' => Homework::class,
            'target' => $homework,
            'title' => $title,
        ];
    }
}
