@extends('forum.layouts.app')

@section('meta')
<title>Profile Settings | {{ config('app.name') }}</title>
@endsection

@section('content')
@include('main.navs.header')

<div class="py-2"></div>

<div class="container">
    <div class="row">
        <div class="col-12 col-lg-9">
            @include('forum.settings.common.pills')
            <div class="panel">
                @if( $student = auth()->user()->student )
                    @component('student.forms.profile', ['student' => $student, 'states' => $states, 'exam_boards' => $exam_boards])
                    @endcomponent
                @elseif( $teacher = auth()->user()->teacher )
                    @component('teacher.forms.profile', ['teacher' => $teacher, 'states' => $states])
                    @endcomponent
                @elseif( $parent = auth()->user()->parent )
                    @component('parent.forms.profile', ['parent' => $parent, 'states' => $states])
                    @endcomponent
                @endif
            </div>
        </div>
    </div>
</div>

<div class="py-2"></div>

@endsection

@section('scripts')
<script>
$(function(){
    new SlimSelect({
        select: '.subject-select',
        placeholder: 'Select Subject',
        allowDeselectOption: true,
        closeOnSelect: false,
    });
});
</script>
@endsection
