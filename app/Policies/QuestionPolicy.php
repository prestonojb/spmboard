<?php

namespace App\Policies;

use App\User;
use App\Question;
use Illuminate\Auth\Access\HandlesAuthorization;

class QuestionPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    // Only question owner/moderator can check question's answers
    public function browse(User $user, Question $question)
    {
        return $user->isAdmin() || $user->isModerator();
    }

    public function create(?User $user)
    {
        return is_null($user) || (!is_null($user) && !$user->is_parent());
    }

    //Only question owner/moderator can check question's answers
    public function edit(User $user, Question $question)
    {
        if($user->isAdmin() || $user->isModerator()) {
            return true;
        }
        return $user->id == $question->user_id;
    }

    public function delete(User $user, Question $question)
    {
        if($user->isAdmin() || $user->isModerator()) {
            return true;
        }
        return $user->id == $question->user_id;
    }

    public function recommend(User $user, Question $question)
    {
        return $user->isAdmin() || $user->isModerator();
    }
}
