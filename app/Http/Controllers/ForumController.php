<?php

namespace App\Http\Controllers;

use App\ExamBoard;
use Illuminate\Http\Request;

class ForumController extends Controller
{
    public function getLandingView()
    {
        $user = auth()->user();
        if($user->teacher ) {
            $exam_board = ExamBoard::first();
            return redirect()->route('questions.index', $exam_board->slug);
        } elseif( $user->student ) {
            if( $user->student->exam_board ) {
                $exam_board = $user->student->exam_board;
            } else {
                $exam_board = ExamBoard::first();
            }
            return redirect()->route('questions.index', $exam_board->slug);
        } elseif( $user->parent ) {
            return redirect('/');
        }
    }
}
