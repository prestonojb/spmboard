@extends('classroom.layouts.app')

@section('meta')
    <title>Join Classroom | {{ config('app.name') }} Classroom</title>
@endsection

@section('content')

@component('classroom.common.breadcrumbs')
    <a href="{{ route('classroom.classrooms.index') }}">Home</a> /
    Join Classroom
@endcomponent

@component('classroom.common.page_heading', ['title' => 'Join Classroom'])
@endcomponent

<div class="wrapper">

    <form action="{{ route('classroom.classrooms.students.join') }}" method="POST">
        @csrf
        <div class="form-field">
            <label>{{ __('Class Code') }}</label>
            <input type="text" name="code" value="{{ old('code') }}" required autofocus placeholder="Class Code">
            @error('code')
                <span class="error-message">{{ $message }}</span>
            @enderror
        </div>

        <div class="text-center mb-2">
            <button type="submit" class="button--primary">Join</button>
        </div>

        <div class="text-center">
            <p class="gray2 font-size3 mb-0">Tip: Ask the class code from your teacher.</p>
        </div>

    </form>
</div>

@endsection
