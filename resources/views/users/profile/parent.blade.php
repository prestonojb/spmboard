@component('forum.users.profile.common.main', ['user' => $parent->user])
@endcomponent

<div class="py-1"></div>

@component('forum.users.profile.common.pills', ['user' => $parent->user])
@endcomponent
