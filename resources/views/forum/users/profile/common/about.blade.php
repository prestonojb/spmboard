<div class="panel p-0">
    <div class="panel-header">
        <h3 class="mb-0">About</h3>
    </div>
    <div class="panel-body">
        <div class="gray2">{!! nl2br($user->about) !!}</div>
    </div>
</div>
