<?php

namespace App\Http\Controllers;

use App\Classroom;
use App\HomeworkSubmission;
use App\Subject;
use App\Lesson;
use App\Homework;
use Illuminate\Support\Facades\Session;
use App\Notifications\Classroom\HomeworkReturned;
use App\Notifications\Classroom\HomeworkSubmitted;
use App\Rules\ValidSubmissionMarks;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ClassroomHomeworkSubmissionController extends Controller
{
    public function show(Lesson $lesson, HomeworkSubmission $homework_submission)
    {
        $classroom = $lesson->classroom;
        $subject = $classroom->subject;
        $homework = $homework_submission->homework;
        $submission = $homework_submission;

        return view('classroom.homeworks.submissions.show', compact('classroom', 'lesson', 'subject', 'homework', 'submission', 'homework_submission'));
    }

    /**
     * Student submit homework
     */
    public function store(Lesson $lesson, Homework $homework)
    {
        request()->validate([
            'file' => 'required|mimes:pdf',
            'comment' => 'nullable'
        ]);

        $classroom = $lesson->classroom;
        // Declare status of submission
        $status = Carbon::now()->greaterThan($homework->due_at) ? 'overdue' : 'submitted';

        $homework_submission = HomeworkSubmission::create([
            'homework_id' => $homework->id,
            'student_id' => auth()->user()->id,
            'status' => $status,
            'comment' => request()->comment,
        ]);

        // Save submission file if any
        if( request()->has('file') && !empty(request()->file('file')) ) {
            $homework_submission->saveFile( request()->file('file') );
        }

        // Notify teacher homework is submitted
        $classroom->teacher->notify(new HomeworkSubmitted( $lesson, $homework_submission ));

        Session::flash('success', 'Homework submitted successfully!');
        return redirect()->back()->with('success', 'Homework submitted successfully!');
    }

    /**
     * Teacher marking and returning homework
     */
    public function markAndReturn(Lesson $lesson, HomeworkSubmission $homework_submission)
    {
        $homework = $homework_submission->homework;
        $this->authorize('mark', $homework);

        // Validate homework marks if is graded
        if( $homework->isGraded() ) {
            request()->validate([
                'marks' => ['required', new ValidSubmissionMarks( $homework )],
            ]);
        }
        // Validate request
        request()->validate([
            'teacher_remark' => 'nullable',
        ]);

        // Update submission
        $homework_submission->update([
            'marks' => $homework->isGraded() ? request('marks') : null,
            'teacher_remark' => request('teacher_remark'),
            'marked_at' => Carbon::now(),
            'returned_at' => Carbon::now(),
            'status' => 'marked'
        ]);

        // Save marked file
        if( request()->has('marked_file') && !empty(request()->file('marked_file')) ) {
            $homework_submission->saveMarkedFile( request()->file('marked_file') );
        }

        $notify_parents = isset(request()->notify_parents) && request('notify_parents');

        // Notify student homework is returned
        $homework_submission->student->notify( new HomeworkReturned( $lesson, $homework_submission ) );
        // Notify parents if teacher opt in
        if($notify_parents && $homework_submission->student->has_parent()) {
            $homework_submission->student->parent->notify( new HomeworkReturned( $lesson, $homework_submission ) );
        }
        Session::flash('success', 'Homework marked and returned successfully!');
        return redirect()->route('classroom.lessons.homeworks.submissions.show', [$homework_submission->homework->lesson->id, $homework_submission->id]);
    }
}
