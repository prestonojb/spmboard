@extends('voyager::master')

@section('page_title', 'Dashboard')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.39.0/css/tempusdominus-bootstrap-4.min.css" integrity="sha512-3JRrEUwaCkFUBLK1N8HehwQgu8e23jTH4np5NHOmQOobuC4ROQxFwFgBLTnhcnQRMs84muMh0PnnwXlPq5MGjg==" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.min.css" integrity="sha512-/zs32ZEJh+/EO2N1b0PEdoA10JkdC3zJ8L5FTiQu82LR9S/rOQNfQN7U59U9BC12swNeRAz3HSzIL2vpp4fv3w==" crossorigin="anonymous" />
@stop

@section('page_header')
    <h1>Dashboard</h1>
@endsection

@section('content')
@if(auth()->user()->isAdmin())

    <div style="margin-bottom: 24px;">
        <a class="btn btn-info" href="{{ route('voyager.resources.update_all_preview_images') }}">Update Resource Preview Images</a>
        <p>*May take a few minutes to update</p>
    </div>

    <div style="padding-left: 20px;">
        {{-- Week Filter --}}
        <div class="">
            <form action="{{ url()->current() }}" method="get">
                @csrf
                {{-- Start Time --}}
                <div class="form-field">
                    <label for="">Week Start Date</label>
                    <input type="text" placeholder="Week Start Date" name="week_start_date" class="datetimepicker-input" id="weekStartDateDtPicker" data-toggle="datetimepicker" data-target="#weekStartDateDtPicker" value="{{ request('week_start_date') ?? Carbon\Carbon::today()->startOfWeek()->format('d-m-Y') }}">
                    @error('week_start_date')
                        <div class="error-message">{{ $message }}</div>
                    @enderror
                </div>
                <a href="{{ url()->current() }}" class="btn btn-warning">
                    This Week
                </a>
                <button type="submit" class="btn btn-primary">Apply</button>
            </form>
        </div>

        <div class="py-3"></div>


        {{-- General --}}
        <h2 class="pb-2 mb-3 border-bottom">General</h2>
        <div class="row">
            <div class="col-4">
                {{-- Total Users --}}
                @component('vendor.voyager.common.card', ['title' => 'Total Users', 'value' => $general_data['total_users']])
                    (Teacher, Student, Parent) - ({{ $general_data['total_teachers'].', '.$general_data['total_students'].', '.$general_data['total_parents'] }})
                @endcomponent
            </div>
            <div class="col-4">
                @component('vendor.voyager.common.card', ['title' => 'Total Signups', 'value' => $general_data['total_signups'], 'percentage_change' => $general_data['total_signups_percentage_change']])
                @endcomponent
            </div>
        </div>

        <h3>New Signups</h3>
        <div class="row justify-content-center">
            {{-- Signups --}}
            <div class="col-6">
                <canvas id="newSignups"></canvas>
            </div>
        </div>

        <hr>

        {{-- Sales --}}
        <h2 class="pb-2 mb-3 border-bottom">Sales</h2>
        <div class="row">
            <div class="col-4">
                {{-- Coins Purchased --}}
                @component('vendor.voyager.common.card', ['title' => 'Coins Purchased', 'value' => $sales_data['coins_purchased']."<span class='font-size3'> (~USD".coinToUsd($sales_data['coins_purchased']).")</span>"])
                @endcomponent
            </div>
        </div>

        <div class="row">
            {{-- Recent Coin Purchases --}}
            <div class="col-6">
                <h3>Coin Purchases (weekly)</h3>
                <table class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th scope="col">Date</th>
                            <th scope="col">Username</th>
                            <th scope="col">Amount (coins)</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($sales_data['coin_purchases_in_period']) > 0)
                            @foreach($sales_data['coin_purchases_in_period'] as $coin_purchase)
                                <tr>
                                    <th scope="row">{{ $coin_purchase->created_at->format('M j, Y g:iA') }}</th>
                                    <td><a href="{{ $coin_purchase->user->show_url }}" target="_blank">{{ $coin_purchase->user->username }}</a></td>
                                    <td>{{ $coin_purchase->amount }}</td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td class="text-center" colspan="3">No data</td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>

        <hr>

        {{-- Marketplace --}}
        <h2 class="pb-2 mb-3 border-bottom">Marketplace</h2>
        <div class="row">
            <div class="col-4">
                {{-- Coins transacted --}}
                @component('vendor.voyager.common.card', ['title' => 'Coins transacted', 'value' => $marketplace_data['coins_transacted']])
                @endcomponent
            </div>
            <div class="col-4">
                {{-- Total unlocks --}}
                @component('vendor.voyager.common.card', ['title' => 'Total unlocks', 'value' => $marketplace_data['total_unlocks']])
                @endcomponent
            </div>
            <div class="col-4">
                {{-- Average coins/unlock --}}
                @component('vendor.voyager.common.card', ['title' => 'Average coins/unlock', 'value' => $marketplace_data['average_coins_per_unlock']." <span class='font-size3'>coins</span>"])
                @endcomponent
            </div>

            {{-- Recent Unlocks --}}
            <div class="col-6">
                <h3>Unlocks (weekly)</h3>
                <table class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th scope="col">Unlocked Date</th>
                            <th scope="col">Resource (Selling price)</th>
                            <th scope="col">Type</th>
                            <th scope="col">Unlocked By</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($marketplace_data['unlocks_in_period']) > 0)
                            @foreach($marketplace_data['unlocks_in_period'] as $resource_user)
                                @php
                                    $resource = App\Resource::find($resource_user->resource_id);
                                    $buyer = App\User::find($resource_user->user_id);
                                @endphp
                                @if(!is_null($resource) && !is_null($buyer))
                                    <tr>
                                        <th scope="row">{{ $resource_user->created_at->format('M j, Y g:iA') }}</th>
                                        <td><a href="{{ $resource->show_url }}" target="_blank">{{ $resource->name }}</a> ({{ $resource->coin_price }} coins)</td>
                                        <td>{{ $resource->type }}</td>
                                        <td><a href="{{ $buyer->show_url }}" target="_blank">{{ $buyer->username }}</a></td>
                                    </tr>
                                @endif
                            @endforeach
                        @else
                            <tr>
                                <td class="text-center" colspan="4">No data</td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </div>

            {{-- Top Selling Resources --}}
            <div class="col-6">
                <h3>Top Unlocked Resources</h3>
                <table class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th scope="col">Resource (Selling price)</th>
                            <th scope="col">Type</th>
                            <th scope="col">Contributed by</th>
                            <th scope="col">Total Unlocks</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($marketplace_data['top_unlocked_resources']) > 0)
                            @foreach($marketplace_data['top_unlocked_resources'] as $resource_user)
                                @php
                                    $resource = App\Resource::find($resource_user->resource_id);
                                    $user = App\User::find($resource_user->user_id);
                                @endphp
                                @if(!is_null($resource) && !is_null($resource->owner()))
                                    <tr>
                                        <td><a href="{{ $resource->show_url }}" target="_blank">{{ $resource->name }}</a> ({{ $resource->coin_price }} coins)</td>
                                        <td>{{ $resource->type }}</td>
                                        <td><a href="{{ $resource->owner()->show_url }}" target="_blank">{{ $resource->owner()->username }}</a></td>
                                        <td>{{ $resource_user->unlock_count }}</td>
                                    </tr>
                                @endif
                            @endforeach
                        @else
                            <tr>
                                <td class="text-center" colspan="5">No data</td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>

        <hr>

        {{-- Board Pass --}}
        <h2 class="pb-2 mb-3 border-bottom">Board Pass</h2>
        <div class="row">
            <div class="col-4">
                {{-- Active Board Passes --}}
                @component('vendor.voyager.common.card', ['title' => 'Active Board Passes', 'value' => $board_pass_data['no_of_active_passes']])
                @endcomponent
            </div>
            <div class="col-4">
                {{-- Active Board Passes --}}
                @component('vendor.voyager.common.card', ['title' => 'Board Pass Sales', 'value' => $board_pass_data['board_pass_sales']." <span class='font-size3'>coins</span>"])
                @endcomponent
            </div>
        </div>

        <hr>

        {{-- Forum --}}
        <h2 class="pb-2 mb-3 border-bottom">Forum</h2>
        <div class="row">
            <div class="col-4">
                {{-- Total Question Asked --}}
                @component('vendor.voyager.common.card', ['title' => 'Question Asked', 'value' => $forum_data['total_questions_asked']])
                @endcomponent
            </div>
            <div class="col-4">
                {{-- Total Answers Given --}}
                @component('vendor.voyager.common.card', ['title' => 'Answers Given', 'value' => $forum_data['total_answers_given']])
                @endcomponent
            </div>
        </div>

    </div>
@endif
@stop

@section('javascript')
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js" integrity="sha512-qTXRIMyZIFb8iQcfjXWCO8+M5Tbc38Qi5WzdPOYZHIlZpzBHG3L3by84BBBOiRGiEb7KKtAOAs5qYdUiZiQNNQ==" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.39.0/js/tempusdominus-bootstrap-4.min.js" integrity="sha512-k6/Bkb8Fxf/c1Tkyl39yJwcOZ1P4cRrJu77p83zJjN2Z55prbFHxPs9vN7q3l3+tSMGPDdoH51AEU8Vgo1cgAA==" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.min.js" integrity="sha512-d9xgZrVZpmmQlfonhQUvTR7lMPtO7NkZMkA0ABN3PHCbKA5nqylQ/yWlFAyY6hYgdF1Qh6nYiuADWwKB4C2WSw==" crossorigin="anonymous"></script>
<script>
$(function(){
    // Initialise datepickers
    $('#weekStartDateDtPicker').datetimepicker({
        useCurrent: false,
        format: 'DD-MM-YYYY',
        daysOfWeekDisabled: [0, 2, 3, 4, 5, 6],
        maxDate: new Date(),
    });

    if( typeof @json($general_data['new_signups_data']) !== 'undefined') {
        var ctx = document.getElementById('newSignups').getContext('2d');
        var labels = @json(array_keys($general_data['new_signups_data']));
        var data = @json(array_values($general_data['new_signups_data']));
        var chart = new Chart(ctx, {
            type: 'line',
            data: {
                // Blade directive 'json' converts Laravel array to Javascript array
                labels: labels,
                datasets: [{
                    fill: true,
                    lineTension: 0,
                    data: data,
                }],
            },
        });
    }
});
</script>
@stop
