<div class="panel qb-container__ask-qb">
    @auth
        <a href="{{ route('users.show', auth()->user()->username) }}"><img class="avatar" src="{{ auth()->user()->avatar }}"></a>
    @else
        <a href="javascript:void(0)"><img class="avatar" src="{{ asset('img/essential/no-profile-picture.jpg') }}"></a>
    @endauth

    <a class="ask" href="{{ route('questions.create', $exam_board) }}" class="h3">What do you want to ask today?</a>

    @auth
        <div class="ask-qb__toolbar">
            @can('create_store', \App\Article::class)
                <a href="{{ route('users.articles.create') }}" class="ask-qb__toolbar-button">
                    <span class="iconify orange" data-icon="ph:article-light" data-inline="false"></span>
                    <span class="gray1">Write Article</span>
                </a>
            @endcan
            @if(auth()->user()->is_teacher())
                <a href="https://forms.gle/WCk8fKqh4qpFR81Q6" target="_blank" class="ask-qb__toolbar-button">
                    <span class="iconify blue5" data-icon="carbon:notebook-reference" data-inline="false"></span>
                    <span class="gray1">Author Guide <span class="font-size1 gray2"><i class="fas fa-external-link-alt"></i></span></span>
                </a>
            @endif
        </div>
    @endauth
</div>
