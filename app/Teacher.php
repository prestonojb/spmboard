<?php

namespace App;

use Carbon\Carbon;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Teacher extends Model
{
    use Notifiable;

    protected $guarded = [];
    protected $primaryKey = 'user_id';

    public function getRouteKeyName()
    {
        return 'user_id';
    }

    /**
     * --------------
     * Relationships
     * --------------
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function classrooms()
    {
        return $this->hasMany(Classroom::class, 'teacher_id');
    }

    public function invoices()
    {
        return $this->hasMany(TeacherInvoice::class, 'teacher_id', 'user_id');
    }

    public function reports()
    {
        return $this->hasMany(StudentReport::class, 'teacher_id', 'user_id');
    }

    public function invoice_setting()
    {
        return $this->hasOne(TeacherInvoiceSetting::class, 'teacher_id', 'user_id');
    }

    public function getStudentsAttribute()
    {
        return $this->students();
    }

    public function students()
    {
        $classroom_ids = $this->classrooms()->pluck('id')->toArray();
        return Student::whereHas('classrooms', function($q) use ($classroom_ids) {
                    $q->whereIn( 'classrooms.id', $classroom_ids );
                })->get();
    }

    /**
     * -------------
     * Accessors
     * -------------
     */
    public function getAccountTypeAttribute()
    {
        return 'teacher';
    }

    public function getUsernameAttribute()
    {
        return $this->user->username;
    }

    public function getEmailAttribute()
    {
        return $this->user->email;
    }

    public function getNameAttribute()
    {
        return $this->user->name;
    }

    public function getStateAttribute()
    {
        return $this->user->state;
    }

    public function getAvatarAttribute()
    {
        return $this->user->avatar;
    }

    public function getLastOnlineAtAttribute()
    {
        return $this->user->last_online_at;
    }

    public function getAboutAttribute()
    {
        return $this->user->about;
    }

    public function getClassroomPostsAttribute()
    {
        return $this->user->classroom_posts;
    }

    public function getHeadlineAttribute()
    {
        return $this->user->headline;
    }

    /**
     * Returns social links array
     * @return array
     */
    public function getSocialLinksAttribute($value)
    {
        return json_decode($value);
    }

    /**
     * Returns Facebook URL
     * @return string
     */
    public function getFacebookUrlAttribute()
    {
        return !is_null($this->social_links) ? $this->social_links->facebook
                                             : null;
    }

    /**
     * Returns Instagram URL
     * @return string
     */
    public function getInstagramUrlAttribute()
    {
        return !is_null($this->social_links) ? $this->social_links->instagram
                                             : null;
    }

    /**
     * Returns Youtube URL
     * @return string
     */
    public function getYoutubeUrlAttribute()
    {
        return !is_null($this->social_links) ? $this->social_links->youtube
                                             : null;
    }

    /**
     * Returns age and gender
     * @return string
     */
    public function getAgeAndGenderAttribute()
    {
        if(!is_null($this->age) && !is_null($this->full_gender_str)) {
            return "{$this->age} years old, ". ucfirst($this->full_gender_str);
        } elseif( !is_null($this->age) && is_null($this->full_gender_str) ) {
            return "{$this->age} years old";
        } elseif( is_null($this->age) && !is_null($this->full_gender_str) ) {
            return ucfirst($this->full_gender_str);
        } else {
            return null;
        }
    }

    /**
     * Returns teacher's age
     * @return int
     */
    public function getAgeAttribute()
    {
        return !is_null($this->year_of_birth) ? Carbon::createFromFormat('Y', $this->year_of_birth)->age
                                              : null;
    }

    /**
     * Returns teacher's full gender string (Male, Female)
     * @return string
     */
    public function getFullGenderStrAttribute()
    {
        $dict = [
            'M' => 'male',
            'F' => 'female',
        ];
        return !is_null($this->gender) ? $dict[$this->gender]
                                       : null;
    }

    /**
     * ------------
     * Mutators
     * ------------
     */

    public function setNameAttribute($value)
    {
        $this->user()->update([
            'name' => $value
        ]);
    }

    public function setStateAttribute($value)
    {
        $this->user()->update([
            'state_id' => $value
        ]);
    }

    public function setHeadlineAttribute($value)
    {
        $this->user()->update([
            'headline' => $value,
        ]);
    }

    /**
     * Returns social links array
     * @return array
     */
    public function setSocialLinksAttribute($value)
    {
        $this->attributes['social_links'] = json_encode($value);
    }

    /**
     * Returns true if teacher teaches student, else false
     * @return boolean
     */
    public function hasStudent(Student $student)
    {
        return $this->students()->contains($student->user_id);
    }

    /**
     * Get maximum invoice sequence based on prefix
     * Returns max int if invoice number with prefix exists, else return null
     * @param string $prefix Invoice Prefix
     * @return int|null
     */
    public function getMaxInvoiceSequenceByPrefix( $prefix )
    {
        // Prefix not null
        if( !is_null($prefix) ) {
            // Check for teacher's existing invoices with same prefix
            $invoice_sequence = $this->invoices()->where('invoice_number', 'LIKE', $prefix.'%')->pluck('invoice_number');
            // Teacher has existing invoices with same prefix
            if( count($invoice_sequence) ) {
                $prefix_length = strlen( $prefix );
                // Map Invoice Prefixes (INV-XXX) to Invoice Sequence (XXX)
                $invoice_sequence = $invoice_sequence->map(function($number) use ($prefix_length){
                    return intval(substr($number, $prefix_length));
                })->toArray();

                // Return max invoice sequence
                return max($invoice_sequence);
            }
        }
        return null;
    }

    /**
     * Returns profile color
     * @return string
     */
    public function getProfileColor()
    {
        return 'blue6';
    }

    /**
     * ---------------
     * FORUM
     * ---------------
     */
    /**
     * ---------------
     * HELPERS
     * ---------------
     */
    /**
     * Returns true if teacher uses Board Classroom
     * @return boolean
     */
    public function useClassroom()
    {
        return count($this->classrooms ?? []) > 0;
    }

    /**
     * Returns all subjects taught
     * @return Collection
     */
    public function getSubjectsTaught()
    {
        return $this->user->subjects()->orderBy('exam_board_id')->orderBy('name')->get();
    }

    /**
     * Returns true if teacher teaches subject, else false
     * @return boolean
     */
    public function teachSubject(Subject $subject)
    {
        return $this->user->chosenSubject($subject);
    }

    /**
     * ------------------------------------
     * Premium Subscription
     * ------------------------------------
     */

     /**
      * Returns true if teacher can create an extra classroom, else false
      * @return boolean
      */
    public function hasExtraClassroom()
    {
        $classroom_count = count( $this->classrooms ?? []);
        return $classroom_count < $this->getClassroomLimit();
    }

    /**
     * Returns true if teacher can add an extra student into classroom, else false
     * @param App\Classroom $classroom
     * @return boolean
     */
    public function hasExtraStudentSlot(Classroom $classroom)
    {
        $student_count = count( $classroom->students ?? []);
        return $student_count < $this->getStudentLimit();
    }

    /**
     * Returns true if teacher can add extra students into classroom, else false
     * @param App\Classroom $classroom
     * @param int $no_of_slots
     * @return boolean
     */
    public function hasExtraStudentSlots(Classroom $classroom, $no_of_slots)
    {
        $student_count = count( $classroom->students ?? []);
        return $student_count + $no_of_slots <= $this->getStudentLimit();
    }

    /**
     * Returns true if teacher can add an extra lesson into classroom, else false
     * @return int
     */
    public function hasExtraLesson(Classroom $classroom)
    {
        $lesson_count = count( $classroom->lessons ?? []);
        return $lesson_count < $this->getLessonLimit();
    }

    /**
     * Returns limit of classroom for teacher's subscription
     * @return int
     */
    public function getClassroomLimit()
    {
        if( $this->user->subscribed('premium') ) {
            return setting('classroom.premium_classroom_limit');
        } else {
            return setting('classroom.free_classroom_limit');
        }
    }

    /**
     * Returns limit of students that can be added into classroom for teacher's subscription
     * @return int
     */
    public function getStudentLimit()
    {
        if( $this->user->subscribed('premium') ) {
            return setting('classroom.premium_student_limit');
        } else {
            return setting('classroom.free_student_limit');
        }
    }

    /**
     * Returns limit of lessons that can be added into classroom for teacher's subscription
     * @return int
     */
    public function getLessonLimit()
    {
        if( $this->user->subscribed('premium') ) {
            return setting('classroom.premium_lesson_limit');
        } else {
            return setting('classroom.free_lesson_limit');
        }
    }

    /**
     * Returns teacher's lessons
     * @param App\Classroom $classroom
     * @return Collection
     */
    public function getLessons(Classroom $classroom = null)
    {
        return Lesson::find($this->getLessonIds($classroom));
    }

    /**
     * Get lesson IDs associated with teacher
     * @param(optional) App\Classroom $classroom Filter lesson IDs by classroom
     * @return array
     */
    public function getLessonIds( Classroom $classroom = null )
    {
        if( !is_null( $classroom ) ) {
            if( !$classroom->isTaughtBy( $this ) ) {
                return array();
            }
            // Get all lessons from classroom
            return $classroom->lessons()->pluck('id')->toArray();
        } else {
            $classroom_ids = $this->classrooms()->pluck('id')->toArray();
            // Get all lessons from the student
            return Lesson::whereHas('classroom', function($q) use ($classroom_ids){
                        $q->whereIn('id', $classroom_ids);
                    })->pluck('id')->toArray();
        }
    }

    public function getLessonsWithStudentIds( Student $student )
    {
        $teacher_lesson_ids = $this->getLessonIds();
        $student_lesson_ids = $student->getLessonIds();
        return array_intersect($teacher_lesson_ids, $student_lesson_ids);
    }

    public function getLessonsWithStudent( Student $student )
    {
        return Lesson::find( $this->getLessonsWithStudentIds($student) );
    }

    /**
     * Student Report functions
     * Relevant unreported lessons of students for teacher
     * T&S unreported lessons = ALL T&S lessons && Student unreported lessons
     * @param App\Student $student
     */
    public function getUnreportedLessonIds( Student $student )
    {
        $all_lesson_ids = $this->getLessonsWithStudentIds( $student );
        $all_student_unreported_lesson_ids = $student->getUnreportedLessonIds();

        return array_intersect( $all_lesson_ids, $all_student_unreported_lesson_ids );
    }

    public function getUnreportedLessons( Student $student )
    {
        return Lesson::find( $this->getUnreportedLessonIds( $student ) );
    }

    /**
     * Student Invoice functions
     */

    /**
     * Returns teacher's unpaid lesson IDs
     * @return array
     */
    public function getUnpaidLessonIds( Student $student )
    {
        $all_lesson_ids = $this->getLessonsWithStudentIds( $student );
        $all_student_unpaid_lesson_ids = $student->getUnpaidLessonIds();

        return array_intersect( $all_lesson_ids, $all_student_unpaid_lesson_ids );
    }

    /**
     * Returns teacher's unpaid lessons
     * @return Collection
     */
    public function getUnpaidLessons( Student $student )
    {
        return Lesson::find( $this->getUnpaidLessonIds( $student ) );
    }

    /**
     * Get Homeworks
     * @param App\Classroom
     * @return Collection
     */
    public function getHomeworks(Classroom $classroom = null)
    {
        return Homework::whereIn('lesson_id', $this->getLessonIds( $classroom ))->get();
    }

    /**
     * Create Invoice Setting if does not exist, else get existing one
     * @return App\InvoiceSetting
     */
    public function createOrGetInvoiceSetting()
    {
        if( !$this->invoice_setting ) {
            // Create invoice setting row for teacher with default values
            return $this->invoice_setting()->create([
                        'next_invoice_sequence' => 1,
                        'prefix' => 'INV-',
                        'default_payment_due_in_days' => 14,
                        'currency' => 'MYR',
                        'default_lesson_rate_per_hour' => 50,
                    ]);
        }

        return $this->invoice_setting;
    }

    /**
     * Returns true if teacher has access to resources (notes, topical past papers)
     * @return boolean
     */
    public function hasResourceAccess()
    {
        return $this->hasPremiumSubscription();
    }

    /**
     * Returns true if teacher has classroom subscription
     * @return boolean
     */
    public function hasPremiumSubscription()
    {
        return $this->user->subscribed('premium');
    }

    /**
     * ----------------
     * Exam Builder
     * ----------------
     */

    /**
     * Returns -1 if user has infinite worksheets, else return number of available worksheets that can be generated this month
     * Free user gets to generate 3 worksheets/month
     * @return int
     */
    public function noOfAvailableWorksheetsLeft()
    {
        return 3 - $this->user->exam_builder_sheets()->whereDate('created_at', '>', Carbon::now()->startOfWeek()->format('Y-m-d'))->count();
    }

    /**
     * Returns true if teacher has permission to generate exam builder MS, else false
     * @return boolean
     */
    public function canGenerateExamBuilderSheet()
    {
        // Returns true if teacher has classroom subscription
        if( $this->hasPremiumSubscription() ) {
            return true;
        } else {
            return 3 - $this->user->exam_builder_sheets()->whereDate('created_at', '>', Carbon::now()->startOfWeek()->format('Y-m-d'))->count();
        }
    }

    /**
     * Returns true if teacher can hide exam builder sheet watermark
     * @return boolean
     */
    public function canHideExamBuilderSheetWatermark()
    {
        return $this->hasPremiumSubscription();
    }

    /**
     * Returns true if teacher can generate exam builder sheet with subchapter
     * @return boolean
     */
    public function canGenerateExamBuilderSheetWithSubchapter()
    {
        return $this->hasPremiumSubscription();
    }

    /**
     * Returns max number of questions teacher can generate for exam builder sheet
     * @return int
     */
    public function getExamBuilderQuestionLimit()
    {
        return $this->hasPremiumSubscription() ? setting('exam_builder.premium_question_limit') : setting('exam_builder.free_question_limit');
    }

    /**
     * Returns max number of chapters teacher can generate for exam builder sheet
     * @return int
     */
    public function getExamBuilderChapterLimit()
    {
        return $this->hasPremiumSubscription() ? 5 : 1;
    }

    /**
     * Returns true if teacher has premium resource access
     * @return boolean
     */
    public function hasPremiumResourceAccess()
    {
        return $this->hasPremiumSubscription();
    }

    /**
     * Returns library limit of user
     * @return int
     */
    public function getLibraryLimit()
    {
        return $this->hasPremiumSubscription() ? setting('board_premium.library_limit') : 1;
    }

    /**
     * Returns ALL certification metrics
     * @return array
     */
    public function getCertificationMetrics()
    {
        return [
            'no_of_answers' => $this->user->answers()->whereDate('created_at', '>=', Carbon::now()->subDays(30)->format('Y-m-d'))->count(),
            'no_of_checked_answers' => $this->user->answers()->whereDate('checked_at', '>=', Carbon::now()->subDays(30)->format('Y-m-d'))->count(),
            'no_of_login_days' => $this->user->reward_point_actions()->where('name', 'Login Reward')->whereDate('created_at', '>=', Carbon::now()->subDays(30)->format('Y-m-d'))->count(),
            'no_of_articles' => $this->user->articles()->whereDate('created_at', '>=', Carbon::now()->subDays(30)->format('Y-m-d'))->count(),
            'no_of_recommendations' => $this->user->recommendations()->count(),
        ];
    }

    /**
     * Returns ALL certification metrics target
     * @return array
     */
    public function getCertificationMetricsTarget()
    {
        return [
            'no_of_answers' => 15,
            'no_of_checked_answers' => 10,
            'no_of_login_days' => 14,
            'no_of_articles' => 1,
            'no_of_recommendations' => 10,
        ];
    }

    /**
     * Returns true if teacher passed all certification metrics, else returns false
     * @return boolean
     */
    public function passedAllCertificationMetrics()
    {
        foreach($this->getCertificationMetrics() as $key => $value) {
            // Returns false if one metric doesn't pass
            if($value < $this->getCertificationMetricsTarget()[$key]) {
                return false;
            }
        }
        return true;
    }
}
