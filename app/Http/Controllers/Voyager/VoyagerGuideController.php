<?php

namespace App\Http\Controllers\Voyager;

use App\Guide;
use App\Subject;
use App\Resource;
use \TCG\Voyager\Http\Controllers\VoyagerBaseController as VoyagerBaseController;
use Illuminate\Http\Request;

class VoyagerGuideController extends VoyagerBaseController
{
    /**
     * Create guides
     */
    public function create(Request $request)
    {
        $is_edit = false;
        return view('vendor.voyager.guides.create_edit', compact('is_edit'));
    }

    /**
     * Store guides
     */
    public function store(Request $request)
    {
        // Validation
        $request->validate([
            'title' => 'required',
            'description' => 'required',
            'body' => 'required',
            'subject_id' => 'required',
            'chapter_id' => 'required',
        ]);

        $guide = Guide::create([
                    'title' => $request->title,
                    'description' => $request->description,
                    'body' => $request->body,
                    'subject_id' => $request->subject_id,
                    'chapter_id' => $request->chapter_id,
                    'user_id' => $request->author_id,
                ]);

        // Create resource
        Resource::create([
            'resourceable_type' => Guide::class,
            'resourceable_id' => $guide->id
        ]);

        return redirect('admin/guides')->with(['alert-type' => 'success', 'message' => 'Guide uploaded successfully!']);
    }

    /**
     * Edit guide
     */
    public function edit(Request $request, $id)
    {
        $guide = Guide::find($id);
        $is_edit = true;
        return view('vendor.voyager.guides.create_edit', compact('guide', 'is_edit'));
    }

    /**
     * Update guides
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => 'required',
            'description' => 'required',
            'body' => 'required',
            'subject_id' => 'required',
            'chapter_id' => 'required',
        ]);

        $guide = Guide::find($id);

        $guide->update([
            'title' => $request->title,
            'description' => $request->description,
            'body' => $request->body,
            'subject_id' => $request->subject_id,
            'chapter_id' => $request->chapter_id,
            'user_id' => $request->author_id,
        ]);

        return redirect('admin/guides')->with(['alert-type' => 'success', 'message' => 'Guide updated successfully!']);
    }

    /**
     * Delete guide
     */
    public function delete(Request $request, $id)
    {
        $guide = Guide::find($id);
        $guide->delete();
        return redirect('admin/guides')->with(['alert-type' => 'success', 'message' => 'Guide deleted successfully!']);
    }
}
