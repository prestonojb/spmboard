/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 3);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/js/classroom.js":
/*!***********************************!*\
  !*** ./resources/js/classroom.js ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

$(function () {
  "use strict"; // Start of use strict
  // Toggle the side navigation

  $("#sidebarToggle, #sidebarToggleTop").on('click', function (e) {
    $("body").toggleClass("sidebar-toggled");
    $(".sidebar").toggleClass("toggled");

    if ($(".sidebar").hasClass("toggled")) {
      $('.sidebar .collapse').collapse('hide');
    }

    ;
  }); // Scroll to top button appear

  $(document).on('scroll', function () {
    var scrollDistance = $(this).scrollTop();

    if (scrollDistance > 100) {
      $('.scroll-to-top').fadeIn();
    } else {
      $('.scroll-to-top').fadeOut();
    }
  }); // Smooth scrolling using jQuery easing

  $(document).on('click', 'a.scroll-to-top', function (e) {
    var $anchor = $(this);
    $('html, body').stop().animate({
      scrollTop: $($anchor.attr('href')).offset().top
    }, 1000, 'easeInOutExpo');
    e.preventDefault();
  });
  $('#classroomSelect').change(function (e) {
    var url = $(this).find('option:selected').data('url');
    window.location.href = url;
  }); // Mark invoice as paid button

  $('.markAsPaidButton').click(function (e) {
    e.preventDefault();
    e.stopPropagation();
    var form = $(this).closest('form');
    Swal.fire({
      title: 'Are you sure to mark this invoice as paid?',
      text: "You won't be able to revert this!",
      type: 'info',
      showCancelButton: true,
      reverseButtons: true,
      confirmButtonText: 'Confirm',
      cancelButtonText: 'Cancel'
    }).then(function (result) {
      if (result.value) {
        form.submit();
      }
    });
  });
  $('.downloadFileLink').click(function (e) {
    e.preventDefault();
    var link = $(this).attr('href');
    link.download = 'file.pdf';
  });
  $('.notificationBellButton').click(function (e) {
    $.ajax({
      method: 'GET',
      url: '/classroom/markAsRead'
    });
  });

  __webpack_require__(/*! ./classroom/add_resource */ "./resources/js/classroom/add_resource.js");

  __webpack_require__(/*! ./classroom/selectize */ "./resources/js/classroom/selectize.js");

  __webpack_require__(/*! ./classroom/report */ "./resources/js/classroom/report.js");

  __webpack_require__(/*! ./classroom/invoice */ "./resources/js/classroom/invoice.js");

  __webpack_require__(/*! ./classroom/lesson */ "./resources/js/classroom/lesson.js");

  __webpack_require__(/*! ./classroom/chart */ "./resources/js/classroom/chart.js");

  __webpack_require__(/*! ./classroom/quill */ "./resources/js/classroom/quill.js");

  __webpack_require__(/*! ./classroom/tinymce */ "./resources/js/classroom/tinymce.js");

  __webpack_require__(/*! ./classroom/datatables */ "./resources/js/classroom/datatables.js");
})(jQuery); // End of use strict

/***/ }),

/***/ "./resources/js/classroom/add_resource.js":
/*!************************************************!*\
  !*** ./resources/js/classroom/add_resource.js ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

var customFile = [];
var $customFileInput = $('[name="custom_file"]');
var $fileBlockCol = $('.fileBlockCol').first();
var $customFileForm = $('#customFileForm');
$('#customFileUploadButton').click(function () {
  $customFileInput.click();
}); // Update custom files

$customFileInput.on('change', function (e) {
  var file = $(this)[0].files[0];
  var fileNameWithType = file.name; // To prevent '.' in filename (i.e. 1.1 Length and Time.pdf), find last '.' in filename, then split the string into NAME and EXT

  var fileExtStartIdx = fileNameWithType.lastIndexOf('.');
  var fileName = fileNameWithType.substring(0, fileExtStartIdx);
  var fileExt = fileNameWithType.substring(fileExtStartIdx + 1, fileNameWithType.length); // EXTENSION TO UPPERCASE

  fileExt = fileExt.toUpperCase();
  var allowedFileExt = ['PDF']; // Reject files more than 10 MB

  if (file.size > 10000000) {
    alert('Only files no more than 10MB can be uploaded.');
  } else {
    //Check file format
    if (!allowedFileExt.includes(fileExt)) {
      if (fileExt !== '') {
        $(this).val('');
        alert('Only .pdf files can be uploaded!');
      }
    } else {
      var formData = new FormData($customFileForm[0]);
      $.ajax({
        type: "POST",
        url: customFileUploadUrl,
        data: formData,
        success: function success(file_url) {
          // Add file block to DOM
          var $newFileBlockCol = $fileBlockCol.clone();
          $newFileBlockCol.find('.file-title').text(fileName);
          $newFileBlockCol.find('.file-type').text('Custom');
          $newFileBlockCol.find('.file-block').attr('data-custom', true);
          $newFileBlockCol.find('.file-block').attr('data-file-url', file_url);
          $newFileBlockCol.find('a').attr('href', storageUrl + file_url);
          $newFileBlockCol.appendTo('.file-block-container .row');
        },
        cache: false,
        contentType: false,
        processData: false
      });
    }
  }
}); // $('.prebuilt-resources-modal .file-block').click(function(){
//     $(this).toggleClass('selected');
// });

$('.past-paper-list-item').click(function () {
  $(this).find('.collapse-arrow').toggleClass('fa-angle-down fa-angle-up');
});
$('.addResourceButton').click(function (e) {
  e.preventDefault();
  var existingSelectedResources = []; // Get all existing resource IDs

  $('.fileBlockCol .file-block').each(function () {
    var existingResource = {};
    existingResource.id = $(this).data('id');
    existingResource.type = $(this).find('.file-type').text();
    ;
    existingSelectedResources.push(existingResource);
  }); // On resources add, add selected file blocks to UI, which will be used later to transfer to backend for storage

  $(this).closest('.modal').find('.resource-list-item .checkbox-input:checked').each(function () {
    // Construct selected resource object
    var resourceId = $(this).closest('.resource-list-item').data('resource-id');
    var resourceType = $(this).closest('.resource-list-item').data('resource-type');
    var resource = {};
    resource.id = resourceId;
    resource.type = resourceType; // JS some() function reference: https://www.tutorialrepublic.com/faq/how-to-check-if-an-array-includes-an-object-in-javascript.php
    // Resource not already selected

    if (!existingSelectedResources.some(function (existingSelectedResource) {
      return existingSelectedResource.id == resource.id && existingSelectedResource.type == resource.type;
    })) {
      var resourceName = $(this).closest('.resource-list-item').data('resource-name');
      var type = $(this).closest('.resource-list-item').data('type');
      var url = $(this).closest('.resource-list-item').find('a').attr('href'); // Add file block to DOM

      var $newFileBlockCol = $fileBlockCol.clone();
      $newFileBlockCol.find('.file-title').text(resourceName);
      $newFileBlockCol.find('.file-type').text(resourceType);
      $newFileBlockCol.find('.file-block').attr('data-id', resourceId); // Type = Question or Answer

      $newFileBlockCol.find('.file-block').attr('data-type', type);
      $newFileBlockCol.find('a').attr('href', url);
      var $filePreview = $newFileBlockCol.find('.file-preview');

      switch (resourceType) {
        case 'Note':
          $filePreview.html('<span class="iconify" data-icon="mdi-light:note-text" data-inline="false"></span>');
          break;

        case 'Past Paper (Question)':
          $filePreview.html('<span class="iconify" data-icon="carbon:stacked-scrolling-1" data-inline="false"></span>');
          break;

        case 'Past Paper (Answer)':
          $filePreview.html('<span class="iconify" data-icon="carbon:stacked-scrolling-1" data-inline="false"></span>');
          break;

        case 'Topical Paper (Question)':
          $filePreview.html('<span class="iconify" data-icon="carbon:category-new-each" data-inline="false"></span>');
          break;

        case 'Topical Paper (Answer)':
          $filePreview.html('<span class="iconify" data-icon="carbon:category-new-each" data-inline="false"></span>');
          break;

        default:
          $filePreview.html('<span class="iconify" data-icon="ant-design:file-outlined" data-inline="false"></span>');
          break;
      }

      $newFileBlockCol.appendTo('.file-block-container .row');
    }
  });
  $(this).closest('.modal').modal('hide');
}); // Remove resources(prebuilt/custom) on cross button

$(document).on('click', '.file-cross-button', function () {
  // File Block to be deleted
  var $fileBlock = $(this).closest('.file-block'); // Delete custom file from custom_files input

  if ($fileBlock.attr('data-custom') == 'true') {
    var files = $customFileInput[0].files;

    for (var i = 0; i < files.length; i++) {
      var file = files[i];

      if (file.lastModified == $fileBlock.attr('last-modified')) {}
    }
  }

  $(this).parents('.fileBlockCol').remove();
}); // Convert resources(prebuilt/custom) to readable data to backend for storage

$('#postResourcesForm [type=submit], #postHomeworkForm [type=submit]').click(function (e) {
  e.preventDefault();
  var resourceData = [];
  var customResourceData = [];
  $('.file-block-container .file-block').each(function () {
    if ($(this).attr('data-custom') == 'true') {
      // Custom Resource
      var custom_resource = {};
      custom_resource.file_url = $(this).attr('data-file-url');
      custom_resource.file_name = $(this).find('.file-title').text();
      customResourceData.push(custom_resource);
    } else {
      // Pre-built Resources
      var resource_id = $(this).attr('data-id');

      if ($(this).attr('data-type')) {
        var type = $(this).attr('data-type');
      } else {
        var type = null;
      }

      var resource = {};
      resource.resource_id = parseInt(resource_id);
      resource.type = type;
      resourceData.push(resource);
    }
  });
  $('[name=resource_data]').val(JSON.stringify(resourceData));
  $('[name=custom_resource_data]').val(JSON.stringify(customResourceData));
  var val = $('[name=resource_data]').val();
  $(this).closest('form').submit();
});

/***/ }),

/***/ "./resources/js/classroom/chart.js":
/*!*****************************************!*\
  !*** ./resources/js/classroom/chart.js ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

Chart.defaults.global.legend.display = false;
var colors = '#2174D3';

if (typeof noOfResourcePostsTimeLabels !== 'undefined' && typeof noOfResourcePostsTimeData !== 'undefined') {
  var ctx = document.getElementById('noOfResourcePostTime').getContext('2d');
  var labels = noOfResourcePostsTimeLabels;
  var data = noOfResourcePostsTimeData;
  var chart = new Chart(ctx, {
    type: 'line',
    data: {
      // Blade directive 'json' converts Laravel array to Javascript array
      labels: labels,
      datasets: [{
        borderColor: colors,
        fill: false,
        lineTension: 0,
        data: data
      }]
    }
  });
}

;

if (typeof noOfHomeworksTimeLabels !== 'undefined' && typeof noOfHomeworksTimeData !== 'undefined') {
  var ctx = document.getElementById('noOfHomeworkTime').getContext('2d');
  var labels = noOfHomeworksTimeLabels;
  var data = noOfHomeworksTimeData;
  var chart = new Chart(ctx, {
    type: 'line',
    data: {
      // Blade directive 'json' converts Laravel array to Javascript array
      labels: labels,
      datasets: [{
        borderColor: colors,
        fill: false,
        lineTension: 0,
        data: data
      }]
    }
  });
}

if (typeof homeworkMarksTimeLabels !== 'undefined' && typeof homeworkMarksTimeData !== 'undefined') {
  var ctx = document.getElementById('homeworkMarksTime').getContext('2d');
  var colors = '#198dc2';
  var data = homeworkMarksTimeData;
  var chart = new Chart(ctx, {
    // The type of chart we want to create
    type: 'line',
    // The data for our dataset
    data: {
      // Blade directive 'json' converts Laravel array to Javascript array
      labels: homeworkMarksTimeLabels,
      datasets: [{
        borderColor: colors,
        fill: false,
        lineTension: 0,
        data: data
      }]
    }
  });
}

/***/ }),

/***/ "./resources/js/classroom/datatables.js":
/*!**********************************************!*\
  !*** ./resources/js/classroom/datatables.js ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

// Reference for scrollX replacement: https://stackoverflow.com/a/61238158
var wrapperHtml = "<div style='overflow:auto; width:100%;position:relative;'></div>"; // Students

$('.studentTable').DataTable({
  // "scrollX": true,
  "initComplete": function initComplete(settings, json) {
    $(this).wrap(wrapperHtml);
  },
  "columnDefs": [{
    "orderable": false,
    "targets": 5
  }]
}); // Lessons

$('.lessonTable').DataTable({
  // "scrollX": true,
  "initComplete": function initComplete(settings, json) {
    $(this).wrap(wrapperHtml);
  },
  "columnDefs": [{
    "orderable": false,
    "targets": [0, 1, 3, 4]
  }]
});
var $invoiceTable = $('.invoiceTable');

if ($invoiceTable.length > 0) {
  // Hide footer if returns true
  var showMinimal = $invoiceTable.data('show-minimal') == '1';
  var invoiceOptions = {
    // "scrollX": true,
    "initComplete": function initComplete(settings, json) {
      $(this).wrap(wrapperHtml);
    },
    "columnDefs": [{
      "orderable": false,
      "targets": 6
    }]
  }; // Add conditional options to report options

  if (showMinimal) {
    invoiceOptions["bInfo"] = false;
    invoiceOptions["bPaginate"] = false;
    invoiceOptions['bFilter'] = false;
  } // Invoices


  $('.invoiceTable').DataTable(invoiceOptions);
}

var $reportTable = $('.reportTable');

if ($reportTable.length > 0) {
  // Hide footer if returns true
  var showMinimal = $reportTable.data('show-minimal') == '1'; // Declare report table options

  var reportOptions = {
    // "scrollX": true,
    "initComplete": function initComplete(settings, json) {
      $(this).wrap(wrapperHtml);
    },
    "columnDefs": [{
      "orderable": false,
      "targets": 5
    }]
  }; // Add conditional options to report options

  if (showMinimal) {
    reportOptions["bInfo"] = false;
    reportOptions["bPaginate"] = false;
    reportOptions['bFilter'] = false;
  } // Reports


  $('.reportTable').DataTable(reportOptions);
}

/***/ }),

/***/ "./resources/js/classroom/invoice.js":
/*!*******************************************!*\
  !*** ./resources/js/classroom/invoice.js ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

var $form = $('#teacherCreateInvoiceForm');
var $lessonTable = $form.find('.lessonTable');

function updateLessonInvoiceItem() {
  var totalDurationInHours = 0; // Loop through CHECKED lesson checkbox and add duration to totalDurationInHours

  $lessonTable.find('.lessonCheckbox:checked').each(function () {
    var durationInHours = parseFloat($(this).parent().siblings('.durationInHoursData').text());
    totalDurationInHours += durationInHours;
  });
  totalDurationInHours = Math.round(totalDurationInHours); // Update Lesson Invoice Item Qty field

  $('.lessonInvoiceItemRow [name=no_of_units]').val(totalDurationInHours);
}

function updateTotal() {
  $form.find('.lessonInvoiceItemRow, .invoiceItemRow').each(function () {
    var $noOfUnitsInput = $(this).find('[name=no_of_units]');
    var $pricePerUnitInput = $(this).find('[name=price_per_unit]');
    var $itemTotalPrice = $(this).find('.itemTotalPrice');
    var noOfUnits = parseInt($noOfUnitsInput.val() || 0);
    var pricePerUnit = parseFloat($pricePerUnitInput.val() || 0);
    var itemtotalPrice = noOfUnits * pricePerUnit;
    $itemTotalPrice.text(itemtotalPrice.toFixed(2));
  });
}

function updateDom() {
  updateLessonInvoiceItem();
  updateTotal();
}

$form.find('select[name=student]').change(function (e) {
  $.ajax({
    type: "post",
    url: "/s/parent",
    data: {
      student: e.target.value
    },
    success: function success(response) {
      if (response) {
        $('[name=parent_email]').val(response.email);
      } else {
        $('[name=parent_email]').val('No Parent found');
      }
    }
  });
  $.ajax({
    type: "post",
    url: "/classroom/t/s/unpaid_lessons",
    data: {
      student: e.target.value
    },
    success: function success(response) {
      var lessons = response;
      $lessonTable.find('tbody').html('');
      lessons.forEach(function (lesson) {
        $newLessonRow = $('.lessonRowHelper').clone();
        $newLessonRow.toggleClass('lessonRowHelper lessonRow');
        $('.selectAllCheckbox').prop('checked', false);
        $newLessonRow.find('.lessonCheckbox').val(lesson.id);
        $newLessonRow.find('.lesson_name').text(lesson.name);
        $newLessonRow.find('.lesson_start_at').text(lesson.start_date);
        $newLessonRow.find('.lesson_duration_in_hours').text(lesson.duration_in_hours);
        $lessonTable.find('tbody').append($newLessonRow);
      });
      updateDom();
    }
  });
});
$form.find('.selectAllCheckbox').change(function (e) {
  var table = $(this).closest('table');

  if ($(this).is(':checked')) {
    table.find('.lessonCheckbox').prop('checked', true);
  } else {
    table.find('.lessonCheckbox').prop('checked', false);
  }

  updateDom();
});
$form.find('.lessonTable').on('change', '.lessonCheckbox', function () {
  var $table = $(this).closest('table'); // Boolean for ALL lesson checkboxes are checked

  var allChecked = $table.find('.lessonCheckbox:checked').length == $table.find('.lessonCheckbox').length;

  if (allChecked) {
    $('.selectAllCheckbox').prop('checked', true);
  } else {
    $('.selectAllCheckbox').prop('checked', false);
  }

  updateDom();
});
$form.find('[name=due_in_days]').change(function () {
  var val = parseInt($(this).val());
  $(this).val(val);
});
$form.find('.addItemButton').click(function (e) {
  var maxNoOfItems = 5;

  if ($('.lessonInvoiceItemRow, .invoiceItemRow').length <= maxNoOfItems) {
    var innerHtml = $('.invoiceItemRowHelper').html(); // Create new DOM object with constructed HTML string

    $('.invoiceItemTable tbody').append('<tr class="invoiceItemRow">' + innerHtml + '</tr>');
  } else {
    if ($('.invoiceItemTableContainer .error-message').length == 0) {
      $('.invoiceItemTable').after(' <div class="error-message">Maximum ' + strval(maxNoOfItems) + ' items can be included in an invoice.</div> ');
    }
  }
});
$form.find('.invoiceItemTable').on('click', '.deleteItemButton', function (e) {
  e.preventDefault();
  var $row = $(this).parents('tr');
  $row.remove();
  $('.invoiceItemTableContainer .error-message').remove();
});
$form.find('.invoiceItemTable').on('change', 'input[name=price_per_unit]', function (e) {
  var _float = parseFloat($(this).val());

  var roundedFloat = Math.round(_float * 100) / 100; // Ensure that price input is 2 decimal places

  $(this).val(roundedFloat.toFixed(2));
  updateDom();
});
$form.find('.invoiceItemTable').on('change', 'input[name=no_of_units]', function (e) {
  updateDom();
});
$form.find('.invoiceItemTable').on('change', 'input', function (e) {
  if ($.trim($(this).val()) !== '') {
    if ($(this).siblings('.error-message').length !== 0) {
      $(this).siblings('.error-message').remove();
    }
  }
});

function validateForm() {
  var lessonChecked = $('.lessonTable .lessonCheckbox:checked').length;
  var $invoiceItemRows = $form.find('.invoiceItemRow');

  if (!lessonChecked) {
    // No lesson and no item
    if ($invoiceItemRows.length == 0) {
      if (!$('.invoiceItemTable').next().hasClass('error-message')) {
        $('.invoiceItemTable').after(' <div class="error-message">Please fill up at least one invoice item!</div> ');
      }
    } else {
      // No lesson with item
      if ($('.invoiceItemTable').next().hasClass('error-message')) {
        $('.invoiceItemTable').next().remove();
      }
    }
  }

  $invoiceItemRows.each(function () {
    $(this).find('input').each(function () {
      if ($.trim($(this).val()) == '') {
        if ($(this).siblings('.error-message').length == 0) {
          $(this).after('<div class="error-message">Required</div>');
        }
      }
    });
  });
}

$form.find('[type=submit]').click(function (e) {
  e.preventDefault();
  $(this).attr('disabled', true); // Validate all input fields

  validateForm();
  var invoiceItemData = [];

  if ($form.find('.invoiceItemTableContainer .error-message').length == 0) {
    // Lesson Invoice Item
    $('.lessonTable .lessonCheckbox:checked').each(function () {
      var invoiceItem = {}; // Add Lesson Invoice Item to invoiceItem array

      invoiceItem.lesson_id = parseInt($(this).val());
      invoiceItem.title = "Lesson(per hour)";
      invoiceItem.no_of_units = 1;
      invoiceItem.price_per_unit = parseInt($('.lessonInvoiceItemRow [name=price_per_unit]').val());
      invoiceItemData.push(invoiceItem);
    }); // Invoice Item

    $('.invoiceItemRow').each(function () {
      var invoiceItem = {}; // Add Invoice Item to invoiceItem array

      invoiceItem.lesson_id = null;
      invoiceItem.title = $(this).find('[name=invoice_item_title]').val();
      invoiceItem.no_of_units = parseInt($(this).find('[name=no_of_units]').val());
      invoiceItem.price_per_unit = parseInt($(this).find('[name=price_per_unit]').val());
      invoiceItemData.push(invoiceItem);
    }); // Submit teacher invoice items form field only if prescription Data is filled

    if (Array.isArray(invoiceItemData) && invoiceItemData.length) {
      $('[name=teacher_invoice_items]').val(JSON.stringify(invoiceItemData));
    }

    $(this).closest('form').submit();
  } else {
    $(this).attr('disabled', false);
    toastr.info('Please fill up the required fields before proceeding.');
  }
});

/***/ }),

/***/ "./resources/js/classroom/lesson.js":
/*!******************************************!*\
  !*** ./resources/js/classroom/lesson.js ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

var $form = $('#createLessonForm'); // Variables to configure datepicker ranges

var minDate = moment();
var maxDate = minDate.clone();
maxDate.add(14, 'day');
var $startTimeDtPicker = $form.find('#startTimeDtPicker');
var $endTimeDtPicker = $form.find('#endTimeDtPicker'); // Initialise datepickers

$startTimeDtPicker.datetimepicker({
  useCurrent: false,
  format: 'DD/MM/YYYY hh:mm A',
  minDate: minDate,
  maxDate: maxDate
});
$endTimeDtPicker.datetimepicker({
  useCurrent: false,
  format: 'DD/MM/YYYY hh:mm A',
  minDate: minDate,
  maxDate: maxDate
}); // Ensure that endtime > starttime

$startTimeDtPicker.on("change.datetimepicker", function (e) {
  $endTimeDtPicker.datetimepicker('minDate', e.date);
});
$endTimeDtPicker.on("change.datetimepicker", function (e) {
  $startTimeDtPicker.datetimepicker('maxDate', e.date);
});

/***/ }),

/***/ "./resources/js/classroom/quill.js":
/*!*****************************************!*\
  !*** ./resources/js/classroom/quill.js ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

// Text Editor JS
if ($('.editor')[0]) {
  // Declare DOMs
  var $editor = $('.editor')[0];
  var $input = $('input[data-editor-input=true]');
  var inputRequired = $input.prop('required');
  var $form = $input.closest('form');
  var $submitButton = $form.find('[type=submit]');
  var editorLabel = $input.attr('name'); // Toolbar

  var toolbarOptions = [[{
    'header': [3, 4, false]
  }], ['bold', 'italic', 'underline'], [{
    'list': 'ordered'
  }, {
    'list': 'bullet'
  }], [{
    'color': []
  }, {
    'background': []
  }], ['link']];
  var quill = new Quill('.editor', {
    modules: {
      toolbar: toolbarOptions,
      clipboard: {
        matchVisual: false,
        matchSpacing: false
      }
    },
    theme: 'snow'
  });
  console.log(quill);
  window.quill = quill; // Fill Text editor with existing data if any

  quill.root.innerHTML = $input.val();
  quill.on('text-change', function () {
    $input.val(quill.root.innerHTML);
  });
  $submitButton.click(function (e) {
    e.preventDefault();
    var $t = $(this);
    var initHtml = $t.html();
    window.showLoader($t, 'in-button', 'white');
    var quillText = quill.getText(); //remove white spaces and line breaks

    quillText = quillText.trim(); // Get HTML in Quill editor

    var editorHTML = $('.ql-editor').html(); // Trim all UNNECESSARY empty paragraphs("<p><br></p>") from Quill editor

    var lenOfEmptyParagraph = '<p><br></p>'.length; // Trim empty paragraphs at start

    while (editorHTML.startsWith('<p><br></p>')) {
      editorHTML = editorHTML.substr(lenOfEmptyParagraph);
    } // Trim empty paragraphs at end


    while (editorHTML.endsWith('<p><br></p>')) {
      editorHTML = editorHTML.substr(0, editorHTML.length - lenOfEmptyParagraph);
    } // Remove remaining duplicate empty paragraphs


    while (editorHTML.includes('<p><br></p><p><br></p>')) {
      editorHTML = editorHTML.replace('<p><br></p><p><br></p>', '<p><br></p>');
    }

    if (editorHTML == '<p><br><p>') {
      editorHTML = '';
    } // Throw alert if input is required but text editor is empty


    if (inputRequired && editorHTML == '') {
      //Throw an alert if quill input is empty
      window.hideLoader($t, initHtml);
      alert('Please fill in the ' + editorLabel + ' field!');
      return;
    } else {
      // Update editor HTML to input
      $input.val(editorHTML);
      $form.submit();
    }
  });
}

/***/ }),

/***/ "./resources/js/classroom/report.js":
/*!******************************************!*\
  !*** ./resources/js/classroom/report.js ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

var $form = $('#teacherCreateReportForm');
var $lessonTable = $form.find('.lessonTable');

function updateLessonTable(lessons) {
  $lessonTable.find('tbody').html('');

  if (lessons.length > 0) {
    lessons.forEach(function (lesson) {
      $newLessonRow = $('#teacherCreateReportForm .lessonRowHelper').clone();
      $newLessonRow.toggleClass('lessonRowHelper lessonRow');
      $('.selectAllCheckbox').prop('checked', false);
      $newLessonRow.find('.lessonCheckbox').val(lesson.id);
      $newLessonRow.find('.lesson_name').text(lesson.name);
      $newLessonRow.find('.lesson_start_at').text(lesson.start_date);
      $newLessonRow.find('.lesson_resource_post_count').text(lesson.resource_post_count);
      $newLessonRow.find('.lesson_homework_count').text(lesson.homework_count);
      $lessonTable.find('tbody').append($newLessonRow);
    });
  } else {
    $newLessonRow = $('.noLessonRow').clone();
    $lessonTable.find('tbody').append($newLessonRow);
  }
}

var $startDateDtPicker = $form.find('#startDateDtPicker');
var $endDateDtPicker = $form.find('#endDateDtPicker');
$startDateDtPicker.datetimepicker({
  defaultDate: false,
  maxDate: new Date(),
  format: 'DD/MM/YYYY',
  useCurrent: false
});
$endDateDtPicker.datetimepicker({
  defaultDate: false,
  maxDate: new Date(),
  format: 'DD/MM/YYYY',
  useCurrent: false
});
$startDateDtPicker.on("change.datetimepicker", function (e) {
  $endDateDtPicker.datetimepicker('minDate', e.date);
});
$endDateDtPicker.on("change.datetimepicker", function (e) {
  $startDateDtPicker.datetimepicker('maxDate', e.date);
});
var parentInput = $('[name=parent]');
var parentDisplayInput = $('[name=parent_display]');
$('#teacherCreateReportForm select[name=student]').change(function (e) {
  e.preventDefault();
  var student_id = $(this).val();
  $.ajax({
    type: "post",
    url: '/s/parent',
    data: {
      student: student_id
    },
    success: function success(response) {
      if (response) {
        $('[name=parent_email]').val(response.email);
      } else {
        $('[name=parent_email]').val('No Parent found');
      }
    }
  }); // Fill lesson table

  $.ajax({
    type: 'post',
    url: '/classroom/t/s/unreported_lessons',
    data: {
      student: student_id
    },
    success: function success(response) {
      updateLessonTable(response);
    }
  });
});
$lessonTable.on('change', '.selectAllCheckbox', function (e) {
  var table = $(this).closest('table');
  table.find('.lessonCheckbox').prop('checked', $(this).is(':checked'));
});
$lessonTable.on('change', '.lessonCheckbox', function (e) {
  var table = $(this).closest('table'); // Boolean for ALL lesson checkboxes are checked

  var allChecked = table.find('.lessonCheckbox:checked').length == table.find('.lessonCheckbox').length;
  table.find('.selectAllCheckbox').prop('checked', allChecked);
});
$('#teacherCreateReportForm [type=submit]').click(function (e) {
  e.preventDefault();
  var reportItemData = []; // Lesson Invoice Item

  $('.lessonTable .lessonCheckbox:checked').each(function () {
    var reportItem = {}; // Add Lesson Invoice Item to invoiceItem array

    reportItem.lesson_id = parseInt($(this).val());
    reportItemData.push(reportItem);
  }); // Submit teacher invoice items form field only if prescription Data is filled

  if (Array.isArray(reportItemData) && reportItemData.length) {
    $('[name=report_items]').val(JSON.stringify(reportItemData));
  }

  $(this).closest('form').submit();
});

/***/ }),

/***/ "./resources/js/classroom/selectize.js":
/*!*********************************************!*\
  !*** ./resources/js/classroom/selectize.js ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

var REGEX_EMAIL = '([a-z0-9!#$%&\'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&\'*+/=?^_`{|}~-]+)*@' + '(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)';
$('.multiple-email-input').selectize({
  plugins: ['restore_on_backspace', 'remove_button'],
  persi: false,
  maxItems: null,
  valueField: 'email',
  render: {
    item: function item(_item, escape) {
      return '<div>' + (_item.email ? '<span class="email">' + escape(_item.email) + '</span>' : '') + '</div>';
    },
    option: function option(item, escape) {
      var label = item.name || item.email;
      var caption = item.name ? item.email : null;
      return '<div>' + (caption ? '<span class="caption">' + escape(caption) + '</span>' : '') + '</div>';
    }
  },
  createFilter: function createFilter(input) {
    var match, regex; // email@address.com

    regex = new RegExp('^' + REGEX_EMAIL + '$', 'i');
    match = input.match(regex);
    if (match) return !this.options.hasOwnProperty(match[0]);
    return false;
  },
  create: function create(input) {
    if (new RegExp('^' + REGEX_EMAIL + '$', 'i').test(input)) {
      return {
        email: input
      };
    }

    var match = input.match(new RegExp('^([^<]*)\<' + REGEX_EMAIL + '\>$', 'i'));

    if (match) {
      return {
        email: match[2]
      };
    }

    alert('Invalid email address.');
    return false;
  }
});

/***/ }),

/***/ "./resources/js/classroom/tinymce.js":
/*!*******************************************!*\
  !*** ./resources/js/classroom/tinymce.js ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

// Reference: https://www.tiny.cloud/docs/plugins/image/
function image_upload_handler(blobInfo, success, failure, progress) {
  var xhr, formData;
  xhr = new XMLHttpRequest();
  xhr.withCredentials = false;
  xhr.open('POST', '/editor/upload');

  xhr.upload.onprogress = function (e) {
    progress(e.loaded / e.total * 100);
  };

  xhr.onload = function () {
    var json;

    if (xhr.status === 403) {
      failure('HTTP Error: ' + xhr.status, {
        remove: true
      });
      return;
    }

    if (xhr.status < 200 || xhr.status >= 300) {
      failure('HTTP Error: ' + xhr.status);
      return;
    }

    json = JSON.parse(xhr.responseText);

    if (!json || typeof json.location != 'string') {
      failure('Invalid JSON: ' + xhr.responseText);
      return;
    }

    success(json.location);
  };

  xhr.onerror = function () {
    failure('Image upload failed due to a XHR Transport error. Code: ' + xhr.status);
  };

  formData = new FormData();
  formData.append('file', blobInfo.blob(), blobInfo.filename());
  xhr.send(formData);
}

;
tinymce.init({
  selector: 'textarea.tinymce-editor',
  height: 250,
  block_formats: 'Header 3=h3; Header 4=h4; Paragraph=p;',
  content_css: '/css/app.css',
  content_style: "@import url('https://fonts.googleapis.com/css?family=Poppins:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i'); html,body {background-color: white;}",
  menubar: false,
  statusbar: false,
  body_class: 'tinymce-editor-contents',
  default_link_target: '_blank',
  link_default_protocol: 'https',
  remove_linebreaks: true,
  setup: function setup(editor) {
    editor.on('init', function (e) {
      $('.loading-icon').hide();
    });
  },
  max_height: 900,
  table_toolbar: "tableprops tabledelete | tableinsertrowbefore tableinsertrowafter tabledeleterow | tableinsertcolbefore tableinsertcolafter tabledeletecol tabledeletecol",
  plugins: ['advlist autoresize autolink lists link image charmap print preview anchor', 'searchreplace visualblocks code fullscreen', 'insertdatetime media table paste code wordcount'],
  toolbar: 'formatselect | ' + 'bold italic underline | alignleft aligncenter ' + 'alignright alignjustify | table link image | bullist numlist | ' + 'forecolor backcolor | removeformat',
  images_upload_handler: image_upload_handler
});

/***/ }),

/***/ 3:
/*!*****************************************!*\
  !*** multi ./resources/js/classroom.js ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\laragon\www\spmboard\resources\js\classroom.js */"./resources/js/classroom.js");


/***/ })

/******/ });