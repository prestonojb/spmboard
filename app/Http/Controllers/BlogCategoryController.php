<?php

namespace App\Http\Controllers;

use App\Blog;
use App\BlogCategory;
use Illuminate\Http\Request;

class BlogCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show(BlogCategory $blogCategory)
    {
        $blogs = Blog::where('blog_category_id', $blogCategory->id)->paginate(10);
        $blog_categories = BlogCategory::all();
        return view('blog.index', ['blogs' => $blogs, 'blog_categories' => $blog_categories]);
    }
}
