<?php

namespace App\Console\Commands;

use App\Mail\LoginReminder;
use App\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class SendLoginReminderEmail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'login_reminder:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send login reminder to users on login day 1,3,6,13 who has not logged in today.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $reward = [10, 20, 30, 40, 50, 50, 'Free Online Class'];

        // $users = User::whereIn('consecutive_login_days', [1, 3, 6, 13])->get();

        $users = User::all();
        foreach($users as $user) {
            if( !is_null($user->last_online_at) && $user->last_online_at->isYesterday() ) {
                Mail::to( $user->email )->send(new LoginReminder( $user, $reward ));
            }
        }
    }
}
