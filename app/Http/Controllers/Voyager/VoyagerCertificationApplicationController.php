<?php

namespace App\Http\Controllers\Voyager;

use App\CertificationApplication;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;
use \TCG\Voyager\Http\Controllers\VoyagerBaseController as VoyagerBaseController;
use App\Notifications\UserCertified;
use App\Notifications\UserCertificationDeclined;

class VoyagerCertificationApplicationController extends VoyagerBaseController
{
    public function index(Request $request)
    {
        $applications = CertificationApplication::orderByDesc('created_at')->paginate(10);
        return view('vendor.voyager.certification_applications.index', compact('applications'));
    }

    public function approve(Request $request, $id)
    {
        // Validation
        request()->validate([
            'admin_comment' => 'nullable',
        ]);
        $application = CertificationApplication::find($id);

        // Update application
        $application->update([
            'status' => 'approved',
            'admin_comment' => request('admin_comment'),
        ]);

        // Certify teacher
        $application->user()->update([
            'is_certified' => 1,
        ]);

        // Notification
        $application->user->notify(new UserCertified($application));

        return redirect()->route('voyager.certification-applications.index')->with('success', 'Application approved!');
    }

    public function decline(Request $request, $id)
    {
        // Validation
        request()->validate([
            'admin_comment' => 'nullable',
        ]);
        $application = CertificationApplication::find($id);
        // Update application
        $application->update([
            'status' => 'declined',
            'admin_comment' => request('admin_comment'),
        ]);

        // Notification
        $application->user->notify(new UserCertificationDeclined($application));

        return redirect()->route('voyager.certification-applications.index')->with('success', 'Application declined!');
    }
}
