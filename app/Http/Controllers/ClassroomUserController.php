<?php

namespace App\Http\Controllers;

use App\ExamBoard;
use App\Group;
use App\State;
use Illuminate\Http\Request;

class ClassroomUserController extends Controller
{
    /**
     * Controller functions for users in Board Classroom
     */

    /**
     * Classroom User settings
     */
    public function getSettingsView()
    {
        return view('classroom.settings.menu');
    }
}
