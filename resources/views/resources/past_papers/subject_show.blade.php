@extends('resources.layouts.app')
@section('meta')
    <title>{{ $subject->name }} - Past Papers | {{ $exam_board->name }} {{ config('app.name') }} </title>
@endsection

@section('header')
    @component('resources.common.header', ['type' => 'past_paper', 'exam_board' => $exam_board])
    @endcomponent
@endsection

@section('breadcrumbs')
    @component('resources.common.breadcrumbs')
        <a href="{{ route('past_papers.home', $exam_board->slug) }}">Home</a> /
        {{ $subject->name }}
    @endcomponent
@endsection

@section('context_panel')
    @component('resources.common.context_panel', ['exam_board' => $exam_board->name, 'title' => $subject->name, 'description' => $subject->description])
        <h3 class="font-size3">
            Part of <a href="{{ route('past_papers.home', [$subject->exam_board->slug]) }}" class="font-size-inherit blue-underline">{{ $subject->exam_board->name }}</a>
        </h3>
    @endcomponent
@endsection

@if($subject->hasPastPapers())
    @section('side')
        @include('resources.past_papers.common.filter')
    @endsection

    @section('main')
        <div class="tab-content">
            <div class="tab-pane fade show active" role="tabpanel" aria-labelledby="available-tab">
                @if($past_papers->count() > 0)

                    <div class="resource__results-count mt-2">Showing {{ $past_papers->firstItem() }} - {{ $past_papers->lastItem() }} of {{ $past_papers->total() }} results</div>

                    @component('resources.past_papers.common.cluster', ['exam_board' => $exam_board, 'past_papers' => $past_papers])
                    @endcomponent

                    {{ $past_papers->appends(request()->query())->links() }}

                @else
                    <div class="resource__empty">
                        <img src="{{ asset('img/essential/empty.svg') }}" class="mb-2" width="150">
                        <h4>No papers yet</h4>
                        <h5>Stay tuned for more.</h5>
                    </div>
                @endif

            </div>
        </div>
    @endsection
@else
    @section('body')
        <div class="text-center p-5">
            <img src="{{ asset('img/essential/empty.svg') }}" alt="" width="400" height="400" class="d-inline-block mb-2">
            <p class="gray2">No past papers available for this chapter yet</p>
        </div>
    @endsection
@endif
