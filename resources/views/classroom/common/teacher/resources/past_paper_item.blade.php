<div class="resource-list-item"  data-toggle="collapse" href="#past_paper_{{ $past_paper->id }}" role="button">
    <div class="d-flex justify-content-start align-items-center">
        <div class="title">{{ $past_paper->name }}</div>
    </div>
    <span class="">
        <i class="fas fa-angle-down"></i>
    </span>
    {{-- <a href="{{ route('notes.show', [$past_papers->subject->exam_board->slug, $past_papers->id]) }}" class="button--primary sm" target="_blank">View</a> --}}
</div>

<div class="collapse ml-2" id="past_paper_{{ $past_paper->id }}">
    @foreach(['question', 'answer'] as $type)
        <div class="resource-list-item" data-type="{{ $type }}" data-resource-type="Past Paper ({{ ucfirst($type) }})" data-resource-id="{{ $past_paper->resource->id }}" data-resource-name="{{ $past_paper->name }}">
            <div class="d-flex justify-content-start align-items-center">
                <input type="checkbox" class="checkbox-input mr-2">
                <div class="title">{{ ucfirst($type) }}</div>
            </div>

            @if( $type == 'question' )
                <a href="{{ $past_paper->qp_url }}" class="button--primary sm" target="_blank">View</a>
            @elseif ( $type == 'answer' )
                <a href="{{ $past_paper->ms_url }}" class="button--primary sm" target="_blank">View</a>
            @endif

        </div>
    @endforeach
</div>
