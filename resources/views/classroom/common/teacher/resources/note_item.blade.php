<div class="resource-list-item note-list-item" data-resource-id="{{ $note->resource->id }}" data-resource-name="{{ $note->name }}" data-resource-type="Note">
    <div class="d-flex justify-content-start align-items-center">
        <input type="checkbox" class="checkbox-input mr-2" @cannot('show', $note) disabled @endcannot>
        <div class="title">{{ $note->name }}</div>
    </div>
    <a href="@cannot('show', $note) javascript:void(0) @else {{ route('notes.show', [$note->subject->exam_board->slug, $note->id]) }} @endif" class="button--primary sm @cannot('show', $note) disabled @endcannot" @can('show', $note) target="_blank" @endcan>View</a>
</div>
