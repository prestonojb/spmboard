<ul class="d-flex justify-content-start align-items-center mb-3">
    <a class="nav-link p-2 mr-1 @if($type == 'note') active @endif" href="{{ route('notes.create', $exam_board->slug) }}">Notes</a>
    <a class="nav-link p-2 mr-1  @if($type == 'topical_past_paper') active @endif" href="{{ route('topical_past_papers.create', $exam_board->slug) }}">Topical Worksheet</a>
</ul>
