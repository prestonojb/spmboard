<div class="panel">
    <div class="d-flex justify-content-between align-items-center mb-1">
        <div>
            <span class="d-inline-block mr-1">
                <a class="username font-weight500" href="{{ $resource_review->user->show_url }}">{{ $resource_review->user->username }}</a>
                {!! $resource_review->user->user_role_icon_popover_html !!} ·
            </span>
            <span class="gray2 font-size1"> {{ $resource_review->updated_at->diffForHumans() }}</span>
        </div>
        <div class="Stars xs mb-1 mr-2" style="--rating: {{ $resource_review->rating }};"></div>
    </div>

    @if(!is_null($resource_review->comment))
        <div class="">
            {!! nl2br($resource_review->comment) !!}
        </div>
    @endif
</div>
