@extends('classroom.layouts.app')

@section('content')
@include('main.navs.header')

<div class="row">
    @foreach( $prices as $price )
        <div class="col-12 col-md-6">
            <div class="panel">

                <div class="text-center">
                    <h2 class="font-weight700">{{ $price->nickname }}</h2>
                    <h3>{{ strtoupper($price->currency). number_format(strval($price->unit_amount/100), 2).'/'.$price->recurring->interval }}</h3>
                </div>
                <ul>
                    @php
                        $descriptions = explode(',', $price->metadata->description);
                    @endphp
                    @foreach( $descriptions as $description )
                        <li>{{ $description }}</li>
                    @endforeach
                </ul>
                {{-- Dropdown --}}
                <a href="{{ route('classroom.prices.index') }}" class="button--primary w-100 subscribeNowButton" data-price="{{ $price->nickname }}" data-price-id="{{ $price->id }}" data-price-per-interval="{{ strtoupper($price->currency). number_format(strval($price->unit_amount/100), 2).'/'.$price->recurring->interval }}">
                    Subscribe Now
                </a>
            </div>
        </div>
    @endforeach

</div>


@endsection

@section('scripts')
@endsection

