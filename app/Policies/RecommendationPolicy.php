<?php

namespace App\Policies;

use App\Recommendation;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class RecommendationPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Permission to update recommendation
     */
    public function update(User $user, Recommendation $recommendation)
    {
        return $recommendation->recommended_by_user_id == $user->id;
    }
}
