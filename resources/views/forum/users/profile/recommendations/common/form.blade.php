@php
if($is_edit) {
    // Get recommendation
    $recommendation = $recommendee->getRecommendationFrom(auth()->user());

    $description = old('description') ?? $recommendation->description;
    $relationship = old('relationship') ?? $recommendation->relationship_key;
    $url = route('recommendations.update', $recommendation);
} else {
    $description = old('description');
    $relationship = old('relationship');
    $url = route('recommendations.store', $recommendee->username);
}
@endphp

<div class="modal fade" id="recommendationModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        {{-- Form --}}
        <form action="{{ $url }}" method="post">
            @csrf
            @if($is_edit) @method('PATCH') @endif

            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">{{ $is_edit ? 'Update Recommendation' : 'Give Recommendation' }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    {{-- Description --}}
                    <div class="form-field">
                        <label for="description">Description</label>
                        <textarea name="description" rows="3">{{ $description }}</textarea>
                        <p class="gray2 font-size1 mb-0">In 20 - 500 characters</p>
                        @error('description')
                            <span class="error-message">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="form-field">
                        <label for="">Relationship</label>
                        {{-- Relationships --}}
                        <select name="relationship" id="">
                            <option value="" disabled selected>Select Relationship with {{ $recommendee->name }}</option>
                            @php
                                $relationships = [
                                    'student_of' => 'Student of',
                                    'worked_with' => 'Worked with',
                                    'parent_of_student_of' => 'Parent of student of',
                                ];
                            @endphp
                            @foreach($relationships as $key=>$value)
                                <option value="{{ $key }}" @if($relationship == $key) selected @endif>{{ $value }} {{ $recommendee->name }}</option>
                            @endforeach
                        </select>
                        @error('relationship')
                            <span class="error-message">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="button--primary disableOnSubmitButton">{{ $is_edit ? 'Update': 'Save' }}</button>
                </div>
            </form>
        </div>
    </div>
  </div>
