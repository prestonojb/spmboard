<?php

namespace App\Http\Controllers;

use App\Homework;
use App\Lesson;
use App\Mail\HomeworkReminder;
use App\Notifications\Classroom\HomeworkGiven;
use App\Notifications\Classroom\ParentHomeworkAssigned;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Session;

class ClassroomHomeworkController extends Controller
{
    // /**
    //  * List all homeworks
    //  */
    // public function index(Lesson $lesson)
    // {
    //     $classroom = $lesson->classroom;
    //     $homeworks = Homework::all();
    //     $chapters = $classroom->subject->chapters;

    //     $subject = $classroom->subject;
    //     return view('classroom.homeworks.index', compact('lesson', 'classroom', 'subject', 'homeworks', 'chapters'));
    // }

    /**
     * Show homework
     */
    public function show(Lesson $lesson, Homework $homework)
    {
        $classroom = $lesson->classroom;
        $submissions = $homework->submissions;
        return view('classroom.homeworks.show', compact('lesson', 'classroom', 'homework', 'submissions'));
    }

    /**
     * Create homework
     */
    public function create(Lesson $lesson)
    {
        $this->authorize('update', $lesson);

        $classroom = $lesson->classroom;
        $subject = $classroom->subject;
        $chapters = $subject->chapters;

        return view('classroom.homeworks.create', compact('lesson', 'classroom', 'subject', 'chapters'));
    }

    /**
     * Store homework
     */
    public function store(Lesson $lesson)
    {
        $this->authorize('update', $lesson);
        request()->validate([
            'title' => 'required|string',
            'instruction' => 'nullable',
            'due_at' => 'nullable',
            'max_marks' => 'nullable|integer|between:1,500',
        ]);

        $classroom = $lesson->classroom;

        // Decode resource JSON
        $resource_data = json_decode( request('resource_data') ) ?? [];
        $custom_resource_data = json_decode( request('custom_resource_data') ) ?? [];

        // Create Homework
        $homework = $lesson->homeworks()->create([
                        'title' => request('title'),
                        'instruction' => request('instruction'),
                        'due_at' => !is_null(request('due_at')) ? Carbon::createFromFormat('d/m/Y g:i A', request('due_at')) : null,
                        'max_marks' => request('max_marks'),
                    ]);

        // Attach resources to homework
        $homework->storeResources( $resource_data, $custom_resource_data );
        $notify_parents = isset(request()->notify_parents) && request('notify_parents');

        // Homework Given Notification
        foreach( $classroom->students as $student ) {
            // Notify student
            $student->notify( new HomeworkGiven( $student, $homework ) );
            // Notify student's parent if any
            if( $notify_parents && $student->parent ){
                $student->parent->notify( new HomeworkGiven( $student, $homework ) );
            }
        }

        Session::flash('success', 'Homework given successfully!');
        return redirect()->route('classroom.lessons.homeworks.show', [$lesson, $homework->id]);
    }

    /**
     * Edit Homework
     */
    public function edit(Lesson $lesson, Homework $homework)
    {
        $this->authorize('update', $homework);

        $classroom = $lesson->classroom;
        $subject = $classroom->subject;
        $chapters = $subject->chapters;
        return view('classroom.homeworks.edit', compact('lesson', 'homework', 'classroom', 'subject', 'chapters'));
    }

    /**
     * Update Homework
     */
    public function update(Lesson $lesson, Homework $homework)
    {
        $this->authorize('update', $homework);
        request()->validate([
            'title' => 'required',
            'instruction' => 'nullable',
            'due_at' => 'nullable',
            'chapter' => 'nullable'
        ]);

        // Update homework
        $homework->update([
            'title' => request()->title,
            'instruction' => request()->instruction,
            'chapter_id' => request()->chapter,
        ]);

        Session::flash( 'success', 'Homework updated successfully!' );
        return redirect()->route('classroom.lessons.homeworks.show', [$lesson, $homework->id]);
    }

    /**
     * Delete homework
     */
    public function delete(Lesson $lesson, Homework $homework)
    {
        $this->authorize('delete', $homework);

        $lesson = $homework->lesson;


        // Delete row
        $homework->cleanDelete();

        Session::flash( 'error', 'Homework deleted successfully!' );
        return redirect()->route('classroom.lessons.show', [$lesson->classroom->id, $lesson->id]);
    }

    // /**
    //  * Send Homework Reminder
    //  */
    // public function send_reminder(Lesson $lesson, Homework $homework)
    // {
    //     $this->authorize('update', $lesson);

    //     $classroom = $lesson->classroom;
    //     $teacher = auth()->user()->teacher;
    //     $students = $classroom->students;

    //     foreach( $students as $student ) {
    //         // Send homework reminder to student via email
    //         Mail::to( $student->email )->send( new HomeworkReminder( $teacher, $student, $classroom, $homework ) );
    //     }

    //     return redirect()->back()->withSuccess('Homework reminder sent to students successfully!');
    // }
}
