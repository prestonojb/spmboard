<div class="gray2 font-size2">
    @auth
        @if($resource->owner()->id == auth()->user()->id)
            <b>{{ $resource->getCoinsMade() }} coins made </b> <button class="p-0 color-inherit" role="button" data-toggle="popover" data-trigger="focus" data-content="Private to you"><i class="fa fa-info-circle" aria-hidden="true"></i></button> ·
        @endif
    @endauth
    <b>{{ views($resource)->count() }} views</b>
    @if($resource->isPaid()) · <b>{{ $resource->getNoOfUnlocks() }} unlocks</b> @endif
    · Updated {{ $resource->updated_at->diffForHumans() }}
</div>
