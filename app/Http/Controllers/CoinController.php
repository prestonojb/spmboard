<?php

namespace App\Http\Controllers;

use App\CoinAction;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Carbon\CarbonPeriod;

class CoinController extends Controller
{
    public function show()
    {
        $user = auth()->user();
        if(!$user->hasCoins()) {
            abort(404);
        }

        $withdrawal_requests = $user->withdrawal_requests()->latest()->get();

        $coin_actions = $user->coin_actions()->latest()->limit(15)->get();

        return view('users.coins.show', compact('user', 'withdrawal_requests', 'coin_actions'));
    }
}
