@extends('classroom.layouts.app')

@section('meta')
    <title>Homework | {{ config('app.name') }} Classroom</title>
@endsection

@section('styles')
@endsection

@section('content')
@component('classroom.common.breadcrumbs')
    Homework
@endcomponent

@component('classroom.common.page_heading', ['title' => 'Homework', 'button' => 'Give homework' ])
    @slot('button_url')
        {{ route('classroom.lessons.homeworks.create', $classroom->id) }}
    @endslot
@endcomponent

<div class="row">
    @foreach($homeworks as $homework)
        <div class="col-12 col-lg-6">
            @component('classroom.homeworks.common.block', ['classroom' => $classroom, 'homework' => $homework, 'footer' => true])
            @endcomponent
        </div>
    @endforeach
</div>


@endsection

@section('scripts')
@endsection
