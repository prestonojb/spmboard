<?php

namespace App\Http\Controllers;

use App\State;
use App\Group;
use App\ExamBoard;
use Illuminate\Http\Request;

class SettingsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function profile()
    {
        $states = State::all();
        $exam_boards = ExamBoard::all();
        return view('forum.settings.profile', compact('states', 'exam_boards'));
    }

    public function password()
    {
        return view('forum.settings.password');
    }

    public function account()
    {
        return view('forum.settings.account');
    }
}
