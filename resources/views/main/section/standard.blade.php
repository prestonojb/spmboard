<section class="@if(isset($light_bg) && $light_bg) bg-light @endif py-5">
    <div class="container text-center">
        <div class="mb-3 mb-md-4">
            <h4 class="font-size10"><b>{{ $title }}</b></h4>
            @if(isset($description) && !is_null($description))
                <h5 class="font-size4 gray2">
                    {!! $description !!}
                </h5>
            @endif
        </div>
    </div>
    {{ $slot }}
</section>
