// Reference: https://stackoverflow.com/questions/10730362/get-cookie-by-name
function getCookie(name) {
    const value = `; ${document.cookie}`;
    const parts = value.split(`; ${name}=`);
    if (parts.length === 2) return parts.pop().split(';').shift();
}

// Clone and show smart bar
$smartBarContainer = $('.smart-bar-container.mathBoardQuiz').clone().removeClass('d-none');
$smartBarContainer.prependTo( $('main') );

if(!loggedIn) {
    // Show smart bar if user has not dismissed it
    if(!getCookie('dismissed_rp_promotion_bar')) {
        // Clone and show smart bar
        $smartBarContainer = $('.smart-bar-container.rpPromotion').clone().removeClass('d-none');
        $smartBarContainer.prependTo( $('main') );
    }

    // Show smart bar if user has not dismissed it
    if(!getCookie('dismissed_ebooks_promotion_bar')) {
        // Clone and show smart bar
        $smartBarContainer = $('.smart-bar-container.ebooksPromotion').clone().removeClass('d-none');
        $smartBarContainer.prependTo( $('main') );
    }

    // Set dismiss state as cookie and hide bar
    $('.dismissButton.rpPromotion').click(function(){
        document.cookie = "dismissed_rp_promotion_bar=true;";
        $(this).parents('.smart-bar-container').hide();
    });

    // Set dismiss state as cookie and hide bar
    $('.dismissButton.ebooksPromotion').click(function(){
        document.cookie = "dismissed_ebooks_promotion_bar=true;";
        $(this).parents('.smart-bar-container').hide();
    });
}
