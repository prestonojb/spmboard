<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use App\Chapter;
use App\Subject;
use Illuminate\Validation\Rule;
use App\TopicalPastPaper;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Illuminate\Support\Facades\Artisan;

class TopicalPastPapersImport implements ToCollection, WithHeadingRow, WithValidation, WithStartRow
{
    /**
    * @param Collection $row
    */
    public function collection(Collection $rows)
    {
        foreach($rows as $row)
        {
            $subject = Subject::find($row->get('subject_id'));
            $chapter = Chapter::find($row->get('chapter_id'));
            $exam_board = $subject->exam_board;

            $filename = $row->get('qp_filename');
            $name = substr($filename, 0, strlen($filename) - 4);

            // Create row in DB
            TopicalPastPaper::create([
                'name' => $name,
                'subject_id' => $row->get('subject_id'),
                'chapter_id' => $row->get('chapter_id'),
                'paper_number' => $row->get('paper_number'),
                'qp_url' => "topical-past-papers/{$exam_board->slug}/".$row->get('qp_filename'),
                'ms_url' => "topical-past-papers/{$exam_board->slug}/".$row->get('ms_filename'),
            ]);
        }
    }

    public function rules(): array
    {
        return [
            'subject_id' => Rule::in( Subject::all()->pluck('id')->toArray() ),
            'chapter_id' => Rule::in( Chapter::all()->pluck('id')->toArray() ),
            'paper_number' => 'required|integer|between:1,6',
            'qp_filename' => 'required|string',
            'ms_filename' => 'required|string',
        ];
    }

    /**
     * Indicate Heading row
     * 1st row is heading row
     */
    public function headingRow(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function startRow(): int
    {
        return 3;
    }
}
