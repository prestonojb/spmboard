<div class="panel p-0 user-credentials">
    <div class="panel-header">
        <h3 class="mb-0">Details</h3>
    </div>

    <div class="panel-body">
        {{-- State --}}
        <div class="user-credentials__item">
            <span class="iconify" data-icon="bx:bx-map" data-inline="false"></span>
            @if(!is_null($student->user->state))
                <div class="value">From {{ $student->user->state->name }}, Malaysia</div>
            @else
                <div class="user-credentials__empty">Not updated yet</div>
            @endif
        </div>

        {{-- School --}}
        <div class="user-credentials__item">
            <span class="iconify" data-icon="bx:bxs-school" data-inline="false"></span>
            @if(!is_null($student->school))
                <div class="value">Studies at {{ $student->school }}</div>
            @else
                <div class="user-credentials__empty">Not updated yet</div>
            @endif
        </div>
    </div>
</div>
