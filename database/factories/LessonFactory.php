<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Lesson;
use Faker\Generator as Faker;
use Carbon\Carbon;

$factory->define(Lesson::class, function (Faker $faker) {
    return [
        'classroom_id' => function() {
            return factory(Classroom::class)->create()->id;
        }, // Can  be overriden
        'name' => $faker->company,
        'description' => $faker->optional()->sentence,
        'start_at' => Carbon::now(),
        'end_at' => Carbon::now(),
    ];
});
