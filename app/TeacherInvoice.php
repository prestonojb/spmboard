<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use PDF;

class TeacherInvoice extends Model
{
    protected $guarded = [];
    protected $dates = ['due_at', 'paid_at'];

    public function lessons()
    {
        $lesson_ids = $this->items()->pluck('lesson_id')->toArray();
        $lessons = Lesson::whereIn('id', $lesson_ids)->get();
        return $lessons;
    }

    public function teacher()
    {
        return $this->belongsTo(Teacher::class, 'teacher_id', 'user_id');
    }

    public function student()
    {
        return $this->belongsTo(Student::class, 'student_id', 'user_id');
    }

    public function parent()
    {
        return $this->belongsTo(StudentParent::class, 'parent_id', 'user_id');
    }

    public function items()
    {
        return $this->hasMany(TeacherInvoiceItem::class, 'invoice_id', 'id');
    }

    public function lesson_items()
    {
        return $this->items()->whereNotNull('lesson_id')->get();
    }

    public function non_lesson_items()
    {
        return $this->items()->whereNull('lesson_id')->get();
    }

    public function getLessonQuantityAttribute()
    {
        if( $this->lesson_items()->count() ) {
            $qty = 0;
            foreach( $this->lesson_items() as $lesson_item ) {
                $qty += $lesson_item->lesson->duration_in_hours;
            }
            return round($qty);
        } else {
            return null;
        }
    }

    public function getLessonPricePerUnitAttribute()
    {
        if( $this->lesson_items()->count() ) {
            return $this->lesson_items()[0]->price_per_unit;
        } else {
            return null;
        }
    }

    public function getAmountAttribute($value)
    {
        return number_format($value, 2);
    }

    public function getUrlAttribute($value)
    {
        if($value){
            if(strpos($value, 'amazonaws')){
                //Amazon link for image
                return $value;
            } else{
                //Amazon S3
                $updated_value = str_replace('\\', '/', $value);
                return Storage::cloud()->url($updated_value);
            }
        } else {
            return null;
        }
    }

    public function getStatusAttribute()
    {
        if( !is_null($this->paid_at) ) {
            return 'paid';
        }
        if( Carbon::parse($this->due_at)->isPast() ) {
            return 'overdue';
        }
        return 'issued';
    }

    public function getStatusTagHtmlAttribute()
    {
        $dict = [
            'issued' => 'blue5',
            'overdue' => 'red',
            'paid' => 'green',
        ];
        return '<span class="status-tag '.$dict[$this->status].'">'.ucfirst($this->status).'</span>';
    }

    public function getTitleAttribute()
    {
        return $this->invoice_number;
    }

    public function getSubtitleAttribute()
    {
        return "for {$this->student->name}";
    }

    public function getPrimaryAttribute()
    {
        return "{$this->title} {$this->subtitle}";
    }

    public function getSecondaryAttribute()
    {
        return "{$this->status_tag_html}";
    }

    public function getNoOfLessonsAttribute()
    {
        return $this->lessons()->count();
    }

    public function getNonLessonItemsAttribute()
    {
        return $this->non_lesson_items();
    }

    public function generatePdf()
    {
        return PDF::loadView('classroom.pdf.invoice', ['invoice' => $this]);
    }

    public function calculateAmount()
    {
        $lesson_amount = 0;
        $non_lesson_amount = 0;

        foreach($this->items as $item) {
            if( !is_null( $item->lesson_id ) ) {
                $lesson = Lesson::find( $item->lesson_id );
                $price_per_unit = $item->price_per_unit;
                $lesson_hours = $lesson->duration_in_hours;
                $lesson_amount += $lesson_hours * $price_per_unit;
            } else {
                $non_lesson_amount += $item->no_of_units * $item->price_per_unit;
            }
        }

        return $lesson_amount + $non_lesson_amount;
    }
}
