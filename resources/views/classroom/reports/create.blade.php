@extends('classroom.layouts.app')

@section('meta')
    <title>Create Report | {{ config('app.name') }} Classroom</title>
@endsection

@section('styles')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.1.2/css/tempusdominus-bootstrap-4.min.css">
@endsection

@section('content')

@component('classroom.common.breadcrumbs')
    <a href="{{ route('classroom.landing') }}">Home</a> /
    <a href="{{ route('classroom.reports.index') }}">Reports</a> /
    Create
@endcomponent

@component('classroom.common.page_heading', ['title' => 'Create Report'])
@endcomponent

<div class="maxw-740px mx-auto my-5">
    <div class="panel">
        <form action="{{ route('classroom.reports.store') }}" method="post" id="teacherCreateReportForm">
            @csrf
            <input type="hidden" name="report_items">

            <div class="form-field">
                <label for="">Student</label>
                <select name="student" id="" required>
                    <option value="" disabled selected>Select a student</option>
                    @foreach( $students as $student )
                        <option value="{{ $student->user_id }}" @if( request()->student == $student->user_id ) selected @endif>{{ $student->name }}</option>
                    @endforeach
                </select>
            </div>

            {{-- <div class="form-field">
                <label for="">Parent's Email</label>
                <input type="email" value="" name="parent_email" readonly>
            </div> --}}

            {{-- Unreported lessons --}}
            <div class="form-field">
                <label for="">Lessons</label>
                @component('classroom.reports.common.lesson_table', ['lessons' => []])
                @endcomponent
            </div>

            <div class="form-field">
                <label for="">Remarks(Optional)</label>
                <textarea name="teacher_remark" rows="3" placeholder="Remarks"></textarea>
            </div>

            <div class="form-field">
                <label for="">Start At</label>
                <input type="text" required name="start_date" class="datetimepicker-input" id="startDateDtPicker" data-toggle="datetimepicker" data-target="#startDateDtPicker">
            </div>

            <div class="form-field">
                <label for="">End At</label>
                <input type="text" required name="end_date" class="datetimepicker-input" id="endDateDtPicker" data-toggle="datetimepicker" data-target="#endDateDtPicker">
            </div>

            <div class="text-center">
                <button class="button--primary disableOnSubmitButton" type="submit">Create</button>
            </div>
        </form>
    </div>
</div>

<div class="py-1"></div>
@endsection
