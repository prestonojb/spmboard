<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Lesson;
use App\Homework;
use App\HomeworkSubmission;
use App\User;
use Carbon\Carbon;
use Faker\Generator as Faker;

$factory->define(Homework::class, function (Faker $faker) {
    return [
        'title' => $faker->name,
        'instruction' => $faker->sentence,
        'due_at' => $faker->dateTimeBetween('now', '+2 weeks'),
        'lesson_id' => function() {
            return factory(Lesson::class)->create()->id;
        },
        'max_marks' => rand(10, 100),
    ];
});

// Create submission
$factory->afterCreatingState(Homework::class, 'with_submission', function($homework, $faker) {
    $marked_at = Carbon::now()->addDays( rand(0, 7) );
    factory(HomeworkSubmission::class)->create([
        'homework_id' => $homework->id,
        'marks' => rand(0, $homework->max_marks),
        'marked_at' => $marked_at,
        'returned_at' => $marked_at->addDays( rand(0, 7) ),
    ]);
});

// Ungraded homework
$factory->state(Homework::class, 'ungraded', function($faker){
    return [
        'max_marks' => 0,
    ];
});
