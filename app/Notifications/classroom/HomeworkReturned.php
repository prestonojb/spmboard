<?php

namespace App\Notifications\Classroom;

use App\HomeworkSubmission;
use App\Lesson;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Notification;
use Illuminate\Support\HtmlString;
use Illuminate\Notifications\Messages\MailMessage;

class HomeworkReturned extends Notification implements ShouldQueue
{
    use Queueable;

    public $lesson;
    public $homework_submission;
    public $homework;
    public $classroom;
    public $teacher;
    public $student;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Lesson $lesson, HomeworkSubmission $homework_submission)
    {
        $this->lesson = $lesson;
        $this->homework_submission = $homework_submission;
        $this->homework = $homework_submission->homework;
        $this->classroom = $lesson->classroom;
        $this->teacher = $lesson->classroom->teacher;
        $this->student = $homework_submission->student;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $lesson = $this->lesson;
        $classroom = $this->classroom;
        $teacher = $this->teacher;
        $homework_submission = $this->homework_submission;
        $homework = $this->homework;
        $student = $this->student;

        if( $notifiable->account_type == 'student' ) {
            $lineHtml = "<b>{$teacher->name}</b> returned your <b>{$homework->title}</b> homework.";
        } elseif( $notifiable->account_type == 'parent' ) {
            $lineHtml = "<b>{$teacher->name}</b> returned homework of {$student->name} entitled <b>{$homework->title}</b>.";
        }

        return (new MailMessage)
                ->line(new HtmlString($lineHtml))
                ->action('Learn More', $this->homework->url);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $lesson = $this->lesson;
        $classroom = $this->classroom;
        $teacher = $this->teacher;
        $homework_submission = $this->homework_submission;
        $homework = $this->homework;
        $student = $this->student;

        if( $notifiable->account_type == 'student' ) {
            $title = "<b>{$teacher->name}</b> returned your <b>{$homework->title}</b> homework.";
        } elseif( $notifiable->account_type == 'parent' ) {
            $title = "<b>{$teacher->name}</b> returned homework of {$student->name} entitled <b>{$homework->title}</b>.";
        }

        return [
            'type' => HomeworkSubmission::class,
            'target' => $homework_submission,
            'lesson_id' => $lesson->id,
            'title' => $title,
        ];
    }
}
