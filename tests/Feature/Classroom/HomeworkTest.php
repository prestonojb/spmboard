<?php

namespace Tests\Feature;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Tests\TestCase;
use App\Classroom;
use App\Lesson;
use App\Resource;
use App\Subject;
use App\User;
use App\Notifications\Classroom\HomeworkGiven;
use App\Homework;
use App\HomeworkSubmission;
use Illuminate\Support\Facades\Notification;
use Carbon\Carbon;

class HomeworkTest extends TestCase
{
    use WithFaker;

    private $teacher;
    private $student;
    private $parent;
    private $subject;

    public function setUp() : void
    {
        parent::setUp();

        // Subscribed Teacher
        $user = factory(User::class)->states('subscribed_teacher')->create();
        $this->subscribed_teacher = $user->teacher;

        // Unsubscribed Teacher
        $user = factory(User::class)->states('teacher')->create();
        $this->teacher = $user->teacher;

        // Student with Board Pass
        $user = factory(User::class)->states('student')->create();
        $this->student = $user->student;

        // Parent
        $user = factory(User::class)->states('parent')->create();
        $this->parent = $user->parent;

        // Subject
        $this->subject = Subject::first();
        // Classroom
        $this->classroom = factory(Classroom::class)->create([
                                'teacher_id' => $this->teacher->user_id,
                                'subject_id' => $this->subject->id,
                            ]);
        // Lesson
        $this->lesson = factory(Lesson::class)->create([
            'classroom_id' => $this->classroom->id
        ]);


        // Student joins classroom
        $this->classroom->addStudent( $this->student );
        // Parent attached to student
        $this->student->updateParent($this->parent);
    }

    /**
     * @group homework
     * Test for teacher giving homework
     */
    public function testStore()
    {
        Notification::fake();

        $init_count = Homework::count();
        // POST request to store homework
        $response = $this->actingAs($this->teacher->user)
                         ->post( route('classroom.lessons.homeworks.store', $this->lesson->id), [
                            'title' => $this->faker->word,
                            'instruction' => $this->faker->optional()->sentence,
                            'due_at' => Carbon::now()->addMonth()->format('d/m/Y g:i A'),
                            'max_marks' => 50,
                         ]);

        // New Homework added to DB
        $this->assertEquals($init_count + 1, Homework::count());

        // Redirect
        $response->assertRedirect();
    }

    /**
     * @group homework
     * Test parent is notified (user intends to) when homework is given
     */
    public function testStoreOptToNotifyParent()
    {
        Notification::fake();

        // POST request to store homework
        $this->actingAs($this->teacher->user)
            ->post( route('classroom.lessons.homeworks.store', $this->lesson->id), [
                'title' => $this->faker->word,
                'instruction' => $this->faker->optional()->sentence,
                'due_at' => Carbon::now()->addMonth()->format('d/m/Y g:i A'),
                'max_marks' => 50,
                'notify_parents' => 1,
            ]);

        // Student
        Notification::assertSentTo(
            [$this->student], HomeworkGiven::class,
            function ($notification, $channels) {
                return $notification->via($this->student) == ['mail', 'database'];
            }
        );

        // Parent
        Notification::assertSentTo(
            [$this->parent], HomeworkGiven::class,
            function ($notification, $channels) {
                return $notification->via($this->parent) == ['mail', 'database'];
            }
        );
    }

    /**
     * @group homework
     * Test parent not notified (user intends to) when homework is given
     */
    public function testStoreNotOptToNotifyParent()
    {
        Notification::fake();

        // POST request to store homework, without opting to notify parent
        $this->actingAs($this->teacher->user)
            ->post( route('classroom.lessons.homeworks.store', $this->lesson->id), [
                'title' => $this->faker->word,
                'instruction' => $this->faker->optional()->sentence,
                'due_at' => Carbon::now()->addMonth()->format('d/m/Y g:i A'),
                'max_marks' => 50,
            ]);

        // Student
        Notification::assertSentTo(
            [$this->student], HomeworkGiven::class,
            function ($notification, $channels) {
                return $notification->via($this->student) == ['mail', 'database'];
            }
        );

        // Parent
        Notification::assertNotSentTo(
            [$this->parent], HomeworkGiven::class,
            function ($notification, $channels) {
                return $notification->via($this->parent) == ['mail', 'database'];
            }
        );
    }

    /**
     * @group homework
     * Test for validation of homework marks
     * Valid Marks: null or 0 < x <= 500
     */
    public function testValidHomeworkMarks()
    {
        $marks = [null, -1, 0, 1, 499, 500, 501];

        foreach($marks as $mark) {
            // POST request to store homework
            $response = $this->actingAs($this->teacher->user)
                            ->post( route('classroom.lessons.homeworks.store', $this->lesson->id), [
                                'title' => $this->faker->word,
                                'instruction' => $this->faker->optional()->sentence,
                                'due_at' => Carbon::now()->addMonth()->format('d/m/Y g:i A'),
                                'max_marks' => $mark,
                            ]);
            if( is_null($mark) || ($mark > 0 && $mark <= 500) ) {
                $response->assertSessionHas('success');
                $response->assertRedirect();
            } else {
                $response->assertSessionHasErrors('max_marks');
            }
        }
    }

    /**
     * @group homework
     * Test delete homework
     */
    public function testDelete()
    {
        Storage::fake('s3');

        // {----------------- HOMEWORK -----------------} //
        $homework = factory(Homework::class)->create([
            'lesson_id' => $this->lesson->id
        ]);
        $homework_id = $homework->id;

        // {----------------- RESOURCE -----------------} //
        $resource = Resource::first();

        // Attach pre-built resource
        $homework->resources()->attach( $resource->id );

        // Upload custom resource
        $custom_file = UploadedFile::fake()->create('document.pdf', 4096, 'application/pdf');
        $obj = (object) [
            'file_name' => $custom_file->hashName(),
            'file_url' => 'custom_resources/'.$custom_file->hashName(),
        ];
        $custom_file->store('custom_resources/'.$custom_file->hashName(), 's3');
        $homework->storeResources([], [$obj]);


        // {---------------- SUBMISSION ----------------} //
        $submission = factory(HomeworkSubmission::class)->create([
            'homework_id' => $homework->id,
            'student_id' => $this->student->user_id,
        ]);
        $submission_id = $submission->id;

        // Save submission file
        $file = UploadedFile::fake()->create('document.pdf', 4096, 'application/pdf');
        $submission->saveFile( $file );



        // {------------------ DELETE RESPONSE ------------------} //
        $response = $this->actingAs($this->teacher->user)
                         ->delete( route('classroom.lessons.homeworks.delete', [$this->lesson->id, $homework->id]) );

        // Assert Homework deleted
        $this->assertTrue( is_null( Homework::find($homework_id)), "Homework not deleted!" );

        // Assert Submission deleted
        $this->assertTrue( is_null( HomeworkSubmission::find($submission_id)), "Homework Submission not deleted!" );

        // Assert homework pre-built resource not deleted
        $this->assertTrue( !is_null( Resource::find($resource->id) ) , "Prebuilt Resource should not be deleted!" );

        // Assert homework pre-built resource relationship detached
        $this->assertTrue( DB::table('homework_resource')->where('homework_id', $homework_id)->whereNotNull('resource_id')->count() == 0, "Homework Prebuilt Resource not detached!" );

       // Assert homework custom resource relationship detached
       $this->assertTrue( DB::table('homework_resource')->where('homework_id', $homework_id)->whereNull('resource_id')->count() == 0, "Resource Post Custom Resource not detached and deleted!" );

        /**
         * Reference: https://laracasts.com/discuss/channels/testing/storageassertmissing-failing-file-is-actually-gone-in-real-life
         * Must specify environment in destroy function() for this test to return true, ignore for now and test file deletion manually
         */
        // Storage::disk('s3')->assertMissing( 'custom_resources/'.$custom_file->hashName(), "Homework Custom Upload File not deleted from Storage!");


        // Submission deleted
        $this->assertTrue( is_null( HomeworkSubmission::find($submission_id) ), "Submission not deleted!" );
        // Storage::disk('s3')->assertMissing( 'homework/submissions/'.$file->hashName(), "Homework Submission Custom Upload File not deleted from Storage!");


        // Assert session 'error'
        $response->assertSessionHas('error');
        // Assert redirect
        $response->assertRedirect();
    }
}
