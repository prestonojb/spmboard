<div class="progress mb-1">
    @foreach($data as $datum)
        <div class="progress-bar bg-{{ $datum['bg-color'] }}" role="progressbar" style="width: {{ $homework->getSubmissionPercentageByStatus( $datum['status'] ) }}%"></div>
    @endforeach
</div>

<div class="row justify-content-center align-items-center">
    @foreach($data as $datum)
        <div class="col-6">
            <div class="d-flex justify-content-between align-items-center maxw-100px mx-auto">
                <div class="text-center">
                    <span class="{{ $datum['bg-color'] }}">■</span>
                    {{ ucfirst($datum['status']) }}
                </div>
            </div>
        </div>
    @endforeach
</div>
