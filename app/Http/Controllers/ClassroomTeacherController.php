<?php

namespace App\Http\Controllers;

use App\Student;
use Illuminate\Http\Request;

class ClassroomTeacherController extends Controller
{
    public function update()
    {
        $user = auth()->user();

        $user->update([
            'state_id' => request()->state
        ]);

        $user->teacher()->update([
            'school' => request()->school,
            'phone_number' => request()->phone_number,
            'gender' => request()->gender,
            'year_of_birth' => request()->year_of_birth,
            'qualification' => request()->qualification,
            'teaching_experience' => request()->teaching_experience,
        ]);

        return redirect()->back()->withSuccess('Profile updated successfully!');
    }

    public function getUnreportedLessons()
    {
        $teacher = auth()->user()->teacher;
        $student = Student::find( request()->student );

        if( $teacher && $student ) {
            return $teacher->getUnreportedLessons( $student );
        } else {
            return null;
        }
    }

    public function getUnpaidLessons()
    {
        $teacher = auth()->user()->teacher;
        $student = Student::find( request()->student );

        if( $teacher && $student ) {
            return $teacher->getUnpaidLessons( $student );
        } else {
            return null;
        }
    }
}
