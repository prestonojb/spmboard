<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExamBoard extends Model
{
    public function getRouteKeyName()
    {
        return 'slug';
    }

    /**
     * ---------------
     * Relationships
     * ---------------
     */
    public function users()
    {
        return $this->hasMany(User::class);
    }

    public function subjects()
    {
        return $this->hasMany(Subject::class)->orderBy('name');
    }

    public function questions()
    {
        return $this->hasManyThrough(Question::class, Subject::class);
    }

    public function notes()
    {
        return $this->hasManyThrough(Notes::class, SUbject::class);
    }

    /**
     * ---------------
     * Helper functions
     * ---------------
     */
    /**
     * Returns subjects with at least 1 exam builder item
     */
    public function getSubjectsWithItems()
    {
        return $this->subjects()->has('exam_builder_items')->get();
    }
}
