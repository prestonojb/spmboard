<?php

namespace App;

use App\Traits\StudentAnalytics;
use Carbon\Carbon;
use Deployer\Host\Storage;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use PDF;
use Exception;

class Student extends Model
{
    use StudentAnalytics;
    use Notifiable;

    protected $guarded = [];

    protected $primaryKey = 'user_id';
    public $additional_attributes = ['understanding_rating'];

    public function getRouteKeyName()
    {
        return 'user_id';
    }

    /**
     * ----------------
     * Relationships
     * ----------------
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function exam_board()
    {
        return $this->belongsTo(ExamBoard::class);
    }

    public function reports()
    {
        return $this->hasMany(StudentReport::class, 'student_id', 'user_id');
    }

    public function classrooms()
    {
        return $this->belongsToMany(Classroom::class, 'classroom_student', 'student_id', 'classroom_id');
    }

    public function homework_submissions()
    {
        return $this->hasMany(HomeworkSubmission::class, 'student_id', 'user_id');
    }

    public function invoices()
    {
        return $this->hasMany( TeacherInvoice::class, 'student_id', 'user_id' );
    }

    public function parent()
    {
        return $this->belongsTo(StudentParent::class, 'parent_id', 'user_id');
    }

    /*
     |---------------------------------
     | Accessors
     |---------------------------------
    */
    public function getAccountTypeAttribute()
    {
        return 'student';
    }

    public function getUsernameAttribute()
    {
        return $this->user->username;
    }

    public function getEmailAttribute()
    {
        return $this->user->email;
    }

    public function getNameAttribute()
    {
        return $this->user->name ?? $this->user->username;
    }

    public function getStateAttribute()
    {
        return $this->user->state;
    }

    public function getAvatarAttribute()
    {
        return $this->user->avatar;
    }

    public function getAvatarRelativweUrlAttribute()
    {
        return $this->user->avatar_relative_url;
    }

    public function getLastOnlineAtAttribute()
    {
        return $this->user->last_online_at;
    }

    public function getHeadlineAttribute()
    {
        return $this->user->headline;
    }

    public function getAboutAttribute()
    {
        return $this->user->about;
    }

    public function getAverageHomeworkMarksAttribute()
    {
        return $this->getAverageHomeworkMarks();
    }

    public function getLessonsAttribute()
    {
        return $this->lessons();
    }

    public function getClassroomPostsAttribute()
    {
        return $this->user->classroom_posts;
    }

    public function getPrimaryAttribute()
    {
        return $this->name;
    }

    public function getSecondaryAttribute()
    {
        $str = "";
        if($this->school) {
            $str .= "{$this->school}";
        }
        if($this->exam_board) {
            $str .= " · {$this->exam_board->name}";
        }
        if($this->last_online_at) {
            $str .= " ·  Active {$this->last_online_at->shortRelativeDiffForHumans()}";
        }
        return $str;
    }

    public function getUrlAttribute()
    {
        return $this->url();
    }


    public function getUnderstandingRatingAttribute()
    {
        $lesson_ids = $this->lesson_ids();

        // Get all resource posts available in selected lessons
        $resource_post_ids = ResourcePost::whereIn('lesson_id', $lesson_ids)->pluck('id')->toArray();
        // Get average rating of resource posts on ALL students, excluding unrated resource posts
        $avg_rating = ResourcePostRating::where('student_id', $this->user_id)->whereIn('resource_post_id', $resource_post_ids)->avg('rating');

        return number_format($avg_rating, 1);
    }

    /*
     |---------------------------------
     | Mutators
     |---------------------------------
    */
    public function setNameAttribute($value)
    {
        $this->user()->update([
            'name' => $value
        ]);
    }

    public function setAboutAttribute($value)
    {
        $this->user()->update([
            'about' => $value
        ]);
    }

    public function setStateAttribute($value)
    {
        $this->user()->update([
            'state_id' => $value
        ]);
    }

    /**
     * Returns profile color
     * @return string
     */
    public function getProfileColor()
    {
        return 'orange';
    }

    /**
     * ----------------------
     * Helper functions
     * ----------------------
     */

    public function url( Classroom $classroom = null)
    {
        if( is_null($classroom) ) {
            return route('classroom.students.show', $this->user_id);
        }
        return route('classroom.students.show', $this->user_id).'?class_selected='.$classroom->id;
    }

    /**
     * Returns true if student has parent, else false,
     * @return boolean
     */
    public function has_parent()
    {
        return !is_null($this->parent);
    }

    /**
     * Update student's parent
     */
    public function updateParent(StudentParent $parent)
    {
        $this->update([
            'parent_id' => $parent->user_id
        ]);
    }

    public function getSelectedClass( $classroom_id = null )
    {
        $user_id = $this->user_id;
        if( !is_null($classroom_id) ) {
            return Classroom::find(request()->class_selected);
        } else {
            return Classroom::whereHas('students', function ($q) use ($user_id) {
                $q->where('user_id', $user_id);
            })->first();
        }
    }

    /**
     * ------------------
     * Classroom
     * ------------------
     */

    /**
     * Returns homeworks
     * @param App\Classroom $classroom
     * @return Collection
     */
    public function getHomeworks(Classroom $classroom = null)
    {
        return Homework::whereIn('lesson_id', $this->getLessonIds($classroom))->get();
    }

    /**
     * Returns average resource rating of student
     * @param App\Classroom $classroom
     * @return float
     */
    public function getAverageResourceRating( $classroom = null )
    {
        $lesson_ids = $this->lesson_ids( $classroom );

        // Get all resource posts available in selected lessons
        $resource_post_ids = ResourcePost::whereIn('lesson_id', $lesson_ids)->pluck('id')->toArray();

        // Get average rating of resource posts on ALL students, excluding unrated resource posts
        $avg_rating = ResourcePostRating::where('student_id', $this->user_id)->whereIn('resource_post_id', $resource_post_ids)->avg('rating');

        return number_format($avg_rating, 1);
    }

    /**
     * Returns number of ratings equal to inputted rating
     * @return int
     */
    public function getRatingCount( $classroom, $rating )
    {
        $lesson_ids = $this->lesson_ids( $classroom );
        // Get all resource posts available in selected lessons
        $resource_post_ids = ResourcePost::whereIn('lesson_id', $lesson_ids)->pluck('id')->toArray();

        return ResourcePostRating::where('student_id', $this->user_id)->whereIn('resource_post_id', $resource_post_ids)->where('rating', $rating)->count();
    }

    public function getAbsoluteTotalRatings( $classroom, $rating )
    {
        $lesson_ids = $this->lesson_ids( $classroom );
        // Get all resource posts available in selected lessons
        $resource_post_ids = ResourcePost::whereIn('lesson_id', $lesson_ids)->pluck('id')->toArray();

        return ResourcePostRating::where('student_id', $this->user_id)->whereIn('resource_post_id', $resource_post_ids)->count();
    }

    public function getRatingCountPercentage( $classroom, $rating )
    {
        // Get all ratings belonging to user and classroom resource posts
        $rating_count = $this->getRatingCount( $classroom, $rating );
        $total = $this->getAbsoluteTotalRatings( $classroom, $rating );

        if( $total ) {
            return number_format(($rating_count/$total) * 100, 1);
        } else {
            return null;
        }
    }

    public function getHomeworkSubmission(Homework $homework)
    {
        return HomeworkSubmission::where('homework_id', $homework->id)->where('student_id', $this->user_id)->first();
    }

    public function getHomeworkMarks(Homework $homework)
    {
        return $this->getHomeworkSubmission($homework) ? $this->getHomeworkSubmission($homework)->marks : 0;
    }

    public function getHomeworkMarksInPercentage(Homework $homework)
    {
        $marks = $this->getHomeworkMarks($homework);
        $max_marks = $homework->max_marks;

        if( !is_null($marks) && $marks > 0 ) {
            return number_format( $marks/$max_marks * 100, 1 );
        } else {
            return 0;
        }
    }

    public function getAverageHomeworkMarksByMonth( $month_of_year, Classroom $classroom = null )
    {
        $dt = Carbon::parse($month_of_year);

        $homeworks = Homework::whereMonth('due_at', $dt->month)
                              ->whereYear('due_at', $dt->year)
                              ->where('due_at', '<=', Carbon::now()->toDateTimeString());

        if( !is_null($classroom) ) {
            $homeworks = $homeworks->whereIn('lesson_id', $classroom->lessons()->pluck('id')->toArray());
        }

        $homeworks = $homeworks->get();

        $total_percentage = 0;
        $count = 0;

        foreach($homeworks as $homework) {
            $total_percentage += $this->getHomeworkMarksInPercentage( $homework );
            $count += 1;
        }

        return $count > 0 ? number_format( $total_percentage/$count, 1 )
                          : 0;
    }

    /**
     * Return average homework marks
     * @param App\Classroom Classroom filter
     */
    public function getAverageHomeworkMarks( $classroom = null )
    {
        $lesson_ids = $this->lesson_ids( $classroom );

        // Get homework submission IDs
        $homework_submission_ids = $this->homework_submissions()->pluck('id')->toArray();
        $homeworks = Homework::whereHas('submissions', function($q) use ( $homework_submission_ids ){
            $q->where('user_id', $this->id)->whereIn('id', $homework_submission_ids);
        });

        $avg_mark_percentage_arr = array();
        // $avg_mark_percentage_arr = [avg1, avg2, avg3, ...]
        foreach( $homeworks as $homework ) {
            array_push( $avg_mark_percentage_arr, $homework->student_submission( $this )->marks_in_percentage );
        }

        if( count($avg_mark_percentage_arr) ) {
            $avg = array_sum($avg_mark_percentage_arr)/count($avg_mark_percentage_arr);
            return number_format($avg, 2);
        } else {
            return 0;
        }
    }

    /*
     |---------------------------------
     | Timeline items
     |---------------------------------
    */
    public function getUpcomingLessons( $days_in_advance = 14 )
    {
        $start_time = Carbon::now();
        $end_time = Carbon::now()->addDays($days_in_advance);
        $lesson_ids = $this->lesson_ids();
        return Lesson::whereIn('id', $lesson_ids)->where('start_at', '>', $start_time)->where('start_at', '<=', $end_time)->orderBy('start_at')->get();
    }

    public function getUpcomingHomeworks( $days_in_advance = 14 )
    {
        $start_time = Carbon::now();
        $end_time = Carbon::now()->addDays($days_in_advance);
        $lesson_ids = $this->getLessonIds();

        return Homework::whereIn('lesson_id', $lesson_ids)->where('due_at', '>=', $start_time)->where('due_at', '<=', $end_time)->orderBy('due_at')->get();
    }

    /*
     |---------------------------------
     | PDF functions
     |---------------------------------
    */
    public function loadReportPdf( $start_date, $end_date, $teacher_remark )
    {
        $classroom_ids = $this->classrooms()->pluck('id');

        $questions = Question::where('user_id', $this->user_id)->whereBetween( 'created_at',  [$start_date, $end_date] )->get();
        $resource_posts = ResourcePost::where('classroom_id', $classroom_ids)->whereBetween( 'created_at',  [$start_date, $end_date] )->get();
        $homeworks = Homework::where('classroom_id', $classroom_ids)->whereBetween( 'created_at',  [$start_date, $end_date] )->get();

        $data = [
            'start_date' => $start_date,
            'end_date' => $end_date,
            'student' => $this,
            'questions' => $questions,
            'resource_posts' => $resource_posts,
            'homeworks' => $homeworks,
            'teacher_remark' => $teacher_remark,
        ];
        return PDF::loadView('classroom.pdf.student_report', $data);
    }

    /**
     * Returns student with email
     * @param string $email
     * @return App\Student
     */
    public static function retrieveWithEmail($email)
    {
        return Student::whereHas('user', function($q) use ($email) {
            $q->where('users.email', $email );
        })->first();
    }

    /**
     * Returns students with emails
     * @param array
     * @return Collection
     */
    public static function retrieveWithEmails($emails)
    {
        return Student::whereHas('user', function($q) use ($emails) {
                    $q->whereIn('users.email', $emails );
                })->get();
    }


    /**
     * ------------------
     * Lessons
     * ------------------
     */

    public function lessons()
    {
        return Lesson::find( $this->getLessonIds() );
    }

    public function lesson_ids( Classroom $classroom = null )
    {
        return $this->user->getLessonIds( $classroom );
    }

    /**
     * Get lessons
     * @param App\Classroom $classroom
     * @return Collection
     */
    public function getLessons(Classroom $classroom = null)
    {
        return Lesson::find($this->getLessonIds($classroom));
    }

    /**
     * Get lesson IDs associated with student
     * @param(optional) App\Classroom $classroom Filter lesson IDs by classroom
     * @return array
     */
    public function getLessonIds( Classroom $classroom = null )
    {
        if( !is_null( $classroom ) ) {
            if( !$classroom->hasStudent($this) ) {
                return array();
            }
            // Get all lessons from classroom
            return $classroom->lessons()->pluck('id')->toArray();
        } else {
            $classroom_ids = $this->classrooms()->pluck('id')->toArray();
            // Get all lessons from the student
            return Lesson::whereHas('classroom', function($q) use ($classroom_ids){
                $q->whereIn('id', $classroom_ids);
            })->pluck('id')->toArray();
        }
    }

    /*
     |---------------------------
     | Student Report
     |---------------------------
    */

    public function getUnreportedLessons()
    {
        return Lesson::find( $this->getUnreportedLessonIds() );
    }

    public function getReportedLessonIds()
    {
        return $this->getReportItemLessonIds();
    }

    public function getReportedLessons()
    {
        return Lesson::find( $this->getReportedLessonIds() );
    }

    public function getUnreportedLessonIds()
    {
        $all_lesson_ids = $this->getLessonIds();
        $reported_lesson_ids = $this->getReportItemLessonIds();

        return array_diff( $all_lesson_ids, $reported_lesson_ids );
    }

    public function getReportItemLessonIds()
    {
        $report_ids = $this->reports()->pluck('id')->toArray();
        return StudentReportItem::whereIn('student_report_id', $report_ids)->pluck('lesson_id')->toArray();
    }

    /**
     * Return student report Items
     * @return App\StudentReportItem
     */
    public function report_items()
    {
        $report_ids = $this->reports()->pluck('id')->toArray();
        return StudentReportItem::whereIn('student_report_id', $report_ids)->get();
    }

    /*
     |---------------------------
     | Teacher Invoice
     |---------------------------
    */

    public function getPaidLessonIds()
    {
        return $this->getInvoiceItemLessonIds();
    }

    public function getPaidLessons()
    {
        return Lesson::find( $this->getPaidLessonIds() );
    }

    public function getUnpaidLessonIds()
    {
        $all_lesson_ids = $this->getLessonIds();
        $paid_lesson_ids = $this->getInvoiceItemLessonIds();

        return array_diff( $all_lesson_ids, $paid_lesson_ids );
    }

    public function getUnpaidLessons()
    {
        return Lesson::find( $this->getUnpaidLessonIds() );
    }

    public function getInvoiceItemLessonIds()
    {
        $invoices_ids = $this->invoices()->pluck('id')->toArray();
        return TeacherInvoiceItem::whereIn('invoice_id', $invoices_ids)->pluck('lesson_id')->toArray();
    }

    /**
     * ------------------------
     * Board Pass
     * ------------------------
     */

    /**
     * Returns true if user has active Board Pass, else false
     * @return boolean
     */
    public function hasActiveBoardPass()
    {
        return $this->user->hasActiveBoardPass();
    }

    /**
     * -------------------------
     * Exam Builder
     * -------------------------
     */

    /**
     * Returns 999 if user has infinite worksheets, else return number of available worksheets that can be generated this week
     * Free user gets to generate 3 worksheets/week
     * @return int
     */
    public function noOfAvailableWorksheetsLeft()
    {
        if($this->hasActiveBoardPass()) {
            return 999;
        } else {
            return 3 - $this->user->exam_builder_sheets()->whereDate('created_at', '>', Carbon::now()->startOfWeek()->format('Y-m-d'))->count();
        }
    }

    /**
     * Returns true if student has permission to generate exam builder sheet, else false
     * @return boolean
     */
    public function canGenerateExamBuilderSheet()
    {
        // Returns true if student has Board Pass
        if( $this->hasActiveBoardPass() ) {
            return true;
        } else {
            return $this->noOfAvailableWorksheetsLeft() > 0;
        }
    }

    /**
     * Returns true if student can hide exam builder sheet watermark
     * @return boolean
     */
    public function canHideExamBuilderSheetWatermark()
    {
        return false;
    }

    /**
     * Returns true if student can generate exam builder sheet with subchapter
     * @return boolean
     */
    public function canGenerateExamBuilderSheetWithSubchapter()
    {
        return $this->hasActiveBoardPass();
    }

    /**
     * Returns max number of questions student can generate for exam builder sheet
     * @return int
     */
    public function getExamBuilderQuestionLimit()
    {
        return $this->hasActiveBoardPass() ? setting('exam_builder.pass_question_limit') : setting('exam_builder.free_question_limit');
    }

    /**
     * Returns max number of chapters teacher can generate for exam builder sheet
     * @return int
     */
    public function getExamBuilderChapterLimit()
    {
        return $this->hasActiveBoardPass() ? setting('exam_builder.pass_chapter_limit') : setting('exam_builder.free_chapter_limit');
    }

    /**
     * Returns true if student has premium resource access
     * @return boolean
     */
    public function hasPremiumResourceAccess()
    {
        return $this->user->hasActiveBoardPass();
    }

    /**
     * Returns library limit of user
     * @return int
     */
    public function getLibraryLimit()
    {
        return $this->hasActiveBoardPass() ? setting('board_pass.library_limit') : 1;
    }

    /**
     * Returns true if student passed all certification metrics, else returns false
     * @return boolean
     */
    public function passedAllCertificationMetrics()
    {
        return false;
    }
}
