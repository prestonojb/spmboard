<div class="d-flex justify-content-between align-items-center">
    <div>
        <div class="d-inline-block mr-1 mb-1 mb-sm-0">
            {{-- Subject/Chapter Modal --}}
            @component('forum.common.filter.subject_chapter', ['exam_board' => $exam_board])
            @endcomponent
        </div>

        @component('forum.common.filter.sort', ['sort' => $sort])
        @endcomponent
    </div>

    <div>
        {{-- Exam Board Modal --}}
        @component('forum.common.filter.exam_board', ['exam_board' => $exam_board])
        @endcomponent
    </div>
</div>
