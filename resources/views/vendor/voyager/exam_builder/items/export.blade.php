@extends('voyager::master')

@section('page_title', 'Exam Builder Item Export')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_header')
<div class="container-fluid">
    <h1 class="page-title">
        [Exam Builder] Export Items
    </h1>
</div>
@stop

@section('content')
    <div class="page-content container-fluid">
        <p>Export all Exam Builder Items as CSV file.</p>

        <form class="form-edit-add" role="form"
              action="{{ route('voyager.exam_builder_items.export') }}"
              method="POST" enctype="multipart/form-data" autocomplete="off">
            {{ csrf_field() }}

            <div class="panel panel-bordered">
                <div class="panel">
                    {{-- Subject --}}
                    <div class="form-group">
                        <label>Subject</label>
                        <select class="subject-select" name="subject_id">
                            <option data-placeholder="true"></option>
                            @foreach($subjects as $subject)
                                <option value="{{ $subject->id }}" {{ old('subject') == $subject->id ? 'selected="selected"' : '' }}>{{ $subject->name_with_exam_board }}</option>
                            @endforeach
                        </select>
                        @error('subject')
                            <span class="error-message">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
            </div>

            <button type="submit" class="btn btn-primary pull-right save">
                Export
            </button>
        </form>
    </div>
@stop

@section('javascript')
<script src="{{ mix('js/forum.js') }}"></script>
@stop
