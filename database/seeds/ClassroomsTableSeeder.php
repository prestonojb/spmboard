<?php

use App\Classroom;
use App\Subject;
use App\Student;
use App\Teacher;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ClassroomsTableSeeder extends Seeder
{
    protected $NO_OF_CLASSROOMS = 10;
    protected $MIN_NO_OF_STUDENTS = 2;
    protected $MAX_NO_OF_STUDENTS = 5;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Classroom::truncate();
        DB::table('classroom_student')->delete();
        $faker = \Faker\Factory::create();

        /**
         * Create classrooms, then add 0-2 students to them
         */

        $faker = \Faker\Factory::create();

        for($i = 0; $i < $this->NO_OF_CLASSROOMS; $i++) {
            $teacher = Teacher::inRandomOrder()->first();
            $subject = Subject::inRandomOrder()->first();

            $classroom = Classroom::create([
                'name' => $faker->sentence(3),
                'teacher_id' => $teacher ? $teacher->user_id : null,
                'subject_id' => $subject ? $subject->id : null,
                'code' => getRandomString(7),
            ]);

            // Add students to classroom
            $no_of_students = rand($this->MIN_NO_OF_STUDENTS, $this->MAX_NO_OF_STUDENTS);
            for($j=0; $j < $no_of_students; $j++) {
                $classroom->students()->syncWithoutDetaching([
                    Student::inRandomOrder()->first()->user_id
                ]);
            }
        }

    }
}
