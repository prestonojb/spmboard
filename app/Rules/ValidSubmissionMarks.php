<?php

namespace App\Rules;

use App\Homework;
use Illuminate\Contracts\Validation\Rule;

class ValidSubmissionMarks implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct(Homework $homework)
    {
        $this->homework = $homework;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return $value >= 0 && $value <= $this->homework->max_marks;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Given marks cannot be less than 0 or greater than total marks!';
    }
}
