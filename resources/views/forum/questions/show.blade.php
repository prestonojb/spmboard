@extends('forum.layouts.app')

@php $exam_board = $question->subject->exam_board; @endphp

@section('meta')
    <title>{{ $question->question }} | {{ $exam_board->name }} {{ config('app.name') }}</title>
    <meta name="description" content="{{ strip_tags($question->description) }}">

    <meta property="og:image" content="{{ $question->img ?? asset('img/essential/og-logo.png') }}" />
    <meta property="og:type" content="website" />
@endsection

@section('content')
@include('common.header2')
{{-- Smart bar --}}
@if( setting('marketing.exam_builder') )
    @component('common.smart_bar', ['title' => '🤩🤩 Customize worksheets in 10 seconds with Exam Builder. Try for FREE now! (IGCSE only)', 'url' => url('exam-builder')])
    @endcomponent
@endif

<div class="container">
    <div class="py-2"></div>
    <div class="row">
        <div class="col-12 col-lg-9 mb-3">
            @include('forum.common.question')

            @if($exam_board->slug == 'igcse')
                <div class="ml-2 mb-2 alert alert-warning mb-0 mt-2" role="alert">
                    Try out the new revision tool built for you IGCSE students! <a href="https://bit.ly/2Vx7JvS" target="_blank" class="underline-on-hover"><b>INTERESTED!</b></a>
                </div>
            @endif

            @foreach($answers as $answer)
                @include('forum.common.answer')
            @endforeach

            @auth
                @can('create', \App\Answer::class)
                    <div class="panel ml-2">
                        @component('forum.answers.common.form', ['question' => $question, 'is_edit' => false])
                        @endcomponent
                    </div>
                @endcan
            @else
                <div class="panel cta-panel ml-2">
                    <div class="text-center p-3">
                        <h2 class="white mb-3"><b>Sign up now to answer this question!</b></h2>
                        <a class="cta-panel__button primary answer-input__sign-up-link" href="{{ route('register') }}">SIGN UP</a>
                    </div>
                </div>
            @endif
        </div>

        <div class="col-12 col-lg-3 mb-3">
            {{-- Ads --}}
            @if( setting('marketing.spm_2020_askathon') )
                @component('forum.common.side_banner', ['img_url' => asset('img/events/spm_askathon_2020.png'), 'redirect_url' => url('spm-askathon-2020')])
                @endcomponent
            @endif

            {{-- Related Questions by Subject --}}
            <div>
                <h4 class="font-size4 border-bottom pb-2 mb-2">Related Questions</h1>
                @foreach($question->getRelatedQuestions() as $related_question)
                    <a class="d-block underline-on-hover py-1" href="{{ $related_question->show_url }}">{{ $related_question->question }}</a>
                @endforeach
                <div class="py-1"></div>
                <a class="gray2 font-size2 underline-on-hover" href="{{ route('questions.create', $question->subject->exam_board->slug) }}">Ask Question</a>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js_dependencies')
{{-- TinyMCE --}}
<script src="https://cdn.tiny.cloud/1/si4jvonmpvtlfypotvf9zu2vci2zsjavfd4v1oyvnzncszag/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>    {{-- KaTeX --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ" crossorigin="anonymous"></script>
@endsection

@section('scripts')
<script>
$(function(){
    var $answerForm = $('#answerForm');
    // Hashes are strictly for answers ID only
    // hash = #20
    var hash = window.location.hash;
    if(hash) {
        // hash = 20
        hash = hash.substring(1);
        // hash = #answer_20
        hash = '#' + 'answer_' + hash;

        $('html, body').animate({
            scrollTop: $(hash).offset().top - 68
        }, 200);

        $(hash).addClass('focus-outline-blue3');
        setTimeout(function(){
            $(hash).removeClass('focus-outline-blue3');
        }, 2500);
    }

    const upvoteButtonAnimation = 'animated pulse faster';

    $('.answer-button').click(function(){
        if(tinymce !== undefined) {
            $('html, body').animate({
                scrollTop: $answerForm.offset().top
            }, 200);
            tinymce.activeEditor.focus();
        } else {
            Swal.fire({
                title: '<strong>Oops! You are not logged in!</strong>',
                type: 'error',
                html: '<p>Log in to answer this question!</p>',
                showCloseButton: true,
                showCancelButton: true,
                focusConfirm: false,
                reverseButtons: true,
                confirmButtonText: 'Log in',
                cancelButtonText: 'Not now',
            }).then(function(result){
                if(result.value){
                    window.location.href = '{{ route('login') }}';
                }
            });
        }
    });

    // Declare event listeners if editor exists
    var answerFocus = window.getParameterByName('answerFocus');
    if(answerFocus == 'true' && tinymce !== undefined) {
        setTimeout(
            function() {
                $('.answer-button').click();
            }, 1000);
    }

    $('.question-upvote-button').click(function(e){
        const loggedIn = {{ auth()->check() ? 'true' : 'false' }};
        if(loggedIn){
            const question_id = $(this).data('questionid');
            var url = "{{ route('questions.upvote', ':question_id') }}";
            url = url.replace(':question_id', question_id);

            $.ajax({
                method: 'POST',
                url: url,
            });

            const $upvoteCount = $('.upvote-count[data-questionid="'+ question_id +'"]');
            //Get the upvote count of the question
            var upvote_count = parseInt($upvoteCount.text());

            $(this).toggleClass('upvoted');
            if($(this).hasClass('upvoted')){
                upvote_count++;
                $upvoteCount.text(upvote_count);

                // Animate the upvote button
                $(this).addClass(upvoteButtonAnimation).on('animationend', function(){
                    $(this).removeClass(upvoteButtonAnimation);
                });
            } else{
                upvote_count--;
                $upvoteCount.text(upvote_count);
            }

        } else{
            Swal.fire({
                title: '<strong>Oops! You are not logged in!</strong>',
                type: 'error',
                html:
                    '<p>Log in to gain the best experience with us!</p>',
                showCloseButton: true,
                showCancelButton: true,
                focusConfirm: false,
                reverseButtons: true,
                confirmButtonText: 'Log in',
                cancelButtonText: 'Not now',
            }).then(function(result){
                if(result.value){
                    window.location.href = '{{ route('login') }}';
                }
            });
        }
    });

    // Recommend question button
    $('.question-recommend-button').click(function(e) {
        e.preventDefault();
        var $t = $(this);
        var url = $t.data('url');
        // Toggle star button
        $('.question-block__star').toggleClass('recommend');
        // Update button text
        if($t.text().trim() == 'Recommend') {
            $t.text('Unrecommend');
        } else {
            $t.text('Recommend');
        }
        // AJAX
        $.ajax({
            method: 'POST',
            url: url,
            success: function (response) {
                if(response == 'recommended') {
                    toastr.success('Question recommended');
                } else if(response == 'unrecommended') {
                    toastr.success('Question unrecommended');
                }
            }
        });
    });

    $('.answer-recommend-button').click(function() {
        var $t = $(this);
        var url = $t.data('url');

        // Toggle star button
        $t.parents('.answerBlock').find('.answer-block__star').toggleClass('recommend');

        // Update button text
        if($t.text().trim() == 'Recommend') {
            $t.text('Unrecommend');
        } else {
            $t.text('Recommend');
        }

        // AJAX
        $.ajax({
            method: 'POST',
            url: url,
            success: function (response) {
                if(response == 'recommended') {
                    toastr.success('Answer recommended');
                } else if(response == 'unrecommended') {
                    toastr.success('Answer unrecommended');
                }
            }
        });
    });

    $('.answer-upvote-button').click(function(e){
        const answer_id = $(this).data('answerid');
        var loggedIn = {{ auth()->check() ? 'true' : 'false' }};
        if(loggedIn){
            //Get the right upvote count <span>
            var $upvoteCount = $('.answer-upvote-count[data-answerid="'+ answer_id +'"]');
            //Get the upvote count of the question
            var upvote_count = parseInt($upvoteCount.text());
            if($(this).hasClass('upvoted')){
                upvote_count--;
                $upvoteCount.text(upvote_count);
            } else{
                upvote_count++;
                $upvoteCount.text(upvote_count);

                // Animate the upvote button
                $(this).addClass(upvoteButtonAnimation).on('animationend', function(){
                    $(this).removeClass(upvoteButtonAnimation);
                });
            }
            $(this).toggleClass('upvoted');
            var url = "{{ route('answers.upvote', ':answer_id') }}";
            url = url.replace(':answer_id', answer_id);
            $.ajax({
                method: 'POST',
                url: url,
            });
        } else{
            Swal.fire({
                title: '<strong>Oops! You are not logged in!</strong>',
                type: 'error',
                html:
                    '<p>Log in to unlock more features</p>',
                showCloseButton: true,
                showCancelButton: true,
                focusConfirm: false,
                reverseButtons: true,
                confirmButtonText: 'Log in',
                cancelButtonText: 'Not now',
            }).then(function(result){
                if(result.value){
                    window.location.href = '{{ route('login') }}';
                }
            });
        }
    });

    // Answer check
    $('.answer-check-button').click(function(){
        const answer_id = $(this).data('answerid');

        // Answer button is unchecked
        if($(this).hasClass('checked')){
            //Update checked to 0
            $(this).removeClass('checked');

            $('.question-block__check').removeClass('checked');
            $('.answer-block__check').filter('[data-answerid="' + answer_id + '"]').removeClass('checked');

            toastr.success('Answer unaccepted!');
        } else{
            //Set all question's answers to 0 and update checked answer button to active
            $('.answer-check-button').each(function(){
                $(this).removeClass('checked');
            });
            $('.answer-block__check').each(function(){
                $(this).removeClass('checked');
            });

            $(this).addClass('checked');

            $('.question-block__check').addClass('checked');
            $('.answer-block__check').filter('[data-answerid="' + answer_id + '"]').addClass('checked');
            toastr.success('Answer accepted!');
        }
        var url = "{{ route('answers.check', ':answer_id') }}";
        url = url.replace(':answer_id', answer_id);
        // AJAX
        $.ajax({
            method: 'POST',
            url: url,
            success: function(res) {
                if(res == 'Points awarded'){
                    // Question is awarded points already, hence unawarded is INACTIVE
                    $('.question-block__award').removeClass('unawarded');
                    // Answer is awarded points, hence awarded is ACTIVE
                    $('.answer-block__award').filter('[data-answerid="' + answer_id + '"]').addClass('awarded');
                }
            }
        });
    });

    //Image popup
    $('.question-img, .answer-img').click(function(){
        var img_src = $(this).attr('src');
        $.magnificPopup.open({
            items: {
                src: img_src,
            },
            type: 'image',
            showCloseButton: false,
        });
    });

    //Delete alert
    $('.delete-button').click(function(e){
        e.preventDefault();
        e.stopPropagation();
        var form = $(this).closest('form');
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            reverseButtons: true,
            confirmButtonText: 'Delete',
            cancelButtonText: 'Cancel',
        }).then(function(result){
            if(result.value){
                form.submit();
            }
        });
    });

    $('.question__comment-button, .answer__comment-button').click(function(){
        $(this).next('.question__comment-list, .answer__comment-list').toggle();
    });
});

</script>
@endsection
