<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    /**
     * Get Payment Update View
     */
    public function getUpdateView()
    {
        $user = auth()->user();

        // Get user default payment
        $default_payment_method = $user->defaultPaymentMethod();
        // Get all user's payment methods
        $payment_methods = $user->paymentMethods();
        // Get user invoices
        $invoices = $user->invoices();

        return view('payment.home', compact('default_payment_method', 'payment_methods', 'invoices'));
    }

    /**
     * Update user Default payment method
     */
    public function updateDefaultPaymentMethod()
    {
        request()->validate([
            'default_payment_method' => 'required'
        ]);

        $user = auth()->user();

        // Create stripe ID for user if user is new stripe customer
        $customer = $user->createOrGetStripeCustomer();

        $user->updateDefaultPaymentMethod( request('default_payment_method') );

        return redirect()->back()->withSuccess('Your default payment method is updated successfully!');
    }

    /**
     * Add Payment Method
     */
    public function addPaymentMethod()
    {
        $user = auth()->user();
        $is_default_pm = isset(request()->is_default_pm);

        try {
            // Create stripe ID for user if user is new stripe customer
            $customer = $user->createOrGetStripeCustomer();
            $user->addPaymentMethod( request('payment_method') );
        } catch (Exception $e) {
            return redirect()->back()->with('error', 'Error processing card. Please try again later.');
        }

        if( $is_default_pm ) {
            $user->updateDefaultPaymentMethod( request()->payment_method );
        }

        return redirect()->back()->withSuccess('Your payment method is updated successfully!');
    }

    /**
     * User Download Invoice
     */
    public function downloadInvoice($invoice_id) {
        return request()->user()->downloadInvoice($invoice_id, [
            'vendor' => 'Board',
            'product' => 'Coin Pack'
        ], 'board_invoice');
    }
}
