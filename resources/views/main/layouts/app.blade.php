<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" class="no-js">
<head>
    @if(config('app.env') == 'production')
        @include('common.third_party_trackers')
    @endif

    <style>
        html{ visibility: hidden; }
        .loader{color:#000;position:fixed;box-sizing:border-box;left:-9999px;top:-9999px;width:0;height:0;overflow:hidden;z-index:999999}.loader:after,.loader:before{box-sizing:border-box;display:none}.loader.is-active{background-color:#fff;width:100%;height:100%;left:0;top:0}.loader.is-active:after,.loader.is-active:before{display:block}@keyframes rotation{0%{transform:rotate(0)}to{transform:rotate(359deg)}}@keyframes blink{0%{opacity:.5}to{opacity:1}}.loader[data-text]:before{position:fixed;left:0;top:50%;color:currentColor;font-family:Helvetica,Arial,sans-serif;text-align:center;width:100%;font-size:14px}.loader[data-text=""]:before{content:"Loading"}.loader[data-text]:not([data-text=""]):before{content:attr(data-text)}.loader[data-text][data-blink]:before{animation:blink 1s linear infinite alternate}.loader-default[data-text]:before{top:calc(50% - 63px)}.loader-default:after{content:"";position:fixed;width:48px;height:48px;border:6px solid #2174D3;border-left-color:transparent;border-radius:50%;top:calc(50% - 24px);left:calc(50% - 24px);animation:rotation 1s linear infinite}.loader-default[data-half]:after{border-right-color:transparent}.loader-default[data-inverse]:after{animation-direction:reverse}
    </style>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    @yield('meta')
    <link rel="shortcut icon" href="{{ asset('img/logo/logo_light_icon.png') }}"/>

    <meta property="og:image" content="{{ asset('img/essential/og-logo.png') }}" />
    <meta property="og:type" content="website" />

    {{-- Bootstrap 4 --}}
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    {{-- Font Awesome 4 --}}
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"/>
    {{-- Font Awesome 5 --}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css" integrity="sha256-h20CPZ0QyXlBuAw7A+KluUYx/3pK+c7lYEpqLTlxjYQ=" crossorigin="anonymous" />
    {{-- Iconify --}}
    <script src="https://code.iconify.design/1/1.0.6/iconify.min.js"></script>
    {{-- Animate.css --}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.css">
    {{-- Slim Select --}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slim-select/1.23.1/slimselect.min.css">
    {{-- Toastr --}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
    {{-- jsSocials --}}
    <link type="text/css" rel="stylesheet" href="https://cdn.jsdelivr.net/jquery.jssocials/1.4.0/jssocials.css" />
    <link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jsSocials/1.5.0/jssocials-theme-minima.min.css"/>
    <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,300;0,400;0,500;0,700;1,300;1,400;1,500;1,700&display=swap">
    @yield('styles')
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
    <link href="{{ mix('css/main/app.css') }}" rel="stylesheet">
    {{-- <script src="https://cdn.jsdelivr.net/npm/@widgetbot/crate@3" async defer>
        new Crate({
          server: '861526603108581437',
          channel: '862248704467795979'
        });
        crate.notify('Meet new friends and share your thoughts about Board on our Discord server!')
</script> --}}
<script data-ad-client="ca-pub-8148175035215703" async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
</head>

<body>
    <div class="root">
        <div class="loader loader-default is-active"></div>
        {{-- Smart bar --}}
        @if( setting('marketing.exam_builder') )
            @component('common.smart_bar', ['title' => '🤩🤩 Customize worksheets in 10 seconds with Exam Builder. Try for FREE now! (IGCSE only)', 'url' => url('exam-builder')])
            @endcomponent
        @endif

        {{-- Smart bar --}}
        @if( setting('marketing.beta_programme') )
            @component('common.smart_bar', ['title' => '🚀🚀🚀 Help us make Board better as a student by joining our Beta Programme now (coins rewarded)!', 'url' => "https://97vwz7jr3kb.typeform.com/to/l14r6J9r"])
            @endcomponent
        @endif

        @include('main.navs.header')
        @yield('content')

    </div>

    {{-- jQuery --}}
    <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    {{-- Popper.js --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    {{-- Bootstrap 4 --}}
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    {{-- Slim Select --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slim-select/1.23.1/slimselect.min.js"></script>
    {{-- Select 2 --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.min.js" integrity="sha512-2ImtlRlf2VVmiGZsjm9bEyhjGW4dU7B6TNwh/hx/iSByxNENtj3WVE6o/9Lj4TJeVXPi4bnOIMXFIJJAeufa0A==" crossorigin="anonymous"></script>
    {{-- Toastr --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    {{-- Sweet Alert 2 --}}
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
    {{-- jsSocials --}}
    <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery.jssocials/1.4.0/jssocials.min.js"></script>
    {{-- PDF.js --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdf.js/2.0.943/pdf.min.js"></script>
    <script src="{{ mix('js/app.js') }}"></script>
    @yield('scripts')
    <script src="{{ mix('js/resource.js') }}"></script>
`   <script>
    $(function(){
        @if(session('success'))
            toastr.success('{{ session("success") }}');
        @endif
        @if(session('info'))
            toastr.info('{{ session("info") }}');
        @endif
        @if(session('status'))
            toastr.info('{{ session("status") }}');
        @endif
        @if(session('verified'))
            toastr.info('{{ session("verified") }}');
        @endif
        @if(session('error'))
            toastr.error('{{ session("error") }}');
        @endif
        @if(session('rp_promotion_awarded'))
            toastr.success('{{ session("rp_promotion_awarded") }}');
        @endif
        @if(session('ebooks_promotion_awarded'))
            toastr.success('{{ session("ebooks_promotion_awarded") }}');
        @endif
    });</script>
</body>
</html>
