<div class="blog-block">
    <a href="{{ $article->show_url }}" class="stretched-link"></a>
    <img src="{{ $article->cover_img }}">

    <div class="blog-block__body">
        <h2 class="blog-block__title">{{ $article->title }}</h2>
        <div>
            @foreach($article->tags as $tag)
                <a class="blog-tag sm mb-1" href="javascript:void(0)">
                    {{ $tag }}
                </a>
            @endforeach
        </div>
    </div>
</div>
