<div class="table-responsive">
    <table class="table table-bordered">
        <tr>
            <th scope="col">Date</th>
            <th scope="col">Name</th>
            <th scope="col">Email</th>
            <th scope="col">Phone No. (Attached to Duitnow)</th>
            <th scope="col">User Coin Balance</th>
            <th scope="col">Status</th>
            <th scope="col">Withdrawal Amount (Coin)</th>
            <th scope="col">Amount transferred (USD)</th>
            <th scope="col">Comment</th>
            <th scope="col">Receipt</th>
            <th scope="col">Action</th>
        </tr>

        @foreach($withdrawal_requests as $withdrawal_request)
            <tr>
                <td>{{ $withdrawal_request->created_at->toFormattedDateString() }}</td>
                <td>{{ $withdrawal_request->user->name }} (@<a href="{{ $withdrawal_request->user->show_url }}" target="_blank">{{ $withdrawal_request->user->username }}</a>)</td>
                <td>{{ $withdrawal_request->user->email }}</td>
                <td>{{ $withdrawal_request->user->phone_number }}</td>
                <td>{{ $withdrawal_request->user->coin_balance }}</td>
                <td>{!! $withdrawal_request->status_tag_html !!}</td>
                <td>{{ $withdrawal_request->withdrawal_amount_in_coin }}</td>
                <td>{{ $withdrawal_request->amount_transferred_in_usd ?? 'N/A' }}</td>
                <td>{{ $withdrawal_request->comment ?? 'N/A' }}</td>
                <td>
                    @if(!is_null($withdrawal_request->receipt_url))
                        <a href="{{ $withdrawal_request->receipt_url }}" target="_blank" class="blue5 underline-on-hover">
                            View
                        </a>
                    @else
                        N/A
                    @endif
                </td>
                <td>
                    @if($withdrawal_request->status == 'pending')
                        <div class="panel">
                            <h3 class="text-success">Complete Withdrawal</h3>
                            {{-- Complete --}}
                            <form action="{{ route('voyager.withdrawal_requests.complete', $withdrawal_request->id) }}" method="POST" enctype= multipart/form-data>
                                @csrf
                                {{-- Comment --}}
                                <div class="form-field">
                                    <label for="">Comment</label>
                                    <input type="text" name="comment" value="{{ old('comment') }}">
                                    @error('comment')
                                        <div class="error-message">{{ $message }}</div>
                                    @enderror
                                </div>

                                {{-- Receipt --}}
                                <div class="form-field">
                                    <label for="">Receipt (PDF) (ONLY if withdrawal request is complete)<span class="text-danger">*</span></label>
                                    <input type="file" name="receipt">
                                    @error('receipt')
                                        <div class="error-message">{{ $message }}</div>
                                    @enderror
                                </div>

                                {{-- Amount Transferred (USD) --}}
                                <div class="form-field">
                                    <label for="">Amount Transferred (USD)<span class="text-danger">*</span></label>
                                    <input type="number" step="0.01" name="amount_transferred_in_usd" placeholder="USD" value="{{ old('amount_transferred_in_usd') }}">
                                    <p>To transfer: USD {{ number_format($withdrawal_request->withdrawal_amount_in_coin * setting('conversion.coin_to_usd_sell'), 2) }}</p>
                                    @error('amount_transferred_in_usd')
                                        <div class="error-message">{{ $message }}</div>
                                    @enderror
                                </div>

                                <button type="submit" class="btn btn-success">Complete</button>
                            </form>
                        </div>

                        <hr>

                        <div class="panel">
                            <h3 class="text-danger">Cancel Withdrawal</h3>
                            {{-- Cancel --}}
                            <form action="{{ route('voyager.withdrawal_requests.cancel', $withdrawal_request->id) }}" method="POST" enctype= multipart/form-data>
                                @csrf
                                {{-- Comment --}}
                                <div class="form-field">
                                    <label for="">Comment<span class="text-danger">*</span></label>
                                    <input type="text" name="comment">
                                    @error('comment')
                                        <div class="error-message">{{ $message }}</div>
                                    @enderror
                                </div>

                                <button type="submit" class="btn btn-danger">Cancel</button>
                            </form>
                        </div>
                    @else
                        N/A
                    @endif
                </td>
            </tr>
        @endforeach
    </table>
</div>
