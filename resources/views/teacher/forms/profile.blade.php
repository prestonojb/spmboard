<form method="POST" action="{{ route('teacher.update', auth()->user()->id) }}" enctype="multipart/form-data">
    @method('PATCH')
    @csrf
    @if( isset($getting_started) && $getting_started )
        <input type="hidden" name="getting_started" value="1">
    @endif

    <div class="mt-2 mb-4">
        <h3 class="font-size7">About</h3>
        <p class="gray2">Help us get to know you better!</p>
    </div>

    {{-- Avatar --}}
    <div class="form-field">
        <label>Avatar</label>
        <div class="avatar-settings-container">
            <img class="file-input-preview profile-img lg mb-2" src="{{ $teacher->avatar }}">
            <input class="file-input" name="avatar" type="file">
            <div class="ml-2">
                <div class="file-drop-area">
                    <span class="fake-btn">Choose files</span>
                    <span class="file-msg">or drag and drop files here</span>
                    <input class="file-input" name="avatar" type="file">
                </div>

                @error('avatar')
                    <div class="error-message">{{ $message }}</div>
                @enderror
            </div>
        </div>
    </div>

    {{-- Name --}}
    <div class="form-field">
        <label>Name<span class="red">*</span></label>
        <input type="text" name="name" placeholder="Name" value="{{ old('name') ?? $teacher->name }}" required/>
        @error('name')
            <div class="error-message">{{ $message }}</div>
        @enderror
    </div>

    {{-- Headline --}}
    <div class="form-field">
        <label>Headline<span class="red">*</span></label>
        <input type="text" name="headline" placeholder="Headline" value="{{ old('headline') ?? $teacher->headline }}" required/>
        <p class="font-size1 gray2">Example: IGCSE Biology Teacher at school X</p>
        @error('headline')
            <div class="error-message">{{ $message }}</div>
        @enderror
    </div>

    {{-- Gender --}}
    <div class="form-row">
        <div class="col-6">
            @php
                $gender = old('gender') ?? $teacher->gender;
            @endphp
            <div class="form-field">
                <label for="">Gender<span class="red">*</span></label>
                <select name="gender" required>
                    <option disabled selected>Select Gender</option>
                    <option value="M" @if($gender == 'M') selected @endif>Male</option>
                    <option value="F" @if($gender == 'F') selected @endif>Female</option>
                </select>
                @error('gender')
                    <div class="error-message">{{ $message }}</div>
                @enderror
            </div>
        </div>

        {{-- Year of Birth --}}
        <div class="col-6">
            <div class="form-field">
                <label for="">Year of Birth<span class="red">*</span></label>
                <select name="year_of_birth" required>
                    <option selected disabled>Select Year of Birth</option>
                    @if( old('year_of_birth') )
                        @foreach(range(2010, 1940, -1) as $year)
                            <option value="{{ $year }}" @if(old('year_of_birth') == $year) selected @endif>{{ $year }}</option>
                        @endforeach
                    @else
                        @foreach(range(2010, 1940, -1) as $year)
                            <option value="{{ $year }}" @if($teacher->year_of_birth == $year) selected @endif>{{ $year }}</option>
                        @endforeach
                    @endif
                </select>
                @error('year_of_birth')
                    <div class="error-message">{{ $message }}</div>
                @enderror
            </div>
        </div>
    </div>

    {{-- State --}}
    <div class="form-field">
        <label>State</label>
        <select name="state" required>
            <option disabled selected>Select State</option>
            @if( old('state') )
                @foreach($states as $state)
                    <option value="{{ $state->id }}" @if( old('state') == $state->id ) selected @endif>{{ $state->name }}</option>
                @endforeach
            @else
                @foreach($states as $state)
                    <option value="{{ $state->id }}" @if( $teacher->state && $teacher->state->id == $state->id ) selected @endif>{{ $state->name }}</option>
                @endforeach
            @endif
        </select>
        @error('state')
            <div class="error-message">{{ $message }}</div>
        @enderror
    </div>

    {{-- Phone Number --}}
    <div class="form-field">
        <label>Phone Number</label>
        <input type="text" name="phone_number" placeholder="Phone Number" value="{{ old('phone_number') ?? $teacher->phone_number }}"/>
        <p class="gray2 font-size1">This information will not be made public.</p>
        @error('phone_number')
            <div class="error-message">{{ $message }}</div>
        @enderror
    </div>

    {{-- About --}}
    <div class="form-field">
        <label for="">About</label>
        <textarea name="about" id="" rows="5" placeholder="About">{{ old('about') ?? $teacher->about }}</textarea>
        <p class="font-size1 gray2">Tell us more about yourself as a teacher in 50-1000 characters!</p>
        @error('about')
            <div class="error-message">{{ $message }}</div>
        @enderror
    </div>


    <hr>

    <div class="mt-2 mb-4">
        <h3 class="font-size7">Teaching</h3>
        <p class="gray2">Help us personalise your experience on Board.</p>
    </div>

    {{-- School/Tuition Centre --}}
    <div class="form-field">
        <label>School/Tuiton Centre you are currently teaching</label>
        <input type="text" name="school" placeholder="e.g. Methodist Boys School" value="{{ old('school') ?? $teacher->school }}">
        @error('school')
            <div class="error-message">{{ $message }}</div>
        @enderror
    </div>

    {{-- Teaching Experience --}}
    <div class="form-field">
        <label>Teaching Experience</label>
        <textarea name="teaching_experience" id="" rows="5" placeholder="Teaching Experience">{{ old('teaching_experience') ?? $teacher->teaching_experience }}</textarea>
        <p class="font-size1 gray2">Write about your experience teaching in 50-1000 characters!</p>
        @error('teaching_experience')
            <div class="error-message">{{ $message }}</div>
        @enderror
    </div>

    {{-- Teaching Approach --}}
    <div class="form-field">
        <label>Teaching Approach</label>
        <textarea name="teaching_approach" id="" rows="5" placeholder="Teaching Approach">{{ old('teaching_approach') ?? $teacher->teaching_approach }}</textarea>
        <p class="font-size1 gray2">Write about how you approach teaching and helping students master their understanding in 50-1000 characters!</p>
        @error('teaching_approach')
            <div class="error-message">{{ $message }}</div>
        @enderror
    </div>

    {{-- Years of Teaching Experience --}}
    <div class="form-field">
        @php
            $years_of_teaching_experience = old('years_of_teaching_experience') ?? $teacher->years_of_teaching_experience;
        @endphp
        <label for="">Years of Teaching Experience</label>
        <select name="years_of_teaching_experience">
            <option value="0" @if($years_of_teaching_experience == 0) selected @endif>Fresh</option>
            <option value="1" @if($years_of_teaching_experience == 1) selected @endif>1 year</option>
            @foreach(range(2, 25) as $year)
                <option value="{{ $year }}" @if($years_of_teaching_experience == $year) selected @endif>{{ $year }} years</option>
            @endforeach
            <option value="25+" @if($years_of_teaching_experience == "25+") selected @endif>25+ years</option>
        </select>
        @error('years_of_teaching_experience')
            <div class="error-message">{{ $message }}</div>
        @enderror
    </div>

    {{-- Qualifications --}}
    <div class="form-field">
        <label>Qualifications</label>
        <input type="text" name="qualifications" placeholder="Qualifications" value="{{ old('qualifications') ?? $teacher->qualifications }}"/>
        <p class="font-size1 gray2">Write about your highest qualification. Example: BsC in Computer Science at University of X</p>
        @error('qualifications')
            <div class="error-message">{{ $message }}</div>
        @enderror
    </div>

    @php $exam_boards = \App\ExamBoard::all(); @endphp


    {{-- Subject --}}
    <div class="form-field">
        <label>Subjects</label>
        <select class="subject-select" name="subjects[]" multiple>
            <option data-placeholder="true"></option>
            @foreach($exam_boards as $exam_board)
                <optgroup label="{{ $exam_board->name }}">
                    @if($errors->any())
                        @foreach($exam_board->subjects as $subject)
                            <option value="{{ $subject->id }}" @if(is_array(old('subjects')) && in_array($subject->id, old('subjects'))) selected @endif>
                                {{ $subject->exam_board_and_name }}
                            </option>
                        @endforeach
                    @else
                        @foreach($exam_board->subjects as $subject)
                            <option value="{{ $subject->id }}" @if($teacher->teachSubject($subject)) selected @endif>
                                {{ $subject->exam_board_and_name }}
                            </option>
                        @endforeach
                    @endif
                </optgroup>
            @endforeach
        </select>
        <p class="font-size1 gray2">Select the subjects you teach.</p>
        @error('subject')
            <span class="error-message">{{ $message }}</span>
        @enderror
    </div>


    <hr>

    <div class="mt-2 mb-4">
        <h3 class="font-size7">
            Socials
            <span class="status-tag blue5 lg">Premium ONLY</span>
        </h3>
        <p class="gray2">Your social links will only made public if you are a Board Premium teacher. <a href="{{ route('classroom.prices.index') }}" target="_blank">Learn More</a></p>
    </div>

    <div class="form-field">
        <div class="row">
            <div class="col-12 col-lg-6">
                <div class="form-field">
                    {{-- Facebook --}}
                    <div class="d-flex justify-content-between align-items-center">
                        <span class="font-size7 gray2 mr-2"><i class="fab fa-facebook"></i></span>
                        <input type="text" name="facebook" value="{{ old('facebook') ?? $teacher->facebook_url }}" placeholder="Facebook Profile URL">
                    </div>
                    <p class="mt-1 mb-0 gray2 font-size1">Example URL: <a class="font-size-inherit" target="_blank" href="https://www.facebook.com/boardedu" class="underline-on-hover font-size-inherit">https://facebook.com/xxx</a></p>
                    @error('facebook')
                        <span class="error-message">{{ $message }}</span>
                    @enderror
                </div>
            </div>

            <div class="col-12 col-lg-6">
                <div class="form-field">
                    {{-- Instagram --}}
                    <div class="d-flex justify-content-between align-items-center">
                        <span class="font-size7 gray2 mr-2"><i class="fab fa-instagram"></i></span>
                        <input type="text" name="instagram" value="{{ old('instagram') ?? $teacher->instagram_url }}" placeholder="Instagram Profile URL">
                    </div>
                    <p class="mt-1 mb-0 gray2 font-size1">Example URL: <a class="font-size-inherit" target="_blank" href="https://www.instagram.com/boardedumy/" class="underline-on-hover font-size-inherit">https://instagram.com/xxx/</a></p>
                    @error('instagram')
                        <span class="error-message">{{ $message }}</span>
                    @enderror
                </div>
            </div>

            <div class="col-12 col-lg-6">
                <div class="form-field">
                    {{-- YouTube --}}
                    <div class="d-flex justify-content-between align-items-center">
                        <span class="font-size7 gray2 mr-2"><i class="fab fa-youtube"></i></span>
                        <input type="text" name="youtube" value="{{ old('youtube') ?? $teacher->youtube_url }}" placeholder="YouTube Channel URL">
                    </div>
                    <p class="mt-1 mb-0 gray2 font-size1">Example URL: <a class="font-size-inherit" target="_blank" href="https://www.youtube.com/channel/UCivlBkwCKaQ1jhn1wyrvmeg" class="underline-on-hover font-size-inherit">https://youtube.com/channel/xxx</a></p>
                    @error('youtube')
                        <span class="error-message">{{ $message }}</span>
                    @enderror
                </div>
            </div>
        </div>
    </div>

    <div class="form-field">
        <span class="red">*</span><span class="gray2">Required fields</span>
    </div>

    <div class="text-center">
        <button type="submit" class="button--primary mb-1 loadOnSubmitButton" style="width: 129px;">Update Profile</button>
    </div>
</form>
