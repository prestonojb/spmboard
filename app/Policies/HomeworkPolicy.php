<?php

namespace App\Policies;

use App\Homework;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class HomeworkPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    public function mark(User $user, Homework $homework)
    {
        $classroom = $homework->lesson->classroom;
        return $user->teacher && $user->id == $classroom->teacher_id;
    }

    public function submit(User $user, Homework $homework)
    {
        $classroom = $homework->lesson->classroom;
        return $user->student && $classroom->students->contains( $user->id );
    }

    /**
     * Permission to update homework
     */
    public function update(User $user, Homework $homework)
    {
        $lesson = $homework->lesson;
        return $user->is_teacher() && $lesson->classroom->hasTeacher($user->teacher);
    }

    /**
     * Permission to delete homework
     */
    public function delete(User $user, Homework $homework)
    {
        $lesson = $homework->lesson;
        return $user->is_teacher() && $lesson->classroom->hasTeacher($user->teacher);
    }
}
