@extends('classroom.layouts.app')

@section('meta')
    <title>Billing History | {{ config('app.name') }} Classroom</title>
@endsection

@section('content')
@component('classroom.common.page_heading', ['title' => 'Billing History'])
@endcomponent

@if( auth()->user()->teacher )
    @component('classroom.navs.teacher.billing')
    @endcomponent
@endif

<div class="table-responsive">
    <table class="table">
        <thead>
            <tr>
                <th>Date</th>
                <th>Amount</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($invoices as $invoice)
                <tr>
                    <td>{{ $invoice->date()->toFormattedDateString() }}</td>
                    <td>{{ $invoice->total() }}</td>
                    <td>
                        <a href="{{ route('classroom.download_invoice', $invoice->id) }}" class="button--primary">Download</a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection


