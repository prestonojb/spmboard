@php
    switch($type) {
        case 'coins':
            $id = 'getCoins';
            break;
    }
@endphp
<div class="bg-blue6 py-4" id="{{ $id }}">
    <div class="container">
        <div class="maxw-740px mx-auto">
            <div class="text-center py-4 mb-2">
                <h3 class="text-white font-size9"><b>{{ $title }}</b></h3>
                {{ $current_balance }}
            </div>
            @switch($type)
                @case('coins')
                    @foreach($items as $item)
                        @component('products.coins.common.block', ['product' => $item])
                        @endcomponent
                        <div class="py-2"></div>
                    @endforeach
                    @break
            @endswitch
        </div>
    </div>
</div>
