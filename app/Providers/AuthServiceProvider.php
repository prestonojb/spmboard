<?php

namespace App\Providers;

use App\User;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
        'App\User' => 'App\Policies\UserPolicy',
        'App\Question' => 'App\Policies\QuestionPolicy',
        'App\Answer' => 'App\Policies\AnswerPolicy',
        'App\Classroom' => 'App\Policies\ClassroomPolicy',
        'App\Notes' => 'App\Policies\NotePolicy',
        'App\Lesson' => 'App\Policies\LessonPolicy',
        'App\Student' => 'App\Policies\StudentPolicy',
        'App\Resource' => 'App\Policies\ResourcePolicy',
        'App\TeacherInvoice' => 'App\Policies\TeacherInvoicePolicy',
        'App\StudentReport' => 'App\Policies\StudentReportPolicy',
        'App\ExamBuilderSheet' => 'App\Policies\ExamBuilderSheetPolicy',
        'App\TopicalPastPaper' => 'App\Policies\TopicalPastPaperPolicy',
        'App\Library' => 'App\Policies\LibraryPolicy',
        'App\CertificationApplication' => 'App\Policies\CertificationApplicationPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        // Admin has all permissions
        // Gate::before(function ($user, $ability) {
        //     if ($user->hasRole('admin')) {
        //         return true;
        //     }
        // });

        Gate::define('access-classroom', function (?User $user) {
            if( !is_null($user) ) {
                return setting('beta.classroom') ? $user->is_beta : true;
            } else {
                return !setting('beta.classroom');
            }
        });

        Gate::define('access-exam-builder', function (?User $user) {
            if( !is_null($user) ) {
                if( !$user->is_parent() ) {
                    return setting('beta.exam_builder') ? $user->is_beta : true;
                } else {
                    return false;
                }
            } else {
                return !setting('beta.exam_builder');
            }
        });

        Gate::define('access-resource', function (?User $user) {
            return is_null($user) || (!is_null($user) && !$user->is_parent());
        });

        Gate::define('access-marketplace', function (?User $user) {
            if( !is_null($user) ) {
                if( !$user->is_parent() ) {
                    return setting('beta.marketplace') ? $user->is_beta : true;
                } else {
                    return false;
                }
            } else {
                return !setting('beta.marketplace');
            }
        });

        Gate::define('upload-resource', function (?User $user) {
            return !is_null($user) && $user->isAdmin();
        });
    }
}
