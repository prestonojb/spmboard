// Reference for scrollX replacement: https://stackoverflow.com/a/61238158
var wrapperHtml = "<div style='overflow:auto; width:100%;position:relative;'></div>";

// Students
$('.studentTable').DataTable({
    // "scrollX": true,
    "initComplete": function (settings, json) {
        $(this).wrap(wrapperHtml);
    },
    "columnDefs": [
        { "orderable": false, "targets": 5 }
    ],
});

// Lessons
$('.lessonTable').DataTable({
    // "scrollX": true,
    "initComplete": function (settings, json) {
        $(this).wrap(wrapperHtml);
    },
    "columnDefs": [
        { "orderable": false, "targets": [0, 1, 3, 4] }
    ]
});

var $invoiceTable = $('.invoiceTable');
if( $invoiceTable.length > 0 ) {
    // Hide footer if returns true
    var showMinimal = $invoiceTable.data('show-minimal') == '1';
    var invoiceOptions = {
        // "scrollX": true,
        "initComplete": function (settings, json) {
            $(this).wrap(wrapperHtml);
        },
        "columnDefs": [
            { "orderable": false, "targets": 6 }
        ]
    };
    // Add conditional options to report options
    if( showMinimal ) {
        invoiceOptions["bInfo"] = false;
        invoiceOptions["bPaginate"] = false;
        invoiceOptions['bFilter'] = false;
    }
    // Invoices
    $('.invoiceTable').DataTable(invoiceOptions);
}

var $reportTable = $('.reportTable');
if( $reportTable.length > 0 ) {
    // Hide footer if returns true
    var showMinimal = $reportTable.data('show-minimal') == '1';
    // Declare report table options
    var reportOptions = {
        // "scrollX": true,
        "initComplete": function (settings, json) {
            $(this).wrap(wrapperHtml);
        },
        "columnDefs": [
            { "orderable": false, "targets": 5 }
        ]
    }
    // Add conditional options to report options
    if( showMinimal ) {
        reportOptions["bInfo"] = false;
        reportOptions["bPaginate"] = false;
        reportOptions['bFilter'] = false;
    }
    // Reports
    $('.reportTable').DataTable(reportOptions);
}
