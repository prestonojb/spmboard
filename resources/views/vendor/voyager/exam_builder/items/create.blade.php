@extends('voyager::master')

@section('page_title', 'Exam Builder Item')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_header')
<div class="container-fluid">
    <h1 class="page-title">
        Exam Builder Items
    </h1>
</div>
@stop

@section('content')
    <div class="page-content container-fluid">
        <form class="form-edit-add" role="form"
              action="{{ route('voyager.exam-builder-items.store') }}"
              method="POST" enctype="multipart/form-data" autocomplete="off">
            {{ csrf_field() }}

            <div class="panel panel-bordered">
                <div class="panel">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <div class="panel-body">
                        {{-- Images --}}
                        <div class="form-group">
                            <label for="imgs">Images</label>
                            <input type="file" multiple class="form-control" id="imgs" name="imgs[]" placeholder="Images">
                            @error('imgs')
                                <div class="error-message">{{ $message }}</div>
                            @enderror
                        </div>

                        {{-- Subject --}}
                        <div class="form-group">
                            <label>Subject</label>
                            <select class="subject-select" name="subject_id">
                                <option data-placeholder="true"></option>
                                @foreach($subjects as $subject)
                                    <option value="{{ $subject->id }}" {{ old('subject') == $subject->id ? 'selected="selected"' : '' }}>{{ $subject->name_with_exam_board }}</option>
                                @endforeach
                            </select>
                            @error('subject')
                                <span class="error-message">{{ $message }}</span>
                            @enderror
                        </div>

                        {{-- Chapters --}}
                        <div class="form-group">
                            <label>Chapters</label>
                            <select class="chapter-select" name="chapter_id" disabled>
                                <option></option>
                                @foreach($subjects as $subject)
                                    <optgroup label="{{ $subject->name_with_exam_board }}">
                                        @foreach($subject->chapters as $chapter)
                                            <option value="{{ $chapter->id }}"
                                                {{ old('chapter') == $chapter->id ? 'selected="selected"' : '' }}>
                                                {{ $chapter->name }}
                                            </option>
                                            @if($chapter->hasSubchapters())
                                                @foreach($chapter->subchapters as $subchapter)
                                                    <option value="{{ $subchapter->id }}" class="bg-dark">
                                                        {{ old('chapter') == $subchapter->id ? 'selected="selected"' : '' }}
                                                        >>> {{ $subchapter->name }}
                                                    </option>
                                                @endforeach
                                            @endif
                                        @endforeach
                                    </optgroup>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </div>

            <button type="submit" class="btn btn-primary pull-right save">
                {{ __('voyager::generic.save') }}
            </button>
        </form>
    </div>
@stop

@section('javascript')
<script src="{{ mix('js/forum.js') }}"></script>
@stop
