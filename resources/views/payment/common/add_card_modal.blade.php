<div class="modal" tabindex="-1" role="dialog" id="addCardModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Update New Payment Method</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">
                <form action="{{ route('payment.add_payment_method') }}" method="POST" id="card-form">
                    @csrf
                    <input type="hidden" name="payment_method">
                    <div class="form-field">
                        <label for="card-holder-name">Name</label>
                        <input id="card-holder-name" type="text">
                    </div>

                    <!-- Stripe Elements Placeholder -->
                    <div class="form-field">
                        <label for="card-element">Credit/Debit Card Information</label>
                        <div id="card-element" style='height: 42px; padding: 0 12px; padding-top: 12px; border: 1px solid lightgray; border-radius: 4px;'></div>
                        <div id="card-errors" class="error" role="alert"></div>
                    </div>

                    <div class="d-flex align-items-center justify-content-start">
                        <input type="checkbox" name="is_default_pm" id="is_default_pm" class="mr-1" @if(empty($payment_methods)) checked disabled @endif>
                        <label for="is_default_pm">Choose as default payment method</label>
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="button--primary--inverse" data-dismiss="modal">Close</button>
                <button type="submit" class="button--primary" id="card-button">Update</button>
            </div>
        </div>
    </div>
</div>
