<div class="card w-100">
    <div class="card-body">
        <h5 class="card-title">{{ $title }}</h5>
        @if(isset($subtitle) && !is_null($subtitle))
            <h6 class="card-subtitle mb-2 text-muted">{{ $subtitle }}</h6>
        @endif

        <div class="card-text">
            {!! $value !!}
            @if(isset($percentage_change) && !is_null($percentage_change))
                @if($percentage_change > 0)
                    <span class="text-success">
                        <i class="fas fa-caret-up"></i>
                        {{ $percentage_change }}%
                    </span>
                @elseif($percentage_change == 0)
                    <span class="text-light">
                        -
                        {{ $percentage_change }}
                    </span>
                @else
                    <span class="text-danger">
                        <i class="fas fa-caret-down"></i>
                        {{ $percentage_change }}%
                    </span>
                @endif
            @endif
        </div>

        {{ $slot }}

    </div>
</div>
