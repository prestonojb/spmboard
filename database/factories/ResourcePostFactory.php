<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Lesson;
use App\Resource;
use App\ResourcePost;
use Faker\Generator as Faker;

$factory->define(ResourcePost::class, function (Faker $faker) {
    return [
        'user_id' => function() {
            return factory(User::class)->states('subscribed_teacher')->create()->id;
        },
        'lesson_id' => function() {
            return factory(Lesson::class)->create()->id;
        },
        'title' => $faker->name,
        'description' => $faker->optional->sentence,
    ];
});

// Add resource to resource post
$factory->afterCreatingState(ResourcePost::class, 'with_resource', function($resource_post, Faker $faker) {
    $resource_post->resources()->attach( Resource::first()->id );
});
