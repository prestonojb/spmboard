<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTeacherInvoiceItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teacher_invoice_items', function (Blueprint $table) {
            $table->increments('id');
            $table->string('invoice_id');
            $table->unsignedInteger('lesson_id')->nullable();

            $table->string('title');
            $table->decimal('price_per_unit', 8, 2);
            $table->char('currency')->default('MYR');
            $table->unsignedInteger('no_of_units')->default(1);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teacher_invoice_items');
    }
}
