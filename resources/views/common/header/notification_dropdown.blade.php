<div class="dropdown">
    <button type="button" class="header-icon bell-icon mr-1 mr-md-2" data-toggle="dropdown"><span class="iconify" data-icon="ic:baseline-notifications-none" data-inline="false"></span></a>
    @if($user->unreadNotifications->count())
        <span class="badge badge-danger notification-badge">{{ $user->unreadNotifications->count() }}</span>
    @endif
    <div class="dropdown-menu notification-dropdown-menu">
        <h6 class="dropdown-header">Notifications</h6>
        <div class="notification-dropdown-list">
            @if($user->notifications->count())
                @php
                    $notifications_arr = [$unread_notifications, $read_notifications];
                @endphp
                {{-- Unread/Read notifications --}}
                @for($i=0; $i <= 1; $i++)
                    @foreach($notifications_arr[$i] as $notification)
                        <div class="notification-list-item position-relative @if($i==0) unread @endif">
                            {{-- Stretched link --}}
                            @if(!is_null($notification->data))
                                <a href="{{ getNotificationUrl($notification) }}" class="stretched-link"></a>
                            @else
                                <a class="stretched-link" href="javascript:void(0)"></a>
                            @endif

                            <p>{!! $notification->data['title'] !!}</p>
                            <span><small>{{ $notification->created_at->diffForHumans(null, false, true) }}</small></span>
                        </div>
                    @endforeach
                @endfor
            @else
                <div class="w-100 h-100 d-flex flex-column align-items-center justify-content-center p-5">
                    <img src="{{ asset('img/essential/no-notification.svg') }}" class="empty" width="200" height="200">
                    <p class="mb-0">No notifications yet</p>
                </div>
            @endif
        </div>
    </div>
</div>
