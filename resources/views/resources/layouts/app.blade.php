@extends('forum.layouts.app')
@section('meta')
@yield('title')
@yield('styles')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.13.0/css/selectize.default.min.css">
@endsection

@section('content')
@include('common.header2')
{{-- Smart bar --}}
@if( setting('marketing.exam_builder') )
    @component('common.smart_bar', ['title' => '🤩🤩 Customize worksheets in 10 seconds with Exam Builder. Try for FREE now! (IGCSE only)', 'url' => url('exam-builder')])
    @endcomponent
@endif

{{-- Smart bar --}}
@if( setting('marketing.beta_programme') )
    @component('common.smart_bar', ['title' => '🚀🚀🚀 Help us make Board better as a student by joining our Beta Programme now (coins rewarded)!', 'url' => "https://97vwz7jr3kb.typeform.com/to/l14r6J9r"])
    @endcomponent
@endif

<div>
    {{-- Background --}}
    <div class="resource__header"></div>

    @yield('header')

    <div class="mb-3"></div>

    <div class="container">
        @yield('breadcrumbs')
    </div>

    <div class="my-2"></div>

    <div class="container">
        @yield('context_panel')

        <hr>

        @if($exam_board->slug == 'igcse' && !(strpos(request()->path(), 'exam-builder') !== false))
            <div class="mb-2 alert alert-warning mb-0 mt-2" role="alert">
                Try out the new revision tool built for you IGCSE students! <a href="https://bit.ly/2Vx7JvS" target="_blank" class="underline-on-hover"><b>INTERESTED!</b></a>
            </div>
        @endif

        @yield('body')

        <div class="row">
            <div class="col-lg-3 mb-4">
                @yield('side')
            </div>

            <div class="col-lg-9">
                @yield('main')
            </div>
        </div>

        @yield('post_body')

        <div class="py-3"></div>
        <hr>
        <div class="py-2"></div>

        @auth
            @if(auth()->user()->is_student())
                @include('resources.banner.board_pass')
            @endif
        @else
            @include('resources.banner.board_pass')
        @endauth

        <div class="py-3"></div>

        @yield('recommendation')
    </div>

    <div class="py-5"></div>

    @include('main.navs.footer')
</div>
@endsection

@section('additional_scripts')
<script>
$(function(){
    new SlimSelect({
        select: '.bug-report-tags-select',
        placeholder: 'Tags',
        limit: 2,
        allowDeselectOption: true
    });
});
</script>
@endsection
