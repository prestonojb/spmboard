@extends('main.layouts.app')

@section('meta')
<title>About | {{ config('app.name') }}</title>
@endsection

@section('content')
<div class="bg-white">
    {{-- Main --}}
    @component('main.section.main', ['title' => 'About', 'description' => 'A new era of learning, made for the next generation', 'body' => 'When an individual learns, the whole society benefits.<br> Your knowledge can go so much further than yourself!'])
        <div class="text-center">
            <a class="button--primary lg" href="#video">
                <span class="iconify" style="font-size: 28px;" data-icon="entypo:controller-play" data-inline="false"></span>
                Watch video
            </a>
        </div>
    @endcomponent

    {{-- Our Journey --}}
    @component('main.section.standard', ['title' => 'Our Journey', 'description' => 'About a year ago, we started Board as a side project to help our community to get help more easily.<br>A lot has changed since then, but our core mission has not.', 'light_bg' => true])
    @endcomponent

    {{-- Introducing Board --}}
    @component('main.section.blue_header', ['title' => 'Introducing Board', 'subtitle' => 'A community of learners', 'body' => 'Board is an online learning marketplace, where we create a community to learn together, with close to nothing!<br> Instead, you get and you give by answering questions and getting yours answered!.'])
    @endcomponent

    {{-- Video --}}
    @component('main.common.video', ['caption' => 'All about Board', 'embed_url' => 'https://www.youtube.com/embed/9oeRsmbtnh0'])
    @endcomponent

    {{-- Is Board for me? --}}
    @php
        $data = [
            ['icon' => 'mdi:lightbulb-on-outline','title' => 'Knowledge', 'description' => 'Board is a learner’s sanctuary - no judgement, no competition and all fun.'],
            ['icon' => 'ri:team-line','title' => 'Community', 'description' => 'Have a question? Want to help with answering other\'s questions? Board is the right place for you.'],
            ['icon' => 'carbon:stacked-scrolling-1','title' => 'Resources', 'description' => 'Accessing quality resources on Board helps you ace in exams.'],
            ['icon' => 'fe:medal','title' => 'Reward', 'description' => 'Though learning is already a rewarding experience, you will get rewarded learning on Board.'],
            ['icon' => 'ant-design:star-outlined','title' => 'Experts', 'description' => 'Be connected with experts from a diverse range of industries.'],
        ];
    @endphp
    @component('main.section.icon_list', ['title' => 'Is Board for me?', 'description' => 'Board is created with the following 5 elements in mind:', 'data' => $data])
    @endcomponent


    {{-- Our Team --}}
    <div class="about__team">
        @component('main.section.blue_header', ['title' => 'Our Team', 'subtitle' => 'The people behind Board'])
        @endcomponent

        <div class="container my-4">
            <div class="row">
                @php
                    $members = [
                        ['name' => 'Andy', 'avatar' => asset('img/about/team/andy.png'), 'role' => 'Business Lead', 'description' => 'Andy is the founder of Ascension Education, he has been teaching for years and are very experienced in all aspects of the syllabus, from answering techniques to revision techniques. He is the star teacher of Ascension as he is very welcomed among the students for his empowering character and energy.'],
                        ['name' => 'Lee Hong', 'avatar' => asset('img/about/team/lee_hong.png'), 'role' => 'Resources Lead', 'description' => 'Lee Hong is one of the founding partners of Ascension, he was a First Class Masters graduate of mechanical engineering in University of Nottingham. Ever since graduating from UK, Lee Hong has been putting in all his efforts and strength to pursue education as he has the passion of making education a fun aspect for everyone!'],
                        ['name' => 'Preston', 'avatar' => asset('img/about/team/preston.jpg'), 'role' => 'Product Lead', 'description' => 'Preston is a Computer Science Student at Monash University Malaysia who enjoys coding. Preston is keen to integrate technology into education and making online learning accessible and easy to use, which led him to develop Board.'],
                        ['name' => 'Joshua', 'avatar' => asset('img/about/team/joshua.png'), 'role' => 'Marketing Lead', 'description' => 'Joshua is a Nottingham University Malaysia scholar studying Chemical Engineering. During his free time, he loves bodybuilding and devoting his time to be a better version of himself every day.']
                    ];
                @endphp
                @foreach($members as $member)
                <div class="col-12 col-md-6 mb-4">
                    <div class="panel box-shadow3 py-4 h-100">
                        <div class="text-center">
                            <div class="mb-4">
                                <h4 class="font-size7 mb-1"><b>{{ $member['name'] }}</b></h4>
                                <h5 class="gray2 font-size4 font-weight500">{{ $member['role'] }}</h5>
                            </div>
                            <div class="mb-2">
                                <img class="about__team-member-avatar" src="{{ $member['avatar'] }}" alt="{{ $member['name'] }}">
                            </div>

                            <h6 class="gray1 font-size3">
                                {{ $member['description'] }}
                            </h6>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>

    {{-- CTA Banner --}}
    @component('main.section.cta_banner', ['title' => 'Want to be part of a community of passionate students and teachers?', 'align_buttons' => true])
        <a href="{{ url('register') }}" class="h3 button primary mb-2 mr-sm-2">Join Now</a>
        <a href="{{ url('contact-us') }}" class="h3 button secondary mb-2">Contact Us</a>
    @endcomponent

    @include('main.navs.footer')
</div>
@endsection

@section('scripts')
<script>
$(function(){
    $('a[href="#video"]').click(function(e) {
        e.preventDefault();
        $('html, body').animate({
            scrollTop: $("#video").offset().top - 68
        }, 500);
    });
});
</script>
@endsection
