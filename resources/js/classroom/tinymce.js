// Reference: https://www.tiny.cloud/docs/plugins/image/
function image_upload_handler (blobInfo, success, failure, progress) {
    var xhr, formData;

    xhr = new XMLHttpRequest();
    xhr.withCredentials = false;
    xhr.open('POST', '/editor/upload');

    xhr.upload.onprogress = function (e) {
      progress(e.loaded / e.total * 100);
    };

    xhr.onload = function() {
      var json;

      if (xhr.status === 403) {
        failure('HTTP Error: ' + xhr.status, { remove: true });
        return;
      }

      if (xhr.status < 200 || xhr.status >= 300) {
        failure('HTTP Error: ' + xhr.status);
        return;
      }

      json = JSON.parse(xhr.responseText);

      if (!json || typeof json.location != 'string') {
        failure('Invalid JSON: ' + xhr.responseText);
        return;
      }

      success(json.location);
    };

    xhr.onerror = function () {
      failure('Image upload failed due to a XHR Transport error. Code: ' + xhr.status);
    };

    formData = new FormData();
    formData.append('file', blobInfo.blob(), blobInfo.filename());

    xhr.send(formData);
};

tinymce.init({
    selector: 'textarea.tinymce-editor',
    height: 250,
    block_formats: 'Header 3=h3; Header 4=h4; Paragraph=p;',
    content_css: '/css/app.css',
    content_style: "@import url('https://fonts.googleapis.com/css?family=Poppins:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i'); html,body {background-color: white;}",
    menubar: false,
    statusbar: false,
    body_class: 'tinymce-editor-contents',
    default_link_target: '_blank',
    link_default_protocol: 'https',
    remove_linebreaks : true,
    setup: function(editor) {
        editor.on('init', function(e) {
            $('.loading-icon').hide();
        });
    },
    max_height: 900,
    table_toolbar: "tableprops tabledelete | tableinsertrowbefore tableinsertrowafter tabledeleterow | tableinsertcolbefore tableinsertcolafter tabledeletecol tabledeletecol",
    plugins: [
        'advlist autoresize autolink lists link image charmap print preview anchor',
        'searchreplace visualblocks code fullscreen',
        'insertdatetime media table paste code wordcount'
    ],
    toolbar: 'formatselect | ' +
        'bold italic underline | alignleft aligncenter ' +
        'alignright alignjustify | table link image | bullist numlist | ' +
        'forecolor backcolor | removeformat',
    images_upload_handler: image_upload_handler,
});
