<?php

namespace App\Http\Controllers;

use App\Classroom;
use App\ExamBoard;
use App\HomeworkSubmission;
use App\Mail\EbooksPromotion;
use App\Question;
use App\State;
use App\Student;
use App\StudentParent;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

class StudentController extends Controller
{
    /**
     * Getting Started View
     */
    public function getGettingStartedView()
    {
        $states = State::all();
        $exam_boards = ExamBoard::all();
        return view('getting_started.student', compact('states', 'exam_boards'));
    }

    /**
     * Update Student Profile
     */
    public function update()
    {
        $user = auth()->user();
        $student = $user->student;

        // Validation
        request()->validate([
            'profile_img' => 'nullable|mimes:jpeg,png,jpg,gif,svg',
            'headline' => 'nullable|max:50',
            'state' => 'nullable',
            'school' => 'nullable|max:80',
        ]);

        // Update user columns
        $user->update([
            'name' => request('name'),
            'headline' => request('headline'),
            'state_id' => request('state'),
        ]);

        // Update student columns
        $student->update([
            'school' => request('school'),
            'exam_board_id' => request('exam_board'),
        ]);

        // Update Avatar if has profile image in request
        $has_profile_img = request()->has('profile_img') && !is_null( request()->file('profile_img') );
        if( $has_profile_img ) {
            $user->updateAvatar( request()->file('profile_img') );
        }

        $exam_board_slug = !is_null($student->exam_board) ? $student->exam_board->slug : "igcse";
        Session::flash('success', 'Profile updated successfully!');
        return request()->getting_started ? redirect($exam_board_slug)
                                          : redirect()->back();
    }

    public function show_progress_in_class()
    {
        $userId = auth()->user()->id;
        $student = auth()->user()->student;
        $classroom_id = request()->classroom;
        $classroom = Classroom::where('id', $classroom_id)->first();

        $homework_marks_data = [];

        $startDt = Carbon::now()->subMonths(6);
        $endDt = Carbon::now();

        $period = CarbonPeriod::create($startDt, '1 month', $endDt);

        if( isset($classroom) && !is_null($classroom) ) {
            // $questions_asked_data = [];
            // foreach( $classroom->subject->chapters as $chapter ) {
            //     $chapterId = $chapter->id;

            //     //Questions from this chapter
            //     $no_of_questions = Question::where('user_id', $userId )->whereHas('chapters', function($query) use ($chapterId) {
            //         $query->where('chapters.id', '=', $chapterId);
            //     })->count();

            //     $questions_asked_data[ $chapter->name ] = $no_of_questions;
            // }

            // $questions_answered_data = [];
            // foreach( $classroom->subject->chapters as $chapter ) {
            //     $chapterId = $chapter->id;

            //     //Questions from this chapter with answers from user
            //     $no_of_questions = Question::whereHas('chapters', function($query) use ($chapterId) {
            //         $query->where('chapters.id', '=', $chapterId);
            //     })->whereHas('answers', function($query) use ($userId){
            //         $query->where('answers.user_id', $userId);
            //     })
            //     ->count();

            //     $questions_answered_data[ $chapter->name ] = $no_of_questions;
            // }

            foreach ($period as $date) {
                $month = $date->format('m');
                $year = $date->format('Y');

                $homework_ids = $classroom->homeworks()->pluck('id');

                $homework_mark_avg = HomeworkSubmission::whereIn('homework_id', $homework_ids)
                                    ->whereMonth('created_at', $month)
                                    ->whereYear('created_at', $year)
                                    ->avg('mark');

                $homework_marks_data[ $date->format('M Y') ] = $homework_mark_avg;
            }
        } else {

            foreach ($period as $date) {
                $month = $date->format('m');
                $year = $date->format('Y');

                $homework_ids = $student->get_homework_ids_of_classrooms();

                $homework_mark_avg = HomeworkSubmission::whereIn('homework_id', $homework_ids)
                                    ->whereMonth('created_at', $month)
                                    ->whereYear('created_at', $year)
                                    ->avg('mark');

                $homework_marks_data[ $date->format('M Y') ] = $homework_mark_avg;
            }
        }

        return view('classrooms.students.show', compact('classroom', 'student', 'homework_marks_data'));
    }

    /**
     * Returns student's parent email, else return null
     *  @return App\StudentParent|null
     */
    public function getParent()
    {
        $student = Student::where('user_id', request()->student)->first();
        return $student->parent;
    }
}
