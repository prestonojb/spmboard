@extends('resources.layouts.app')
@section('meta')
    <title>{{ $guide->title }} - Guides | {{ $exam_board->name }} {{ config('app.name') }} </title>
@endsection

@section('header')
    @component('resources.common.header', ['type' => 'guide', 'exam_board' => $exam_board])
    @endcomponent
@endsection

@section('breadcrumbs')
    @component('resources.common.breadcrumbs')
    <a href="{{ route('guides.home', $exam_board->slug) }}">Home</a> /
    <a href="{{ route('guides.subject_show', [$exam_board->slug, $guide->subject->id]) }}">{{ $guide->subject->name }}</a> /
    {{ str_limit($guide->title, 30) }}
    @endcomponent
@endsection

@section('context_panel')
    @component('resources.common.context_panel', ['exam_board' => $guide->subject->exam_board->name, 'title' => $guide->title, 'description' => $guide->description])
        <h3 class="font-size3">Part of <a href="{{ route('guides.subject_show', [$guide->subject->exam_board->slug, $guide->subject->id]) }}" class="font-size-inherit blue-underline">{{ $guide->subject->name }}</a> |
            <a href="javascript:void(0)" class="font-size-inherit blue-underline">{{ $guide->chapter->name ?? 'General' }}</a>
        </h3>

        {{-- Toolbar --}}
        <div class="py-1 my-1 border-left-gray5-3px pl-2">
            @auth
                {{-- Save --}}
                <button type="button" class="gray-button sm" data-toggle="modal" data-target="#saveToLibraryModal">
                    <span class="iconify" data-icon="cil:playlist-add" data-inline="false"></span>Save</button>

                    @component('resources.common.save_to_library', ['resource' => $guide->resource])
                    @endcomponent
            @else
                <a href="{{ route('login') }}" class="gray-button sm">
                    <span class="iconify" data-icon="cil:playlist-add" data-inline="false"></span>Save</button></a>
            @endauth
        </div>
    @endcomponent
@endsection

@section('side')
    @component('resources.guides.common.chapter_menu', ['current_guide' => $guide])
    @endcomponent
@endsection

@section('main')
    <div class="maxw-740px mx-auto">
        {{-- Edit --}}
        @can('edit', $guide)
            <a target="_blank" href="{{ route('voyager.guides.edit', $guide->id) }}" class="d-inline-block underline-on-hover mr-1 mb-2"><span class="iconify" data-icon="bx:bx-edit" data-inline="false"></span> Edit</a>
        @endcan

        {{-- Delete --}}
        @can('delete', $guide)
            <form action="{{ route('guides.delete', [$exam_board->slug, $guide->id]) }}" method="POST" class="d-inline">
                @csrf
                @method('delete')
                <button class="red button-link underline-on-hover deleteButton">
                    <span class="iconify" data-icon="carbon:delete" data-inline="false"></span>
                    Delete
                </button>
            </form>
        @endcan

        @php
            $tag = 'resource';
            $description = '[Guide ID: '.$guide->id.'] '.$guide->title.' has error.';
            $email = auth()->check() ? auth()->user()->email : '';
        @endphp
        @component('resources.common.share_and_report_toolbar', ['tag' => $tag, 'description' => $description, 'email' => $email])
        @endcomponent


        <div class="gray2 font-size2 mb-2"><b>{{ views($guide->resource)->count() }} views</b> · Updated {{ $guide->updated_at->diffForHumans() }}</div>
        <div class="panel">
            {{-- Guide Body --}}
            <article class="tinymce-editor-contents">
                {!! $guide->body !!}
            </article>
        </div>

        <div class="py-2"></div>

        {{-- Guide Prev Next Buttons --}}
        @component('resources.guides.common.prev_next', ['guide' => $guide])
        @endcomponent

        <div class="py-1"></div>

        @component('resources.common.share_and_report_toolbar', ['tag' => $tag, 'description' => $description, 'email' => $email])
        @endcomponent

        <div class="py-3"></div>

        @component('resources.common.author_details', ['user' => $guide->getAuthor()])
        @endcomponent
    </div>
@endsection

@section('recommendation')
    {{-- Related Guides by Chapter --}}
    {{-- <h4 class="font-size5 border-bottom pb-2 mb-2"><b>More from {{ $guide->chapter->name }}</b></h4>
    @if(count($guide->similarByChapter() ?? []))
        <div class="row">
            @foreach($guide->similarByChapter() as $guide)
                <div class="col-12 col-lg-4">
                    <h5 class="mb-1">
                        <a href="{{ route('guides.show', [$guide->subject->exam_board->slug, $guide->id]) }}" class="font-size5 text-dark">{{ $guide->title }}</a>
                    </h5>
                    <p class="gray2 font-size3">{{ str_limit($guide->description, 100) }}</p>
                </div>
            @endforeach
        </div>
    @else
        <p class="py-3 font-size3 gray2 text-center">No related guides</p>
    @endif

    <div class="py-3"></div> --}}

    {{-- Related Guides by Subject --}}
    {{-- <h4 class="font-size5 border-bottom pb-2 mb-2"><b>More from {{ $guide->subject->name }}</b></h4>
    @if(count($guide->similarBySubject() ?? []))
        <div class="row">
            @foreach($guide->similarBySubject() as $guide)
                <div class="col-12 col-lg-4">
                    <h5 class="mb-1">
                        <a href="{{ route('guides.show', [$guide->subject->exam_board->slug, $guide->id]) }}" class="font-size5 text-dark">{{ $guide->title }}</a>
                    </h5>
                    <p class="gray2 font-size3">{{ str_limit($guide->description, 100) }}</p>
                </div>
            @endforeach
        </div>
    @else
        <p class="py-3 font-size3 gray2 text-center">No related guides</p>
    @endif --}}
@endsection
