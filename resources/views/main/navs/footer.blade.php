<footer class="main__footer bg-white">
    <div class="py-3"></div>
     <div class="container text-center text-md-left">
        <div class="row">
            <div class="col-md-4 mt-md-0">
                <img src="{{ asset('img/logo/logo_light.png') }}" alt="" width="250" class="mb-3">
                <p>{{ config('app.name') }} is an Edu-Tech company based in Malaysia.</p>
            </div>

            <div class="col-md-2 mb-md-0 mb-3">
                <hr class="d-md-none pb-3">

                <h5 class="text-uppercase">Company</h5>
                <ul class="list-unstyled">
                    <li>
                        <a href="{{ route('about') }}">About Us</a>
                    </li>
                </ul>
            </div>

            <div class="col-md-2 mb-md-0 mb-3">
                <h5 class="text-uppercase">Products</h5>
                <ul class="list-unstyled">
                    <li>
                        <a href="#">Coins</a>
                    </li>
                    <li>
                        <a href="{{ route('classroom.landing') }}">Classroom</a>
                    </li>
                </ul>
            </div>

            <div class="col-md-2 mb-md-0 mb-3">
                <h5 class="text-uppercase">Services</h5>
                <ul class="list-unstyled">
                    {{-- <li>
                        <a href="{{ route('physical_resources') }}">Printed Resources</a>
                    </li> --}}
                    <li>
                        <a href="https://bit.ly/board_made_tutor" target="_blank">Become A Tutor</a>
                    </li>
                </ul>
            </div>

            <div class="col-md-2 mb-md-0 mb-3">
                <h5 class="text-uppercase">Help</h5>
                <ul class="list-unstyled">
                    <li>
                        <a href="{{ route('faq') }}">FAQ</a>
                    </li>
                    <li>
                        <a href="/contact-us">Contact Us</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>


    <div class="container">
        <hr class="mb-1">
        <div class="row justify-content-between align-items-start main__footer-social-accounts py-4">
            <div class="col-12 col-md-6 col-lg-4 text-center text-md-left mb-3 mb-md-0 main">
                <h6 class="heading"><b>Follow us on</b></h6>
                <div>
                    <a target="_blank" href="https://www.linkedin.com/company/31002139/" class="linkedin"><span class="iconify" data-icon="ant-design:linkedin-filled" data-inline="false"></span></span></a>
                    <a target="_blank" href="#" class="youtube"><span class="iconify" data-icon="ant-design:youtube-filled" data-inline="false"></span></span></a>
                </div>
            </div>

            <div class="col-12 col-md-6 col-lg-4 text-center text-md-left sub">
                <div class="form-row">
                    <div class="col-6">
                        <span class="sub-heading">For <b>SPM</b></span>
                        <div>
                            <a target="_blank" href="https://www.facebook.com/spmboardmy" class="facebook"><span class="iconify" data-inline="false" data-icon="ant-design:facebook-filled" style="font-size: 38px;"></span></a>
                            <a target="_blank" href="https://www.facebook.com/spmboardmy/" class="instagram"><span class="iconify" data-inline="false" data-icon="fa-brands:instagram" style="font-size: 38px;"></span></a>
                        </div>
                    </div>
                    <div class="col-6">
                        <span class="sub-heading">For <b>IGCSE</b></span>
                        <div>
                            <a target="_blank" href="https://www.facebook.com/igcseboardmy" class="facebook"><span class="iconify" data-inline="false" data-icon="ant-design:facebook-filled" style="font-size: 38px;"></span></a>
                            <a target="_blank" href="https://www.facebook.com/igcseboardmy/" class="instagram"><span class="iconify" data-inline="false" data-icon="fa-brands:instagram" style="font-size: 38px;"></span></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="bg-gray6 px-1 py-4">
        <div class="container">
            <div class="row">
                <div class="col-md-6 text-center text-md-left mb-2 mb-md-0">
                    <a href="{{ url('privacy-policy') }}" class="d-inline-block mr-1">Privacy Policy</a>
                    <a href="{{ url('terms-of-service') }}" class="d-inline-block mr-1">Terms Of Service</a>
                    <a href="{{ url('faq') }}" class="d-inline-block mr-1">FAQ</a>
                </div>
                <div class="col-md-6 text-center text-md-right">
                    Board {{ now()->year }}, All rights reserved
                </div>
            </div>
        </div>
    </div>
</footer>
