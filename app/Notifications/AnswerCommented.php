<?php

namespace App\Notifications;

use App\AnswerComment;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class AnswerCommented extends Notification implements ShouldQueue
{
    use Queueable;

    public $answer_comment;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(AnswerComment $answer_comment)
    {
        $this->answer_comment = $answer_comment;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'type' => AnswerComment::class,
            'target' => $this->answer_comment,
            'title' => "<b>{$this->answer_comment->user->username}</b> commented on your answer on <b>{$this->answer_comment->answer->question->subject->exam_board_and_name}</b>.",
        ];
    }
}
