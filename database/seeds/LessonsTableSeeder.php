<?php

use App\Classroom;
use App\Lesson;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class LessonsTableSeeder extends Seeder
{
    protected $NO_OF_LESSONS = 100;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Lesson::truncate();
        $faker = \Faker\Factory::create();

        for($i = 0; $i < $this->NO_OF_LESSONS; $i++) {
            $classroom = Classroom::inRandomOrder()->first();
            $start_at = Carbon::createFromTimeStamp($faker->dateTimeBetween('-1 years', '+1 month')->getTimestamp());
            $end_at = $start_at->copy()->addHours( $faker->numberBetween( 1, 5 ) );

            Lesson::create([
                'classroom_id' => $classroom ? $classroom->id : null,
                'name' => $faker->sentence(3),
                'description' => $faker->optional()->paragraph,
                'start_at' => $start_at->format('Y-m-d H:i:s'),
                'end_at' => $end_at->format('Y-m-d H:i:s'),
            ]);
        }
    }
}
