<div class="panel p-0 mb-3">
    <div class="panel-header">
        <a href="{{ route('classroom.lessons.resource_posts.show', [$resource_post->lesson->id, $resource_post->id]) }}" class="underline-on-hover text-dark">
            <h3 class="position-relative font-size5 mb-1">
                {{ $resource_post->primary }}
            </h3>
        </a>
        <h4 class="font-size1 gray2 mb-0">
            {!! $resource_post->secondary !!}
        </h4>
    </div>

    @if( count($resource_post->resources ?? []) || count($resource_post->custom_resource_urls() ?? []) )
        <div class="panel-body">
            @if($resource_post->instruction)
                <p class="mb-2">{{ $resource_post->instruction }}</p>
            @endif

            @component('classroom.resource_posts.common.file_block_container', ['resource_post' => $resource_post])
            @endcomponent
        </div>
    @else
        <div class="panel-body">
            <div class="text-center py-2 gray2">
                No data
            </div>
        </div>
    @endif

</div>

