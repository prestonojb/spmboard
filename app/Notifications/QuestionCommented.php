<?php

namespace App\Notifications;

use App\QuestionComment;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class QuestionCommented extends Notification implements ShouldQueue
{
    use Queueable;

    public $question_comment;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(QuestionComment $question_comment)
    {
        $this->question_comment = $question_comment;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'type' => QuestionComment::class,
            'target' => $this->question_comment,
            'title' => "<b>{$this->question_comment->user->username}</b> commented on your question on {$this->question_comment->question->subject->exam_board_and_name}.",
        ];
    }
}
