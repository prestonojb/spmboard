<?php

namespace App\Http\Middleware;

use Closure;

class AdminAuthenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if( auth()->check() ) {
            if( auth()->user()->hasPermission('browse_admin') ) {
                return $next($request);
            }
            return redirect()->route('/');
        } else {
            return redirect()->route('voyager.login');
        }

    }
}
