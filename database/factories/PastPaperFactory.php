<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\PastPaper;
use App\Subject;
use Faker\Generator as Faker;

$subjects = Subject::all();
$seasons = ['Feb/March', 'May/June', 'Oct/Nov'];

$factory->define(PastPaper::class, function (Faker $faker) use ($subjects, $seasons) {
    $subject = $subjects->random();
    return [
        'name' => $faker->sentence(3),
        'subject_id' => $subject->id,
        'year' => rand(2005, 2020),
        'paper_number' => rand(1, 6),
        'qp_url' => 'https://www.w3.org/WAI/ER/tests/xhtml/testfiles/resources/pdf/dummy.pdf',
        'ms_url' => 'https://www.w3.org/WAI/ER/tests/xhtml/testfiles/resources/pdf/dummy.pdf',
        'code' => getRandomString(6),
        'season' => $seasons[ rand(0, count($seasons) - 1) ],
        'type' => null
    ];
});
