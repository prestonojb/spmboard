@component('mail::message')
# Invoice Sent
## Hi {{ $invoice->parent->name }}, kindly check the attached invoice for your child {{ $invoice->student->name }}.
*Thank you in advance for your payment on time.*

### Invoice Details
@component('mail::panel')
Due At: {{ $invoice->due_at->toFormattedDateString() }}

Amount: MYR{{ $invoice->amount }}
@endcomponent


Regards,

{{ $invoice->teacher->name }}
@endcomponent
