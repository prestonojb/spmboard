@extends('forum.layouts.app')

@section('meta')
<title>Past Year Papers | {{ $exam_board }} {{ config('app.name') }}</title>
@endsection

@section('styles')
{{-- Magnific Popup core CSS file --}}
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.css">
@endsection

@section('content')
@include('common.header2')
<section class="notes">
    <header class="notes__header">
        <div class="container">
            <h1><b>Past Papers</b></h1>
            <h2 class="h4">Practice makes perfect!</h2>
        </div>
    </header>
    <section class="container notes__body">

        @include('resources.past_papers.common.header')

        <div class="learn__breadcrumbs">
            <a href="{{ route('guides.landing', $exam_board) }}">Home</a> /
            {{ $subject->name }}
        </div>
        <div class="mb-3">
            <h3 class="learn__heading">{{ $subject->name }}</h3>
            <h4 class="learn__sub-heading">{{ $subject->description }}</h4>
            <p class="gray mb-1">Total: {{ count($subject->guides) }} @if(count($subject->guides) > 1) guides @else guide @endif</p>
        </div>

        <div class="row flex-lg-row-reverse">
            <div class="col-12 col-lg-3 p-2">
                @include('resources.past_papers.common.filter')
            </div>

            <div class="col-12 col-lg-9 p-2">
                <ul class="nav nav-tabs nav--tabs" id="notesTab" role="tablist">
                    <li class="nav-item">
                      <a class="nav-link active" href="{{ url('past-papers') }}">Available</a>
                    </li>
                </ul>
                <div class="tab-content" id="notesTabContent">
                    <div class="tab-pane fade show active" id="available" role="tabpanel" aria-labelledby="available-tab">
                        @if($past_papers->count() > 0)

                        <div class="notes__results-count">Showing {{ $past_papers->firstItem() }} - {{ $past_papers->lastItem() }} of {{ $past_papers->total() }} results</div>

                        @foreach($past_papers as $paper)
                            @include('common.past_paper')
                        @endforeach

                        {{ $past_papers->appends(request()->query())->links() }}

                        @else
                            <div class="note__empty">
                                <img src="{{ asset('img/essential/empty.svg') }}" class="mb-2" width="150">
                                <h4>No papers yet</h4>
                                <h5>Stay tuned for more.</h5>
                            </div>
                        @endif

                    </div>
                  </div>
            </div>
        </div>
    </section>
</section>
@endsection

@section('scripts')
{{-- Sweet Alert 2 --}}
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
{{-- Magnific Popup core JS file --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.js"></script>
<script src="{{ asset('js/slim_select.js') }}"></script>
<script src="{{ asset('js/past-papers.js') }}"></script>
@endsection

@section('scripts')
@endsection
