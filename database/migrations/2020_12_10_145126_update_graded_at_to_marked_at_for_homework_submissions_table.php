<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateGradedAtToMarkedAtForHomeworkSubmissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('homework_submissions', function (Blueprint $table) {
            $table->renameColumn('graded_at', 'marked_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('homework_submissions', function (Blueprint $table) {
            $table->renameColumn('marked_at', 'graded_at');
        });
    }
}
