@extends('voyager::master')

@section('page_title', 'Generate Topical Paper with Exam Builder')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_header')
<div class="container-fluid">
    <h1 class="page-title">
        [Exam Builder] Generate Topical Papers
    </h1>
</div>
@stop

@section('content')
    <div class="page-content container-fluid">
        <form class="form-edit-add" role="form"
              action="{{ route('voyager.exam_builder_sheets.topical.generate') }}"
              method="POST" enctype="multipart/form-data" autocomplete="off">
            {{ csrf_field() }}

            <div class="panel panel-bordered">
                <div class="panel">
                    <p>Generate all the exam builder items of a chapter into one PDF. Retrieve the result PDFs from the user exam builder sheet page.</p>
                    <p>Please be patient as generating all the questions take some time.</p>

                    <div class="panel-body">
                        {{-- Name --}}
                        <div class="form-group">
                            <label for="">Name</label>
                            <input type="text" name="name" class="form-control" value="{{ old('name') }}" placeholder="Name">
                            @error('name')
                                <div class="text-danger">{{ $message }}</div>
                            @enderror
                        </div>

                        {{-- Subject --}}
                        <div class="form-group">
                            <label>Subject</label>
                            <select class="subject-select" name="subject">
                                <option data-placeholder="true"></option>
                                @foreach($subjects as $subject)
                                    <option value="{{ $subject->id }}" {{ old('subject') == $subject->id ? 'selected="selected"' : '' }}>{{ $subject->name_with_exam_board }}</option>
                                @endforeach
                            </select>
                            @error('subject')
                                <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>

                        {{-- Chapters --}}
                        <div class="form-group">
                            <label>Chapters</label>
                            <select class="chapter-select" name="chapter" disabled>
                                <option></option>
                                @foreach($subjects as $subject)
                                    <optgroup label="{{ $subject->name_with_exam_board }}">
                                        @foreach($subject->chapters as $chapter)
                                            <option value="{{ $chapter->id }}"
                                                {{ old('chapter') == $chapter->id ? 'selected="selected"' : '' }}>
                                                {{ $chapter->name }}
                                            </option>
                                            @if($chapter->hasSubchapters())
                                                @foreach($chapter->subchapters as $subchapter)
                                                    <option value="{{ $subchapter->id }}" class="bg-dark">
                                                        {{ old('chapter') == $subchapter->id ? 'selected="selected"' : '' }}
                                                        >>> {{ $subchapter->name }}
                                                    </option>
                                                @endforeach
                                            @endif
                                        @endforeach
                                    </optgroup>
                                @endforeach
                            </select>
                        </div>

                        {{-- Seasons --}}
                        <div class="form-group">
                            <label for="">Seasons</label>
                            <select name="seasons[]" id="" multiple>
                                <option value="" disabled selected>Select seasons</option>
                                @php
                                    $seasons = [
                                        'FM' => 'Feb/Mar',
                                        'MJ' => 'May/June',
                                        'ON' => 'Oct/Nov',
                                    ];
                                @endphp
                                @foreach($seasons as $key=>$value)
                                    <option value="{{ $key }}">{{ $value }}</option>
                                @endforeach
                            </select>
                            <p><i>*Hold CTRL to select multiple seasons.</i></p>
                        </div>

                        {{-- Paper Number --}}
                        <div class="form-group">
                            <label for="">Paper Number</label>
                            <select name="paper_number" id="">
                                <option value="" disabled selected>Select Paper Number</option>
                                <option value="2">2</option>
                                <option value="4">4</option>
                                <option value="6">6</option>
                            </select>
                            @error('paper_number')
                                <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>

                        {{-- Min Year --}}
                        <div class="form-group">
                            <label for="">Min Year</label>
                            <select name="min_year" id="">
                                <option value="" disabled selected>Leave empty if no min year</option>
                                @foreach(range(2025, 1980, -1) as $i)
                                    <option value="{{ $i }}">{{ $i }}</option>
                                @endforeach
                            </select>
                        </div>

                        {{-- Max Year --}}
                        <div class="form-group">
                            <label for="">Max Year</label>
                            <select name="max_year" id="">
                                <option value="" disabled selected>Leave empty if no max year</option>
                                @foreach(range(2025, 1980, -1) as $i)
                                    <option value="{{ $i }}">{{ $i }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </div>

            <div class="text-right">
                <a href="{{ route('exam_builder.landing') }}" class="btn btn-warning" target="_blank">Show all sheets</a>
                <button type="submit" class="btn btn-primary save">
                    Generate Topical Paper
                </button>
            </div>
        </form>
    </div>
@stop

@section('javascript')
<script src="{{ mix('js/forum.js') }}"></script>
@stop
