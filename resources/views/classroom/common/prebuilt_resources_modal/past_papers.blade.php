<div class="modal fade prebuilt-resources-modal prebuilt-pp-modal show" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-xl">
      <div class="modal-content">
          <div class="modal-header">
              Past Papers
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
          </div>

          <div class="modal-body">
                <div class="row">

                    @foreach(range(2020, 2010, -1) as $year)
                        @if(count($subject->getPastPapersByYear($year) ?? []))
                            <div class="col-12">
                                <div class="mb-4">
                                    <h4 class="h3 border-bottom border-gray3 pb-1 mb-3"><b>{{ $year }}</b></h4>
                                    <div class="form-row">
                                        @foreach($subject->getPastPapersByYear($year) as $past_paper)
                                            <div class="col-12 col-md-6">
                                                @component('classroom.common.teacher.resources.past_paper_item', ['past_paper' => $past_paper])
                                                @endcomponent
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        @endif
                    @endforeach

                </div>
          </div>

          <div class="modal-footer">
            <button class="button--primary--inverse" data-dismiss="modal">Close</button>
            <button class="button--primary addResourceButton">Add</button>
        </div>
      </div>
    </div>
  </div>
