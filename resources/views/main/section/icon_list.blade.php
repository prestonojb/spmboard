<section class="@if(isset($bg_light) && $bg_light) bg-light @endif">
    <div class="container">
        <div class="py-5 text-center">
            <div class="mb-3 mb-md-4">
                <div class="mb-5">
                    <h2 class="font-size9"><b>{{ $title }}</b></h2>
                    @if(isset($description) && !is_null($description))
                        <p class="gray2 font-size5 font-weight500">{!! $description !!}</p>
                    @endif
                </div>
            </div>
            <div class="row justify-content-center">
                @foreach($data as $datum)
                    <div class="col-12 col-md-4 mb-3">
                        @if(isset($custom_img) && $custom_img)
                            <img src="{{ $datum['custom_img'] }}" alt="">
                        @else
                            <span class="iconify blue6 mb-3" style="font-size: 48px;" data-icon="{{ $datum['icon'] }}" data-inline="false"></span>
                        @endif
                        <h3 class="font-size5">{{ $datum['title'] }}</h3>
                        <h4 class="gray2 font-size3 font-weight400">{!! $datum['description'] !!}</h4>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</section>
