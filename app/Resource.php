<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use CyrildeWit\EloquentViewable\Viewable;
use CyrildeWit\EloquentViewable\Contracts\Viewable as ViewableContract;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;
use Org_Heigl\Ghostscript\Ghostscript;
use Illuminate\Http\File;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Log;
use Amplitude;

class Resource extends Model implements ViewableContract
{
    use Viewable;

    protected $removeViewsOnDelete = true;
    protected $guarded = [];
    public $additional_attributes = ['name'];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('resourceable', function (Builder $builder) {
            $builder->whereHasMorph('resourceable', ['App\PastPaper', 'App\TopicalPastPaper', 'App\Notes', 'App\Guide'], function(Builder $q){
                $q->whereNull('deleted_at');
            });
        });
    }

    /**
     * Returns sellable resource types
     * @return array
     */
    public static function getSellableTypes()
    {
        return ['Topical Paper', 'Note'];
    }

    /**
     * Returns sellable resource types
     * @return array
     */
    public static function getSellableClasses()
    {
        return [TopicalPastPaper::class, Notes::class];
    }

    /**
     * Returns true if resource is sellable, else false
     * @return boolean
     */
    public function isSellable()
    {
        return in_array($this->type, self::getSellableTypes());
    }

    /**
     * ---------------
     * Relationships
     * ---------------
     */

    public function resourceable()
    {
        return $this->morphTo();
    }

    public function users()
    {
        return $this->belongsToMany(User::class)->withTimestamps();
    }

    public function past_papers()
    {
        return Resource::where('resourceable_type', PastPaper::class);
    }

    public function notes()
    {
        return Resource::where('resourceable_type', Notes::class);
    }

    public function resource_posts()
    {
        return $this->belongsToMany( ResourcePost::class, 'resource_resource_post' )->withPivot('type');
    }

    public function reviews()
    {
        return $this->hasMany(ResourceReview::class, 'resource_id', 'id');
    }

    /**
     * Returns owner of resource (limited to resources with sellers)
     * @return App\User
     */
    public function owner()
    {
        return $this->resourceable->owner;
    }

    /**
     * --------------
     * Accessors
     * --------------
     */
    /**
     * Returns primary attribute
     * @return string
     */
    public function getPrimaryAttribute()
    {
        return !is_null($this->resourceable) ? $this->resourceable->primary
                                             : null;
    }

    /**
     * Returns secondary attribute
     * @return string
     */
    public function getSecondaryAttribute()
    {
        return !is_null($this->resourceable) ? $this->resourceable->secondary
                                             : null;
    }

    public function getNameAttribute()
    {
        return !is_null($this->resourceable) ? $this->resourceable->name
                                             : null;
    }

    public function getDataIconAttribute()
    {
        switch($this->resourceable_type) {
            case 'App\TopicalPastPaper':
                return 'carbon:category-new-each';
            case 'App\Notes':
                return 'mdi-light:note-text';
            case 'App\PastPaper':
                return 'carbon:stacked-scrolling-1';
            case 'App\Guide':
                return 'ic:outline-article';
            default:
                return 'ant-design:file-outlined';
        }
    }

    public function getTypeAttribute()
    {
        switch($this->resourceable_type) {
            case 'App\TopicalPastPaper':
                return 'Topical Paper';
            case 'App\Notes':
                return 'Note';
            case 'App\PastPaper':
                return 'Past Paper';
            case 'App\Guide':
                return 'Guide';
        }
        return null;
    }

    /**
     * Returns preview image URL
     * @return string
     */
    public function getPreviewImgAttribute()
    {
        return $this->resourceable->preview_img;
    }

    /**
     * Returns rating (out of 5)
     * @return int
     */
    public function getRatingAttribute()
    {
        return number_format($this->reviews()->avg('rating'), 1);
    }

    /**
     * Returns show URL
     * @return string
     */
    public function getShowUrlAttribute()
    {
        return !is_null($this->resourceable) ? $this->resourceable->show_url
                                             : null;
    }

    /**
     * Returns URL
     */
    public function getUrlAttribute()
    {
        $resource_id = $this->resourceable->id;
        $exam_board_slug = $this->resourceable->subject->exam_board->slug;

        switch($this->resourceable_type) {
            case 'App\TopicalPastPaper':
                return route('topical_past_papers.qp.show', [$exam_board_slug, $resource_id] );
            case 'App\Notes':
                return route('notes.show', [$exam_board_slug, $resource_id] );
            case 'App\PastPaper':
                return $this->resourceable->qp_url;
            case 'App\Guide':
                return $this->resourceable->url;
        }
        return 'javascript:void(0)';
    }

    /**
     * Returns QP URL
     * @return string
     */
    public function getQpUrlAttribute()
    {
        return !is_null($this->resourceable) ? $this->resourceable->qp_url
                                             : null;
    }

    /**
     * Returns MS URL
     * @return string
     */
    public function getMsUrlAttribute()
    {
        return !is_null($this->resourceable) ? $this->resourceable->ms_url
                                             : null;
    }

    /**
     * Returns URL relative path
     */
    public function getUrlRelativePathAttribute()
    {
        return $this->resourceable->url_relative_path;
    }

    public function getQpRouteAttribute()
    {
        return !is_null($this->resourceable) ? $this->resourceable->qp_route
                                             : null;
    }

    public function getMsRouteAttribute()
    {
        return !is_null($this->resourceable) ? $this->resourceable->ms_route
                                             : null;
    }

    public function getRouteAttribute()
    {
        return !is_null($this->resourceable) ? $this->resourceable->route
                                             : null;
    }

    public function getCoinPriceAttribute()
    {
        return !is_null($this->resourceable) ? $this->resourceable->coin_price
                                             : null;
    }

    /**
     * Returns all tags HTML
     * @return string
     */
    public function getAllTagsHtml()
    {
        return $this->resourceable->getAllTagsHtml();
    }

    /**
     * Get Resources by subject
     * @var App\Subject $subject
     *
     * @return Collection $resources
     */
    public static function getIdsBySubject( Subject $subject )
    {
        $resources = collect();
        foreach( Resource::all() as $resource ) {
            if( $resource->resourceable->subject->id == $subject->id ) {
                $resources->push( $resource );
            }
        }

        return $resources->pluck('id')->toArray();
    }

    public static function getBySubject( Subject $subject )
    {
        return Resource::find( self::getIdsBySubject($subject) );
    }

    /**
     * Get number of unlocks
     * @return int
     */
    public function getNoOfUnlocks()
    {
        return $this->users()->count();
    }

    /**
     * Get coins made (for sellers)
     * @return int
     */
    public function getCoinsMade()
    {
        if(!$this->isSellable()) {
            throw new \Exception("Resource is not sellable.");
        }
        return $this->getNoOfUnlocks() * $this->resourceable->coin_price;
    }

    /**
     * Returns true if resource is free, else false
     * @return boolean
     */
    public function isFree()
    {
        if(!$this->isSellable()) {
            throw new \Exception("Resource is not sellable.");
        }
        return $this->resourceable->isFree();
    }

    /**
     * Returns true if resource is paid, else false
     * @return boolean
     */
    public function isPaid()
    {
        return !$this->resourceable->isFree();
    }

    /**
     * Returns total reviews
     * @return int
     */
    public function getTotalReviews()
    {
        return $this->reviews->count();
    }

    /**
     * Update all resource preview images
     * @return string String information about failed updates
     */
    public static function updateAllPreviewImages()
    {
        $return_msg = "";

        $notes = Notes::whereNull('preview_img')->get();
        foreach($notes as $note) {
            try {
                $note->updatePreviewImage();
            } catch (\Exception $e) {
                Log::debug($e->getMessage());
                $return_msg .= "Failed updating note #{$note->id}";
            }
        }

        $topical_past_papers = TopicalPastPaper::whereNull('preview_img')->get();
        foreach($topical_past_papers as $paper) {
            try {
                $paper->updatePreviewImage();
            } catch (\Exception $e) {
                Log::debug($e->getMessage());
                $return_msg .= "Failed updating topical paper #{$paper->id}";
            }
        }
        return $return_msg == "" ? "Updated successfully"
                                 : $return_msg;
    }

    /**
     * Save preview image (first page of PDF file)
     * Temporary public File directory: public/temp/resources
     * Temporary public Preview Image directory: public/temp/preview_imgs
     * S3 Preview Image directory: resources/preview_imgs
     * @param UploadedFile $file
     */
    public function savePreviewImage($file)
    {
        if(!isPdfFile($file)) {
            throw new \Exception("File must be a PDF!");
        }

        // Save file from S3 to public folder
        Storage::disk('public')->putFile("temp/resources", $file);

        // Get file name/path
        $filename = $file->hashName();
        $file_relative_path = "temp/resources/{$filename}";

        // Set Ghostscript path (on Windows machine ONLY)
        $gs_path = config('services.ghostscript.path');
        if(!is_null($gs_path)) {
            Ghostscript::setGsPath(config('services.ghostscript.path'));
        }

        // Generate PNG from PDF and save to public temp file
        $pdf = new \Spatie\PdfToImage\Pdf($file_relative_path);
        $pdf->setResolution(50);

        $preview_img_relative_path = 'temp/preview_imgs/'.$this->name.'.png';
        $preview_img_address = public_path( $preview_img_relative_path );
        $pdf->saveImage($preview_img_address);

        try {
            $file = new File($preview_img_address);
            // Save image from public folder to S3
            $preview_img = Storage::disk('s3')->put('resources/preview_imgs', $file);
        } catch (\Exception $e) {
            $preview_img = null;
        }

        // Delete image from public folder
        Storage::disk('public')->delete($preview_img_relative_path);

        $this->resourceable->update([
            'preview_img' => $preview_img
        ]);

        // Delete file from public folder once saved preview image
        Storage::disk('public')->delete($file_relative_path);
    }

    /**
     * Update preview image (first page of PDF file)
     *
     * Convert S3 file to Uploaded file instance by stremaing in locally, and constructing an UploadedFile instance
     * Using Resource@savePreviewImage(UploadedFile $file)
     *
     * ONLY PDF files supported/tested
     * **NOTE: S3 file permissions will affect the usability of this function
     * @see http://api.symfony.com/2.0/Symfony/Component/HttpFoundation/File/UploadedFile.html
     */
    public function updatePreviewImage()
    {
        // Stream file from S3
        $s3_file = Storage::disk('s3')->get($this->url_relative_path);

        $path = "temp/resources/{$this->name}.pdf";

        // Store file locally
        Storage::disk('public')->put($path, $s3_file);

        // Create UploadedFile instance
        $file = new UploadedFile(
            public_path( $path ),
            $this->name,
            'mime/pdf'
        );

        // Save preview image
        $this->savePreviewImage($file);

        // Delete file locally
        Storage::disk('public')->delete($path);
    }

    /**
     * Returns adjusted coin price
     * @return int
     */
    public function getAdjustedCoinPrice(User $buyer)
    {
        return $this->resourceable->getAdjustedCoinPrice($buyer);
    }

    /**
     * Returns coin discount
     * @return int
     */
    public function getCoinDiscount(User $buyer)
    {
        return $this->resourceable->getCoinDiscount($buyer);
    }

    /**
     * Unlock resource for user
     */
    public function unlockFor(User $buyer)
    {
        $purchase_price = $this->getAdjustedCoinPrice($buyer);
        $coin_discount = $this->getCoinDiscount($buyer);

        if($buyer->coin_balance < $purchase_price) {
            return new \Exception('Insufficient coin balance!');
        }

        // Deduct coins from buyer's balance
        $buyer->deductCoins($purchase_price);
        /**
         * Add 2 coin actions:
         * 1. One charging original price
         * 2. One adding back to user's coin balance
         */
        $buyer->coin_actions()->create([
            'name' => "{$this->type}#{$this->resourceable_id} ({$this->name}) Unlock",
            'coin_actionable_type' => $this->resourceable_type,
            'coin_actionable_id' => $this->resourceable_id,
            'amount' => -($this->coin_price),
        ]);
        if($coin_discount > 0) {
            $buyer->coin_actions()->create([
                'name' => "{$this->type}#{$this->resourceable_id} ({$this->name}) Coin Discount",
                'coin_actionable_type' => $this->resourceable_type,
                'coin_actionable_id' => $this->resourceable_id,
                'amount' => $coin_discount,
            ]);
        }

        $seller = $this->owner();
        // Add to seller's balance
        $seller->addCoins($this->coin_price);
        $seller->coin_actions()->create([
            'name' => "{$this->type}#{$this->resourceable_id} ({$this->name}) Sale",
            'coin_actionable_type' => $this->resourceable_type,
            'coin_actionable_id' => $this->resourceable_id,
            'amount' => $this->coin_price,
        ]);

        // Unlock resource for buyer
        $buyer->unlockResource($this);

        // Log event on Amplitude
        Amplitude::setUserId($buyer->id);
        Amplitude::queueEvent('unlock_resource', ['type' => $this->type, 'resourceable_id' => $this->resourceable_id, 'base_coin_amount' => $this->coin_price, 'coin_discount' => $coin_discount]);
    }
}
