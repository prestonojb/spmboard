@extends('voyager::master')

@section('page_title', 'Generate Exam Builder Item')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_header')
<div class="container-fluid">
    <h1 class="page-title">
        [Exam Builder] List Items
    </h1>
</div>
@stop

@section('content')
    <div class="page-content container-fluid">
        <div class="panel panel-bordered">
            <a href="{{ route('voyager.exam_builder_items.get_generate_view') }}" class="btn btn-warning">Regenerate List</a>
            <div class="panel-body">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Label</th>
                            <th>Question</th>
                            <th>Answer</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($items as $item)
                            <tr class="@if(!$item->is_complete) bg-warning @endif">
                                <td scope="row">{{ $item->name }}</td>
                                <td>
                                    @if(count($item->question_img_urls ?? []))
                                        @foreach($item->question_img_urls as $url)
                                            <img src="{{ Storage::cloud()->url($url) }}" width="600" alt="" class="mb-2">
                                        @endforeach
                                    @else
                                        No Question
                                    @endif
                                </td>
                                <td>
                                    @if(count($item->answer_img_urls ?? []))
                                        @foreach($item->answer_img_urls as $url)
                                            <img src="{{ Storage::cloud()->url($url) }}" width="600" alt="" class="mb-2">
                                        @endforeach
                                    @else
                                        No Answer
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@stop

@section('javascript')
<script src="{{ mix('js/forum.js') }}"></script>
@stop
