<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Answer;
use App\AnswerComment;
use App\Notifications\AnswerCommented;
use Amplitude;

class AnswerCommentController extends Controller
{
    public function store(Answer $answer)
    {
        // Validation
        request()->validate([
            'answer_comment' => 'required|max:191'
        ]);

        $user = auth()->user();

        // Create comment
        $answer_comment = $answer->comments()->create([
            'user_id' => $user->id,
            'comment' => request('answer_comment'),
        ]);

        if($user->id != $answer->user->id) {
            $answer->user->notify(new AnswerCommented($answer_comment));
        }

        // Log event on Amplitude
        Amplitude::setUserId($user->id);
        Amplitude::queueEvent('add_answer_comment', ['question_id' => $answer->question->id, 'answer_id' => $answer->id, 'answer_comment_id' => $answer_comment->id]);

        return back()->withSuccess('Comment added successfully!');
    }

    public function destroy($id)
    {
        $answer_comment = AnswerComment::find($id);
        $this->authorize('delete', $answer_comment);

        $answer_comment->delete();
        return back()->withError('Comment deleted successfully!');
    }
}
