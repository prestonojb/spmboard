<div class="nav nav-pills resource-nav-pills" role="tablist" aria-orientation="vertical">
    @foreach($subject->chapters as $chapter)
        <a class="nav-link @if($loop->first) active @endif" id="{{ str_slug($chapter->name) }}_nav_link" data-toggle="pill" href="#{{ str_slug($chapter->name) }}_chapter" role="tab">
            {{ $chapter->name }}
            <span class="subtitle">({{ count($chapter->topical_past_papers ?? []) }})</span>
        </a>
    @endforeach
</div>
