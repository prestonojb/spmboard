<?php

namespace App\Http\Controllers\Voyager;

use App\Imports\NoteImport;
use Illuminate\Http\Request;
use Excel;
use App\Notes;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Artisan;

class VoyagerNotesController extends \TCG\Voyager\Http\Controllers\VoyagerBaseController
{
    public function create(Request $request)
    {
        return view('vendor.voyager.notes.create');
    }

    public function store(Request $request)
    {
        // Validation
        request()->validate([
            'file' => 'required|mimes:csv,xlsx,xls'
        ]);
        // Import rows from excel sheet to DB
        Excel::import(new NoteImport, request()->file('file'));

        // Sync Resources
        Artisan::call('sync:resources');

        return redirect()->back()->with(['alert-type' => 'success', 'message' => 'Updated Database successfully! Remember to upload your files on S3 too.']);
    }

    public function destroy(Request $request, $id)
    {
        $notes = Notes::find($id);
        $notes->delete();

        return redirect('admin/notes')->with('error', 'Notes deleted successfully!');
    }
}


