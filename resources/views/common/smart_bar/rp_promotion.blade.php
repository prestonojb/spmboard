@if( setting('marketing.rp_promotion_slots_available') > 0 && setting('marketing.rp_promotion_amount') > 0 )
    <div class="d-none smart-bar-container rpPromotion">
        <div class="container">
            <div class="smart-bar">
                <div class="d-none d-lg-block"></div>
                <div class="text">
                    Receive {{ setting('marketing.rp_promotion_amount') }} Reward Points when you sign up now! <b>{{ setting('marketing.rp_promotion_slots_available') }} slots left only!
                </div>
                <button class="dismiss-button dismissButton rpPromotion"><span class="iconify" data-icon="emojione-monotone:cross-mark" data-inline="false"></span></button>
            </div>
        </div>
    </div>
@endif
