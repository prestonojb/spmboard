<div class="panel p-0 mb-2 itemBlock" data-item-id="{{ $item->id }}" data-item-name="{{ $item->name }}">
    <div class="panel-header">
        <button class="float-right red pt-0 font-size4 removeItemButton"><i class="fas fa-times"></i></button>
        <h4 class="mb-1">{{ $item->name }}</h4>
        <p class="mb-0 gray2 font-size2 font-weight400">
            {{ $item->chapter->name }}
        </p>
    </div>
    <div class="panel-body">
        {{-- Question --}}
        <div class="item__question">
            <p class="font-size3"><b>Question</b></p>
            @foreach($item->question_img_urls as $url)
                <img class="d-block w-100" src="{{ Storage::cloud()->url($url) }}" alt="">
            @endforeach
        </div>

        {{-- Answer --}}
        {{-- <button class="button-link blue4 underline-on-hover mb-1 toggleAnswerButton">Show Answer</button>
        <div class="d-none item__answer">
            <hr>
            <p class="font-size3"><b>Answer</b></p>
            @foreach($item->answer_img_urls as $url)
                <img class="d-block w-100" src="{{ Storage::cloud()->url($url) }}" alt="">
            @endforeach
        </div> --}}
    </div>

    <div class="panel-footer border-top">
        <div class="d-flex justify-content-end align-items-center">
            @php
                $tag = 'resource';
                $description = "[Exam Builder Item ID: {$item->id}] {$item->name} is not working as expected.";
                $email = auth()->check() ? auth()->user()->email : null;
            @endphp
            <div class="mr-2">
                {{-- Report Button --}}
                <button class="button-link blue4 underline-on-hover" data-toggle="modal" data-target="#bugReportModal">Report</button>
                @component('bug_reports.common.store_modal', ['tag' => $tag, 'description' => $description, 'email' => $email])
                @endcomponent
            </div>
            <button class="button--success regenerateItemButton" style="width: 112px;">Regenerate</button>
        </div>
    </div>
</div>
