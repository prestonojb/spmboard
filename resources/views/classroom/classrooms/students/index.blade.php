@extends('classroom.layouts.app')

@section('meta')
    <title>Students | {{ config('app.name') }} Classroom</title>
@endsection

@section('styles')
@endsection

@section('content')

@component('classroom.common.breadcrumbs')
    <a href="{{ route('classroom.classrooms.show', $classroom->id) }}">Dashboard</a> /
    Students
@endcomponent

@component('classroom.common.page_heading', ['title' => 'Students', 'button' => 'Add Student', 'button_url' => route('classroom.students.get_add_view', $classroom->id)])
@endcomponent

<div class="maxw-740px mx-auto my-5">
    <h3 class="">{{ count($classroom->students ?? []) }} @if(count($classroom->students ?? []) > 1) students @else student @endif</h3>
    @if( count($classroom->students ?? []) )
        @foreach($classroom->students as $student)
            <div class="panel p-0 mb-2">
                <div class="d-flex justify-content-between align-items-center p-2 @if(!$loop->last) border-bottom @endif">
                    <div class="d-flex justify-content-start align-items-center">
                        <a target="_blank" href="{{ route('classroom.students.show', $student->user_id).'?class_selected='.$classroom->id }}">
                            <img class="avatar mr-2" src="{{ $student->avatar }}">
                        </a>
                        <div class="text">
                            <a target="_blank" class="inactive" href="{{ route('classroom.students.show', $student->user_id).'?class_selected='.$classroom->id }}">{{ $student->username }}</a>
                        </div>
                    </div>
                    <div class="d-flex justify-content-start align-items-center">
                        <a target="_blank" href="{{ route('classroom.students.show', $student->user_id).'?class_selected='.$classroom->id }}" class="button--primary mr-1">View</a>
                        <form action="{{ route('classroom.students.kick', $classroom->id) }}" class="kickStudentButton" method="POST">
                            @csrf
                            <input type="hidden" name="student_user_id" value="{{ $student->user_id }}">
                            <button type="submit" class="button--danger kickStudentButton">Kick</button>
                        </form>
                    </div>
                </div>
            </div>
        @endforeach
    @else
        <div class="panel">
            <div class="text-center my-4">
                <img src="{{ asset('img/essential/empty.svg') }}" alt="No students yet" width="220" height="220" class="mb-2">
                <h2 class="h3">No students yet</h2>
                <a href="{{ route('classroom.students.get_add_view', $classroom->id) }}" class="button--primary"><i class="fas fa-plus"></i> Add Student</a>
            </div>
        </div>
    @endif
</div>

@endsection

@section('scripts')

@endsection
