<?php

namespace Tests\Feature\Classroom;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Carbon\Carbon;
use App\Classroom;
use App\ClassroomPost;
use App\Lesson;
use App\Subject;
use App\User;
use Illuminate\Support\Facades\Notification;
use App\Notifications\Classroom\NewClassroomPostAdded;

class ClassroomPostTest extends TestCase
{
    use WithFaker;

    private $teacher;
    private $student;
    private $parent;
    private $classroom;
    private $subject;

    public function setUp() : void
    {
        parent::setUp();

        // Subscribed Teacher
        $user = factory(User::class)->states('subscribed_teacher')->create();
        $this->teacher = $user->teacher;

        // Student with Board Pass
        $user = factory(User::class)->states('student_with_board_pass')->create();
        $this->student= $user->student;

        // Parent
        $user = factory(User::class)->states('parent')->create();
        $this->parent = $user->parent;


        // Subject and classroom
        $this->subject = Subject::first();
        $this->classroom = factory(Classroom::class)->create([
                            'teacher_id' => $this->teacher->user_id,
                            'subject_id' => $this->subject->id,
                        ]);
        // Attach parent to child
        $this->student->updateParent( $this->parent );
        // Add student to classroom
        $this->classroom->addStudent($this->student);
    }

    /**
     * @group classroom_post
     * Test Create Classroom Post
     */
    public function testCreate()
    {
        // GET request to create lesson
        $response = $this->actingAs($this->teacher->user)->get( route('classroom.posts.create', $this->classroom->id) );

        $response->assertSuccessful();
    }

    /**
     * @group classroom_post
     * Test Store Classroom Post
     */
    public function testStore()
    {
        Notification::fake();

        $init_count = ClassroomPost::count();

        // POST request to store lesson
        $response = $this->actingAs($this->teacher->user)->post( route('classroom.posts.store', $this->classroom->id), $this->getStoreData());
        $response->assertRedirect();

        // Null title
        $response = $this->actingAs($this->teacher->user)->post( route('classroom.posts.store', $this->classroom->id), $this->getNullTitleStoreData());
        $response->assertSessionHasErrors('title', "Null 'title' field should be validated!");

        // Null description
        $response = $this->actingAs($this->teacher->user)->post( route('classroom.posts.store', $this->classroom->id), $this->getNullDescriptionStoreData());
        $response->assertSessionHasErrors('description', "Null 'description' field should be validated!");

        // Null categories
        $response = $this->actingAs($this->teacher->user)->post( route('classroom.posts.store', $this->classroom->id), $this->getNullCategoriesStoreData());
        $response->assertSessionHasErrors('categories', "Null 'categories' field should be validated!");

        // Null categories
        $response = $this->actingAs($this->teacher->user)->post( route('classroom.posts.store', $this->classroom->id), $this->getInvalidCategoriesStoreData());
        $response->assertSessionHasErrors('categories', "Invalid 'categories' field should be validated!");

        // String categories
        $response = $this->actingAs($this->teacher->user)->post( route('classroom.posts.store', $this->classroom->id), $this->getStringCategoriesStoreData());
        $response->assertSessionHasErrors('categories', "String 'categories' field should be validated!");

        // Null type
        $response = $this->actingAs($this->teacher->user)->post( route('classroom.posts.store', $this->classroom->id), $this->getNullTypeStoreData());
        $response->assertSessionHasErrors('type', "Null 'type' field should be validated!");

        // Assert classroom post row added to DB
        $this->assertEquals( $init_count + 1, ClassroomPost::count() );
    }

    /**
     * @group classroom_post
     * Test that email notification is sent to students and parents
     */
    public function testNotificationStore()
    {
        Notification::fake();

        $response = $this->actingAs($this->teacher->user)->post( route('classroom.posts.store', $this->classroom->id), $this->getStoreData());
        // Student
        Notification::assertSentTo(
            [$this->student], NewClassroomPostAdded::class,
            function ($notification, $channels) {
                return $notification->via($this->student) == ['mail', 'database'];
            }
        );

        // Parent
        Notification::assertNotSentTo(
            [$this->parent], NewClassroomPostAdded::class,
            function ($notification, $channels) {
                    return $notification->via($this->parent) == ['mail', 'database'];
                }
        );
    }

    /**
     * @group classroom_post
     * Test that email notification is sent to parent when post is stored (if teacher opts in)
     */
    public function testEmailNotificationToParentStore()
    {
        Notification::fake();

        $data = $this->getStoreData();
        $data["notify_parents"] = 1;

        $response = $this->actingAs($this->teacher->user)->post( route('classroom.posts.store', $this->classroom->id), $data );

        // Student
        Notification::assertSentTo(
            [$this->student], NewClassroomPostAdded::class,
            function ($notification, $channels) {
                return $notification->via($this->student) == ['mail', 'database'];
            }
        );

        // Parent
        Notification::assertSentTo(
            [$this->parent], NewClassroomPostAdded::class,
            function ($notification, $channels) {
                    return $notification->via($this->parent) == ['mail', 'database'];
                }
        );
    }

    /**
     * @group classroom_post
     * Test classroom post edit view
     */
    public function testEdit()
    {
        $post = factory(ClassroomPost::class)->create([
            'classroom_id' => $this->classroom->id,
            'user_id' => $this->teacher->user_id,
        ]);

        $response = $this->actingAs($this->teacher->user)->get( route('classroom.posts.edit', [$this->classroom->id, $post->id]) );
        $response->assertSuccessful();
    }

    /**
     * @group classroom_post
     * Test that classroom post is updated correctly
     */
    public function testUpdate()
    {
        $post = factory(ClassroomPost::class)->create([
            'classroom_id' => $this->classroom->id,
            'user_id' => $this->teacher->user_id,
        ]);

        $post_id = $post->id;

        // PATCH request
        $response = $this->actingAs($this->teacher->user)
                         ->patch( route('classroom.posts.update', [$this->classroom->id, $post->id]), $this->getEditData() );

        $response->assertSessionHas('success');
        $response->assertRedirect();

        $post = ClassroomPost::find($post_id);


        // Assert post has updated values
        $this->assertEquals( $post->title, "edited", "Classroom Post title not updated!" );
        $this->assertEquals( $post->description, "edited", "Classroom Post description not updated!" );
        $this->assertEquals( $post->categories, ["lesson"], "Classroom Post categories not updated!" );
        $this->assertEquals( $post->type, "announcement", "Classroom Post type not updated!" );
    }

    /**
     * @group classroom_post
     * Test that classroom post is deleted
     */
    public function testDelete()
    {
        $post = factory(ClassroomPost::class)->create([
            'classroom_id' => $this->classroom->id,
            'user_id' => $this->teacher->user_id,
        ]);

        $post_id = $post->id;
        $init_count = ClassroomPost::count();
        $response = $this->actingAs($this->teacher->user)->delete( route('classroom.posts.delete', [$this->classroom->id, $post->id]) );

        // Assert classroom post deleted
        $this->assertTrue( is_null( ClassroomPost::find($post_id) ), "Classroom Post not deleted!" );
        $this->assertEquals( $init_count - 1, ClassroomPost::count() );

        $response->assertSessionHas('error');
        $response->assertRedirect();
    }

    /**
     * Get POST request Store Classroom Post data array
     * @return array
     */
    public function getStoreData()
    {
        return [
            'title' => $this->faker->sentence,
            'description' => $this->faker->text,
            'classroom_id' => $this->classroom->id,
            'user_id' => $this->teacher->user_id,
            'categories' => ["lesson", "resource", "homework"],
            'type' => 'announcement',
        ];
    }

    /**
     * Get PATCH request Edit Classroom Post data array
     * @return array
     */
    public function getEditData()
    {
        return [
            'title' => "edited",
            'description' => 'edited',
            'classroom_id' => 1,
            'user_id' => 1,
            'categories' => ["lesson"],
            'type' => 'announcement',
        ];
    }

    /**
     * Get POST request Store Classroom Post data array
     * :'title' => null:
     * @return array
     */
    public function getNullTitleStoreData()
    {
        return [
            'title' => null,
            'description' => $this->faker->text,
            'classroom_id' => $this->classroom->id,
            'user_id' => $this->teacher->user_id,
            'categories' => ["lesson", "resource", "homework"],
            'type' => 'announcement',
        ];
    }

    /**
     * Get POST request Store Classroom Post data array
     * :'description' => null:
     * @return array
     */
    public function getNullDescriptionStoreData()
    {
        return [
            'title' => $this->faker->sentence,
            'description' => null,
            'classroom_id' => $this->classroom->id,
            'user_id' => $this->teacher->user_id,
            'categories' => ["lesson", "resource", "homework"],
            'type' => 'announcement',
        ];
    }

    /**
     * Get POST request Store Classroom Post data array
     * :'categories' => null:
     * @return array
     */
    public function getNullCategoriesStoreData()
    {
        return [
            'title' => $this->faker->sentence,
            'description' => $this->faker->text,
            'classroom_id' => $this->classroom->id,
            'user_id' => $this->teacher->user_id,
            'categories' => null,
            'type' => 'announcement',
        ];
    }

    /**
     * Get POST request Store Classroom Post data array
     * :'categories' => "lesson":
     * @return array
     */
    public function getStringCategoriesStoreData()
    {
        return [
            'title' => $this->faker->sentence,
            'description' => $this->faker->text,
            'classroom_id' => $this->classroom->id,
            'user_id' => $this->teacher->user_id,
            'categories' => "lesson",
            'type' => 'announcement',
        ];
    }

    /**
     * Get POST request Store Classroom Post data array
     * :'categories' => "asd":
     * @return array
     */
    public function getInvalidCategoriesStoreData()
    {
        return [
            'title' => $this->faker->sentence,
            'description' => $this->faker->text,
            'classroom_id' => $this->classroom->id,
            'user_id' => $this->teacher->user_id,
            'categories' => ["asd"],
            'type' => 'announcement',
        ];
    }

    /**
     * Get POST request Store Classroom Post data array
     * :'type' => null:
     * @return array
     */
    public function getNullTypeStoreData()
    {
        return [
            'title' => $this->faker->sentence,
            'description' => $this->faker->text,
            'classroom_id' => $this->classroom->id,
            'user_id' => $this->teacher->user_id,
            'categories' => ["lesson", "resource", "homework"],
            'type' => null,
        ];
    }
}
