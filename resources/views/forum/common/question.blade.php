<div class="panel mb-3 @if($question->is_question_of_the_day) border-gold border-width3px @endif">
    <div class="question-block__header">
        @component('questions.common.tags', ['question' => $question])
        @endcomponent

        @component('questions.common.icon_group', ['question' => $question])
        @endcomponent
    </div>

    <div class="question__user-header">
        <div class="question-block__user-info">
            @if( !is_null($question->user) && !$question->anonymous )
                <a href="{{ route('users.show', $question->user->username) }}">
                    <img class="avatar" src="{{ $question->user->avatar ?? asset('img/essential/no-profile-picture.jpg') }}">
                </a>
            @else
                <a href="javascript:void(0)">
                    <img class="avatar" src="{{ asset('img/essential/no-profile-picture.jpg') }}">
                </a>
            @endif
            <div class="mt-1 text">
                <div class="user-info">
                    @if( !is_null($question->user) && !$question->anonymous )
                        <a class="username" href="{{ route('users.show', $question->user->username) }}">{!! $question->user->username_with_badge !!}</a>
                    @else
                        <a class="username" href="javascript:void(0)"><i>Unknown User</i></a>
                    @endif
                    {!! $question->user->user_role_icon_popover_html !!}
                </div>
                <span class="time">Asked {{ $question->created_at->longRelativeDiffForHumans() }}</span>
            </div>
        </div>

        <div class="btn-group" role="group">
            <button type="button" class="more-button" data-toggle="dropdown">
                <i class="fa fa-ellipsis-h" aria-hidden="true"></i>
            </button>

            <div class="dropdown-menu dropdown-menu-right">
                <button type="button" class="dropdown-item" data-toggle="modal" data-target="#editHistoryModal">
                    Edit history
                </button>
                @can('recommend', $question)
                    <button type="button" class="dropdown-item question-recommend-button" data-url="{{ route('questions.recommend', $question->id) }}">@if(!$question->recommended) Recommend @else Unrecommend @endif</button>
                @endcan
                @can('edit', $question)
                    <a class="dropdown-item" href="{{ route('questions.edit', [$exam_board->slug, $question->id]) }}">Edit question</a>
                @endcan
                @can('delete', $question)
                    <form method="POST" action="{{ route('questions.destroy', [$exam_board->slug, $question->id]) }}">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="dropdown-item text-danger delete-button">Delete question</button>
                    </form>
                @endcan
            </div>
        </div>
    </div>

    <h1 class="question__title">{{ $question->question }}</h1>
    <div class="tinymce-editor-contents">
        <div class="question__description">{!! $question->description !!}</div>
    </div>

    <div class="py-1"></div>

    @if($question->img)
    <div class="question__image-container">
        <img src="{{ $question->img }}" class="question-img">
    </div>
    @endif

    <div class="position-relative mb-1">
        <span><b>{{ views($question)->count() }} view(s)</b></span>
        @if( !empty($question->flagged_as) )
            <div class="question-block__flag-left {{ str_slug($question->flagged_as) }}">
                {{ $question->flagged_as }}
                <div class="triangle-left"></div>
            </div>
        @endif
    </div>

    <div class="question-block__button-group">
        <button type="button" class="question-upvote-button @if($question->upvoted) upvoted @endif" data-questionid="{{ $question->id }}"><span class="icon"></span> <span class="text">Upvote</span> · <span class="upvote-count" data-questionid="{{ $question->id }}">{{ readableNumber($question->upvote_count) }}</span></button>
        @can('create', \App\Answer::class)
            <button type="button" class="answer-button"><span class="icon"></span> <span class="text">Answer</span> · <span class="answer-count">{{ count($question->answers) }}</span></button>
        @endcan
    </div>

    <!-- Modal -->
    <div class="modal fade" id="editHistoryModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                <h5 class="modal-title">Edit history</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                </div>
                <div class="modal-body">
                    @foreach($question->revisionHistory as $history)
                        <div class="edit-history-item py-1">
                            <a href="{{ route('users.show', $history->userResponsible()->username) }}">
                                {{ $history->userResponsible()->username }} </a>
                                changed {{ $history->fieldName() }} from <span class="text-white bg-danger">{{ $history->oldValue() }}</span>
                                to <span class="text-white bg-success">{{ $history->newValue() }}</span>
                            <span class="float-right">
                                <small>
                                    {{ $history->created_at->diffForHumans() }}
                                </small>
                            </span>
                        </div>
                    @endforeach
                </div>
                <div class="modal-footer">
                    <button type="button" class="button--primary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="question__comments">
        <button class="question__comment-button">Comments({{ $question->comments->count() }})</button>
        <ul class="list-group question__comment-list">
            @foreach($question->comments as $comment)
                <li class="list-group-item question__comment-item">
                    <span class="comment">{{ $comment->comment }} - <a href="{{ route('users.show', $comment->user->username) }}" style="font-size: inherit;">{{ $comment->user->username }}</a></span> <span class="time">{{ $comment->created_at->diffForHumans() }}</span>
                    @can('delete', $comment)
                        <form method="POST" action="{{ route('question_comments.delete', $comment->id) }}" class="d-inline">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="delete-button"><i class="fa fa-trash" aria-hidden="true"></i></button>
                        </form>
                    @endcan
                </li>
            @endforeach
            @auth
                @can('store', App\QuestionComment::class)
                    <li class="list-group-item question__comment-item">
                        <form action="{{ route('question_comments.store', $question->id) }}" method="POST" class="question-comment-form">
                            @csrf
                            <input type="text" class="d-inline" name="question_comment" required>
                            <button type="submit" class="button--primary">Add Comment</button>
                        </form>
                        @error('question_comment')
                            <span class="error-message">{{ $message }}</span>
                        @enderror
                    </li>
                @endcan
            @else
                <li class="list-group-item question__comment-item"><a href="{{ route('login') }}" style="font-size: inherit;">Log in</a> to comment.</li>
            @endauth
        </ul>
    </div>
</div>
