@extends('main.layouts.app')

@section('meta')
    <title>Register | {{ config('app.name') }}</title>
    <meta name="description" content="Register now to gain access to various features!">
@endsection

@section('content')

<div class="py-4"></div>

<div class="wrapper">

    <form method="POST" action="{{ route('register') }}">
        @csrf

        <div class="text-center">
            @if(session('status'))
                <div class="alert alert-danger" data-dismiss="alert" role="alert">
                    {{ session('status') }}
                </div>
            @endif
            {{-- RP Promotion Alert --}}
            @if(setting('marketing.rp_promotion_slots_available') > 0 && setting('marketing.rp_promotion_amount') > 0)
                <div class="alert alert-primary text-center" role="alert">
                    Receive {{ setting('marketing.rp_promotion_amount') }} Reward Points when you<br> sign up now!<br>
                    <b>{{ setting('marketing.rp_promotion_slots_available') }} slots left!</b>
                </div>
            @endif

            {{-- Ebooks Promotion Alert --}}
            @if( setting('marketing.ebooks_promotion') )
                <div class="alert alert-primary text-center" role="alert">
                    Get up to <b>3 FREE E-books</b> of your preferred subjects when you sign up now!<br>
                    <b>Limited time only!</b>
                </div>
            @endif

            <h1 class="heading">Register</h1>
            <h2 class="sub-heading">Register now and start sharing knowledge to the world!</h2>
        </div>

        {{-- Referral Code --}}
        @if(request()->has('referral_code') && !is_null(request('referral_code')))
            <div class="form-field">
                <label for="">Referral Code
                    <button class="transparent p-0" type="button" data-container="body" data-toggle="popover" data-placement="right" data-content="This code is generated automatically based on the referral link shared to you.">
                        <i class="fa fa-info-circle" aria-hidden="true"></i>
                    </button>
                </label>
                <input type="text" name="referral_code" value="{{ request('referral_code') }}" readonly>
            </div>
            @error('referral_code')
                <span class="error-message">{{ $message }}</span>
            @enderror
        @endif

        <div class="pt-3">
            <label for="">Join Board as a</label>
            <div class="form-row">
                @foreach(['teacher', 'student', 'parent'] as $account_type)
                    <div class="col-4">
                        <div class="styled-radio-input">
                            <input type="radio" name="account_type" value="{{ $account_type }}" id="account_type_{{ $account_type }}" @if(old('account_type') == $account_type) checked @endif>
                            <label for="account_type_{{ $account_type }}">{{ ucfirst($account_type) }}</label>
                        </div>
                    </div>
                @endforeach
                @error('account_type')
                    <div class="error-message">{{ $message }}</div>
                @enderror
            </div>
        </div>

        <div class="@if(!$errors->any()) hiddenFormSection @endif">

            <hr>

            <a class="btn d-flex justify-content-center align-items-center google-btn mt-2 loginWithGoogleButton" href="{{ route('google.redirect') }}">
                <img class="google-icon" src="{{ asset('img/essential/google.png') }}" alt="google-icon">
                SIGN UP WITH GOOGLE
            </a><br>

            <div class="text-center">
                <span class="or-text font-weight700">OR</span>
            </div>

            <div class="form-field">
                <label>{{ __('Username') }}</label>
                <input type="text" name="username" value="{{ old('username') }}" required autofocus autocomplete="username" placeholder="Username">
                @error('username')
                    <span class="error-message">{{ $message }}</span>
                @enderror
            </div>

            <div class="form-field">
                <label>{{ __('Email Address') }}</label>
                <input type="email" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="Email">
                @error('email')
                    <span class="error-message">{{ $message }}</span>
                @enderror
            </div>

            <div class="form-field">
                <label>{{ __('Password') }}</label>
                <input type="password" name="password" required placeholder="Password">
                @error('password')
                    <span class="error-message">{{ $message }}</span>
                @enderror
            </div>

            <div class="form-field">
                <label>{{ __('Confirm Password') }}</label>
                <input type="password" name="password_confirmation" required placeholder="Confirm Password">
                @error('password_confirmation')
                    <span class="error-message">{{ $message }}</span>
                @enderror
            </div>

            <div class="text-center">
                <button type="submit" class="button--primary mb-0" data-disable-on-submit="true">Sign up</button>
            </div>

        </div>

        <div class="text-center mt-2">
            <p class="mb-0">Already a part of us? <a href="{{ route('login') }}">Log in</a></p>
        </div>

    </form>
</div>

<div class="py-4"></div>

@endsection

@section('scripts')
<script>
$(function(){
    var accountType = window.getParameterByName("account_type");
    var $accountTypeRadioInput = $('input[name=account_type]');
    $accountTypeRadioInput.change(function(e){
        $('.hiddenFormSection').removeClass('hiddenFormSection');
    });

    // Pre-select account type
    var $targetRadioInput = $accountTypeRadioInput.filter("[value=" + accountType + "]");
    if( $targetRadioInput.length > 0 ) {
        $targetRadioInput.prop('checked', true);
        $accountTypeRadioInput.change();
    }

    $('.loginWithGoogleButton').click(function(e){
        e.preventDefault();
        var url = $(this).attr('href');
        var accountTypes = ['teacher', 'student', 'parent'];
        var account_type = $('[name=account_type]:checked').val();
        // Account type front-end validation
        if( accountTypes.includes(account_type) ) {
            window.location.href = url + '?account_type=' + account_type;
        } else {
            toastr.error("Please select the account type you want to create before proceeding!");
        }
    });
});
</script>
@endsection
