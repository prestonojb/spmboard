<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class HomeworkReminder extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($teacher, $student, $classroom, $homework)
    {
        $this->teacher = $teacher;
        $this->student = $student;
        $this->classroom = $classroom;
        $this->homework = $homework;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('teacher.homeworks.email.reminder')
                     ->with([
                         'homework_url' => route('classroom.lessons.homeworks.show', [$this->classroom->id, $this->homework->id]),
                         'teacher' => $this->teacher,
                         'student' => $this->student,
                         'classroom' => $this->classroom,
                         'homework' => $this->homework,
                     ]);
    }
}
