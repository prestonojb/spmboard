<section class="about__main" @if(isset($bg_img) && !is_null($bg_img)) style="background-image: url('{{ $bg_img }}');" @endif>
    <div class="container text-center">
        <div class="mb-4">
            <h1 class="font-weight-bold font-size9 @if(isset($light_title) && $light_title) text-white @endif">{{ $title }}</h1>
            @if(isset($description) && !is_null($description))
                <h2 class="sub-heading">{{ $description }}</h2>
            @endif
            @if(isset($body) && !is_null($body))
                <h3 class="gray2 font-size4 mb-4">
                    {!! $body !!}
                </h3>
            @endif
        </div>
        {{ $slot }}
    </div>
</section>
