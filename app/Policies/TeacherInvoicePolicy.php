<?php

namespace App\Policies;

use App\TeacherInvoice;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class TeacherInvoicePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index(User $user)
    {
        return $user->teacher || $user->parent;
    }

    public function store(User $user)
    {
        return $user->teacher;
    }

    public function show(User $user, TeacherInvoice $teacher_invoice)
    {
        if( $user->teacher ) {
            return $teacher_invoice->teacher_id == $user->id;
        }
        if( $parent = $user->parent ) {
            return in_array($teacher_invoice->student_id, $parent->children_ids);
        }
        return false;
    }

    public function delete(User $user, TeacherInvoice $teacher_invoice)
    {
        return $user->teacher && $teacher_invoice->teacher_id == $user->id;
    }

    public function mark_as_paid(User $user, TeacherInvoice $teacher_invoice)
    {
        return $user->teacher && $teacher_invoice->teacher_id == $user->id;
    }

    public function download_pdf(User $user, TeacherInvoice $teacher_invoice)
    {
        if( $user->teacher ) {
            return $teacher_invoice->teacher_id == $user->id;
        }
        if( $parent = $user->parent ) {
            return in_array($teacher_invoice->student_id, $parent->children_ids);
        }
        return false;
    }
}
