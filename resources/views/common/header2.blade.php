<nav class="navbar header2">
    <!-- Hamburger-icon -->
    <div class="header2__left">
        <button class="header2__hamburger-button" type="button" aria-expanded="false" aria-label="Toggle navigation">
            <i class="fa fa-bars" aria-hidden="true"></i>
        </button>
        <a class="d-none d-sm-block header2__logo" href="{{ route('questions.index', $exam_board->slug) }}">
            <img class="d-none d-md-inline" src="{{ asset('img/logo/logo_light_'.$exam_board->slug.'.png') }}">
        </a>
    </div>

    <ul class="header2__right">
        <li class="d-sm-none header2__search">
            <button type="button" class="header-icon header2__mobile-search-icon mr-1 mr-md-2"><span class="iconify" data-icon="bx:bx-search" data-inline="false"></span></a>
        </li>

        <form class="d-sm-none header2__mobile-search-form" method="GET" action="{{ route('search.index', $exam_board->slug) }}">
            <input class="mobile-search-input-header" type="text" name="q" placeholder="Search" autocomplete="off" required>
            <button class="d-none" type="submit"></button>
        </form>

        <form class="d-none d-sm-inline-block header2__search-form" method="GET" action="{{ route('search.index', $exam_board->slug) }}">
            <span class="iconify" data-icon="bx:bx-search" data-inline="false"></span>
            <input type="text" name="q" placeholder="Search" autocomplete="off" required>
            <button class="d-none" type="submit"></button>
        </form>

        @auth
            @component('common.header.notification_dropdown', ['user' => auth()->user()])
            @endcomponent
        @endauth

        @component('common.header.profile_dropdown', ['hide_rp_coins' => false])
        @endcomponent
    </ul>
</nav>

{{-- Overlay --}}
<div class="overlay">
    <nav class="animated slideInLeft faster header2__sidenav-container">
        <a class="header2__sidenav-logo mobile-nav-brand" href="{{ route('questions.index', $exam_board->slug) }}">
            <img class="" src="{{ asset('img/logo/logo_light_'.$exam_board->slug.'.png') }}">
        </a>
        <section class="header2__sidenav-section">
            <h3 class="heading">Discover</h3>
            <ul class="header2__sidenav-menu">
                <li class="header2__sidenav-item mobile-nav-item {{ request()->is('questions')? 'active' : '' }}">
                    <span class="header2__sidenav-icon mobile-nav-icon"><span class="iconify" data-icon="cil:newspaper" data-inline="false"></span></span>
                    <a class="stretched-link header2__sidenav-link mobile-nav-link mobile-nav-link" href="{{ route('questions.index', $exam_board->slug) }}">News Feed</a>
                </li>
                @if(Gate::allows('access-resource'))
                    <li class="header2__sidenav-item mobile-nav-item {{ request()->is('questions')? 'active' : '' }}">
                        <span class="header2__sidenav-icon mobile-nav-icon"><span class="iconify" data-icon="codicon:library" data-inline="false"></span></span>
                        <a class="stretched-link header2__sidenav-link mobile-nav-link mobile-nav-link" href="{{ route('libraries.index') }}">My Library</a>
                    </li>
                @endif
            </ul>
        </section>

        @if(Gate::allows('access-resource'))
            <section class="header2__sidenav-section">
                <h3 class="heading">Resources</h3>
                <ul class="header2__sidenav-menu">
                    <li class="header2__sidenav-item mobile-nav-item {{ request()->is('*/past-papers') || request()->is('*/past-papers/*') ? 'active' : '' }}">
                        <span class="header2__sidenav-icon mobile-nav-icon"><span class="iconify" data-icon="carbon:stacked-scrolling-1" data-inline="false"></span></span>
                        <a class="stretched-link header2__sidenav-link mobile-nav-link mobile-nav-link" href="{{ route('past_papers.home', $exam_board->slug) }}">Past Papers</a>
                    </li>
                    @if(Gate::allows('access-marketplace'))
                        <li class="header2__sidenav-item mobile-nav-item {{ request()->is('*/topical-past-papers') || request()->is('*/topical-past-papers/*') ? 'active' : '' }}">
                            <span class="header2__sidenav-icon mobile-nav-icon"><span class="iconify" data-icon="carbon:category-new-each" data-inline="false"></span></span>
                            <a class="stretched-link header2__sidenav-link mobile-nav-link mobile-nav-link" href="{{ route('topical_past_papers.home', $exam_board->slug) }}">
                                Topical Worksheets
                                @if(setting('beta.marketplace')) <span class="badge badge-primary">BETA</span> @endif
                            </a>
                        </li>
                        <li class="header2__sidenav-item mobile-nav-item {{ request()->is('*/notes') || request()->is('*/notes/*') ? 'active' : '' }}">
                            <span class="header2__sidenav-icon mobile-nav-icon"><span class="iconify" data-icon="mdi-light:note-text" data-inline="false"></span></span>
                            <a class="stretched-link header2__sidenav-link mobile-nav-link mobile-nav-link" href="{{ route('notes.home', $exam_board->slug) }}">
                                Notes
                                @if(setting('beta.marketplace')) <span class="badge badge-primary">BETA</span> @endif
                            </a>
                        </li>
                    @endif
                    <li class="header2__sidenav-item mobile-nav-item {{ request()->is('*/guides') || request()->is('*/guides/*') ? 'active' : '' }}">
                        <span class="header2__sidenav-icon mobile-nav-icon"><span class="iconify" data-icon="carbon:notebook-reference" data-inline="false"></span></span>
                        <a class="stretched-link header2__sidenav-link mobile-nav-link mobile-nav-link" href="{{ route('guides.home', $exam_board->slug) }}">Guides</a>
                    </li>
                    @if(Gate::allows('access-marketplace'))
                        @if(Gate::allows('upload-resource'))
                            <li class="header2__sidenav-item mobile-nav-item red {{ request()->is('*/resources') || request()->is('*/resources/*') ? 'active' : '' }}">
                                <span class="header2__sidenav-icon mobile-nav-icon"><span class="iconify" data-icon="carbon:add-alt" data-inline="false"></span></span>
                                <a class="stretched-link header2__sidenav-link mobile-nav-link mobile-nav-link" href="{{ route('notes.create', $exam_board->slug) }}">
                                    Add Resource
                                    @if(setting('beta.marketplace')) <span class="badge badge-primary">BETA</span> @endif
                                </a>
                            </li>
                        @endif
                    @endif
                    @if(Gate::allows('access-exam-builder'))
                        <li class="header2__sidenav-item mobile-nav-item">
                            <span class="header2__sidenav-icon mobile-nav-icon"><span class="iconify" data-icon="carbon:model-builder" data-inline="false"></span></span>
                            <a class="stretched-link header2__sidenav-link mobile-nav-link mobile-nav-link" href="{{ route('exam_builder.landing').'?exam_board='.$exam_board->slug }}">
                                Exam Builder
                                <span class="badge badge-danger">NEW</span>
                            </a>
                        </li>
                    @endif
                </ul>
            </section>
        @endif
        <section class="header2__sidenav-section">
            <h3 class="heading">More</h3>
            <ul class="mobile-nav-menu">
                {{-- Top Up Coins --}}
                @auth
                    @if( auth()->user()->hasCoins() )
                        <li class="header2__sidenav-item mobile-nav-item {{ request()->is('*/store') || request()->is('*/store/*') ? 'active' : '' }}">
                            <span class="header2__sidenav-icon mobile-nav-icon"><span class="iconify" data-icon="la:coins-solid" data-inline="false"></span></span>
                            <a class="stretched-link header2__sidenav-link mobile-nav-link mobile-nav-link" href="{{ route('products.coins') }}">Top Up Coins</a>
                        </li>
                    @endif
                @else
                    <li class="header2__sidenav-item mobile-nav-item {{ request()->is('*/store') || request()->is('*/store/*') ? 'active' : '' }}">
                        <span class="header2__sidenav-icon mobile-nav-icon"><span class="iconify" data-icon="la:coins-solid" data-inline="false"></span></span>
                        <a class="stretched-link header2__sidenav-link mobile-nav-link mobile-nav-link" href="{{ route('products.coins') }}">Top Up Coins</a>
                    </li>
                @endif
                {{-- Classroom --}}
                @if(Gate::allows('access-classroom'))
                    <li class="header2__sidenav-item mobile-nav-item">
                        <span class="header2__sidenav-icon mobile-nav-icon"><span class="iconify" data-icon="la:chalkboard-teacher-solid" data-inline="false"></span></span>
                        <a class="stretched-link header2__sidenav-link mobile-nav-link mobile-nav-link" href="{{ route('classroom.landing') }}">
                            Classroom
                            @if( setting('beta.classroom') ) <span class="badge badge-primary">BETA</span> @endif
                        </a>
                    </li>
                @endif
                <li class="header2__sidenav-item mobile-nav-item">
                    <span class="header2__sidenav-icon mobile-nav-icon"><span class="iconify" data-icon="subway:world" data-inline="false"></span></span>
                    <a class="stretched-link header2__sidenav-link mobile-nav-link mobile-nav-link" href="{{ 'https://'.config('app.url') }}">Back to Board</a>
                </li>
            </ul>
        </section>

        <section class="pl-2">
            <a href="{{ route('about') }}" class="inactive">About</a> ·
            <a href="{{ route('faq') }}" class="inactive">FAQ</a> ·
            <a href="{{ route('contactUs.createEmail') }}" class="inactive">Contact Us</a> ·
            <a href="https://forms.gle/15wUg56eDbMAe43q6" target="_blank" class="inactive">Give a Feedback</a>
            {{-- <a href="https://bit.ly/board_made_tutor" target="_blank" class="inactive">Become a Tutor</a> --}}
        </section>

        <section class="nav-footer mobile-nav-footer py-2">
            © Board {{ now()->year }}
        </section>
    </nav>
</div>
