<div class="nav nav-pills resource-nav-pills" role="tablist" aria-orientation="vertical">
    <a class="nav-link active" id="general_nav_link" data-toggle="pill" href="#general_chapter" role="tab">
        General
        <span class="subtitle">({{ count($subject->general_guides ?? []) }})</span>
    </a>
    @foreach($subject->chapters as $chapter)
        <a class="nav-link" id="{{ str_slug($chapter->name) }}_nav_link" data-toggle="pill" href="#{{ str_slug($chapter->name) }}_chapter" role="tab">
            {{ $chapter->name }}
            <span class="subtitle">({{ count($chapter->guides ?? []) }})</span>
        </a>
    @endforeach
</div>
