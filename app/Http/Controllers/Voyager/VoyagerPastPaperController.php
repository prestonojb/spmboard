<?php

namespace App\Http\Controllers\Voyager;

use Illuminate\Http\Request;
use App\ExamBoard;
use App\PastPaper;
use App\Resource;
use App\Subject;
use Illuminate\Support\Facades\Storage;

class VoyagerPastPaperController extends \TCG\Voyager\Http\Controllers\VoyagerBaseController
{
    protected $valid_paper_types = ['qp', 'ms'];

    /**
     * Create Past Papers
     */
    public function create(Request $request)
    {
        $igcse_exam_board = ExamBoard::where('slug', 'igcse')->first();
        $subjects = $igcse_exam_board->subjects()->get();
        return view('vendor.voyager.past_papers.create', compact('subjects'));
    }

    /**
     * Store Past Papers
     */
    public function store(Request $request)
    {
        // Validation
        $request->validate([
            'subject_id' => 'required',
            'files.*' => 'required|mimes:pdf',
        ]);

        $files = $request->file('files');
        $subject = Subject::find($request->subject_id);

        if ($subject->exam_board->slug == 'igcse') {
            // Validation
            foreach($files as $file) {
                $file_name = $file->getClientOriginalName();

                // Ensure there are 3 "_" in file name
                if( substr_count($file_name, '_') !== 3 ) {
                    return redirect()->back()->with(['alert-type' => 'error', 'message' => 'File name is invalid. Please try again.']);
                }

                if(!$this->isValidPaperType( $file_name ) ) {
                    return redirect()->back()->with(['alert-type' => 'error', 'message' => 'Only QP and MS are allowed to be uploaded. Please try again.']);
                }

                // Get paper data
                $paper_year = $this->getIgcsePaperYear($file_name);
                $paper_season = $this->getIgcsePaperSeason($file_name);
                $paper_paper_number = $this->getIgcsePaperPaperNumber($file_name);

                if( is_null($paper_year) ) {
                    return redirect()->back()->with(['alert-type' => 'error', 'message' => 'Paper year is invalid. Please try again.']);
                }

                if( is_null($paper_season) ) {
                    return redirect()->back()->with(['alert-type' => 'error', 'message' => 'Paper season is invalid. Please try again.']);
                }

                if( is_null($paper_paper_number) ) {
                    return redirect()->back()->with(['alert-type' => 'error', 'message' => 'Paper number is invalid. Please try again.']);
                }
            }

            // IGCSE Past Papers
            foreach($files as $file) {
                $file_name = $file->getClientOriginalName();

                // Check if paper is QP/MS
                $is_mark_scheme = $this->isMarkScheme($file_name);

                // Get paper data
                $paper_year = $this->getIgcsePaperYear($file_name);
                $paper_season = $this->getIgcsePaperSeason($file_name);
                $paper_paper_number = $this->getIgcsePaperPaperNumber($file_name);

                // Check if paper exists
                $past_paper = PastPaper::where('subject_id', $subject->id)
                                    ->where('year', $paper_year)
                                    ->where('season', $paper_season)
                                    ->where('paper_number', $paper_paper_number)
                                    ->first();

                if( is_null($past_paper) ) {
                    // Save files on S3
                    $paper_url = PastPaper::upload($file, $subject->exam_board);

                    // Create Past Paper
                    $past_paper = $subject->past_papers()->create([
                                        'name' => $this->getIgcsePaperName($subject, $file_name),
                                        'year' => $paper_year,
                                        'paper_number' => $paper_paper_number,
                                        'season' => $paper_season,
                                        'code' => $this->getIgcsePaperCode($file_name),
                                        'type' => null,
                                        'qp_url' => $is_mark_scheme ? null : $paper_url,
                                        'ms_url' => $is_mark_scheme ? $paper_url : null,
                                    ]);
                    // Create resource
                    Resource::create([
                        'resourceable_type' => PastPaper::class,
                        'resourceable_id' => $past_paper->id
                    ]);
                } else {
                    // Add file to past paper row
                    $past_paper->addFile($file, $is_mark_scheme);
                }
            }
        } else {
            // Other Past Papers
            return redirect()->back()->with(['alert-type' => 'error', 'message' => 'Only IGCSE Past Papers can be uploaded here!']);
        }

        return redirect('admin/past-papers')->withSuccess('Past Year Paper uploaded successfully!');
    }

    public function update(Request $request, $id)
    {
        PastPaper::where('id', $id)
                    ->update([
                        'name' => $request->name,
                        'subject_id' => $request->subject_id,
                        'paper_number' => $request->paper_number,
                        'year' => $request->year,
                        'code' => $request->code,
                        'type' => $request->type,
                        'season' => $request->season,
                    ]);

        $past_paper = PastPaper::find($id);

        // Save QP and MS files
        $qp_file = $request->file('qp_url')[0] ?? null;
        $ms_file = $request->file('ms_url')[0] ?? null;
        $past_paper->save_files($qp_file, $ms_file, true);

        return redirect('admin/past-papers')->withSuccess('Past Year Paper uploaded successfully!');
    }

    public function destroy(Request $request, $id)
    {
        $past_paper = PastPaper::find($id);
        $past_paper->delete();

        return redirect('admin/past-papers')->with('error', 'Past Year Paper deleted successfully!');
    }

    /**
     * Get IGCSE Past Paper Year
     * Works for years 1970 - 2050
     * @param string $file_name
     * @return int
     */
    public function getIgcsePaperYear($file_name)
    {
        // $season_year = 9710_**m20**_qp_21
        $season_year = explode('_', $file_name)[1];

        // Get year from file name and cast to int
        // $year = 9710_m**20**_qp_21
        $year = substr($season_year, 1, 2);
        $year = intval($year);

        // Returns year 2000 - 2050
        if( $year >= 0 && $year <= 50 ) {
            return 2000 + $year;
        }

        // Returns year 1970 - 1999
        if( $year >= 70 && $year <= 99 ) {
            return 1999 + $year;
        }

        return null;
    }

    /**
     * Returns paper season
     * @param string $file_name
     * @return string|null
     */
    public function getIgcsePaperSeason($file_name)
    {
        // $season_year = 9710_**m20**_qp_21
        $season_year = explode('_', $file_name)[1];
        $season_char = substr($season_year, 0, 1);

        switch($season_char) {
            case 's':
                return 'May/June';
            case 'w':
                return 'Oct/Nov';
            case 'm':
                return 'Feb/March';
            default:
                return null;
        }
    }

    /**
     * Returns Paper Code i.e. (0610/m18/qp/22)
     * @param string $file_name
     * @return string
     */
    public function getIgcsePaperCode($file_name) {
        $file_name = explode('.', $file_name)[0];
        return str_replace('_', '/', $file_name);
    }

    /**
     * Returns true if IGCSE paper type is valid, esle false
     * @param string $file_name
     * @return boolean
     */
    public function isValidPaperType($file_name)
    {
        return in_array($this->getIgcsePaperType($file_name), $this->valid_paper_types);
    }

    /**
     * Returns IGCSE paper type
     * @param $file_name
     * @return string
     */
    public function getIgcsePaperType($file_name)
    {
        // $type = 9710_m20_**qp**_21
        return explode('_', $file_name)[2];
    }

    /**
     * Returns true if paper is mark scheme, else false
     * @param string $file_name
     * @return boolean|null
     */
    public function isMarkScheme($file_name)
    {
        $type = $this->getIgcsePaperType($file_name);

        switch($type) {
            case 'qp':
                return false;
            case 'ms':
                return true;
            default:
                return null;
        }
    }

    /**
     * Returns paper number
     * @param string $file_name
     * @return int|null
     */
    public function getIgcsePaperPaperNumber($file_name)
    {
        // $number = 9710_m20_qp_**21**.pdf
        $end = explode('_', $file_name)[3];
        $paper_number = explode('.', $end)[0];
        if( ctype_digit($paper_number) ) {
            return (int) $paper_number;
        } else {
            return null;
        }
    }

    /**
     * Get IGCSE Past Paper Name
     * @param App\Subject $subject
     * @param string $file_name
     * @return string
     */
    public function getIgcsePaperName(Subject $subject, $file_name)
    {
        return "{$this->getIgcsePaperYear($file_name)} {$subject->name} {$this->getIgcsePaperSeason($file_name)} Paper {$this->getIgcsePaperPaperNumber($file_name)}";
    }
}
