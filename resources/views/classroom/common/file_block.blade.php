<div class="file-block">
    {{-- Link --}}
    @if(isset($resource) && !is_null($resource))
        @if( isset($type) && !is_null($type) )
            @if( $type == 'question' )
                <a target="_blank" href="{{ $resource->qp_url }}" class="stretched-link"></a>
            @elseif( $type == 'answer')
                <a target="_blank" href="{{ $resource->ms_url }}" class="stretched-link"></a>
            @else
                <a target="_blank" href="{{ $resource->url }}" class="stretched-link"></a>
            @endif
        @else
            <a target="_blank" href="{{ $resource->url }}" class="stretched-link"></a>
        @endif
    @elseif( isset($custom_resource) )
        <a target="_blank" href="{{ $custom_resource->file_url }}" class="stretched-link"></a>
    @else
        <a target="_blank" href="{{ isset($url) ? $url : 'javascript:void(0)' }}" class="stretched-link"></a>
    @endif

    {{-- Preview --}}
    <div class="file-preview">
        <span class="iconify" data-icon="{{ isset($resource) ? $resource->data_icon : 'carbon:stacked-scrolling-1' }}" data-inline="false"></span>
    </div>

    {{-- Content --}}
    <div class="file-content">
        @if( isset($resource) )
            <div class="file-title">{{ $resource->name }}</div>
            <div class="file-type">
                {{ $resource->type }}
                @if(!is_null($resource->pivot->type))
                    {{ '('.ucfirst($resource->pivot->type).')' }}
                @endif
            </div>
        @elseif( isset($custom_resource) )
            <div class="file-title">{{ $custom_resource->file_name }}</div>
            <div class="file-type">Custom</div>
        @else
            <div class="file-title">{{ isset($name) ? $name : 'File' }}</div>
            <div class="file-type">{{ isset($type) ? $type : 'Custom File' }}</div>
        @endif
    </div>

    @if( isset($deletable) && $deletable )
        <div class="file-cross-button">
            <span class="iconify" data-icon="gridicons:cross-small" data-inline="false"></span>
        </div>
    @endif
</div>
