<?php

namespace App\Notifications;

use App\Answer;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\HtmlString;

class QuestionAnswered extends Notification implements ShouldQueue
{
    use Queueable;

    public $answer;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Answer $answer)
    {
        $this->answer = $answer;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database'];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'type' => Answer::class,
            'target' => $this->answer,
            'title' => "<b>{$this->answer->user->username}</b> answered your question \"{$this->answer->question->question}\"!",
        ];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject("👋 Your question is answered!")
                    ->greeting("{$this->answer->user->username} answered your question \"{$this->answer->question->question}\"!")
                    ->line(new HtmlString("<b>Click on the button below</b> to read the answer!"))
                    ->line("Don't forget to accept the answer to thank the answerer 😇, and ask follow-up questions if you still don't quite understand how to solve the question! 🙌")
                    ->action('View Answer', $this->answer->show_url)
                    ->line("See you on Board! 🚀");
    }
}
