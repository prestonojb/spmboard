<div class="panel">
    <div class="resource__exam-board">
        <span>
            {{ $exam_board }}
        </span>
    </div>
    <div class="mt-2 mb-3">
        <h1 class="font-size9 mb-1">
            <b>{{ $title }}</b>
            @if(isset($locked) && $locked)
                <span class="iconify red" data-icon="ant-design:lock-filled" data-inline="false"></span>
            @endif
        </h1>

        <div class="">
            {!! $description !!}
        </div>
    </div>

    {{ $slot }}
</div>
