<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Notes;
use App\Subject;
use Faker\Generator as Faker;

$subjects = Subject::all();

$factory->define(Notes::class, function (Faker $faker) use($subjects) {
    $subject = $subjects->random();
    $chapter = $subject->chapters->random();
    $types = ['one_page', 'comprehensive'];
    return [
        'subject_id' => $subject->id,
        'chapter_id' => $chapter->id,
        'description' => $faker->sentence,
        'name' => $faker->sentence(3),
        'type' => $types[ array_rand($types) ],
        'url' => 'https://www.w3.org/WAI/ER/tests/xhtml/testfiles/resources/pdf/dummy.pdf',
    ];
});
