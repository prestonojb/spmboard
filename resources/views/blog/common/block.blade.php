<div class="blog-block">
    <a href="{{ route('blog.show', $blog->slug) }}" class="stretched-link"></a>
    <img src="{{ $blog->img }}">

    <div class="blog-block__body">
        <h2 class="blog-block__title">{{ $blog->title }}</h2>
        <p class="gray2 font-size3">
            <i>
                By {{ $blog->blog_author->name }}
            </i>
        </p>
        <div>
            <a style="z-index: 100;" class="blog-tag sm mb-1" href="javascript:void(0)">
                {{ $blog->blog_category->name }}
            </a>
        </div>
    </div>
</div>
