@extends('classroom.layouts.app')

@section('meta')
<title>Question | {{ config('app.name') }}</title>
@endsection

@section('styles')
@endsection

@section('content')
@component('classroom.common.breadcrumbs')
    <a href="{{ route('classroom.classrooms.show', $classroom->id) }}">Dashboard</a> /
    <a href="{{ route('classroom.questions', $classroom->id) }}">Questions</a> /
    {{ str_limit($question->question, 25) }}
@endcomponent

@component('classroom.common.page_heading', ['title' => 'Question'])
@endcomponent


<div class="row">
    <div class="col-12 col-lg-9">
        @include('teacher.questions.common.qs-question-block')
        @foreach($answers as $answer)
            @include('teacher.questions.common.answer-block')
        @endforeach

        @include('teacher.questions.common.answer-input')
    </div>
</div>


@endsection

@section('scripts')
<script>
$(function(){
    function getParameterByName(name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, '\\$&');
        var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, ' '));
    }

    $('[type=submit]').click(function(e){
        e.preventDefault();
        $(this).attr('disabled', true);
        $(this).closest('form').submit();
    });

    var answerFocus = getParameterByName('answerFocus');
    if(answerFocus) {
        if(quill !== undefined) {
            quill.focus();
        }
    }

    $('.answer-button').click(function(){
        if(quill !== undefined) {
            quill.focus();
        }
    });
});
</script>
@endsection
