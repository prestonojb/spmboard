@component('mail::message')
# Homework Notification
## Hi {{ $student->parent->name }}, Homework has been assigned to your child( {{ $student->name }} ).
*Kindly ensure that your child finishes his/her classwork before the due date.*

### Homework Details
@component('mail::panel')
Title: {{ $homework->title }}

Class: {{ $homework->lesson->classroom->name }}

Due at: {{ $homework->due_at->toFormattedDateString() }}
@endcomponent


Regards,
{{ $homework->lesson->classroom->teacher->name }}

Teacher of Class {{ $homework->lesson->classroom->name }}

@endcomponent
