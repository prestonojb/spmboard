<div class="panel resource__block">
    <div class="row">
        <div class="col-12 @if(!isset($side)) col-lg-8 @endif mb-2">
            <h3 class="">
                <a href="{{ $paper->qp_route }}" class="font-size-inherit blue-underline">
                    <b>{{ $paper->name }}</b>
                </a>
            </h3>
            <h4 class="gray2 font-size2 font-weight-normal mb-0">{{ $paper->description }}</h4>
        </div>

        <div class="col-12 @if(!isset($side)) col-lg-4 @endif d-flex justify-content-end align-items-center">
            <div class="">
                <a href="{{ $paper->qp_route }}" class="button--primary sm mr-1">Question</a>
                <a href="@cannot('answer_show', $paper) javascript:void:(0) @else {{ $paper->ms_route }} @endcannot" class="button--primary--inverse sm @cannot('answer_show', $paper) disabled @endcannot">Answer</a>
            </div>
        </div>
    </div>
</div>
