<?php

namespace App\Http\Controllers\Voyager;

use Illuminate\Http\Request;
use App\Chapter;
use App\Subject;
use App\ExamBuilderItem;
use App\ExamBuilderSheet;
use App\Http\Controllers\Controller;
use App\Jobs\LoadExamBuilderSheet;

class VoyagerExamBuilderSheetController extends Controller
{
    /**
     * Get generate all view
     */
    public function getGenerateTopicalView()
    {
        $this->middleware('admin_auth');
        $subjects = Subject::whereHas('exam_builder_items')->get();
        return view('vendor.voyager.exam_builder.sheets.generate', compact('subjects'));
    }

    /**
     * Generate all QP for subject
     * @param $subject
     * @param $is_question
     */
    public function generateTopical()
    {
        $this->middleware('admin_auth');
        $user = auth()->user();
        request()->validate([
            'name' => 'required',
            'chapter' => 'required',
            'paper_number' => 'required',
            'min_year' => 'nullable|integer',
            'max_year' => 'nullable|integer',
        ]);
        // Get subject
        $subject = Subject::find(request('subject'));
        $chapter = Chapter::find(request('chapter'));

        $min_year = request('min_year') ?? 0;
        $max_year = request('max_year') ?? 9999;
        $seasons = request('seasons');

        if($chapter->isParentChapter()) {
            $items = ExamBuilderItem::whereIn('chapter_id', $chapter->getRelatedChapterIds());
        } else {
            // Get all exam builder items from subject
            $items = ExamBuilderItem::where('chapter_id', $chapter->id);
        }

        // Paper Number filter
        $items = $items->where('paper_number', request('paper_number'));

        // Year Filter
        $items = $items->whereBetween('year', [$min_year, $max_year]);

        // Season Filter
        if(is_array($seasons) && !is_null($seasons)) {
            $items = $items->whereIn('season', $seasons);
        }

        $items = $items->orderBy('year')->orderBy('season')->get();

        // Filter complete items
        $items = $items->filter(function($item){
            return $item->is_complete;
        });

        // Set options
        $show_label = true;
        $hide_watermark = false;

        // Store Sheet on DB
        $sheet = $user->exam_builder_sheets()->create([
                    'name' => request('name'),
                    'subject_id' => $subject->id,
                    'paper_number' => request('paper_number'),
                    'qp_status' => 'loading',
                ]);
        // Save chapters to sheet
        $sheet->chapters()->attach( $chapter->id );
        $sheet->items()->attach( $items->pluck('id')->toArray() );

        // ADD QP/MS PDF generation to queue
        LoadExamBuilderSheet::dispatch($sheet->id, true, $show_label, $hide_watermark);
        LoadExamBuilderSheet::dispatch($sheet->id, false, $show_label, $hide_watermark);

        // Redirect
        return redirect()->route('exam_builder.sheets.show', $sheet->id);
    }
}
