<div id="reviews">
    <div class="border-bottom pb-2 mb-1">
        <div class="d-flex flex-wrap align-items-center">
            <h3 class="font-size6 mb-1 mr-1">Reviews</h3>
            <div class="Stars sm mb-1 mr-2" style="--rating: {{ $resource->rating }};"></div>
        </div>
        <p class="gray2 font-size1 mb-0">*You can only write a review if you have full access to the resource. Only top 5 reviews are displayed.</p>
    </div>

    @can('leave_review', $resource)
        <div class="py-2">
            <button type="button" class="button--primary sm" data-toggle="modal" data-target="#createEditResourceReviewModal">
                <span class="iconify" data-icon="bx:bx-edit" data-inline="false"></span>
                Write a review</button>
            @component('resources.resource_reviews.modal.create_edit', ['resource' => $resource, 'exam_board' => $exam_board])
            @endcomponent
        </div>
    @endcan

    @if(count($resource->reviews) ?? [])
        @foreach($resource->reviews()->orderByDesc('rating')->latest()->take(5)->get() as $review)
            <div class="py-1">
                @component('resources.resource_reviews.common.block', ['resource_review' => $review])
                @endcomponent
            </div>
        @endforeach
    @else
        <div class="text-center py-2">
            <img src="{{ asset('img/essential/empty.svg') }}" class="" width="200" height="200">
            <p class="gray2">No reviews yet</p>
        </div>
    @endif
</div>
