<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExamBuilderItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exam_builder_items', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->unsignedInteger('subject_id');
            $table->unsignedInteger('chapter_id');

            $table->unsignedInteger('year');
            $table->string('season')->nullable();
            $table->unsignedInteger('paper_number');
            $table->unsignedInteger('label_number');

            $table->json('question_imgs')->nullable();
            $table->json('answer_imgs')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exam_builder_items');
    }
}
