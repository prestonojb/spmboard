@php
    $exam_boards = App\ExamBoard::all();
@endphp

{{-- Header --}}
<header class="w100-header">
    <div class="container">
        <button type="button" class="button-link resource__board-select-button" data-toggle="modal" data-target="#boardSelect">
            <span class="name">{{ $exam_board->name }}</span>
            <span class="iconify" data-icon="dashicons:arrow-down" data-inline="false"></span>
        </button>
        <h1 class="mb-1"><b>Exam Builder</b></h1>
        <h2 class="h4 gray2">Build your exam papers and start practicing!</h2>
    </div>
</header>

{{-- Modal --}}
<div class="modal" id="boardSelect" tabindex="-1" role="dialog" aria-labelledby="sortSelect" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="list-group">
                @foreach($exam_boards as $board)
                    <a href="{{ route('exam_builder.landing').'?exam_board='.$board->slug }}" class="list-group-item list-group-item-action @if($board->slug == $exam_board->slug) active @endif">{{ $board->name }}</a>
                @endforeach
            </div>
        </div>
    </div>
</div>
