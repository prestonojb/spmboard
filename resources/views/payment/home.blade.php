@extends('forum.layouts.app')

@section('meta')
<title>Payment | {{ config('app.name') }}</title>
@endsection

@section('content')
@include('main.navs.header')
<section class="payment">
    <header class="w100-header">
        <div class="container">
            <h1 class="mt-2 mb-1"><b>Payment</b></h1>
            <h2 class="h4 gray2">Accelerate your learning by topping up for coins!</h2>
            @if(count($payment_methods ?? []) > 0)
                <a href="{{ route('products.coins') }}" class="button--danger" type="button">
                    <img class="inline-image" src="{{ asset('img/essential/coin.svg') }}" alt="">
                    Buy Coins</a>
            @else
                <span class="d-inline-block" tabindex="0" data-toggle="tooltip" title="Update default payment to start buying coins!">
                    <button class="button--danger" style="pointer-events: none;" type="button" disabled>
                        <img class="inline-image" src="{{ asset('img/essential/coin.svg') }}" alt="">
                        Buy coins</button>
                </span>
            @endif
        </div>
    </header>
    <div class="container">
        <div class="row py-2">
            <div class="col-12 col-lg-9">
                <div class="panel mb-2">
                    <label>
                        Default Payment Method
                        <button type="button" class="gray2 p-0" data-toggle="popover" data-content="The default credit/debit card will be charged for all the cash purchases on Board.">
                            <i class="fas fa-info-circle"></i>
                        </button>
                    </label>
                    <form action="{{ route('payment.update_default_payment_method') }}" method="POST">
                        @csrf
                        <div class="mb-2">
                            <select class="" name="default_payment_method" id="default_payment_method">
                                <option value="" disabled selected>Debit/Credit Card</option>
                                @foreach($payment_methods as $payment_method)
                                    <option value="{{ $payment_method->id }}" @if($default_payment_method && $default_payment_method->id == $payment_method->id) selected @endif>
                                        [{{ strtoupper($payment_method->card->brand) }}] •••• •••• •••• {{ $payment_method->card->last4 }}(Expires at {{ $payment_method->card->exp_month }}/{{ $payment_method->card->exp_year }})
                                    </option>
                                @endforeach
                            </select>
                            @error('default_payment_method')
                                <div class="error-message">{{ $message }}</div>
                            @enderror
                            <p class="gray2 font-size1 mt-1">All credit/debit card details are handled by our payment gateway provider <a target="_blank" href="https://stripe.com" class="font-size-inherit">Stripe</a>. We do not store any of your personal credit/debit card details.</p>
                        </div>

                        <button type="submit" class="button--primary">Update</button>
                        <button type="button" class="button--primary--inverse mr-1" data-toggle="modal" data-target="#addCardModal">Add Card</button>
                    </form>
                </div>

                <div class="py-2"></div>

                {{-- Invoices --}}
                @component('components.section', ['title' => 'Invoices'])
                    <div class="panel p-0">
                        @if(count($invoices ?? []) > 0)
                            <div class="overflow-x-auto">
                                <table class="table mb-0">
                                    <thead>
                                        <th>Date</th>
                                        <th>Description</th>
                                        <th>Amount (USD)</th>
                                        <th></th>
                                    </thead>
                                    <tbody>
                                        @foreach($invoices as $invoice)
                                            <tr>
                                                <td>{{ $invoice->date()->toFormattedDateString() }}</td>
                                                <td>{{ $invoice->lines->data[0]->description }}</td>
                                                <td>{{ $invoice->total() }}</td>
                                                <td>
                                                    <form action="{{ route('payment.download_invoice', $invoice->id) }}" method="POST">
                                                        @csrf
                                                        <input type="hidden" name="date" value="{{ $invoice->date()->toFormattedDateString() }}">
                                                        <button type="submit" class="button-link blue4">
                                                            Download
                                                        </button>
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        @else
                            <div class="gray2 text-center py-3">
                                No invoice found
                            </div>
                        @endif
                    </div>
                @endcomponent

                <div class="py-2"></div>
            </div>

            <div class="col-12 col-lg-3">
                <div class="alert alert-info font-size3" role="alert">
                    <p class="mb-0"><b>NOTE:</b></p>
                    Default Debit/Credit card will be <b>charged</b> for any in-app purchases.
                </div>
            </div>

        </div>
    </div>
</section>

@include('payment.common.add_card_modal');
@endsection

@section('scripts')
<script src="https://js.stripe.com/v3/"></script>
<script>
const stripe = Stripe('{{ config("services.stripe.key") }}');

const elements = stripe.elements();
const cardElement = elements.create('card');

cardElement.mount('#card-element');

const cardHolderName = document.getElementById('card-holder-name');
const cardButton = document.getElementById('card-button');
const cardForm = document.getElementById('card-form');
const paymentMethodInput = document.querySelector('[name="payment_method"]');

cardButton.addEventListener('click', async (e) => {
    e.target.disabled = true;
    const { paymentMethod, error } = await stripe.createPaymentMethod(
        'card', cardElement, {
            billing_details: { name: cardHolderName.value }
        }
    );

    if (error) {
        e.target.disabled = false;
        // Display "error.message" to the user...
        cardError.textContent = error.message;
    } else {
        // The card has been verified successfully...
        paymentMethodInput.value = paymentMethod.id;
        cardForm.submit();
    }
});
</script>
@endsection
