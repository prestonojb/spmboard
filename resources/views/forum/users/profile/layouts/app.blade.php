@extends('forum.layouts.app')
@section('meta')
    @yield('meta')
    @yield('styles')
@endsection

@section('content')

<div class="py-2"></div>
@include('main.navs.header')

<div class="container">
    <div class="form-row">

        <div class="col-12 col-lg-9">
            @if($user->is_teacher())
                @component('users.profile.teacher', ['teacher' => $user->teacher])
                @endcomponent
            @elseif($user->is_student())
                @component('users.profile.student', ['student' => $user->student])
                @endcomponent
            @elseif($user->is_parent())
                @component('users.profile.parent', ['parent' => $user->parent])
                @endcomponent
            @endif

            @yield('main')
        </div>

        <div class="d-none d-lg-block col-lg-3">
            @if($user->is_teacher())
                @component('teacher.profile.more', ['teacher' => $user->teacher])
                @endcomponent
            @elseif($user->is_student())
                @component('student.profile.more', ['student' => $user->student])
                @endcomponent
            @endif
        </div>

    </div>
</div>

<div class="py-2"></div>
@endsection

@section('scripts')
{{-- Sweet Alert 2 --}}
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
{{-- Magnific Popup core JS file --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.js"></script>
<script>
$(function(){
    @if($errors->any())
        $('#recommendationModal').modal('show');
    @endif

    const upvoteButtonAnimation = 'animated pulse faster';

    $('.answer-upvote-button').click(function(e){
        const answer_id = $(this).data('answerid');
        var loggedIn = {{ auth()->check() ? 'true' : 'false' }};
        if(loggedIn){
            //Get the right upvote count <span>
            var $upvoteCount = $('.answer-upvote-count[data-answerid="'+ answer_id +'"]');
            //Get the upvote count of the question
            var upvote_count = parseInt($upvoteCount.text());
            if($(this).hasClass('upvoted')){
                upvote_count--;
                $upvoteCount.text(upvote_count);
            } else{
                upvote_count++;
                $upvoteCount.text(upvote_count);

                // Animate the upvote button
                $(this).addClass(upvoteButtonAnimation).on('animationend', function(){
                    $(this).removeClass(upvoteButtonAnimation);
                });
            }
            $(this).toggleClass('upvoted');
            var url = "{{ route('answers.upvote', ':answer_id') }}";
            url = url.replace(':answer_id', answer_id);
            $.ajax({
                method: 'POST',
                url: url,
            });
        } else{
            Swal.fire({
                title: '<strong>Oops! You are not logged in!</strong>',
                type: 'error',
                html:
                    '<p>Log in to unlock more features</p>',
                showCloseButton: true,
                showCancelButton: true,
                focusConfirm: false,
                reverseButtons: true,
                confirmButtonText: 'Log in',
                cancelButtonText: 'Not now',
                }).then(function(result){
                    if(result.value){
                        window.location.href = '{{ route('login') }}';
                    }
                });
        }
    });

    $('.answer-button').click(function(){
        var url = $(this).data('url');
        window.location.href = url + '?answerFocus=true';
    });

    //Image popup
    $('.question--block .question-block__image').click(function(){
        var img_src = $(this).attr('src');
        $.magnificPopup.open({
            items: {
                src: img_src,
            },
            type: 'image',
            showCloseButton: false,
        });
    });

    $('.question-upvote-button').click(function(e){
        const loggedIn = {{ auth()->check() ? 'true' : 'false' }};
        if(loggedIn){

            const question_id = $(this).data('questionid');
            var url = "{{ route('questions.upvote', ':question_id') }}";
            url = url.replace(':question_id', question_id);

            $.ajax({
                method: 'POST',
                url: url,
            });

            const $upvoteCount = $('.upvote-count[data-questionid="'+ question_id +'"]');
            //Get the upvote count of the question
            var upvote_count = parseInt($upvoteCount.text());

            $(this).toggleClass('upvoted');
            if($(this).hasClass('upvoted')){
                upvote_count++;
                $upvoteCount.text(upvote_count);

                // Animate the upvote button
                $(this).addClass(upvoteButtonAnimation).on('animationend', function(){
                    $(this).removeClass(upvoteButtonAnimation);
                });
            } else{
                upvote_count--;
                $upvoteCount.text(upvote_count);
            }

        } else{
            Swal.fire({
                title: '<strong>Oops! You are not logged in!</strong>',
                type: 'error',
                html:
                    '<p>Log in to gain the best experience with us!</p>',
                showCloseButton: true,
                showCancelButton: true,
                focusConfirm: false,
                reverseButtons: true,
                confirmButtonText: 'Log in',
                cancelButtonText: 'Not now',
            }).then(function(result){
                if(result.value){
                    window.location.href = '{{ route('login') }}';
                }
            });
        }
    });

    //Image popup
    $('.question-img').click(function(){
        var img_src = $(this).attr('src');
        $.magnificPopup.open({
            items: {
                src: img_src,
            },
            type: 'image',
            showCloseButton: false,
        });
    });

});
</script>
@endsection
