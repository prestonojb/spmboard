@extends('classroom.layouts.app')


@section('meta')
    <title>Submission #{{ $homework_submission->id }} | {{ config('app.name') }} Classroom</title>
@endsection

@section('content')
@component('classroom.common.breadcrumbs')
    <a href="{{ route('classroom.classrooms.show', $classroom->id) }}">Dashboard</a> /
    <a href="{{ route('classroom.lessons.index', $classroom->id) }}">Lessons</a> /
    <a href="{{ route('classroom.lessons.show', [$classroom->id, $lesson->id]) }}">{{ $lesson->name }}</a> /
    <a href="{{ route('classroom.lessons.homeworks.show', [$homework_submission->homework->lesson->id, $homework->id]) }}">{{ str_limit($homework->title, 50) }}</a> /
    Submission #{{ $homework_submission->id }}
@endcomponent

@component('classroom.common.page_heading', ['title' => "Submission #{$homework_submission->id}"])
@endcomponent


<div class="row">
    <div class="col-12 col-lg-9">

        @component('classroom.homeworks.submissions.common.block', ['classroom' => $classroom, 'submission' => $homework_submission])
        @endcomponent

        <hr>

        <h3><b>Teacher's Feedback</b></h3>
        @if( $homework_submission->isMarkable() )
            @can('mark', $homework)
                {{-- Mark and Return Homework Submission --}}
                <div class="panel">
                    <form action="{{ route('classroom.lessons.homeworks.submissions.mark', [$lesson->id, $homework_submission->id]) }}" method="post" enctype="multipart/form-data">
                        @csrf
                        {{-- <input type="hidden" name="return_homework_submission" value="0"> --}}
                        @if( $homework->isGraded() )
                            <div class="form-field">
                                <label>Marks</label>
                                <input name="marks" type="number" min="0" max="{{ $homework->max_marks }}" step="1" required value="{{ old('marks') }}">
                                <p class="mb-0 font-size3 gray2">Max Marks: {{ $homework->max_marks }}</p>
                                @error('marks')
                                    <span class="error-message">{{ $message }}</span>
                                @enderror
                            </div>
                        @endif

                        <div class="form-field">
                            <label>Marked File(Optional)</label>
                            <input type="file" name="marked_file" id="">
                            @error('marked_file')
                                <span class="error-message">{{ $message }}</span>
                            @enderror
                        </div>

                        <div class="form-field">
                            <label>Remarks(Optional)</label>
                            <textarea name="teacher_remark" rows="3">{{ old('teacher_remark') }}</textarea>
                            @error('teacher_remarks')
                                <span class="error-message">{{ $message }}</span>
                            @enderror
                        </div>

                        <div class="d-flex align-items-center justify-content-start mb-3">
                            <input type="checkbox" name="notify_parents" id="notify_parents" {{ old('notify_parents') ? 'checked' : '' }}>
                            <label for="notify_parents">{{ __('Notify parents') }}</label>
                        </div>

                        <div class="text-right">
                            {{-- <button type="submit" class="button--primary--inverse markButton">Mark</button> --}}
                            <button type="submit" class="button--primary disableOnSubmitButton">Mark and Return</button>
                        </div>
                    </form>
                </div>
            @endcan

        @elseif( $homework_submission->isMarked() )
            {{-- Homework Submission Data --}}
            <div class="panel">
                @php
                    $data = [];
                    if($homework_submission->isMarked()) {
                        $data['Percentage Marks'] = $homework_submission->hasMarks() ? $homework_submission->marks_in_percentage.'%' : 'Ungraded';
                        $data['Absolute Marks'] = $homework_submission->hasMarks() ? $homework_submission->absolute_marks_to_max : 'Ungraded';
                    }
                    $data['Returned At'] = $homework_submission->isMarked() ? $homework_submission->returned_at->format('M d, Y h:iA') : 'N/A';
                    $data['Remark'] = $homework_submission->teacher_remark ?? 'N/A';
                @endphp
                @foreach($data as $key => $value)
                    @include('classroom.common.key_value_pair')
                @endforeach

                <div class="key-value-pair">
                    <div class="key">
                        Marked File
                    </div>
                    <div class="value">
                        @if( !empty($homework_submission->marked_file_url) )
                            @component('classroom.common.file_block', ['url' => $homework_submission->marked_file_url])
                            @endcomponent
                        @else
                            N/A
                        @endif
                    </div>
                </div>

            </div>
        @endif
    </div>
</div>

<div class="py-2"></div>
@endsection

@section('script')
<script>
</script>
@endsection
