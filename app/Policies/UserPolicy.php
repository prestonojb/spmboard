<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function view_calendar(User $user)
    {
        return $user->teacher || $user->student;
    }

    public function view_classroom_billing(User $user)
    {
        return $user->teacher;
    }

    /**
     * Permission to withdraw coins
     */
    public function withdraw_coins(User $user)
    {
        return $user->hasCoins() && $user->is_teacher();
    }
}
