<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddStatusColumnsToExamBuilderSheetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('exam_builder_sheets', function (Blueprint $table) {
            $table->string('qp_status')->default('available');
            $table->string('ms_status')->default('unavailable');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('exam_builder_sheets', function (Blueprint $table) {
            $table->dropColumn('qp_status');
            $table->dropColumn('ms_status');
        });
    }
}
