<section>
    <div class="container">
        <div class="py-5">
            <div class="mb-3 mb-md-4">
                <h4 class="text-center font-size9"><b>FAQ</b></h4>
            </div>
            <div class="accordion" id="accordian__classroom">
                @foreach($qna as $question => $answer)
                    <div class="card faq__card">
                        <div class="card-header faq__card-header">
                            <h5 class="mb-0">
                                <button class="button-link" data-toggle="collapse" data-target="#question_{{ $loop->index }}" aria-expanded="true" aria-controls="collapseOne">
                                    {{ $question }}
                                </button>
                            </h5>
                        </div>
                        <div id="question_{{ $loop->index }}" class="collapse" aria-labelledby="headingOne" data-parent="#accordian__classroom">
                            <div class="card-body faq__card-body">
                                {!! $answer !!}
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</section>
