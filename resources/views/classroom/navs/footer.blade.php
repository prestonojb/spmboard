<!-- Footer -->
<footer class="sticky-footer bg-white border-top">
    <div class="container my-auto">
        <div class="copyright text-center my-auto">
            <span>Copyright &copy; {{ config('app.name') }} 2020</span>
        </div>
    </div>
</footer>
