<?php

namespace Tests\Feature\Classroom;

use App\Classroom;
use App\ExamBoard;
use App\Student;
use App\Subject;
use App\Teacher;
use App\User;
use Tests\TestCase;

use function Deployer\get;

class ClassroomTest extends TestCase
{
    private $subscribed_teacher;
    private $student_with_board_pass;
    private $subject;

    public function setUp() : void
    {
        parent::setUp();

        // Subscribed Teacher
        $user = factory(User::class)->states('subscribed_teacher')->create();
        $this->subscribed_teacher = $user->teacher;

        // Unsubscribed Teacher
        $user = factory(User::class)->states('teacher')->create();
        $this->unsubscribed_teacher = $user->teacher;

        // Student with Board Pass
        $user = factory(User::class)->states('student_with_board_pass')->create();
        $this->student_with_board_pass= $user->student;

        // Subject and classroom
        $this->subject = Subject::first();
        $this->classroom = factory(Classroom::class)->create([
                                'teacher_id' => $this->subscribed_teacher->user_id,
                                'subject_id' => $this->subject->id,
                            ]);
    }

    /**
     * @group classroom
     * Test for creating classroom
     */
    public function testCreate()
    {
        $teacher = $this->subscribed_teacher;

        // GET response to create classroom view
        $response = $this->actingAs($teacher->user)
                    ->get( route('classroom.classrooms.create') );

        $response->assertSuccessful();
    }

    /**
    * @group classroom
     * Test for adding classroom
     */
    public function testStore()
    {
        $teacher = $this->subscribed_teacher;

        // Valid response to store classroom
        $response = $this->actingAs($teacher->user)
                        ->post( route('classroom.classrooms.store') , [
                            '_token' => csrf_token(),
                            'name' => 'Harvard 101',
                            'subject' => $this->subject->id,
                        ]);

        // Record is uploaded in database
        $this->assertTrue(
            Classroom::where('name', 'Harvard 101')
                    ->where('subject_id', $this->subject->id)
                    ->exists(),
        "Classroom is not stored!");

        $response->assertRedirect();
    }

    /**
     * @group classroom
     * Test for editing classroom
     */
    public function testEdit()
    {
        $teacher = $this->subscribed_teacher;

        // GET response to create classroom view
        $response = $this->actingAs($teacher->user)
                         ->get( route('classroom.classrooms.edit', $this->classroom->id) );
        $response->assertSuccessful();
    }

    /**
     * @group classroom
     * Test for updating classroom
     */
    public function testUpdate() {
        $teacher = $this->subscribed_teacher;

        // PATCH response to update classroom
        $response = $this->actingAs($teacher->user)
                        ->patch( route('classroom.classrooms.update', $this->classroom->id), [
                            'name' => 'Stanford 101',
                        ]);

        // Record is uploaded in database
        $this->assertTrue(
            Classroom::where('name', 'Stanford 101')
                        ->where('subject_id', $this->subject->id)
                        ->exists()
        );

        $response->assertRedirect( route('classroom.classrooms.show', $this->classroom->id) );
    }

    /**
     * @group classroom
     * Test for getting joining classroom view
     */
    public function testGetJoinClassView()
    {
        $student = $this->student_with_board_pass;

        // GET request to join classroom view
        $response = $this->actingAs($student->user)->get( route('classroom.classrooms.students.getJoinClassView') );
        $response->assertSuccessful();
    }

    /**
     * @group classroom
     * Test for joining classroom
     */
    public function testJoin()
    {
        $student = $this->student_with_board_pass;

        // POST request to join classroom
        $response = $this->actingAs($student->user)
                        ->post( route('classroom.classrooms.students.join') , [
                            'code' => $this->classroom->code
                        ]);

        // Student is added into classroom
        $this->assertTrue(
            $this->classroom->students->contains( $student->user_id ),
            "Student is not added into classroom"
        );
        $response->assertRedirect( route('classroom.classrooms.show', $this->classroom->id) );
    }
}
