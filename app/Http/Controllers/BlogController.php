<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Blog;
use App\BlogAuthor;
use App\BlogCategory;
use App\Rules\DifferentBlogSlug;
use Illuminate\Pagination\LengthAwarePaginator;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $blogs = Blog::orderByDesc('updated_at')->paginate(10);
        $blog_categories = BlogCategory::all();
        return view('blog.index', ['blogs' => $blogs, 'blog_categories' => $blog_categories]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Blog $blog)
    {
        //Exclude current articles, show 3 newest articles
        $recent_blogs = Blog::where('id', '!=', $blog->id)->orderByDesc('updated_at')->take(3)->get();
        $related_blogs = Blog::where('id', '!=', $blog->id)->where('blog_category_id', $blog->blog_category->id)->inRandomOrder()->take(3)->get();
        return view('blog.show', ['blog' => $blog, 'recent_blogs' => $recent_blogs, 'related_blogs' => $related_blogs]);
    }
}
