<div class="accordion custom-accordion-menu" id="guideChapterMenu">
    @if($current_guide->subject->hasGeneralGuides())
        <div class="card">
            <div class="card-header @if($current_guide->isGeneral()) active @endif">
                <button class="p-0 text-left stretched-link @if(!$current_guide->isGeneral()) collapsed @endif" type="button" data-toggle="collapse" data-target="#guideChapterMenu_general" aria-expanded="true" aria-controls="collapseOne">
                    <h5 class="mb-0">
                        General
                    </h5>
                    <div class="arrow-icon">
                        <i class="fas fa-chevron-right"></i>
                    </div>
                </button>
            </div>

            <div id="guideChapterMenu_general" class="collapse @if($current_guide->isGeneral()) show @endif" aria-labelledby="headingOne" data-parent="#guideChapterMenu">
                <div class="card-body">
                    @foreach($current_guide->subject->general_guides as $guide)
                        <a href="{{ $guide->url }}" class="@if($guide->id == $current_guide->id)) active @endif">{{ $guide->title }}</a>
                    @endforeach
                </div>
            </div>
        </div>
    @endif
    @foreach($current_guide->subject->chapters()->whereHas('guides')->get() as $chapter)
        <div class="card">
            <div class="card-header @if($chapter->hasGuide($current_guide)) active @endif">
                <button class="p-0 text-left stretched-link @if(!$chapter->hasGuide($current_guide)) collapsed @endif" type="button" data-toggle="collapse" data-target="#guideChapterMenu_{{ str_slug($chapter->name) }}" aria-expanded="true" aria-controls="collapseOne">
                    <h5 class="mb-0">
                        {{ $chapter->name }}
                    </h5>
                    <div class="arrow-icon">
                        <i class="fas fa-chevron-right"></i>
                    </div>
                </button>
            </div>

            <div id="guideChapterMenu_{{ str_slug($chapter->name) }}" class="collapse @if($chapter->hasGuide($current_guide)) show @endif" aria-labelledby="headingOne" data-parent="#guideChapterMenu">
                <div class="card-body">
                    @foreach($chapter->guides as $guide)
                        <a href="{{ $guide->url }}" class="@if($guide->id == $current_guide->id)) active @endif">{{ $guide->title }}</a>
                    @endforeach
                </div>
            </div>
        </div>
    @endforeach
</div>
