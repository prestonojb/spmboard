<div class="panel position-relative scale-on-hover fast border-left-blue5-3px">
    <a href="{{ route('classroom.children.show', $child->user_id) }}" class="stretched-link"></a>
    <div class="px-2 pt-3 pb-2">
        <h3 class="font-size7 font-weight700 mt-2 mb-3">{{ $child->name }}</h3>
        <div class="mb-3">
            <h4 class="gray2 font-size3 mb-1">{{ $child->understanding_rating }} Understanding Rating</h4>
            <h4 class="gray2 font-size3 mb-0">{{ $child->average_homework_marks }} Average Homework Marks</h4>
        </div>
    </div>
</div>
