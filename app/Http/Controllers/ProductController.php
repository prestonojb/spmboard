<?php

namespace App\Http\Controllers;

use App\Product;
use Exception;
use Amplitude;
use Illuminate\Support\Facades\Hash;

class ProductController extends Controller
{
    public function index()
    {
        // Retrieve products
        $cash_products = Product::where('price', '!=', 0)->get();
        $non_cash_products = Product::where('price', 0)->get();

        // Check if user has coins (when logged in)
        if(auth()->check()) {
            $user = auth()->user();
            if( !$user->is_student() ) {
                $message = ucfirst($user->account_type)." account is ineligible to purchase coins.";
                return redirect()->back()->with('error', $message);
            }
        }

        return view('products.coins', compact('cash_products', 'non_cash_products'));
    }

    public function purchase(Product $product)
    {
        $user = auth()->user();
        if( !$user->is_student() ) {
            $message = ucfirst($user->account_type)." account is ineligible to purchase coins.";
            return redirect()->back()->with('error', $message);
        }

        request()->validate([
            'password' => 'required',
        ]);

        // Returns incorrect password error
        if(!Hash::check(request('password'), $user->password)) {
            return redirect()->back()->with('error', 'Password is incorrect. Please try again.');
        }

        // Product not involving cash
        if ( !$product->involves_cash ) {
            if ( $user->reward_point_balance >= $product->price_in_rp ) {
                // Deduct reward points from user
                $user->deductRewardPoints($product->price_in_rp);
                $user->reward_points()->create([
                    'user_id' => $user->id,
                    'name' => 'Coin Purchase',
                    'reward_point_actionable_type' => Product::class,
                    'reward_point_actionable_id' => $product->id,
                    'amount' => -($product->price_in_rp),
                ]);

                // Add coins to user
                $user->addCoins($product->coins_in_return);
                $user->coin_actions()->create([
                    'name' => 'Coin Purchase with Reward Points',
                    'coin_actionable_type' => Product::class,
                    'coin_actionable_id' => $product->id,
                    'amount' => $product->coins_in_return
                ]);
            } else {
                return redirect()->back()->with('error', 'You have insufficient reward point balance.');
            }
        // Product involving cash
        } else {
            // Purchase with cash
            if ( $user->hasDefaultPaymentMethod() ) {
                try {
                    $user->invoiceFor($product->coins_in_return.' coins', $product->price * 100);
                } catch(Exception $e) {
                    return redirect()->back()->with('error', 'Payment processing failed! Please try again later or with a different payment method.');
                }

                // Add coins to user
                $user->addCoins($product->coins_in_return);
                $user->coin_actions()->create([
                    'name' => 'Coin Purchase',
                    'coin_actionable_type' => Product::class,
                    'coin_actionable_id' => $product->id,
                    'amount' => $product->coins_in_return
                ]);

                // Log event on Amplitude
                Amplitude::setUserId($user->id);
                Amplitude::queueEvent('purchase_coins', ['price_in_usd' => $product->price, 'coin_amount' => $product->coins_in_return]);

            } else {
                return redirect()->back()->with('error', 'You will need to setup your default payment method under the "Payment" Page before purchasing coins.');
            }
        }
        return redirect()->back()->withSuccess('Purchase successful! '. $product->coins_in_return .' coins have been added to your account!');
    }
}
