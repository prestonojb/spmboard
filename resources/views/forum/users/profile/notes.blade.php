@extends('forum.users.profile.layouts.app')

@section('meta')
    <title>Notes - {{ $user->username }} | {{ config('app.name') }}</title>
    <meta property="og:image" content="{{ $user->avatar }}" />
    <meta property="og:type" content="website" />
@endsection

@section('styles')
    {{-- Magnific Popup core CSS file --}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.css">
@endsection

@section('main')
    @if(count($notes ?? []))
        <div class="row">
            @foreach($notes as $note)
                <div class="col-12 col-md-6 col-lg-4 mb-2">
                    @component('resources.notes.common.panel', ['note' => $note])
                    @endcomponent
                </div>
            @endforeach
        </div>
        {{ $notes->links() }}
    @else
        <section class="text-center mb-4">
            <img src="{{ asset('img/essential/reading.svg') }}" alt="No Notes Found" width="220" height="220" class="mb-2">
            <h2 class="gray2 font-size4">No notes by user yet</h2>
        </section>
    @endif
@endsection
