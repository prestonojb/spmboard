@extends('classroom.layouts.app')

@section('meta')
    <title>Reporting | {{ config('app.name') }} Classroom</title>
@endsection


@section('content')
{{-- Breadcrumbs --}}
@component('classroom.common.breadcrumbs')
    <a href="{{ route('classroom.landing') }}">Home</a> /
    Reports
@endcomponent

{{-- Heading --}}
@component('classroom.common.page_heading', ['title' => 'Reporting'])
    @slot('button_group')
        <a class="button--primary sm @cannot('store', App\StudentReport::class) disabled @endcannot" @can('store', App\StudentReport::class) href="{{ route('classroom.reports.create') }}" @else href="javascript:void(0)" @endif><i class="fas fa-plus"></i> Create Report</a>
    @endslot
@endcomponent

@component('classroom.reports.common.filter', ['students' => $students])
@endcomponent

{{-- Table --}}
@component('classroom.reports.common.table', ['reports' => $reports, 'show_students' => true])
@endcomponent

<div class="py-2"></div>
@endsection
