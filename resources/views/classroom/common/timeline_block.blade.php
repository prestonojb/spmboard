<div class="panel p-0">
    <div class="panel-header">
        Timeline
    </div>
    <div class="panel-body p-0">
        @if( count($timeline_items ?? []) )
            @foreach($timeline_items as $item)
                <div class="position-relative @if(!$loop->last) border-bottom @endif white-hoverable">
                    <a href="{{ $item['url'] }}" class="stretched-link"></a>

                    <div class="d-flex justify-content-start align-items-center">

                        <div class="px-3">
                            @switch($item['type'])
                                @case('App\Lesson')
                                    <div class="icon-circle font-size7 bg-blue5 white">
                                        <span class="iconify" data-icon="ion:cube-outline" data-inline="false"></span>
                                    </div>
                                    @break
                                @case('App\Homework')
                                    <div class="icon-circle font-size7 bg-red white">
                                        <span class="iconify" data-icon="system-uicons:write" data-inline="false"></span>
                                    </div>
                                    @break
                            @endswitch
                        </div>

                        <div class="py-2 pr-2">
                            <h4 class="font-size3 font-weight500 mb-1">{{ $item['title'] }}</h4>
                            <p class="font-size1 gray2 mb-0">{{ $item['due_at']->shortRelativeDiffForHumans() }}</p>
                        </div>
                    </div>
                </div>
            @endforeach
        @else
            <div class="text-center py-2">No items</div>
        @endif
    </div>
</div>
