<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\StudentParent;
use App\Student;
use App\Teacher;
use App\User;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
        'username' => $faker->userName,
        'email' => $faker->unique()->safeEmail,
        'email_verified_at' => now(),
        'password' => bcrypt('asdasdasd'), // password
        'about' => $faker->sentence,
        'avatar' => 'users/November2020/tZjuMe7mmdv59z9uBXVVNnVH58jkoO5knMzjGdBn.png',
        'remember_token' => Str::random(10),
    ];
});

// Teacher subscribed to 'classroom' plan
$factory->afterCreatingState(User::class, 'subscribed_teacher', function ($user, $faker) {
    factory(Teacher::class)->states('with_classroom_subscription')->create([
        'user_id' => $user->id
    ]);
});

// Teacher
$factory->afterCreatingState(User::class, 'teacher', function ($user, $faker) {
    factory(Teacher::class)->create([
        'user_id' => $user->id
    ]);
});

// Student subscribed to Board Pass
$factory->afterCreatingState(User::class, 'student_with_board_pass', function ($user, Faker $faker) {
    factory(Student::class)->states('with_board_pass_subscription')->create([
        'user_id' => $user->id
    ]);
});

// Student
$factory->afterCreatingState(User::class, 'student', function ($user, Faker $faker) {
    factory(Student::class)->create([
        'user_id' => $user->id
    ]);
});

$factory->afterCreatingState(User::class, 'parent', function ($user, Faker $faker) {
    factory(StudentParent::class)->create([
        'user_id' => $user->id
    ]);
});
