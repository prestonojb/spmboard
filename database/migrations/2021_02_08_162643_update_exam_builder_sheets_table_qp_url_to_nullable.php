<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateExamBuilderSheetsTableQpUrlToNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('exam_builder_sheets', function (Blueprint $table) {
            $table->string('qp_url')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('exam_builder_sheets', function (Blueprint $table) {
            $table->string('qp_url')->change();
        });
    }
}
