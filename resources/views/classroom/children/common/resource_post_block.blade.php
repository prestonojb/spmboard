<div class="panel p-0 mb-3">
    <div class="panel-header">
        Resource Posts
    </div>
    <div class="panel-body">
        <div class="row">

            <div class="col-12 col-lg-6">
                <div class="pt-3">
                    <div class="text-center">
                        <p>Average understanding rating</p>
                        {{-- Reference: https://codepen.io/FredGenkin/pen/eaXYGV --}}
                        <div class="Stars" style="--rating: {{ $child->getAverageResourceRating( $class_selected ) }};"></div>
                    </div>

                    <p class="gray2 text-center">
                        {{ $child->getAverageResourceRating( $class_selected ) }} stars
                    </p>
                </div>
            </div>

            <div class="col-12 col-lg-6 h-100">
                @foreach( range(5, 1, -1) as $rating)
                    <div class="rating-progress-bar lg">
                        <div class="unit">
                            {{ $rating }}<span class="iconify" data-icon="emojione:star" data-inline="false"></span>
                        </div>
                        <div class="progress">
                            <div class="progress-bar" role="progressbar" style="width: {{ $child->getRatingCountPercentage( $class_selected, $rating ) }}%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                        <div class="metrics">
                            <div class="d-flex flex-column justify-content-center align-items-center">
                                <div>
                                    {{ $child->getRatingCountPercentage( $class_selected, $rating ) }}%
                                </div>
                                <div class="gray2">
                                    <small>
                                        <i>
                                            {{ $child->getRatingCount( $class_selected, $rating ) }}/{{ $child->getAbsoluteTotalRatings( $class_selected, $rating ) }}
                                        </i>
                                    </small>
                                </div>

                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
