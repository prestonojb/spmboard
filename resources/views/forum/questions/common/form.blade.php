@php
    if($is_edit) {
        $question_title = old('question') ?? $question->question;
        $description = old('description') ?? $question->description;
        $subject_id = old('subject') ?? $question->subject_id;
        if($errors->any()) {
            $chapter_ids = [old('chapter.0'), old('chapter.1')];
        } else {
            $chapter_ids = $question->chapters()->get()->pluck('id')->toArray();
        }
        $reward_points = old('reward_points') ?? $question->reward_points;
        $flagged_as = old('flagged_as') ?? $question->flagged_as;

        $url = route('questions.update', [$exam_board, $question->id]);
    } else {
        $question_title = old('question');
        $description = old('description');
        $subject_id = old('subject');
        $chapter_ids = [old('chapter.0'), old('chapter.1')];
        $reward_points = old('reward_points');
        $flagged_as = old('flagged_as');

        $url = route('questions.store', $exam_board->slug);
    }
@endphp

<form method="POST" action="{{ $url }}" class="form-container create-question-form createQuestionForm" enctype="multipart/form-data">
    @csrf
    @if($is_edit)
        @method('PATCH')
    @endif

    {{-- Question --}}
    <div class="form-field">
        <label>Question</label>
        <input name="question" placeholder="e.g What is the powerhouse of the cell?" value="{{ $question_title }}" required autocomplete="off">
        @error('question')
            <span class="error-message">{{ $message }}</span>
        @enderror
    </div>

    {{-- Description --}}
    <div class="form-group">
        <label for="">Description</label>
        <div class="loading-icon loading in-editor blue"></div>
        <textarea class="question-tinymce-editor" name="description">{!! $description !!}</textarea>
        @error('description')
            <div class="error-message">{{ $message }}</div>
        @enderror
    </div>

    {{-- Image --}}
    @if($is_edit)
        <div class="form-field">
            <label for="">Image</label>
            @if(!is_null($question->img))
                <img class="file-input-preview" src="{{ $question->img }}">
            @else
                <p class="py-1 gray2">No image</p>
            @endif
        </div>
    @else
        <div class="form-field">
            <label>Image <span style="font-weight: 400">(Optional)</span></label>
            <div class="file-drop-area">
                <span class="fake-btn">Choose files</span>
                <span class="file-msg">or drag and drop files here</span>
                <input class="file-input" name="img" type="file">
            </div>
            <img class="file-input-preview">
            @error('img')
                <span class="error-message">{{ $message }}</span>
            @enderror
        </div>
    @endif


    {{-- Subject --}}
    <div class="form-field">
        <label>Subject</label>
        <select class="subject-select" name="subject">
            <option data-placeholder="true"></option>
            @foreach($subjects as $subject)
                <option value="{{ $subject->id }}" {{ $subject_id == $subject->id ? 'selected="selected"' : '' }}>{{ $subject->name }}</option>
            @endforeach
        </select>
        @error('subject')
            <span class="error-message">{{ $message }}</span>
        @enderror
    </div>

    {{-- Chapter --}}
    <div class="form-field">
        <label>Chapters <span style="font-weight: 400">(maximum 2)</span></label>
        <select class="chapter-select" name="chapter[]" multiple disabled>
            <option></option>
            @foreach($subjects as $subject)
                <optgroup label="{{ $subject->name }}">
                    @foreach($subject->chapters as $chapter)
                        <option value="{{ $chapter->id }}"
                            {{ in_array($chapter->id, $chapter_ids) ? 'selected="selected"' : '' }}>
                            {{ $chapter->name }}
                        </option>
                    @endforeach
                </optgroup>
            @endforeach
        </select>
        @error('chapter')
            <span class="error-message">{{ $message }}</span>
        @enderror
    </div>

    {{-- RP Offer --}}
    <div class="form-field">
        <label>Reward Points
            @if($is_edit)
                <button type="button" class="transparent p-0" data-toggle="popover" data-placement="right" data-content="You can't change your reward point offering for your question once you have posted it."><i class="fa fa-info-circle" aria-hidden="true"></i></button>
            @else
                <button type="button" class="transparent p-0" data-toggle="popover" data-placement="right" data-content="Offer points so your questions get answered more quickly!"><i class="fa fa-info-circle" aria-hidden="true"></i></button>
            @endif
        </label>
        <select name="reward_points" class="coin-select">
            <option value="0" selected {{ $reward_points == 0 ? 'selected="selected"' : '' }}>0</option>
            @foreach( range(5, 30, 5) as $i )
                <option value="{{ $i }}" {{ $reward_points == $i ? 'selected="selected"' : '' }}>{{ $i }}</option>
            @endforeach
        </select>
        @error('reward_points')
            <span class="error-message">{{ $message }}</span>
        @enderror
    </div>

    <div class="form-field">
        <input type="checkbox" class="styled-checkbox--green" name="anonymous" id="anonymous" {{ old('anonymous') ? 'checked' : '' }}>
        <label for="anonymous">{{ __('Ask Anonymously') }}
            <button class="transparent p-0" type="button" data-container="body" data-toggle="popover" data-placement="right" data-content="When you ask question anonymously, the community will not know who asked the question. However, it is still a must to comply to Board's asking guidelines.">
                <i class="fa fa-info-circle" aria-hidden="true"></i>
            </button>
        </label>
    </div>

    {{-- Flagged As --}}
    @if( auth()->user()->isAdmin() )

        <hr>

        <div class="form-field">
            <label>Flagged As</label>
            <select name="flagged_as">
                <option value="" disabled selected>Flagged As</option>
                @php
                    $flags = [
                        'question of the day' => 'Question of the Day',
                        'add math challenge' => 'Add Math Challenge',
                        'stoichiometry mania' => 'Stoichiometry Mania',
                    ];
                @endphp
                @foreach($flags as $key => $value)
                    <option value="{{ $key }}" @if($key == $flagged_as) selected @endif>{{ $value }}</option>
                @endforeach
            </select>
        </div>
    @endif

    <div class="text-center">
        <button type="submit" class="w-250px button--primary loadOnSubmitButton">{{ $is_edit ? 'Update' : 'Ask' }}</button>
    </div>
</form>
