<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class TeacherInvoiceSetting extends Model
{
    protected $guarded = [];
    protected $primaryKey = 'teacher_id';

    public function teacher()
    {
        return $this->hasOne(Teacher::class);
    }

    public function getFullLogoUrlAttribute()
    {
        $value = $this->logo_url;
        if($value){
            if(strpos($value, 'amazonaws')){
                //Amazon link for image
                return $value;
            } else{
                //Amazon S3
                $updated_value = str_replace('\\', '/', $value);
                return Storage::cloud()->url($updated_value);
            }
        } else {
            return null;
        }
    }

    public function incrementSequence()
    {
        $this->update([
            'next_invoice_sequence' => $this->next_invoice_sequence + 1,
        ]);
    }

    public function getNextInvoiceNumber( $increment = false )
    {
        $sequence = $this->next_invoice_sequence;
        // Pad sequence with 0s to the left if less than 1000(i.e 1 to 0001)
        $sequence = str_pad($sequence, 4, "0", STR_PAD_LEFT);
        // $invoice_number = 'INV-'.'0001'
        if( $increment ) {
            $this->incrementSequence();
        }
        return $this->prefix.strval($sequence);
    }
}
