<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\HomeworkSubmission;
use Carbon\Carbon;
use App\User;
use Faker\Generator as Faker;

$factory->define(HomeworkSubmission::class, function (Faker $faker) {
    return [
        'homework_id' => function() {
            return factory(Homework::class)->create()->id;
        },
        'student_id' => function() {
            return factory(User::class)->states('student')->create()->id;
        },
        'status' => 'submitted',
        'comment' => $faker->optional->sentence,
        'marks' => 0, // Can be overriden
        'marked_file_url' => null,
        'teacher_remark' => null,
        'marked_at' => null,
        'returned_at' => null,
    ];
});

$factory->state(HomeworkSubmission::class, 'marked', function (Faker $faker){
    return [
        'marked_file_url' => 'https://www.w3.org/WAI/ER/tests/xhtml/testfiles/resources/pdf/dummy.pdf',
        'marked_at' => Carbon::now(),
        'returned_at' => Carbon::now(),
        'teacher_remark' => $faker->optional->sentence,
    ];
});
