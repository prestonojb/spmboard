<?php

namespace App\Http\Controllers;

use App\Student;
use Illuminate\Support\Facades\Session;
use App\Classroom;
use App\Rules\Classroom\ValidCommaSeperatedEmailString;
use Illuminate\Http\Request;

class ClassroomClassroomStudentController extends Controller
{
    public function index(Classroom $classroom)
    {
        $students = $classroom->students;
        return view('classroom.classrooms.students.index', compact('classroom', 'students'));
    }

    public function getAddView(Classroom $classroom)
    {
        $this->authorize('add_student', $classroom);
        $user = auth()->user();
        $teacher = $user->teacher;

        if(!$teacher->hasExtraStudentSlot($classroom)) {
            return redirect()->route('classroom.prices.index')->with('info', 'Upgrade your subscription to add more students into the classroom!');
        }
        return view('classroom.classrooms.students.add', compact('classroom'));
    }

    public function add(Classroom $classroom)
    {
        $this->authorize('add_student', $classroom);
        // Validation
        request()->validate([
            'email' => ['required', 'string', new ValidCommaSeperatedEmailString],
        ]);

        $user = auth()->user();
        $teacher = $user->teacher;

        $student_emails = commaSeperatedStringToArray(request('email'));
        $student_emails_count = count($student_emails);

        // Verify if teacher can add extra students into classroom
        if(!$teacher->hasExtraStudentSlots($classroom, $student_emails_count)) {
            return redirect()->back()->withInput()->with('error', 'Upgrade your subscription to add more students into the classroom!');
        }

        // Error email array
        $already_added_emails = [];
        $no_account_emails = [];

        // Loop through all student emails
        foreach($student_emails as $email) {
            if ($student = Student::retrieveWithEmail($email)) {
                // Student is already in classroom
                if( $classroom->hasStudent($student) ) {
                    array_push($already_added_emails, $email);
                }
            } else {
                array_push($no_account_emails, $email);
            }
        }

        // Validate student emails
        $error_html = $this->getAddStudentErrorHtml($already_added_emails, $no_account_emails);
        if( !empty($error_html) ) {
            return redirect()->back()->withInput()->withErrors(['email' => $error_html]);
        }

        // Add all students into classroom
        foreach($student_emails as $email) {
            $student = Student::retrieveWithEmail($email);
            $classroom->addStudent($student);
        }

        Session::flash('success', "Students added to classroom successfully!");
        return redirect()->route('classroom.classrooms.show', $classroom->id);
    }


    /**
     * Remove student from classroom
     */
    public function remove(Classroom $classroom)
    {
        $this->authorize('remove_student', $classroom);
        // Remove student to classroom
        $classroom->students()->detach( request()->student_user_id );

        return redirect()->back()->withSuccess('Student removed from classroom successfully!');
    }

    public function getJoinClassView()
    {
        $this->authorize('join', Classroom::class);
        return view('classroom.classrooms.students.join');
    }

    public function join()
    {
        $this->authorize('join', Classroom::class);

        request()->validate([
            'code' => 'required'
        ]);

        $user = auth()->user();
        $classroom = Classroom::where('code', request('code'))->first();

        // Show error if classroom code is invalid
        if( is_null($classroom) ) {
            return redirect()->back()->withInput( request()->all() )->withErrors(['code' => 'Invalid class code! Please confirm the class code with your teacher again.']);
        }

        // Add student to classroom
        if( !$classroom->hasStudent( $user->student ) ) {
            // Student is not in class
            $classroom->addStudent( $user->student );
            Session::flash('success', 'Joined class successfully!');
        } else {
            return redirect()->back()->withInput( request()->all() )->withErrors(['code' => 'You have already joined this class!']);
        }

        return redirect()->route('classroom.classrooms.show', $classroom->id);
    }

    /**
     * Returns add student error html
     * @param array $already_added_emails
     * @param array $no_account_emails
     * @return string $error_html
     */
    public function getAddStudentErrorHtml($already_added_emails, $no_account_emails)
    {
        $error_html = "";
        // Has invalid emails
        if( !empty($already_added_emails) || !empty($no_account_emails) ) {
            // Add already added emails to error message
            if(!empty($already_added_emails)) {
                $error_html .= "<p class='mb-0'>These students are already added into classroom:</p>";
                $error_html .= "<ul>";
                foreach($already_added_emails as $email) {
                    $error_html .= "<li><i>{$email}</i></li>";
                }
                $error_html .= "</ul>";
            }
            // Add no account emails to error message
            if(!empty($no_account_emails)) {
                $error_html .= "<p class='mb-0'>These emails do not have a student account on Board:</p>";
                $error_html .= "<ul>";
                foreach($no_account_emails as $email) {
                    $error_html .= "<li><i>{$email}</i></li>";
                }
                $error_html .= "</ul>";
            }
            $error_html .= "<p>To bulk add other valid emails, ensure that all the keyed in emails are valid.</p>";
        }
        return $error_html;
    }

}
