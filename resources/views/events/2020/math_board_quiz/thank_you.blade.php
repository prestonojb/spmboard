@extends('forum.layouts.app')
@section('meta')
    <title>Thank you - 2020 Math Board Quiz | {{ config('app.name') }}</title>
@endsection

@section('content')
@include('main.navs.header')
<div class="ask-wrapper">
    <h1 class="heading">What's Next?</h1>
    <h2 class="sub-heading">It's not the end yet :)</h2>

    <div class="quiz-thank-you__steps">
        <div class="quiz-thank-you__step">

            <div class="step-number">1</div>

            <div class="step-body">
                <div class="step-text">
                    <h2 class="heading">Like our Facebook Page to stay updated with the winners of the contest!
                        We will update the results of the contests on both our pages!</h2>
                </div>

                <div class="quiz__social-icons">
                    <a href="https://facebook.com/spmboardmy" target="_blank" class="button--primary fb-button mr-1">
                        <span class="iconify" data-icon="logos:facebook" data-inline="false"></span>
                        <span class="text">SPM <span class="d-none d-md-inline">Board</span></span>
                    </a>
                    <a href="https://facebook.com/igcseboardmy" target="_blank" class="button--primary fb-button">
                        <span class="iconify" data-icon="logos:facebook" data-inline="false"></span>
                        <span class="text">IGCSE <span class="d-none d-md-inline">Board</span></span>
                    </a>
                </div>
            </div>
        </div>

        <div class="quiz-thank-you__step">

            <div class="step-number">2</div>

            <div class="stepbody">
                <div class="step-text">
                    <h2 class="heading">Share this contest with your friends!</h2>
                </div>

                <div id="shareIcons"></div>
            </div>
        </div>

        <div class="quiz-thank-you__step">
            <div class="step-number">3</div>

            @if(!$is_existing_user)
                <div class="stepbody">
                    <div class="step-text">
                        <h2 class="heading">Start asking questions on Board!</h2>
                        <p class="sub-heading">*Ask questions, get answered, get rewarded.</p>
                    </div>

                    <a href="{{ route('register').'?utm_source=website&utm_campaign=board_quiz&utm_medium=thank_you' }}" class="button--primary lg mr-2">Join for Free</a>
                    <a class="underline-on-hover" href="{{ route('/').'?utm_source=website&utm_campaign=board_quiz&utm_medium=thank_you' }}">Learn More</a>
                </div>
            @else
                <div class="stepbody">
                    <div class="step-text">
                        <h2 class="heading">Log In and start collecting Reward Points(RP) now!</h2>
                        <p class="sub-heading">*You can spend Reward Points on getting help for your homework, redeem coins for resources and many more!</p>
                    </div>
                    <a href="{{ route('login').'?utm_source=website&utm_campaign=board_quiz&utm_medium=thank_you' }}" class="button--primary lg mr-2">Log In</a>
                    <a class="underline-on-hover" href="{{ url('faq').'?utm_source=website&utm_campaign=board_quiz&utm_medium=thank_you' }}">Learn More about RP</a>
                </div>
            @endif
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
$(function(){
    jsSocials.shares.twitter = {
        logo: "fa fa-twitter",
        shareUrl: "https://twitter.com/share?url=" + "{{ route('events.2020.get_math_board_quiz_view') }}" + "&text=Join me for this Board Quiz and stand a chance to win Netflix Gift Cards worth RM50 now!",
    };

    jsSocials.shares.facebook = {
        logo: "fa fa-facebook",
        shareUrl: "https://www.facebook.com/share.php?u="+ "{{ route('events.2020.get_math_board_quiz_view') }}" +"&title=Join me for this Board Quiz and stand a chance to win Netflix Gift Cards worth RM50 now!",
    };

    jsSocials.shares.whatsapp = {
        logo: "fa fa-whatsapp",
        shareUrl: "https://api.whatsapp.com/send?text=Join me for this Board Quiz and stand a chance to win Netflix Gift Cards worth RM50 now! To join Board Quiz, click this link: "+ "{{ route('events.2020.get_math_board_quiz_view') }}",
    };

    $("#shareIcons").jsSocials({
        showLabel: false,
        showCount: false,
        shares: ["twitter", "facebook", "whatsapp"],
        shareIn: "popup",
    });
});
</script>
<script>

</script>
@endsection
