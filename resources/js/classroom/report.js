var $form = $('#teacherCreateReportForm');
var $lessonTable = $form.find('.lessonTable');

function updateLessonTable(lessons) {
    $lessonTable.find('tbody').html('');

    if( lessons.length > 0 ) {
        lessons.forEach(function(lesson){
            $newLessonRow = $('#teacherCreateReportForm .lessonRowHelper').clone();
            $newLessonRow.toggleClass('lessonRowHelper lessonRow');
            $('.selectAllCheckbox').prop('checked', false);

            $newLessonRow.find('.lessonCheckbox').val( lesson.id );
            $newLessonRow.find('.lesson_name').text( lesson.name );
            $newLessonRow.find('.lesson_start_at').text( lesson.start_date );
            $newLessonRow.find('.lesson_resource_post_count').text( lesson.resource_post_count );
            $newLessonRow.find('.lesson_homework_count').text( lesson.homework_count );

            $lessonTable.find('tbody').append( $newLessonRow );
        });
    } else {
        $newLessonRow = $('.noLessonRow').clone();
        $lessonTable.find('tbody').append( $newLessonRow );
    }
}

var $startDateDtPicker = $form.find('#startDateDtPicker');
var $endDateDtPicker = $form.find('#endDateDtPicker');

$startDateDtPicker.datetimepicker({
    defaultDate: false,
    maxDate: new Date(),
    format: 'DD/MM/YYYY',
    useCurrent: false
});

$endDateDtPicker.datetimepicker({
    defaultDate: false,
    maxDate: new Date(),
    format: 'DD/MM/YYYY',
    useCurrent: false
});

$startDateDtPicker.on("change.datetimepicker", function (e) {
    $endDateDtPicker.datetimepicker('minDate', e.date);
});

$endDateDtPicker.on("change.datetimepicker", function (e) {
    $startDateDtPicker.datetimepicker('maxDate', e.date);
});

var parentInput = $('[name=parent]');
var parentDisplayInput = $('[name=parent_display]');
$('#teacherCreateReportForm select[name=student]').change(function(e){
    e.preventDefault();
    var student_id = $(this).val();
    $.ajax({
        type: "post",
        url: '/s/parent',
        data: { student: student_id },
        success: function (response) {
            if( response ) {
                $('[name=parent_email]').val( response.email );
            } else {
                $('[name=parent_email]').val( 'No Parent found' );
            }
        }
    });

    // Fill lesson table
    $.ajax({
        type: 'post',
        url: '/classroom/t/s/unreported_lessons',
        data: { student: student_id },
        success: function (response) {
            updateLessonTable(response);
        }
    });
});

$lessonTable.on('change', '.selectAllCheckbox', function(e){
    var table = $(this).closest('table');
    table.find('.lessonCheckbox').prop('checked',  $(this).is(':checked'));
});

$lessonTable.on('change', '.lessonCheckbox', function(e){
    var table = $(this).closest('table');
    // Boolean for ALL lesson checkboxes are checked
    var allChecked = table.find('.lessonCheckbox:checked').length == table.find('.lessonCheckbox').length;
    table.find('.selectAllCheckbox').prop('checked', allChecked);
});


$('#teacherCreateReportForm [type=submit]').click(function(e){
    e.preventDefault();
    var reportItemData = [];
        // Lesson Invoice Item
    $('.lessonTable .lessonCheckbox:checked').each(function(){
        var reportItem = {};
        // Add Lesson Invoice Item to invoiceItem array
        reportItem.lesson_id = parseInt( $(this).val() );
        reportItemData.push( reportItem );
    });
    // Submit teacher invoice items form field only if prescription Data is filled
    if( Array.isArray(reportItemData) && reportItemData.length ) {
        $('[name=report_items]').val( JSON.stringify( reportItemData ) );
    }
    $(this).closest('form').submit();
});
