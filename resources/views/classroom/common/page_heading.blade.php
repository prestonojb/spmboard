<div class="d-sm-flex align-items-center justify-content-between mt-3">
    <h1 class="font-weight700 mb-1">
        {{ $title }}
        @if(isset($subtitle) && !is_null($subtitle))
            <span class="gray2 font-size4 font-weight400">{{ $subtitle }}</span>
        @endif
    </h1>

    @if(isset($button_group))
        <div class="page-heading__button-group">
            {{ $button_group }}
        </div>
    @endif
</div>

<hr class="border-secondary mt-2">

{{ $slot }}

<div class="py-1"></div>
