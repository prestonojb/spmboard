@extends('forum.layouts.app')

@section('meta')
<title>Write Article | {{ config('app.name') }}</title>
@endsection

@section('styles')
{{-- Selectize --}}
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/css/selectize.min.css" integrity="sha512-87wkTHUArAnTBwQ5XL6+G68i54R3TXYDZoXewRsdhIv/ztcEr2Z1Mrk+aXBCKOZUtih0XWiBhXv3/bWjHTL2Bw==" crossorigin="anonymous" />
@endsection

@section('content')
@include('main.navs.header')

<div class="py-3"></div>

<div class="container">
    <div class="row">
        <div class="col-12 col-lg-9 mb-2">
            <div class="maxw-740px mx-auto">
                <a class="d-block mb-2 inactive font-size2 gray2" href="{{ route('/') }}">< Back to Home</a>

                <h1 class="font-size7">{{ $is_edit ? 'Edit Article' : 'Write Article' }}</h1>

                <div class="panel">
                    @component('forum.users.profile.articles.common.form', ['is_edit' => $is_edit, 'article' => $article])
                    @endcomponent
                </div>
            </div>
        </div>
        <div class="col-12 col-lg-3 mb-2">
        </div>
    </div>
</div>

<div class="py-3"></div>
@endsection

@section('js_dependencies')
{{-- TinyMCE --}}
<script src="https://cdn.tiny.cloud/1/si4jvonmpvtlfypotvf9zu2vci2zsjavfd4v1oyvnzncszag/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>    {{-- KaTeX --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ" crossorigin="anonymous"></script>
@endsection

@section('scripts')
{{-- Sweet Alert 2 --}}
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
{{-- Selectize.js --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/js/standalone/selectize.min.js" integrity="sha512-hgoywpb1bcTi1B5kKwogCTG4cvTzDmFCJZWjit4ZimDIgXu7Dwsreq8GOQjKVUxFwxCWkLcJN5EN0W0aOngs4g==" crossorigin="anonymous"></script>
<script>
$(function(){
    // Tags Input
    var options = [
        {value: 'Opinion'},
        {value: 'Teaching Techniques'},
        {value: 'Exam Tips'},
    ];

    var items = options.map(x => x.name);
    $('.articleTagsInput').selectize({
        plugins: ['remove_button'],
        delimiter: ',',
        persist: false,
        maxItems: 3,
        options: options,
        items: items,
        labelField: 'value',
        valueField: 'value',
        create: function(input) {
            return {
                value: input,
                text: input
            }
        }
    });
});
</script>
@endsection

