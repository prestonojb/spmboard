<?php

namespace App\Http\Controllers\Voyager;
use App\Imports\StudentAndParentImport;
use Excel;
use TCG\Voyager\Http\Controllers\VoyagerUserController as BaseVoyagerUserController;

class VoyagerUserController extends BaseVoyagerUserController
{
    /**
     * Get Student and Parent Import view
     */
    public function getStudentAndParentImportView()
    {
        return view('vendor.voyager.users.import.student_and_parent');
    }

    /**
     * Import student and parent accounts based on uploaded file
     */
    public function studentAndParentImport()
    {
        // Validation
        request()->validate([
            'file' => 'required|mimes:csv,xlsx,xls'
        ]);
        Excel::import(new StudentAndParentImport, request()->file('file'));

        return redirect()->back()->with('success', 'All good!');
    }
}
