<div class="panel user-credentials">
    <h3 class="block-title">Highlights</h3>
    {{-- State --}}
    <div class="user-credentials__item">
        <span class="iconify" data-icon="bx:bx-map" data-inline="false"></span>
        @if(!is_null($user->state))
            <div>From {{ $user->state->name }}, Malaysia</div>
        @else
            <div class="user-credentials__empty">Not updated yet</div>
        @endif
    </div>

    {{-- School --}}
    <div class="user-credentials__item">
        <span class="iconify" data-icon="bx:bxs-school" data-inline="false"></span>
        @if(!is_null($user->school))
            <div class="value">{{ $user->school }}</div>
        @else
            <div class="user-credentials__empty">Not updated yet</div>
        @endif
    </div>
</div>
