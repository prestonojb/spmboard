<div class="container landing__forum-content py-5 mb-lg-5">
    <div class="row">
        @foreach($data as $datum)
            <div class="col-12 col-lg-4">
                <div class="landing__card">
                    <div class="image">
                        <img src="{{ $datum['img'] }}" alt="{{ $datum['heading'] }}">
                    </div>
                    <div class="caption">
                        <h5 class="heading">{{ $datum['heading'] }}</h5>
                        <p>{{ $datum['subheading'] }}</p>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>
