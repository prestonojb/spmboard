<?php

namespace App\Notifications\Classroom;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use App\Mail\InvoiceSent as Mailable;
use App\TeacherInvoice;

class InvoiceIssued extends Notification implements ShouldQueue
{
    use Queueable;

    public $invoice;
    public $pdf;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(TeacherInvoice $invoice, $pdf)
    {
        $this->invoice = $invoice;
        $this->pdf = $pdf;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    // /**
    //  * Get the mail representation of the notification.
    //  *
    //  * @param  mixed  $notifiable
    //  * @return \Illuminate\Notifications\Messages\MailMessage
    //  */
    // public function toMail($notifiable)
    // {
    //     return (new Mailable($this->invoice, $this->pdf))->to($notifiable->email);
    // }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $invoice = $this->invoice;
        $teacher = $invoice->teacher;
        $student = $invoice->student;

        $title = "<b>{$teacher->name}</b> issued an invoice for <b>{$student->name}</b>.";
        return [
            'type' => TeacherInvoice::class,
            'target' => $invoice,
            'title' => $title,
        ];
    }
}
