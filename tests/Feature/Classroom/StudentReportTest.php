<?php

namespace Tests\Feature\Classroom;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Classroom;
use App\Lesson;
use App\Subject;
use App\User;
use App\Notifications\Classroom\ReportPrepared;
use App\StudentReport;
use Illuminate\Support\Facades\Notification;
use Carbon\Carbon;

class StudentReportTest extends TestCase
{
    use WithFaker;

    private $teacher;
    private $student;
    private $parent;
    private $subject;

    public function setUp() : void
    {
        parent::setUp();

        // Subscribed Teacher
        $user = factory(User::class)->states('subscribed_teacher')->create();
        $this->subscribed_teacher = $user->teacher;

        // Unsubscribed Teacher
        $user = factory(User::class)->states('teacher')->create();
        $this->teacher = $user->teacher;

        // Student with Board Pass
        $user = factory(User::class)->states('student')->create();
        $this->student = $user->student;

        // Parent
        $user = factory(User::class)->states('parent')->create();
        $this->parent = $user->parent;

        // Subject
        $this->subject = Subject::first();
        // Classroom
        $this->classroom = factory(Classroom::class)->create([
                                'teacher_id' => $this->teacher->user_id,
                                'subject_id' => $this->subject->id,
                            ]);
        // Lesson
        $this->lesson = factory(Lesson::class)->create([
            'classroom_id' => $this->classroom->id
        ]);

        // Student joins classroom
        $this->classroom->addStudent( $this->student );
        // Parent attached to student
        $this->student->updateParent($this->parent);
    }

    /**
     * @group report
     * Test for teacher preparing report
     */
    public function testStore()
    {
        Notification::fake();

        $teacher_remark = $this->faker->sentence;
        $start_date = Carbon::now()->subDays( rand(0, 14) )->format('d/m/Y');
        $end_date = Carbon::now()->addDays( rand(0, 14) )->format('d/m/Y');
        $report_items = '[{"lesson_id": '. strval($this->lesson->id) .'}]';

        // POST request to store report
        $response = $this->actingAs($this->teacher->user)
                         ->post( route('classroom.reports.store'), [
                            'student' => $this->student->user_id,
                            'teacher_id' => $this->teacher->user_id,
                            'teacher_remark' => $teacher_remark,
                            'start_date' => $start_date,
                            'end_date' => $end_date,
                            'report_items' => $report_items,
                          ]);

        // Get newly stored report
        $report = StudentReport::latest()->first();

        // Assert model store
        $this->assertEquals( $report->student_id, $this->student->user_id, "Report 'student_id' is not stored correctly!" );
        $this->assertEquals( $report->teacher_id, $this->teacher->user_id, "Report 'teacher_id' is not stored correctly!" );
        $this->assertEquals( $report->teacher_remark, $teacher_remark, "Report 'teacher_remark' is not stored correctly!" );
        $this->assertEquals( $report->start_date->format('d/m/Y'), $start_date, "Report 'start_date' is not stored correctly!" );
        $this->assertEquals( $report->end_date->format('d/m/Y'), $end_date, "Report 'end_date' is not stored correctly!" );

        // Assert lesson attached to report
        $this->assertTrue( $report->items->contains( 'lesson_id', $this->lesson->id ) );

        // Notification received by parent
        Notification::assertSentTo(
            [$this->student->parent], ReportPrepared::class
        );

        // Redirect
        $response->assertRedirect();
    }

    /**
     * @group report
     * Test update report
     */
    // public function testUpdate()
    // {
    //     // Report
    //     $this->report = factory(StudentReport::class)->create([
    //         'teacher_id' => $this->teacher->user_id,
    //         'student_id' => $this->student->user_id,
    //     ]);
    // }
}
