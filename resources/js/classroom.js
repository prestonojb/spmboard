$(function() {
    "use strict"; // Start of use strict

    // Toggle the side navigation
    $("#sidebarToggle, #sidebarToggleTop").on('click', function(e) {
      $("body").toggleClass("sidebar-toggled");
      $(".sidebar").toggleClass("toggled");
      if ($(".sidebar").hasClass("toggled")) {
        $('.sidebar .collapse').collapse('hide');
      };
    });

    // Scroll to top button appear
    $(document).on('scroll', function() {
      var scrollDistance = $(this).scrollTop();
      if (scrollDistance > 100) {
        $('.scroll-to-top').fadeIn();
      } else {
        $('.scroll-to-top').fadeOut();
      }
    });

    // Smooth scrolling using jQuery easing
    $(document).on('click', 'a.scroll-to-top', function(e) {
      var $anchor = $(this);
      $('html, body').stop().animate({
        scrollTop: ($($anchor.attr('href')).offset().top)
      }, 1000, 'easeInOutExpo');
      e.preventDefault();
    });

    $('#classroomSelect').change(function(e){
        var url = $(this).find('option:selected').data('url');
        window.location.href = url;
    });
    // Mark invoice as paid button
    $('.markAsPaidButton').click(function(e){
        e.preventDefault();
        e.stopPropagation();
        var form = $(this).closest('form');
        Swal.fire({
            title: 'Are you sure to mark this invoice as paid?',
            text: "You won't be able to revert this!",
            type: 'info',
            showCancelButton: true,
            reverseButtons: true,
            confirmButtonText: 'Confirm',
            cancelButtonText: 'Cancel',
        }).then(function(result){
            if(result.value){
                form.submit();
            }
        });
    });

    $('.downloadFileLink').click(function(e){
        e.preventDefault();
        var link = $(this).attr('href');
        link.download = 'file.pdf';
    });

    $('.notificationBellButton').click(function(e){
        $.ajax({
            method: 'GET',
            url: '/classroom/markAsRead',
        });
    });

    require('./classroom/add_resource');
    require('./classroom/selectize');
    require('./classroom/report');
    require('./classroom/invoice');
    require('./classroom/lesson');
    require('./classroom/chart');
    require('./classroom/quill');
    require('./classroom/tinymce');
    require('./classroom/datatables');

})(jQuery); // End of use strict


