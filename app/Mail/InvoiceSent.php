<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class InvoiceSent extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($invoice, $pdf)
    {
        $this->invoice = $invoice;
        $this->pdf = $pdf;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $invoice = $this->invoice;
        return $this->markdown('classroom.email.teacher.invoice_sent')
                     ->from( $invoice->teacher->email )
                     ->with([
                        'invoice' => $invoice,
                     ])
                     ->attachData($this->pdf->output(), 'Invoice #'.$invoice->id.'.pdf', [
                        'mime' => 'application/pdf',
                    ]);
    }
}
