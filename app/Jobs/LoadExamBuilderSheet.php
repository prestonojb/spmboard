<?php

namespace App\Jobs;

use App\ExamBuilderSheet;
use App\Notifications\ExamBuilderSheetGenerated;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class LoadExamBuilderSheet implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $exam_builder_sheet_id;
    public $is_qp;
    public $show_label;
    public $hide_watermark;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($exam_builder_sheet_id, $is_qp, $show_label, $hide_watermark)
    {
        $this->exam_builder_sheet_id = $exam_builder_sheet_id;
        $this->is_qp = $is_qp;
        $this->show_label = $show_label;
        $this->hide_watermark = $hide_watermark;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $sheet = ExamBuilderSheet::find($this->exam_builder_sheet_id);
        // Generate and store PDF
        $sheet->generateAndStorePdf($this->is_qp, $this->show_label, $this->hide_watermark);
        if($this->is_qp) {
            $sheet->update(['qp_status' => 'available']);
        } else {
            $sheet->update(['ms_status' => 'available']);
        }
        // Notification
        $sheet->user->notify(new ExamBuilderSheetGenerated($sheet, $this->is_qp));
    }
}
