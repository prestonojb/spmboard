@extends('forum.users.profile.layouts.app')

@section('meta')
    <title>Articles - {{ $user->username }} | {{ config('app.name') }}</title>
    <meta property="og:image" content="{{ $user->avatar }}" />
    <meta property="og:type" content="website" />
@endsection

@section('styles')
@endsection

@section('main')
    {{-- Metrics --}}
    {{-- @php
        $data = [
            ['key' => 'Upvotes',
            'value' => $user->getQuestionUpvoteCount(),
            'icon' => 'fas fa-arrow-up fa-2x',
            'color' => 'blue6'],
            ['key' => 'Recommended',
            'value' => $user->getQuestionRecommendationsCount(),
            'icon' => 'fas fa-star fa-2x',
            'color' => 'yellow']
        ];
    @endphp
    <div class="form-row border-bottom pb-3 mb-3">
        @foreach($data as $datum)
            <div class="col-12 col-lg-4 mb-2">
                @component('forum.users.profile.common.key_value_block', ['datum' => $datum])
                @endcomponent
            </div>
        @endforeach
    </div> --}}

    @if(count($articles ?? []))

        <p class="font-size2">Showing {{ $articles->firstItem() }}-{{ $articles->lastItem() }}  of {{ $articles->total() }} articles</p>
        <div class="row">
            @foreach($articles as $article)
                <div class="col-12 col-lg-6 mb-3">
                    @component('forum.users.profile.articles.common.block', ['article' => $article])
                    @endcomponent
                </div>
            @endforeach
        </div>
    @else
        <section class="text-center mt-4">
            <img src="{{ asset('img/essential/reading.svg') }}" alt="No Questions Found" width="220" height="220" class="mb-2">
            <p class="gray2 font-size4">No articles yet...</p>
        </section>
    @endif
    {{ $articles->links() }}
@endsection
