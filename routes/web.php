<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Mail\ClaimedOnlineClass;
use App\Mail\ErrorFeedback;
use Carbon\Carbon;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Spatie\CalendarLinks\Link;

if(config('app.env') != 'production') {
    Route::get('/debug-sentry', function () {
        throw new Exception('My first Sentry error!');
    });
}
// Landing page
Route::view('/', 'landing')->name('/');
Route::view('/privacy-policy', 'privacy_policy')->name('privacy_policy');
Route::view('/terms-of-service', 'terms_of_service')->name('terms_of_service');
Route::view('/faq', 'faq')->name('faq');
Route::view('/about', 'about')->name('about');

// General routes
Auth::routes(['verify' => true]);

Route::post('continue_login', 'Auth\LoginController@continue_login')->name('continue_login');
Route::post('continue_register', 'Auth\RegisterController@continue_register')->name('continue_register');

Route::middleware('student_auth')->group(function(){
    // Getting started
    Route::get('s/getting-started', 'StudentController@getGettingStartedView');
    Route::patch('s/update', 'StudentController@update')->name('student.update');
});

// Notifications
// Route::get('/notifications', 'NotificationController@index')->name('notifications.index');

// Google Login
Route::get('/redirect', 'Auth\LoginController@redirectToProvider')->name('google.redirect');
Route::get('/callback', 'Auth\LoginController@handleProviderCallback')->name('google.callback');

Route::view('login/choose_redirect', 'login_choose_redirect')->name('login_choose_redirect')->middleware('auth');

Route::middleware('auth')->group(function(){
    // Certification
    Route::name('certification_applications')->group(function(){
        Route::get('/certification', 'CertificationApplicationController@index')->name('.index');
        Route::get('/certification/apply', 'CertificationApplicationController@create')->name('.create');
        Route::post('/certification/apply', 'CertificationApplicationController@store')->name('.store');
    });

    // Referral
    Route::prefix('/referral')->name('referral')->group(function(){
        Route::get('/', 'ReferralController@getView')->name('.get_view');
        Route::post('/generate_link', 'ReferralController@generateReferralLink')->name('.generate_referral_link');
    });

    // Recommendations
    Route::prefix('/recommendations')->name('recommendations')->group(function(){
        Route::post('/{recommendee}/store', 'RecommendationController@store')->name('.store');
        Route::patch('/{recommendation}/update', 'RecommendationController@update')->name('.update');
    });

    // Library
    Route::prefix('/libraries')->name('libraries')->group(function(){
        Route::get('/', 'LibraryController@index')->name('.index');
        Route::get('/create', 'LibraryController@create')->name('.create');
        Route::post('/store', 'LibraryController@store')->name('.store');

        Route::get('/unlocked_resources', 'LibraryController@showUnlockedResources')->name('.show_unlocked_resources');
        Route::prefix('/{library}')->group(function(){
            Route::get('/show', 'LibraryController@show')->name('.show');
            Route::post('/{resource}/toggle', 'LibraryController@toggleResource')->name('.toggle_resource');
            Route::post('/{resource}/get_list_item_html', 'LibraryController@getListItemHtml')->name('.get_list_item_html');
            Route::delete('/delete', 'LibraryController@delete')->name('.delete');
        });

    });
});

Route::group(['prefix' => 'admin'], function () {

    Route::middleware('admin_auth')->name('voyager')->group(function(){
        // Exam Builder Items
        Route::prefix('exam-builder-items')->name('.exam_builder_items')->group(function(){
            Route::get('/export', 'Voyager\VoyagerExamBuilderItemController@getExportView')->name('.get_export_view');
            Route::post('/export', 'Voyager\VoyagerExamBuilderItemController@export')->name('.export');
            Route::get('/generate', 'Voyager\VoyagerExamBuilderItemController@generate')->name('.get_generate_view');
            Route::post('/result', 'Voyager\VoyagerExamBuilderItemController@result')->name('.result');
        });
        // Chapters
        Route::prefix('chapters')->name('.chapters')->group(function(){
            Route::get('/bulk-create', 'Voyager\VoyagerChapterController@bulk_create')->name('.bulk_create');
            Route::post('/bulk-create', 'Voyager\VoyagerChapterController@bulk_store')->name('.bulk_store');
            Route::post('/reorder', 'Voyager\VoyagerChapterController@reorder')->name('.reorder');
        });
        // Certification
        // Approve/Decline
        Route::name('.certification_applications')->group(function(){
            Route::post('/certification/{application}/approve', 'Voyager\VoyagerCertificationApplicationController@approve')->name('.approve');
            Route::post('/certification/{application}/decline', 'Voyager\VoyagerCertificationApplicationController@decline')->name('.decline');
        });

        // Withdrawal Request
        // Complete/Cancel
        Route::prefix('/withdrawal_requests')->name('.withdrawal_requests')->group(function(){
            Route::post('/{withdrawal_request}/complete', 'Voyager\VoyagerWithdrawalRequestController@complete')->name('.complete');
            Route::post('/{withdrawal_request}/cancel', 'Voyager\VoyagerWithdrawalRequestController@cancel')->name('.cancel');
        });
    });

    Voyager::routes();

    Route::middleware('admin_auth')->group(function(){
        Route::get('/', 'AdminController@dashboard')->name('voyager.dashboard');

        Route::prefix('users/import')->name('voyager.users.import')->group(function(){
            Route::get('student_and_parent', 'Voyager\VoyagerUserController@getStudentAndParentImportView')->name('.get_student_and_parent_view');
            Route::post('student_and_parent', 'Voyager\VoyagerUserController@studentAndParentImport')->name('.student_and_parent');
            // Route::post('teacher', 'Voyager\VoyagerUserController@teacherImport')->name('.teacher');
        });

        // Guides
        Route::prefix('guides')->name('voyager.guides')->group(function(){
            Route::get('create', 'Voyager\VoyagerGuideController@create')->name('.create');
            Route::post('store', 'Voyager\VoyagerGuideController@store')->name('.store');
            Route::get('{id}/edit', 'Voyager\VoyagerGuideController@edit')->name('.edit');
            Route::patch('{id}/update', 'Voyager\VoyagerGuideController@update')->name('.update');
        });

        // Generate Topical Exam Builder Sheets
        Route::prefix('/exam-builder-sheets')->name('voyager.exam_builder_sheets')->group(function(){
            Route::get('topical/generate', 'Voyager\VoyagerExamBuilderSheetController@getGenerateTopicalView')->name('.topical.get_generate_view');
            Route::post('topical/generate', 'Voyager\VoyagerExamBuilderSheetController@generateTopical')->name('.topical.generate');
        });

        // Resource
        Route::get('/resources/update_all_preview_images', 'ResourceController@updateAllPreviewImages')->name('voyager.resources.update_all_preview_images');
    });
});


// Editor File Upload
Route::post('/editor/upload', function() {
    $file = request()->file('file');
    $path = $file->store('/editor', 's3');
    return response()->json(['location' => Storage::url($path)]);
});

// Blog
Route::resource('/blog', 'BlogController')->only([
    'index', 'show'
]);
// Blog categories
Route::get('/blog/categories/{blog_category}', 'BlogCategoryController@show')->name('blog_categories.show');
// Blog authors
Route::get('/blog/author/{blog_author}', 'BlogAuthorController@show')->name('blog_authors.show');
// Contact Us
Route::get('/contact-us', 'ContactUsController@createEmail')->name('contactUs.createEmail');
Route::post('/contact-us', 'ContactUsController@sendEmail')->name('contactUs.sendEmail');

// Forum routes
Route::get('forum', 'ForumController@getLandingView')->name('forum.landing');

Route::middleware(['auth', 'exam_builder'])->prefix('exam-builder')->name('exam_builder')->group(function(){
    // Index
    Route::get('/', 'ExamBuilderController@landing')->name('.landing');
    // Create
    Route::get('/create', 'ExamBuilderController@getFilterView')->name('.get_filter_view');
    Route::post('/create', 'ExamBuilderController@filter')->name('.filter');
    Route::post('/customize', 'ExamBuilderController@customize')->name('.customize');
    // Sheets
    Route::prefix('/sheets')->name('.sheets')->group(function(){
        // Store
        Route::post('/store', 'ExamBuilderSheetController@store')->name('.store');
        Route::post('/{exam_builder_sheet}/generate-ms', 'ExamBuilderSheetController@generateAndStoreMsPdf')->name('.generate_ms');
        Route::get('/{exam_builder_sheet}/qp', 'ExamBuilderSheetController@show')->name('.show');
        Route::get('/{exam_builder_sheet}/ms', 'ExamBuilderSheetController@showMs')->name('.show_ms');
        // Download
        Route::get('/download/qp/{exam_builder_sheet}', 'ExamBuilderSheetController@downloadQp')->name('.download_qp');
        Route::get('/download/ms/{exam_builder_sheet}', 'ExamBuilderSheetController@downloadMs')->name('.download_ms');
        // Delete
        Route::delete('/{exam_builder_sheet}/delete', 'ExamBuilderSheetController@delete')->name('.delete');
    });

    Route::post('/items/{exam_builder_item}/replace', 'ExamBuilderController@replaceItem')->name('.replace_item');
    Route::post('/items/{exam_builder_item}/html', 'ExamBuilderController@getBlockHtml')->name('.get_block_html');

    // AJAX routes
    Route::get('/subjects/{subject}/paper_numbers', 'ExamBuilderController@getPaperNumbersBySubject');
});

Route::post('/s/parent', 'StudentController@getParent');

// Classroom routes
Route::middleware(['classroom'])->prefix('classroom')->name('classroom')->group(function(){
    Route::get('/', 'ClassroomController@getLandingView')->name('.landing');

    Route::middleware('auth')->group(function(){
        Route::get('/markAsRead', function(){
            if( auth()->user()->teacher ) {
                auth()->user()->teacher->unreadNotifications->markAsRead();
            } elseif( auth()->user()->student ) {
                auth()->user()->student->unreadNotifications->markAsRead();
            } elseif( auth()->user()->parent ) {
                auth()->user()->parent->unreadNotifications->markAsRead();
            }
        })->name('.markAsRead');

        // Calendar
        Route::get('calendar', 'ClassroomController@calendar')->name('.calendar');

        // AJAX
        Route::post('/t/s/unreported_lessons', 'ClassroomTeacherController@getUnreportedLessons');
        Route::post('/t/s/unpaid_lessons', 'ClassroomTeacherController@getUnpaidLessons');

        // Classrooms
        Route::prefix('classrooms')->name('.classrooms')->group(function(){
            Route::get('/', 'ClassroomController@index')->name('.index');
            Route::get('/create', 'ClassroomController@create')->name('.create');
            Route::post('/store', 'ClassroomController@store')->name('.store');
            // Edit classroom
            Route::get('/{classroom}/edit', 'ClassroomController@edit')->name('.edit');
            Route::patch('/{classroom}/update', 'ClassroomController@update')->name('.update');
            // Delete classroom
            // Route::delete('/{classroom}/delete', 'ClassroomController@delete')->name('.delete');
            Route::get('/{classroom}/dashboard', 'ClassroomController@show')->name('.show');
            // Add student to classroom
            Route::get('/{classroom}/students', 'ClassroomClassroomStudentController@index')->name('.students.index');
            Route::get('/{classroom}/add', 'ClassroomClassroomStudentController@getAddView')->name('.students.get_add_view');
            Route::post('/{classroom}/add', 'ClassroomClassroomStudentController@add')->name('.students.add');
            // Kick student from classroom
            Route::post('/{classroom}/remove', 'ClassroomClassroomStudentController@remove')->name('.students.remove');
            // Join Classroom
            Route::get('/join', 'ClassroomClassroomStudentController@getJoinClassView')->name('.students.getJoinClassView');
            Route::post('/join', 'ClassroomClassroomStudentController@join')->name('.students.join');
        });

        // Classroom-specific routes
        Route::prefix('{classroom}')->group(function(){
            // Progress in Class
            Route::get('/progress', 'StudentController@show_progress_in_class')->name('.progress');

            // Lessons
            Route::prefix('/lessons')->name('.lessons')->group(function(){
                Route::get('/', 'ClassroomLessonController@index')->name('.index');
                Route::get('/create', 'ClassroomLessonController@create')->name('.create');
                Route::post('/store', 'ClassroomLessonController@store')->name('.store');
                Route::get('/{lesson}', 'ClassroomLessonController@show')->name('.show');

                Route::get('/{lesson}/edit', 'ClassroomLessonController@edit')->name('.edit');
                Route::patch('/{lesson}', 'ClassroomLessonController@update')->name('.update');
                Route::delete('/{lesson}', 'ClassroomLessonController@destroy')->name('.destroy');
            });

            Route::name('.posts')->group(function(){
                Route::get('/discussion', 'ClassroomClassroomPostController@index')->name('.index');
                Route::prefix('/posts')->group(function(){
                    Route::get('/create', 'ClassroomClassroomPostController@create')->name('.create');
                    Route::post('/store', 'ClassroomClassroomPostController@store')->name('.store');
                    // Route::get('/{post}', 'ClassroomClassroomPostController@show')->name('.show');

                    Route::get('/{post}/edit', 'ClassroomClassroomPostController@edit')->name('.edit');
                    Route::patch('/{post}', 'ClassroomClassroomPostController@update')->name('.update');
                    Route::delete('/{post}', 'ClassroomClassroomPostController@destroy')->name('.delete');
                });
            });
        });

        // Students
        Route::prefix('/students')->name('.students')->group(function(){
            Route::get('', 'ClassroomStudentController@index')->name('.index');
            Route::get('/{student}', 'ClassroomStudentController@show')->name('.show');
        });

        // Children
        Route::prefix('/children')->name('.children')->group(function(){
            Route::get('/', 'ClassroomChildrenController@index')->name('.index');
            Route::get('/{student}', 'ClassroomStudentController@show')->name('.show');
        });

        // Reports
        Route::prefix('reports')->name('.reports')->group(function(){
            Route::get('/', 'ClassroomStudentReportController@index')->name('.index');
            Route::get('/create', 'ClassroomStudentReportController@create')->name('.create');
            Route::post('/store', 'ClassroomStudentReportController@store')->name('.store');
            Route::get('/{report}/show', 'ClassroomStudentReportController@show')->name('.show');
            Route::post('/{report}/download_pdf', 'ClassroomStudentReportController@download_pdf')->name('.download_pdf');
            Route::delete('/{report}/delete', 'ClassroomStudentReportController@delete')->name('.delete');
        });

        // // Invoices
        // Route::prefix('invoices')->name('.invoices')->group(function(){
        //     Route::get('/', 'ClassroomInvoiceController@index')->name('.index');
        //     Route::get('/create', 'ClassroomInvoiceController@create')->name('.create');
        //     Route::post('/store', 'ClassroomInvoiceController@store')->name('.store');
        //     Route::get('/{teacher_invoice}/show', 'ClassroomInvoiceController@show')->name('.show');
        //     Route::post('/{teacher_invoice}/mark-as-paid', 'ClassroomInvoiceController@mark_as_paid')->name('.mark_as_paid');
        //     Route::delete('/{teacher_invoice}/delete', 'ClassroomInvoiceController@delete')->name('.delete');
        //     Route::post('/{teacher_invoice}/download_pdf', 'ClassroomInvoiceController@download_pdf')->name('.download_pdf');
        // });


        Route::prefix('lessons/{lesson}')->name('.lessons')->group(function(){
            // Lesson resource post
            Route::prefix('/resources')->name('.resource_posts')->group(function(){
                Route::get('/', 'ClassroomResourcePostController@index');
                Route::get('/create', 'ClassroomResourcePostController@create')->name('.create');
                Route::post('/store', 'ClassroomResourcePostController@store')->name('.store');

                Route::prefix('{resource_post}')->group(function(){
                    Route::get('/edit', 'ClassroomResourcePostController@edit')->name('.edit');
                    Route::patch('/update', 'ClassroomResourcePostController@update')->name('.update');
                    Route::get('/show', 'ClassroomResourcePostController@show')->name('.show');
                    Route::delete('/delete', 'ClassroomResourcePostController@delete')->name('.delete');
                    // Rate Resource Post
                    Route::post('/rate', 'ClassroomResourcePostController@rate')->name('.rate');
                });
            });

            // Lesson homework
            Route::prefix('/homework')->name('.homeworks')->group(function(){
                Route::get('/', 'ClassroomHomeworkController@index');
                Route::get('/create', 'ClassroomHomeworkController@create')->name('.create');
                Route::post('/store', 'ClassroomHomeworkController@store')->name('.store');

                Route::prefix('{homework}')->group(function(){
                    Route::get('/edit', 'ClassroomHomeworkController@edit')->name('.edit');
                    Route::get('/show', 'ClassroomHomeworkController@show')->name('.show');
                    Route::patch('/update', 'ClassroomHomeworkController@update')->name('.update');
                    Route::delete('/delete', 'ClassroomHomeworkController@delete')->name('.delete');

                    Route::post('/send_reminder', 'ClassroomHomeworkController@send_reminder')->name('.send_reminder');
                });

                Route::name('.submissions')->group(function(){
                    // Homework submission
                    Route::get('/submissions/{homework_submission}', 'ClassroomHomeworkSubmissionController@show')->name('.show');
                    // Mark Homework Submission
                    // Route::post('/submissions/{homework_submission}/return', 'ClassroomHomeworkSubmissionController@return')->name('.return');
                    Route::post('/submissions/{homework}/submissions', 'ClassroomHomeworkSubmissionController@store')->name('.store');
                    Route::post('/submissions/{homework_submission}/mark', 'ClassroomHomeworkSubmissionController@markAndReturn')->name('.mark');
                });

            });
        });

        // Upload custom resource
        Route::post('/custom_resources/upload', 'ClassroomController@custom_file_upload')->name('.custom_file_upload');

        // Prices
        Route::get('prices', 'ClassroomPriceController@index')->name('.prices.index');
        // Subscriptions
        Route::prefix('subscription')->name('.subscriptions')->group(function(){
            Route::get('/', 'ClassroomSubscriptionController@show')->name('.show');
            Route::post('/store', 'ClassroomSubscriptionController@store')->name('.store');
            Route::post('/cancel', 'ClassroomSubscriptionController@cancel')->name('.cancel');
            Route::post('/resume', 'ClassroomSubscriptionController@resume')->name('.resume');
        });

        // Settings
        Route::get('/settings', 'ClassroomUserController@getSettingsView')->name('.settings');
        Route::get('/settings/t/invoice', 'ClassroomInvoiceSettingController@show')->name('.settings.teacher.invoice');
        Route::patch('/settings/t/invoice', 'ClassroomInvoiceSettingController@update')->name('.settings.teacher.invoice.update');

        Route::get('/settings/s/parent', 'ClassroomStudentController@editParentSettings')->name('.settings.student.parent');
        Route::patch('/settings/s/parent', 'ClassroomStudentController@updateParentSettings')->name('.settings.student.parent.update');

        // Billing
        Route::get('billing_history', 'ClassroomBillingController@history')->name('.billing_history');
        Route::get('download_invoice/{invoice_id}', function($invoice_id){
            return request()->user()->downloadInvoice($invoice_id, [
                'vendor' => config('app.name'),
                'product' => 'Classroom',
            ], 'board_invoice');
        })->name('.download_invoice');
    });
});

// Teacher Routes
Route::middleware('teacher_auth')->prefix('t')->name('teacher')->group(function(){
    // Getting started
    Route::get('getting-started', 'TeacherController@getGettingStartedView');
    Route::patch('/update', 'TeacherController@update')->name('.update');
});

// Parents routes
Route::prefix('p')->name('parent')->middleware('parent_auth')->group(function(){
    // Getting started
    Route::get('getting-started', 'StudentParentController@get_getting_started_view');
    Route::patch('/update', 'StudentParentController@update')->name('.update');
});

Route::view('/r/spm-state-papers', 'board_resources.spm_state_papers');

//Post and delete answers
Route::middleware('optimizeImages')->group(function () {
    Route::post('/questions/{question}/answers', 'AnswerController@store')->name('answers.store');
});

// Answers
Route::prefix('/answers')->name('answers')->group(function(){
    Route::get('/{answer}/edit', 'AnswerController@edit')->name('.edit');
    Route::patch('/{answer}/update', 'AnswerController@update')->name('.update');
    Route::delete('/{answer}', 'AnswerController@destroy')->name('.delete');

    Route::post('/{answer}/upvotes', 'AnswerUpvoteController@toggleUpvote')->name('.upvote');
    Route::post('/{answer}/check', 'AnswerCheckController@update')->name('.check');
});

// Question comments
Route::name('question_comments')->group(function(){
    Route::post('/questions/{question}/comments', 'QuestionCommentController@store')->name('.store');
    Route::delete('/questions/comments/{id}', 'QuestionCommentController@destroy')->name('.delete');
});

// Answer comments
Route::name('answer_comments')->group(function(){
    Route::post('/answers/{answer}/comments', 'AnswerCommentController@store')->name('.store');
    Route::delete('/answers/comments/{id}', 'AnswerCommentController@destroy')->name('.delete');
});

//Upvote/check for questions and answers
Route::post('/questions/{question}/upvotes', 'QuestionUpvoteController@toggleUpvote')->name('questions.upvote');

// Users
Route::prefix('/users')->name('users')->group(function(){
    // Questions/Answers
    Route::get('/{user}', 'UserController@show')->name('.show');
    Route::get('/{user}/questions', 'UserController@questions')->name('.questions');

    // Notes
    Route::get('/{user}/notes', 'UserController@notes')->name('.notes');

    // Articles
    Route::prefix('/{user}/articles')->name('.articles')->group(function(){
        Route::get('/', 'ArticleController@index');
    });

    Route::prefix('/articles')->name('.articles')->group(function(){
        Route::get('/create', 'ArticleController@create')->name('.create');
        Route::post('/store', 'ArticleController@store')->name('.store');

        Route::prefix('/{article}')->group(function(){
            Route::get('/show', 'ArticleController@show')->name('.show');
            Route::get('/edit', 'ArticleController@edit')->name('.edit');
            Route::patch('/update', 'ArticleController@update')->name('.update');
            Route::delete('/delete', 'ArticleController@delete')->name('.delete');
        });
    });

    // Account
    Route::patch('/{user}/account', 'UserController@accountSettings')->name('.account.settings');
    // Password
    Route::post('/{user}/password', 'Auth\ChangePasswordController@change')->name('.changePassword');
    // Switch Account Type
    Route::post('/{user}/account/switch-type', 'UserController@switchAccountType')->name('.account.switch_account_type');
});

// Settings
Route::prefix('/settings')->name('settings')->group(function(){
    // Profile
    Route::get('/', 'SettingsController@profile')->name('');
    // Password
    Route::get('/password', 'SettingsController@password')->name('.password');
    // Account
    Route::get('/account', 'SettingsController@account')->name('.account');
});

// Mark as Read
Route::middleware('auth')->get('/markAsRead', function(){
    $user = auth()->user();
    $user->unreadNotifications->markAsRead();
})->name('markAsRead');

// Board Pass
Route::get('/board-pass', 'BoardPassController@getBoardPassView')->name('board_pass');

Route::get('/coins', 'ProductController@index')->name('products.coins');

// Board Pass AJAX routes
Route::post('/board-pass/end-at', 'BoardPassController@getEndAtDate');
Route::post('/board-pass/no-of-additional-subscription-months', 'BoardPassController@getNoOfAdditionalSubscriptionMonths');
Route::post('/board-pass/amount-of-coins', 'BoardPassController@getAmountOfCoins');
Route::post('/board-pass/discount', 'BoardPassController@getDiscount');

Route::middleware('auth')->group(function(){
    Route::post('/products/{product}/purchase', 'ProductController@purchase')->name('products.purchase');
    Route::post('/board-pass/purchase', 'BoardPassController@purchase')->name('board_pass.purchase');

    Route::get('/payment', 'PaymentController@getUpdateView')->name('payment');
    Route::post('/payment/default/update', 'PaymentController@updateDefaultPaymentMethod')->name('payment.update_default_payment_method');
    Route::post('/payment/add', 'PaymentController@addPaymentMethod')->name('payment.add_payment_method');
    Route::post('/payment/invoice/{invoice}/download', 'PaymentController@downloadInvoice')->name('payment.download_invoice');

    Route::post('/topical-past-papers/claim/{past_paper}', 'TopicalPastPaperController@claim')->name('topical_past_papers.claim');

    Route::get('/my-coins', 'CoinController@show')->name('coins.show');
    // Route::get('/my-resources', 'ResourceController@show')->name('resources.show');

    // Recommend questions/answers routes
    Route::post('/questions/{question}/recommend', 'QuestionController@recommend')->name('questions.recommend');
    Route::post('/answers/{answer}/recommend', 'AnswerController@recommend')->name('answers.recommend');
});

Route::post('error/feedback', function(){
    // Data for email
    $email = request()->email ?? null;
    $feedback = request()->feedback;
    // To redirect users after submitting form
    $exam_board = request()->exam_board ?? null;

    // Send error feedback form via email
    Mail::to( config('mail.report_to.address') )->send(new ErrorFeedback($email, $feedback));

    if( !is_null($exam_board) ) {
        return redirect()->route('questions.index', $exam_board)->withSuccess('Thank you for your feedback!');
    }
    return redirect()->route('/')->withSuccess('Thank you for your feedback!');

})->name('errorFeedback');

// Events
Route::prefix('events')->name('events')->group(function(){
    Route::prefix('/2020')->name('.2020')->group(function(){
        Route::get('/math-board-quiz', 'EventController@getMathQuiz2020View')->name('.get_math_board_quiz_view');
        Route::post('/math-board-quiz', 'EventController@mathQuiz2020')->name('.math_board_quiz.quiz');
        Route::get('/math-board-quiz/thank-you/{is_existing_user}', 'EventController@getMathQuiz2020ThankYouView')->name('.math_board_quiz.thank_you');

        Route::middleware('auth')->group(function(){
            Route::get('/stoichiometry-mania/register', 'EventController@getStoichiometryMania2020RegisterView')->name('.get_stoichiometry_mania_register_view');
            Route::post('/stoichiometry-mania', 'EventController@stoichiometryMania2020Register')->name('.stoichiometry_mania_register');
        });
    });
});

// Route::view('/physical-resources', 'physical_resources')->name('physical_resources');
Route::view('/subject-chapter-id-reference', 'subject_chapter_id_reference')->name('subject_chapter_id_reference');

// Bug Report
Route::post('/bug_report', 'BugReportController@store')->name('bug_reports.store');

// Exam Board specific routes
Route::prefix('/{exam_board}')->group(function(){
    //Subjects and chapters
    Route::get('/subjects/{subject}', 'SubjectController@show')->name('subject');
    Route::get('/chapters/{chapter}', 'ChapterController@show')->name('chapter');

    Route::get('/questions/create', 'QuestionController@create')->name('questions.create');

    // Resource route for questions
    Route::resource('/questions', 'QuestionController');
    // Overriding resource routes
    Route::get('/', 'QuestionController@index')->name('questions.index');
    Route::get('/questions/{id}/{slug?}', 'QuestionController@show')->name('questions.show');
    Route::middleware('optimizeImages')->group(function () {
        Route::post('/questions', 'QuestionController@store')->name('questions.store');
    });

    Route::get('/search', 'SearchController@index')->name('search.index');

    // Resources
    Route::middleware('resource')->group(function(){
        // Past Papers
        Route::prefix('/past-papers')->name('past_papers')->group(function(){
            Route::get('/', 'PastPaperController@getHomeView')->name('.home');
            Route::get('/subjects/{subject?}', 'PastPaperController@subjectShow')->name('.subject_show');

            Route::prefix('/{past_paper}')->group(function(){
                Route::get('/qp', 'PastPaperController@questionShow')->name('.qp.show');
                Route::get('/ms', 'PastPaperController@answerShow')->name('.ms.show');

                Route::get('/qp/download', 'PastPaperController@downloadQpFile')->name('.qp.download');
                Route::get('/ms/download', 'PastPaperController@downloadMsFile')->name('.ms.download');

                Route::delete('/delete', 'PastPaperController@delete')->name('.delete');
            });
        });

        // Guides
        Route::prefix('guides')->name('guides')->group(function(){
            Route::get('/', 'GuideController@getHomeView')->name('.home');
            Route::get('/subjects/{subject}', 'GuideController@subjectShow')->name('.subject_show');

            Route::prefix('/{guide}')->group(function(){
                Route::get('/', 'GuideController@show')->name('.show');
                Route::delete('/delete', 'GuideController@delete')->name('.delete');
            });
        });

        // Marketplace
        Route::middleware('marketplace')->group(function(){
            // Notes
            Route::prefix('/notes')->name('notes')->group(function(){
                Route::get('', 'NotesController@getHomeView')->name('.home');
                Route::get('/subjects/{subject?}', 'NotesController@subjectShow')->name('.subject_show');

                Route::middleware('auth')->group(function(){
                    Route::get('/create', 'NotesController@create')->name('.create');
                    Route::post('/store', 'NotesController@store')->name('.store');
                    Route::prefix('/{note}')->group(function(){
                        Route::get('/', 'NotesController@show')->name('.show');

                        Route::get('/edit', 'NotesController@edit')->name('.edit');
                        Route::patch('/update', 'NotesController@update')->name('.update');

                        Route::get('/download_pdf', 'NotesController@downloadPdf')->name('.download_pdf');
                        Route::post('/unlock', 'NotesController@unlock')->name('.unlock');

                        Route::delete('/delete', 'NotesController@delete')->name('.delete');
                    });
                });
            });

            // Topical Past Papers
            Route::prefix('/topical-past-papers')->name('topical_past_papers')->group(function(){
                Route::get('', 'TopicalPastPaperController@getHomeView')->name('.home');
                Route::get('/subjects/{subject?}', 'TopicalPastPaperController@subjectShow')->name('.subject_show');

                Route::prefix('/{topical_past_paper}')->group(function(){
                    // QP/MS show
                    Route::get('/qp', 'TopicalPastPaperController@questionShow')->name('.qp.show');
                    Route::get('/ms', 'TopicalPastPaperController@answerShow')->name('.ms.show');

                    Route::get('/qp/download', 'TopicalPastPaperController@downloadQpFile')->name('.qp.download');
                    Route::get('/ms/download', 'TopicalPastPaperController@downloadMsFile')->name('.ms.download');

                    Route::delete('/delete', 'TopicalPastPaperController@delete')->name('.delete');
                });

                Route::middleware('auth')->group(function(){
                    Route::get('/create', 'TopicalPastPaperController@create')->name('.create');
                    Route::post('/store', 'TopicalPastPaperController@store')->name('.store');

                    Route::prefix('/{topical_past_paper}')->group(function(){
                        Route::get('/', 'TopicalPastPaperController@show')->name('.show');

                        Route::get('/edit', 'TopicalPastPaperController@edit')->name('.edit');
                        Route::patch('/update', 'TopicalPastPaperController@update')->name('.update');

                        Route::get('/download_pdf', 'TopicalPastPaperController@downloadPdf')->name('.download_pdf');
                        Route::post('/unlock', 'TopicalPastPaperController@unlock')->name('.unlock');

                        Route::delete('/delete', 'TopicalPastPaperController@delete')->name('.delete');
                    });
                });
            });
        });

        // Resource Review
        Route::middleware('auth')->prefix('/resources/{resource}/resource-reviews')->name('resource_reviews')->group(function(){
            Route::post('/store', 'ResourceReviewController@store')->name('.store');
        });
    });
});

Route::middleware('auth')->group(function(){
    // Withdrawal Requests
    Route::prefix('/withdrawal-requests')->name('withdrawal_requests')->group(function(){
        Route::post('/store', 'WithdrawalRequestController@store')->name('.store');
    });
});
