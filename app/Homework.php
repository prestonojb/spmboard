<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class Homework extends Model
{
    protected $table = 'homeworks';
    
    protected $guarded = [];
    protected $dates = ['due_at', 'created_at', 'updated_at'];

    /**
     * ------------------
     * Relationships
     * ------------------
     */

    public function chapter()
    {
        return $this->belongsTo(Chapter::class);
    }

    public function resources()
    {
        return $this->belongsToMany( Resource::class)->withPivot('type', 'created_at', 'updated_at');
    }

    public function submissions()
    {
        return $this->hasMany( HomeworkSubmission::class );
    }

    public function lesson()
    {
        return $this->belongsTo(Lesson::class);
    }

    public function classroom()
    {
        return $this->lesson->classroom;
    }

    /**
     * ----------------
     * Helpers
     * ----------------
     */

    public function getSubmissionCountByStatus( $status = null )
    {
        if(is_null($status)) {
            return $this->submissions()->count();
        }

        return $this->submissions()->where('status', $status)->count();
    }

    public function getSubmissionPercentageByStatus( $status = null )
    {
        $total_submissions = $this->submissions()->count();

        if($total_submissions) {
            return ($this->getSubmissionCountByStatus($status))/$total_submissions * 100;
        } else {
            return 0;
        }
    }

    public function getAssignedCount()
    {
        return count($this->lesson->students ?? []);
    }

    public function unsubmittedStudents()
    {
        $homeworkId = $this->id;
        $students = $this->lesson->classroom->students()->whereDoesntHave('homework_submissions', function($q) use ($homeworkId){
            $q->where('homework_id', $homeworkId);
        })->get();

        return $students;
    }

    public function unsubmittedStudentCount()
    {
        return count($this->unsubmittedStudents() ?? []);
    }

    // Returns collection of custom resoures with full URL path
    public function getCustomResources()
    {
        $custom_resource_rows = DB::table('homework_resource')->select('file_name', 'file_url')->where('homework_id', $this->id)->whereNull('resource_id')->get();

        $custom_resources = collect();

        // Modify 'file_url' column to contain full path
        foreach( $custom_resource_rows as $row ) {
            $row->file_url = Storage::url( $row->file_url );
            $custom_resources->push( $row );
        }

        return $custom_resources;
    }

    /**
     * Get all Custom Resource URLs
     * @return Collection
     */
    public function custom_resource_urls()
    {
        // Returns all custom resource URLs
        $urls = $this->customResourceRelativeUrls( false );
        $full_urls = $urls->map(function($url){
            return Storage::cloud()->url($url);
        });

        return $full_urls;
    }

    /**
     * Returns all Custom Resource URLs
     * @param boolean (optional, default = true) $toArray
     * @return array|Collection
     */
    public function customResourceRelativeUrls( $toArray = true )
    {
        $urls = DB::table('homework_resource')->where('homework_id', $this->id)->whereNull('resource_id')->pluck('file_url');

        return $toArray ? $urls->toArray()
                        : $urls;
    }


    /**
     * ------------------
     * Accessors
     * ------------------
     */

    public function getPrimaryAttribute()
    {
        return $this->title;
    }

    public function getSecondaryAttribute()
    {
        $str = "";

        if( auth()->user()->student ) {
            $str .= $this->getStatusTagHtmlForStudent( auth()->user()->student ). " ";
        }
        if($this->due_at) {
            $str .= "Due {$this->due_at->toFormattedDateString()} · ";
        }

        if($this->chapter) {
            $str .= "{$this->chapter->name} ·";
        }

        return $str . "Posted {$this->created_at->shortRelativeDiffForHumans()}";
    }

    public function getUrlAttribute()
    {
        return route('classroom.lessons.homeworks.show', [$this->lesson->id, $this->id]);
    }

    /**
     * -----------------
     * Helper Functions
     * -----------------
     */

    /**
     * Attach resources(belongsToMany) relationship to homework
     * @param array $resource_data Resource IDs to be attached
     * @param array $custom_resource_data Custom Resource file name & URL to be attached
     */
    public function storeResources( $resource_data, $custom_resource_data )
    {
        foreach( $resource_data as $resource_datum ) {
            $this->resources()->attach(
                $resource_datum->resource_id, [
                    'type' => $resource_datum->type,
                ]
            );
        }

        foreach( $custom_resource_data as $custom_resource_datum ) {
            DB::table('homework_resource')->insert([
                'file_name' => $custom_resource_datum->file_name,
                'file_url' => $custom_resource_datum->file_url,
                'homework_id' => $this->id,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]);
        }
    }

    /**
     * Returns true if homework is graded (marks given), else false
     * @return boolean
     */
    public function isGraded()
    {
        return $this->max_marks > 0;
    }

    public function getSubmissionsWithStatus($status) {
        return $this->submissions()->where('status',  $status )->get();
    }

    public function hasSubmitted()
    {
        return $this->submissions()->where('student_id', auth()->user()->student->user_id)->exists();
    }

    /**
     * Returns true if has student submission, else false
     * @return boolean
     */
    public function hasStudentSubmission( Student $student )
    {
        return !is_null($this->getStudentSubmission($student));
    }

    public function getStudentSubmission( Student $student )
    {
        return $student ? HomeworkSubmission::where('homework_id', $this->id)->where('student_id', $student->user_id)->first()
                        : null;
    }

    public function getStatusTagHtmlForStudent( Student $student )
    {
        $submission = $this->getStudentSubmission( $student );
        return $submission ? $submission->status_tag_html
                            : '<span class="status-tag unsubmitted">Unsubmitted</span>';
    }

    /**
     * Delete resource post all related relationships and itself
     * :deleted: Submissions (and Files), Custom Resource Files, Homework
     * :detached: Resources
     */
    public function cleanDelete()
    {
        // Delete all submissions
        foreach( $this->submissions as $submission ) {
            // Delete submission file
            Storage::cloud()->delete( $submission->file_relative_url );
            // Delete marked file
            Storage::cloud()->delete( $submission->marked_file_relative_url );
            // Delete DB row
            $submission->delete();
        }

        // {---- RESOURCES ----} //
        // Detach prebuilt-resources relationship
        $this->resources()->detach();
        // Delete ALL custom files
        $urls = $this->customResourceRelativeUrls();
        Storage::cloud()->delete( $urls );

        $this->delete();
    }
}
