<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\SoftDeletes;

class PastPaper extends Model
{
    use SoftDeletes;

    protected $guarded = [];

    /**
     * -----------------
     * Relationships
     * -----------------
     */

    public function subject()
    {
        return $this->belongsTo(Subject::class);
    }

    public function chapter()
    {
        return $this->belongsTo(Chapter::class);
    }

    public function resource()
    {
        return $this->morphOne('App\Resource', 'resourceable');
    }

    /**
     * -----------------
     * Accessors
     * -----------------
     */
    public function getPaperAttribute()
    {
        $paper_number = $this->paper_number;
        if($paper_number >= 10) {
            // Divide without remainder
            return (int) ($paper_number / 10);
        }
        return $paper_number;
    }

    public function getVariantAttribute()
    {
        $paper_number = $this->paper_number;
        if($paper_number >= 10) {
            return $paper_number % 10;
        }
        return null;
    }

    // URL
    /**
     * Return Question Paper file URL on S3
     * @return string|null
     */
    public function getQpUrlAttribute($value)
    {;
        if(strpos($value, 'http') !== false || strpos($value, 'https') !== false){
            return $value;
        }
        return $value ? Storage::cloud()->url($value) : null;
    }

    /**
     * Returns QP relative URL path, used with Storage::url()
     * @return string
     */
    public function getQpUrlRelativePathAttribute()
    {
        return $this->getOriginal('qp_url');
    }

    /**
     * Returns URL
     * @return string
     */
    public function getUrlAttribute()
    {
        return $this->qp_url;
    }

    /**
     * Returns show URL
     * @return string
     */
    public function getShowUrlAttribute()
    {
        return $this->qp_route;
    }

    /**
     * Returns QP show URL
     * @return string
     */
    public function getQpRouteAttribute()
    {
        return route('past_papers.qp.show', [$this->subject->exam_board->slug, $this->id]);
    }

    public function getMsUrlAttribute($value)
    {
        if(strpos($value, 'http') !== false || strpos($value, 'https') !== false){
            return $value;
        }
        return $value ? Storage::cloud()->url($value) : null;
    }

    public function getMsUrlRelativePathAttribute()
    {
        return $this->getOriginal('ms_url');
    }

    public function getMsRouteAttribute()
    {
        return route('past_papers.ms.show', [$this->subject->exam_board->slug, $this->id]);
    }

    public function getSubjectRouteAttribute()
    {
        return route('past_papers.subject_show', [$this->subject->exam_board->slug, $this->subject->id]);
    }

    /**
     * Get question paper file download route
     */
    public function getQpDownloadRouteAttribute()
    {
        return route('past_papers.qp.download', [$this->subject->exam_board->slug, $this->id]);
    }

    /**
     * Get mark scheme file download route
     */
    public function getMsDownloadRouteAttribute()
    {
        return route('past_papers.ms.download', [$this->subject->exam_board->slug, $this->id]);
    }

    // --URL
    /**
     * Returns primary attribute
     * @return string
     */
    public function getPrimaryAttribute()
    {
        return $this->name;
    }

    /**
     * Returns secondary attribute
     * @return string
     */
    public function getSecondaryAttribute()
    {
        return $this->code ?? 'Paper'.$this->paper_number;
    }

    /**
     * Return description of past paper
     * @return string
     */
    public function getDetailsAttribute()
    {
        return $this->description;
    }

    /**
     * Return description of past paper
     * @return string
     */
    public function getDescriptionAttribute()
    {
        // code, type, paper, season
        $description = "Year {$this->year} ";
        if( !is_null($this->season) ) {
            $description .= '· '.$this->season.' ';
        }
        if( !is_null($this->paper_number) ) {
            $description .= '· Paper '.$this->paper_number.' ';
        }
        if( !is_null($this->code) ) {
            $description .= '· '.$this->code.' ';
        }
        if( !is_null($this->type) ) {
            $description .= '· '.$this->type.' ';
        }
        return $description;
    }

    /**
     * Inverse hasOneThrough Query Builder Reference: https://stackoverflow.com/questions/39159285/laravel-5-hasmanythrough-inverse-querying
     * Returns an Eloquent builder of Past Paper by exam board
     *
     * @param string $exam_board Exam Board name('SPM', 'IGCSE'...)
     * @return Eloquent Builder
     */
    public static function getPastPaperByExamBoard($exam_board)
    {
        return PastPaper::whereHas('subject.exam_board', function($q) use ($exam_board) {
            return $q->where('name', $exam_board->name);
        });
    }

    /**
     * Returns file URL on S3 after upload
     * @param UploadedFile $file
     * @param App\ExamBoard $exam_board
     * @return string File URL
     */
    public static function upload($file, ExamBoard $exam_board)
    {
        // File overriding fails to update file
        $filename = $file->getClientOriginalName();
        $path = "past-papers/{$exam_board->slug}";

        // Delete file if exists
        if( Storage::cloud()->exists( $path.$filename ) ) {
            Storage::cloud()->delete( $path.$filename );
        }
        $file->storeAs($path, $filename, 's3');
        return "{$path}/{$filename}";
    }

    /**
     * Add files to existing past paper
     * @param UploadedFile $file
     * @param boolean $is_mark_scheme
     */
    public function addFile($file, $is_mark_scheme)
    {
        $exam_board = $this->subject->exam_board;
        $file_url = self::upload($file, $exam_board);

        if($is_mark_scheme) {
            $this->update(['ms_url' => $file_url]);
        } else {
            $this->update(['qp_url' => $file_url]);
        }
    }

    // When is update is set to true, existing files will be deleted first before uploading
    public function save_files($qp_file, $ms_file, $isUpdating = false)
    {
        $exam_board_slug = $this->subject->exam_board->slug;

        if( !is_null($qp_file) ) {

            // Delete existing files
            if( $isUpdating ) {
                Storage::cloud()->delete( $this->qp_url_relative_path );
            }

            //Store file to S3 with filename as the original
            $filename = str_replace(' ', '_', $qp_file->getClientOriginalName());

            $path = $qp_file->storeAs('past-papers/'.$exam_board_slug, $filename,'s3');

            //Save Question Paper Url in database
            $this->qp_url = 'past-papers/'.$exam_board_slug.'/'.$filename;
        }

        if( !is_null($ms_file) ) {

            // Delete existing files
            if( $isUpdating ) {
                Storage::cloud()->delete( $this->ms_url_relative_path );
            }

            $filename = str_replace(' ', '_', $ms_file->getClientOriginalName());

            //Store file to S3
            $path = $ms_file->storeAs('past-papers/'.$exam_board_slug, $filename, 's3');

            //Save Mark Scheme Url in database
            $this->ms_url = 'past-papers/'.$exam_board_slug.'/'.$filename;
        }

        $this->save();
    }

    /**
     * Returns a Collection of set papers
     * @return Collection
     */
    public function getSetPapers()
    {
        $set_papers = self::where('subject_id', $this->subject_id)->where('year', $this->year);

        if(!is_null($this->season)) {
            $set_papers = $set_papers->where('season', $this->season);
        }

        return $set_papers->orderBy('paper_number')->get();
    }
}
