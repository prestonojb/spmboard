<?php

namespace Tests\Feature\Classroom;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use App\Notifications\Classroom\ResourcePostPosted;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Tests\TestCase;
use Carbon\Carbon;
use App\Classroom;
use App\Lesson;
use App\Subject;
use App\Resource;
use App\ResourcePost;
use App\ResourcePostRating;
use App\User;
use Illuminate\Support\Facades\Notification;

class ResourcePostTest extends TestCase
{
    use WithFaker;

    private $teacher;
    private $student;
    private $parent;
    private $classroom;
    private $subject;

    public function setUp() : void
    {
        parent::setUp();

        // Subscribed Teacher
        $user = factory(User::class)->states('subscribed_teacher')->create();
        $this->teacher = $user->teacher;

        // Student with Board Pass
        $user = factory(User::class)->states('student_with_board_pass')->create();
        $this->student= $user->student;

        // Parent
        $user = factory(User::class)->states('parent')->create();
        $this->parent = $user->parent;


        // Subject and classroom
        $this->subject = Subject::first();
        $this->classroom = factory(Classroom::class)->create([
                            'teacher_id' => $this->teacher->user_id,
                            'subject_id' => $this->subject->id,
                        ]);
        // Lesson
        $this->lesson = factory(Lesson::class)->create([
                            'classroom_id' => $this->classroom->id
                        ]);

        // Attach parent to child
        $this->student->updateParent( $this->parent );
        // Add student to classroom
        $this->classroom->addStudent($this->student);
    }

    /**
     * @group resource_post
     * Test for storing resource post
     */
    public function testStore()
    {
        Notification::fake();

        $init_count = ResourcePost::count();

        $response = $this->actingAs($this->teacher->user)
                        ->post( route('classroom.lessons.resource_posts.store', $this->lesson->id), [
                            'title' => $this->faker->name,
                            'description' => $this->faker->optional->sentence,
                        ]);

        // Test Resource post added into DB
        $this->assertEquals($init_count + 1, ResourcePost::count(), "Resource Post is not stored in DB!");

        // Lesson stored Notification sent to students/parents
        Notification::assertSentTo(
            [$this->student], ResourcePostPosted::class
        );

        // Redirect
        $response->assertRedirect();
    }

    /**
     * @group resource_post
     * Test for rating resource post
     */
    public function testRate()
    {
        // Resource Post
        $resource_post = factory(ResourcePost::class)->create([
            'user_id' => $this->teacher->user_id,
            'lesson_id' => $this->lesson->id,
        ]);

        // Test student have not rated resource post
        $this->assertTrue( $resource_post->getStudentRating( $this->student ) == 0);

        foreach(range(1, 1) as $rating) {
            $response = $this->actingAs($this->student->user)
                            ->post( route('classroom.lessons.resource_posts.rate', [$this->lesson, $resource_post->id]), [
                                'rating' => $rating,
                            ]);

            $student_rating = $resource_post->getStudentRating($this->student);
            // Test student rated resource post
            $this->assertTrue( $student_rating == strval(number_format($rating, 1)) );
        }
    }

    /**
     * @group resource_post
     * Test Resource Post rate over maximum(5)
     */
    public function testRateOverMax()
    {
        // Resource Post
        $resource_post = factory(ResourcePost::class)->create([
            'user_id' => $this->teacher->user_id,
            'lesson_id' => $this->lesson->id,
        ]);

        $response = $this->actingAs($this->student->user)
                            ->post( route('classroom.lessons.resource_posts.rate', [$this->lesson, $resource_post->id]), [
                                'rating' => 6,
                            ]);

        // Test resource post rating fail to update (Max rating = 5)
        $response->assertSessionHasErrors('rating', "Resource Post Rating cannot be set more than 5!");
    }

    /**
     * @group resource_post
     * Test Delete Resource Post
     */
    public function testDelete()
    {
        Storage::fake('s3');

        // Resource Post with resource
        $resource_post = factory(ResourcePost::class)->states('with_resource')->create([
            'user_id' => $this->teacher->user_id,
            'lesson_id' => $this->lesson->id,
        ]);

        $resource = Resource::first();

        // Attach pre-built resource
        $resource_post->resources()->attach( $resource->id );

        // Upload custom resource
        $custom_file = UploadedFile::fake()->create('document.pdf', 4096, 'application/pdf');
        $obj = (object) [
            'file_name' => $custom_file->hashName(),
            'file_url' => 'custom_resources/'.$custom_file->hashName(),
        ];
        $custom_file->store('custom_resources/'.$custom_file->hashName(), 's3');
        $resource_post->storeResources([], [$obj]);

        $resource_post_id = $resource_post->id;

        // Add rating to resource post
        $init_rating = rand(0,5);
        $resource_post->updateOrAddRating( $this->student, $init_rating );

        // DELETE request
        $response = $this->actingAs($this->teacher->user)
                         ->delete( route('classroom.lessons.resource_posts.delete', [$this->lesson->id, $resource_post->id]), []);

        // Assert resource post deleted
        $this->assertTrue( is_null( ResourcePost::find($resource_post_id) ), "Resource Post not deleted!" );


        // Assert resource post ratings deleted
        $this->assertTrue( ResourcePostRating::where('resource_post_id', $resource_post_id)->count() == 0, "Resource Post Rating not deleted!" );


        // Assert resource post pre-built resource not deleted
        $this->assertTrue( !is_null( Resource::find($resource->id) ) , "Prebuilt Resource should not be deleted!" );

        // Assert resource post pre-built resource relationship detached
        $this->assertTrue( DB::table('resource_resource_post')->where('resource_post_id', $resource_post_id)->whereNotNull('resource_id')->count() == 0, "Resource Post Prebuilt Resource not detached!" );


        // Assert resource post custom resource relationship detached
        $this->assertTrue( DB::table('resource_resource_post')->where('resource_post_id', $resource_post_id)->whereNull('resource_id')->count() == 0, "Resource Post Custom Resource not detached and deleted!" );


        /**
         * Reference: https://laracasts.com/discuss/channels/testing/storageassertmissing-failing-file-is-actually-gone-in-real-life
         * Must specify environment in destroy function() for this test to return true, ignore for now and test file deletion manually
         */
        // Storage::disk('s3')->assertMissing( 'custom_resources/'.$custom_file->hashName(), "Resource Post Custom Upload File not deleted from Storage!");


        // Assert session 'error'
        $response->assertSessionHas('error');
        // Assert Redirect
        $response->assertRedirect( route('classroom.lessons.show', [$this->classroom->id, $this->lesson->id]), "Response did not redirect to route!" );
    }
}
