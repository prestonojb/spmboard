<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Classroom;
use App\ClassroomPost;
use App\User;
use Faker\Generator as Faker;

$factory->define(ClassroomPost::class, function (Faker $faker) {
    return [
        'title' => $this->faker->sentence,
        'description' => $this->faker->text,
        'classroom_id' => function() {
            return factory(Classroom::class)->create()->id;
        },
        'user_id' => function() {
            return factory(User::class)->create()->id;
        },
        'categories' => json_encode(['lesson']),
        'type' => 'announcement'
    ];
});
