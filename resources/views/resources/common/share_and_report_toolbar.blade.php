<div class="d-flex justify-content-between align-items-center">
    {{-- Share Buttons --}}
    <div class="shareIcons"></div>
    {{-- Report Button --}}
    <button class="button-link blue4 underline-on-hover" data-toggle="modal" data-target="#bugReportModal">Report</button>
    @component('bug_reports.common.store_modal', ['tag' => $tag, 'description' => $description, 'email' => $email])
    @endcomponent
</div>
