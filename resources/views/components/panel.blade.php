<div class="panel p-0">
    <div class="panel-header">
        {{ $title }}
    </div>
    <div class="panel-body">
        {{ $slot }}
    </div>
</div>
