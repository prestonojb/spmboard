<?php

namespace App\Policies;

use App\ExamBuilderSheet;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ExamBuilderSheetPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Permission to show Exam Builder Sheet
     */
    public function show(User $user, ExamBuilderSheet $exam_builder_sheet)
    {
        return $user->id == $exam_builder_sheet->user_id;
    }

    /**
     * Permission to download Exam Builder Sheet QP
     */
    public function download_qp(User $user, ExamBuilderSheet $exam_builder_sheet)
    {
        return !is_null($exam_builder_sheet->qp_url) && $user->id == $exam_builder_sheet->user_id && $user->hasActiveBoardPass();
    }

    /**
     * Permission to download Exam Builder Sheet MS
     */
    public function download_ms(User $user, ExamBuilderSheet $exam_builder_sheet)
    {
        return !is_null($exam_builder_sheet->ms_url) && $user->id == $exam_builder_sheet->user_id && $user->hasActiveBoardPass();
    }

    /**
     * Permission to create Exam Builder Question Paper
     */
    public function generate_qp(User $user)
    {
        return $user->canGenerateExamBuilderSheet();
    }

    /**
     * Permission to create Exam Builder Sheet Mark Scheme
     */
    public function generate_ms(User $user)
    {
        return true;
    }

    /**
     * Permission to delete Exam Builder Sheet
     */
    public function delete(User $user, ExamBuilderSheet $exam_builder_sheet)
    {
        return $user->hasActiveBoardPass();
    }

    /**
     * Permission to hide water
     */
    public function hide_watermark(User $user)
    {
        return $user->canHideExamBuilderSheetWatermark();
    }

    /**
     * Permission to generate sheet with subchapters
     */
    public function generate_with_subchapter(User $user)
    {
        return $user->canGenerateExamBuilderSheetWithSubchapter();
    }
}
