<?php

namespace App\Policies;

use App\Answer;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Auth;

class AnswerPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Permission to browse answers (Admin Panel)
     */
    public function browse(User $user)
    {
        return $user->isAdmin() || $user->isModerator();
    }

    /**
     * Permission to accept answer
     */
    public function check(User $user, Answer $answer)
    {
        return $user->id == $answer->question->user_id;
    }

    /**
     * Permission to answer
     */
    public function create(User $user)
    {
        return !$user->is_parent();
    }

    /**
     * Permission to edit existing answer
     */
    public function edit(User $user, Answer $answer)
    {
        if($user->isAdmin() || $user->isModerator()) {
            return true;
        }
        return $user->id == $answer->user_id;
    }

    /**
     * Permission to delete existing answer
     */
    public function delete(User $user, Answer $answer)
    {
        if($user->isAdmin() || $user->isModerator()) {
            return true;
        }
        return $user->id == $answer->user_id;
    }

    /**
     * Permission to recommend answer
     * Refer to AnswerController@recommend
     */
    public function recommend(User $user, Answer $answer)
    {
        return $user->isAdmin() || $user->isModerator();
    }
}
