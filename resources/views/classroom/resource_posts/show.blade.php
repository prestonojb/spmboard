@extends('classroom.layouts.app')

@section('meta')
    <title>Resource - {{ $resource_post->title }} | {{ config('app.name') }} Classroom</title>
@endsection


@section('content')

@component('classroom.common.breadcrumbs')
    <a href="{{ route('classroom.classrooms.show', $classroom->id) }}">Dashboard</a> /
    <a href="{{ route('classroom.lessons.index', $classroom->id) }}">Lessons</a> /
    <a href="{{ route('classroom.lessons.show', [$lesson->classroom->id, $lesson->id]) }}">{{ $lesson->name }}</a> /
    {{ str_limit( $resource_post->title, 25 ) }}
@endcomponent

@component('classroom.common.page_heading', ['title' => $resource_post->primary])
    <div class="mb-0 gray2">{!! $resource_post->secondary !!}</b></div>

    @slot('button_group')
        @can('update', $resource_post)
            <a href="{{ route('classroom.lessons.resource_posts.edit', [$lesson->id, $resource_post->id]) }}" class="button--primary--inverse mr-1"><span class="iconify" data-icon="feather:edit" data-inline="false"></span> Edit</a>
        @endcan
        @can('delete', $resource_post)
            <form action="{{ route('classroom.lessons.resource_posts.delete', [$lesson->id, $resource_post->id]) }}" method="post">
                @csrf
                @method('DELETE')
                <button type="submit" class="button--danger deleteButton"><span class="iconify" data-icon="emojione-monotone:cross-mark" data-inline="false"></span> Delete</button>
            </form>
        @endcan
    @endslot
@endcomponent


{{-- Key Value Block --}}
@php
    $data = [
        ['key' => 'Chapter',
         'value' => $resource_post->chapter ? $resource_post->chapter->name : 'N/A',
         'icon' => 'fas fa-list-ul fa-2x',
         'color' => 'primary'],
        ['key' => 'Average Understanding Rating',
         'value' => $resource_post->average_rating ?? 'N/A',
         'icon' => 'fas fa-star fa-2x',
         'color' => 'info'],
    ];
@endphp
<div class="row">
    @foreach($data as $datum)
        <div class="col-xl-4 col-md-6 mb-4">
            @component('classroom.classrooms.common.key_value_block', ['datum' => $datum])
            @endcomponent
        </div>
    @endforeach
</div>

@php
    $data = [
        'Description' => $resource_post->description ?? 'N/A',
        'Created At' => $resource_post->created_at->toDayDateTimeString(),
        'Updated At' => $resource_post->updated_at->toDayDateTimeString(),
    ];
@endphp
@component('classroom.common.details', ['data' => $data, 'resourceable' => $resource_post])
@endcomponent

<div class="mt-4 mb-5 border-bottom"></div>

<div class="row">
    <div class="col-12 col-lg-9">

        @can('update_store_lesson', $classroom)
            <h3 class="pb-3 mb-2 border-bottom"><b>Understanding Rating</b></h3>
            <div class="row">
                @foreach(range(1, 5) as $rating)
                    @php
                        $title = $rating > 1 ? $rating.' stars' : $rating.' star';
                        $subtitle = $resource_post->getStudentCountByRating($rating) > 1 ? $resource_post->getStudentCountByRating($rating).' students' : $resource_post->getStudentCountByRating($rating).' student';
                    @endphp
                    <div class="col-12 col-md-6">
                        @component('classroom.classrooms.common.list', ['classroom' => $classroom, 'type' => 'student', 'title' => $title, 'subtitle' => $subtitle, 'collection' => $resource_post->getStudentsByRating( $rating )])
                        @endcomponent
                    </div>
                @endforeach
            </div>
        @endcan

        @can('rate', $resource_post)
            <div class="text-center pt-2 pb-3">
                <h3 class="mb-1">How well did you understand this resource?</h3>
                <p class="mb-3 gray2 font-size2">Rate your understanding from 1-5 stars. Your teacher will be able to review this.</p>
                <div class="star-rating-bar">
                    @foreach( range(1,5) as $i )
                        <span class="iconify star {{ $resource_post->getStudentRating(auth()->user()->student) >= $i ? 'active' : '' }}" data-rating="{{ $i }}" data-icon="ant-design:star-filled" data-inline="false"></span>
                    @endforeach
                </div>
            </div>
        @endcan
    </div>

{{--@can('update_store_lesson', $classroom)
        <div class="col-12 col-lg-3">
            <div class="panel p-0">
                <div class="panel-header">Understanding Rating</div>

                <div class="panel-body">
                    @foreach(range(5, 1, -1) as $rating)
                        <div class="rating-progress-bar @if($loop->last) mb-0 @endif">
                            <div class="unit">
                                {{ $rating }}<i class="yellow fas fa-star"></i>
                            </div>
                            <div class="progress">
                                <div class="progress-bar" style="width: {{ $resource_post->getStudentPercentageByRating( $rating ) }}%;" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    @endcan --}}

</div>

<div class="py-2"></div>
@endsection

@section('scripts')
<script>
$(function(){
    $('.learn__chapter').click(function(){
        $(this).toggleClass('active');
    });
    $('.star-rating-bar .star').mouseover(function(){
        $(this).prevAll('.star').addClass('hover');
    });

    $('.star-rating-bar .star').mouseout(function(){
        $(this).siblings('.star').removeClass('hover');
    });

    $('.star-rating-bar .star').click(function(){
        var rating = $(this).data('rating');
        var count = 1;
        $( '.star-rating-bar .star' ).each(function(){
            $(this).removeClass('active');
            if( count <= rating ) {
                $(this).addClass('active');
            }
            count += 1;
        });
        $.ajax({
            type: "POST",
            url: "{{ route('classroom.lessons.resource_posts.rate', [$lesson->id, $resource_post->id]) }}",
            data: {
                'rating': rating,
            },
            success: function ( rating ) {
            }
        });
    });
});
</script>
@endsection
