@extends('classroom.layouts.app')

@section('meta')
    <title>No Classroom | {{ config('app.name') }} Classroom</title>
@endsection

@section('content')
<div class="container">
    <div class="text-center py-5">
        <img src="{{ asset('img/essential/empty.svg') }}" alt="" width="300" height="300">
        <h3 class="h4">No classrooms created yet, would you like to create one?</h3>
        <a href="{{ route('classroom.lessons.create') }}" class="btn btn-primary">Create classroom</a>
    </div>
</div>
@endsection
