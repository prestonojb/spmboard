{{-- Button --}}
<button type="button" class="button-link" data-toggle="modal" data-target="#sortSelect">
    Sort by: {{ isset($sort) ? ucfirst($sort) : 'Hot' }}
</button>

{{-- Sort --}}
<div class="modal" id="sortSelect" tabindex="-1" role="dialog" aria-labelledby="sortSelect" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="list-group">
                <a href="{{ url()->current(). '?sort=' }}" class="list-group-item list-group-item-action @if(!isset($sort)) active @endif">Hot</a>
                {{-- <a href="{{ url()->current(). '?sort=top' }}" class="list-group-item list-group-item-action @if($sort=='top') active @endif">Top</a>
                <a href="{{ url()->current(). '?sort=new' }}" class="list-group-item list-group-item-action @if($sort=='new') active @endif">New</a> --}}
                <a href="{{ url()->current(). '?sort=answered' }}" class="list-group-item list-group-item-action @if($sort=='answered') active @endif">Answered</a>
            </div>
        </div>
    </div>
</div>
