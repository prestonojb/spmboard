<div class="key-value-pair">
    <div class="key">{{ $key }}</div>
    <div class="value">{!! $value !!}</div>
</div>
