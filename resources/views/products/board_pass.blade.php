@extends('main.layouts.app')

@section('meta')
<title>Board Pass | {{ config('app.name') }}</title>
@endsection

@section('content')
@include('main.navs.header')
<div class="bg-white">
    {{-- Main --}}
    @component('main.section.main', ['title' => 'Board Pass', 'description' => 'Supercharge your revision with the all-powerful Exam Builder premium features!'])
        <div class="text-center">
            <a class="button--primary lg" href="#getBoardPass">
                Get Board Pass
            </a>
        </div>
    @endcomponent

    {{-- Here's what you can access with Premium Exam Builder --}}
    @php
        $data = [
            ['title' => 'Unlimited Worksheets', 'description' => 'Generate as many worksheets you need across 5 Cambridge IGCSE subjects (Math, Add Math, Biology, Chemistry, Physics)', 'icon' => 'ion:infinite-outline'],
            ['title' => '20 questions per worksheet', 'description' => 'Get more questions in your worksheet and speed up your revision progress', 'icon' => 'bi:list-nested'],
            ['title' => 'Download worksheets in PDF', 'description' => ' Print your worksheets and practice on paper', 'icon' => 'ant-design:cloud-download-outlined'],
            // ['title' => 'Discounted Resources', 'description' => setting('board_pass.resource_discount_in_percentage').'% for all study resources', 'icon' => 'akar-icons:book'],
            // ['title' => 'Additional Libraries', 'description' => 'Store all your study materials under different libraries for different purposes', 'icon' => 'codicon:library'],
        ];
    @endphp
    @component('main.section.icon_list', ['bg_light' => true, 'title' => 'Why Board Pass?', 'description' => 'Unlimited access to Exam Builder!', 'data' => $data])
    @endcomponent

    {{-- Board Pass --}}
    @auth
        <div class="container">
            @component('main.section.standard', ['title' => 'Current Board Pass'])
                <div class="maxw-960px mx-auto">
                    {{-- Active Board Pass --}}
                    @component('resources.common.board_pass', ['board_pass' => auth()->user()->getLatestBoardPass()])
                    @endcomponent
                </div>
            @endcomponent
        </div>
    @endauth

    {{-- Board Pass Purchase Form Section --}}
    <div class="bg-blue6 py-4" id="getBoardPass">
        <div class="container">
            <div class="maxw-960px mx-auto">
                <div class="text-center py-4 mb-2">
                    <h3 class="text-white font-size9"><b>BOARD PASS</b></h3>
                    @auth
                        @if(auth()->user()->hasActiveBoardPass())
                            <div class="badge bg-white blue6 font-size5">Current Board Pass active until:
                                {{ auth()->user()->getActiveBoardPass()->end_at->format('M Y') }}
                            </div>

                            <div class="py-1"></div>
                        @endif
                        @if(auth()->user()->hasCoins())
                            <div class="badge bg-white blue6 font-size5">Current Balance:
                                <img src="{{ asset('img/essential/coin.svg') }}" alt="coin" width="20" height="20" class="d-inline-block mr-1">
                                {{ auth()->user()->coin_balance }}
                            </div>
                        @endif
                    @endauth


                    {{-- Board Pass Purchase Form --}}
                    @auth
                        <div class="py-3"></div>
                        <div class="panel w-100 py-3">
                            <h3 class="blue6 font-size7">Subscribe until
                                {{-- Month --}}
                                <select name="subscribe_until_month" style="width: 120px;">
                                    <option value="" disabled selected>Month</option>
                                    @foreach(getAllMonths() as $key=>$value)
                                        @if(!is_null(old('subscribe_until_month')))
                                            <option value="{{ $key }}" @if(old('subscribe_until_month') == $key) selected @endif>{{ $value }}</option>
                                        @else
                                            <option value="{{ $key }}" @if($subscribe_from_date->copy()->addMonth()->month == $key) selected @endif>{{ $value }}</option>
                                        @endif
                                    @endforeach
                                </select>

                                {{-- Year --}}
                                <select name="subscribe_until_year" style="width: 75px;">
                                    <option value="" disabled selected>Year</option>
                                    @php $now = \Carbon\Carbon::now(); @endphp
                                    @foreach(range($now->year, $now->copy()->addYears(3)->year) as $i)
                                        @if(!is_null(old('subscribe_until_year')))
                                            <option value="{{ $key }}" @if(old('subscribe_until_year') == $i) selected @endif>{{ $value }}</option>
                                        @else
                                            <option value="{{ $i }}" @if($subscribe_from_date->copy()->addMonth()->year == $i) selected @endif>{{ $i }}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </h3>

                            <div class="mb-3">
                                <p class="font-size4 mb-1">No. of additional subscription months: <b><span class="font-weight700 font-size-inherit noOfSubscriptionMonths">0</span></b></p>
                                <p class="font-size4 mb-0">Valid until: <b><span class="font-weight700 font-size-inherit validUntilDate"></span></b></p>
                            </div>

                            <form action="{{ route('board_pass.purchase') }}" method="post" class="">
                                @csrf
                                <input type="hidden" name="duration_in_month">

                                {{-- Discount Tag --}}
                                <span class="badge badge-warning d-none font-size1 mb-2 discountTag">
                                    <div class=""><span class="font-size7 font-weight-bold text-white discountTagAmount">%</span> OFF</div>
                                </span>

                                <button type="button" class="button--danger lg d-block mx-auto purchaseButton">
                                    <img src="{{ asset('img/essential/coin.svg') }}" alt="coin" width="20" height="20"> <span class="amountOfCoins">{{ $board_pass->unit_price_in_coin }}</span> coins
                                </button>
                            </form>

                            <div class="py-1"></div>
                            <p class="gray2">Not enough coins? <a class="inactive blue4 underline-on-hover" href="{{ route('products.coins') }}">Top up here</a></p>

                            <div class="py-1"></div>
                            {{-- Button --}}
                            <button type="button" class="p-0 blue-underline font-size5" data-toggle="modal" data-target="#boardPassPriceChart">
                                <b>View price chart</b>
                            </button>

                            {{-- Modal --}}
                            <div class="modal fade" id="boardPassPriceChart" role="dialog">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Board Pass Price Chart</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <table class="table table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th class="text-left">Subscription Duration</th>
                                                        <th>Amount (coin <img src="{{ asset('img/essential/coin.svg') }}" width="20" height="20" alt="">)</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td class="text-left">1-{{ setting('board_pass.tier_1_threshold_in_month') }} months</td>
                                                        <td>{{ $board_pass->getBoardPassPrice(1) }}/mo</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text-left">{{ setting('board_pass.tier_1_threshold_in_month') + 1 }}-{{ setting('board_pass.tier_2_threshold_in_month') }} months</td>
                                                        <td>{{ $board_pass->getBoardPassPrice(setting('board_pass.tier_1_threshold_in_month') + 1)/(setting('board_pass.tier_1_threshold_in_month') + 1) }}/mo</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text-left">>{{ setting('board_pass.tier_2_threshold_in_month') }} months</td>
                                                        <td>{{ $board_pass->getBoardPassPrice(setting('board_pass.tier_2_threshold_in_month') + 1)/(setting('board_pass.tier_2_threshold_in_month') + 1) }}/mo</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="button--primary--inverse" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @else
                        <a href="{{ route('login') }}" class="button--primary--inverse lg">Login to continue</a>
                    @endauth
                </div>
            </div>
        </div>
    </div>

    {{-- FAQ --}}
    {{-- @php
        $qna = [
            // 'Is there any other way to subscribe to Board Pass without using coins?' => 'Yes, you can subscribe to Board Pass on monthly/annual plan for your children with a parent account.',
        ];
    @endphp
    @component('main.section.faq', ['qna' => $qna])
    @endcomponent --}}

@include('main.navs.footer')
</div>
@endsection

@section('scripts')
{{-- Moment JS --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js" integrity="sha512-qTXRIMyZIFb8iQcfjXWCO8+M5Tbc38Qi5WzdPOYZHIlZpzBHG3L3by84BBBOiRGiEb7KKtAOAs5qYdUiZiQNNQ==" crossorigin="anonymous"></script>
<script>
$(function() {
    // Reference: https://stackoverflow.com/a/4312956
    function monthDiff(dateFrom, dateTo) {
        return dateTo.getMonth() - dateFrom.getMonth() +
        (12 * (dateTo.getFullYear() - dateFrom.getFullYear()));
    }

    var $noOfSubscriptionMonths = $('.noOfSubscriptionMonths');
    var $amountOfCoins = $('.amountOfCoins');
    var $validUntilDate = $('.validUntilDate');
    var $durationInMonth = $('[name=duration_in_month]');
    var $discountTag = $('.discountTag');
    var $discountTagAmount = $('.discountTag .discountTagAmount');

    var $subscribeUntilMonth = $('[name=subscribe_until_month]');
    var $subscribeUntilYear = $('[name=subscribe_until_year]');

    var subscribeUntilMonth = $subscribeUntilMonth.find('option:selected').val();
    var subscribeUntilYear = $subscribeUntilYear.find('option:selected').val();
    var subscribeUntil;

    // Get subscribe from date
    var subscribeFromDate = moment("{{ $subscribe_from_date->format('d/m/Y') }}", "DD/MM/YYYY");

    // Update subscription_until select
    function updateSelect()
    {
        // Disable subscribe until month select
        $subscribeUntilMonth.find('option:not(:first-child)').each(function(){
            // Update datetime selects
            var subscribeUntilDate = moment("01" + "/" + $(this).val() + "/" + subscribeUntilYear, "DD/MM/YYYY");
            if( subscribeFromDate.isAfter(subscribeUntilDate) ) {
                $(this).prop('disabled', true);
            } else {
                $(this).prop('disabled', false);
            }
        });

        // Disable subscribe until year select
        $subscribeUntilYear.find('option:not(:first-child)').each(function(){
            // Update datetime selects
            var subscribeUntilDate = moment("01" + "/" + subscribeUntilMonth + "/" + $(this).val(), "DD/MM/YYYY");
            if( subscribeFromDate.isAfter(subscribeUntilDate) ) {
                $(this).prop('disabled', true);
            } else {
                $(this).prop('disabled', false);
            }
        });
    }

    updateSelect();
    updateComponents();

    // Update subscription until components
    function updateComponents() {
        // Update valid until date
        $.ajax({
            method: 'POST',
            url: '/board-pass/end-at',
            data: {
                subscribe_until_month: subscribeUntilMonth,
                subscribe_until_year: subscribeUntilYear,
            },
            success: function(date) {
                $validUntilDate.text( date );
            },
        });
        // Update no of additional months
        $.ajax({
            method: 'POST',
            url: '/board-pass/no-of-additional-subscription-months',
            data: {
                subscribe_until_month: subscribeUntilMonth,
                subscribe_until_year: subscribeUntilYear,
            },
            success: function(no_of_months) {
                $noOfSubscriptionMonths.text( no_of_months );

                // Update discount tag
                $.ajax({
                    method: 'POST',
                    url: '/board-pass/discount',
                    data: {
                        no_of_months: no_of_months,
                    },
                    success: function(discountInPercentage) {
                        if( discountInPercentage > 0 ) {
                            // Show discount tag with percentage
                            $discountTag.addClass('d-inline-block');
                            $discountTag.removeClass('d-none');
                            $discountTagAmount.text(discountInPercentage + "%");
                        } else {
                            // Hide discount tag
                            $discountTag.addClass('d-none');
                            $discountTag.removeClass('d-inline-block ');
                            $discountTagAmount.text('');
                        }
                    },
                });
            },
        });
        // Update amount of coins
        $.ajax({
            method: 'POST',
            url: '/board-pass/amount-of-coins',
            data: {
                subscribe_until_month: subscribeUntilMonth,
                subscribe_until_year: subscribeUntilYear,
            },
            success: function(amount) {
                $amountOfCoins.text( amount );
            },
        });
        updateSelect();
    }

    // Subscribe until month
    $subscribeUntilMonth.change(function(e){
        e.preventDefault();
        subscribeUntilMonth = $(this).val();
        subscribeUntil = new Date(1, subscribeUntilMonth, subscribeUntilYear);
        updateComponents();
    });

    // Subscribe until year
    $subscribeUntilYear.change(function(e){
        e.preventDefault();
        subscribeUntilYear = $(this).val();
        subscribeUntil = new Date(1, subscribeUntilMonth, subscribeUntilYear);
        updateComponents();
    });

    // Purchase Button
    $('.purchaseButton').click(function(e){
        e.preventDefault();
        var form = $(this).closest('form');
        var valid_until_date = $validUntilDate.text();
        var price_in_coins = $amountOfCoins.text();
        var duration_in_month = parseInt($noOfSubscriptionMonths.text());
        // Update duration in month input field
        $durationInMonth.val(duration_in_month);
        Swal.fire({
            title: 'Are you sure?',
            html: "Your Board Pass will be valid until <b>" + valid_until_date + "</b> with <b>" + price_in_coins + "</b> coins.",
            type: 'info',
            showCancelButton: true,
            reverseButtons: true,
            confirmButtonText: 'Confirm',
            cancelButtonText: 'Cancel',
        }).then(function(result){
            if(result.value){
                form.submit();
            }
        });
    });
});
</script>
@endsection
