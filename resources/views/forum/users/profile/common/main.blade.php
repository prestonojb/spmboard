<div class="panel p-0">

    <div class="bg-{{ $user->getProfileColor() }} text-white text-uppercase text-center font-weight-bold px-2 py-2">
        <span class="font-size4">{{ $user->account_type }}</span>
    </div>

    <div class="panel-body">
        <div class="user-profile__header">
            <img class="d-block profile-img lg mr-3" width="100" height="100" src="{{ $user->avatar }}">

            <div class="mt-1 mt-md-2">
                <div>
                    <h1 class="font-size8 d-inline-block"><b>{{ $user->name }}</b></h1>

                    @if($user->is_teacher())
                        {{-- Board Premium Badge (if any) --}}
                        @if($user->teacher->hasPremiumSubscription())
                            <a class="font-size7 dark-yellow" target="_blank" href="{{ route('classroom.prices.index') }}"><span class="iconify" data-icon="fluent:premium-20-regular" data-inline="false"></span></a>
                        @endif
                    @endif
                    {{-- Certification Badge (if any) --}}
                    @if($user->is_certified)
                        <a class="font-size7" target="_blank" href="{{ route('certification_applications.index') }}"><span class="iconify" data-icon="bx:bxs-badge-check" data-inline="false"></span></a>
                    @endif
                    @auth
                        @if(auth()->user()->id == $user->id)
                            <a href="{{ route('settings') }}" class="button--primary--inverse sm mb-1 mr-2">Edit Profile</a>
                        @endif
                    @endauth
                </div>
                @if(!is_null($user->headline))
                    <h2 class="gray2 font-size3">{{ $user->headline }}</h2>
                @endif
                @if( !is_null($user->last_online_at) )
                    <p class="sub-text">Active {{ $user->last_online_at->diffForHumans() }}</p>
                @endif

                @if($user->is_teacher())
                    <div>
                        <span class="status-tag blue5 lg mb-1 mr-1">{{ count($user->questions ?? []) }} questions, {{ count($user->answers ?? []) }} answers</span>
                        @if($user->useClassroom())
                            <a href="{{ route('classroom.landing') }}" target="_blank">
                                <span class="status-tag blue5 lg mb-1">Uses Board Classroom <i class="fas fa-external-link-alt"></i></span>
                            </a>
                        @endif
                    </div>
                @endif

                @if($user->is_teacher())
                    @if($user->teacher->hasPremiumSubscription())
                        <div>
                            {{-- Facebook --}}
                            <a @if(!is_null($user->teacher->facebook_url)) target="_blank" @endif href="{{ !is_null($user->teacher->facebook_url) ? $user->teacher->facebook_url : 'javascript:void(0)' }}" class="mr-1 @if(is_null($user->teacher->facebook_url)) disabled @endif">
                                <span class="gray2 font-size7"><i class="fab fa-facebook"></i></span>
                            </a>
                            {{-- Instagram --}}
                            <a @if(!is_null($user->teacher->instagram_url)) target="_blank" @endif href="{{ !is_null($user->teacher->instagram_url) ? $user->teacher->instagram_url : 'javascript:void(0)' }}" class="mr-1 @if(is_null($user->teacher->instagram_url)) disabled @endif">
                                <span class="gray2 font-size7"><i class="fab fa-instagram"></i></span>
                            </a>
                            {{-- Youtube --}}
                            <a @if(!is_null($user->teacher->youtube_url)) target="_blank" @endif href="{{ !is_null($user->teacher->youtube_url) ? $user->teacher->youtube_url : 'javascript:void(0)' }}" class="mr-1 @if(is_null($user->teacher->youtube_url)) disabled @endif">
                                <span class="gray2 font-size7"><i class="fab fa-youtube"></i></span>
                            </a>
                        </div>
                    @else
                        <p class="gray2 py-1">Want to show your social links? <a href="{{ route('classroom.prices.index') }}" target="_blank">Learn More about Board Premium</a></p>
                    @endif
                @endif
            </div>
        </div>
    </div>
</div>
