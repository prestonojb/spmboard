<div class="panel text-dark mb-3">
    <div class="question-block__header">
        <ul class="question-block__tags">
            @if( $question->chapters->count() )
                @foreach($question->chapters as $chapter)
                    <li>
                        <a href="javascript:void(0)">{{ $chapter->name }}</a>
                    </li>
                @endforeach
            @else
                <li>
                    General
                </li>
            @endif
        </ul>
        <ul class="nav question-block__icon-group mb-1">
            @if( !$question->is_public )
                <button type="button" class="button--primary--inverse sm eclipse mr-1" disabled>
                    Private
                </button>
            @endif
            <button class="question-block__check @if($question->answered) checked @endif m-0" data-toggle="popover" data-trigger="focus" data-placement="top" data-content="Lights up when question has an accepted answer.">
                <i class="far fa-check-circle"></i>
            </button>
        </ul>
    </div>
    <div class="question__user-header">
        <div class="question-block__user-info">
            <a href="javascript:void(0)">
                <img class="avatar" src="{{ $question->user->avatar ?? asset('img/essential/no-profile-picture.jpg') }}">
            </a>
            <div class="mt-1 text">
                <div class="user-info"><a class="username" href="javascript:void(0)">{{ $question->user->username }}</a></div>
                <span class="time">Asked {{ $question->created_at->longRelativeDiffForHumans() }}</span>
            </div>
        </div>
    </div>

    <h1 class="question__title">{{ $question->question }}</h1>
    <div class="question__description">{!! $question->description !!}</div>

    @if($question->img)
    <div class="question__image-container">
        <img src="{{ $question->img }}" class="question-img">
    </div>
    @endif

    <div class="question-block__button-group">
        <button type="button" class="answer-button"><span class="icon"></span> <span class="text">Answer</span> · <span class="answer-count">{{ $question->get_answers_from_classroom_count( $classroom ) }}</span></button>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="editHistoryModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                <h5 class="modal-title">Edit history</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                </div>
                <div class="modal-body">
                    @foreach($question->revisionHistory as $history)
                        <div class="edit-history-item py-1">
                            <a href="javascript:void(0)">
                                {{ $history->userResponsible()->username }} </a>
                                changed {{ $history->fieldName() }} from <span class="text-white bg-danger">{{ $history->oldValue() }}</span>
                                to <span class="text-white bg-success">{{ $history->newValue() }}</span>
                            <span class="float-right">
                                <small>
                                    {{ $history->created_at->diffForHumans() }}
                                </small>
                            </span>
                        </div>
                    @endforeach
                </div>
                <div class="modal-footer">
                    <button type="button" class="button--primary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

</div>
