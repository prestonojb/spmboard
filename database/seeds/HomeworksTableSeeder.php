<?php

use App\Homework;
use App\HomeworkSubmission;
use App\Lesson;
use App\Resource;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class HomeworksTableSeeder extends Seeder
{
    protected $NO_OF_HOMEWORKS = 100;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Homework::truncate();
        HomeworkSubmission::truncate();
        $faker = \Faker\Factory::create();

        // Create Homeworks
        for($i = 0; $i < $this->NO_OF_HOMEWORKS; $i++) {
            $lesson = Lesson::inRandomOrder()->first();
            $classroom = $lesson->classroom;

            $subject = $classroom->subject;
            $chapter = $subject->chapters->random()->first();
            // Get shuffled resource IDs array
            $resource_ids = Resource::getIdsBySubject( $subject );

            $due_at = Carbon::createFromTimeStamp($faker->dateTimeBetween('+1 day', '+1 month')->getTimestamp());

            $homework = Homework::create([
                            'title' => $faker->sentence(3),
                            'instruction' => $faker->paragraph,
                            'due_at' => $due_at->format('Y-m-d H:i:s'),
                            'lesson_id' => $lesson ? $lesson->id : null,
                            'chapter_id' => $chapter ? $chapter->id : null,
                            'max_marks' => rand(0, 100),
                        ]);

            // Attach resources
            for($j = 0; $j <= rand(0, 2); $j++) {
                $resource_id = $resource_ids[ array_rand($resource_ids) ];
                // Add resources to homework
                $homework->resources()->syncWithoutDetaching([
                    // Pop a random resource ID from resource IDs array
                    $resource_ids[ array_rand($resource_ids) ]
                ]);
                unset($resource_ids[ array_rand($resource_ids) ]);
            }


            $student = !$classroom->students->isEmpty() ? $classroom->students->random() : null;

            if( !is_null($student) ) {
                $graded_at = $due_at->addDays( rand(1, 50) );
                $returned_at = $graded_at;

                // Add submission to homework
                $homework->submissions()->create([
                    'student_id' => $student->user_id,
                    'file_url' => 'https://www.w3.org/WAI/ER/tests/xhtml/testfiles/resources/pdf/dummy.pdf',
                    'comment' => $faker->optional()->sentence,
                    'marks' => rand( 0, $homework->max_marks ),
                    'marked_file_url' => 'https://www.w3.org/WAI/ER/tests/xhtml/testfiles/resources/pdf/dummy.pdf',
                    'teacher_remark' => $faker->optional()->paragraph,
                    'graded_at' => $graded_at,
                    'returned_at' => $returned_at,
                ]);
            }
        }
    }
}
