<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Http\Request;
use App\Notifications\ReferralReward;
use Illuminate\Support\Facades\Notification;
use Amplitude;
use App\ExamBoard;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {
        return view('auth.login');
    }

    /**
     * This function is run after user is authenticated
     */
    public function authenticated(Request $request, $user)
    {
        $exam_board_slug = !is_null($user->exam_board) ? $user->exam_board->slug : "igcse";
        return redirect()->intended($exam_board_slug);
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Redirect the user to the Google authentication page.
    *
    * @return \Illuminate\Http\Response
    */
    public function redirectToProvider()
    {
        request()->session()->put('account_type', request()->account_type);
        request()->session()->put('referral_code', request()->referral_code);
        return Socialite::driver('google')->redirect();
    }

    /**
     * Obtain the user information from Google.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback()
    {
        try {
            $user = Socialite::driver('google')->user();
        } catch (\Exception $e) {
            return redirect()->route('login')->withError('Signing in with Google failed. Please try again.');
        }

        $existingUser = User::where('email', $user->email)->first();
        if($existingUser){
            auth()->login($existingUser, true);
        } else {
            // Referral Code VALIDATION
            // Add RP if referral code is valid
            if( request()->has('referral_code') && !is_null(request('referral_code')) ) {
                $referral = User::getByReferralCode( request('referral_code') );
                if(is_null($referral)) {
                    return redirect()->withErrors(['referral_code', 'Referral Code is invalid.']);
                }
            } else {
                $referral = null;
            }

            // Account type VALIDATION
            $account_type = request()->session()->get('account_type');

            $account_types = ['teacher', 'student',  'parent'];
            if( !in_array($account_type, $account_types) ) {
                return redirect('/register')->withError('Signing in with Google failed. Please select the account type you want to create before proceeding.');
            }

            // Create user
            $newUser                  = new User;
            $newUsername = explode('@', $user->email)[0];
            $uniqueNewUsername        = $this->makeUnique($newUsername);
            $newUser->username        = $uniqueNewUsername;

            $newUser->name              = $user->name;
            $newUser->email             = $user->email;
            $newUser->email_verified_at = now();
            $newUser->google_id         = $user->id;
            $newUser->avatar            = $user->avatar;
            $newUser->avatar_original   = $user->avatar_original;
            $newUser->save();

            // Reward point reward for first # new users
            // $promotion_is_available = setting('marketing.rp_promotion_slots_available') > 0;

            // if( $promotion_is_available ) {
            //     $newUser->reward_point_balance += 100;
            //     $newUser->save();
            //     // Decrement RP promotion slots by 1
            //     DB::table('settings')->where('key', 'marketing.rp_promotion_slots_available')->decrement('value', 1);
            //     // Flash message to user
            //     Session::flash('rp_promotion_awarded', 'You are awarded '. setting('marketing.rp_promotion_amount') .' Reward Points for signing up!');
            // }

            $newUser->createAccountType( $account_type );
            auth()->login($newUser, true);

            // Add RP if there is referral
            if(!is_null($referral)) {
                // Referral/Referee
                $referee = User::find($newUser->id);
                try {
                    // Add RP to referral/Notify
                    $referral->addRewardPointAction("Refer a friend", setting('marketing.referral_rp_amount'));
                    Notification::send($referral, new ReferralReward($referral, $referee, true));
                } catch (\Exception $e) {}
                try {
                    // Add RP to referee/Notify
                    $referee->addRewardPointAction('Getting referred by a friend', setting('marketing.referral_rp_amount'));
                    Notification::send($referee, new ReferralReward($referral, $referee, false));
                } catch (\Exception $e) {}
            }

            // Log event on Amplitude
            Amplitude::setUserId($newUser->id);
            Amplitude::setUserProperties([
                'email' => $newUser->email,
                'account_type' => $newUser->getUserAccountType(),
            ]);
            Amplitude::queueEvent('register', ['method' => 'google']);

            return redirect( $newUser->getGettingStartedPath() );
        }

        return redirect()->intended('login/choose_redirect');
    }

    /*
     *
     * Make username unique for users logging in with Google
     * Get unique username by adding counter at the end of the string
     *
     * Repeat with counter + 1 if username with counter is not unique
     *
     */
    public function makeUnique($newUsername, $counter = 0)
    {
        //Non-first iteration
        if($counter != 0){
            // Check if user with this username exists
            $newUsernameExists = User::where('username', '=', $newUsername)->exists();

            if(!$newUsernameExists){
                return $newUsername;
            } else{
                //Remove counter with counter + 1
                $newUsername = substr($newUsername, 0, -1);
                $newUsername .= "$counter";
                $counter += 1;
                return $this->makeUnique($newUsername, $counter);
            }

        } else{
            // Check if user with this username exists
            $newUsernameExists = User::where('username', '=', $newUsername)->exists();

            if(!$newUsernameExists){
                // return 'norepeat';
                return $newUsername;
            } else{
                //Add 0 to end of string and check if username is unique
                $newUsername .= "$counter";
                $counter += 1;
                return $this->makeUnique($newUsername, $counter);
            }
        }
    }
}
