<?php

use App\Answer;
use Illuminate\Database\Seeder;

class AnswersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $answers = [
        //     ['question_id' => 1, 'user_id' => 2, 'answer' => 'The distance formula is derived from the Pythagorean theorem.', 'checked' => 1],
        //     ['question_id' => 2, 'user_id' => 2, 'answer' => 'The number D = b2 – 4ac determined from the coefficients of the equation ax2 + bx + c = 0. The discriminant reveals what type of roots the equation has.', 'checked' => 0],
        //     ['question_id' => 3, 'user_id' => 1, 'answer' => 'The distance formula is derived from the Pythagorean theorem.', 'checked' => 1],
        // ];

        // foreach($answers as $answer){
        //     Answer::create($answer);
        // }

        Answer::create( [
            'id'=>1,
            'question_id'=>1,
            'user_id'=>'1',
            'answer'=>'Physical quantity is a quantity that is measurable. \r\nExamples of physical quantity: length, time, temperature etc.\r\n\r\nIt can be measured with a measuring instrument.\r\nExamples of measuring instrument: ruler, stopwatch, thermometer etc.\r\n',
            'img'=>NULL,
            'checked'=>1
            ] );
            
            
                        
            Answer::create( [
            'id'=>2,
            'question_id'=>2,
            'user_id'=>'1',
            'answer'=>'Base quantities are physical quantities that cannot be defined by other physical quantities.\r\n\r\nDerived quantities are physical quantities that is either a product or ratio of a combination of other physical quantities.',
            'img'=>NULL,
            'checked'=>1
            ] );
            
            
                        
            Answer::create( [
            'id'=>4,
            'question_id'=>4,
            'user_id'=>'1',
            'answer'=>'An error is an uncertainty in a measurement.\r\n',
            'img'=>NULL,
            'checked'=>1
            ] );
            
            
                        
            Answer::create( [
            'id'=>5,
            'question_id'=>5,
            'user_id'=>'1',
            'answer'=>'Systematic errors are errors that are carried forward to every measurement if not corrected.\r\nExamples of systematic errors: zero error, wrong technique of taking measurements\r\n\r\nRandom errors are errors that can be minimized, but not prevented.\r\nExamples of random errors: change in surroundings(wind movement, temperature), parallax error',
            'img'=>NULL,
            'checked'=>1
            ] );
            
            
                        
            Answer::create( [
            'id'=>6,
            'question_id'=>6,
            'user_id'=>'1',
            'answer'=>'Accuracy is the degree of closeness of the measured value to the actual value. High accuracy represents that the measured value is very close to the actual value.\r\n\r\nConsistency is the ability to register the same reading when multiple readings are taken. High consistency represents that multiple measured values taken are very close to each other.\r\n\r\nSensitivity is the ability of the apparatus to sense a small change in the measurement. High sensitivity represents that the a',
            'img'=>NULL,
            'checked'=>0
            ] );
            
            
                        
            Answer::create( [
            'id'=>7,
            'question_id'=>3,
            'user_id'=>'1',
            'answer'=>'Similarity\r\nScalar and vector quantities are both physical quantities.\r\n\r\nDifference\r\nScalar quantities are physical quantities that have magnitude ONLY.\r\nExamples: time, mass, work etc.\r\n\r\nVector quantities are physical quantities that have both magnitude and direction.\r\nExamples: displacement, velocity, momentum etc.',
            'img'=>NULL,
            'checked'=>1
            ] );
            
            
                        
            Answer::create( [
            'id'=>8,
            'question_id'=>7,
            'user_id'=>'1',
            'answer'=>'Accuracies of the apparatus are as follows:\r\nMetre rule: 0.1 cm\r\nVernier callipers: 0.01 cm\r\nMicrometer screw gauge: 0.01 mm',
            'img'=>NULL,
            'checked'=>1
            ] );
            
            
                        
            Answer::create( [
            'id'=>9,
            'question_id'=>8,
            'user_id'=>'1',
            'answer'=>'Similarity\r\nSpeed and velocity have the same unit(m s^-1).\r\n\r\nDifferences\r\nSpeed is a scalar quantity.\r\nVelocity is a vector quantity.\r\n\r\nSpeed is the rate of change of distance.\r\nVelocity is the rate of change of displacement.',
            'img'=>NULL,
            'checked'=>1
            ] );
            
            
                        
            Answer::create( [
            'id'=>10,
            'question_id'=>10,
            'user_id'=>'1',
            'answer'=>'Displacement(y-axis) of the object for any given time(x-axis) is the y-axis value. \r\n\r\nVelocity of the object between any two time intervals is the gradient between the two respective points(x-axis).',
            'img'=>NULL,
            'checked'=>1
            ] );
            
            
                        
            Answer::create( [
            'id'=>11,
            'question_id'=>11,
            'user_id'=>'1',
            'answer'=>'Distance travelled by the object is the area bounded by the graph and the x-axis.\r\n\r\nVelocity of the object at any given time(x-value) is the corresponding y-value.\r\n\r\nAcceleration of the object at any two time intervals is the gradient between the two corresponding x-values(time).',
            'img'=>NULL,
            'checked'=>1
            ] );
            
            
                        
            Answer::create( [
            'id'=>12,
            'question_id'=>12,
            'user_id'=>'1',
            'answer'=>'Distance travelled by the object is obtained by calculating the area of the v-t graph regardless of the position of the area.\r\n\r\nDisplacement of the object is also obtained by calculating the area of the v-t graph, BUT area above x-axis is positive displacement while area below x-axis is negative displacement.',
            'img'=>NULL,
            'checked'=>0
            ] );
            
            
                        
            Answer::create( [
            'id'=>13,
            'question_id'=>13,
            'user_id'=>'1',
            'answer'=>'The four equations are:\r\nv = u + at\r\ns = (1/2)(u+v)t\r\ns = ut + (1/2)at^2\r\nv^2 = u^2 + 2as\r\nwhere \r\ns = displacement of the object,\r\nu = initial velocity of the object,\r\nv = final velocity of the object,\r\na = acceleration of the object,\r\nt = time of motion.',
            'img'=>NULL,
            'checked'=>1
            ] );
            
            
                        
            Answer::create( [
            'id'=>14,
            'question_id'=>14,
            'user_id'=>'1',
            'answer'=>'Inertia is NOT a physical quantity.\r\n\r\nInertia is the property of an object which resists the change in motion.\r\nA stationary object will tend to remain stationary.\r\nA moving object will tend to move in a straight line at constant velocity.',
            'img'=>NULL,
            'checked'=>1
            ] );
            
            
                        
            Answer::create( [
            'id'=>15,
            'question_id'=>15,
            'user_id'=>'1',
            'answer'=>'Mass is the only factor that affects inertia. \r\nThe greater the mass, the higher the inertia.',
            'img'=>NULL,
            'checked'=>1
            ] );
    }
}
