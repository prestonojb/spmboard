<div class="user-profile__header">
    <div>
        <img class="profile-img mr-lg-3" src="{{ $student->avatar }}">
    </div>
    <div class="mt-1 mt-md-2">
        <div class="">
            <h1 class="heading d-inline-block mb-1 mr-2">{{ $student->username }}</h1>
        </div>
        <h2 class="sub-heading">{{ $student->about }}</h2>
        <p class="sub-text">Active {{ $student->last_online_at->shortRelativeDiffForHumans() }}</p>
    </div>
    <div>
        {{-- <a href="#" class="button--primary--inverse">Top Up Coins</a> --}}
    </div>
</div>
