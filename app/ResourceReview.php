<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ResourceReview extends Model
{
    protected $guarded = [];

    /**
     * ==================
     * RELATIONSHIPS
     * ==================
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function resource()
    {
        return $this->belongsTo(Resource::class);
    }

    /**
     * ==================
     * HELPERS
     * ==================
     */
}
