@component('mail::message')
# Weekly Report
## Start of Week: {{ $start_of_week }}
## End of Week: {{ $end_of_week }}
### Generated at: {{ $generated_at }}
*All metrics reflect data for the past week only.*

## Questions
@component('mail::table')
| Metrics | Value |
|:--|:--|
| Number of Questions Asked | {{ $data['no_of_questions_asked'] }} |
| Number of Questions Answered | {{ $data['no_of_questions_answered'] }} |
| Number of Questions with Accepted Answer | {{ $data['no_of_questions_with_accepted_answer'] }} |
| Number of Questions Recommended | {{ $data['no_of_recommended_questions'] }} |
@endcomponent

------

## Answers
@component('mail::table')
| Metrics | Value |
|:--|:--|
| Number of Answers Accepted | {{ $data['no_of_accepted_answers'] }} |
| Number of Answers Recommended | {{ $data['no_of_recommended_answers'] }} |
@endcomponent

------

## Users
@component('mail::table')
| Metrics | Value |
|:--|:--|
| Number of new Users | {{ $data['no_of_users'] }} |
| Number of new SPM Users | {{ $data['no_of_spm_users'] }} |
| Number of new IGCSE Users | {{ $data['no_of_igcse_users'] }} |
@endcomponent

------

## Views
@component('mail::table')
| Metrics | Value |
|:--|:--|
| Number of Views | {{ $data['no_of_views'] }} |
@endcomponent

------

## Reward Points
@component('mail::table')
| Metrics | Value |
|:--|:--|
| Given for Login Reward | {{ $data['rp_given_for_login_reward'] }} |
| Given for Recommended Question | {{ $data['rp_given_for_recommended_question'] }} |
| Given for Accepted Answer | {{ $data['rp_given_for_accepted_answer'] }} |
| Given for Recommended Answer | {{ $data['rp_given_for_recommended_answer'] }} |
| Spent on Asking Questions | {{ $data['rp_spent_on_asking_questions'] }} |
@endcomponent

------

## Coins
@component('mail::table')
| Metrics | Value |
|:--|:--|
| Spent on Notes | {{ $data['coins_spent_on_notes'] }} |
| Spent on Topical Past Paper | {{ $data['coins_spent_on_tpp'] }} |
| Bought at Store | {{ $data['coins_bought_at_store'] }} |
@endcomponent

@endcomponent
