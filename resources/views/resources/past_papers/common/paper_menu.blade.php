<div class="accordion custom-accordion-menu" id="pastPaperMenu">
    <div class="card">
        {{-- Set Papers --}}
        <div class="card-header active">
            <button class="p-0 text-left stretched-link" type="button" data-toggle="collapse" data-target="#pastPaperSetPaperMenu" aria-expanded="true" aria-controls="collapseOne">
                <h5 class="mb-0">
                    Set Papers
                </h5>
                <div class="arrow-icon">
                    <i class="fas fa-chevron-right"></i>
                </div>
            </button>
        </div>

        <div id="pastPaperSetPaperMenu" class="collapse show" aria-labelledby="headingOne" data-parent="#pastPaperMenu">
            <div class="card-body">
                @foreach($current_paper->getSetPapers() as $paper)
                    <a href="{{ $paper->qp_route }}" class="@if($paper->id == $current_paper->id)) active @endif">Paper {{ $paper->paper_number }}</a>
                @endforeach
            </div>
        </div>
    </div>
</div>
