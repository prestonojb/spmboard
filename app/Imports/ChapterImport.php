<?php

namespace App\Imports;

use App\Chapter;
use App\Notes;
use App\Subject;
use Illuminate\Support\Collection;
use Illuminate\Validation\Rule;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithStartRow;

class ChapterImport implements ToCollection, WithHeadingRow, WithValidation, WithStartRow
{
    /**
    * @param Collection $collection
    */
    public function collection(Collection $rows)
    {
        foreach($rows as $row)
        {
            $subject = Subject::find($row->get('subject_id'));

            // Create chapters
            $subject->chapters()->create([
                'name' => $row->get('name'),
                'description' => $row->get('description'),
                'chapter_number' => $row->get('chapter_number'),
            ]);
        }
    }


    public function rules(): array
    {
        return [
            'subject_id' => Rule::in( Subject::all()->pluck('id')->toArray() ),
            'name' => 'required|string',
            'description' => 'required|string',
            'chapter_number' => 'required|integer',
        ];
    }

    /**
     * Indicate Heading row
     * 1st row is heading row
     */
    public function headingRow(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function startRow(): int
    {
        return 3;
    }
}
