<div class="panel p-0 mb-3">
    <div class="panel-header m-0">
        {{ $title }}
        @if(isset($subtitle) && !is_null($subtitle))
            <span class="font-weight-normal gray2 font-size1">{{ $subtitle }}</span>
        @endif
    </div>
    <div class="panel-body p-0">
        @if( $collection->count() )
            @if( $type == 'student' )
                @foreach($collection as $item)
                    <div class="position-relative @if(!$loop->last) border-bottom @endif p-2 d-flex justify-content-between align-items-center">
                        @component('classroom.common.student_avatar', ['student' => $item, 'classroom' => $classroom])
                        @endcomponent

                        <div>
                            @can('view_student', $classroom)
                                <a href="{{ $item->url( $classroom ) }}" class="button--primary sm">View</a>
                            @endcan
                            @can('remove_student', $classroom)
                                <form class="d-inline-block" action="{{ route('classroom.classrooms.students.remove', $classroom->id) }}" method="POST">
                                    @csrf
                                    <input type="hidden" name="student_user_id" value="{{ $item->user_id }}">
                                    <button type="submit" class="button--danger sm deleteButton">Remove</button>
                                </form>
                            @endcan
                        </div>
                    </div>
                @endforeach
            @else
                @foreach($collection as $item)
                    <div class="position-relative @if(!$loop->last) border-bottom @endif p-2 white-hoverable">
                        <a href="{{ $item->url }}" class="stretched-link"></a>
                        <h4 class="font-weight500 mb-1">{{ $item->primary }}</h4>
                        <p class="font-size1 gray2 mb-0">{!! $item->secondary !!}</p>
                    </div>
                @endforeach
            @endif
        @else
            <div class="py-2">
                <p class="py-2 text-center gray2 mb-0">No {{ $type }} found</p>
            </div>
        @endif
        @if( isset($url) && !is_null($url) )
            <a class="d-block p-2 hover-on-underline border-top" href="{{ $url }}">Show more {{ $type.'s' }} ></a>
        @endif
    </div>
</div>
