<div class="modal fade" id="answeringGuidelineModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Answering Guidelines</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                @php
                    $guidelines = [
                        'Answer should make the forum more helpful' => 'Incorrect, outdated or misleading answers are not helpful. Good answers should provide explanation to the question. Any opinions or advice given in the answer should be supported with reasoning for the reader’s understanding. We wish to make the forum a place whereby everyone could find the relevant answers to solve their academic problem. The answer can be “try this method instead” rather than “no point solving this stupid question” as it would demotivate your peers.',
                        'Write an organised answer' => 'Here’s how you can write a well formatted answer for your peers:
                                                        <li class="font-size-inherit">Clear and easy to understand answers</li>
                                                        <li class="font-size-inherit">Use paragraph to break up large sentence into a few block which improve readability</li>
                                                        <li class="font-size-inherit">Put relevant spacing between each rows</li>
                                                        <li class="font-size-inherit">Bold or italic to emphasise any important points. Try not to use block capitals too much in the answer</li>
                                                        <li class="font-size-inherit">Use bullet points when necessary</li>
                                                        <li class="font-size-inherit">Give personal opinion and examples that are helpful</li>
                                                        <li class="font-size-inherit">Attach relevant image if necessary</li>',
                        'Build a community' => 'There’s still no answer for the question? Try your best to help your peers by doing some research! You might end up learning something new which will help you in the future. In this forum, we shall remember that learning never comes to an end. Any answer that gets the asker going in the right direction is helpful, but do try to mention any limitations, assumptions or simplifications in your answer. Brevity is acceptable, but fuller explanations are better.
                                                We encourage discussion within the question itself and correct any other peers that might have answer the question incorrectly, in a nice and polite manner of course.',
                        'Answer in the context of the syllabus' => 'The answers given shall be relevant to the latest syllabus and should be quoted in the answers if they’re out of the scope. We understand that sometimes certain question might be a little challenging for a given syllabus as teachers tend to provide such question to assess the students’ capability. Nevertheless, if the answer required knowledge that is outside of the syllabus, please indicate in the answer.',
                        'Polite and Have Fun!' => 'We do not expect all answers to be perfect and it’s fine to disagree and express your concern. Nevertheless, please do not scold or use any vulgar language during answering. Try to think in other shoes, not everyone are smart and there will be some peers that need actual help. Be polite and kind will allow the community to grow and help even more in the future!',
                    ];
                @endphp
                <div id="guidelineAccordion">
                    @foreach($guidelines as $title => $description)
                        <div class="">
                            <div class="mb-1" id="headingOne">
                                <h5 class="mb-0">
                                    <button class="w-100 text-left border-bottom" data-toggle="collapse" data-target="#{{ str_slug($title) }}_guideline" aria-expanded="true" aria-controls="collapseOne">
                                        {{ $loop->index + 1 }}. {{ $title }}
                                    </button>
                                </h5>
                            </div>
                            <div id="{{ str_slug($title) }}_guideline" class="collapse" aria-labelledby="headingOne" data-parent="#guidelineAccordion">
                                <div class="pl-3 gray2 font-size1 mb-1">
                                    {!! $description !!}
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="button--primary" data-dismiss="modal">Got it!</button>
            </div>
        </div>
    </div>
  </div>
