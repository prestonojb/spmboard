<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\BoardPassSubscription;
use App\ExamBoard;
use App\Student;
use App\StudentParent;
use App\User;
use Faker\Generator as Faker;

$factory->define(Student::class, function (Faker $faker) {
    return [
        'coin_balance' => 100,
        'reward_point_balance' => 1000,
        'school' => $faker->company,
        'parent_id' => function() {
            return rand(0, 1) > 0.5 ? factory(StudentParent::class)->create()->id
                                    : null;
        },
        'exam_board_id' => rand(0,1) > 0.5 ? ExamBoard::first()->id : null,
    ];
});

// Student with Board Pass
$factory->afterCreatingState(Student::class, 'with_board_pass_subscription', function($student, $faker) {
    factory(BoardPassSubscription::class)->create([
        'user_id' => $student->user_id
    ]);
});
