<form action="{{ url()->current() }}" method="get">
    @csrf
    <div class="filter-container">
        <div class="select-container">
            <select name="issued_days_ago" id="" class="filter-select--lg">
                <option value="" disabled selected>Date Issued</option>
                <option value="0">Today</option>
                <option value="1" @if( request()->issued_days_ago == 1 ) selected @endif>Yesterday</option>
                @foreach([3, 7, 30, 90, 365] as $day)
                    <option value="{{ $day }}" @if( request()->issued_days_ago == $day ) selected @endif>Past {{ $day }} days</option>
                @endforeach
            </select>

            <select name="amount" class="filter-select--md" id="">
                <option value="" disabled selected>Amount</option>
                @foreach( range(0, 1000, 200) as $i )
                    <option value="{{ $i }}" @if( isset(request()->amount) && $i == request()->amount ) selected @endif>
                        @if( $loop->first )
                            MYR {{ $i.' - '.($i + 200) }}
                        @else
                            MYR {{ ($i+1).' - '.($i + 200) }}
                        @endif
                    </option>
                @endforeach
                <option value="1001" @if( 1001 == request()->amount ) selected @endif>> MYR 1000</option>
            </select>
        </div>

        <div class="filter-buttons">
            <button type="button" class="button-link underline-on-hover mr-1 clearFilterButton">Clear</button>
            <button type="submit" class="button--primary">Apply Filter</button>
        </div>

    </div>
</form>
