<?php

namespace App\Http\Controllers;

use App\Classroom;
use App\ClassroomPost;
use App\Lesson;
use App\Notifications\Classroom\NewClassroomPostAdded;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class ClassroomClassroomPostController extends Controller
{
    /**
     * List all posts in classroom
     */
    public function index(Classroom $classroom)
    {
        $this->authorize('index_post', $classroom);

        $posts = $classroom->posts()->latest()->paginate(30);
        return view('classroom.posts.index', compact('classroom', 'posts'));
    }

    public function create(Classroom $classroom)
    {
        $this->authorize('store_post', $classroom);
        return view('classroom.posts.create', compact('classroom'));
    }

    /**
     * Create post in classroom
     */
    public function store(Classroom $classroom)
    {
        $this->authorize('store_post', $classroom);
        request()->validate([
            'title' => 'required',
            'description' => 'required',
            'categories' => 'required|array|min:1|in:lesson,resource,homework',
            'type' => 'required',
        ]);

        $user = auth()->user();

        $notify_parents = isset(request()->notify_parents) && request('notify_parents');

        // Create lesson
        $post = $user->classroom_posts()->create([
                    'title' => request('title'),
                    'description' => request('description'),
                    'classroom_id' => $classroom->id,
                    'categories' => request('categories'),
                    'type' => request('type'),
                ]);

        // New Classroom Post Added Notification
        foreach( $classroom->students as $student ) {
            // Notify student
            $student->notify( new NewClassroomPostAdded( $student, $post ) );
            // Notify student's parent if selected and student has parent
            if( $notify_parents && $student->has_parent() ) {
                $student->parent->notify( new NewClassroomPostAdded( $student, $post ) );
            }
        }

        Session::flash('success', 'Post added successfully!');
        return redirect()->route('classroom.posts.index', $classroom->id);
    }


    // /**
    //  * Show post in classroom
    //  */
    // public function show(Classroom $classroom, ClassroomPost $post)
    // {
    //     $this->authorize('show', $post);

    //     return view('classroom.posts.show', compact('classroom', 'post'));
    // }


    /**
     * Edit post
     */
    public function edit(Classroom $classroom, ClassroomPost $post)
    {
        $this->authorize('update', $post);
        return view('classroom.posts.edit', compact('classroom', 'post'));
    }

    /**
     * Update post
     */
    public function update(Classroom $classroom, ClassroomPost $post)
    {
        $this->authorize('update', $post);
        request()->validate([
            'title' => 'required',
            'description' => 'required',
            'categories' => 'required|array|min:1|in:lesson,resource,homework,others',
            'type' => 'required',
        ]);

        // Create lesson
        $post->update([
            'title' => request('title'),
            'description' => request('description'),
            'classroom_id' => $classroom->id,
            'categories' => request('categories'),
            'type' => request('type'),
        ]);

        Session::flash('success', 'Post updated successfully!');
        return redirect()->route('classroom.posts.index', $classroom->id);
    }

    /**
     * Delete post
     */
    public function destroy(Classroom $classroom, ClassroomPost $post)
    {
        $this->authorize('delete', $post);

        $post->delete();

        Session::flash('error', 'Post deleted successfully!');
        return redirect()->route('classroom.posts.index', $classroom->id);
    }
}
