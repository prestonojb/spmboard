<?php

namespace App\Http\Controllers;

use App\Classroom;
use App\Student;
use Illuminate\Http\Request;

class ClassroomChildrenController extends Controller
{
    /**
     * List all parent's children
     */
    public function index()
    {
        $user = auth()->user();
        $parent = $user->parent;
        $children = $parent->children;

        $timeline_items = $user->getTimelineItems();

        return view('classroom.children.index', compact('parent', 'children', 'timeline_items'));
    }
}
