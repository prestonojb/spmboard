@extends('classroom.layouts.app')

@section('meta')
    <title>Calendar | {{ config('app.name') }} Classroom</title>
@endsection


@section('styles')
<link rel="stylesheet" type="text/css" href="https://uicdn.toast.com/tui-calendar/latest/tui-calendar.css" />

<!-- If you use the default popups, use this. -->
<link rel="stylesheet" type="text/css" href="https://uicdn.toast.com/tui.date-picker/latest/tui-date-picker.css" />
<link rel="stylesheet" type="text/css" href="https://uicdn.toast.com/tui.time-picker/latest/tui-time-picker.css" />

@endsection

@section('content')

@component('classroom.common.breadcrumbs')
    <a href="{{ route('classroom.landing') }}">Home</a> /
    Calendar
@endcomponent

{{-- Page Header --}}
@component('classroom.common.page_heading', ['title' => 'Calendar'])
@endcomponent

{{-- Top Section --}}
<div class="d-flex flex-wrap justify-content-between align-items-center mb-2">
    {{-- Class Selected --}}
    <select name="class_selected" id="" class="maxw-250px">
        <option value="" class="border-bottom" selected @if( !isset(request()->class_selected) ) selected @endif>All Classes</option>
        @foreach($classrooms as $room)
            <option value="{{ $room->id }}" @if( request()->has('class_selected') && $room->id == request('class_selected') ) selected @endif>{{ $room->name }}</option>
        @endforeach
    </select>
</div>

{{-- Labels --}}
<div class="d-flex justify-content-start align-items-center mb-2">
    @php
        $types = ['Lesson', 'Homework'];
        $bgClasses = ['bg-blue2', 'bg-light-red'];
    @endphp
    @for($i=0; $i < count($types); $i++)
        <div class="d-flex justify-content-between align-items-center maxw-100px">
            <div class="text-center mw-30px">
                <span class="square-dot {{ $bgClasses[$i] }}"></span>
            </div>
            <div class="text-center flex-1" >
                {{ $types[$i] }}
            </div>
        </div>
    @endfor
</div>

{{-- Calendar Menu --}}
<div id="calendarMenu" class="py-2">
    <span id="menu-navi" class="menu-navi">
        <button type="button" class="calendar-btn calendar-move-today">Today</button>
        <button type="button" class="calendar-btn calendar-move-day prev"><i class="calendar-icon ic-arrow-line-left"></i></button>
        <button type="button" class="calendar-btn calendar-move-day next"><i class="calendar-icon ic-arrow-line-right"></i></button>
    </span>
    <span class="font-weight-normal calendar-render-range"></span>
</div>

{{-- Calendar --}}
<div class="panel p-0">
    <div id="calendar" style="height:800px;"></div>
</div>

<div class="py-2"></div>

@endsection

@section('scripts')
<script src="https://uicdn.toast.com/tui.code-snippet/latest/tui-code-snippet.js"></script>
<script src="https://uicdn.toast.com/tui.dom/v3.0.0/tui-dom.js"></script>
<script src="https://uicdn.toast.com/tui.time-picker/latest/tui-time-picker.min.js"></script>
<script src="https://uicdn.toast.com/tui.date-picker/latest/tui-date-picker.min.js"></script>
<script src="https://uicdn.toast.com/tui-calendar/latest/tui-calendar.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.20.1/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/chance/1.0.13/chance.min.js"></script>
<script>
// Format Date
function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2)
        month = '0' + month;
    if (day.length < 2)
        day = '0' + day;

    return [year, month, day].join('/');
}

// Set date boundaries
var lbDate = "{{ $lb_date->format('Y-m-d') }}";
var date_arr = lbDate.split('-');
lbDate = new Date(date_arr[0], date_arr[1] - 1, date_arr[2]);

var ubDate = "{{ $ub_date->format('Y-m-d') }}";
var date_arr = ubDate.split('-');
ubDate = new Date(date_arr[0], date_arr[1] - 1, date_arr[2]);

var calendar = new tui.Calendar('#calendar', {
    defaultView: 'week',
    isReadOnly: true,
    taskView: false,
    scheduleView: ['time'],  // e.g. true, false, or ['allday', 'time'])
    isReadOnly: true,
});

updateRenderRange();


// Update render range
// Reference: https://stackoverflow.com/a/23593099
function updateRenderRange() {
    var newStartDate = calendar.getDateRangeStart()._date;
    var newEndDate = calendar.getDateRangeEnd()._date;
    var newRange = formatDate(newStartDate) + ' - ' + formatDate(newEndDate);
    $('.calendar-render-range').text(newRange);
}

// Calendar Menu Buttons
$('.calendar-move-today').click(function(){
    calendar.today();
});

// Calendar Previous button
$('.calendar-move-day.prev').click(function(){
    var startDateRange = calendar.getDateRangeStart()._date;
    if( startDateRange <= lbDate ) {
        $(this).prop('disabled', true);
    }
    $('.calendar-move-day.next').prop('disabled', false);
    calendar.prev();

    updateRenderRange();
});

// Calendar Next button
$('.calendar-move-day.next').click(function(){
    var startDateRange = calendar.getDateRangeStart()._date;
    if( startDateRange >= ubDate ) {
        $(this).prop('disabled', true);
    }
    $('.calendar-move-day.prev').prop('disabled', false);
    calendar.next();

    updateRenderRange();
});

// Calendar Event Listeners
calendar.on('clickSchedule', function(e) {
    var schedule = e.schedule;
    var url = schedule.raw;

    window.open( url );
});

// -------------- Create schedule objects -------------- //

var scheduleData = [];
var lessonData = @json($lessons);
var homeworkData = @json($homeworks);

// Cast JSON objects to array
if(!Array.isArray(lessonData)) {
    lessonData = Object.values(lessonData);
}

if(!Array.isArray(homeworkData)) {
    homeworkData = Object.values(homeworkData);
}

var count = 1;

lessonData.forEach(lesson => {
    count += 1;
    var url = "{{ route('classroom.lessons.show', [':classroom_id', ':lesson_id']) }}";
    url = url.replace(':classroom_id', lesson['classroom_id']);
    url = url.replace(':lesson_id', lesson['id']);
    scheduleData.push({
        id: count,
        calendarId: '1',
        title: lesson['name'],
        category: 'time',
        start: lesson['start_at'],
        end: lesson['end_at'],
        bgColor: '#B6D9EE', // blue2
        borderColor: '#1da0dd', // blue4
        isReadOnly: true,
        raw: url
    });
});

homeworkData.forEach(homework => {
    count += 1;
    var url = "{{ route('classroom.lessons.homeworks.show', [':lesson_id', ':homework_id']) }}";
    url = url.replace(':lesson_id', homework['lesson_id']);
    url = url.replace(':homework_id', homework['id']);
    scheduleData.push({
        id: count,
        calendarId: '1',
        title: homework['title'],
        category: 'time',
        start: homework['due_at'],
        bgColor: '#e77e87', // light-red
        borderColor: '#e63946', // red
        isReadOnly: true,
        raw: url
    });
});

calendar.createSchedules( scheduleData );

$('[name=class_selected]').change(function(e){
    e.preventDefault();
    var baseUrl = '{{ url()->current() }}';
    var currentUrl = '{{ url()->full() }}';

    var class_selected = e.target.value;
    // Dummy input
    var start_of_week = $('[name=start_of_week]').val();

    var params = {
        'class_selected': class_selected,
        'start_of_week': start_of_week
    };

    location.href = baseUrl + '?' + jQuery.param(params);
});
</script>
@endsection
