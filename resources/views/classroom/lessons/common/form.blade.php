@php
    if($is_edit) {
        $name = old('name') ?? $lesson->name;
        $description = old('description') ?? $lesson->description;
    } else {
        $name = old('name');
        $description = old('description');
        $start_at = old('start_at');
        $end_at = old('end_at');
    }
@endphp

<form action="{{ $is_edit ? route('classroom.lessons.update', [$classroom->id, $lesson->id])
                          : route('classroom.lessons.store', $classroom->id) }}" method="POST" id="createLessonForm">
    @csrf
    @if($is_edit)
        @method('PATCH')
    @endif
    {{-- Name --}}
    <div class="form-field">
        <label for="">Name</label>
        <input type="text" name="name" id="" value="{{ $name }}" placeholder="Name">
        @error('name')
            <div class="error-message">{{ $message }}</div>
        @enderror
    </div>

    {{-- Description --}}
    <div class="form-field">
        <label for="">Description (Optional)</label>
        <div class="loading-icon loading in-editor blue"></div>
        <textarea class="tinymce-editor" name="description">{!! $description !!}</textarea>
        @error('description')
            <div class="error-message">{{ $message }}</div>
        @enderror
    </div>

    @if(!$is_edit)
        {{-- Start At --}}
        <div class="form-field">
            <label for="">Start Time</label>
            <input type="text" placeholder="Start Time" name="start_at" class="datetimepicker-input" id="startTimeDtPicker" data-toggle="datetimepicker" data-target="#startTimeDtPicker" value="{{ $start_at }}">
            @error('start_at')
                <div class="error-message">{{ $message }}</div>
            @enderror
        </div>

        {{-- End At --}}
        <div class="form-field">
            <label for="">End Time</label>
            <input type="text" placeholder="End Time" name="end_at" class="datetimepicker-input" id="endTimeDtPicker" data-toggle="datetimepicker" data-target="#endTimeDtPicker" value="{{ $end_at }}">
            @error('end_at')
                <div class="error-message">{{ $message }}</div>
            @enderror
        </div>
    @endif

    <div class="text-center">
        <button type="submit" class="button--primary loadOnSubmitButton" style="width: 133px;">{{ $is_edit ? 'Update' : 'Create' }}</button>
    </div>
</form>
