<table class="table panel p-0 studentTable">
    <thead>
    <tr>
        <th scope="col">Name</th>
        <th scope="col">Parent's Email</th>
        <th scope="col">Und. Rating</th>
        <th scope="col">Avg Marks</th>
        <th scope="col">Unpaid Lessons</th>
        <th scope="col">Action</th>
    </tr>
    </thead>
    <tbody>
        @foreach( $students as $student )
            <tr>
                <td scope="row" class="text-center text-lg-left">
                    <img class="avatar" src="{{ $student->avatar }}" width="35" height="35">  {{ $student->username }}
                </td>
                <td>{{ $student->parent ? $student->parent->email : 'N/A' }}</td>
                <td>{{ $student->getAverageResourceRating( $class_selected ) }}</td>
                <td>{{ $student->getAverageHomeworkMarks( $class_selected ) }}</td>
                <td>{{ $student->unpaid_lesson_count ?? 0 }}</td>
                <td>
                    <div class="d-flex">
                        <a href="{{ route('classroom.students.show', $student->user_id) }}" class="button--primary--inverse sm mr-1"><span class="iconify" data-icon="mdi:magnify-plus-outline" data-inline="false"></span></a>
                    </div>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>
