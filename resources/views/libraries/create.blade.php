@extends('main.layouts.app')

@section('meta')
<title>Create Library | {{ config('app.name') }}</title>
@endsection

@section('content')
@include('main.navs.header')

<div class="py-2"></div>

<div class="container">
    <div class="row">
        <div class="col-12 col-lg-9 mb-2">
            <div class="maxw-740px mx-auto">
                <a class="d-block mb-2 inactive font-size2 gray2" href="{{ route('libraries.index') }}">< Back to My Libraries</a>

                <h1 class="font-size7">Create Library</h1>
                <p class="gray2">Number of libraries remaining: {{ auth()->user()->getNoOfLibrariesRemaining() }}</p>

                <div class="panel">
                    <form action="{{ route('libraries.store') }}" method="post">
                        @csrf
                        <input type="hidden" name="is_ajax" value="false">
                        {{-- Name --}}
                        <div class="form-field">
                            <label for="">Name</label>
                            <input type="text" name="name" value="{{ old('name') }}" required>
                            @error('name')
                                <div class="error-message">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class="text-right">
                            <button type="submit" class="button--primary disableOnSubmitButton" style="width: 76px;">Create</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="py-2"></div>
@endsection
