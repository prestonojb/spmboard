<?php

namespace Tests\Feature\Classroom;

use App\Notifications\Classroom\LessonAdded;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Carbon\Carbon;
use App\Classroom;
use App\Lesson;
use App\Subject;
use App\User;
use Illuminate\Support\Facades\Notification;

class LessonTest extends TestCase
{
    use WithFaker;

    private $teacher;
    private $student;
    private $parent;
    private $classroom;
    private $subject;

    public function setUp() : void
    {
        parent::setUp();

        // Subscribed Teacher
        $user = factory(User::class)->states('subscribed_teacher')->create();
        $this->teacher = $user->teacher;

        // Student with Board Pass
        $user = factory(User::class)->states('student_with_board_pass')->create();
        $this->student= $user->student;

        // Parent
        $user = factory(User::class)->states('parent')->create();
        $this->parent = $user->parent;


        // Subject and classroom
        $this->subject = Subject::first();
        $this->classroom = factory(Classroom::class)->create([
                            'teacher_id' => $this->teacher->user_id,
                            'subject_id' => $this->subject->id,
                        ]);
        // Attach parent to child
        $this->student->updateParent( $this->parent );
        // Add student to classroom
        $this->classroom->addStudent($this->student);
    }


    /**
     * @group lesson
     * Store Lesson
     */
    public function testStore()
    {
        Notification::fake();

        // Initial lesson count
        $init_count = Lesson::count();
        $response = $this->actingAs($this->teacher->user)
                        ->post( route('classroom.lessons.store', $this->classroom->id), [
                            'name' => $this->faker->name,
                            'description' => $this->faker->sentence,
                            'start_at' => Carbon::now()->format('d/m/Y g:i A'),
                            'end_at' => Carbon::now()->format('d/m/Y g:i A'),
                        ]);

        // Lesson added
        $this->assertEquals($init_count + 1, Lesson::count());

        // Lesson stored Notification sent to students/parents
        Notification::assertSentTo(
            [$this->student, $this->parent], LessonAdded::class
        );

        $response->assertRedirect();
    }
}
