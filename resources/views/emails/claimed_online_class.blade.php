@component('mail::message')
# Congratulations on logging in 7 days in a streak!
## As promised, you will be entitled to one FREE online class :)

Here are the details of your online class reward:

@component('mail::table')
| Field         | Value                    |
|:------------- |:------------------------ |
| Name          | {{ $data['name'] }}      |
| Email         | {{ $data['email'] }}     |
| Subject       | {{ $data['subject'] }}   |
| Claimed at    | {{ $data['claimed_at'] }}|

We will send you an invitation link to the online class ASAP, so stay tuned ;)

Regards,<br>
The Board Team
@endcomponent

@endcomponent
