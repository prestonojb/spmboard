<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Recommendation extends Model
{
    protected $guarded = [];

    /**
     * ------------------
     * RELATIONSHIPS
     * ------------------
     */
    public function recommendee()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function recommender()
    {
        return $this->belongsTo(User::class, 'recommended_by_user_id', 'id');
    }

    /**
     * -------------------
     * ACCESSORS
     * -------------------
     */
    /**
     * Returns relationship of recommendation
     * @return string
     */
    public function getRelationshipAttribute($value)
    {
        switch($value) {
            case 'student_of':
                return "{$this->recommender->name} is a student of {$this->recommendee->name}";
            case 'worked_with':
                return "{$this->recommender->name} worked with {$this->recommendee->name}";
            case 'parent_of_student_of':
                return "{$this->recommender->name} is a parent of a student of {$this->recommendee->name}";
            default:
                throw new \Exception("Invalid relationship value!");
        }
    }

    /**
     * Get relationship key
     * @return string
     */
    public function getRelationshipKeyAttribute()
    {
        return $this->getOriginal('relationship');
    }
}
