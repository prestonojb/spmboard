@php
$is_admin_view = isset($is_admin_view) && $is_admin_view;
@endphp

<table class="table table-bordered bg-white">
    <thead>
        <tr>
            <th scope="col">Submitted At</th>
            @if($is_admin_view)
                <th scope="col">Teacher Username</th>
            @endif
            <th scope="col">Resume</th>
            <th scope="col">Status</th>
            <th scope="col">Actions</th>
        </tr>
    </thead>
    <tbody>
        @if(count($applications ?? []) > 0)
            @foreach($applications as $application)
                <tr>
                    <td>{{ $application->created_at->toFormattedDateString() }}</td>
                    @if($is_admin_view)
                        <td><a href="{{ $application->user->show_url }}" target="_blank">{{ $application->user->username }}</a></td>
                    @endif
                    <td><a target="_blank" href="{{ $application->resume }}">View</a></td>
                    <td>{!! $application->status_tag_html !!}</td>
                    <td>
                        @if($is_admin_view)
                            @if(!$application->isResolved())
                                <textarea name="admin_comment" id="" rows="3" placeholder="Admin comment"></textarea>
                                <p class="gray2">Add a comment if you have a specific reason to reject a teacher's application. Update application status (Approved/Declined) with the either one of the buttons below.</p>
                                <div class="text-right">
                                    {{-- Approve --}}
                                    <form class="d-inline-block" action="{{ route('voyager.certification_applications.approve', $application->id) }}" method="post">
                                        @csrf
                                        <input type="hidden" name="admin_comment">
                                        <button class="btn btn-primary actionButton" style="margin:right: 4px;'" type="submit">Approve</button>
                                    </form>
                                    {{-- Decline --}}
                                    <form class="d-inline-block" action="{{ route('voyager.certification_applications.decline', $application->id) }}" method="post">
                                        @csrf
                                        <input type="hidden" name="admin_comment">
                                        <button class="btn btn-danger actionButton" type="submit">Decline</button>
                                    </form>
                                </div>
                            @else
                                @if(!is_null($application->admin_comment))
                                    <div class="mb-2">
                                        <label for="">Comment</label>
                                        <p>{{ $application->admin_comment }}</p>
                                    </div>
                                @else
                                    <label for="">No Comment</label>
                                @endif
                            @endif
                        @else
                            <div class="d-flex justify-content-start align-items-center">
                                <button type="button" class="button--primary sm" data-toggle="modal" data-target="#application_details_{{ $application->id }}_modal">View Details</button>
                                {{-- <button class="class--primary--inverse" data-toggle="modal" data-target="#">Edit</button> --}}
                            </div>
                        @endif
                    </td>
                </tr>

                {{-- Modal --}}
                <div class="modal fade" id="application_details_{{ $application->id }}_modal" tabindex="-1" role="dialog">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Certification Application #{{ $application->id }}</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                {{-- Admin Comments --}}
                                @if(!is_null($application->admin_comment))
                                    <div class="form-field">
                                        <label for="">Admin Comment</label>
                                        <p>{{ $application->admin_comment }}</p>
                                    </div>
                                @endif
                                {{-- Resume --}}
                                <div class="form-field">
                                    <label for="">Resume</label>
                                    <p><a href="{{ $application->resume }}" target="_blank">View</a></p>
                                </div>
                                {{-- Status --}}
                                <div class="form-field">
                                    <label for="">Status</label>
                                    <p>{!! $application->status_tag_html !!}</p>
                                </div>
                                {{-- Submitted At --}}
                                <div class="form-field">
                                    <label for="">Submitted At</label>
                                    <p>{{ $application->created_at->toDateTimeString() }}</p>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="button--primary--inverse" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        @else
            <tr>
                <td colspan="4" class="text-center">No application</td>
            </tr>
        @endif
    </tbody>
</table>

{{ $applications->links() }}
