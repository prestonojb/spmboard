<div class="store__balance-info py-2 mb-2">
    <div class="row">
        @if( auth()->user()->hasCoins() )
            <div class="col-6 col-lg-5">
                <div class="store__balance-type">
                    <h3 class="title">Coins</h3>
                    <img src="{{ asset('img/essential/coin.svg') }}" alt="coin" class="inline-image">
                    <span class="value">{{ auth()->user()->coin_balance }}</span>
                </div>
            </div>
        @endif
        @if( auth()->user()->hasRewardPoints() )
            <div class="col-6 col-lg-5">
                <div class="store__balance-type">
                    <h3 class="title">Reward Points</h3>
                    <img src="{{ asset('img/essential/points.svg') }}" alt="reward point" class="inline-image">
                    <span class="value">{{ auth()->user()->reward_point_balance }}</span>
                </div>
            </div>
        @endif
            {{-- <div class="col-12 col-lg-2 p-2">
            <a href="#" class="how-it-works">How it works?</a>
        </div> --}}
    </div>
</div>
