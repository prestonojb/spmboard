<?php

namespace App\Http\Controllers;

use App\Notifications\QuestionCommented;
use Illuminate\Http\Request;
use App\Question;
use App\QuestionComment;
use Amplitude;

class QuestionCommentController extends Controller
{
    public function store(Question $question)
    {
        request()->validate([
            'question_comment' => 'required|max:191'
        ]);

        $user = auth()->user();

        // Create comment
        $question_comment = $question->comments()->create([
            'user_id' => $user->id,
            'comment' => request('question_comment'),
        ]);

        if($user->id != $question->user->id) {
            $question->user->notify(new QuestionCommented($question_comment));
        }

        // Log event on Amplitude
        Amplitude::setUserId($user->id);
        Amplitude::queueEvent('add_question_comment', ['question_id' => $question->id, 'question_comment_id' => $question_comment->id]);

        return back()->withSuccess('Comment uploaded successfully!');
    }

    public function destroy($id)
    {
        $question_comment = QuestionComment::find($id);
        $this->authorize('delete', $question_comment);

        $question_comment->delete();
        return back()->withError('Comment deleted successfully!');
    }
}
