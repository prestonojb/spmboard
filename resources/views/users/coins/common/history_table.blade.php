<div class="panel p-0">
    @if(count($coin_actions) > 0)
        @foreach($coin_actions as $action)
            <div class="px-2 py-1 @if(!$loop->last) border-bottom @endif">
                <div class="my-coins__coin-history-item">
                    <span class="amount badge {{ $action->amount >= 0 ? 'green' : 'red' }} text-white">{{ $action->amount >= 0 ? '+' : '' }}{{ $action->amount }}</span>
                    <div><a class="name">{{ $action->name }}</a></div>
                    <span class="time">{{ $action->display_created_at }}</span>
                </div>
            </div>
        @endforeach
    @else
        <div class="text-center w-100 my-4">
            <img src="{{ asset('img/essential/empty.svg') }}" alt="No coin history" width="220" height="220" class="mb-2">
            <h2 class="h3">No coin history</h2>
        </div>
    @endif
</div>
