// Loading URL only works in JS file, throws CORS error if used in Laravel Blade
var pdfViewers = document.getElementsByClassName('custom_pdf_viewer');
var pdfViewerPreview = document.getElementsByClassName('custom_pdf_viewer_preview');
var pdfRendererHtml = '<canvas class="pdf_renderer w-100 border box-shadow1 mb-1"></canvas>';
var loadingIconHtml = '<div class="my-5 loading-icon loading in-editor blue"></div>';

var zoom = 2;

// Renders PDF currentPage on chosen canvas
function render(pdf, currentPage, canvas) {
    pdf.getPage(currentPage).then((page) => {
        var ctx = canvas.getContext('2d');
        var viewport = page.getViewport(zoom);

        // Set dimensions of canvas
        canvas.width = viewport.width;
        canvas.height = viewport.height;

        page.render({
            canvasContext: ctx,
            viewport: viewport
        });
    });
}

// PDF preview viewer
for(let pdfViewer of pdfViewerPreview) {
    var pdfUrl = pdfViewer.getAttribute('data-url');
    var maxPagePreview = parseInt(pdfViewer.getAttribute('data-max-page-preview'));

    // Initialise loading icon to PDF renderer
    pdfViewer.insertAdjacentHTML('beforeend', loadingIconHtml);
    var loadingIcon = pdfViewer.querySelector('.loading-icon');

    pdfjsLib.getDocument( pdfUrl ).then((pdf) => {
        var totalPages = pdf.numPages;
        var pagesForPreview = Math.min(maxPagePreview, totalPages);
        // Render first PDF page only
        for (currentPage = 1; currentPage <= pagesForPreview; currentPage++) {
            // Clone a PDF renderer <div> and append it to the end of <canvas> element
            loadingIcon.insertAdjacentHTML('beforebegin', pdfRendererHtml);

            // Get last PDF render as canvas
            var pdf_renderers = document.getElementsByClassName("pdf_renderer");
            var length = pdf_renderers.length;
            var canvas = pdf_renderers[ length - 1 ];

            // Render current page on last PDF renderer <div>
            render(pdf, currentPage, canvas);
        }
        loadingIcon.remove();
    });
}

// PDF viewer
for(let pdfViewer of pdfViewers) {
    var pdfUrl = pdfViewer.getAttribute('data-url');

    // Initialise loading icon to PDF renderer
    pdfViewer.insertAdjacentHTML('beforeend', loadingIconHtml);
    var loadingIcon = pdfViewer.querySelector('.loading-icon');

    pdfjsLib.getDocument( pdfUrl ).then((pdf) => {
        // Get total number of pages
        var totalPages = pdf.numPages;

        // Render all PDF pages
        for (currentPage = 1; currentPage <= totalPages; currentPage++) {
            // Clone a PDF renderer <div> and append it to the end of <canvas> element
            loadingIcon.insertAdjacentHTML('beforebegin', pdfRendererHtml);

            // Get last PDF render as canvas
            var pdf_renderers = document.getElementsByClassName("pdf_renderer");
            var length = pdf_renderers.length;
            var canvas = pdf_renderers[ length - 1 ];

            // Render current page on last PDF renderer <div>
            render(pdf, currentPage, canvas);
        }
        loadingIcon.remove();
    });
}

// document.getElementById('go_previous')
//     .addEventListener('click', (e) => {
//         if(myState.pdf == null
//             || myState.currentPage == 1) return;
//         myState.currentPage -= 1;
//         document.getElementById("current_page")
//                 .value = myState.currentPage;

//         // Set PDF viewer to top
//         canvasContainer.scrollTop = 0;

//         render();
//     });

// document.getElementById('go_next')
//     .addEventListener('click', (e) => {
//         if(myState.pdf == null
//         || myState.currentPage >= myState.pdf._pdfInfo.numPages)
//         return;

//         myState.currentPage += 1;
//         document.getElementById("current_page")
//                 .value = myState.currentPage;

//         // Scroll PDF viewer to top
//         canvasContainer.scrollTop = 0;

//         render();
//     });

// document.getElementById('current_page')
//     .addEventListener('keypress', (e) => {
//         if(myState.pdf == null) return;

//         // Get key code
//         var code = (e.keyCode ? e.keyCode : e.which);

//         // If key code matches that of the Enter key
//         if(code == 13) {
//             var desiredPage =
//                     document.getElementById('current_page')
//                             .valueAsNumber;

//             if(desiredPage >= 1
//                 && desiredPage <= myState.pdf
//                                         ._pdfInfo.numPages) {
//                     myState.currentPage = desiredPage;
//                     document.getElementById("current_page")
//                             .value = desiredPage;
//                     render();
//             }
//         }
//     });

// document.getElementById('zoom_in')
//     .addEventListener('click', (e) => {
//         if(myState.pdf == null) return;
//         if(myState.zoom <= 3.5) {
//             myState.zoom += 0.5;
//         }
//         render();
//     });

// document.getElementById('zoom_out')
//     .addEventListener('click', (e) => {
//         if(myState.pdf == null) return;
//         if(myState.zoom >= 1.5) {
//             myState.zoom -= 0.5;
//         }
//         render();
//     });
