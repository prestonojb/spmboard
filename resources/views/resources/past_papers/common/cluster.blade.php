@if( $exam_board->name == 'SPM' )
    @for($i = 0; $i < count($past_papers); $i++)
        @php
            $paper = $past_papers[$i];
            if( $i != count($past_papers) -2 ) {
                $next_paper = $past_papers[$i + 1];
            }
        @endphp

        @if( $i == 0 )
            <h4 class="h3 mt-4">Year {{ $paper->year }}</h4>
            <hr class="mt-2 mb-3">
        @endif

        <div class="ml-3">
            @include('resources.past_papers.common.block')
        </div>

        @if( isset($next_paper) && $paper->year != $next_paper->year )
            <h4 class="h3 mt-4">Year {{ $next_paper->year }}</h4>
            <hr class="mt-2 mb-3">
        @endif
    @endfor

@elseif( $exam_board->name == 'IGCSE' )
    @for($i = 0; $i < count($past_papers); $i++)
        @php
            $paper = $past_papers[$i];
            if( $i != count($past_papers) - 2 ) {
                $next_paper = $past_papers[$i + 1];
            }
        @endphp

        @if( $i == 0 )
            <h4 class="h3 mt-4">Year {{ $paper->year }} {{ $paper->season }}</h4>
            <hr class="mt-2 mb-3">
        @endif

        <div class="ml-3">
            @include('resources.past_papers.common.block')
            @if( isset($next_paper) && $paper->paper != $next_paper->paper )
                <hr>
            @endif
        </div>

        @if( isset($next_paper) && ($paper->season != $next_paper->season || $paper->season != $next_paper->season) )
            <h4 class="h3 mt-4">Year {{ $next_paper->year }} {{ $next_paper->season }}</h4>
            <hr class="mt-2 mb-3">
        @endif
    @endfor
@endif
