<ul class="nav nav-tabs nav-stacked mb-3">
    <li class="nav-pills {{ request()->is('classroom/subscription') ? 'active' : '' }}">
        <a href="{{ route('classroom.subscriptions.show') }}" class="">Subscription</a>
    </li>
    <li class="nav-pills {{ request()->is('classroom/prices') ? 'active' : '' }}">
        <a href="{{ route('classroom.prices.index') }}" class="">Plans</a>
    </li>
    <li class="nav-pills {{ request()->is('classroom/billing_history') ? 'active' : '' }}">
        <a href="{{ route('classroom.billing_history') }}" class="{{ request()->is('t/subscription ') ? 'active' : '' }}">Billing History</a>
    </li>
</ul>
