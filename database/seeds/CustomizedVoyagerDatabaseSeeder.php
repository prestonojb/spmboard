<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Traits\Seedable;
use Illuminate\Support\Facades\DB;

class CustomizedVoyagerDatabaseSeeder extends Seeder
{
    use Seedable;

    protected $seedersPath = __DIR__.'/';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        $this->call(CustomizedSettingsTableSeeder::class);
        $this->call(CustomizedRolesTableSeeder::class);
        $this->call(CustomizedPermissionsTableSeeder::class);
        $this->call(CustomizedPermissionRoleTableSeeder::class);
        $this->call(CustomizedMenusTableSeeder::class);
        $this->call(CustomizedMenuItemsTableSeeder::class);
        $this->call(CustomizedDataTypesTableSeeder::class);
        $this->call(CustomizedDataRowsTableSeeder::class);
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
