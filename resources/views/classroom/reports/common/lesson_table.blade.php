<table class="table lessonTable">
    <thead>
        <tr>
            <th scope="col"><input type="checkbox" class="selectAllCheckbox"></th>
            <th scope="col">Name</th>
            <th scope="col">Start Date</th>
            <th scope="col">No. of Resource Posts</th>
            <th scope="col">No. of Homework</th>
        </tr>
    </thead>
    <tbody>
        @if( count($lessons ?? []) )
            @foreach( $lessons as $lesson )
                <tr class="lessonRow">
                    <td><input type="checkbox" class="lessonCheckbox" value="{{ $lesson->id }}" id=""></td>
                    <td>{{ $lesson->name }}</td>
                    <td>{{ $lesson->start_at->toFormattedDateString() }}</td>
                    <td>{{ $lesson->resource_post_count }}</td>
                    <td>{{ $lesson->homework_count }}</td>
                </tr>
            @endforeach
        @else
            <tr class="text-center py-2 noLessonRow">
                <td colspan="5">No Lessons found</td>
            </tr>
        @endif
    </tbody>
</table>

<div class="d-none">
    <table>
        <tr class="lessonRowHelper">
            <td><input type="checkbox" class="lessonCheckbox" value="" id=""></td>
            <td class="lesson_name"></td>
            <td class="lesson_start_at"></td>
            <td class="lesson_resource_post_count"></td>
            <td class="lesson_homework_count"></td>
        </tr>
        <tr class="text-center py-2 noLessonRow">
            <td colspan="5">No Lessons found</td>
        </tr>
    </table>
</div>
