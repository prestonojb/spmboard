@extends('classroom.layouts.app')

@section('meta')
<title>Questions | {{ config('app.name') }}</title>
@endsection

@section('styles')
@endsection

@section('content')
@component('classroom.common.breadcrumbs')
    <a href="{{ route('classroom.questions', $classroom->id) }}">Questions</a> /
    {{ str_limit($question->question, 25) }}
@endcomponent


@component('classroom.common.page_heading')
    @slot('title')
        Edit Question
    @endslot
@endcomponent

<div class="ask-wrapper">
    <h1 class="heading">Edit</h1>
    <h2 class="sub-heading">Edit your question to help others understand your question better!</h2>

    <form class="form-container" method="POST" action="{{ route('classroom.questions.update', [$classroom->id, $question->id]) }}">
        @csrf
        @method('PATCH')
        <div class="form-field">
            <label>Question</label>
            @if($errors->any())
                <input name="question" value="{{ old('question') }}" required autocomplete="off">
            @else
                <input name="question" value="{{ $question->question }}" required autocomplete="off">
            @endif
            @error('question')
                <span class="error-message">{{ $message }}</span>
            @enderror
        </div>

        @if($question->img)
            <div class="form-field">
                <img class="file-input-preview" src="{{ $question->img }}">
            </div>
        @endif

        <div class="form-field">

            <label>Description</label>

            @if($errors->any())
                <div class="editor">{!! old('description') !!}</div>
            @else
                <div class="editor">{!! $question->description !!}</div>
            @endif
                <input type="hidden" name="description">
            @error('description')
                <span class="error-message">{{ $message }}</span>
            @enderror
        </div>

        <div class="form-field">
            <label>Chapter</label>
            <select class="" name="chapter">
                <option disabled selected></option>
                @foreach($classroom->subject->chapters as $chapter)
                    <option value="{{ $chapter->id }}" @if(in_array(  $chapter->id, $question->chapters->pluck('id')->toArray()  )) selected @endif>
                        {{ $chapter->name }}
                    </option>
                @endforeach
            </select>
            @error('chapter')
                <span class="error-message">{{ $message }}</span>
            @enderror
        </div>

        <div class="text-center">
            <button type="submit" class="ask-button button--primary askButton">Update</button>
        </div>
    </form>
</div>

@endsection
@section('scripts')
@endsection
