<?php

namespace App\Http\Controllers;

use App\Answer;
use App\Classroom;
use App\Rules\QuillEmpty;
use Illuminate\Http\Request;

class TeacherAnswerController extends Controller
{
    public function edit(Classroom $classroom, Answer $answer)
    {
        $question = $answer->question;
        return view('classroom.answers.edit', compact('classroom', 'question', 'answer'));
    }

    public function update(Classroom $classroom, Answer $answer)
    {
        $answer->answer = request()->answer;
        $answer->save();

        return redirect()->route('classroom.questions.show', [$classroom->id, $answer->question->id]);
    }
}
