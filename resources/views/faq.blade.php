@extends('main.layouts.app')
@section('meta')
<title>FAQ | {{ config('app.name') }}</title>
@endsection

@section('content')
<section class="container py-4">
    <div class="text-center mb-3">
        <h2 class="heading">FAQ</h2>
        <h3 class="sub-heading">Have a question about Board? Read more on the FAQ below.</h3>
        <h4 class="body">Your question is not here? <a href="{{ url('contact-us') }}" style="font-size: inherit;">Contact us</a> now </h4>
    </div>

    {{-- FAQ Tabs --}}
    <div class="row justify-content-center align-items-center faq__menu-list mb-3" id="faqMenuList" role="tablist">

        <div class="col-12 col-sm-6 col-md-4 col-lg-3">
            <a class="faq__menu-list-item" href="#rp_coins" role="tab">
                <span class="iconify" data-icon="ri:vip-crown-2-line" data-inline="false"></span>
                <span class="text">RP/Coins</span>
            </a>
        </div>

        <div class="col-12 col-sm-6 col-md-4 col-lg-3">
            <a class="faq__menu-list-item" href="#qna" role="tab">
                <span class="iconify" data-icon="cil:newspaper" data-inline="false"></span>
                <span class="text">Q&A Platform</span>
            </a>
        </div>

        <div class="col-12 col-sm-6 col-md-4 col-lg-3">
            <a class="faq__menu-list-item" href="#resources" role="tab">
                <span class="iconify" data-icon="mdi-light:note-text" data-inline="false"></span>
                <span class="text">Resources</span>
            </a>
        </div>
    </div>

    {{-- FAQ Tabs Content --}}
    <div class="faq__tab-content tabContent">

        {{-- RP/Coins --}}
        <div class="tab active" id="rp_coins" role="tabpanel">
            <div id="rp_coins_accordion">
                <h4 class="faq__tab-title">Reward Points/Coins</h4>
                <div class="card faq__card">
                    <div class="card-header faq__card-header">
                        <h5 class="mb-0">
                            <button class="button-link" data-toggle="collapse" data-target="#rp_coins1" aria-expanded="true" aria-controls="collapseOne">
                                How can I redeem coins?
                            </button>
                        </h5>
                    </div>
                    <div id="rp_coins1" class="collapse" aria-labelledby="headingOne" data-parent="#rp_coins_accordion">
                        <div class="card-body faq__card-body">
                            You can redeem coins at the Store with your reward points! Stay tuned for more redemption options!
                        </div>
                    </div>
                </div>

                <div class="card faq__card">
                    <div class="card-header faq__card-header">
                        <h5 class="mb-0">
                            <button class="button-link" data-toggle="collapse" data-target="#rp_coins2" aria-expanded="true" aria-controls="collapseOne">
                                What can I spend my coins on?
                            </button>
                        </h5>
                    </div>

                    <div id="rp_coins2" class="collapse" aria-labelledby="headingOne" data-parent="#rp_coins_accordion">
                        <div class="card-body faq__card-body">
                            You can purchase educational resources(Past Papers, Notes, Topical Past Papers) and more with your coins! Stay tuned as we bring more amazing deals to reward you for studying with us!
                        </div>
                    </div>
                </div>


                <div class="card faq__card">
                    <div class="card-header faq__card-header">
                        <h5 class="mb-0">
                            <button class="button-link" data-toggle="collapse" data-target="#rp_coins3" aria-expanded="true" aria-controls="collapseOne">
                                How can I earn coins?
                            </button>
                        </h5>
                    </div>
                    <div id="rp_coins3" class="collapse" aria-labelledby="headingOne" data-parent="#rp_coins_accordion">
                        <div class="card-body faq__card-body">
                            Unfornately, we do not offer coins directly to our learners! However, we reward Reward Points to our learners, which can be used to claim coins!
                        </div>
                    </div>
                </div>


                <div class="card faq__card">
                    <div class="card-header faq__card-header">
                        <h5 class="mb-0">
                            <button class="button-link" data-toggle="collapse" data-target="#rp_coins4" aria-expanded="true" aria-controls="collapseOne">
                                How can I earn Reward Points?
                            </button>
                        </h5>
                    </div>

                    <div id="rp_coins4" class="collapse" aria-labelledby="headingOne" data-parent="#rp_coins_accordion">
                        <div class="card-body faq__card-body">
                            Log in everyday and you will be rewarded with Reward Points! * Go on a streak to earn more!
                            You may also earn reward points by answering bountied questions(questions with point offerring) or contributing quality questions/amswers(our moderators are in a hunt for them)
                        </div>
                    </div>
                </div>


                <div class="card faq__card">
                    <div class="card-header faq__card-header">
                        <h5 class="mb-0">
                            <button class="button-link" data-toggle="collapse" data-target="#rp_coins5" aria-expanded="true" aria-controls="collapseOne">
                                Can I convert my coins to cash?
                            </button>
                        </h5>
                    </div>

                    <div id="rp_coins5" class="collapse" aria-labelledby="headingOne" data-parent="#rp_coins_accordion">
                        <div class="card-body faq__card-body">
                            Although it is currently not possible, we are working to ensure that the hardworkers and contributors are taken care of(which includes payments in cash)!
                        </div>
                    </div>
                </div>


                <div class="card faq__card">
                    <div class="card-header faq__card-header">
                        <h5 class="mb-0">
                            <button class="button-link" data-toggle="collapse" data-target="#rp_coins6" aria-expanded="true" aria-controls="collapseOne">
                                Can I purchase notes/past year papers directly with cash?
                            </button>
                        </h5>
                    </div>

                    <div id="rp_coins6" class="collapse" aria-labelledby="headingOne" data-parent="#rp_coins_accordion">
                        <div class="card-body faq__card-body">
                            We do not encourage the purchase of resources directly from our website. Instead, we try our best to incorporate the best learning experience to our learners by actively engaging in the forum and accessing our resources!
                            That being said, if you wish to accelerate your learning, you may opt to purchase coins directly from the store with cash(currently unavailable)
                        </div>
                    </div>
                </div>

                <div class="card faq__card">
                    <div class="card-header faq__card-header">
                        <h5 class="mb-0">
                            <button class="button-link" data-toggle="collapse" data-target="#rp_coins7" aria-expanded="true" aria-controls="collapseOne">
                                What is the difference between Reward Points and coins?
                            </button>
                        </h5>
                    </div>

                    <div id="rp_coins7" class="collapse" aria-labelledby="headingOne" data-parent="#rp_coins_accordion">
                        <div class="card-body faq__card-body">
                            Reward points is a way we reward you learners from learning with us! Coins is used as a means of purchase on Board!
                            You can choose to convert your RP to coins at the store too!
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <div class="tab" id="qna" role="tabpanel">
            <div id="qna_accordion">

                <h4 class="faq__tab-title">Question & Answer Platform</h4>
                <div class="card faq__card">
                    <div class="card-header faq__card-header">
                        <h5 class="mb-0">
                            <button class="button-link" data-toggle="collapse" data-target="#qna1" aria-expanded="true" aria-controls="collapseOne">
                                Who will be answering my questions?
                            </button>
                        </h5>
                    </div>
                    <div id="qna1" class="collapse" aria-labelledby="headingOne" data-parent="#qna_accordion">
                        <div class="card-body faq__card-body">
                            There are a team of mentors waiting to answer your questions! Besides, the community is always there to help you!
                        </div>
                    </div>
                </div>


                <div class="card faq__card">
                    <div class="card-header faq__card-header">
                        <h5 class="mb-0">
                            <button class="button-link" data-toggle="collapse" data-target="#qna2" aria-expanded="true" aria-controls="collapseOne">
                                Will I be rewarded for answering questions?
                            </button>
                        </h5>
                    </div>
                    <div id="qna2" class="collapse" aria-labelledby="headingOne" data-parent="#qna_accordion">
                        <div class="card-body faq__card-body">
                            If there is a Reward Point bounty on the question, you will receive the Reward Points if your answer is the first to be accepted by the question asker!
                            Our team of moderators are also always on the hunt for quality questions/answers, therefore if your question/answer gets recommended by our moderators,
                            you will also receive additional Reward Points!
                        </div>
                    </div>
                </div>

                <div class="card faq__card">
                    <div class="card-header faq__card-header">
                        <h5 class="mb-0">
                            <button class="button-link" data-toggle="collapse" data-target="#qna3" aria-expanded="true" aria-controls="collapseOne">
                                How do I ensure that my questions are answered correctly?
                            </button>
                        </h5>
                    </div>
                    <div id="qna3" class="collapse" aria-labelledby="headingOne" data-parent="#qna_accordion">
                        <div class="card-body faq__card-body">
                            Our moderators are always in the works! All the answers will be corrected and rectified if there is any mistake to the answers.
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <div class="tab" id="resources" role="tabpanel">
            <div id="resources_accordion">

                <h4 class="faq__tab-title">Resources</h4>
                <div class="card faq__card">
                    <div class="card-header faq__card-header">
                        <h5 class="mb-0">
                            <button class="button-link" data-toggle="collapse" data-target="#resources1" aria-expanded="true" aria-controls="collapseOne">
                                Can I purchase resources with cash directly?
                            </button>
                        </h5>
                    </div>
                    <div id="resources1" class="collapse" aria-labelledby="headingOne" data-parent="#resources_accordion">
                        <div class="card-body faq__card-body">
                            Not at the moment! However, you may purchase coins with cash and buy resources with coins in the next coming updates.
                        </div>
                    </div>
                </div>

                <div class="card faq__card">
                    <div class="card-header faq__card-header">
                        <h5 class="mb-0">
                            <button class="button-link" data-toggle="collapse" data-target="#resources2" aria-expanded="true" aria-controls="collapseOne">
                                I purchased resources with coins but I can’t seem to have access to it. What should I do?
                            </button>
                        </h5>
                    </div>
                    <div id="resources2" class="collapse" aria-labelledby="headingOne" data-parent="#resources_accordion">
                        <div class="card-body faq__card-body">
                            Please do not hesitate to <a href="{{ url('contact/us') }}">contact us</a> to ask for assistance.
                        </div>
                    </div>
                </div>


                <div class="card faq__card">
                    <div class="card-header faq__card-header">
                        <h5 class="mb-0">
                            <button class="button-link" data-toggle="collapse" data-target="#resources3" aria-expanded="true" aria-controls="collapseOne">
                                Do I get the most updated resources?
                            </button>
                        </h5>
                    </div>
                    <div id="resources3" class="collapse" aria-labelledby="headingOne" data-parent="#resources_accordion">
                        <div class="card-body faq__card-body">
                            Yes! All your purchases on resources will be one-time and you will still be able to access the resources in the event that it is updated.
                        </div>
                    </div>
                </div>

            </div>

        </div>

        <div class="tab" id="account" role="tabpanel">
            <div id="account_accordion">
                <h4 class="faq__tab-title">Account</h4>
                <div class="card faq__card">
                    <div class="card-header faq__card-header">
                        <h5 class="mb-0">
                            <button class="button-link" data-toggle="collapse" data-target="#account1" aria-expanded="true" aria-controls="collapseOne">
                                How do I switch account type?
                            </button>
                        </h5>
                    </div>
                    <div id="account1" class="collapse" aria-labelledby="headingOne" data-parent="#account_accordion">
                        <div class="card-body faq__card-body">
                            To switch account, existing account must be cancelled and deleted.
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>
@endsection

@section('scripts')
<script>
$(function(){
    $('#faqMenuList a').on('click', function (e) {
        e.preventDefault();
        var tabs = $('.tabContent').find('.tab');
        tabs.removeClass('active');

        // Get fragment name
        var selectedTabId = $(this).attr('href').split('#').pop();
        var selectedTab = $('.tab#' + selectedTabId);
        selectedTab.addClass('active');
    });
});
</script>
@endsection
