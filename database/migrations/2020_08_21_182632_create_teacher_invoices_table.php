<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTeacherInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teacher_invoices', function (Blueprint $table) {
            $table->increments('id');
            $table->string('invoice_number');
            $table->unsignedInteger('teacher_id');
            $table->unsignedInteger('student_id');

            $table->decimal('amount', 8, 2);
            $table->string('currency')->default('MYR');

            $table->timestamp('due_at');
            $table->timestamp('paid_at')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teacher_invoices');
    }
}
