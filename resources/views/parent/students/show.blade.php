@extends('forum.layouts.app')

@section('content')
@include('parent.common.header')
<main>
    <div class="container py-3">

        <div class="mb-3">
            @include('parent.students.common.details')
        </div>

        <div class="mb-3">
            <select name="classroom" id="" class="d-inline-block maxw-250px">
                @foreach($classrooms as $room)
                    <option value="{{ $room->id }}" @if($room->id == $classroom->id) selected @endif>{{ $room->name }}</option>
                @endforeach
            </select>
        </div>


        <h3><b>Progress Report</b></h3>
        <div class="panel p-0">
            <div class="table-responsive">
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">Report Period</th>
                        <th scope="col">Remarks</th>
                        <th scope="col">Created At</th>
                        <th scope="col">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach( $reports as $report )
                            <tr>
                                <td>{{ $report->start_date->toFormattedDateString() }} - {{ $report->end_date->toFormattedDateString() }}</td>
                                <td>{{ $report->remark ?? 'N/A' }}</td>
                                <td>{{ $report->created_at->toFormattedDateString() }}</td>
                                <td>
                                    <div class="d-flex">
                                        <a href="{{ $report->url }}" target="_blank" class="button--primary mr-1">View PDF</a>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

                {{ $reports->links() }}
            </div>
        </div>
    </div>


</main>
@endsection

@section('scripts')
@endsection
