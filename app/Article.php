<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use CyrildeWit\EloquentViewable\Viewable;
use CyrildeWit\EloquentViewable\Contracts\Viewable as ViewableContract;

class Article extends Model implements ViewableContract
{
    use Viewable;

    //Allow mass assignment to all variables in the model
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * -----------------
     * ACCESSORS
     * -----------------
     */
    /**
     * Returns relative path of cover image
     * @return string
     */
    public function getCoverImgRelativeUrlAttribute()
    {
        return $this->getOriginal('cover_img');
    }

    /**
     * Returns comma-delimited string tags
     * @return string
     */
    public function getTagsCommaDelimitedStrAttribute()
    {
        return $this->getOriginal('tags');
    }

    /**
     * Returns tags in array
     * @return array
     */
    public function getTagsAttribute($value)
    {
        return array_filter(explode(',', $value));
    }

    /**
     * Get full cover image URL path
     * @return string
     */
    public function getCoverImgAttribute($value)
    {
        return !is_null($value) ? Storage::cloud()->url($value)
                                : asset('img/essential/article_default_cover_img.png');
    }

    /**
     * Returns show URL
     * @return string
     */
    public function getShowUrlAttribute()
    {
        return route('users.articles.show', $this->id);
    }

    /**
     * --------------------
     * HELPERS
     * --------------------
     */
    /**
     * Updates article cover image
     * @param $img
     */
    public function updateCoverImage($img)
    {
        if( is_null($img) ) {
            throw new \Exception("Cover image cannot be empty");
        }

        //Delete existing user profile image if any
        if( !is_null($this->cover_img_relative_url) ){
            Storage::cloud()->delete( $this->cover_img_relative_url );
        }

        $img->store('articles/', 's3');
        $filename = $img->hashName();
        $url = "articles/{$filename}";

        $this->update(['cover_img' => $url]);
    }

    /**
     * Returns Collection of Articles by user
     * @param int $take
     * @return Collection
     */
    public function getMoreArticlesByUsers($take = 3)
    {
        return self::where('id', '!=', $this->id)->where('user_id', $this->user_id)->orderByDesc('created_at')->take($take)->get();
    }
}
