<?php

namespace App\Http\Controllers;

use App\BoardPassSubscription;
use Carbon\Carbon;
use App\CoinProduct;
use Amplitude;

class BoardPassController extends Controller
{
    public function getBoardPassView()
    {
        if(auth()->check()) {
            $user = auth()->user();
            if($user->is_teacher() || $user->is_parent()) {
                return redirect('/')->with('error', 'Board Pass is not available for '.$user->getUserAccountType().' account!');
            }
            $subscribe_from_date = $user->hasActiveBoardPass() ? $user->getActiveBoardPass()->end_at : Carbon::now();
        } else {
            $subscribe_from_date = Carbon::now();
        }

        // Get Board Pass
        $board_pass = CoinProduct::getBoardPass();

        return view('products.board_pass', compact('board_pass', 'subscribe_from_date'));
    }

    /**
     * Purchase Board Pass
     */
    public function purchase()
    {
        // Validation
        request()->validate([
            'email' => 'nullable|email',
            'duration_in_month' => 'required|integer|between:1,60',
        ]);
        $user = auth()->user();
        // Get Board Pass Product
        $board_pass = CoinProduct::getBoardPass();
        $duration_in_month = intval(request('duration_in_month'));

        if( $user->is_student() ) {
            $price_in_coins = $board_pass->getBoardPassPrice($duration_in_month);
            // Check if user has sufficient coin balance
            if($user->coin_balance < $price_in_coins) {
                return redirect()->back()->with('error', 'You have insufficient coin balance.');
            }
            // Check if there's existing Board Pass subscription
            if( $user->hasActiveBoardPass() ) {
                $board_pass_subscription = $user->getActiveBoardPass();

                // Extend Board Pass Subscription
                $board_pass_subscription->extend($duration_in_month);
                $message = "Board Pass is extended for {$duration_in_month} months!";
            } else {
                // Create new subscription
                $board_pass_subscription = $user->board_pass_subscriptions()->create([
                                                'duration_in_month' => $duration_in_month,
                                                'activated_at' => Carbon::now(),
                                            ]);
                $message = "Board Pass is activated for {$duration_in_month} months!";
            }
            // Deduct coins from user
            $user->deductCoins($price_in_coins);
            // Track Coin Action on 'coin_actions' table
            $user->coin_actions()->create([
                'name' => 'Board Pass',
                'description' => "Purchase/Upgrade Board Pass for {$duration_in_month} months",
                'amount' => -($price_in_coins),
                'coin_actionable_id' => $board_pass_subscription->id,
                'coin_actionable_type' => BoardPassSubscription::class,
            ]);

            // Log event on Amplitude
            Amplitude::setUserId($user->id);
            Amplitude::queueEvent('subscribe_to_board_pass', ['coin_amount' => $price_in_coins, 'duration_in_month' => $duration_in_month]);

            return redirect()->back()->with('success', $message);
        } else {
            // Redirect if user is not student/parent
            return redirect()->back()->with('error', 'Board Pass is not available for '.ucfirst($user->getUserAccountType()).' account!');
        }
    }

    /**
     * Returns no. of additional subscription months
     * AJAX
     */
    public function getNoOfAdditionalSubscriptionMonths()
    {
        // Validation
        request()->validate([
            'subscribe_until_month' => 'required|integer|between:1,12',
            'subscribe_until_year' => "required|integer",
        ]);

        $user = auth()->user();
        $end_at = Carbon::createFromFormat("d m Y", '1 '.request('subscribe_until_month').' '.request('subscribe_until_year'));

        try {
            if($user->hasActiveBoardPass()) {
                return $user->getActiveBoardPass()->getActiveSubscriptionNoOfAdditionalSubscriptionMonths($end_at, null);
            } else {
                return BoardPassSubscription::getNoOfAdditionalSubscriptionMonths($end_at, null);
            }
        } catch (\Exception $e) {
            throw new \Exception("An error occured!");
        }
    }

    /**
     * Returns valid until date
     * AJAX
     */
    public function getEndAtDate()
    {
        // Validation
        request()->validate([
            'subscribe_until_month' => 'required|integer|between:1,12',
            'subscribe_until_year' => "required|integer",
        ]);

        $end_at = Carbon::createFromFormat("d m Y", '1 '.request('subscribe_until_month').' '.request('subscribe_until_year'));

        try {
            if(auth()->check()) {
                $user = auth()->user();
                if($user->hasActiveBoardPass()) {
                    return $user->getActiveBoardPass()->getActiveSubscriptionEndAtDate($end_at, null);
                } else {
                    return BoardPassSubscription::getEndAtDate($end_at, null);
                }
            } else {
                return BoardPassSubscription::getEndAtDate($end_at, null);
            }
        } catch (\Exception $e) {
            throw new \Exception("An error occured!");
        }
    }

    /**
     * Returns amount of coins
     * AJAX
     */
    public function getAmountOfCoins()
    {
        // Validation
        request()->validate([
            'subscribe_until_month' => 'required|integer|between:1,12',
            'subscribe_until_year' => "required|integer",
        ]);

        $user = auth()->user();
        $end_at = Carbon::createFromFormat("d m Y", '1 '.request('subscribe_until_month').' '.request('subscribe_until_year'));

        try {
            if($user->hasActiveBoardPass()) {
                return $user->getActiveBoardPass()->getActiveSubscriptionAmountOfCoins($end_at, null);
            } else {
                return BoardPassSubscription::getAmountOfCoins($end_at, null);
            }
        } catch (\Exception $e) {
            throw new \Exception("An error occured!");
        }
    }

    /**
     * Returns discount (in %) of Board Pass based on no. of months of subscription
     * AJAX
     */
    public function getDiscount()
    {
        // Validation
        request()->validate([
            'no_of_months' => 'required|min:1',
        ]);

        $board_pass = CoinProduct::getBoardPass();
        return $board_pass->getBoardPassDiscount(request('no_of_months'));
    }
}
