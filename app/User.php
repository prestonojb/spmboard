<?php

namespace App;

use Carbon\Carbon;
use Exception;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Contracts\Auth\CanResetPassword;
use Newsletter;
use Illuminate\Support\Facades\Storage;
use Laravel\Cashier\Billable;
use TCG\Voyager\Models\Role;

class User extends \TCG\Voyager\Models\User implements MustVerifyEmail, CanResetPassword
{
    use Notifiable, Billable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    protected $casts = [
        'is_certified' => 'boolean',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['email_verified_at', 'last_online_at'];

    public function getRouteKeyName()
    {
        return 'username';
    }

    /**
     * Route notifications for the mail channel.
     *
     * @param  \Illuminate\Notifications\Notification  $notification
     * @return string
     */
    public function routeNotificationForMail($notification)
    {
        return $this->email;
    }

    public function makeAdmin()
    {
        $role = Role::where('name', 'admin')->first();
        $this->update(['role_id' => $role->id]);
    }

    /**
     * Returns true if user is admin, else false
     * @return boolean
     */
    public function isAdmin()
    {
        return !is_null($this->role) && $this->role->name == 'admin';
    }

    /**
     * Returns true if user is moderator, else false
     * @return boolean
     */
    public function isModerator()
    {
        return !is_null($this->role) && $this->role->name == 'moderator';
    }

    /**
     * ----------------
     * Relationships
     * ----------------
     */
    public function student()
    {
        return $this->hasOne(Student::class);
    }

    public function teacher()
    {
        return $this->hasOne(Teacher::class);
    }

    public function parent()
    {
        return $this->hasOne(StudentParent::class);
    }

    public function board_pass_subscriptions()
    {
        return $this->hasMany(BoardPassSubscription::class);
    }

    public function questions()
    {
        return $this->hasMany(Question::class);
    }

    public function answers()
    {
        return $this->hasMany(Answer::class);
    }

    public function question_upvotes()
    {
        return $this->hasMany(QuestionUpvote::class);
    }

    public function answer_upvotes()
    {
        return $this->hasMany(AnswerUpvote::class);
    }

    public function state()
    {
        return $this->belongsTo(State::class);
    }

    public function coin_actions()
    {
        return $this->hasMany(CoinAction::class);
    }

    public function reward_point_actions()
    {
        return $this->hasMany(RewardPointAction::class);
    }

    public function resources()
    {
        return $this->belongsToMany(Resource::class)
                    ->whereHasMorph('resourceable', ['App\PastPaper', 'App\TopicalPastPaper', 'App\Notes', 'App\Guide'], function($q){
                        $q->whereNull('deleted_at');
                    })
                    ->withTimestamps();
    }

    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    public function subjects()
    {
        return $this->belongsToMany(Subject::class);
    }

    public function exam_builder_sheets()
    {
        return $this->hasMany(ExamBuilderSheet::class);
    }

    public function classrooms()
    {
        return $this->account_variant->classrooms;
    }

    public function getClassroomsAttribute()
    {
        return $this->classrooms();
    }

    public function classroom_posts()
    {
        return $this->hasMany(ClassroomPost::class);
    }

    public function articles()
    {
        return $this->hasMany(Article::class);
    }

    public function recommendations()
    {
        return $this->hasMany(Recommendation::class)->orderByDesc('created_at');
    }

    public function libraries()
    {
        return $this->hasMany(Library::class)->orderBy('created_at');
    }

    public function certification_applications()
    {
        return $this->hasMany(CertificationApplication::class)->latest();
    }

    /**
     * Returns user's owned notes
     */
    public function owned_notes()
    {
        return $this->hasMany(Notes::class, 'user_id', 'id');
    }

    /**
     * Returns user's topical past papers
     */
    public function topical_past_papers()
    {
        return $this->hasMany(TopicalPastPaper::class, 'user_id', 'id');
    }

    public function resource_review()
    {
        return $this->hasMany(ResourceReview::class);
    }

    public function withdrawal_requests()
    {
        return $this->hasMany(WithdrawalRequest::class);
    }

    /**
     * -------------
     * Accessors
     * -------------
     */

    public function getShowUrlAttribute()
    {
        return route('users.show', $this->username);
    }

    public function getUserRoleAttribute()
    {
        if( $this->student ) {
            return $this->student;
        } elseif( $this->teacher ) {
            return $this->teacher;
        } elseif( $this->parent ) {
            return $this->parent;
        }
    }

    public function getUserTypeAttribute()
    {
        if( $this->student ) {
            return $this->student;
        } elseif( $this->teacher ) {
            return $this->teacher;
        } elseif( $this->parent ) {
            return $this->parent;
        }
    }

    /**
     * Return user account type icon string HTML
     * @return string
     */
    public function getUserAccountTypeIconHtmlAttribute()
    {
        if( $this->is_teacher() ) {
            $icon_html = 'la:chalkboard-teacher-solid';
        } elseif( $this->is_student() ) {
            $icon_html = 'icons8:student';
        } elseif( $this->is_parent() ) {
            $icon_html = 'ri:parent-line';
        }
        return '<span class="iconify font-size-inherit color-inherit" data-icon="'. $icon_html .'" data-inline="false"></span>';
    }

    /**
     * Return user account type icon popover HTML
     * @return string
     */
    public function getUserRoleIconPopoverHtmlAttribute()
    {
        $html = '<button type="button" class="user-role-icon" data-container="body" data-toggle="popover" data-placement="top" data-content="'.ucfirst($this->account_type).'">';
        $icon_html = $this->user_account_type_icon_html;

        return $html.$icon_html.'</button>';
    }

    public function getRecommendCountAttribute()
    {
        $question_count = Question::where('user_id', $this->id)->whereNotNull('recommended_by_user_id')->whereNotNull('recommended_at')->count();
        $answer_count = Answer::where('user_id', $this->id)->whereNotNull('recommended_by_user_id')->whereNotNull('recommended_at')->count();

        return $question_count + $answer_count;
    }

    public function getSchoolAttribute()
    {
        if( $this->student ) {
            return $this->student->school;
        } elseif( $this->teacher ) {
            return $this->teacher->school;
        }
        return null;
    }

    /**
     * Returns phone number
     * @return string
     */
    public function getPhoneNumberAttribute()
    {
        return $this->account_variant->phone_number;
    }

    /**
     * Returns username with badge HTML (certified badge) if applicable
     * @return string
     */
    public function getUsernameWithBadgeAttribute()
    {
        $value = $this->username;
        return $this->is_certified ? $value.$this->certified_tag_html
                                   : $value;
    }

    public function getNameAttribute($value)
    {
        return $value ?? $this->username;
    }

    public function getAvatarAttribute($value)
    {
        if(!is_null($value)){
            if(strpos($value, 'googleusercontent') || strpos($value, 'amazonaws')){
                //Google
                return $value;
            } else{
                //Amazon S3
                $updated_value = str_replace('\\', '/', $value);
                return Storage::cloud()->url($updated_value);
            }
        } else {
            //No profile picture
            return asset('img/essential/no-profile-picture.jpg');
        }
    }

    /**
     * Returns certified tag HTML
     * @return string
     */
    public function getCertifiedTagHtmlAttribute()
    {
        return '<span class="iconify blue6" data-icon="ic:baseline-verified" data-inline="false"></span>';
    }

    /**
     * Get Avatar Relative URL
     * @return string
     */
    public function getAvatarRelativeUrlAttribute()
    {
        return $this->attributes['avatar'];
    }

    public function getRewardDayAttribute()
    {
        // For 7 days reward
        if ($this->consecutive_login_days !== 0 && $this->consecutive_login_days % 7 == 0) {
            return 7;
        }
        return $this->consecutive_login_days % 7;
    }

    /**
     * Returns last online at
     * @return string
     */
    public function getLastOnlineDayAttribute()
    {
        if($this->last_online_at->isToday()) {
            return 'today';
        } else {
            return $this->last_online_at->diffForHumans();
        }
    }

    /**
     * ---------------
     * Mutators
     * ---------------
     */
    public function setSchoolAttribute($value)
    {
        if( $this->student ) {
            $this->student()->update([
                'school' => $value
            ]);
        } elseif( $this->teacher ) {
            $this->teacher()->update([
                'school' => $value
            ]);
        }
    }

    /**
     * Returns user's profile description
     * @return string
     */
    public function getProfileSummaryAttribute()
    {
        return $this->account_variant->profile_summary;
    }


    /**
     * Returns user's referral link
     * @return string
     */
    public function getReferralLinkAttribute()
    {
        return route('register')."?referral_code={$this->referral_code}";
    }

    /**
     * -----------------------
     * HELPERS
     * -----------------------
     */

    /**
     * Returns true if user uses Board Classroom
     * @return boolean
     */
    public function useClassroom()
    {
        if($this->is_teacher()) {
            return $this->account_variant->useClassroom();
        } else {
            return false;
        }
    }

    /**
     * Returns true if user opts for subject, else false
     * @return boolean
     */
    public function chosenSubject(Subject $subject)
    {
        return $this->subjects->contains($subject->id);
    }

    /**
     * Returns true if user exists with email, else false
     * @return boolean
     */
    public static function exists($email)
    {
        return self::where('email', $email)->exists();
    }

    /**
     * Update user avatar to "users/{MY}" path on S3
     * Update 'avatar' column on DB
     */
    public function updateAvatar( $img )
    {
        if( is_null($img) ) {
            throw new Exception("Profile image cannot be empty");
        }

        //Delete existing user profile image if any
        if( !is_null($this->avatar_relative_url) ){
            Storage::cloud()->delete( $this->avatar_relative_url );
        }

        //Current Month and Year
        $my = date('FY', time());

        //Store image to S3
        $img->store('users/'. $my. '/', 's3');
        $filename = $img->hashName();
        //Save image url in database
        $avatar_url = "users/{$my}/{$filename}";

        // Update avatar column on DB
        $this->update(['avatar' => $avatar_url]);
    }

    /**
     * Returns a unique username for user from email
     * @return string
     */
    public static function generateUsernameFromEmail( $email )
    {
        // Get initial username by taking the front part of the email
        $username = explode('@', $email)[0];
        $new_username = $username;
        // Returns true if email is unique, else false
        $username_exists = self::where('username', $username)->exists();
        $counter = 0;

        while ($username_exists) {
            // Append counter to username
            $new_username = $username."$counter";
            $counter++;
            $username_exists = self::where('username', $new_username)->exists();
        }
        return $new_username;
    }

    /**
     * ------------------
     * Account Variant
     * ------------------
     */

     /**
      * Returns account variant name
      * @return string
      */
    public function getUserAccountType()
    {
        if( $this->is_student() ) {
            return 'student';
        } elseif( $this->is_teacher() ) {
            return 'teacher';
        } elseif( $this->is_parent() ) {
            return 'parent';
        }
    }

    public function getAccountTypeAttribute()
    {
        return $this->getUserAccountType();
    }

    /**
     * Returns true if user is student, else false
     * @return boolean
     */
    public function is_student()
    {
        return !is_null($this->student);
    }

    /**
     * Returns true if user is teacher, else false
     * @return boolean
     */
    public function is_teacher()
    {
        return !is_null($this->teacher);
    }

    /**
     * Returns true if user is parent, else false
     * @return boolean
     */
    public function is_parent()
    {
        return !is_null($this->parent);
    }

    /**
     * Returns account variant (student, teacher, parent) of user
     * @return \App\Teacher|\App\Student|\App\StudentParent|null
     */
    public function getAccountVariantAttribute()
    {
        if( $this->is_teacher() ) {
            return $this->teacher;
        } elseif( $this->is_student() ) {
            return $this->student;
        } elseif( $this->is_parent() ) {
            return $this->parent;
        }
    }

    /**
     * Switch User Account Variant
     * @param string $existing_account_type
     * @param string $new_account_type
     */
    public function switchAccountType( $existing_account_type, $new_account_type )
    {
        if( $existing_account_type == $new_account_type ) {
            throw new Exception("New account type must be different from existing account type.");
        }
        $this->deleteAccountType( $existing_account_type );
        $this->createAccountType( $new_account_type );
    }

    /**
     * Create User Account Variant
     * @param string $account_type
     */
    public function createAccountType( $account_type )
    {
        // if( $this->account_type ) {
        //     throw new Exception("User already has an existing '{$this->account_type}' account!");
        // }

        // Create account type table row
        switch( $account_type ) {
            case 'student':
                Student::create([
                    'user_id' => $this->id,
                ]);
                return;
            case 'teacher':
                Teacher::create([
                    'user_id' => $this->id,
                ]);
                return;
            case 'parent':
                StudentParent::create([
                    'user_id' => $this->id
                ]);
                return;
        }
    }

    /**
     * Create a teacher account for user
     */
    public function createAsTeacher()
    {
        $this->createAccountType('teacher');
    }

    /**
     * Create a student account for user
     */
    public function createAsStudent()
    {
        $this->createAccountType('student');
    }
    /**
     * Create a parent account for user
     */
    public function createAsParent()
    {
        $this->createAccountType('parent');
    }

    /**
     * Delete User Account Variant
     * @param string $account_type
     */
    public function deleteAccountType( $account_type )
    {
        if( $this->account_type != $account_type ) {
            throw new Exception("User does not have the inputted account type.");
        }

        // Redirect to 'Getting Started' page
        switch( $account_type ) {
            case 'student':
                $this->student()->delete();
                return;
            case 'teacher':
                $this->teacher()->delete();
                return;
            case 'parent':
                $this->parent()->delete();
                return;
        }
    }

    /**
     * Returns profile color
     * @return string
     */
    public function getProfileColor()
    {
        return $this->account_variant->getProfileColor();
    }

    /*
    |---------------------------------
    | FORUM
    |---------------------------------
    */
    /**
     * Returns top subjects from forum
     * @return Collection
     */
    public function getTopSubjectsFromForum()
    {
        return $this->account_variant->getTopSubjectsFromForum();
    }

    /**
     * Get number of answers for subject
     * @return int
     */
    public function getNoOfAnswers(Subject $subject)
    {
        return $this->answers()->where('subject_id', $subject->id)->count();
    }

    /*
    |---------------------------------
    | Reward Point Balance
    |---------------------------------
    */

    /**
     * Get reward point balance of user
     * @return int
     */
    public function getRewardPointBalanceAttribute()
    {
        return $this->account_variant->reward_point_balance;
    }

    /**
     * Set user's Reward Point
     * @throws Exception if user account variant does not have reward points
     */
    public function setRewardPointBalanceAttribute($value)
    {
        if( $this->hasRewardPoints() ) {
            if( $this->is_student() ) {
                $this->student()->update([
                    'reward_point_balance' => $value,
                ]);
            } elseif( $this->is_teacher() ) {
                    $this->teacher()->update([
                        'reward_point_balance' => $value,
                    ]);
                }
        } else {
            throw new Exception("User type does not have reward points!");
        }
    }

    /**
     * Returns true if user account type has reward points
     * @return boolean
     */
    public function hasRewardPoints()
    {
        return !is_null($this->reward_point_balance);
    }

    /**
     * Add reward points to user
     */
    public function addRewardPoints($amount)
    {
        if( $amount <= 0 ) {
            throw new Exception("Amount must be greater than 0!");
        }
        if(!$this->hasRewardPoints()) {
            throw new Exception("User's account cannot own Reward Points!");
        }
        $this->reward_point_balance += $amount;
        $this->save();
    }

    /**
     * Deduct reward points from user
     * @throws Exception when amount is less than 0
     * @throws Exception when user's reward point balance is trying to be deducted below zero
     */
    public function deductRewardPoints($amount)
    {
        if( $amount <= 0 ) {
            throw new Exception("Amount must be greater than 0!");
        }
        if( $this->reward_point_balance < $amount ) {
            throw new Exception("User's reward point balance cannot be deducted below zero!");
        }
        $this->reward_point_balance -= $amount;
        $this->save();
    }

    /**
     * Add reward point action to user
     */
    public function addRewardPointAction( $name, $amount )
    {
        $this->addRewardPoints($amount);
        RewardPointAction::create([
            'user_id' => $this->id,
            'name' => $name,
            'amount' => $amount,
        ]);
    }

    /*
    |---------------------------------
    | Coin Balance
    |---------------------------------
    */

    /**
     * Get reward point balance of user
     * @return int
     */
    public function getCoinBalanceAttribute()
    {
        return $this->account_variant->coin_balance;
    }

    /**
     * Returns true if user account type has coins
     * @return boolean
     */
    public function hasCoins()
    {
        return !is_null($this->coin_balance);
    }

    /**
     * Add coin to user
     * @param $amount
     * @throws Exception when amount is less than 0
     */
    public function addCoins($amount)
    {
        if( $amount <= 0 ) {
            throw new Exception("Amount must be greater than 0!");
        }
        $this->coin_balance += $amount;
        $this->save();
    }

    /**
     * Deduct coins from user account
     * @param int $amount
     * @throws Exception when amount is less than 0
     * @throws Exception when user's coin balance is less than input amount
     */
    public function deductCoins($amount)
    {
        if( $amount <= 0 ) {
            throw new Exception("Amount must be greater than 0!");
        }
        if( $this->coin_balance < $amount ) {
            throw new Exception("User's coin balance cannot be deducted below zero!");
        }
        $this->coin_balance -= $amount;
        $this->save();
    }

    /**
     * Set user coin balance
     * @throws Exception when user account variant does not have coins
     */
    public function setCoinBalanceAttribute($value)
    {
        if($this->hasCoins()) {
            if($this->is_student()) {
                $this->student()->update([
                    'coin_balance' => $value,
                ]);
            } elseif( $this->is_teacher() ) {
                $this->teacher()->update([
                    'coin_balance' => $value,
                ]);
            }
        } else {
            throw new Exception("User type does not have coins!");
        }
    }

    /**
     * Add coin action to user
     */
    public function addCoinAction( $name, $amount )
    {
        $this->addCoins($amount);
        $this->coin_actions()->create([
            'name' => $name,
            'amount' => $amount,
        ]);
    }


    public function getGettingStartedPath()
    {
        // Redirect to path after register
        switch( $this->account_type ) {
            case 'student':
                return 's/getting-started';
            case 'teacher':
                return 't/getting-started';
            case 'parent':
                return 'p/getting-started';
            default:
                return '/';
        }
    }

    /**
     * Get lesson IDs associated with user
     * @param (optional) App\Classroom $classroom Filter lesson IDs by classroom
     */
    public function getLessonIds( Classroom $classroom = null )
    {
        return $this->account_variant->getLessonIds( $classroom );
    }

    /**
     * Get upcoming lessons
     * @param int $days_in_advance
     */
    public function getUpcomingLessons( $days_in_advance = 14 )
    {
        $start_time = Carbon::now();
        $end_time = Carbon::now()->addDays($days_in_advance);

        $lesson_ids = $this->getLessonIds();

        return Lesson::whereIn('id', $lesson_ids)->where('start_at', '>', $start_time)->where('start_at', '<=', $end_time)->orderBy('start_at')->get();
    }

    /**
     * Get upcoming homeworks
     * @param int $days_in_advance
     */
    public function getUpcomingHomeworks( $days_in_advance = 14 )
    {
        $start_time = Carbon::now();
        $end_time = Carbon::now()->addDays($days_in_advance);

        $lesson_ids = $this->getLessonIds();

        return Homework::whereIn('lesson_id', $lesson_ids)->where('due_at', '>', $start_time)->where('due_at', '<=', $end_time)->orderBy('due_at')->get();
    }

    /**
     * Get Timeline Items of users
     */
    public function getTimelineItems()
    {
        $timeline_items = collect();

        $lessons = $this->getUpcomingLessons();
        $homeworks = $this->getUpcomingHomeworks();

        foreach($lessons as $lesson) {
            $timeline_items->push([
                'type' => Lesson::class,
                'url' => route('classroom.lessons.show', [$lesson->classroom_id, $lesson->id]),
                'due_at' => $lesson->start_at,
                'title' => "Get ready for \"{$lesson->name}\" Lesson!",
            ]);
        }

        foreach($homeworks as $homework) {
            $timeline_items->push([
                'type' => Homework::class,
                'url' => route('classroom.lessons.homeworks.show', [$homework->lesson_id, $homework->id]),
                'due_at' => $homework->due_at,
                'title' => "\"{$homework->title}\" Homework is due soon!",
            ]);
        }

        return $timeline_items;
    }

    /**
     * ------------------
     * BOARD PASS
     * ------------------
     */
    /**
     * Returns true if user has active Board Pass, else false
     * @return boolean
     */
    public function hasActiveBoardPass()
    {
        return !is_null($this->getActiveBoardPass());
    }

    /**
     * Returns user's active Board Pass
     * @return App\BoardPassSubscription|null
     */
    public function getActiveBoardPass()
    {
        // Limit Board Pass to only users with coins
        if( $this->hasCoins() ) {
            // Check if latest active Board Pass is active
            if(!is_null($this->getLatestBoardPass()) && $this->getLatestBoardPass()->is_active) {
                return $this->getLatestBoardPass();
            } else {
                return null;
            }
        }
        return null;
    }

    /**
     * Returns user's latest Board Pass, despite it being active or not
     * @return App\BoardPassSubscription|null
     */
    public function getLatestBoardPass()
    {
        return $this->board_pass_subscriptions()->orderByDesc('activated_at')->first();
    }

    /**
     * Create a Board Pass subscription for user
     * @param integer $duration_in_month
     * @return App\BoardPassSubscription
     * @throws Exception if @param duration_in_month is not a positive integer
     */
    public function createBoardPassSubscription($duration_in_month)
    {
        // Ensure duration in month is a positive integer
        if(is_int($duration_in_month) && $duration_in_month > 0) {
            return $this->board_pass_subscriptions()->create([
                'duration_in_month' => $duration_in_month
            ]);
        } else {
            throw new \Exception("Duration in month must be in integer");
        }
    }

    /**
     * Return sheets by exam board
     * @param \App\ExamBoard $exam_board
     * @return \Illuminate\Pagination\Paginator;|\Illuminate\Support\Collection
     */
    public function getSheetsByExamBoard(ExamBoard $exam_board, $paginate = false, $perPage = 15)
    {
        $sheets = $this->exam_builder_sheets()->whereHas('subject', function($q) use ($exam_board) {
            return $q->where('exam_board_id', $exam_board->id);
        });

        $sheets = $sheets->orderByDesc('created_at');
        return $paginate ? $sheets->paginate($perPage)
                         : $sheets->get();
    }

    /**
     * --------------------
     * EXAM BUILDER
     * --------------------
     */
    /**
     * Returns -1 if user has infinite worksheets, else return number of available worksheets that can be generated this month
     * Free user gets to generate 3 worksheets/month
     * @return int
     */
    public function noOfAvailableWorksheetsLeft()
    {
        return $this->account_variant->noOfAvailableWorksheetsLeft();
    }

    /**
     * Returns true if user can generate exam builder MS
     * @return boolean
     */
    public function canGenerateExamBuilderSheet()
    {
        return $this->account_variant->canGenerateExamBuilderSheet();
    }

    /**
     * Returns true if user can hide exam builder sheet watermark
     * @return boolean
     */
    public function canHideExamBuilderSheetWatermark()
    {
        return $this->account_variant->canHideExamBuilderSheetWatermark();
    }

    /**
     * Returns true if user can generate exam builder sheet with subchapter
     * @return boolean
     */
    public function canGenerateExamBuilderSheetWithSubchapter()
    {
        return $this->account_variant->canGenerateExamBuilderSheetWithSubchapter();
    }

    /**
     * Returns max number of questions user can generate for exam builder sheet
     * @return int
     */
    public function getExamBuilderQuestionLimit()
    {
        return $this->account_variant->getExamBuilderQuestionLimit();
    }

    /**
     * Returns max number of chapters user can generate for exam builder sheet
     * @return int
     */
    public function getExamBuilderChapterLimit()
    {
        return $this->account_variant->getExamBuilderChapterLimit();
    }

    /**
     * Returns true if user has premium resource access
     * @return boolean
     */
    public function hasPremiumResourceAccess()
    {
        return $this->account_variant->hasPremiumResourceAccess();
    }

    /**
     * -----------------
     * Profile
     * -----------------
     */
    /**
     * Returns number of question upvotes
     * @return int
     */
    public function getQuestionUpvoteCount()
    {
        $question_ids = $this->questions()->pluck('id')->toArray();
        return QuestionUpvote::whereIn('question_id', $question_ids)->count();
    }
    /**
     * Returns number of question recommendations
     * @return int
     */
    public function getQuestionRecommendationsCount()
    {
        return $this->questions()->whereNotNull('recommended_at')->count();
    }
    /**
     * Returns number of answer upvotes
     * @return int
     */
    public function getAnswerUpvoteCount()
    {
        $answer_ids = $this->answers()->pluck('id')->toArray();
        return AnswerUpvote::whereIn('answer_id', $answer_ids)->count();
    }
    /**
     * Returns number of answer recommendations
     * @return int
     */
    public function getAnswerRecommendationsCount()
    {
        return $this->answers()->whereNotNull('recommended_at')->count();
    }
    /**
     * Returns number of answer checks
     * @return int
     */
    public function getAnswerChecksCount()
    {
        return $this->answers()->whereNotNull('checked_at')->count();
    }

    /**
     * Returns true if user has referral link, else false
     * @return boolean
     */
    public function hasReferralLink()
    {
        return !is_null($this->referral_code);
    }

    /**
     * Get user by referral code
     * @param string $code
     * @return App\User
     */
    public static function getByReferralCode($code)
    {
        return self::where('referral_code', $code)->first();
    }

    /**
     * -------------------
     * RECOMMENDATION
     * -------------------
     */
    /**
     * Returns true if user has recommendation from another user
     * @return boolean
     */
    public function hasRecommendationFrom(User $user)
    {
        return $this->recommendations()->where('recommended_by_user_id', $user->id)->exists();
    }

    /**
     * Returns recommendation by recommender to recommendee
     * @return boolean
     */
    public function getRecommendationFrom(User $user)
    {
        return $this->recommendations()->where('recommended_by_user_id', $user->id)->first();
    }

    /**
     * Returns true if this user can recommend the other user
     * @param App\User $user User that is to be recommended
     * @return boolean
     */
    public function canRecommend(User $user)
    {
        // Recommendee must be a teacher and cannot be self
        return $user->is_teacher() && auth()->user()->id != $user->id;
    }

    /**
     * Returns true if user has extra library
     * @return boolean
     */
    public function hasExtraLibrary()
    {
        return count($this->libraries ?? []) < $this->getLibraryLimit();
    }

    /**
     * Returns library limit of user
     * @return int
     */
    public function getLibraryLimit()
    {
        return $this->account_variant->getLibraryLimit();
    }

    /**
     * Returns most recent library, else false
     * @return App\Library|null
     */
    public function getMostRecentLibrary()
    {
        return $this->libraries()->latest()->first();
    }

    /**
     * Returns number of libraries remaining
     * @return int
     */
    public function getNoOfLibrariesRemaining()
    {
        return $this->getLibraryLimit() - $this->libraries()->count();
    }

    /**
     * Returns true if user has pending application, else false
     * @return boolean
     */
    public function hasPendingApplication()
    {
        return $this->certification_applications()->where('status', 'pending')->exists();
    }

    /**
     * Returns ALL certification metrics
     * @return array
     */
    public function getCertificationMetrics()
    {
        return $this->account_variant->getCertificationMetrics();
    }

    /**
     * Returns ALL certification metrics target
     * @return array
     */
    public function getCertificationMetricsTarget()
    {
        return $this->account_variant->getCertificationMetricsTarget();
    }

    /**
     * Returns true if user passed all certification metrics, else returns false
     * @return boolean
     */
    public function passedAllCertificationMetrics()
    {
        return $this->account_variant->passedAllCertificationMetrics();
    }

    /**
     * Returns true if user has resource
     * @param App\Resource $resource
     * @return boolean
     */
    public function hasResource(Resource $resource)
    {
        return $this->resources->contains($resource->id);
    }

    /**
     * Unlocks resource for user
     * @param App\Resource $resource
     */
    public function unlockResource(Resource $resource)
    {
        $this->resources()->attach($resource->id);
    }

    /**
     * --------------------
     * RESOURCES
     * --------------------
     */
    /**
     * Returns true if user has unlocked resources
     * @return Collection
     */
    public function hasUnlockedResources()
    {
        return $this->resources->count();
    }

    /**
     * Returns owned past papers
     * @return Collection
     */
    public function getOwnedPastPapers()
    {
        return $this->resources()->where('resourceable_type', PastPaper::class)->get();
    }

    /**
     * Returns owned topical past papers
     * @return Collection
     */
    public function getOwnedTopicalPastPapers()
    {
        return $this->resources()->where('resourceable_type', TopicalPastPaper::class)->get();
    }
    /**
     * Returns owned past papers
     * @return Collection
     */
    public function getOwnedNotes()
    {
        return $this->resources()->where('resourceable_type', Notes::class)->get();
    }


    /**
     * --------------------
     * WITHDRAWAL REQUESTS
     * --------------------
     */
    /**
     * Returns true if user can withdraw money from coins, else false
     * @return boolean
     */
    public function canWithdraw()
    {
        return $this->is_teacher();
    }

    /**
     * Returns true if user can request withdrawal, else false
     * @return boolean
     */
    public function canRequestWithdrawal()
    {
        // Check for pending request
        $pending_request_exists = $this->withdrawal_requests()->where('status', 'pending')->exists();
        // Check for request a week ago
        $request_week_ago_exists = $this->withdrawal_requests()->whereDate('created_at', '>=', Carbon::now()->subMonth()->format('Y-m-d'))->exists();

        return !$pending_request_exists && !$request_week_ago_exists;
    }
}
