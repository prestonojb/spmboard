<div class="table-responsive">
    <table class="table table-bordered panel">
        <thead>
            <tr>
                <th scope="col">Date</th>
                <th scope="col">Status</th>
                <th scope="col">Withdrawal Amount (Coin)</th>
                <th scope="col">Amount transferred (USD)</th>
                <th scope="col">Receipt</th>
                <th scope="col">Comment</th>
            </tr>
        </thead>

        <tbody>
            @if(count($withdrawal_requests ?? []))
                @foreach($withdrawal_requests as $withdrawal_request)
                    <tr>
                        <td>{{ $withdrawal_request->created_at->toFormattedDateString() }}</td>
                        <td>{!! $withdrawal_request->status_tag_html !!}</td>
                        <td>{{ $withdrawal_request->withdrawal_amount_in_coin }}</td>
                        <td>{{ $withdrawal_request->amount_transferred_in_usd ?? 'N/A' }}</td>
                        <td>
                            @if(!is_null($withdrawal_request->receipt_url))
                                <a href="{{ $withdrawal_request->receipt_url }}" target="_blank" class="blue5 underline-on-hover">
                                    View
                                </a>
                            @else
                                N/A
                            @endif
                        </td>
                        <td>{{ $withdrawal_request->comment ?? 'N/A' }}</td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="6">
                        <div class="text-center gray2 mb-0">
                            No data found
                        </div>
                    </td>
                </tr>
            @endif
        </tbody>
    </table>
</div>
