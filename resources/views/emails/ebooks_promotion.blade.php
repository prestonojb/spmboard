@component('mail::message')
# Welcome on Board!
## As promised, here are your e-books for your preferred subjects!

E-book links for download:<br>
@component('mail::panel')
@foreach($subjects as $subject)
{{ $subject->exam_board->name.' '.$subject->name }}:<br>
{{ Storage::cloud()->url('marketing/ebooks/'.$subject->exam_board->slug.'-'.$subject->slug.'.pdf') }}<br>
------------------------------------------------------
@endforeach
@endcomponent


All you have to do now is to ask away!

@component('mail::button', ['url' => $url, 'color' => 'primary'])
Start asking on Board
@endcomponent

------------------------------------------------------
*The download links will be active for 60 minutes.*
@endcomponent
