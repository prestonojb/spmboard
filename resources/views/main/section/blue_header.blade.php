<div class="landing__section-banner">
    <div class="container landing__section-banner-container">
        <h4 class="heading">{{ $title }}</h4>
        <h5 class="sub-heading">{{ $subtitle }}</h5>
        @if(isset($body) && !is_null($body))
            <p class="body">{!! $body !!}</p>
        @endif
    </div>
</div>
