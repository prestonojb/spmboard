@extends('forum.layouts.app')

@section('meta')
<title>500 - Something went wrong | {{ config('app.name') }}</title>
@endsection

@section('content')
@include('main.navs.header')
<div class="container">
    <div class="error-page-container">
        <img src="{{ asset('img/essential/error_500.svg') }}" alt="error 500" class="main-img">
        <h3 class="description">Oops! Seems like something went wrong.<br>
            Would you be so kind to tell us what led you to this error?</h3>
        <form action="{{ route('errorFeedback') }}" method="POST">
            @csrf
            @auth
                <input type="hidden" name="email" value="{{ auth()->user()->email }}">
            @endauth

            <textarea name="feedback" cols="30" rows="10" placeholder="Specify what led you to this error"></textarea>
            <div class="d-flex justify-content-end align-items-center">
                <a href="{{ route('/') }}" class="d-inline-block inactive mr-2">Return to Board</a>
                <button type="submit" class="button--primary link disableOnSubmitButton">Submit and return to Board</a>
            </div>
        </form>
    </div>
</div>
@endsection
