<?php

use App\Question;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class QuestionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $questions = array(
            0 => array('id' => '1', 'user_id' => '1', 'question' => 'Tube P contain more nitrogenous waste compared to tube Q. Explain why', 'description' => '', 'subject_id' => '', 'img' => '', 'slug' => 'tube-p-contain-more-nitrogenous-waste-compared-to-tube-q-explain-why'),
            1 => array('id' => '2', 'user_id' => '1', 'question' => 'How to prepare 0.1 mol/dm^3 of hydrochloric acid?', 'description' => '', 'subject_id' => '', 'img' => '', 'slug' => 'how-to-prepare-01-moldm3-of-hydrochloric-acid'),
            2 => array('id' => '3', 'user_id' => '1', 'question' => 'Chemical reaction between zinc with ethanoic acid', 'description' => '', 'subject_id' => '', 'img' => '', 'slug' => 'chemical-reaction-between-zinc-with-ethanoic-acid'),
            3 => array('id' => '4', 'user_id' => '1', 'question' => 'Compare the boiling point of water and potassium oxide. Explain your answer.', 'description' => '', 'subject_id' => '', 'img' => '', 'slug' => 'compare-the-boiling-point-of-water-and-potassium-oxide-explain-your-answer'),
            4 => array('id' => '5', 'user_id' => '1', 'question' => 'State two condition needed to produce ammonia in Haber Process.', 'description' => '', 'subject_id' => '', 'img' => '', 'slug' => 'state-two-condition-needed-to-produce-ammonia-in-haber-process'),
            5 => array('id' => '6', 'user_id' => '1', 'question' => 'What is the name of the solution where the concentration of the solution is accurately known?', 'description' => '', 'subject_id' => '', 'img' => '', 'slug' => 'what-is-the-name-of-the-solution-where-the-concentration-of-the-solution-is-accurately-known'),
            6 => array('id' => '7', 'user_id' => '1', 'question' => 'The ball of mass 0.15 kg moves with a velocity 30 ms-1 when it hits the glove with the time of impact is 2 x 10-2 s. Calculate the impulsive force acting on the glove.', 'description' => '', 'subject_id' => '', 'img' => '', 'slug' => 'the-ball-of-mass-015-kg-moves-with-a-velocity-30-ms-1-when-it-hits-the-glove-with-the-time-of-impact-is-2-x-10-2-s-calculate-the-impulsive-force-acting-on-the-glove'),
            7 => array('id' => '8', 'user_id' => '1', 'question' => 'What is meant by specific latent heat?', 'description' => '', 'subject_id' => '', 'img' => '', 'slug' => 'what-is-meant-by-specific-latent-heat'),
            8 => array('id' => '9', 'user_id' => '1', 'question' => 'Why is diamond ring shinier than glass ring?', 'description' => '', 'subject_id' => '', 'img' => '', 'slug' => 'why-is-diamond-ring-shinier-than-glass-ring'),
            9 => array('id' => '10', 'user_id' => '2', 'question' => 'Explain how the G-M tube works to detect the radioactivity of radioactive substances.', 'description' => '', 'subject_id' => '', 'img' => '', 'slug' => 'explain-how-the-g-m-tube-works-to-detect-the-radioactivity-of-radioactive-substances'),
            10 => array('id' => '11', 'user_id' => '2', 'question' => 'What is the meaning of “240 V, 2200 W” for the electric kettle?', 'description' => '', 'subject_id' => '', 'img' => '', 'slug' => 'what-is-the-meaning-of-240-v-2200-w-for-the-electric-kettle'),
            11 => array('id' => '12', 'user_id' => '2', 'question' => 'Suggest a suitable setting of the speakers so that sound can be heard clearly throughout the hall.', 'description' => '', 'subject_id' => '', 'img' => '', 'slug' => 'suggest-a-suitable-setting-of-the-speakers-so-that-sound-can-be-heard-clearly-throughout-the-hall'),
            12 => array('id' => '13', 'user_id' => '2', 'question' => 'Which of the following are the types of interaction between organism P, Q and R?', 'description' => '', 'subject_id' => '', 'img' => '', 'slug' => 'which-of-the-following-are-the-types-of-interaction-between-organism-p-q-and-r'),
            13 => array('id' => '14', 'user_id' => '2', 'question' => 'Explain the regulatory mechanism of oxygen in the body when you are climbing a mountain.', 'description' => '', 'subject_id' => '', 'img' => '', 'slug' => 'explain-the-regulatory-mechanism-of-oxygen-in-the-body-when-you-are-climbing-a-mountain'),
            14 => array('id' => '15', 'user_id' => '2', 'question' => 'Which of the following stimulates ovulation during the menstrual cycle?', 'description' => '', 'subject_id' => '', 'img' => '', 'slug' => 'which-of-the-following-stimulates-ovulation-during-the-menstrual-cycle'),
            15 => array('id' => '16', 'user_id' => '2', 'question' => 'Which of the following shows deficiency in vitamin D for a long period of time?', 'description' => '', 'subject_id' => '', 'img' => '', 'slug' => 'which-of-the-following-shows-deficiency-in-vitamin-d-for-a-long-period-of-time'),
        );
        
        foreach ($questions as $value) {

            $data = array(
                'id' => $value['id'],
                'user_id' => $value['user_id'],
                'question' => $value['question'],
                'description' => $value['description'],
                'subject_id' => $value['subject_id'],
                'img' => $value['img'],
                'slug' => $value['slug'],
            );

            $data_exists = Question::find($value['id']);
            if (!is_null($data_exists)) {
                $data['updated_at'] = date("Y-m-d H:i:s");
                Question::where('id', $value['id'])
                        ->update($data); //UPDATE
            } else {
                $data['created_at'] = date("Y-m-d H:i:s");
                $data['updated_at'] = date("Y-m-d H:i:s");
                Question::create($data); //CREATE
            }
        }

    }
}
