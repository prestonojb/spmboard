@extends('classroom.layouts.app')

@section('meta')
<title>Questions | {{ config('app.name') }}</title>
@endsection

@section('styles')
@endsection

@section('content')

@component('classroom.common.breadcrumbs')
    <a href="{{ route('classroom.classrooms.show', $classroom->id) }}">Dashboard</a> /
    Questions
@endcomponent

@component('classroom.common.page_heading', ['title' => 'Questions'])
@endcomponent

<div class="row">
    @foreach($questions as $question)
        <div class="col-12 col-lg-6">
            @include('teacher.questions.common.qi-question-block')
        </div>
    @endforeach
</div>

@endsection

@section('scripts')
<script>
$(function(){
    $('.answer-button').click(function(){
        var url = $(this).data('url');
        window.location.href = url + '?answerFocus=true';
    });
});
</script>
@endsection
