<div class="smart-bar-container bg-blue1 text-center py-1">
    <div class="container">
        <div class="smart-bar">
            <div class="d-none d-lg-block"></div>
            <div class="text py-2">
                {{ $title }} <a target="_blank" href="{{ $url }}" class="underline-on-hover"><b>Learn more </b></a>
            </div>
            <div></div>
        </div>
    </div>
</div>
