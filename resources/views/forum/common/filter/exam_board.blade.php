
<select class="switch-exam-board-select maxw-150px" name="exam_board" style="width:85px;">
    @foreach(\App\ExamBoard::all() as $board)
        <option value="{{ $board->slug }}" data-url="{{ route('questions.index', $board->slug) }}" @if($board->slug == $exam_board->slug) selected @endif>{{ $board->name }}</option>
    @endforeach
</select>
