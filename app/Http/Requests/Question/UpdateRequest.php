<?php

namespace App\Http\Requests\Question;

use Illuminate\Foundation\Http\FormRequest;
use App\Question;


class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function withValidator($validator)
    {
        //Check if question slug is unique only when it is changed
        $validator->after(function($validator){
            //Slug for before and after change question
            $initial_question_slug = $this->route('question')->slug;
            $question_slug = str_slug($this->input('question'));

            //Validate if slug unique when question slug is changed
            if($question_slug !== $initial_question_slug){
                if(Question::where('slug', $question_slug)->exists()){
                    $validator->errors()->add('question', 'This question has already been asked.');
                }
            }
        });
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'question' => 'required|max:191',
            'description' => 'required',
            'subject' => 'required',
            'chapter' => 'required',
        ];

    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
