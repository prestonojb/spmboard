<?php

use App\Notes;
use App\PastPaper;
use App\Resource;
use App\TopicalPastPaper;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;

class ResourcesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Resource::truncate();
        PastPaper::truncate();
        TopicalPastPaper::truncate();
        Notes::truncate();

        factory(App\PastPaper::class, 200)->create();
        factory(App\TopicalPastPaper::class, 50)->create();
        factory(App\Notes::class, 50)->create();

        Artisan::call('sync:resources');
    }
}
