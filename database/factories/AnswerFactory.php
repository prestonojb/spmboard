<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Answer;
use Faker\Generator as Faker;

$factory->define(Answer::class, function (Faker $faker) {
    return [
        'user_id' => mt_rand(1, 10),
        'answer' => $faker->sentence,
        'checked' => $faker->boolean
    ];
});
