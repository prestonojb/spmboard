@extends('resources.layouts.app')

@section('meta')
<title>Create Worksheet | {{ $exam_board->name }} {{ config('app.name') }} </title>
@endsection


@section('header')
    @component('resources.common.header', ['type' => 'exam_builder', 'exam_board' => $exam_board])
    @endcomponent
@endsection

@section('breadcrumbs')
    @component('resources.common.breadcrumbs')
        <a href="{{ route('exam_builder.landing').'?exam_board='.$exam_board->slug }}">Home</a> /
        Create Worksheet
    @endcomponent
@endsection

@section('context_panel')
    @component('resources.common.context_panel', ['exam_board' => $exam_board->name, 'title' => 'Create Worksheet', 'description' => 'Create your personalised worksheet.'])
    @endcomponent
@endsection

@section('body')
    {{-- Body --}}
    <div class="container">
        {{-- Steps nav --}}
        @component('exam_builder.common.steps', ['steps' => 1, 'exam_board' => $exam_board])
        @endcomponent

        <div class="panel">
            @include('exam_builder.forms.filter')
        </div>
    </div>
@endsection

@section('scripts')
<script>
    var examBuilderChapterLimit = "{{ auth()->user()->getExamBuilderChapterLimit() }}";
</script>
@endsection
