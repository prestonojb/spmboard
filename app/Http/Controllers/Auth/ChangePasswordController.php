<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use App\Rules\MatchOldPassword;
use App\Rules\DifferentPassword;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class ChangePasswordController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function change()
    {
        $user = auth()->user();

        // User has existing password
        if(!is_null($user->password)) {
            request()->validate([
                'old_password' => ['required', new MatchOldPassword],
                'new_password' => ['required', 'confirmed', 'min:8', new DifferentPassword],
            ]);
        // User does not have password
        } else {
            request()->validate([
                'new_password' => ['required', 'confirmed', 'min:8'],
            ]);
        }

        $user->password = Hash::make(request('new_password'));
        $user->save();

        return back()->withSuccess('Password changed successfully!');

    }

}
