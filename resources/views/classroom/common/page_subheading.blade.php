<div class=" pb-2 mb-3 border-bottom border-gray3">
    <div class="d-flex justify-content-between align-items-center">
        <h3 class="mb-0"><b>{{ $title }}</b></h3>
        {{ isset($button_group) ? $button_group : '' }}
    </div>
</div>
