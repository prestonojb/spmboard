@extends('classroom.layouts.app')

@section('content')
@component('classroom.common.breadcrumbs')
    <a href="{{ route('classroom.landing') }}">Home</a> /
    @if( auth()->user()->is_teacher() )
        <a href="{{ route('classroom.students.index') }}">Students</a> /
        {{ $student->name }}
    @elseif( auth()->user()->is_student() )
        My Progress
    @elseif( auth()->user()->is_parent() )
        <a href="{{ route('classroom.children.index') }}">Children</a> /
        {{ $student->name }}
    @endif
@endcomponent

@component('classroom.students.common.details', ['student' => $student])
@endcomponent

<hr class="border-secondary mt-2">
<div class="py-2"></div>

{{-- Progress --}}
@component('classroom.common.page_subheading', ['title' => 'Progress'])
@endcomponent
{{-- Classroom filter --}}
<div class="mb-3">
    <select name="class_selected" id="" class="maxw-250px">
        @foreach($classrooms as $room)
            <option value="{{ $room->id }}" @if( isset($class_selected) && $room->id == $class_selected->id ) selected @endif>{{ $room->name }}</option>
        @endforeach
    </select>
</div>

<div class="row mb-5">
    <div class="col-12 col-lg-6">
        @component('classroom.students.common.resource_post_panel', ['student' => $student, 'class_selected' => $class_selected])
        @endcomponent
    </div>

    <div class="col-12 col-lg-6">
        @component('classroom.students.common.homework_panel')
        @endcomponent
    </div>
</div>

{{-- Reports --}}
@can('index', App\StudentReport::class)
    <div class="mb-5">
        @component('classroom.common.page_subheading', ['title' => 'Reports'])
            @slot('button_group')
                @can('store', App\StudentReport::class)
                    <a href="{{ route('classroom.reports.create').'?student='.$student->user_id }}" class="button--primary--inverse sm"><i class="fas fa-plus"></i> Create Report</a>
                @endcan
            @endslot
        @endcomponent

        @component('classroom.reports.common.table', ['reports' => $reports, 'show_minimal' => true])
        @endcomponent

        <div class="py-1"></div>

        <a href="{{ route('classroom.reports.index') }}" class="underline-on-hover">View all reports</a>
    </div>
@endcan

{{-- Invoices --}}
{{-- @can('index', App\TeacherInvoice::class)
    <div class="mb-5">
        @component('classroom.common.page_subheading', ['title' => 'Invoices'])
            @slot('button_group')
                @can('store', App\TeacherInvoice::class)
                    <a href="{{ route('classroom.invoices.create').'?student='.$student->user_id }}" class="button--primary--inverse sm"><i class="fas fa-plus"></i> Create Invoice</a>
                @endcan
            @endslot
        @endcomponent

        @component('classroom.invoices.common.table', ['invoices' => $invoices, 'show_minimal' => true])
        @endcomponent

        <div class="py-1"></div>

        <a href="{{ route('classroom.invoices.index') }}" class="underline-on-hover">View all invoices</a>
    </div>
@endcan --}}


@endsection

@section('scripts')
<script>
var homeworkMarksTimeLabels = @json( array_keys($homework_marks_data) );
var homeworkMarksTimeData = @json( array_values( $homework_marks_data) );
$(function () {
    $('#startDtPicker').datetimepicker({
        format: 'L',
    });
    $('#endDtPicker').datetimepicker({
        useCurrent: false,
        format: 'L',
    });
    $("#startDtPicker").on("change.datetimepicker", function (e) {
        $('#endDtPicker').datetimepicker('minDate', e.date);
    });
    $("#endDtPicker").on("change.datetimepicker", function (e) {
        $('#startDtPicker').datetimepicker('maxDate', e.date);
    });

    function getParameterByName(name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, '\\$&');
        var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, ' '));
    }

    $('[name=class_selected]').change(function(e){
        e.preventDefault();
        var baseUrl = '{{ url()->current() }}';
        var currentUrl = '{{ url()->full() }}';

        var class_selected = e.target.value;

        var params = {
            'class_selected': class_selected
        };

        location.href = baseUrl + '?' + jQuery.param(params);
    });
});
</script>
@endsection
