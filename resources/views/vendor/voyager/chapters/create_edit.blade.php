@extends('voyager::master')

@php
$page_title = $is_edit ? 'Edit Chapter' : 'Create Chapter';
@endphp
@section('page_title', $page_title)

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_header')
<div class="container-fluid">
    <h1 class="page-title">
        {{ $page_title }}
    </h1>
</div>
@stop

@section('content')
    <div class="page-content container-fluid">
        @component('vendor.voyager.chapters.common.form', ['is_edit' => $is_edit, 'subjects' => $subjects, 'chapter' => isset($chapter) ? $chapter : null])
        @endcomponent
    </div>
@stop

@section('javascript')
<script src="{{ mix('js/forum.js') }}"></script>
<script src="https://raw.githack.com/SortableJS/Sortable/master/Sortable.js"></script>
<script>
$(function(){
    // Simple list
    Sortable.create(subchapterList, {
    scroll: true, // Enable the plugin. Can be HTMLElement.
    scrollSensitivity: 30, // px, how near the mouse must be to an edge to start scrolling.
    scrollSpeed: 10, // px, speed of the scrolling
    bubbleScroll: true // apply autoscroll to all parent elements, allowing for easier movement
});

    var $subchapterList = $('#subchapterList');

    $('#orderSubchapterForm button[type=submit]').click(function(e){
        e.preventDefault();
        $(this).prop('disabled', true);

        // Fill chapterOrder data
        var chapterOrderArr = [];

        $.each($subchapterList.find('.list-group-item'), function (idx, item) {
            chapterOrderArr.push($(this).data('chapter-id'));
        });

        $('[name=chapter_order]').val( JSON.stringify(chapterOrderArr) );
        $(this).closest('form').submit();
    });
});
</script>
@stop
