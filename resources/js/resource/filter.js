$(function(){
    $('.purchaseResourceButton').click(function(e){
        e.preventDefault();
        var form = e.target.form;
        var resource_name = $(this).data('resource-name');
        Swal.fire({
            title: 'Are you sure?',
            text: "You will be purchasing the " + product_name + " coin pack for MYR" + price + "!",
            type: 'warning',
            showCancelButton: true,
            reverseButtons: true,
            confirmButtonText: 'Confirm Purchase',
            cancelButtonText: 'Cancel',
        }).then(function(result){
            if(result.value){
                form.submit();
            }
        });
    });

    // Topical Paper
    if( $('.paper-select').length > 0 ) {
        var paperSelect = new SlimSelect({
            select: '.paper-select',
            placeholder: 'Paper',
            allowDeselectOption: true,
        });
    } else {
        var paperSelect = null;
    }

    if( $('.chapter-select').length > 0 ) {
        var chapterSelect = new SlimSelect({
            select: '.chapter-select',
            placeholder: 'Select Chapter',
            allowDeselectOption: true
        });
    } else {
        var chapterSelect = null;
    }

    if( $('.year-select').length > 0 ) {
        var yearSelect = new SlimSelect({
            select: '.year-select',
            placeholder: 'Year',
            limit: 5,
            allowDeselectOption: true,
            closeOnSelect: false
        });
    };

    if( $('.paper-select').length > 0 ) {
        var paperSelect = new SlimSelect({
            select: '.paper-select',
            limit: 5,
            placeholder: 'Paper Number',
            allowDeselectOption: true,
            closeOnSelect: false
        });
    };

    if ($('.season-select').length > 0) {
        var seasonSelect = new SlimSelect({
            select: '.season-select',
            limit: 3,
            placeholder: 'Season',
            allowDeselectOption: true,
            closeOnSelect: false
        });
    }

    if ($('.variant-select').length > 0) {
        var variantSelect = new SlimSelect({
            select: '.variant-select',
            limit: 3,
            placeholder: 'Variant',
            allowDeselectOption: true,
            closeOnSelect: false
        });
    }

    if( $('.chapter-select').length > 0 ) {
        var chapterSelect = new SlimSelect({
            select: '.chapter-select',
            placeholder: 'Select Chapter',
            allowDeselectOption: true
        });
    }


    $('.clear-filter').click(function () {
        if( typeof yearSelect !== 'undefined' ) {
            yearSelect.set([]);
        }
        if( typeof paperSelect !== 'undefined' ) {
            paperSelect.set([]);
        }
        if( typeof seasonSelect !== 'undefined' ) {
            seasonSelect.set([]);
        }
        if( typeof variantSelect !== 'undefined' ) {
            variantSelect.set([]);
        }
        if( typeof chapterSelect !== 'undefined' ) {
            chapterSelect.set([]);
        }
        $('input[name="exclude_reference"]').prop('checked', false);
    });
});
