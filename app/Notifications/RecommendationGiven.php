<?php

namespace App\Notifications;

use App\Recommendation;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\HtmlString;

class RecommendationGiven extends Notification implements ShouldQueue
{
    use Queueable;

    public $recommendation;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Recommendation $recommendation)
    {
        $this->recommendation = $recommendation;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->greeting("Hi {$notifiable->name}!")
                    ->line(new HtmlString("<b>{$this->recommendation->recommender->name}</b> gave you a recommendation!"))
                    ->action('Learn More', $this->recommendation->recommendee->show_url);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'type' => Recommendation::class,
            'target' => $this->recommendation,
            'title' => "<b>{$this->recommendation->recommender->name}</b> gave you a recommendation!",
        ];
    }
}
