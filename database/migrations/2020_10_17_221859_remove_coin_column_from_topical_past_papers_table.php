<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveCoinColumnFromTopicalPastPapersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('topical_past_papers', function (Blueprint $table) {
            $table->removeColumn('coin');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('topical_past_papers', function (Blueprint $table) {
            $table->unsignedInteger('coin')->default(5);
        });
    }
}
