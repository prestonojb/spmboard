<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Mail;
use App\Mail\ContactUs;

class ContactUsController extends Controller
{
    public function createEmail()
    {
        return view('contact_us');
    }

    public function sendEmail()
    {
        request()->validate([
           'name' => 'required',
           'email' => 'required|email',
           'subject' => 'required',
           'body' => 'required',
        ]);

        // Declared variable first to allow mutating values of $request
        $data = request()->all();

        // Swap 'others subject' with 'subject'
        if ( request()->subject == 'Others' ) {
            request()->validate([
                'others_subject' => 'required',
            ]);
            $data['subject'] = request()->others_subject;
        }

        Mail::to(config('mail.from.address'))->send(new ContactUs($data));

        return redirect()->back()->withSuccess('Thanks for your email! We will try to reply you ASAP!');
    }
}
