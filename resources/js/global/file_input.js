function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            $('.file-input-preview').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}

$(function(){
    var $fileInput = $('.file-input');
    var $fileInputPreview = $('.file-input-preview');
    var $droparea = $('.file-drop-area');

    // highlight drag area
    $fileInput.on('dragenter focus click', function() {
        $droparea.addClass('is-active');
    });

    // back to normal state
    $fileInput.on('dragleave blur drop', function() {
        $droparea.removeClass('is-active');
    });

    // change inner text
    $fileInput.on('change', function(e) {
        e.preventDefault();
        var filesCount = $(this)[0].files.length;
        var $textContainer = $(this).prev();

        var fileExt = $(this).val().split('.').pop();
        var imageExt = ['jpg', 'jpeg', 'png', 'gif', 'svg'];

        //Check file format
        if(!imageExt.includes(fileExt)){
            if(fileExt !== ''){
                $(this).val('');
                $textContainer.text('or drag and drop files here');
                alert('Only .jpg, .jpeg, .png, .gif, .svg image files can be uploaded!');
            } else{
                $textContainer.text('or drag and drop files here');
            }
            $fileInputPreview.attr('src', '');
        } else{
            if (filesCount === 1) {
                // if single file is selected, show file name
                var fileName = $(this).val().split('\\').pop();
                $textContainer.text(fileName);
            } else {
                // otherwise show number of files
                $textContainer.text(filesCount + ' files selected');
            }
            readURL(this);
        }
    });

    $('.clearFileInputButton').click(function(e){
        e.preventDefault();
        if( $fileInput.val() ) {
            $fileInput.val('');
            $fileInput.trigger('change');
            if( logoUrl ) {
                $fileInputPreview.attr('src', logoUrl);
            } else {
                $fileInputPreview.attr('src', logoPlaceholderUrl);
            }
        }
    });

    $('.removeFileButton').click(function(e){
        e.preventDefault();
        $('[name=remove_file]').val(1);
        $fileInput.val('');
        $fileInput.trigger('change');
        $fileInputPreview.attr('src', logoPlaceholderUrl);
    });
});
