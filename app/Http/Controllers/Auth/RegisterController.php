<?php

namespace App\Http\Controllers\Auth;

use App\ExamBoard;
use App\User;
use App\Http\Controllers\Controller;
use App\Mail\EbooksPromotion;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Notifications\NewUser;
use App\Notifications\ReferralReward;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;
use Amplitude;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        // Reward point reward for first # new users
        $promotion_is_available = setting('rp_promotion_slots_available') > 0;

        return view('auth.register', compact('promotion_is_available'));
    }

    protected function registered(Request $request, $user)
    {
        return redirect( $user->getGettingStartedPath() );
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'username' => ['required', 'string', 'max:30', 'unique:users'],
            'email' => ['required', 'email', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'account_type' => ['required'],
            'referral_code' => ['nullable', 'string', 'min:10', 'max:10'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        // Referral Code VALIDATION
        // Add RP if referral code is valid
        if( request()->has('referral_code') && !is_null(request('referral_code')) ) {
            $referral = User::getByReferralCode( request('referral_code') );
            if(is_null($referral)) {
                return redirect()->withErrors(['referral_code', 'Referral Code is invalid.']);
            }
        } else {
            $referral = null;
        }

        // Create user
        $user = User::create([
            'username' => $data['username'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);

        $user->createAccountType( $data['account_type'] );

        // Add RP if there is referral
        if(!is_null($referral)) {
            $referee = $user;
            try {
                // Add RP to referral
                $referral->addRewardPointAction("Refer a friend", setting('marketing.referral_rp_amount'));
                Notification::send($referral, new ReferralReward($referral, $referee, true));
            } catch (\Exception $e) {}
            try {
                // Add RP to referee
                $referee->addRewardPointAction('Getting referred by a friend', setting('marketing.referral_rp_amount'));
                Notification::send($referee, new ReferralReward($referral, $referee, false));
            } catch (\Exception $e) {}
        }

        // Log event on Amplitude
        Amplitude::setUserId($user->id);
        Amplitude::setUserProperties([
            'email' => $user->email,
            'account_type' => $user->getUserAccountType(),
        ]);
        Amplitude::queueEvent('register', ['method' => 'email']);

        return $user;

        // // Save user preferred subjects
        // $user->subjects()->attach( request()->preferred_subjects );

        // // Reward point reward for first # new users
        // $promotion_is_available = setting('marketing.rp_promotion_slots_available') > 0;

        // if( $promotion_is_available ) {
        //     $user->update(['reward_point_balance' => 100]);
        //     // Decrement RP promotion slots by 1
        //     DB::table('settings')->where('key', 'marketing.rp_promotion_slots_available')->decrement('value', 1);

        //     // Flash message to user
        //     Session::flash('rp_promotion_awarded', 'You are awarded '. setting('marketing.rp_promotion_amount') .' Reward Points for signing up!');
        // }

        // // E-books promotion
        // $ebooks_promotion_is_available = setting('marketing.ebooks_promotion');

        // if( $ebooks_promotion_is_available ) {
        //     $subjects = $user->subjects;
        //     if(count($subjects) > 0) {
        //         // Send ebooks via email
        //         Mail::to($user->email)->send(new EbooksPromotion($user->exam_board, $subjects));
        //         // Flash message to user
        //         Session::flash('ebooks_promotion_awarded', "Your Preferred Subjects' E-books are sent to your email!");
        //     }
        // }
    }
}
