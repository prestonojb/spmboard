@extends('classroom.layouts.app')

@section('content')

@component('classroom.common.breadcrumbs')
    <a href="{{ route('classroom.questions', $classroom->id) }}">Questions</a> /
    Create
@endcomponent

@component('classroom.common.page_heading', ['title' => 'Ask a Question'])
@endcomponent


<div class="panel mb-2">
    <form action="{{ route('classroom.questions.store', $classroom->id) }}" method="POST" id="createClassroomQuestionForm">
        @csrf
        <input type="hidden" name="is_public" value="1">
        <button type="button" class="button-link" data-toggle="modal" data-target="#toModal"><span id="toStatus">To: Public(Forum)</span></button>

        <div class="form-field">
            <label>Question</label>
            <input name="question" placeholder="e.g What is the powerhouse of the cell?" value="{{ old('question') }}" required autocomplete="off">

            @error('question')
                <span class="error-message">{{ $message }}</span>
            @enderror
        </div>

        <div class="form-field">
            <label>Image <span style="font-weight: 400">(Optional)</span></label>
            <div class="file-drop-area">
                <span class="fake-btn">Choose files</span>
                <span class="file-msg">or drag and drop files here</span>
                <input class="file-input" name="img" type="file">
            </div>
            <img class="file-input-preview">
            @error('img')
                <span class="error-message">{{ $message }}</span>
            @enderror
        </div>

        <div class="form-field">
            <label>Description</label>
            <div class="collapse" id="descriptiontips">
                <div class="alert alert-primary" role="alert">
                    Stuff you can list here:
                    <ul class="m-0">
                        <li>The source of the question(e.g MRSM 2019 Paper 2 Question 5)</li>
                        <li>What you have attempted to solve the question</li>
                        <li>Additional details you want to be provided by the answerer</li>
                    </ul>
                </div>
            </div>
            <div class="editor">{!! old('description') !!}</div>
            <input type="hidden" name="description">
            @error('description')
                <span class="error-message">{{ $message }}</span>
            @enderror
        </div>

        <div class="form-field">
            <label>Chapters(Optional)</label>
            <select class="chapter-select" name="chapter">
                <option value="" disabled selected>Select Chapter</option>
                @foreach($classroom->subject->chapters as $chapter)
                    <option value="{{ $chapter->id }}">
                        {{ $chapter->name }}
                    </option>
                @endforeach
            </select>
            @error('chapter')
                <span class="error-message">{{ $message }}</span>
            @enderror
        </div>

        <div class="text-center">
            <button type="submit" class="ask-button button--primary askButton">Ask</button>
        </div>

    </form>
</div>

{{-- Sort --}}
<div class="modal" id="toModal" tabindex="-1" role="dialog" aria-labelledby="toModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="list-group">
                <button class="list-group-item list-group-item-action isPublicButton active" data-is-public="true">Public(Forum)</button>
                <button class="list-group-item list-group-item-action isPublicButton" data-is-public="false">Private</button>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script>
$(function(){
    $('.isPublicButton').click(function(){
        if( $(this).data('is-public') == true ) {
            $('[name=is_public]').val(1);
        } else {
            $('[name=is_public]').val(0);
        }

        $('.isPublicButton').removeClass('active');
        $(this).addClass('active');

        $('#toModal').modal('hide');

        // Update status
        $('#toStatus').text( 'To: ' + $(this).text() );
    });
});
</script>
@endsection
