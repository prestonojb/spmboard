@extends('forum.layouts.app')
@section('meta')
    <title>2020 Stoichiometry Mania | {{ config('app.name') }}</title>
    <meta name="description" content="Score the highest in the mania and stand a chance to win a pair of Apple Airpods!">
    <meta property="og:image" content="{{ asset('img/events/2020_stoichiometry_mania.jpeg') }}" />
    <meta property="og:type" content="website" />
@endsection

@section('content')
@include('main.navs.header')
<main class="quiz">
    <header class="quiz__header">
        <div class="container">
            <h1 class="heading">2020 Stoichiometry Mania</h1>
            <h2 class="sub-heading">Score the highest in the mania and stand a chance to win a pair of <b>Apple Airpods</b>!</h2>
            <p class="body">By entering the Rules Quiz, each entrant agrees to be <br>
                bound by these <button type="button" class="button-link blue4" data-toggle="modal" data-target="#quizTncModal" style="font-size: inherit;">terms and conditions</button>.</p>
            <p class="font-size6 mb-2"><b>14/8/2020(Friday) 12:00AM - 16/8/2020(Sunday) 11:59PM</b></p>


            <button class="button--primary lg startQuizButton">
                <span class="iconify" data-icon="entypo:controller-play" data-inline="false"></span>
                Register for Mania
            </button>
        </div>
    </header>


    <section class="quiz__body my-4" id="quizBody">
        <div class="container">

            <h2 class="heading">Be ready for Stoichiometry Mania!</h2>
            <h3 class="sub-heading mb-1">Score the highest in the mania and stand a chance to win a pair of <b>Apple Airpods</b>!</h3>
            <p class="gray2">*Registration is required for your entry to the mania to be valid :) Failure to register may result in disqualification.</p>

            <div class="row">

                <div class="col-12 col-lg-8">

                    <div class="panel mt-2 mb-4">

                        <form action="{{ route('events.2020.stoichiometry_mania_register') }}" method="POST" id="mathQuizForm" class="steps-form">
                            @csrf
                            <div class="form-field">
                                <label>{{ __('Email') }}</label>
                                <input type="text" name="email" value="{{ auth()->user()->email }}" readonly >
                            </div>

                            <div class="form-field">
                                <label>{{ __('Name') }}</label>
                                <input type="text" name="name" required >
                                @error('name')
                                    <span class="error-message">{{ $message }}</span>
                                @enderror
                            </div>
                            <div class="form-field">
                                <label>{{ __('Phone Number') }}</label>
                                <input type="text" name="phone_number" value="{{ old('phone_number') }}" required autocomplete="phone_number" placeholder="Phone Number">
                                <p><small>* This is how we can notify you if you win the quiz!</small></p>
                                @error('phone_number')
                                    <span class="error-message">{{ $message }}</span>
                                @enderror
                            </div>

                            <div class="text-right">
                                <button type="submit" class="button--primary">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>

                {{-- Posters --}}
                <div class="col-12 col-lg-4">
                    <h3 class="h4 mb-2">Share to friends!</h3>
                    <div class="mb-2" id="shareIcons"></div>

                    <hr>

                    <div class="">
                        <h3 class="h4 mb-2">Need printed State Papers?</h3>
                        <a href="{{ route('physical_resources').'?utm_source=website&utm_campaign=board_quiz&utm_medium=quiz_poster_pre' }}" target="_blank">
                            <img src="{{ asset('img/phy_resources/ad_poster.png') }}" alt="">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>

</main>

<div class="modal fade" id="quizTncModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Terms and Condition</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        <div class="modal-body">
            <div class="mb-3">
                <h3 class="mb-1">Eligibility</h3>
                <p>The 2020 Stoichiometry Mania is open to current SPM and IGCSE students, further verification might be required to verify the validity of the contestants.</p>
                <p>Failure to verify your student identity might cause your entry to be disqualified.</p>
            </div>

            <div class="mb-3">
                <h3 class="mb-1">Data Consent</h3>
                <p>On submission of your completed entry, you consent to your email address being added to the Board database and periodically will be sent newsletters from Board. For the avoidance of doubt, your email address will not be displayed or used for any other purpose; and
                    you will be automatically bound by these terms and conditions.</p>
                <p>By entering the 2020 Stoichiometry Mania, the winner also consents to the publishing of their name on Board website(<a href="https://boardedu.org" target="_blank">https://boardedu.org</a>).</p>
            </div>

            <div class="mb-3">
                <h3 class="mb-1">Entry</h3>
                <p>Entry into the 2020 Stoichiometry Mania is <b>FREE</b>.</p>
            </div>

            <div class="mb-3">
                <h3 class="mb-1">Winning Conditions</h3>
                <p>To stand a chance to become the winners, entrants must achieve a score of 5 / 5 in the 2020 Board Quiz. In the event that two or more entrants tie for the first prize for the 2020 Board Quiz, those entrants will be entered into a draw. The winner will be the first entry drawn at random by Board.</p>
            </div>

            <div class="mb-3">

                <h3 class="mb-1">Prize Disclaimer</h3>
                <p>The Board Quiz prizes cannot be redeemed for cash or benefits in kind such as gift vouchers. The prizes are non-transferable.
                    In the unlikely event that no entries are received before the closing date the prizes will not be awarded.</p>
            </div>

            <div class="mb-3">
                <h3 class="mb-1">Prize Giving Mechanims</h3>
                <p>The winners of the 2020 Board Quiz will be notified by Whatsapp after the closing date. The winner will be asked to provide proof of student ID within 14 days of being notified or they will forfeit the prize.
                    Method of despatch of prizes will be further clarified on Whatsapp.</p>
            </div>

            <div class="mb-3">
                <h3 class="mb-1">Legal</h3>
                <p>To the fullest extent permitted by law, Board hereby excludes all warranties, representations, covenants and liability (whether express or implied) regarding the prize or a prize draw.</p>
            </div>

            <div class="mb-3">
                <h3 class="mb-1">Disclaimer</h3>
                <p>In the event of any dispute regarding the 2020 Stoichiometry Mania, the decision of Board shall be final and binding and no correspondence or discussion shall be entered into.
                    At any time prior to the closing date, Board may withdraw or cancel the 2020 Board Quiz for any reason and may change the prize to a prize of a similar value. Any such cancellation or change shall be notified on Board website.</p>
                <p>Board reserves the right to cancel the Board Quiz and reject redemption of any Board Quiz Prizes if it reasonably believes that the Board Quiz or any prize is being used unlawfully or illegally.</p>
            </div>

            <div class="mb-3">
                <h3 class="mb-1">Change of Terms</h3>
                <p>Board reserves the right to amend these terms and conditions without prior notice. Any changes will be posted on Board website located at <a href="https://boardedu.org" target="_blank">https://boardedu.org</a></p>
            </div>

        </div>
        <div class="modal-footer">
            <button type="button" class="button--primary--inverse" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-steps/1.1.0/jquery.steps.js"></script>
<script>
$(function(){
    $('.startQuizButton').click(function(e){
        e.preventDefault();
        $('html, body').animate({
            scrollTop: $("#quizBody").offset().top - 68
        }, 500);
    });

    jsSocials.shares.twitter = {
        logo: "fa fa-twitter",
        shareUrl: "https://twitter.com/share?url=" + "{{ route('events.2020.get_stoichiometry_mania_register_view') }}" + "&text=Score the highest in the mania and stand a chance to win a pair of Apple Airpods!",
    };

    jsSocials.shares.facebook = {
        logo: "fa fa-facebook",
        shareUrl: "https://www.facebook.com/share.php?u="+ "{{ route('events.2020.get_stoichiometry_mania_register_view') }}" +"&title=Score the highest in the mania and stand a chance to win a pair of Apple Airpods!",
    };

    jsSocials.shares.whatsapp = {
        logo: "fa fa-whatsapp",
        shareUrl: "https://api.whatsapp.com/send?text=Score the highest in the mania and stand a chance to win a pair of Apple Airpods! To register for Stoichiometry Mania, click this link: "+ "{{ route('events.2020.get_stoichiometry_mania_register_view') }}",
    };

    $("#shareIcons").jsSocials({
        showLabel: false,
        showCount: false,
        shares: ["twitter", "facebook", "whatsapp"],
        shareIn: "popup",
    });
});
</script>
@endsection
