<table class="table table-bordered bg-white mb-0">
    <thead>
        <tr>
            <th scope="col">Metric</th>
            <th scope="col">My Progress</th>
            <th scope="col">Certification Target</th>
            <th scope="col">Status</th>
        </tr>
    </thead>
    <tbody>
        @php
            $data = [
                ['title' => 'No. of answers',
                'description' => 'Number of answers contributed in the forum for the last 30 days',
                'progress' => $teacher->getCertificationMetrics()['no_of_answers'],
                'target' => $teacher->getCertificationMetricsTarget()['no_of_answers'],
                'passed' => $teacher->getCertificationMetrics()['no_of_answers'] >= $teacher->getCertificationMetricsTarget()['no_of_answers']],
                ['title' => 'No. of checked answers',
                'description' => 'Number of checked answers in the forum for the last 30 days',
                'progress' => $teacher->getCertificationMetrics()['no_of_checked_answers'],
                'target' => $teacher->getCertificationMetricsTarget()['no_of_checked_answers'],
                'passed' => $teacher->getCertificationMetrics()['no_of_checked_answers'] >= $teacher->getCertificationMetricsTarget()['no_of_checked_answers']],
                ['title' => 'No. of logins',
                'description' => 'Number of days logged in to Board for the last 30 days',
                'progress' => $teacher->getCertificationMetrics()['no_of_login_days'],
                'target' => $teacher->getCertificationMetricsTarget()['no_of_login_days'],
                'passed' => $teacher->getCertificationMetrics()['no_of_login_days'] >= $teacher->getCertificationMetricsTarget()['no_of_login_days']],
                ['title' => 'No. of articles',
                'description' => 'Number of articles written for the last 30 days',
                'progress' => $teacher->getCertificationMetrics()['no_of_articles'],
                'target' => $teacher->getCertificationMetricsTarget()['no_of_articles'],
                'passed' => $teacher->getCertificationMetrics()['no_of_articles'] >= $teacher->getCertificationMetricsTarget()['no_of_articles']],
                ['title' => 'No. of recommendations',
                'description' => 'Total number of recommendations',
                'progress' => $teacher->getCertificationMetrics()['no_of_recommendations'],
                'target' => $teacher->getCertificationMetricsTarget()['no_of_recommendations'],
                'passed' => $teacher->getCertificationMetrics()['no_of_recommendations'] >= $teacher->getCertificationMetricsTarget()['no_of_recommendations']],
            ];
        @endphp
        @foreach($data as $datum)
            <tr>
                <td>
                    {{ $datum['title'] }}
                    <button type="button" class="transparent gray3 p-0" data-toggle="popover" data-placement="right" data-content="{{ $datum['description'] }}"><i class="fa fa-info-circle" aria-hidden="true"></i></button></th>
                <td>{{ $datum['progress'] }}</td>
                <td>≥ {{ $datum['target'] }}</td>
                <td>
                    @if($datum['passed'])
                        <span class="green"><i class="fas fa-check-circle"></i></span> Passed
                    @else
                    <span class="gray3"><i class="fas fa-check-circle"></i></span> Didn't Pass
                    @endif
                </td>
            </tr>
        @endforeach
    </tbody>
  </table>
