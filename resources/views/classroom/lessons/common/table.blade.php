<table class="table panel p-0 lessonTable">
    <thead>
    <tr>
        <th scope="col">Start At</th>
        <th scope="col">End At</th>
        <th scope="col">Name</th>
        <th scope="col">Status</th>
        <th scope="col">Action</th>
    </tr>
    </thead>
    <tbody>
        @foreach( $lessons as $lesson )
            <tr>
                <td scope="row">{{ $lesson->start_at->format('M d, Y h:iA') }}</td>
                <td scope="row">{{ $lesson->end_at->format('M d, Y h:iA') }}</td>
                <td>{{ $lesson->name }}</td>
                <td>{!! $lesson->statusTagHtml !!}</td>
                <td>
                    <div class="d-flex">
                        @can('show', $lesson->classroom)
                            <a href="{{ route('classroom.lessons.show', [$lesson->classroom->id, $lesson->id]) }}" class="button--primary--inverse sm mr-1"><span class="iconify" data-icon="mdi:magnify-plus-outline" data-inline="false"></span></a>
                        @endcan
                        @can('update_store_lesson', $lesson->classroom)
                        <a href="{{ route('classroom.lessons.edit', [$lesson->classroom->id, $lesson->id]) }}" class="button--success--inverse sm mr-1"><span class="iconify" data-icon="feather:edit" data-inline="false"></span></a>
                        @endcan
                        @can('delete_lesson', $lesson->classroom)
                            <form action="{{ route('classroom.lessons.destroy', [$lesson->classroom->id, $lesson->id]) }}" method="post">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="button--danger--inverse sm deleteButton"><span class="iconify" data-icon="carbon:delete" data-inline="false"></span></button>
                            </form>
                        @endcan
                    </div>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>
