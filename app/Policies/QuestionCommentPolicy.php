<?php

namespace App\Policies;

use App\User;
use App\QuestionComment;
use Illuminate\Auth\Access\HandlesAuthorization;

class QuestionCommentPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function store(User $user)
    {
        return !$user->parent;
    }

    public function delete(User $user, QuestionComment $question_comment)
    {
        return $user->id == $question_comment->user_id;
    }

}
