@extends('main.layouts.app')

@section('meta')
<title>Submit Certification Application | {{ config('app.name') }}</title>
@endsection

@section('content')
@php
    $user = auth()->user();
@endphp
<div class="py-2"></div>

<div class="container">
    <div class="maxw-740px mx-auto">
        <a class="d-block mb-2 inactive font-size2 gray2" href="{{ route('certification_applications.index') }}">< Back to Certification Page</a>

        <h1 class="font-size7">Submit Certification Application</h1>
        <div class="panel">
            {{-- Form --}}
            <form action="{{ route('certification_applications.create') }}" method="POST" enctype="multipart/form-data">
                @csrf
                {{-- Resume --}}
                <div class="form-field">
                    <label>Resume</label>
                    <input class="" name="resume" type="file" accept=".pdf,.doc,.docx">
                    <p class="gray2">Accepted file format: .pdf, .doc, .docx</p>
                    @error('resume')
                        <span class="error-message">{{ $message }}</span>
                    @enderror
                </div>

                <div class="text-right">
                    <button type="submit" class="button--primary loadOnSubmitButton" style="width: 78px;">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection
