/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 2);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/js/resource.js":
/*!**********************************!*\
  !*** ./resources/js/resource.js ***!
  \**********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

$(function () {
  __webpack_require__(/*! ./resource/exam_builder */ "./resources/js/resource/exam_builder.js");

  __webpack_require__(/*! ./resource/filter */ "./resources/js/resource/filter.js");

  __webpack_require__(/*! ./resource/custom_pdf_viewer */ "./resources/js/resource/custom_pdf_viewer.js");

  __webpack_require__(/*! ./resource/library */ "./resources/js/resource/library.js"); // Coin Price Select


  var $coinPriceSelect = $('select[name=coin_price]');
  $('[name=coin_price_toggle]').change(function (e) {
    var $t = $(this);

    if ($t.val() == 'free') {
      // Reset Coin Price Select
      $coinPriceSelect.val(0);
      $coinPriceSelect.prop('disabled', true);
    } else if ($t.val() == 'paid') {
      $coinPriceSelect.prop('disabled', false);
    }
  }); // Bug Report Submit Button

  $('.bugReportSubmitButton').click(function (e) {
    e.preventDefault();
    var $t = $(this);
    var $modal = $('#bugReportModal');
    var $form = $t.closest('form');
    var initHtml = $(this).html();
    window.showLoader($t, 'in-button', 'white');
    $.ajax({
      url: $form.attr('action'),
      method: 'POST',
      data: $form.serialize(),
      success: function success(response) {
        if (response == 'OK') {
          toastr.success('Bug Report submitted successfully!');
          $modal.modal('hide');
        } else {
          toastr.error('An error occured!');
        }

        window.hideLoader($t, initHtml);
      },
      error: function error(e) {
        console.log(e);
        toastr.error('An error occured!');
        window.hideLoader($t, initHtml);
      }
    });
  });
  $('.resource__panel-dropdown-header').click(function (e) {
    $(this).toggleClass('active');
  });
  $(document).on('click', 'a[href^="#"]', function (e) {
    // target element id
    var id = $(this).attr('href'); // target element

    var $id = $(id);

    if ($id.length === 0) {
      return;
    } // prevent standard hash navigation (avoid blinking in IE)


    e.preventDefault(); // top position relative to the document

    var pos = $id.offset().top; // animated top scrolling

    $('body, html').animate({
      scrollTop: pos - 142
    });
  });
});

/***/ }),

/***/ "./resources/js/resource/custom_pdf_viewer.js":
/*!****************************************************!*\
  !*** ./resources/js/resource/custom_pdf_viewer.js ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _createForOfIteratorHelper(o) { if (typeof Symbol === "undefined" || o[Symbol.iterator] == null) { if (Array.isArray(o) || (o = _unsupportedIterableToArray(o))) { var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e) { throw _e; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var it, normalCompletion = true, didErr = false, err; return { s: function s() { it = o[Symbol.iterator](); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e2) { didErr = true; err = _e2; }, f: function f() { try { if (!normalCompletion && it["return"] != null) it["return"](); } finally { if (didErr) throw err; } } }; }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(n); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

// Loading URL only works in JS file, throws CORS error if used in Laravel Blade
var pdfViewers = document.getElementsByClassName('custom_pdf_viewer');
var pdfViewerPreview = document.getElementsByClassName('custom_pdf_viewer_preview');
var pdfRendererHtml = '<canvas class="pdf_renderer w-100 border box-shadow1 mb-1"></canvas>';
var loadingIconHtml = '<div class="my-5 loading-icon loading in-editor blue"></div>';
var zoom = 2; // Renders PDF currentPage on chosen canvas

function render(pdf, currentPage, canvas) {
  pdf.getPage(currentPage).then(function (page) {
    var ctx = canvas.getContext('2d');
    var viewport = page.getViewport(zoom); // Set dimensions of canvas

    canvas.width = viewport.width;
    canvas.height = viewport.height;
    page.render({
      canvasContext: ctx,
      viewport: viewport
    });
  });
} // PDF preview viewer


var _iterator = _createForOfIteratorHelper(pdfViewerPreview),
    _step;

try {
  for (_iterator.s(); !(_step = _iterator.n()).done;) {
    var pdfViewer = _step.value;
    var pdfUrl = pdfViewer.getAttribute('data-url');
    var maxPagePreview = parseInt(pdfViewer.getAttribute('data-max-page-preview')); // Initialise loading icon to PDF renderer

    pdfViewer.insertAdjacentHTML('beforeend', loadingIconHtml);
    var loadingIcon = pdfViewer.querySelector('.loading-icon');
    pdfjsLib.getDocument(pdfUrl).then(function (pdf) {
      var totalPages = pdf.numPages;
      var pagesForPreview = Math.min(maxPagePreview, totalPages); // Render first PDF page only

      for (currentPage = 1; currentPage <= pagesForPreview; currentPage++) {
        // Clone a PDF renderer <div> and append it to the end of <canvas> element
        loadingIcon.insertAdjacentHTML('beforebegin', pdfRendererHtml); // Get last PDF render as canvas

        var pdf_renderers = document.getElementsByClassName("pdf_renderer");
        var length = pdf_renderers.length;
        var canvas = pdf_renderers[length - 1]; // Render current page on last PDF renderer <div>

        render(pdf, currentPage, canvas);
      }

      loadingIcon.remove();
    });
  } // PDF viewer

} catch (err) {
  _iterator.e(err);
} finally {
  _iterator.f();
}

var _iterator2 = _createForOfIteratorHelper(pdfViewers),
    _step2;

try {
  for (_iterator2.s(); !(_step2 = _iterator2.n()).done;) {
    var _pdfViewer = _step2.value;

    var pdfUrl = _pdfViewer.getAttribute('data-url'); // Initialise loading icon to PDF renderer


    _pdfViewer.insertAdjacentHTML('beforeend', loadingIconHtml);

    var loadingIcon = _pdfViewer.querySelector('.loading-icon');

    pdfjsLib.getDocument(pdfUrl).then(function (pdf) {
      // Get total number of pages
      var totalPages = pdf.numPages; // Render all PDF pages

      for (currentPage = 1; currentPage <= totalPages; currentPage++) {
        // Clone a PDF renderer <div> and append it to the end of <canvas> element
        loadingIcon.insertAdjacentHTML('beforebegin', pdfRendererHtml); // Get last PDF render as canvas

        var pdf_renderers = document.getElementsByClassName("pdf_renderer");
        var length = pdf_renderers.length;
        var canvas = pdf_renderers[length - 1]; // Render current page on last PDF renderer <div>

        render(pdf, currentPage, canvas);
      }

      loadingIcon.remove();
    });
  } // document.getElementById('go_previous')
  //     .addEventListener('click', (e) => {
  //         if(myState.pdf == null
  //             || myState.currentPage == 1) return;
  //         myState.currentPage -= 1;
  //         document.getElementById("current_page")
  //                 .value = myState.currentPage;
  //         // Set PDF viewer to top
  //         canvasContainer.scrollTop = 0;
  //         render();
  //     });
  // document.getElementById('go_next')
  //     .addEventListener('click', (e) => {
  //         if(myState.pdf == null
  //         || myState.currentPage >= myState.pdf._pdfInfo.numPages)
  //         return;
  //         myState.currentPage += 1;
  //         document.getElementById("current_page")
  //                 .value = myState.currentPage;
  //         // Scroll PDF viewer to top
  //         canvasContainer.scrollTop = 0;
  //         render();
  //     });
  // document.getElementById('current_page')
  //     .addEventListener('keypress', (e) => {
  //         if(myState.pdf == null) return;
  //         // Get key code
  //         var code = (e.keyCode ? e.keyCode : e.which);
  //         // If key code matches that of the Enter key
  //         if(code == 13) {
  //             var desiredPage =
  //                     document.getElementById('current_page')
  //                             .valueAsNumber;
  //             if(desiredPage >= 1
  //                 && desiredPage <= myState.pdf
  //                                         ._pdfInfo.numPages) {
  //                     myState.currentPage = desiredPage;
  //                     document.getElementById("current_page")
  //                             .value = desiredPage;
  //                     render();
  //             }
  //         }
  //     });
  // document.getElementById('zoom_in')
  //     .addEventListener('click', (e) => {
  //         if(myState.pdf == null) return;
  //         if(myState.zoom <= 3.5) {
  //             myState.zoom += 0.5;
  //         }
  //         render();
  //     });
  // document.getElementById('zoom_out')
  //     .addEventListener('click', (e) => {
  //         if(myState.pdf == null) return;
  //         if(myState.zoom >= 1.5) {
  //             myState.zoom -= 0.5;
  //         }
  //         render();
  //     });

} catch (err) {
  _iterator2.e(err);
} finally {
  _iterator2.f();
}

/***/ }),

/***/ "./resources/js/resource/exam_builder.js":
/*!***********************************************!*\
  !*** ./resources/js/resource/exam_builder.js ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

var $form = $('#examBuilderFilterForm');
var $paperNumberSelect = $('[name=paper_number]');
var chapter_selectHTML = $('#examBuilderFilterForm .exam-builder__chapter-select').html(); // ------------------
// Filter
// ------------------

function updateChapterSelect() {
  // Get select
  var $chapterSelect = $('#examBuilderFilterForm .exam-builder__chapter-select');
  var $originalChapterSelect = $('#examBuilderFilterForm select.exam-builder__chapter-select'); // Reset Options

  var $options = $chapterSelect.find('option');
  $options.removeAttr('disabled'); // Get options
  // Parent

  var $parentOptions = $options.filter('.parent');
  var $selectedParentOptions = $parentOptions.filter(':selected'); // Child

  var $childOptions = $options.filter('.child');
  var $selectedChildOptions = $childOptions.filter(':selected'); // Disable child chapters of selected parent chapter options

  $selectedParentOptions.each(function (i, $parent) {
    $childOptions.filter('[data-parent-chapter-id=' + $parent.value + ']').attr('disabled', 'disabled');
  }); // Disable parent chapter of selected child chapter options

  $selectedChildOptions.each(function (i, $child) {
    $parentOptions.filter('[value=' + $child.dataset.parentChapterId + ']').attr('disabled', 'disabled');
  }); // Sync wrapper select div element with original select element

  $originalChapterSelect.show();
  $originalChapterSelect.hide();
}

if ($form.find('.exam-builder__subject-select').length > 0 && $form.find('.exam-builder__chapter-select').length > 0) {
  new SlimSelect({
    select: '#examBuilderFilterForm .exam-builder__subject-select',
    placeholder: 'Select a subject',
    onChange: function onChange() {
      var $subject_selected = $form.find('.exam-builder__subject-select :selected');
      var subject_id = $subject_selected.val();
      var subject_selected = $subject_selected.text();
      var $chapter_select = $('#examBuilderFilterForm .exam-builder__chapter-select');
      $chapter_select.prop('disabled', false); // restore the full select list first.

      $chapter_select.html(chapter_selectHTML);
      var optGroup = $('optgroup[label="' + subject_selected + '"]').html();
      $chapter_select.html(optGroup);
      new SlimSelect({
        select: '#examBuilderFilterForm .exam-builder__chapter-select',
        placeholder: 'Select Chapters',
        limit: examBuilderChapterLimit,
        closeOnSelect: false,
        onChange: function onChange() {
          updateChapterSelect();
        }
      });

      if ($paperNumberSelect.length > 0) {
        // Update Paper Number Select options
        $.ajax({
          method: 'GET',
          url: '/exam-builder/subjects/' + subject_id + '/paper_numbers',
          success: function success(paperNumbers) {
            $paperNumberSelect.attr('disabled', false);
            var optionsHtml = '<option value="" disabled selected>Select Paper Number</option>';
            paperNumbers.forEach(function (paperNumber) {
              optionsHtml += '<option value="' + paperNumber + '">' + paperNumber + '</option>';
            }); // Add Paper Number options on select element

            $paperNumberSelect.html(optionsHtml);
          }
        });
      }
    }
  });
  /*
  * For failed requests
  */

  var subject_selected = $('.exam-builder__subject-select :selected').text();
  var $subject_selected = $form.find('.exam-builder__subject-select :selected');
  var subject_id = $subject_selected.val();
  var $chapter_select = $('#examBuilderFilterForm .exam-builder__chapter-select'); //Enable chapter select if old subject is selected

  if (subject_selected) {
    $chapter_select.prop('disabled', false);
  } //Remove unrelated chapters


  var optGroup = $('optgroup[label="' + subject_selected + '"]').html();
  $chapter_select.html(optGroup);
  /*
  * Inititialise chapter select
  */

  new SlimSelect({
    select: '#examBuilderFilterForm .exam-builder__chapter-select',
    placeholder: 'Select Chapters',
    limit: examBuilderChapterLimit,
    closeOnSelect: false,
    onChange: function onChange() {
      updateChapterSelect();
    }
  });

  if ($paperNumberSelect.length > 0 && !Number.isNaN(parseInt(subject_id))) {
    // Enable Paper Select
    $paperNumberSelect.attr('disabled', false);
    var oldPaperNumber = $paperNumberSelect.val(); // Update Paper Number Select options

    $.ajax({
      method: 'GET',
      url: '/exam-builder/subjects/' + subject_id + '/paper_numbers',
      success: function success(paperNumbers) {
        var optionsHtml = '<option value="" disabled selected>Select Paper Number</option>';
        paperNumbers.forEach(function (paperNumber) {
          optionsHtml += '<option value="' + paperNumber + '">' + paperNumber + '</option>';
        }); // Add Paper Number options on select element

        $paperNumberSelect.html(optionsHtml);
        $paperNumberSelect.find('option[value="' + oldPaperNumber + '"]').prop('selected', true);
      }
    });
  }
} // --------------
// Customize
// --------------


$(document).on('click', '.itemBlock .toggleAnswerButton', function () {
  $(this).next('.item__answer').toggleClass('d-none');

  if ($(this).text() == 'Show Answer') {
    $(this).text('Hide Answer');
  } else {
    $(this).text('Show Answer');
  }
}); // Stores current worksheet's item IDs

var $itemsInput = $('[name=item_ids]');
var $itemCount = $('span.itemCount'); // Replace current worksheet's item IDs with new item ID
// Reference: https://stackoverflow.com/a/5915824

function replaceItemId(id, new_id) {
  var itemIds = $itemsInput.val(); // Convert string into JS array

  itemIds = JSON.parse(itemIds); // Replace index number from old to new item ID

  var i = itemIds.indexOf(id);
  itemIds[i] = new_id; // Maintain the JSON string format of items input

  $itemsInput.val('[' + itemIds + ']');
} // Remove item ID from item IDs array
// Reference: https://stackoverflow.com/a/5767357


function removeItemId(id) {
  var itemIds = $itemsInput.val(); // Convert string into JS array

  itemIds = JSON.parse(itemIds);
  var i = itemIds.indexOf(id);

  if (i > -1) {
    itemIds.splice(i, 1);
  } // Maintain the JSON string format of items input


  $itemsInput.val('[' + itemIds + ']');
}

$(document).on('click', '.itemBlock .regenerateItemButton', function () {
  var $t = $(this);
  var $initItemBlock = $t.parents('.itemBlock');
  var initHtml = $t.html();
  var itemId = $initItemBlock.data('item-id');
  var itemIds = $itemsInput.val(); // Show loader in button

  window.showLoader($t, 'in-button', 'white'); // Replace item

  $.ajax({
    method: 'POST',
    url: '/exam-builder/items/' + itemId + '/replace',
    data: {
      item_ids: itemIds
    },
    success: function success(item) {
      // Get item block HTML with AJAX call
      $.ajax({
        method: 'POST',
        url: '/exam-builder/items/' + item.id + '/html',
        // Returns new item block html
        success: function success(html) {
          // Replace item block by appending new block to existing block, then deleting the existing block
          $initItemBlock.after(html);
          $initItemBlock.remove(); // Replace current worksheet's item IDs with new item ID

          replaceItemId(itemId, item.id);
          toastr.success("Item replaced successfully!");
        },
        error: function error() {
          toastr.error("Item cannot be replaced!");
          window.hideLoader($t, initHtml);
        }
      });
    },
    error: function error() {
      toastr.error("Item cannot be replaced!");
      window.hideLoader($t, initHtml);
    }
  });
});
$(document).on('click', '.itemBlock .removeItemButton', function () {
  var $t = $(this);
  var $initItemBlock = $t.parents('.itemBlock');
  var itemId = $initItemBlock.data('item-id');
  var itemName = $initItemBlock.data('item-name');
  var isLastItem = $('.itemBlock').length <= 1;

  if (!isLastItem) {
    // Show delete alert
    Swal.fire({
      title: 'Are you sure?',
      type: 'error',
      html: '<b>' + itemName + '</b> will be removed from your current worksheet!',
      showCloseButton: true,
      showCancelButton: true,
      focusConfirm: false,
      reverseButtons: true,
      confirmButtonText: 'Confirm'
    }).then(function (result) {
      if (result.value) {
        // Remove item ID from item IDs array
        removeItemId(itemId); // Reflect -1 item count on front-end counter

        var val = parseInt($itemCount.text());
        $itemCount.text(val - 1); // Remove Item Block

        $initItemBlock.remove();
      }
    });
  } else {
    Swal.fire({
      title: 'Exam Paper cannot be empty!',
      type: 'error',
      html: 'This is the last question for your exam paper!',
      showCloseButton: true,
      focusConfirm: true,
      confirmButtonText: 'Confirm'
    });
  }
});

/***/ }),

/***/ "./resources/js/resource/filter.js":
/*!*****************************************!*\
  !*** ./resources/js/resource/filter.js ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

$(function () {
  $('.purchaseResourceButton').click(function (e) {
    e.preventDefault();
    var form = e.target.form;
    var resource_name = $(this).data('resource-name');
    Swal.fire({
      title: 'Are you sure?',
      text: "You will be purchasing the " + product_name + " coin pack for MYR" + price + "!",
      type: 'warning',
      showCancelButton: true,
      reverseButtons: true,
      confirmButtonText: 'Confirm Purchase',
      cancelButtonText: 'Cancel'
    }).then(function (result) {
      if (result.value) {
        form.submit();
      }
    });
  }); // Topical Paper

  if ($('.paper-select').length > 0) {
    var paperSelect = new SlimSelect({
      select: '.paper-select',
      placeholder: 'Paper',
      allowDeselectOption: true
    });
  } else {
    var paperSelect = null;
  }

  if ($('.chapter-select').length > 0) {
    var chapterSelect = new SlimSelect({
      select: '.chapter-select',
      placeholder: 'Select Chapter',
      allowDeselectOption: true
    });
  } else {
    var chapterSelect = null;
  }

  if ($('.year-select').length > 0) {
    var yearSelect = new SlimSelect({
      select: '.year-select',
      placeholder: 'Year',
      limit: 5,
      allowDeselectOption: true,
      closeOnSelect: false
    });
  }

  ;

  if ($('.paper-select').length > 0) {
    var paperSelect = new SlimSelect({
      select: '.paper-select',
      limit: 5,
      placeholder: 'Paper Number',
      allowDeselectOption: true,
      closeOnSelect: false
    });
  }

  ;

  if ($('.season-select').length > 0) {
    var seasonSelect = new SlimSelect({
      select: '.season-select',
      limit: 3,
      placeholder: 'Season',
      allowDeselectOption: true,
      closeOnSelect: false
    });
  }

  if ($('.variant-select').length > 0) {
    var variantSelect = new SlimSelect({
      select: '.variant-select',
      limit: 3,
      placeholder: 'Variant',
      allowDeselectOption: true,
      closeOnSelect: false
    });
  }

  if ($('.chapter-select').length > 0) {
    var chapterSelect = new SlimSelect({
      select: '.chapter-select',
      placeholder: 'Select Chapter',
      allowDeselectOption: true
    });
  }

  $('.clear-filter').click(function () {
    if (typeof yearSelect !== 'undefined') {
      yearSelect.set([]);
    }

    if (typeof paperSelect !== 'undefined') {
      paperSelect.set([]);
    }

    if (typeof seasonSelect !== 'undefined') {
      seasonSelect.set([]);
    }

    if (typeof variantSelect !== 'undefined') {
      variantSelect.set([]);
    }

    if (typeof chapterSelect !== 'undefined') {
      chapterSelect.set([]);
    }

    $('input[name="exclude_reference"]').prop('checked', false);
  });
});

/***/ }),

/***/ "./resources/js/resource/library.js":
/*!******************************************!*\
  !*** ./resources/js/resource/library.js ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

// Create new library Button
$('#createNewLibraryButton').click(function () {
  var $t = $(this);
  var initHtml = $(this).html();
  window.showLoader($t, 'in-button', 'white'); // Validate Form

  var $form = $t.closest('form');
  var resource_id = $form.find('input[name=resource_id]').val(); // Validate inputs with required property

  $form.find('input').each(function () {
    // Adds error message if input is required but empty length
    if ($(this).prop('required')) {
      if (!$.trim($(this).val()).length) {
        // Prevent adding more than one error message after input
        if ($(this).next('.error-message').length == 0) {
          $(this).after('<span class="error-message">This field is required.</span>');
        }
      } else {
        $(this).next('.error-message').remove();
      }
    }
  }); // Disable modal if exists

  var $modal = $t.closest('.modal');
  var $saveToLibraryModal = $('#saveToLibraryModal');

  if ($modal.length > 0) {
    // Reference: https://stackoverflow.com/a/38502417
    $modal.data('bs.modal')._config.backdrop = 'static';
    $modal.data('bs.modal')._config.keyboard = false;
  }

  if ($form.find('.error-message').length > 0) {
    // Form is invalid
    window.hideLoader($t, initHtml); // Re-enable modal

    if ($modal.length > 0) {
      $modal.data('bs.modal')._config.backdrop = true;
      $modal.data('bs.modal')._config.keyboard = true;
    }
  } else {
    // Form is valid
    $.ajax({
      method: "POST",
      url: "/libraries/store",
      data: $form.serialize(),
      success: function success(response) {
        if (typeof response == 'string') {
          // Reset Button
          window.hideLoader($t, initHtml); // Show error message

          toastr.error(response);
        } else {
          var library = response; // Add library block to modal (#saveToLibraryModal)

          $.ajax({
            method: "POST",
            url: "/libraries/" + library.id + "/" + resource_id + "/get_list_item_html",
            success: function success(newLibraryBlockHtml) {
              $saveToLibraryModal.find('#createNewLibraryListItem').before(newLibraryBlockHtml);
            }
          }); // Reset Button

          window.hideLoader($t, initHtml); // Reset Modal
          // Remove form input values

          $form.find('input[type=text]').val(''); // Hide modal

          $modal.modal('hide');
          $modal.data('bs.modal')._config.backdrop = true;
          $modal.data('bs.modal')._config.keyboard = true; // Show toast message

          toastr.success(library.name + ' added successfully!');
        }
      },
      error: function error() {
        toastr.error('An error occured.');
      }
    });
  }
}); // Toggle save to library checkbox

$('body').on('change', '.toggleSaveToLibraryCheckBox', function (e) {
  e.preventDefault();
  e.stopPropagation();
  var $t = $(this);
  var $form = $t.closest('form');
  var library = $t.data('library');
  var resource = $t.data('resource'); // AJAX

  $.ajax({
    url: $form.attr('action'),
    method: 'POST',
    success: function success(response) {
      if (response == 'saved') {
        toastr.success('Saved to ' + library.name);
        $t.prop('checked', true);
      } else if (response == 'removed') {
        toastr.success('Removed from ' + library.name);
        $t.prop('checked', false);
      } else if (response == 'full') {
        toastr.error(library.name + ' is full!');
        $t.prop('checked', false);
      } else {
        toastr.error('An error occurred!');
      }
    },
    error: function error(resposne) {
      toastr.error('An error occurred!');
    }
  });
}); // Remove resource from library button

$('.deleteResourceFromLibraryButton').click(function (e) {
  e.preventDefault();
  e.stopPropagation();
  var $t = $(this);
  var url = $t.data('action');
  var library = $t.data('library');
  Swal.fire({
    title: 'Remove from ' + library.name + '?',
    text: "You won't be able to revert this!",
    type: 'warning',
    showCancelButton: true,
    reverseButtons: true,
    confirmButtonText: 'Confirm'
  }).then(function (result) {
    if (result.value) {
      $.ajax({
        method: "POST",
        url: url,
        success: function success(response) {
          if (response == 'removed') {
            $t.parents('.file-block').parent('.col-12').remove();
            toastr.success('Removed from ' + library.name);
          } else {
            toastr.error("An error occured!");
          }
        },
        error: function error(response) {
          toastr.error("An error occured!");
        }
      });
    }
  });
});

/***/ }),

/***/ 2:
/*!****************************************!*\
  !*** multi ./resources/js/resource.js ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\laragon\www\spmboard\resources\js\resource.js */"./resources/js/resource.js");


/***/ })

/******/ });