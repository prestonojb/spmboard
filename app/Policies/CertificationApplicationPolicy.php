<?php

namespace App\Policies;

use App\CertificationApplication;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class CertificationApplicationPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Permission to browse (admin)
     */
    public function browse(User $user)
    {
        return $user->isAdmin();
    }

    /**
     * Permission to list applications
     */
    public function index(User $user)
    {
        return $user->is_teacher();
    }

    /**
     * Permission to create/store application
     */
    public function create_store(User $user)
    {
        return $user->is_teacher() && !$user->hasPendingApplication() && $user->passedAllCertificationMetrics() && !$user->is_certified;
    }

    // /**
    //  * Permission to update application
    //  */
    // public function update(User $user, CertificationApplication $certification_application)
    // {
    //     return $user->id == $certification_application->user_id;
    // }

    /**
     * Permission to update/approve application
     */
    public function adminUpdate(User $user)
    {
        return $user->isAdmin();
    }
}
