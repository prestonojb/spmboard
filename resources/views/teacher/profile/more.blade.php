<div class="panel p-0 user-credentials">
    <div class="panel-header">
        <h3 class="mb-0">Details</h3>
    </div>

    <div class="panel-body">
        {{-- Age/Gender --}}
        <div class="user-credentials__item">
            <span class="iconify" data-icon="carbon:user-profile" data-inline="false"></span>
            @if(!is_null($teacher->age_and_gender))
                <div class="value">{{ $teacher->age_and_gender }}</div>
            @else
                <div class="user-credentials__empty">No data</div>
            @endif
        </div>

        {{-- State --}}
        <div class="user-credentials__item">
            <span class="iconify" data-icon="bx:bx-map" data-inline="false"></span>
            @if(!is_null($teacher->user->state))
                <div class="value">From {{ $teacher->user->state->name }}, Malaysia</div>
            @else
                <div class="user-credentials__empty">No data</div>
            @endif
        </div>

        {{-- School --}}
        <div class="user-credentials__item">
            <span class="iconify" data-icon="bx:bxs-school" data-inline="false"></span>
            @if(!is_null($teacher->school))
                <div class="value">Teaches at {{ $teacher->school }}</div>
            @else
                <div class="user-credentials__empty">No data</div>
            @endif
        </div>
    </div>
</div>
