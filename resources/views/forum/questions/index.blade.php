@extends('forum.layouts.app')

@section('meta')
    @if(isset($subject))
        <title>{{ $subject->name }} | {{ $exam_board->name }} {{ config('app.name') }}</title>
        <meta name="description" content="{{ $subject->description }}">
    @elseif(isset($chapter))
        <title>{{ $chapter->name }} - {{ $chapter->name }} | {{ $exam_board->name }} {{ config('app.name') }}</title>
        <meta name="description" content="{{ $chapter->description }}">
    @endif
    <title>A community of knowledge sharing | {{ $exam_board->name }} {{ config('app.name') }}</title>
    <meta name="description" content="Board is an online community for students to learn from mentors. Share the joy of learning and teaching with the Board community now!">
@endsection

@section('styles')
@endsection

@section('content')
@include('forum.common.header')
@include('forum.common.sidenav')
<section class="content">
    <div class="row mx-0">
        <div class="col-12 col-lg-9 px-0">

            <div class="qb-container">
                {{-- Subject Info (if any) --}}
                @if(isset($subject))
                    <div class="d-flex justify-content-start align-items-start py-3 border-bottom mb-2">
                        <div class="font-size-64px blue6 mr-2">
                            {!! $subject->icon !!}
                        </div>
                        <div>
                            <h1 class="font-size9 mb-1">{{ $subject->name }}</h1>
                            <h2 class="gray2 font-size3">{{ $subject->description }}</h2>
                            <p class="gray2 font-size1">{{ count($subject->questions ?? []) }} questions</p>
                        </div>
                    </div>
                @endif

                @if(isset($chapter))
                    <div class="d-flex justify-content-start align-items-start py-3 border-bottom mb-2">
                        <div class="font-size-64px blue6 mr-2">
                            {!! $chapter->subject->icon !!}
                        </div>
                        <div>
                            <h3 class="font-size2 text-uppercase mb-1">{{ $chapter->subject->name }}</h3>
                            <h1 class="font-size9 mb-1">{{ $chapter->name }}</h1>
                            <h2 class="gray2 font-size3">{{ $chapter->description }}</h2>
                            <p class="gray2 font-size1">{{ count($chapter->questions ?? []) }} questions</p>
                        </div>
                    </div>
                @endif

                @can('create', \App\Question::class)
                    @include('forum.questions.common.ask-question-block')
                @endcan

                @if($exam_board->slug == 'igcse')
                    <div class="alert alert-warning mb-0 mt-2" role="alert">
                        Try out the new revision tool built for you IGCSE students! <a href="https://bit.ly/2Vx7JvS" target="_blank" class="underline-on-hover"><b>INTERESTED!</b></a>
                    </div>
                @endif

                {{-- Sort --}}
                <div class="py-2">
                    @include('forum.questions.common.sort')
                </div>

                @if( count($questions ?? []) )
                    @foreach($questions as $question)
                        @include('forum.common.question-block')
                    @endforeach
                    {{ $questions->appends(['sort' => $sort])->links() }}
                @else
                    <section class="text-center my-4">
                        <img src="{{ asset('img/essential/empty.svg') }}" alt="No Questions Found" width="220" height="220" class="mb-2">
                        <h2 class="gray2 font-size4">No questions found</h2>
                        @can('create', \App\Question::class)
                            <a href="{{ route('questions.create', $exam_board) }}" class="button--primary lg">Ask Question</a>
                        @endcan
                    </section>
                @endif


            </div>
        </div>

        <div class="col-12 col-lg-3 px-2">
            @if(setting('marketing.2020_math_board_quiz'))
                <a href="{{ route('events.2020.get_math_board_quiz_view') }}" class="button--primary lg w-100 mt-2">Join 2020 Math Board Quiz</a>
            @endif

            @if(setting('marketing.2020_stoichiometry_mania'))
                <div class="d-none d-lg-block">
                    <a href="{{ route('events.2020.get_stoichiometry_mania_register_view') }}">
                        <img width="100%" style="object-fit: cover;" src="{{ asset('img/events/2020_stoichiometry_mania.jpg') }}" class="mt-2" alt="">
                    </a>
                </div>
            @endif
            @include('forum.common.login_rewards')
        </div>
    </div>

</section>
@endsection


@section('scripts')
<script>
$(function() {
    const upvoteButtonAnimation = 'animated pulse faster';
    $('.answer-button').click(function(){
        var url = $(this).data('url');
        window.location.href = url + '?answerFocus=true';
    });

    //Image popup
    $('.question--block .question-block__image').click(function(){
        var img_src = $(this).attr('src');
        $.magnificPopup.open({
            items: {
                src: img_src,
            },
            type: 'image',
            showCloseButton: false,
        });
    });

    $('.question-upvote-button').click(function(e){
        const loggedIn = {{ auth()->check() ? 'true' : 'false' }};
        if(loggedIn){

            const question_id = $(this).data('questionid');
            var url = "{{ route('questions.upvote', ':question_id') }}";
            url = url.replace(':question_id', question_id);

            $.ajax({
                method: 'POST',
                url: url,
            });

            const $upvoteCount = $('.upvote-count[data-questionid="'+ question_id +'"]');
            //Get the upvote count of the question
            var upvote_count = parseInt($upvoteCount.text());

            $(this).toggleClass('upvoted');
            if($(this).hasClass('upvoted')){
                upvote_count++;
                $upvoteCount.text(upvote_count);

                // Animate the upvote button
                $(this).addClass(upvoteButtonAnimation).on('animationend', function(){
                    $(this).removeClass(upvoteButtonAnimation);
                });
            } else{
                upvote_count--;
                $upvoteCount.text(upvote_count);
            }

        } else {
            Swal.fire({
                title: '<strong>Oops! You are not logged in!</strong>',
                type: 'error',
                html:
                    '<p>Log in to gain the best experience with us!</p>',
                showCloseButton: true,
                showCancelButton: true,
                focusConfirm: false,
                reverseButtons: true,
                confirmButtonText: 'Log in',
                cancelButtonText: 'Not now',
            }).then(function(result){
                if(result.value){
                    window.location.href = '{{ route('login') }}';
                }
            });
        }
    });

    $('.auth-login-reward').click(function(){
        Swal.fire({
            title: 'Come back tomorrow to receive your reward!',
            type: 'info',
            html:
                '<p>Coins can be used to claim our resources.</p>',
            focusConfirm: false,
            reverseButtons: true,
        })
    });

    $('.guest-login-reward').click(function(){
        Swal.fire({
            title: 'Oops! You are not logged in!',
            type: 'warning',
            html:
                '<p>Log in to receive reward points daily! Points can be converted into coins to buy resources.</p>',
            showCloseButton: true,
            showCancelButton: true,
            focusConfirm: false,
            reverseButtons: true,
            confirmButtonText: 'Log in',
            cancelButtonText: 'Maybe later',
        }).then(function(result){
            if(result.value){
                window.location.href = '{{ route('login') }}';
            }
        });
    })
});

</script>
@endsection
