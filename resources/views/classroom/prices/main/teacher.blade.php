<div class="row justify-content-center align-items-center" style="height: 430px;">
    {{-- Free --}}
    <div class="col-12 col-md-6 col-lg-4 mb-2 h-100">
        <div class="panel d-flex flex-column justify-content-between align-items-start h-100">
            <div class="">
                <h2 class="my-2 font-size7 mb-0"><b>Free</b></h2>
                <p class="gray2">For teachers trying out Board for an unlimited period of time</p>
                <h3 class="mb-3 font-size7 pt-2 pb-1"><b>$0 USD/forever</b></h3>
            </div>
            <ul class="checklist">
                <li>Access to {{ setting('classroom.free_classroom_limit') }} classrooms of up to {{ setting('classroom.free_student_limit') }} students each</li>
                <li>Track performance and progress in class</li>
                <li>Report to parents about activities in class</li>
                <li>Generate worksheets by chapters according to your teaching style</li>
                <li>Supplementary teaching materials</li>
            </ul>

            <a href="javascript:void(0)" class="button--primary--inverse w-100 disabled">
                Unlocked
            </a>
        </div>
    </div>

    {{-- Board Premium --}}
    <div class="col-12 col-md-6 col-lg-4 mb-2 h-100">
        <div class="panel d-flex flex-column justify-content-between align-items-start h-100">
            <div class="">
                <h2 class="my-2 font-size7 mb-0"><b>Board Premium</b></h2>
                <p class="gray2">For teachers shifting to online learning</p>
                <h3 class="mb-3 font-size7 pt-2 pb-1">
                    <p class="font-size2">As low as</p>
                    <b>${{ number_format( (($annual_price->unit_amount)/12)/100, 2) }} USD/month</b>
                </h3>
            </div>
            <p class="mb-1"><b>All the benefits of Free, and:</b></p>
            <ul class="checklist">
                <li>Access to {{ setting('stripe.premium_classroom_limit') }} classrooms of up to {{ setting('stripe.premium_student_limit') }} students each</li>
                <li>Customised worksheets with advanced settings</li>
                <li>Access to all teaching materials</li>
            </ul>

            <div class="w-100">
                <button type="button" class="button--danger--inverse w-100 subscribeNowButton mb-1" @if(!is_null($active_price) && $active_price->id == $monthly_price->id) disabled @endif data-price="{{ $monthly_price->nickname }}" data-price-id="{{ $monthly_price->id }}" data-price-per-interval="{{ strtoupper($monthly_price->currency). number_format(strval($monthly_price->unit_amount/100), 2).'/month' }}" data-toggle="modal" data-target="#subscriptionModal">
                    Unlock at ${{ number_format($monthly_price->unit_amount/100, 2) }} USD/month
                </button>
                <button type="button" class="button--danger w-100 subscribeNowButton" @if(!is_null($active_price) && $active_price->id == $annual_price->id) disabled @endif data-price="{{ $annual_price->nickname }}" data-price-id="{{ $annual_price->id }}" data-price-per-interval="{{ strtoupper($annual_price->currency). number_format(strval($annual_price->unit_amount/100), 2).'/year' }}" data-toggle="modal" data-target="#subscriptionModal">
                    Unlock at ${{ number_format($annual_price->unit_amount/100, 2) }} USD/year
                </button>
            </div>
        </div>
    </div>
</div>


{{-- Modal --}}
<div class="modal fade" id="subscriptionModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="{{ route('classroom.subscriptions.store') }}" method="POST" id="subscriptionForm">
                @csrf
                <input type="hidden" name="price">
                <input type="hidden" name="payment_method">

                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Payment Details</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p class="">
                        You are subscribing to <b><span class="price"></span> Plan</b> for <b><span class="price-per-interval"></span></b>.
                    </p>

                    <hr>

                    <div class="form-field">
                        <label>Choose Payment Method</label>
                        @if( !is_null($payment_methods) && count($payment_methods) > 0)
                            <select name="payment_method" id="payment_method">
                                <option value="" disabled selected>Debit/Credit Card</option>
                                @foreach($payment_methods as $payment_method)
                                    <option value="{{ $payment_method->id }}">[{{ strtoupper($payment_method->card->brand) }}] •••• •••• •••• {{ $payment_method->card->last4 }}(Expires at {{ $payment_method->card->exp_month }}/{{ $payment_method->card->exp_year }})</option>
                                @endforeach
                            </select>
                        @else
                            <p class="gray2">Set up your Payment Method <a href="{{ route('payment') }}" target="_blank">here</a> before proceeding.</p>
                        @endif
                        @error('payment_method')
                            <div class="error-message">{{ $message }}</div>
                        @enderror
                    </div>

                    <div class="form-field">
                        <label for="">Promo Code(if any)</label>
                        <input type="text" name="promo_code" placeholder="Enter your Promo Code" value="{{ old('promo_code') }}">
                        @error('promo_code')
                            <div class="error-message">{{ $message }}</div>
                        @enderror
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="button--primary--inverse" data-dismiss="modal">Close</button>
                    <button type="button" class="button--primary disableOnSubmitButton" id="card-button" data-secret="{{ $intent->client_secret }}">Confirm Payment</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="py-5"></div>

@include('classroom.prices.common.premium_table')

<div class="py-1"></div>
