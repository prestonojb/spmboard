<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCoinProductTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coin_product_types', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('coin_product_id');
            $table->string('name');
            $table->unsignedInteger('price_in_coins');
            $table->unsignedInteger('discounted_price_in_coins');
            $table->boolean('is_active')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coin_product_types');
    }
}
