<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use PDF;

class ExamBuilderSheet extends Model
{
    protected $guarded = [];
    protected $dates = ['created_at', 'updated_at', 'ms_generated_at'];

    /**
     * --------------
     * Relationships
     * --------------
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function subject()
    {
        return $this->belongsTo(Subject::class);
    }

    public function chapters()
    {
        return $this->belongsToMany(Chapter::class);
    }

    public function items()
    {
        return $this->belongsToMany(ExamBuilderItem::class);
    }

    /**
     * -----------
     * ACCESSORS
     * -----------
     */
    /**
     * Get QP route
     * @return string
     */
    public function getQpRouteAttribute()
    {
        return route('exam_builder.sheets.show', $this->id);
    }

    /**
     * Get MS route
     * @return string
     */
    public function getMsRouteAttribute()
    {
        return route('exam_builder.sheets.show_ms', $this->id);
    }

    /**
     * Get QP full URL path
     */
    public function getQpUrlAttribute($value)
    {
        if(strpos($value, 'http') !== false || strpos($value, 'https') !== false){
            return $value;
        }
        return $value ? Storage::cloud()->url($value) : null;
    }

    /**
     * Get QP relative URL path
     */
    public function getQpRelativeUrlAttribute()
    {
        return $this->getOriginal('qp_url');
    }

    /**
     * Return QP Download Route
     */
    public function getQpDownloadRouteAttribute()
    {
        return route('exam_builder.sheets.download_qp', $this);
    }

    /**
     * Get QP full URL path
     */
    public function getMsUrlAttribute($value)
    {
        if(strpos($value, 'http') !== false || strpos($value, 'https') !== false){
            return $value;
        }
        return $value ? Storage::cloud()->url($value) : null;
    }

    /**
     * Get QP relative URL path
     */
    public function getMsRelativeUrlAttribute()
    {
        return $this->getOriginal('ms_url');
    }

    /**
     * Return MS Download Route
     */
    public function getMsDownloadRouteAttribute()
    {
        return route('exam_builder.sheets.download_ms', $this);
    }

    /**
     * Returns chapters comma-delimited string
     * @return string
     */
    public function getChaptersStrAttribute()
    {
        $str = '';
        if(count($this->chapters ?? [])) {
            for($i=0;$i < count($this->chapters);$i++) {
                $chapter = $this->chapters[$i];
                if($i != count($this->chapters) - 1) {
                    $str .= "{$chapter->name}, ";
                } else {
                    $str .= $chapter->name;
                }
            }
        }
        return $str;
    }

    /**
     * Returns QP status tag HTML
     * @return string
     */
    public function getQpStatusTagHtmlAttribute()
    {
        $dict = [
            'unavailable' => 'red',
            'loading' => 'yellow',
            'available' => 'green',
        ];
        return '<span class="status-tag '.$dict[$this->qp_status].'">'.ucfirst($this->qp_status).'</span>';
    }

    /**
     * Returns MS status tag HTML
     * @return string
     */
    public function getMsStatusTagHtmlAttribute()
    {
        $dict = [
            'unavailable' => 'red',
            'loading' => 'yellow',
            'available' => 'green',
        ];
        return '<span class="status-tag '.$dict[$this->ms_status].'">'.ucfirst($this->ms_status).'</span>';
    }


    /**
     * Front-end HTML functions
    */
    public function getChapterTagsHtmlAttribute()
    {
        $html = "";
        foreach($this->chapters as $chapter) {
            $html .= "<span class='tag--light'>{$chapter->name}</span> ";
        }
        return $html;
    }

    /**
     * Returns description of sheet
     * @return string
     */
    public function getDescriptionAttribute()
    {
        return "{$this->subject->name} · Paper {$this->paper_number}";
    }

    /**
     * -----------------------
     * Attributes functions
     * -----------------------
     */

    public function hasQp()
    {
        return !is_null($this->qp_url);
    }

    public function hasMs()
    {
        return !is_null($this->ms_url);
    }

    /**
     * --------------
     * Helper functions
     * --------------
     */
    /**
     * Stores QP/MS PDF generated on S3 & DB
     * @param boolean $is_qp
     * @param \PDF $qp_pdf Question Paper PDF
     * @param \PDF $ms_pdf Mark Scheme PDF
     */
     public function generateAndStorePdf($is_qp, $show_label, $hide_watermark)
    {
        // Generate PDF from items
        $pdf = $this->generatePdf( $is_qp, $show_label, $hide_watermark );
        // Store QP on S3 & DB
        $this->storePdf($pdf, $is_qp);
     }

    /**
     * --------------------
     * GENERATE PDF
     * --------------------
     */
    /**
     * Generate QP/MS PDF
     * @param array \Illuminate\Support\Collection All items to be included in the exam paper
     */
    public function generatePdf( $is_qp, $show_label, $hide_watermark )
    {
        $data = [
            'subject' => $this->subject,
            'chapters_str' => $this->chapters_str,
            'items' => $this->items,
            'is_qp' => $is_qp,
            'title' => $is_qp ? 'Question Paper' : 'Mark Scheme',
            'show_label' => $show_label,
            'hide_watermark' => $hide_watermark,
        ];
        return PDF::loadView('exam_builder.pdf.sheet', compact('data'));
    }

    /**
     * --------------------
     * STORE PDF
     * --------------------
     */
    /**
     * Stores Question/Answer PDF on S3
     * @param PDF $qp_pdf
     * @param boolean $is_qp
     */
    public function storePdf($pdf, $is_qp)
    {
        $hash = date('mdYHis') . uniqid();
        $path = $is_qp ? 'exam-builder/sheets/qp/'.$hash.'.pdf'
                        : 'exam-builder/sheets/ms/'.$hash.'.pdf';

        // Store on S3
        Storage::cloud()->put( $path, $pdf->output() );

        // Update DB
        if($is_qp) {
            $this->update(['qp_url' => $path]);
        } else {
            $this->update(['ms_url' => $path]);
        }
    }

     /**
      * Delete PDFs from S3
      */
    public function deletePdfs()
    {
        Storage::cloud()->delete($this->qp_relative_url);
        Storage::cloud()->delete($this->ms_relative_url);
    }
}
