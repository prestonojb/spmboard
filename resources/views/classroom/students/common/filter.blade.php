<form action="{{ url()->current() }}" method="get">
    @csrf
    <div class="filter-container">
        <div class="select-container">
            {{-- Classroom --}}
            <select name="class_selected" id="" class="filter-select--lg">
                <option value="" disabled selected>Classroom</option>
                @foreach( auth()->user()->teacher->classrooms as $room )
                    <option value="{{ $room->id }}" @if( request()->class_selected == $room->id) selected @endif>{{ $room->name }}</option>
                @endforeach
            </select>

            {{-- Understanding rating --}}
            <select name="understanding_rating" class="filter-select--md" id="">
                <option value="" disabled selected>Und. rating</option>
                 @foreach( range(0,4) as $i )
                    <option value="{{ $i }}" @if(isset(request()->understanding_rating) && request()->understanding_rating == $i) selected @endif>{{ $i.'-'.($i+1) }}</option>
                @endforeach
            </select>

            {{-- Unpaid lessons --}}
            <select name="unpaid_lessons" id="" class="filter-select--md">
                <option value="" disabled selected>Unpaid lessons</option>
                @foreach( range(0, 4) as $i )
                    <option value="{{ $i }}" @if( isset(request()->unpaid_lessons) &&  request()->unpaid_lessons == $i ) selected @endif>{{ $i.'-'.($i+1) }}</option>
                @endforeach
                <option value="5" @if( request()->unpaid_lessons == 5 ) selected @endif>>5</option>
            </select>
        </div>

        <div class="filter-buttons">
            <button type="button" class="button-link underline-on-hover mr-2 clearFilterButton">Clear</button>
            <button type="submit" class="button--primary">Apply Filter</button>
        </div>
    </div>
</form>
