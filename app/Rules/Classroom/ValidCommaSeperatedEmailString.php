<?php

namespace App\Rules\Classroom;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\Validator;

/**
 * Validate Teacher Bulk Add Student to classroom string
 */
class ValidCommaSeperatedEmailString implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $emails = commaSeperatedStringToArray($value);
        // Validate all strings in array are valid emails
        foreach($emails as $email) {
            $validator = Validator::make(['email' => $email], [
                'email' => 'required|email'
            ]);
            if($validator->fails()) {
                return false;
            }
        }
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The email format is invalid, please check and try again.';
    }
}
