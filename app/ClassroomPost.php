<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClassroomPost extends Model
{
    protected $guarded = [];
    /**
     * -------------------
     * Relationships
     * -------------------
     */
    public function classroom()
    {
        return $this->belongsTo(Classroom::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * -----------------
     * Accessors
     * -----------------
     */
    /**
     * Returns 'categories' column as array from JSON string
     * @return array
     */
    public function getCategoriesAttribute($value)
    {
        return json_decode($value);
    }

    public function getCategoryTagsHtmlAttribute()
    {
        $html = "";
        foreach($this->categories as $category) {
            $color = self::getCategoryColor($category);
            $html .= '<span class="status-tag '. $color .' mr-1">'. $category .'</span>';
        }
        return $html;
    }

    /**
     * -----------------
     * Mutators
     * -----------------
     */
    /**
     * Set 'categories' column from array to JSON string in DB
     */
    public function setCategoriesAttribute($value)
    {
        $this->attributes['categories'] = json_encode($value);
    }

    /**
     * -------------------
     * Helpers
     * -------------------
     */
    /**
     * Returns category color
     * @param string $category
     * @return string
     */
    public static function getCategoryColor($category)
    {
        $dict = [
            "lesson" => "yellow",
            "resource" => "green",
            "homework" => "red",
            "others" => "gray",
        ];

        return $dict[$category];
    }
}
