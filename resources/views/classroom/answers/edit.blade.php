@extends('classroom.layouts.app')

@section('meta')
<title>Edit Answer - {{ str_limit($answer->answer, 50) }} | {{ config('app.name') }}</title>
@endsection

@section('styles')
@endsection

@section('content')
@component('classroom.common.breadcrumbs')
    <a href="{{ route('classroom.classrooms.show', $classroom->id) }}">Dashboard</a>
    <a href="{{ route('classroom.questions', $classroom->id) }}">Questions</a> /
    <a href="{{ route('classroom.questions.show', [$classroom->id, $question->id]) }}">{{ $question->id }}</a> /
    Answers /
    {{ $answer->id }} /
    Edit
@endcomponent

@component('classroom.common.page_heading', ['title' => 'Edit Answer'])
@endcomponent

<div class="ask-wrapper">
    <h1 class="heading">Edit</h1>
    <h2 class="sub-heading">Edit your answer here!</h2>

    <form class="form-container" method="POST" action="{{ route('classroom.answers.update', [$classroom->id, $answer->id]) }}">
        @csrf
        @method('PATCH')
        <div class="form-field">
            <label>Answer</label>

            @if($errors->any())
                <div class="editor">{!! old('description') !!}</div>
            @else
                <div class="editor">{!! $question->description !!}</div>
            @endif
                <input type="hidden" name="description">
            @error('description')
                <span class="error-message">{{ $message }}</span>
            @enderror
        </div>

        <div class="text-center">
            <button type="submit" class="ask-button button--primary askButton">Update</button>
        </div>
    </form>
</div>

@endsection

@section('scripts')
@endsection
