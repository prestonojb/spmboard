@extends('voyager::master')

@php
$page_title = $is_edit ? 'Edit Blog' : 'Create Blog';
@endphp
@section('page_title', $page_title)

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_header')
<div class="container-fluid">
    <h1 class="page-title">
        {{ $page_title }}
    </h1>
    @if($is_edit)
        <a target="_blank" href="{{ $blog->show_url }}" class="btn btn-warning">View on Website</a>
    @endif
</div>
@stop

@section('content')
    <div class="page-content container-fluid">
        @component('vendor.voyager.blogs.common.form', ['is_edit' => $is_edit, 'blog_authors' => $blog_authors, 'blog_categories' => $blog_categories, 'blog' => isset($blog) ? $blog : null])
        @endcomponent
    </div>
@stop

@section('javascript')
<script src="{{ mix('js/forum.js') }}"></script>
{{-- TinyMCE --}}
<script src="https://cdn.tiny.cloud/1/si4jvonmpvtlfypotvf9zu2vci2zsjavfd4v1oyvnzncszag/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>    {{-- KaTeX --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ" crossorigin="anonymous"></script>
<script>
$(function(){
    // Reference: https://www.tiny.cloud/docs/plugins/image/
    function image_upload_handler (blobInfo, success, failure, progress) {
        var xhr, formData;

        xhr = new XMLHttpRequest();
        xhr.withCredentials = false;
        xhr.open('POST', '/editor/upload');

        xhr.upload.onprogress = function (e) {
        progress(e.loaded / e.total * 100);
        };

        xhr.onload = function() {
        var json;

        if (xhr.status === 403) {
            failure('HTTP Error: ' + xhr.status, { remove: true });
            return;
        }

        if (xhr.status < 200 || xhr.status >= 300) {
            failure('HTTP Error: ' + xhr.status);
            return;
        }

        json = JSON.parse(xhr.responseText);

        if (!json || typeof json.location != 'string') {
            failure('Invalid JSON: ' + xhr.responseText);
            return;
        }

        success(json.location);
        };

        xhr.onerror = function () {
        failure('Image upload failed due to a XHR Transport error. Code: ' + xhr.status);
        };

        formData = new FormData();
        formData.append('file', blobInfo.blob(), blobInfo.filename());

        xhr.send(formData);
    };

    tinymce.init({
        selector: 'textarea.tinymce-editor',
        height: 500,
        block_formats: 'Header 2=h2; Header 3=h3; Header 4=h4; Paragraph=p; Wrapper=wrapper;',
        formats: {
            'wrapper': {block: 'div', classes: 'content-wrapper', wrapper: true},
        },
        body_class: 'tinymce-editor-contents',
        content_css: '/css/app.css',
        content_style: "@import url('https://fonts.googleapis.com/css?family=Poppins:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i'); html,body {background-color: white;}",
        menubar: false,
        statusbar: false,
        default_link_target: '_blank',
        link_default_protocol: 'https',
        remove_linebreaks : true,
        setup: function(editor) {
            editor.on('init', function(e) {
                $('.loading-icon').hide();
                var content = editor.getContent();;
                editor.setContent(content);
            });
        },
        max_height: 600,
        table_toolbar: "tableprops tabledelete | tableinsertrowbefore tableinsertrowafter tabledeleterow | tableinsertcolbefore tableinsertcolafter tabledeletecol tabledeletecol",
        plugins: [
            'advlist autoresize autolink charmap lists link image charmap print preview anchor',
            'searchreplace visualblocks code fullscreen hr',
            'insertdatetime media table paste code wordcount'
        ],
        toolbar: 'formatselect | ' +
            'bold italic underline | alignleft aligncenter ' +
            'alignright alignjustify | superscript subscript charmap | table link image | bullist numlist hr | ' +
            'removeformat',
        charmap: [
            [0x2190, 'left arrow'],
            [0x2192, 'right arrow'],
            [0x21CC, 'reversible arrow'],
            [0x2260, 'not equal'],
            [0x2103, 'celcius'],
        ],
        images_upload_handler: image_upload_handler,
    });
});
</script>
@stop
