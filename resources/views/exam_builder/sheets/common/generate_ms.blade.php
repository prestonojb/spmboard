{{-- Toggle Modal Button --}}
<div class="panel p-0 maxw-460px mx-auto text-center ">
    <div class="px-3 py-4">
        <h4 class="mb-3">Generate your Mark Scheme here!</h4>
        <button class="button--primary" id="generateMarkSchemeButton" data-toggle="modal" data-target="#generateMarkSchemeModal">Generate</button>
    </div>
</div>

{{-- Modal --}}
<div class="modal fade" id="generateMarkSchemeModal" tabindex="-1" role="dialog" aria-labelledby="generateMarkSchemeModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        {{-- Form --}}
        <form action="{{ route('exam_builder.sheets.generate_ms', $sheet->id) }}" method="POST" id="generateMarkSchemeForm">
            @csrf
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Generate Mark Scheme</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="mb-4">
                        {{-- Options --}}
                        <label for="" class="mb-2">Options</label>

                        {{-- Show Item Label --}}
                        <div class="mb-2">
                            <input type="checkbox" name="show_label" id="show_label" {{ old('show_label') ? 'checked' : '' }}>
                            <label for="show_label">{{ __('Show Question Label') }}</label>
                        </div>

                        {{-- Show Watermark --}}
                        <div class="mb-2">
                            <input type="checkbox" name="hide_watermark" id="hide_watermark" {{ old('hide_watermark') ? 'checked' : '' }}
                            @cannot('hide_watermark', \App\ExamBuilderSheet::class) disabled @endcannot>
                            <label for="hide_watermark">{{ __('Hide Watermark') }}</label>
                        </div>
                    </div>

                    <p class="gray2">Your mark scheme will take <b>5-10 seconds</b> to generate.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="button--primary--inverse" data-dismiss="modal">Close</button>
                    <button type="button" class="button--primary loadOnSubmitButton" style="width: 94px;">Generate</button>
                </div>
            </div>
        </form>
    </div>
</div>
