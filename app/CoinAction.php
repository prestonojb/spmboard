<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CoinAction extends Model
{
    protected $guarded = [];

    public function coin_actionable()
    {
        return $this->morphTo();
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getDisplayCreatedAtAttribute()
    {
        return $this->created_at->format('M j, Y g:iA');
    }
}
