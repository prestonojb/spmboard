<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class MathBoardQuizThankYou extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data, $is_existing_user)
    {
        $this->data = $data;
        $this->is_existing_user = $is_existing_user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.events.2020.math_board_quiz')
                    ->with([
                        'data' => $this->data,
                        'is_existing_user' => $this->is_existing_user,
                        'login_url' => route('login'),
                        'register_url' => route('register'),
                        'facebook_share_url' => "https://www.facebook.com/share.php?u=". route('events.2020.get_math_board_quiz_view') ."&title=Join me for this Board Quiz and stand a chance to win Netflix Gift Cards worth RM50 now!",
                        'twitter_share_url' => "https://twitter.com/share?url=".route('events.2020.get_math_board_quiz_view')."&text=Join me for this Board Quiz and stand a chance to win Netflix Gift Cards worth RM50 now!",
                        'whatsapp_share_url' => "https://api.whatsapp.com/send?text=Join me for this Board Quiz and stand a chance to win Netflix Gift Cards worth RM50 now! To join Board Quiz, click this link: ".route('events.2020.get_math_board_quiz_view'),
                    ]);
    }
}
