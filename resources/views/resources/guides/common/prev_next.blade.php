<div class="resource__nav-menu">
    @if(!is_null($guide->prev()))
        <a href="{{ $guide->prev()->show_url }}" class="button--primary--inverse resource__prev-button mb-2 mb-md-0">
            <span class="iconify" data-icon="bx:bx-chevron-left" data-inline="false"></span>
            <div class="w-100">
                <p class="prev-next-text gray2">Previous:</p>
                <p class="title">{{ str_limit($guide->prev()->title, 30) }}</p>
            </div>
        </a>
    @else
        <div></div>
    @endif

    @if(!is_null($guide->next()))
        <a href="{{ $guide->next()->show_url }}" class="button--primary resource__next-button mb-2 mb-md-0">
            <div class="w-100">
                <p class="prev-next-text gray5">Next:</p>
                <p class="title">{{ str_limit($guide->next()->title, 30) }}</p>
            </div>
            <span class="iconify" data-icon="bx:bx-chevron-right" data-inline="false"></span>
        </a>
    @else
        <div></div>
    @endif
</div>
