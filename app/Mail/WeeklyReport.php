<?php

namespace App\Mail;

use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class WeeklyReport extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.reports.weekly')
                    ->with([
                        'data' => $this->data,
                        'generated_at' => Carbon::now()->toDayDateTimeString(),
                        'start_of_week' => Carbon::today()->subWeek()->startOfWeek()->toFormattedDateString(),
                        'end_of_week' => Carbon::today()->subWeek()->endOfWeek()->toFormattedDateString(),
                    ]);
    }
}
