<?php

use Illuminate\Database\Seeder;

class CustomizedPermissionsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('permissions')->delete();
        
        \DB::table('permissions')->insert(array (
            0 => 
            array (
                'id' => 1,
                'key' => 'browse_admin',
                'table_name' => NULL,
                'created_at' => '2020-02-05 16:38:06',
                'updated_at' => '2020-02-05 16:38:06',
            ),
            1 => 
            array (
                'id' => 2,
                'key' => 'browse_bread',
                'table_name' => NULL,
                'created_at' => '2020-02-05 16:38:06',
                'updated_at' => '2020-02-05 16:38:06',
            ),
            2 => 
            array (
                'id' => 3,
                'key' => 'browse_database',
                'table_name' => NULL,
                'created_at' => '2020-02-05 16:38:06',
                'updated_at' => '2020-02-05 16:38:06',
            ),
            3 => 
            array (
                'id' => 4,
                'key' => 'browse_media',
                'table_name' => NULL,
                'created_at' => '2020-02-05 16:38:06',
                'updated_at' => '2020-02-05 16:38:06',
            ),
            4 => 
            array (
                'id' => 5,
                'key' => 'browse_compass',
                'table_name' => NULL,
                'created_at' => '2020-02-05 16:38:06',
                'updated_at' => '2020-02-05 16:38:06',
            ),
            5 => 
            array (
                'id' => 6,
                'key' => 'browse_menus',
                'table_name' => 'menus',
                'created_at' => '2020-02-05 16:38:06',
                'updated_at' => '2020-02-05 16:38:06',
            ),
            6 => 
            array (
                'id' => 7,
                'key' => 'read_menus',
                'table_name' => 'menus',
                'created_at' => '2020-02-05 16:38:06',
                'updated_at' => '2020-02-05 16:38:06',
            ),
            7 => 
            array (
                'id' => 8,
                'key' => 'edit_menus',
                'table_name' => 'menus',
                'created_at' => '2020-02-05 16:38:06',
                'updated_at' => '2020-02-05 16:38:06',
            ),
            8 => 
            array (
                'id' => 9,
                'key' => 'add_menus',
                'table_name' => 'menus',
                'created_at' => '2020-02-05 16:38:06',
                'updated_at' => '2020-02-05 16:38:06',
            ),
            9 => 
            array (
                'id' => 10,
                'key' => 'delete_menus',
                'table_name' => 'menus',
                'created_at' => '2020-02-05 16:38:06',
                'updated_at' => '2020-02-05 16:38:06',
            ),
            10 => 
            array (
                'id' => 11,
                'key' => 'browse_roles',
                'table_name' => 'roles',
                'created_at' => '2020-02-05 16:38:06',
                'updated_at' => '2020-02-05 16:38:06',
            ),
            11 => 
            array (
                'id' => 12,
                'key' => 'read_roles',
                'table_name' => 'roles',
                'created_at' => '2020-02-05 16:38:06',
                'updated_at' => '2020-02-05 16:38:06',
            ),
            12 => 
            array (
                'id' => 13,
                'key' => 'edit_roles',
                'table_name' => 'roles',
                'created_at' => '2020-02-05 16:38:06',
                'updated_at' => '2020-02-05 16:38:06',
            ),
            13 => 
            array (
                'id' => 14,
                'key' => 'add_roles',
                'table_name' => 'roles',
                'created_at' => '2020-02-05 16:38:06',
                'updated_at' => '2020-02-05 16:38:06',
            ),
            14 => 
            array (
                'id' => 15,
                'key' => 'delete_roles',
                'table_name' => 'roles',
                'created_at' => '2020-02-05 16:38:06',
                'updated_at' => '2020-02-05 16:38:06',
            ),
            15 => 
            array (
                'id' => 16,
                'key' => 'browse_users',
                'table_name' => 'users',
                'created_at' => '2020-02-05 16:38:06',
                'updated_at' => '2020-02-05 16:38:06',
            ),
            16 => 
            array (
                'id' => 17,
                'key' => 'read_users',
                'table_name' => 'users',
                'created_at' => '2020-02-05 16:38:06',
                'updated_at' => '2020-02-05 16:38:06',
            ),
            17 => 
            array (
                'id' => 18,
                'key' => 'edit_users',
                'table_name' => 'users',
                'created_at' => '2020-02-05 16:38:06',
                'updated_at' => '2020-02-05 16:38:06',
            ),
            18 => 
            array (
                'id' => 19,
                'key' => 'add_users',
                'table_name' => 'users',
                'created_at' => '2020-02-05 16:38:06',
                'updated_at' => '2020-02-05 16:38:06',
            ),
            19 => 
            array (
                'id' => 20,
                'key' => 'delete_users',
                'table_name' => 'users',
                'created_at' => '2020-02-05 16:38:06',
                'updated_at' => '2020-02-05 16:38:06',
            ),
            20 => 
            array (
                'id' => 21,
                'key' => 'browse_settings',
                'table_name' => 'settings',
                'created_at' => '2020-02-05 16:38:06',
                'updated_at' => '2020-02-05 16:38:06',
            ),
            21 => 
            array (
                'id' => 22,
                'key' => 'read_settings',
                'table_name' => 'settings',
                'created_at' => '2020-02-05 16:38:06',
                'updated_at' => '2020-02-05 16:38:06',
            ),
            22 => 
            array (
                'id' => 23,
                'key' => 'edit_settings',
                'table_name' => 'settings',
                'created_at' => '2020-02-05 16:38:06',
                'updated_at' => '2020-02-05 16:38:06',
            ),
            23 => 
            array (
                'id' => 24,
                'key' => 'add_settings',
                'table_name' => 'settings',
                'created_at' => '2020-02-05 16:38:06',
                'updated_at' => '2020-02-05 16:38:06',
            ),
            24 => 
            array (
                'id' => 25,
                'key' => 'delete_settings',
                'table_name' => 'settings',
                'created_at' => '2020-02-05 16:38:06',
                'updated_at' => '2020-02-05 16:38:06',
            ),
            25 => 
            array (
                'id' => 26,
                'key' => 'browse_blogs',
                'table_name' => 'blogs',
                'created_at' => '2020-02-05 16:43:31',
                'updated_at' => '2020-02-05 16:43:31',
            ),
            26 => 
            array (
                'id' => 27,
                'key' => 'read_blogs',
                'table_name' => 'blogs',
                'created_at' => '2020-02-05 16:43:31',
                'updated_at' => '2020-02-05 16:43:31',
            ),
            27 => 
            array (
                'id' => 28,
                'key' => 'edit_blogs',
                'table_name' => 'blogs',
                'created_at' => '2020-02-05 16:43:31',
                'updated_at' => '2020-02-05 16:43:31',
            ),
            28 => 
            array (
                'id' => 29,
                'key' => 'add_blogs',
                'table_name' => 'blogs',
                'created_at' => '2020-02-05 16:43:31',
                'updated_at' => '2020-02-05 16:43:31',
            ),
            29 => 
            array (
                'id' => 30,
                'key' => 'delete_blogs',
                'table_name' => 'blogs',
                'created_at' => '2020-02-05 16:43:31',
                'updated_at' => '2020-02-05 16:43:31',
            ),
            30 => 
            array (
                'id' => 31,
                'key' => 'browse_groups',
                'table_name' => 'groups',
                'created_at' => '2020-02-05 16:55:20',
                'updated_at' => '2020-02-05 16:55:20',
            ),
            31 => 
            array (
                'id' => 32,
                'key' => 'read_groups',
                'table_name' => 'groups',
                'created_at' => '2020-02-05 16:55:20',
                'updated_at' => '2020-02-05 16:55:20',
            ),
            32 => 
            array (
                'id' => 33,
                'key' => 'edit_groups',
                'table_name' => 'groups',
                'created_at' => '2020-02-05 16:55:20',
                'updated_at' => '2020-02-05 16:55:20',
            ),
            33 => 
            array (
                'id' => 34,
                'key' => 'add_groups',
                'table_name' => 'groups',
                'created_at' => '2020-02-05 16:55:20',
                'updated_at' => '2020-02-05 16:55:20',
            ),
            34 => 
            array (
                'id' => 35,
                'key' => 'delete_groups',
                'table_name' => 'groups',
                'created_at' => '2020-02-05 16:55:20',
                'updated_at' => '2020-02-05 16:55:20',
            ),
            35 => 
            array (
                'id' => 36,
                'key' => 'browse_blog_authors',
                'table_name' => 'blog_authors',
                'created_at' => '2020-02-05 17:07:36',
                'updated_at' => '2020-02-05 17:07:36',
            ),
            36 => 
            array (
                'id' => 37,
                'key' => 'read_blog_authors',
                'table_name' => 'blog_authors',
                'created_at' => '2020-02-05 17:07:36',
                'updated_at' => '2020-02-05 17:07:36',
            ),
            37 => 
            array (
                'id' => 38,
                'key' => 'edit_blog_authors',
                'table_name' => 'blog_authors',
                'created_at' => '2020-02-05 17:07:36',
                'updated_at' => '2020-02-05 17:07:36',
            ),
            38 => 
            array (
                'id' => 39,
                'key' => 'add_blog_authors',
                'table_name' => 'blog_authors',
                'created_at' => '2020-02-05 17:07:36',
                'updated_at' => '2020-02-05 17:07:36',
            ),
            39 => 
            array (
                'id' => 40,
                'key' => 'delete_blog_authors',
                'table_name' => 'blog_authors',
                'created_at' => '2020-02-05 17:07:36',
                'updated_at' => '2020-02-05 17:07:36',
            ),
            40 => 
            array (
                'id' => 41,
                'key' => 'browse_blog_categories',
                'table_name' => 'blog_categories',
                'created_at' => '2020-02-05 17:07:41',
                'updated_at' => '2020-02-05 17:07:41',
            ),
            41 => 
            array (
                'id' => 42,
                'key' => 'read_blog_categories',
                'table_name' => 'blog_categories',
                'created_at' => '2020-02-05 17:07:41',
                'updated_at' => '2020-02-05 17:07:41',
            ),
            42 => 
            array (
                'id' => 43,
                'key' => 'edit_blog_categories',
                'table_name' => 'blog_categories',
                'created_at' => '2020-02-05 17:07:41',
                'updated_at' => '2020-02-05 17:07:41',
            ),
            43 => 
            array (
                'id' => 44,
                'key' => 'add_blog_categories',
                'table_name' => 'blog_categories',
                'created_at' => '2020-02-05 17:07:41',
                'updated_at' => '2020-02-05 17:07:41',
            ),
            44 => 
            array (
                'id' => 45,
                'key' => 'delete_blog_categories',
                'table_name' => 'blog_categories',
                'created_at' => '2020-02-05 17:07:41',
                'updated_at' => '2020-02-05 17:07:41',
            ),
            45 => 
            array (
                'id' => 46,
                'key' => 'browse_questions',
                'table_name' => 'questions',
                'created_at' => '2020-02-05 17:33:29',
                'updated_at' => '2020-02-05 17:33:29',
            ),
            46 => 
            array (
                'id' => 47,
                'key' => 'read_questions',
                'table_name' => 'questions',
                'created_at' => '2020-02-05 17:33:29',
                'updated_at' => '2020-02-05 17:33:29',
            ),
            47 => 
            array (
                'id' => 48,
                'key' => 'edit_questions',
                'table_name' => 'questions',
                'created_at' => '2020-02-05 17:33:29',
                'updated_at' => '2020-02-05 17:33:29',
            ),
            48 => 
            array (
                'id' => 49,
                'key' => 'add_questions',
                'table_name' => 'questions',
                'created_at' => '2020-02-05 17:33:29',
                'updated_at' => '2020-02-05 17:33:29',
            ),
            49 => 
            array (
                'id' => 50,
                'key' => 'delete_questions',
                'table_name' => 'questions',
                'created_at' => '2020-02-05 17:33:29',
                'updated_at' => '2020-02-05 17:33:29',
            ),
            50 => 
            array (
                'id' => 51,
                'key' => 'browse_answers',
                'table_name' => 'answers',
                'created_at' => '2020-02-05 17:33:37',
                'updated_at' => '2020-02-05 17:33:37',
            ),
            51 => 
            array (
                'id' => 52,
                'key' => 'read_answers',
                'table_name' => 'answers',
                'created_at' => '2020-02-05 17:33:37',
                'updated_at' => '2020-02-05 17:33:37',
            ),
            52 => 
            array (
                'id' => 53,
                'key' => 'edit_answers',
                'table_name' => 'answers',
                'created_at' => '2020-02-05 17:33:37',
                'updated_at' => '2020-02-05 17:33:37',
            ),
            53 => 
            array (
                'id' => 54,
                'key' => 'add_answers',
                'table_name' => 'answers',
                'created_at' => '2020-02-05 17:33:37',
                'updated_at' => '2020-02-05 17:33:37',
            ),
            54 => 
            array (
                'id' => 55,
                'key' => 'delete_answers',
                'table_name' => 'answers',
                'created_at' => '2020-02-05 17:33:37',
                'updated_at' => '2020-02-05 17:33:37',
            ),
            55 => 
            array (
                'id' => 56,
                'key' => 'browse_subjects',
                'table_name' => 'subjects',
                'created_at' => '2020-02-06 21:46:23',
                'updated_at' => '2020-02-06 21:46:23',
            ),
            56 => 
            array (
                'id' => 57,
                'key' => 'read_subjects',
                'table_name' => 'subjects',
                'created_at' => '2020-02-06 21:46:23',
                'updated_at' => '2020-02-06 21:46:23',
            ),
            57 => 
            array (
                'id' => 58,
                'key' => 'edit_subjects',
                'table_name' => 'subjects',
                'created_at' => '2020-02-06 21:46:23',
                'updated_at' => '2020-02-06 21:46:23',
            ),
            58 => 
            array (
                'id' => 59,
                'key' => 'add_subjects',
                'table_name' => 'subjects',
                'created_at' => '2020-02-06 21:46:23',
                'updated_at' => '2020-02-06 21:46:23',
            ),
            59 => 
            array (
                'id' => 60,
                'key' => 'delete_subjects',
                'table_name' => 'subjects',
                'created_at' => '2020-02-06 21:46:23',
                'updated_at' => '2020-02-06 21:46:23',
            ),
            60 => 
            array (
                'id' => 61,
                'key' => 'browse_chapters',
                'table_name' => 'chapters',
                'created_at' => '2020-02-06 21:47:34',
                'updated_at' => '2020-02-06 21:47:34',
            ),
            61 => 
            array (
                'id' => 62,
                'key' => 'read_chapters',
                'table_name' => 'chapters',
                'created_at' => '2020-02-06 21:47:34',
                'updated_at' => '2020-02-06 21:47:34',
            ),
            62 => 
            array (
                'id' => 63,
                'key' => 'edit_chapters',
                'table_name' => 'chapters',
                'created_at' => '2020-02-06 21:47:34',
                'updated_at' => '2020-02-06 21:47:34',
            ),
            63 => 
            array (
                'id' => 64,
                'key' => 'add_chapters',
                'table_name' => 'chapters',
                'created_at' => '2020-02-06 21:47:34',
                'updated_at' => '2020-02-06 21:47:34',
            ),
            64 => 
            array (
                'id' => 65,
                'key' => 'delete_chapters',
                'table_name' => 'chapters',
                'created_at' => '2020-02-06 21:47:34',
                'updated_at' => '2020-02-06 21:47:34',
            ),
            65 => 
            array (
                'id' => 66,
                'key' => 'browse_hooks',
                'table_name' => NULL,
                'created_at' => '2020-02-07 10:00:18',
                'updated_at' => '2020-02-07 10:00:18',
            ),
            66 => 
            array (
                'id' => 67,
                'key' => 'browse_past_papers',
                'table_name' => 'past_papers',
                'created_at' => '2020-04-26 06:16:08',
                'updated_at' => '2020-04-26 06:16:08',
            ),
            67 => 
            array (
                'id' => 68,
                'key' => 'read_past_papers',
                'table_name' => 'past_papers',
                'created_at' => '2020-04-26 06:16:08',
                'updated_at' => '2020-04-26 06:16:08',
            ),
            68 => 
            array (
                'id' => 69,
                'key' => 'edit_past_papers',
                'table_name' => 'past_papers',
                'created_at' => '2020-04-26 06:16:08',
                'updated_at' => '2020-04-26 06:16:08',
            ),
            69 => 
            array (
                'id' => 70,
                'key' => 'add_past_papers',
                'table_name' => 'past_papers',
                'created_at' => '2020-04-26 06:16:08',
                'updated_at' => '2020-04-26 06:16:08',
            ),
            70 => 
            array (
                'id' => 71,
                'key' => 'delete_past_papers',
                'table_name' => 'past_papers',
                'created_at' => '2020-04-26 06:16:08',
                'updated_at' => '2020-04-26 06:16:08',
            ),
            71 => 
            array (
                'id' => 72,
                'key' => 'browse_notes',
                'table_name' => 'notes',
                'created_at' => '2020-04-26 08:45:52',
                'updated_at' => '2020-04-26 08:45:52',
            ),
            72 => 
            array (
                'id' => 73,
                'key' => 'read_notes',
                'table_name' => 'notes',
                'created_at' => '2020-04-26 08:45:52',
                'updated_at' => '2020-04-26 08:45:52',
            ),
            73 => 
            array (
                'id' => 74,
                'key' => 'edit_notes',
                'table_name' => 'notes',
                'created_at' => '2020-04-26 08:45:52',
                'updated_at' => '2020-04-26 08:45:52',
            ),
            74 => 
            array (
                'id' => 75,
                'key' => 'add_notes',
                'table_name' => 'notes',
                'created_at' => '2020-04-26 08:45:52',
                'updated_at' => '2020-04-26 08:45:52',
            ),
            75 => 
            array (
                'id' => 76,
                'key' => 'delete_notes',
                'table_name' => 'notes',
                'created_at' => '2020-04-26 08:45:52',
                'updated_at' => '2020-04-26 08:45:52',
            ),
            76 => 
            array (
                'id' => 77,
                'key' => 'browse_topical_past_papers',
                'table_name' => 'topical_past_papers',
                'created_at' => '2020-04-26 13:50:17',
                'updated_at' => '2020-04-26 13:50:17',
            ),
            77 => 
            array (
                'id' => 78,
                'key' => 'read_topical_past_papers',
                'table_name' => 'topical_past_papers',
                'created_at' => '2020-04-26 13:50:17',
                'updated_at' => '2020-04-26 13:50:17',
            ),
            78 => 
            array (
                'id' => 79,
                'key' => 'edit_topical_past_papers',
                'table_name' => 'topical_past_papers',
                'created_at' => '2020-04-26 13:50:17',
                'updated_at' => '2020-04-26 13:50:17',
            ),
            79 => 
            array (
                'id' => 80,
                'key' => 'add_topical_past_papers',
                'table_name' => 'topical_past_papers',
                'created_at' => '2020-04-26 13:50:17',
                'updated_at' => '2020-04-26 13:50:17',
            ),
            80 => 
            array (
                'id' => 81,
                'key' => 'delete_topical_past_papers',
                'table_name' => 'topical_past_papers',
                'created_at' => '2020-04-26 13:50:17',
                'updated_at' => '2020-04-26 13:50:17',
            ),
            81 => 
            array (
                'id' => 82,
                'key' => 'browse_coin_actions',
                'table_name' => 'coin_actions',
                'created_at' => '2020-05-02 10:29:18',
                'updated_at' => '2020-05-02 10:29:18',
            ),
            82 => 
            array (
                'id' => 83,
                'key' => 'read_coin_actions',
                'table_name' => 'coin_actions',
                'created_at' => '2020-05-02 10:29:18',
                'updated_at' => '2020-05-02 10:29:18',
            ),
            83 => 
            array (
                'id' => 84,
                'key' => 'edit_coin_actions',
                'table_name' => 'coin_actions',
                'created_at' => '2020-05-02 10:29:18',
                'updated_at' => '2020-05-02 10:29:18',
            ),
            84 => 
            array (
                'id' => 85,
                'key' => 'add_coin_actions',
                'table_name' => 'coin_actions',
                'created_at' => '2020-05-02 10:29:18',
                'updated_at' => '2020-05-02 10:29:18',
            ),
            85 => 
            array (
                'id' => 86,
                'key' => 'delete_coin_actions',
                'table_name' => 'coin_actions',
                'created_at' => '2020-05-02 10:29:18',
                'updated_at' => '2020-05-02 10:29:18',
            ),
        ));
        
        
    }
}