<?php

namespace App\Console\Commands;

use App\PastPaper;
use App\Notes;
use App\TopicalPastPaper;
use App\Guide;
use App\Resource;
use Illuminate\Console\Command;

class SynchronizeResources extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:resources';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Synchronize resources(polymorphic) on "resources" table.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Past Papers
        $past_papers = PastPaper::all();
        foreach( $past_papers as $past_paper ) {
            $resource_exists = Resource::where('resourceable_type', PastPaper::class)->where('resourceable_id', $past_paper->id)->exists();
            if( !$resource_exists ) {
                Resource::create([
                    'resourceable_type' => PastPaper::class,
                    'resourceable_id' => $past_paper->id,
                ]);
            }
        }

        // Topical Past Papers
        $past_papers = TopicalPastPaper::all();
        foreach( $past_papers as $past_paper ) {
            $resource_exists = Resource::where('resourceable_type', TopicalPastPaper::class)->where('resourceable_id', $past_paper->id)->exists();
            if( !$resource_exists ) {
                Resource::create([
                    'resourceable_type' => TopicalPastPaper::class,
                    'resourceable_id' => $past_paper->id,
                ]);
            }
        }

        // Notes
        $notes = Notes::all();
        foreach( $notes as $note ) {
            $resource_exists = Resource::where('resourceable_type', Notes::class)->where('resourceable_id', $note->id)->exists();
            if( !$resource_exists ) {
                Resource::create([
                    'resourceable_type' => Notes::class,
                    'resourceable_id' => $note->id,
                ]);
            }
        }

        // Guides
        $guides = Guide::all();
        foreach( $guides as $guide ) {
            $resource_exists = Resource::where('resourceable_type', Guide::class)->where('resourceable_id', $guide->id)->exists();
            if( !$resource_exists ) {
                Resource::create([
                    'resourceable_type' => Guide::class,
                    'resourceable_id' => $guide->id,
                ]);
            }
        }
    }
}
