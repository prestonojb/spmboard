<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class CertificationApplication extends Model
{
    protected $guarded = [];

    /**
     * ---------------
     * RELATIONSHIPS
     * ---------------
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * ---------------
     * ACCESSORS
     * ---------------
     */
    public function getStatusTagHtmlAttribute()
    {
        $dict = [
            'declined' => 'red',
            'pending' => 'yellow',
            'approved' => 'green',
        ];
        return "<span class='status-tag {$dict[$this->status]}'>".ucfirst($this->status)."</span>";
    }

    public function getResumeAttribute($value)
    {
        if($value){
            if(strpos($value, 'amazonaws')){
                //Amazon link for image
                return $value;
            } else{
                //Amazon S3
                $updated_value = str_replace('\\', '/', $value);
                return Storage::cloud()->url($updated_value);
            }
        } else {
            return null;
        }
    }

    /**
     * Returns show URL
     * @return string
     */
    public function getShowUrlAttribute()
    {
        return route('certification_applications.index');
    }

    /**
     * ---------------
     * HELPERS
     * ---------------
     */
    /**
     * Save resume on S3 and DB
     */
    public function saveResume($file)
    {
        //Store image to S3
        $path = $file->store('resumes/', 's3');
        $filename = $file->hashName();
        //Save image url in database
        $file_url = 'resumes/'.$filename;
        $this->update([
            'resume' => $file_url,
        ]);
    }

    /**
     * Returns true if application is resolved (approved/declined), else false
     * @return boolean
     */
    public function isResolved()
    {
        return in_array($this->status, ['approved', 'declined']);
    }
}
