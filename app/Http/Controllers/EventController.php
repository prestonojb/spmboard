<?php

namespace App\Http\Controllers;

use App\ExamBoard;
use App\Mail\MathBoardQuizThankYou;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Revolution\Google\Sheets\Facades\Sheets;

class EventController extends Controller
{
    public function getMathQuiz2020View()
    {
        $quiz_start_date = setting('marketing.quiz_start_date');
        $quiz_end_date = setting('marketing.quiz_end_date');

        if( !is_null( $quiz_start_date ) && !is_null( $quiz_end_date ) ) {
            $start_time = Carbon::parse( $quiz_start_date )->startOfDay();
            $end_time = Carbon::parse( $quiz_end_date )->endOfDay();

            if( Carbon::now()->lessThan( $start_time ) ) {
                $status = 'pre';
            } elseif( Carbon::now()->greaterThan( $start_time ) && Carbon::now()->lessThan( $end_time ) ) {
                $status = 'active';
            } else {
                $status = 'post';
            }
        }

        $questions = ['How many significant figures are there in 5030.0?',
                            'Find the sum of the interior angles of a 25-sided polygon.',
                            'Preston sells the horse for $17 280. Calculate the percentage loss on the $21 600 he paid for the horse.',
                            'A Laptop is being sold at a 25% discount. The sale price is $619. What was its original price?',
                            'Preston and Joshua share a sum of money with the ratio of Preston:Joshua = 9:7. Preston receives $600 more than Joshua. What is the sum of money that both of them received?'];

        $options = [[2, 3, 4, 5],
                    [4140, 4500, 4320, 3600],
                    ['20%', '80%', '25%', '30%'],
                    ['$2476', '$825.33', '$773.75', '$728.25'],
                    [2000, 4800, 8400, 4400]];

        $exam_boards = ExamBoard::all();

        return view('events.2020.math_board_quiz.quiz', compact('questions', 'options', 'exam_boards', 'start_time', 'end_time', 'status'));
    }

    public function mathQuiz2020()
    {
        request()->validate([
            'name' => 'required',
            'email' => 'required|email',
            'phone_number' => 'required',
            'exam_board' => 'required',
            'question_0' => 'required',
            'question_1' => 'required',
            'question_2' => 'required',
            'question_3' => 'required',
            'question_4' => 'required',
        ]);
        $data = [
            'name' => request()->name,
            'email' => request()->email,
            'phone_number' => request()->phone_number,
            'exam_board' => request()->exam_board,
            'question_0' => request()->question_0,
            'question_1' => request()->question_1,
            'question_2' => request()->question_2,
            'question_3' => request()->question_3,
            'question_4' => request()->question_4,
            'submitted_at' => Carbon::now()->toDayDateTimeString(),
        ];

        // Add record to Google Sheets
        $sheets = Sheets::spreadsheet(config('google.sheets.2020_board_math_quiz'))
        ->sheet('entries')
        ->append( [$data] );

        // Add to no. of entries
        DB::table('settings')->where('key', 'marketing.2020_math_board_quiz_no_of_entries')->increment('value');

        // Flash message to user
        Session::flash('success', 'Your entry has been recorded! We will announce the winners on our Facebook Page very soon. Stay tuned ;)');

        // Check if is existing user
        $is_existing_user = User::where('email', request('email'))->exists() ? 1 : 0;

        // Send Thank you email to participant
        Mail::to( $data['email'] )->send(new MathBoardQuizThankYou($data, $is_existing_user));

        return redirect()->route('events.2020.math_board_quiz.thank_you', $is_existing_user);
    }

    public function getMathQuiz2020ThankYouView($is_existing_user)
    {
        return view('events.2020.math_board_quiz.thank_you', compact('is_existing_user'));
    }

    public function getStoichiometryMania2020RegisterView()
    {
        return view('events.2020.stoichiometry_mania.register');
    }

    public function stoichiometryMania2020Register()
    {
        request()->validate([
            'phone_number' => 'required',
        ]);

        $data = [
            'name' => request()->name,
            'email' => auth()->user()->email,
            'phone_number' => request()->phone_number,
            'submitted_at' => Carbon::now()->toDayDateTimeString(),
        ];

        // Add record to Google Sheets
        $sheets = Sheets::spreadsheet(config('google.sheets.2020_stoichiometry_mania'))
                        ->sheet('entries')
                        ->append( [$data] );

        // Flash message to user
        Session::flash('success', 'You have successfully registered for Board\'s Stoichiometry Mania! You may start answering questions now ;)');

        if ( !is_null( auth()->user()->exam_board_id) ) {
            $exam_board = auth()->user()->exam_board->slug;
            return redirect()->route('questions.index', $exam_board);
        }
        return redirect('/');
    }
}
