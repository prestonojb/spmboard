<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TeacherInvoiceItem extends Model
{
    protected $guarded = [];

    public function lesson()
    {
        return $this->belongsTo(Lesson::class, 'lesson_id', 'id');
    }

    public function invoice()
    {
        return $this->belongsTo(TeacherInvoice::class, 'invoice_id', 'id');
    }

    public function getAmountAttribute()
    {
        return number_format($this->price_per_unit * $this->no_of_units, 2);
    }
}
