@extends('forum.layouts.app')

@section('meta')
    <title>Getting Started as a Parent | {{ config('app.name') }}</title>
@endsection

@section('content')
@include('main.navs.header')

<div class="py-4"></div>

<div class="wrapper">
    <div class="text-center">
        <h1 class="heading">Getting Started as a Parent</h1>
        <h2 class="sub-heading mb-3">Tell us more about yourself so we can cater your experience with us!</h2>
    </div>

    @component('parent.forms.profile', ['parent' => auth()->user()->parent, 'getting_started' => true])
    @endcomponent
</div>

<div class="py-4"></div>

@endsection

@section('scripts')
@endsection
