@extends('forum.layouts.app')

@section('meta')
<title>503 - System is currently under maintenance | {{ config('app.name') }}</title>
@endsection

@section('content')
<div class="container">
    <div class="error-page-container">
        <img src="{{ asset('img/essential/error_503.svg') }}" alt="error 503" class="main-img mb-4">
        <h3 class="font-size7 font-weight500 mb-4">We've got something special for you on Board.</h3>
            <p class="font-size4 gray2 mb-1">And we can't wait for you to see it.</p>
            <p class="font-size4 gray2"> Please check back soon.</p>
    </div>
</div>
@endsection
