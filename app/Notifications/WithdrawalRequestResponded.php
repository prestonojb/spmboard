<?php

namespace App\Notifications;

use App\WithdrawalRequest;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class WithdrawalRequestResponded extends Notification implements ShouldQueue
{
    use Queueable;

    public $withdrawal_request;
    public $title;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(WithdrawalRequest $withdrawal_request)
    {
        $this->withdrawal_request = $withdrawal_request;
        if($withdrawal_request->isCompleted()) {
            $this->title = "Your withdrawal request totalling {$withdrawal_request->withdrawal_amount_in_coin} coins is completed.";
        } else {
            $this->title = "Your withdrawal request totalling {$withdrawal_request->withdrawal_amount_in_coin} coins is cancelled. Learn why here.";
        }
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $request = $this->withdrawal_request;
        $title = $this->title;
        return (new MailMessage)
                    ->greeting("Withdrawal Request")
                    ->line($title)
                    ->action('Learn More', route('coins.show'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $request = $this->withdrawal_request;
        $title = $this->title;
        return [
            'type' => WithdrawalRequest::class,
            'target' => $this->withdrawal_request,
            'title' => $title,
        ];
    }
}
