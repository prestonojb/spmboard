@extends('classroom.layouts.app')

@section('meta')
<title>Parent Settings | {{ config('app.name') }}</title>
@endsection

@section('styles')
@endsection

@section('content')


@component('classroom.common.breadcrumbs')
    <a href="{{ route('classroom.landing') }}">Home</a> /
    <a href="{{ route('classroom.settings') }}">Settings</a> /
    Invoice Setting
@endcomponent

@component('classroom.common.page_heading', ['title' => 'Invoice Setting'])
@endcomponent

<div class="maxw-460px mx-auto my-5">
    <div class="panel">
        <form action="{{ route('classroom.settings.student.parent.update') }}" method="POST">
            @csrf
            @method('PATCH')

            <div class="form-field">
                <label for="parent_email">Parent's Account</label>
                <input type="email" name="parent_email" id="parent_email" value="{{ $parent ? $parent->email : '' }}">
                @error('parent_email')
                    <span class="error-message">{{ $message }}</span>
                @enderror

                @if( $parent )
                    <span class="green">Linked</span>
                @else
                    <span class="text-danger">Unlinked</span>
                @endif
                <p><small>*Parent's email associated with existing Board account. Parent account can be used to monitor and receive your progress reports in class.</small></p>
            </div>

            <div class="text-center">
                <button type="submit" class="button--primary" id="saveParentAccountButton">Save Parent account</button>
            </div>
        </form>
    </div>
</div>
@endsection

@section('scripts')
<script>
$(function(){
    $('#saveParentAccountButton').click(function(e){
        e.preventDefault();
        $(this).attr('disabled', true);
        $(this).closest('form').submit();
    });
});
</script>
@endsection
