<div class="maxw-740px mx-auto my-5">
    <div class="panel mb-2">
        <div class="mt-2 mb-4">
            <h3 class="">Current Plan</h3>
            @if( $active_price )
                <p class="gray2 mb-1">Board Premium ({{ $active_price->nickname }}) <a href="{{ route('classroom.prices.index') }}">Switch Plan</a></p>
            @else
                <p class="gray2 mb-1">Not subscribed <a href="{{ route('classroom.prices.index') }}">Start a Plan</a></p>
            @endif
        </div>

        <div class="mb-4">
            <h3 class="">Next Billing Date</h3>
            <p class="gray2 mb-1">{{ !is_null($next_billing_date) ? $next_billing_date->toFormattedDateString() : 'N/A' }}</p>
        </div>

        @if( auth()->user()->subscription('premium') )
            @if( auth()->user()->subscription('premium')->onGracePeriod() )
                <form action="{{ route('classroom.subscriptions.resume') }}" method="post">
                    @csrf
                    <button type="submit" class="button--primary--inverse">Resume Subscription</button>
                </form>
            @elseif( auth()->user()->subscription('premium')->onTrial() || auth()->user()->subscription('premium')->recurring() )
                <form action="{{ route('classroom.subscriptions.cancel') }}" method="post">
                    @csrf
                    <button type="submit" class="button--danger deleteButton">Cancel Subscription</button>
                </form>
            @endif
        @endif
    </div>
</div>
