{{-- Save to Library Modal --}}
<div class="modal" tabindex="-1" role="dialog" id="saveToLibraryModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Save to...</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                {{-- Library List --}}
                @foreach(auth()->user()->libraries as $library)
                    @component('libraries.common.list_item', ['library' => $library, 'resource' => $resource])
                    @endcomponent
                @endforeach
                {{-- Add New Library --}}
                <div class="py-2" id="createNewLibraryListItem">
                    <button class="button-link blue4 underline-on-hover" data-toggle="modal" data-target="#createNewLibraryModal">
                        <i class="fas fa-plus"></i>
                        Create a new library</button>
                </div>
            </div>
        </div>
    </div>
</div>

{{-- Create New Library Modal --}}
<div class="modal" tabindex="-1" role="dialog" id="createNewLibraryModal">
    <form action="{{ route('libraries.store') }}" method="POST">
        @csrf
        <input type="hidden" name="resource_id" value="{{ $resource->id }}">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Create a new library</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-field">
                        <label for="">Name</label>
                        <input type="text" name="name" value="{{ old('name') }}" required>
                        @error('name')
                            <p class="error-message">{{ $message }}</p>
                        @enderror
                    </div>
                </div>
                <div class="modal-footer position-relative">
                    <button class="button--primary" id="createNewLibraryButton" style="width: 76px;">Create</button>
                </div>
            </div>
        </div>
    </form>
</div>
