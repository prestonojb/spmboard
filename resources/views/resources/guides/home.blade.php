@extends('resources.layouts.app')
@section('meta')
<title> Guides | {{ $exam_board->name }} {{ config('app.name') }} </title>
@endsection

@section('header')
    @component('resources.common.header', ['type' => 'guide', 'exam_board' => $exam_board])
    @endcomponent
@endsection

@section('breadcrumbs')
    @component('resources.common.breadcrumbs')
    Home
    @endcomponent
@endsection

@section('context_panel')
    @component('resources.common.context_panel', ['exam_board' => $exam_board->name, 'title' => 'Guides', 'description' => 'Revise with Guides and ace your exams!'])
    @endcomponent
@endsection

@section('body')
    <div class="form-row justify-content-center align-items-center resource__subject-list" role="tablist">
        @foreach($subjects as $subject)
            <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                <a class="subject-item mb-3" href="{{ route('guides.subject_show', [$exam_board, $subject->id]) }}" role="tab">
                    <span class="subject-item__icon">{!! $subject->icon !!}</span>
                    <span class="subject-item__title">{{ $subject->name }}</span>
                    <span class="subject-item__subtitle">{{ count($subject->guides) }} @if(count($subject->guides) > 1) guides @else guide @endif</span>
                </a>
            </div>
        @endforeach
    </div>
@endsection
