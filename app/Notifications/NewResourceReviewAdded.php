<?php

namespace App\Notifications;

use App\ResourceReview;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class NewResourceReviewAdded extends Notification implements ShouldQueue
{
    use Queueable;

    public $resource_review;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(ResourceReview $resource_review)
    {
        $this->resource_review = $resource_review;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    // /**
    //  * Get the mail representation of the notification.
    //  *
    //  * @param  mixed  $notifiable
    //  * @return \Illuminate\Notifications\Messages\MailMessage
    //  */
    // public function toMail($notifiable)
    // {
    //     return (new MailMessage)
    //                 ->line('The introduction to the notification.')
    //                 ->action('Notification Action', url('/'))
    //                 ->line('Thank you for using our application!');
    // }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $resource = $this->resource_review->resource;
        $resource_review = $this->resource_review;
        return [
            'type' => ResourceReview::class,
            'target' => $this->resource_review,
            'title' => "<b>{$resource_review->user->username}</b> left you a review on {$resource->primary} ({$resource->type}).",
        ];
    }
}
