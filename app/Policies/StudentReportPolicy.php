<?php

namespace App\Policies;

use App\StudentReport;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class StudentReportPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index(User $user)
    {
        return $user->teacher || $user->parent;
    }

    public function store(User $user)
    {
        return $user->teacher && $user->teacher->hasPremiumSubscription();
    }

    public function show(User $user, StudentReport $report)
    {
        if( $user->teacher ) {
            return $report->teacher_id == $user->id;
        } elseif( $parent = $user->parent ) {
            return in_array($report->student_id, $parent->children_ids);
        }
        return false;
    }

    public function download_pdf(User $user, StudentReport $report)
    {
        if( $user->teacher ) {
            return $report->teacher_id == $user->id && $user->teacher->hasPremiumSubscription();
        } elseif( $parent = $user->parent ) {
            return in_array($report->student_id, $parent->children_ids);
        }
        return false;
    }

    public function delete(User $user, StudentReport $report)
    {
        return $user->teacher && $user->id == $report->teacher_id;
    }
}
