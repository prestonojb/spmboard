<ul class="nav nav-tabs nav-stacked mb-3">
    <li class="nav-pills {{ request()->is('users/*') && !request()->is('users/*/*') ? 'active' : '' }}">
        <a href="{{ route('users.show', $user->username) }}" class="">Answers <span class="font-size1">({{ count($user->answers ?? []) }})</span></a>
    </li>
    <li class="nav-pills {{ request()->is('users/*/questions') ? 'active' : '' }}">
        <a href="{{ route('users.questions', $user->username) }}" class="">Questions <span class="font-size1">({{ count($user->questions ?? []) }})</span></a>
    </li>
    @if($user->is_teacher())
        <li class="nav-pills {{ request()->is('users/*/articles') ? 'active' : '' }}">
            <a href="{{ route('users.articles', $user->username) }}" class="">Articles <span class="font-size1">({{ count($user->articles ?? []) }})</span></a>
        </li>
        <li class="nav-pills {{ request()->is('users/*/notes') ? 'active' : '' }}">
            <a href="{{ route('users.notes', $user->username) }}" class="">Notes <span class="font-size1">({{ count($user->owned_notes ?? []) }})</span></a>
        </li>
    @endif
</ul>
