@php
if($is_edit) {
    $name = old('name') ?? $note->name;
    $description = old('description') ?? $note->description;
    $coin_price = old('coin_price') ?? $note->coin_price;
    $type = old('type') ?? $note->type;
    $subject_id = old('subject') ?? $note->subject_id;
    $chapter_id = old('chapter') ?? $note->chapter_id;
    $url = route('notes.update', [$exam_board->slug, $note->id]);
} else {
    $name = old('name');
    $description = old('description');
    $coin_price = old('coin_price');
    $type = old('type');
    $subject_id = old('subject');
    $chapter_id = old('chapter');
    $url = route('notes.store', $exam_board->slug);
}
@endphp

<form method="POST" action="{{ $url }}" enctype="multipart/form-data">
    @csrf
    @if($is_edit)
        @method('PATCH')
    @endif
    {{-- Name --}}
    <div class="form-group">
        <label for="name">Name<span class="red">*</span></label>
        <input type="text" name="name" value="{{ $name }}" placeholder="Name" required>
        @error('name')
            <div class="error-message">{{ $message }}</div>
        @enderror
    </div>

    {{-- Description --}}
    <div class="form-group">
        <label for="name">Description</label>
        <div class="loading-icon loading in-editor blue"></div>
        <textarea class="resource-tinymce-editor" name="description">{!! $description !!}</textarea>
        @error('description')
            <div class="error-message">{{ $message }}</div>
        @enderror
    </div>

    {{-- Subject/Chapters --}}
    <div class="form-field">
        <label>Subject<span class="red">*</span></label>
        <select class="subject-select" name="subject">
            <option data-placeholder="true"></option>
            @foreach($exam_board->subjects as $subject)
                <option value="{{ $subject->id }}" {{ $subject_id == $subject->id ? 'selected="selected"' : '' }}>{{ $subject->name }}</option>
            @endforeach
        </select>
        @error('subject')
            <span class="error-message">{{ $message }}</span>
        @enderror
    </div>

    {{-- Chapter --}}
    <div class="form-field">
        <label>Chapter</label>
        <select class="chapter-select" name="chapter" disabled>
            <option data-placeholder="true"></option>
            @foreach($exam_board->subjects as $subject)
                <optgroup label="{{ $subject->name }}">
                    @foreach($subject->chapters as $chapter)
                        <option value="{{ $chapter->id }}"
                            {{ $chapter->id == $chapter_id ? 'selected="selected"' : '' }}>
                            {{ $chapter->name }}
                        </option>
                    @endforeach
                </optgroup>
            @endforeach
        </select>
        @error('chapter')
            <span class="error-message">{{ $message }}</span>
        @enderror
    </div>

    {{-- Type --}}
    <div class="form-group">
        <label for="type">Type<span class="red">*</span></label>
        <select name="type" id="type">
            <option value="" disabled selected>Select Type</option>
            @foreach(App\Notes::getAllTypes() as $key=>$value)
                <option value="{{ $key }}" @if($type == $key) selected @endif>{{ $value }}</option>
            @endforeach
        </select>
        @error('type')
            <div class="error-message">{{ $message }}</div>
        @enderror
    </div>

    {{-- File --}}
    @if($is_edit)
        <div class="form-field">
            <label for="">File<span class="red">*</span></label>
            <a class="underline-on-hover" href="{{ $note->url }}" target="_blank">View</a>
        </div>
    @else
        <div class="form-field">
            <label>File</label>
            <input name="file" type="file" accept=".pdf">
            <p class="mb-0 font-size1 gray2"><i>Accepted file format: .pdf only</i></p>

            @error('file')
                <span class="error-message">{{ $message }}</span>
            @enderror
        </div>
    @endif


    @if(!$is_edit)
        {{-- Coin Price --}}
        <div class="form-group">
            <label for="name">Coin Price</label>

            {{-- Coin Price Radio Buttons --}}
            <div class="d-flex justify-content-start align-items-center mb-2">
                {{-- Free --}}
                <div class="styled-radio-input mr-2">
                    <input value="free" name="coin_price_toggle" type="radio" id="freeCoinPriceRadio" checked>
                    <label for="freeCoinPriceRadio" class="">Free</label>
                </div>

                {{-- Paid --}}
                <div class="styled-radio-input">
                    <input value="paid" name="coin_price_toggle" type="radio" id="paidCoinPriceRadio">
                    <label for="paidCoinPriceRadio" class="">Paid</label>
                </div>
            </div>

            {{-- Coin Price Select --}}
            <select name="coin_price" id="" disabled>
                <option value="0" disabled selected>Coin Price</option>
                @foreach(range(5, 100, 5) as $i)
                    <option value="{{ $i }}">{{ $i }} coins (USD {{ convertUsdToCoinSell($i) }})</option>
                @endforeach
            </select>

            <p class="mt-1 mb-0 font-size1 gray2">
                If resource is set to paid, user will have to unlock your resource with coins (first 2 pages will be used as preview), with all coin proceeds going directly your account.
            </p>

            @error('coin_price')
                <div class="error-message">{{ $message }}</div>
            @enderror
        </div>
    @else
        <div class="form-group">
            <label for="">Coin Price</label>
            <p class="mb-0 blue4">{{ $note->coin_price }} coins (USD {{ convertUsdToCoinSell($note->coin_price) }})</p>
        </div>
    @endif

    {{-- Confirm TnC --}}
    @if(!$is_edit)
        <div class="form-field">
            <input class="styled-checkbox" type="checkbox" name="confirm_tnc" id="confirm_tnc" required>
            <label for="confirm_tnc">I confirm I own or have permission to use all content in this resource and understand I may be liable for any copyright infringement. I verify that I have read and agree to the Board Author Code as well as to the general and additional terms.</label>
            @error('confirm_tnc')
                <div class="error-message">{{ $message }}</div>
            @enderror
        </div>
    @endif

    <div class="form-field">
        <p class="gray2 font-size3"><span class="red">*</span>Required fields</p>
    </div>

    <div class="text-center">
        <button type="submit" class="button--primary w-250px loadOnSubmitButton">{{ $is_edit ? 'Update' : 'Upload' }}</button>
    </div>
</form>
