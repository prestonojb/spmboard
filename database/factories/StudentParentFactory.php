<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\StudentParent;
use App\User;
use Faker\Generator as Faker;

$factory->define(StudentParent::class, function (Faker $faker) {
    return [
        'user_id' => function() {
            return factory(User::class)->create()->id;
        },
        'phone_number' => $faker->phoneNumber,
    ];
});
