<?php

namespace App\Http\Controllers;

use App\Answer;
use App\AnswerUpvote;
use Illuminate\Http\Request;
use Amplitude;

class AnswerUpvoteController extends Controller
{
    public function toggleUpvote(Answer $answer)
    {
        $user = auth()->user();
        $upvoted = AnswerUpvote::where('user_id', $user->id)->where('answer_id', $answer->id)->exists();
        if(!$upvoted){
            $this->store($answer);

            // Log event on Amplitude
            Amplitude::setUserId($user->id);
            Amplitude::queueEvent('upvote_answer', ['question_id' => $answer->question->id, 'answer_id' => $answer->id]);

        } else{
            $this->destroy($answer);
        }
    }

     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Answer $answer)
    {
        AnswerUpvote::create([
            'user_id' => auth()->user()->id,
            'answer_id' => $answer->id
        ]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Answer $answer)
    {
        AnswerUpvote::where('user_id', auth()->user()->id)->where('answer_id', $answer->id)->delete();
    }
}
