@extends('resources.layouts.app')
@section('meta')
    <title>{{ $subject->name }} - Guides | {{ $exam_board->name }} {{ config('app.name') }} </title>
@endsection

@section('header')
    @component('resources.common.header', ['type' => 'guide', 'exam_board' => $exam_board])
    @endcomponent
@endsection

@section('breadcrumbs')
    @component('resources.common.breadcrumbs')
        <a href="{{ route('guides.home', $exam_board->slug) }}">Home</a> /
        {{ $subject->name }}
    @endcomponent
@endsection

@section('context_panel')
    @component('resources.common.context_panel', ['exam_board' => $exam_board->name, 'title' => $subject->name, 'description' => $subject->description])
        <h3 class="font-size3">
            Part of <a href="{{ route('guides.home', [$subject->exam_board->slug]) }}" class="font-size-inherit blue-underline">{{ $subject->exam_board->name }}</a>
        </h3>
    @endcomponent
@endsection

@if($subject->hasGuides())
    @section('side')
        {{-- @component('resources.guides.common.chapter_pills', ['subject' => $subject])
        @endcomponent --}}

        <div>
            @foreach($subject->chapters()->whereHas('guides')->get() as $chapter)
                <div class="py-1">
                    <a href="#{{ str_slug($chapter->name) }}" class="blue4 underline-on-hover">{{ $chapter->name }}</a>
                </div>
            @endforeach
        </div>
    @endsection

    @section('main')
        {{-- @component('resources.guides.common.chapter_tabs', ['subject' => $subject])
        @endcomponent --}}

        @foreach($subject->chapters()->whereHas('guides')->get() as $chapter)
            <div class="mb-2">
                <h4 id="{{ str_slug($chapter->name) }}" class="resource__section-header">{{ $chapter->name }}</h4>
                <div class="resource__panel-dropdown-container">
                    <div class="form-row">
                        @foreach($chapter->guides as $guide)
                            <div class="col-12 col-sm-6 col-md-4 col-lg-3 mb-2">
                                @component('resources.guides.common.block', ['guide' => $guide])
                                @endcomponent
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="py-2"></div>
        @endforeach
    @endsection
@else
    @section('body')
        <div class="text-center p-5">
            <img src="{{ asset('img/essential/empty.svg') }}" alt="" width="400" height="400" class="d-inline-block mb-2">
            <p class="gray2">No guides available for this subject yet</p>
        </div>
    @endsection
@endif
