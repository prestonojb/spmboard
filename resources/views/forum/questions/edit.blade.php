@extends('forum.layouts.app')

@section('meta')
<title>Edit: {{ $question->question }} | {{ $exam_board->name }} {{ config('app.name') }}</title>

<meta property="og:image" content="{{ asset('img/logo/logo_sm.png') }}" />
<meta property="og:type" content="website" />
@endsection

@section('content')

<div class="py-3"></div>

@include('common.header2')
<div class="maxw-740px mx-auto">
    <div class="mb-2">
        <a class="inactive font-size2 gray2" href="{{ route('questions.show', [$exam_board->slug, $question->id]) }}">< Back to question</a>
    </div>

    <h1 class="font-size7">Edit Question</h1>

    <div class="panel">
        @component('forum.questions.common.form', ['is_edit' => true, 'exam_board' => $exam_board, 'question' => $question, 'subjects' => $subjects])
        @endcomponent
    </div>
</div>

<div class="py-3"></div>
@endsection

@section('js_dependencies')
{{-- TinyMCE --}}
<script src="https://cdn.tiny.cloud/1/si4jvonmpvtlfypotvf9zu2vci2zsjavfd4v1oyvnzncszag/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>    {{-- KaTeX --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ" crossorigin="anonymous"></script>
@endsection
