<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateCoinProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('coin_products', function (Blueprint $table) {
            $table->unsignedInteger('unit_price_in_coin');
            $table->unsignedInteger('unit_discounted_price_in_coin');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('coin_products', function (Blueprint $table) {
            $table->dropColumn('unit_price_in_coin');
            $table->dropColumn('unit_price_in_coin');
        });
    }
}
