<?php

namespace App\Imports;

use App\Chapter;
use App\Notes;
use App\Subject;
use Illuminate\Support\Collection;
use Illuminate\Validation\Rule;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithStartRow;

class NoteImport implements ToCollection, WithHeadingRow, WithValidation, WithStartRow
{
    /**
    * @param Collection $collection
    */
    public function collection(Collection $rows)
    {
        foreach($rows as $row)
        {
            $subject = Subject::find($row->get('subject_id'));
            $chapter = Chapter::find($row->get('chapter_id'));
            $exam_board = $subject->exam_board;

            $filename = $row->get('filename');
            $name = substr($filename, 0, strlen($filename) - 4);

            // Create row in DB
            Notes::create([
                'name' => $name,
                'subject_id' => $row->get('subject_id'),
                'chapter_id' => $row->get('chapter_id'),
                'type' => $row->get('type'),
                'url' => "notes/{$exam_board->slug}/".$row->get('filename'),
            ]);
        }
    }


    public function rules(): array
    {
        return [
            'subject_id' => Rule::in( Subject::all()->pluck('id')->toArray() ),
            'chapter_id' => Rule::in( Chapter::all()->pluck('id')->toArray() ),
            'filename' => 'required|string',
            'type' => Rule::in(['comprehensive', 'one_page']),
        ];
    }

    /**
     * Indicate Heading row
     * 1st row is heading row
     */
    public function headingRow(): int
    {
        return 1;
    }

    /**
     * @return int
     */
    public function startRow(): int
    {
        return 3;
    }
}
