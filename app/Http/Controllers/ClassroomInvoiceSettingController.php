<?php

namespace App\Http\Controllers;

use App\State;
use App\Teacher;
use App\TeacherInvoice;
use App\TeacherInvoiceSetting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ClassroomInvoiceSettingController extends Controller
{
    public function show()
    {
        $user = auth()->user();
        $teacher = $user->teacher;
        $setting = $teacher->invoice_setting;
        $states = State::all();

        return view('classroom.settings.teacher.invoice', compact('setting', 'states'));
    }

    public function update()
    {
        request()->validate([
            'default_payment_due_in_days' => 'nullable',
            'prefix' => 'min:3|max:8',
            'from_address_1' => 'nullable',
            'from_address_2' => 'nullable',
            'postal_code' => 'nullable',
            'city' => 'nullable',
            'state_id' => 'nullable',
            'default_lesson_rate_per_hour' => 'nullable',
            'memo' => 'nullable',
        ]);

        $teacher = auth()->user()->teacher;
        $setting = $teacher->invoice_setting;

        if( $setting ) {

            // Get larger between expected and requested and set as next_invoice_sequence
            $expected_next_invoice_sequence = $teacher->getMaxInvoiceSequenceByPrefix( request()->prefix ) + 1;
            // Get larger between requested and expected
            if( $expected_next_invoice_sequence > request()->next_invoice_sequence ) {
                $next_invoice_sequence = $expected_next_invoice_sequence;
            } else {
                $next_invoice_sequence = request()->next_invoice_sequence;
            }

            $setting->update([
                'default_payment_due_in_days' => request()->default_payment_due_in_days,
                'prefix' => request()->prefix,
                'next_invoice_sequence' => $next_invoice_sequence,
                'from_address_1' => request()->from_address_1,
                'from_address_2' => request()->from_address_2,
                'postal_code' => request()->postal_code,
                'city' => request()->city,
                'state_id' => request()->state,
                'default_lesson_rate_per_hour' => request()->default_lesson_rate_per_hour,
                'memo' => request()->memo,
            ]);

            //Save image in storage
            if(request()->has('logo') && !empty(request()->file('logo')) ){
                // Delete existing logo if any
                if( !is_null($setting->logo_url) ) {
                    Storage::cloud()->delete($setting->logo_url);
                }

                $logo = request()->file('logo');

                //Store image to S3
                $path = $logo->store('invoice_settings/logo/', 's3');
                $filename = $logo->hashName();
                //Save image url in database
                $setting->update([
                    'logo_url' => 'invoice_settings/logo/'.$filename,
                ]);
            } else {
                // Remove logo if remove_file set to true
                if( !is_null(request()->remove_file) && request()->remove_file ) {
                    // Delete existing logo if any
                    if( !is_null($setting->logo_url) ) {
                        Storage::cloud()->delete($setting->logo_url);
                    }
                    $setting->update([
                        'logo_url' => null,
                    ]);
                }
            }

        } else {
            $setting = TeacherInvoiceSetting::create([
                'teacher_id' => $teacher->user_id,
                'default_payment_due_in_days' => request()->default_payment_due_in_days ?? 14,
                'prefix' => request()->prefix ?? 'INV-',
                'from_address_1' => request()->from_address_1,
                'from_address_2' => request()->from_address_2,
                'postal_code' => request()->postal_code,
                'city' => request()->city,
                'state_id' => request()->state,
                'default_lesson_rate_per_hour' => request()->default_lesson_rate_per_hour ?? 50,
                'memo' => request()->memo,
            ]);


            //Save image in storage
            if(request()->has('logo') && !empty(request()->file('logo')) ){
                $logo = request()->file('logo');

                //Store image to S3
                $path = $logo->store('invoice_settings/logo/', 's3');
                $filename = $logo->hashName();
                //Save image url in database
                $setting->update([
                    'logo_url' => 'invoice_settings/logo/'.$filename,
                ]);
            }
        }

        return redirect()->back()->with('success', 'Invoice Settings updated successfully!');
    }
}
