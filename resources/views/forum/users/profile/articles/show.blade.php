@extends('main.layouts.app')

@section('meta')
<title>{{ $article->title }} | {{ config('app.name') }}</title>
<meta name="description" content="{{ $article->description }}">

<meta property="og:image" content="{{ $article->cover_img }}" />
<meta property="og:type" content="article" />
@endsection

@section('content')
<div class="container">
    <div class="maxw-740px mx-auto">

        <div class="py-1"></div>
        {{-- Back Button --}}
        <a href="{{ route('users.articles', $article->user->username) }}" class="d-inline-block gray2 underline-on-hover mb-2">< Back to {{ $article->user->username }} Articles</a>

        <div class="panel">
            <img class="blog__cover-img" src="{{ $article->cover_img }}" alt="" class="cover-img mb-2">

            <div class="blog-content">
                <div class="mb-3">
                    <div class="mt-2">
                        @foreach($article->tags as $tag)
                            <a href="javascript:void" class="blog-tag mb-1">{{ $tag }}</a>
                        @endforeach
                    </div>
                    <h1 class="font-size11"><b>{{ $article->title }}</b></h1>

                    <div class="py-1"></div>

                    {{-- Share Buttons --}}
                    <div class="shareIcons"></div>

                    <div class="py-1"></div>

                    {{-- Edit/Delete Buttons --}}
                    @can('edit_update', $article)
                        <div>
                            <a href="{{ route('users.articles.edit', $article->id) }}" class="d-inline-block underline-on-hover mr-1 mb-2"><span class="iconify" data-icon="bx:bx-edit" data-inline="false"></span> Edit</a>
                            @can('delete', $article)
                                <form action="{{ route('users.articles.delete', $article->id) }}" class="d-inline-block mb-2" method="post">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="red deleteButton p-0 underline-on-hover">
                                        <span class="iconify" data-icon="carbon:trash-can" data-inline="false"></span>
                                        Delete
                                    </button>
                                </form>
                            @endcan
                        </div>
                    @endcan
                </div>

                <p><b>{{ views($article)->count() }} views</b></p>

                {{-- Author --}}
                <div class="d-flex justify-content-start align-items-start border-top border-bottom py-3">
                    <a href="{{ $article->user->show_url }}">
                        <img src="{{ $article->user->avatar }}" class="avatar mr-2" alt="Author Avatar">
                    </a>
                    <div>
                        <h4 class="font-size3 font-weight500 mb-1">
                            <a href="{{ $article->user->show_url }}" class="font-size-inherit color-inherit">
                                {{ $article->user->name }}
                            </a>
                        </h4>
                        <p class="gray2 font-size3 mb-0">
                            {{ $article->updated_at->toFormattedDateString() }}
                        </p>
                    </div>
                </div>

                <div class="py-2"></div>

                {{-- Body --}}
                <article class="tinymce-editor-contents">
                    {!! $article->body !!} <br>
                </article>

                <hr>


                {{-- Published By --}}
                <div>
                    {{-- Share Buttons --}}
                    <div class="shareIcons"></div>

                    <div class="py-1"></div>

                    <p class="gray2 text-uppercase font-size3">Published By</p>

                    <div class="d-flex justify-content-start align-items-start">
                        <a href="{{ $article->user->show_url }}">
                            <img src="{{ $article->user->avatar }}" class="avatar-lg mr-1" alt="Author Avatar">
                        </a>
                        <div>
                            <h4 class="font-size5 font-weight500">
                                <a href="{{ $article->user->show_url }}" class="font-size-inherit color-inherit">
                                    {{ $article->user->name }}
                                </a>
                            </h4>
                            @if(!is_null($article->user->about))
                                <h5 class="font-size3">{{ $article->user->about }}</h5>
                            @endif
                            @if(!is_null($article->user->profile_summary))
                                <p class="font-size3 gray2">{!! nl2br($article->user->profile_summary) !!}</p>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="py-1"></div>

            </div>
        </div>
    </div>

    <div class="py-4"></div>

    @if(count($article->getMoreArticlesByUsers() ?? []))
        <section class="recommended-articles mb-1">
            <h6 class="font-size5 border-bottom pb-2 mb-2"><b>More Articles by {{ $article->user->username }}</b></h6>
            <div class="row">
                @foreach($article->getMoreArticlesByUsers() as $article)
                    <div class="col-12 col-lg-4 mb-2">
                        @component('forum.users.profile.articles.common.block', ['article' => $article])
                        @endcomponent
                    </div>
                @endforeach
            </div>
        </section>
    @endif
</div>

@endsection
