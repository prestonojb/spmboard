<?php

namespace App;

use DivisionByZeroError;
use Exception;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Blade;
use PDF;
use Image;

class ExamBuilderItem extends Model
{
    protected $guarded = [];

    /**
     * ------------------
     * Relationships
     * ------------------
     */
    public function subject()
    {
        return $this->belongsTo(Subject::class);
    }

    public function chapter()
    {
        return $this->belongsTo(Chapter::class);
    }

    public function sheets()
    {
        return $this->belongsToMany(ExamBuilderSheet::class);
    }

    /**
     * ---------------------
     * Accessors
     * ---------------------
     */

    /**
     * Returns true if question and answer are filled
     * @return boolean
     */
    public function getIsCompleteAttribute()
    {
        return !is_null($this->question_imgs) && !is_null($this->answer_imgs);
    }

    /**
     * Return sorted Question Image Urls
     * @return array
     */
    public function getQuestionImgUrlsAttribute()
    {
        $arr = json_decode($this->question_imgs);
        if( !is_null($arr) ) {
            sort($arr);
            return $arr;
        } else {
            return [];
        }
    }

    /**
     * Return sorted Answer Image Urls
     * @return array
     */
    public function getAnswerImgUrlsAttribute()
    {
        $arr = json_decode($this->answer_imgs);
        if( !is_null($arr) ) {
            sort($arr);
            return $arr;
        } else {
            return [];
        }
    }

    /**
     * Return Item Block HTML
     * @return string HTML string for item block
     */
    public function getHtmlAttribute()
    {
        return view('exam_builder.common.item_block', ['item' => $this])->render();
    }

    /**
     * ---------------------
     * Helper Functions
     * ---------------------
     */

     /**
      * Get another item from the same chapter of the item to be replaced
      * @param \App\ExamBuilderItem $item Item to be replaced
      * @param array Item IDs that should not be used to replace inputted item
      * @return \App\ExamBuilderItem Item to replace
      */
    public function replace($item_ids)
    {
        $chapter = Chapter::find( $this->chapter_id );
        // Loop through all chapters until item is found, starting from the chapter of the item to be replaced
        $no_of_chapters = Chapter::where('subject_id', $chapter->subject_id)->count();
        $no_of_chapters_looped = 0;
        // While there are still chapters to be looped
        while( $no_of_chapters > $no_of_chapters_looped ) {
            $item = ExamBuilderItem::where('chapter_id', $chapter->id )->where('id', '!=', $this->id)->whereNotIn('id', $item_ids)->first();
            if( !is_null($item)  ) {
                return $item;
            } else {
                $chapter = $chapter->next( true );
                $no_of_chapters_looped += 1;
            }
        }
    }

    /**
     * Add image to Exam Builder Item
     * @param Illuminate\Http\UploadedFile
     * @param string $path
     * @param boolean $is_answer
     */
    public function addImage( $file, $path, $is_answer )
    {
        $file_name = $file->getClientOriginalName();
        if( $is_answer ) {
            $this->appendAnswerImage( $file_name, $path );
        } else {
            $this->appendQuestionImage( $file_name, $path );
        }
        self::uploadImage($file, $path);
    }

    /**
     * Append filename to question_imgs column
     * @param string $file_name
     * @param string $path
     */
    public function appendQuestionImage($file_name, $path)
    {
        $arr = json_decode( $this->question_imgs ) ?? [];
        if( !in_array( $path.$file_name, $arr ) ) {
            array_push( $arr, $path.$file_name );
        }
        $this->update([
            'question_imgs' => json_encode( $arr )
        ]);
    }

    /**
     * Append filename to answer_imgs column
     * @param string $file_name
     * @param string $path
     */
    public function appendAnswerImage($file_name, $path)
    {
        $arr = json_decode( $this->answer_imgs ) ?? [];
        if( !in_array( $path.$file_name, $arr ) ) {
            array_push( $arr, $path.$file_name );
        }
        $this->update([
            'answer_imgs' => json_encode( $arr )
        ]);
    }

    /**
     * Store image on S3
     * Resizing Image Reference: https://laracasts.com/discuss/channels/laravel/saving-an-intervention-image-instance-into-amazon-s3?page=1
     * @param string $file_name
     * @param string $path
     */
    public static function uploadImage($file, $path)
    {
        // File overriding fails to update file
        $filename = $file->getClientOriginalName();

        // Delete file if exists
        if( Storage::cloud()->exists( $path.$filename ) ) {
            Storage::cloud()->delete( $path.$filename );
        }
        $file->storeAs($path, $filename, 's3');
    }

    /**
     * Delete all images on S3
     * @param string $file_name
     * @param string $path
     */
    public function deleteAllImages()
    {
        $this->deleteAllQuestionImages();
        $this->deleteAllAnswerImages();
    }

    /**
     *
     * Delete all question images on S3
     */
    public function deleteAllQuestionImages()
    {
        $question_arr = json_decode( $this->question_imgs ) ?? [];
        if( count($question_arr ?? []) ) {
            foreach( $question_arr as $file_path ) {
                Storage::cloud()->delete( $file_path );
            }
        }
        $this->update(['question_imgs' => null]);
    }

    /**
     *
     * Delete all answer images on S3
     */
    public function deleteAllAnswerImages()
    {
        $answer_arr = json_decode( $this->answer_imgs ) ?? [];
        if( count($answer_arr ?? []) ) {
            foreach( $answer_arr as $file_path ) {
                Storage::cloud()->delete( $file_path );
            }
        }
        $this->update(['answer_imgs' => null]);
    }


    /**
     * Returns random items by chapters and paper number
     * @param int $total
     * @param \Illuminate\Support\Collection $chapters
     * @return \Illuminate\Support\Collection
     */
    public static function getRandomItems($total, $chapters, $paper_number)
    {
        $items = collect();
        // Get number of items to be generated per chapter
        $no_of_items_by_chapter = ExamBuilderItem::getNoOfItems( $total, $chapters, $paper_number );
        // Get random items by chapter
        foreach( $chapters as $chapter ) {
            $total = self::totalByChapter($chapter, $paper_number);
            $no_of_items = $no_of_items_by_chapter[$chapter->id];

            // Add chapter items to items if total items for chapter exceeds required number of items
            if( $total >= $no_of_items ) {
                $chapter_items = self::random($chapter, $paper_number, $no_of_items);
                $items->push( $chapter_items );
            } else {
                throw new Exception("Requested items exceed total number of items!");
            }
        }
        return $items->flatten();
    }

    /**
     * Returns the number of items to be generated per chapter based on the proportion of items by chapter in the given subject, filtered by paper number
     * @param int $total
     * @param \Illuminate\Support\Collection $chapters
     * @return array [1 => 2, 2 => 3, 3 => 1, ...]
     */
    public static function getNoOfItems( $total, $chapters, $paper_number = null )
    {
        $arr = array();
        foreach( $chapters as $chapter ) {
            $no_of_items = self::getNoOfItemsByChapterWeightage($total, $chapter, $paper_number);
            $arr[$chapter->id] = $no_of_items;
        }
        return self::rescaleNoOfItemsByChapter($arr, $total, $paper_number);
    }

    /**
     * Rescales the inputted array to ensure the total number of items is equal to the inputted total
     * @param array $arr
     * @param int $total
     * @return array
     */
    public static function rescaleNoOfItemsByChapter( $arr, $total, $paper_number = null )
    {
        // Scale number of items to ensure TOTAL number of items is as inputted
        $total_no_of_items = array_sum( array_values($arr) );
        // Arrays to ensure that while loop does not run infinitely
        // Incremental => can add 1 item from this chapter
        // Decremental => can remove 1 item from this chapter
        $incrementable_chapter_ids = array_keys($arr);
        $decrementable_chapter_ids = array_keys($arr);

        // Get random chapter ID
        $chapter_id = array_rand($arr);
        $chapter = Chapter::find($chapter_id);
        // Stop while loop when no of items is greater than required total
        while( $total_no_of_items != $total ) {
            if( $total_no_of_items < $total ) {
                // Get random chapter ID from incrementable chapters
                $chapter_id = $incrementable_chapter_ids[ array_rand( $incrementable_chapter_ids ) ];
                $chapter = Chapter::find($chapter_id);
                // Get number of items of chapter from array
                $no_of_items = $arr[$chapter_id];
                // Decrease random no of items of chapter by one if total items is greater than required total
                if( self::canRescaleNoOfChapterItems( $no_of_items + 1, $chapter, $paper_number ) ) {
                    $arr[$chapter_id] += 1;
                    $total_no_of_items += 1;
                } else {
                    // Remove chapter from incrementable chapters list
                    $key = array_search($chapter_id, $incrementable_chapter_ids);
                    unset($incrementable_chapter_ids[$key]);

                    // Throw Exception if it is not possible to reach the required total
                    if( count($incrementable_chapter_ids) < 1 ) {
                        throw new Exception("Not enough exam builder items!");
                    }
                }
            } else {
                // Increase random no of items of chapter by one if total items is less than required total
                if( self::canRescaleNoOfChapterItems( $arr[$chapter_id] - 1, $chapter, $paper_number ) ) {
                    // Get random chapter ID from decrementable chapters
                    $chapter_id = $decrementable_chapter_ids[ array_rand( $decrementable_chapter_ids ) ];
                    $chapter = Chapter::find($chapter_id);
                    // Get number of items of chapter from array
                    $no_of_items = $arr[$chapter_id];
                    // Increase random no of items of chapter by one
                    $arr[$chapter_id] -= 1;
                    $total_no_of_items -= 1;
                } else {
                    // Remove chapter from incrementable chapters list
                    $key = array_search($chapter_id, $decrementable_chapter_ids);
                    unset($decrementable_chapter_ids[$key]);

                    // Throw Exception if it is not possible to reach the required total
                    if( count($decrementable_chapter_ids) < 1 ) {
                        throw new Exception("Not enough exam builder items!");
                    }
                }
            }
        }
        return $arr;
    }

    /**
     * Randomize number of chapters, filtered by paper number
     * @param int $total
     * @param \App\Chapter $chapter
     * @param int $paper_number
     */
    public static function getNoOfItemsByChapterWeightage($total, $chapter, $paper_number = null)
    {
        // Get percentage of items of chapter
        $percentage = self::getChapterPercentageBySubject($chapter, $paper_number);
        $no_of_items = floor($percentage * $total);

        return self::randomizeNoOfItemsOfChapters($no_of_items, $chapter, $paper_number, 1);
    }

    /**
     * Randomize the number of items by +-(spread), minimum value is 0
     * @param int $no_of_items
     * @param \App\Chapter $chapter
     * @param int $paper_number
     * @param int $spread
     * @return int
     */
    public static function randomizeNoOfItemsOfChapters($no_of_items, $chapter, $paper_number, $spread = 1)
    {
        $rand = rand(-($spread), $spread);
        if( self::canRescaleNoOfChapterItems( $no_of_items + $rand, $chapter, $paper_number ) ) {
            $no_of_items += $rand;
        }
        return $no_of_items;
    }

    /**
     * Returns a random item from chapter (with non-empty question and answer images), filtered by paper number
     * @param \App\Chapter $chapter
     * @param int $paper_number
     * @return \App\ExamBuilderItem|\Illuminate\Support\Collection
     */
    public static function random( Chapter $chapter, $paper_number = null, $take = 1 )
    {
        // Get item with question and answer from chapter
        // If chapter is a parent chapter, get items from own + subchapters
        if($chapter->isParentChapter()) {
            $item = self::whereIn('chapter_id', $chapter->getRelatedChapterIds());
        } else {
            $item = self::where('chapter_id', $chapter->id);
        }

        $item = $item->whereNotNull('question_imgs')->whereNotNull('answer_imgs');

        if( !is_null($paper_number) ) {
            $item = $item->where('paper_number', $paper_number);
        }

        $item = $item->inRandomOrder();

        return $take == 1 ? $item->first()
                          : $item->take($take)->get();
    }

    /**
     * Returns percentage of (no. of chapter items)/(no. of subject items)
     * @param \App\Chapter $chapter
     * @param int $paper_number
     * @return float
     */
    public static function getChapterPercentageBySubject( Chapter $chapter, $paper_number = null )
    {
        try {
            return self::totalByChapter($chapter, $paper_number) / self::totalBySubject($chapter->subject, $paper_number);
        } catch(Exception $e) {
            // Handle division by zero
            return 0;
        }
    }

    /**
     * Returns total number of items from subject, filtered by paper number(optional)
     * @param \App\Subject $subject
     * @param int $paper_number
     * @return int
     */
    public static function totalBySubject( Subject $subject, $paper_number = null )
    {
        $items = self::where('subject_id', $subject->id);
        if( !is_null($paper_number) ) {
            $items = $items->where('paper_number', $paper_number);
        }

        return $items->count();
    }

    /**
     * Returns total number of items from chapter, filtered by paper number(optional)
     * @param \App\Chapter $chapter
     * @param int $paper_number
     * @return int
     */
    public static function totalByChapter( Chapter $chapter, $paper_number = null )
    {
        // If chapter is a parent chapter, get items from own + subchapters
        if($chapter->isParentChapter()) {
            $items = self::whereIn('chapter_id', $chapter->getRelatedChapterIds());
        } else {
            $items = self::where('chapter_id', $chapter->id);
        }

        if( !is_null($paper_number) ) {
            $items = $items->where('paper_number', $paper_number);
        }

        return $items->count();
    }


    /**
     * Returns true if new number of chapter items is valid
     * @param int $new New number of chapter items
     * @param \App\Chapter $chapter
     * @param $paper_number
     * @return boolean
     */
    public static function canRescaleNoOfChapterItems( $new, $chapter, $paper_number = null )
    {
        return $new >= 0 && $new <= self::totalByChapter($chapter, $paper_number);
    }
}
