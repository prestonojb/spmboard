var customFile = [];
var $customFileInput = $('[name="custom_file"]');
var $fileBlockCol = $('.fileBlockCol').first();
var $customFileForm = $('#customFileForm');

$('#customFileUploadButton').click(function(){
    $customFileInput.click();
});

// Update custom files
$customFileInput.on('change', function(e) {
    var file = $(this)[0].files[0];

    var fileNameWithType = file.name;

    // To prevent '.' in filename (i.e. 1.1 Length and Time.pdf), find last '.' in filename, then split the string into NAME and EXT
    var fileExtStartIdx = fileNameWithType.lastIndexOf('.');
    var fileName = fileNameWithType.substring(0, fileExtStartIdx);
    var fileExt = fileNameWithType.substring(fileExtStartIdx + 1, fileNameWithType.length);

    // EXTENSION TO UPPERCASE
    fileExt = fileExt.toUpperCase();

    var allowedFileExt = ['PDF'];

    // Reject files more than 10 MB
    if( file.size > 10000000 ) {
        alert('Only files no more than 10MB can be uploaded.');
    } else {
    //Check file format
        if(!allowedFileExt.includes( fileExt )){
            if(fileExt !== ''){
                $(this).val('');
                alert('Only .pdf files can be uploaded!');
            }
        } else{
            var formData = new FormData( $customFileForm[0] );

            $.ajax({
                type: "POST",
                url: customFileUploadUrl,
                data: formData,
                success: function ( file_url ) {
                    // Add file block to DOM
                    var $newFileBlockCol = $fileBlockCol.clone();
                    $newFileBlockCol.find('.file-title').text( fileName );
                    $newFileBlockCol.find('.file-type').text( 'Custom' );

                    $newFileBlockCol.find('.file-block').attr('data-custom', true);
                    $newFileBlockCol.find('.file-block').attr('data-file-url', file_url);
                    $newFileBlockCol.find('a').attr('href', storageUrl + file_url);

                    $newFileBlockCol.appendTo( '.file-block-container .row' );
                },
                cache: false,
                contentType: false,
                processData: false
            });
        }
    }
});

// $('.prebuilt-resources-modal .file-block').click(function(){
//     $(this).toggleClass('selected');
// });

$('.past-paper-list-item').click(function(){
    $(this).find('.collapse-arrow').toggleClass('fa-angle-down fa-angle-up');
});

$('.addResourceButton').click(function(e){
    e.preventDefault();
    var existingSelectedResources = [];
    // Get all existing resource IDs
    $('.fileBlockCol .file-block').each(function(){
        var existingResource = {};
        existingResource.id = $(this).data('id');
        existingResource.type = $(this).find('.file-type').text();;
        existingSelectedResources.push( existingResource );
    });
    // On resources add, add selected file blocks to UI, which will be used later to transfer to backend for storage
    $(this).closest('.modal').find('.resource-list-item .checkbox-input:checked').each(function(){
        // Construct selected resource object
        var resourceId = $(this).closest('.resource-list-item').data('resource-id');
        var resourceType = $(this).closest('.resource-list-item').data('resource-type');

        var resource = {};
        resource.id = resourceId;
        resource.type = resourceType;

        // JS some() function reference: https://www.tutorialrepublic.com/faq/how-to-check-if-an-array-includes-an-object-in-javascript.php
        // Resource not already selected
        if(  !existingSelectedResources.some(existingSelectedResource => (existingSelectedResource.id == resource.id && existingSelectedResource.type == resource.type) ) ) {
            var resourceName = $(this).closest('.resource-list-item').data('resource-name');
            var type = $(this).closest('.resource-list-item').data('type');
            var url = $(this).closest('.resource-list-item').find('a').attr('href');

            // Add file block to DOM
            var $newFileBlockCol = $fileBlockCol.clone();

            $newFileBlockCol.find('.file-title').text( resourceName );
            $newFileBlockCol.find('.file-type').text( resourceType );
            $newFileBlockCol.find('.file-block').attr( 'data-id', resourceId );
            // Type = Question or Answer
            $newFileBlockCol.find('.file-block').attr( 'data-type', type );
            $newFileBlockCol.find('a').attr('href', url);

            var $filePreview = $newFileBlockCol.find('.file-preview');
            switch( resourceType ) {
                case 'Note':
                    $filePreview.html( '<span class="iconify" data-icon="mdi-light:note-text" data-inline="false"></span>' );
                    break;
                case 'Past Paper (Question)':
                    $filePreview.html( '<span class="iconify" data-icon="carbon:stacked-scrolling-1" data-inline="false"></span>' );
                    break;
                case 'Past Paper (Answer)':
                    $filePreview.html( '<span class="iconify" data-icon="carbon:stacked-scrolling-1" data-inline="false"></span>' );
                    break;
                case 'Topical Paper (Question)':
                    $filePreview.html( '<span class="iconify" data-icon="carbon:category-new-each" data-inline="false"></span>' );
                    break;
                case 'Topical Paper (Answer)':
                    $filePreview.html( '<span class="iconify" data-icon="carbon:category-new-each" data-inline="false"></span>' );
                    break;
                default:
                    $filePreview.html( '<span class="iconify" data-icon="ant-design:file-outlined" data-inline="false"></span>' );
                    break;
            }

            $newFileBlockCol.appendTo( '.file-block-container .row' );
        }
    });
    $(this).closest('.modal').modal('hide');
});

// Remove resources(prebuilt/custom) on cross button
$(document).on('click', '.file-cross-button', function(){
    // File Block to be deleted
    var $fileBlock = $(this).closest('.file-block');

    // Delete custom file from custom_files input
    if( $fileBlock.attr('data-custom') == 'true' ) {
        var files = $customFileInput[0].files;

        for (var i = 0; i < files.length; i++) {
            var file = files[i];
            if( file.lastModified == $fileBlock.attr('last-modified') ) {}
        }
    }
    $(this).parents('.fileBlockCol').remove();
});

// Convert resources(prebuilt/custom) to readable data to backend for storage
$('#postResourcesForm [type=submit], #postHomeworkForm [type=submit]').click(function(e){
    e.preventDefault();
    var resourceData = [];
    var customResourceData = [];

    $('.file-block-container .file-block').each(function(){
        if( $(this).attr('data-custom') == 'true' ) {
            // Custom Resource
            var custom_resource = {};
            custom_resource.file_url = $(this).attr('data-file-url');
            custom_resource.file_name = $(this).find('.file-title').text();
            customResourceData.push( custom_resource );
        } else {
            // Pre-built Resources
            var resource_id = $(this).attr('data-id');

            if( $(this).attr('data-type') ) {
                var type = $(this).attr('data-type');
            } else {
                var type = null;
            }

            var resource = {};

            resource.resource_id = parseInt(resource_id);
            resource.type = type;

            resourceData.push( resource );
        }
    });

    $( '[name=resource_data]' ).val( JSON.stringify(resourceData) );
    $( '[name=custom_resource_data]' ).val( JSON.stringify(customResourceData) );

    var val = $( '[name=resource_data]' ).val();

    $(this).closest('form').submit();
});
