@php
if($is_edit) {
    $name = old('name') ?? $topical_past_paper->name;
    $description = old('description') ?? $topical_past_paper->description;
    $coin_price = old('coin_price') ?? $topical_past_paper->coin_price;
    $subject_id = old('subject') ?? $topical_past_paper->subject_id;
    $chapter_id = old('chapter') ?? $topical_past_paper->chapter_id;
    $url = route('topical_past_papers.update', [$exam_board->slug, $topical_past_paper->id]);
} else {
    $name = old('name');
    $description = old('description');
    $coin_price = old('coin_price');
    $type = old('type');
    $subject_id = old('subject');
    $chapter_id = old('chapter');
    $url = route('topical_past_papers.store', $exam_board->slug);
}
@endphp

<form method="POST" action="{{ $url }}" enctype="multipart/form-data">
    @csrf
    @if($is_edit)
        @method('PATCH')
    @endif
    {{-- Name --}}
    <div class="form-group">
        <label for="name">Name<span class="red">*</span></label>
        <input type="text" name="name" value="{{ $name }}" placeholder="Name" required>
        @error('name')
            <div class="error-message">{{ $message }}</div>
        @enderror
    </div>

    {{-- Description --}}
    <div class="form-group">
        <label for="name">Description</label>
        <div class="loading-icon loading in-editor blue"></div>
        <textarea class="resource-tinymce-editor" name="description">{!! $description !!}</textarea>
        @error('description')
            <div class="error-message">{{ $message }}</div>
        @enderror
    </div>

    {{-- Subject/Chapters --}}
    <div class="form-field">
        <label>Subject<span class="red">*</span></label>
        <select class="subject-select" name="subject">
            <option data-placeholder="true"></option>
            @foreach($exam_board->subjects as $subject)
                <option value="{{ $subject->id }}" {{ $subject_id == $subject->id ? 'selected="selected"' : '' }}>{{ $subject->name }}</option>
            @endforeach
        </select>
        @error('subject')
            <span class="error-message">{{ $message }}</span>
        @enderror
    </div>

    {{-- Chapter --}}
    <div class="form-field">
        <label>Chapter</label>
        <select class="chapter-select" name="chapter" disabled>
            <option></option>
            @foreach($exam_board->subjects as $subject)
                <optgroup label="{{ $subject->name }}">
                    @foreach($subject->chapters as $chapter)
                        <option value="{{ $chapter->id }}"
                            {{ $chapter->id == $chapter_id ? 'selected="selected"' : '' }}>
                            {{ $chapter->name }}
                        </option>
                    @endforeach
                </optgroup>
            @endforeach
        </select>
        @error('chapter')
            <span class="error-message">{{ $message }}</span>
        @enderror
    </div>

    {{-- File --}}
    @if($is_edit)
        <div class="form-field">
            <label for="">Question Paper File<span class="red">*</span></label>
            <a href="{{ $topical_past_paper->qp_url }}" target="_blank">View</a>
        </div>

        <div class="form-field">
            <label for="">Mark Scheme File</label>
            <a href="{{ $topical_past_paper->ms_url }}" target="_blank">View</a>
        </div>
    @else
        {{-- QP File --}}
        <div class="form-field">
            <label>Question Paper File<span class="red">*</span></label>
            <input name="qp_file" type="file" accept=".pdf">
            <p class="mb-0 font-size1 gray2"><i>Accepted file format: .pdf only</i></p>
            @error('qp_file')
                <span class="error-message">{{ $message }}</span>
            @enderror
        </div>

        {{-- MS File --}}
        <div class="form-field">
            <label>Mark Scheme File</label>
            <input name="ms_file" type="file" accept=".pdf">
            <p class="mb-0 font-size1 gray2"><i>Accepted file format: .pdf only</i></p>
            @error('ms_file')
                <span class="error-message">{{ $message }}</span>
            @enderror
        </div>
    @endif


    @if(!$is_edit)
        {{-- Coin Price --}}
        <div class="form-group">
            <label for="name">Coin Price</label>

            {{-- Coin Price Radio Buttons --}}
            <div class="d-flex justify-content-start align-items-center mb-2">
                {{-- Free --}}
                <div class="styled-radio-input mr-2">
                    <input value="free" name="coin_price_toggle" type="radio" id="freeCoinPriceRadio" checked>
                    <label for="freeCoinPriceRadio" class="">Free</label>
                </div>

                {{-- Paid --}}
                <div class="styled-radio-input">
                    <input value="paid" name="coin_price_toggle" type="radio" id="paidCoinPriceRadio">
                    <label for="paidCoinPriceRadio" class="">Paid</label>
                </div>
            </div>

            {{-- Coin Price Select --}}
            <select name="coin_price" id="" disabled>
                <option value="0" disabled selected>Coin Price</option>
                @foreach(range(5, 100, 5) as $i)
                    <option value="{{ $i }}">{{ $i }} coins (USD {{ number_format($i * setting('conversion.coin_to_usd_sell'), 2) }})</option>
                @endforeach
            </select>

            @error('coin_price')
                <div class="error-message">{{ $message }}</div>
            @enderror
        </div>
    
        {{-- Confirm TnC --}}
        <div class="form-field">
            <input class="styled-checkbox" type="checkbox" name="confirm_tnc" id="confirm_tnc" required>
            <label for="confirm_tnc">I confirm I own or have permission to use all content in this resource and understand I may be liable for any copyright infringement. I verify that I have read and agree to the Board Author Code as well as to the general and additional terms.</label>
            @error('confirm_tnc')
                <div class="error-message">{{ $message }}</div>
            @enderror
        </div>
    @endif

    <div class="form-field">
        <span class="red">*</span><span class="gray2 font-size2">Required fields</span>
    </div>

    <div class="text-center">
        <button type="submit" class="button--primary w-250px loadOnSubmitButton">{{ $is_edit ? 'Update' : 'Upload' }}</button>
    </div>
</form>
