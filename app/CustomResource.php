<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomResource extends Model
{
    protected $guarded = [];

    /**
     * --------------
     * RELATIONSHIPS
     * --------------
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * --------------
     * ACCESSORS
     * --------------
     */
    /**
     * Returns data icon HTML string (iconify)
     * @return string
     */
    public function getDataIconAttribute()
    {
        return 'carbon:stacked-scrolling-1';
    }

    /**
     * --------------
     * HELPERS
     * --------------
     */

    /**
     * Save files on S3
     * @param UploadedFile $file
     * @return string File URL
     */
    public static function saveFile($file)
    {
        $file->store('custom_resources/', 's3');
        $filename = $file->hashName();
        return "custom_resources/{$filename}";
    }
}
