@extends('classroom.layouts.app')

@section('meta')
<title>Homework - {{ $homework->title }} | {{ config('app.name') }}</title>
@endsection

@section('content')
@component('classroom.common.breadcrumbs')
    <a href="{{ route('classroom.classrooms.show', $classroom->id) }}">Dashboard</a> /
    <a href="{{ route('classroom.lessons.index', $classroom->id) }}">Lessons</a> /
    <a href="{{ route('classroom.lessons.show', [$lesson->classroom->id, $lesson->id]) }}">{{ $lesson->name }}</a> /
    {{ str_limit($homework->title, 50) }}
@endcomponent

@component('classroom.common.page_heading', ['title' => $homework->primary])
    <div class="mb-0 gray2">{!! $homework->secondary !!}</b></div>

    @slot('button_group')
        @can('update', $homework)
            <a href="{{ route('classroom.lessons.homeworks.edit', [$lesson->id, $homework->id]) }}" class="button--primary--inverse mr-1"><span class="iconify" data-icon="feather:edit" data-inline="false"></span> Edit</a>
        @endcan
        @can('delete', $homework)
            <form action="{{ route('classroom.lessons.homeworks.delete', [$lesson->id, $homework->id]) }}" method="post">
                @csrf
                @method('DELETE')
                <button type="submit" class="button--danger deleteButton"><span class="iconify" data-icon="emojione-monotone:cross-mark" data-inline="false"></span> Delete</button>
            </form>
        @endcan
    @endslot
@endcomponent

{{-- Key Value Block --}}
@php
    $data = [
        ['key' => 'Given At',
         'value' => $homework->created_at->format('M d, Y h:iA'),
         'icon' => 'fas fa-calendar fa-2x',
         'color' => 'info'],
        ['key' => 'Due Date',
         'value' => $homework->due_at ? $homework->due_at->format('M d, Y h:iA') : 'N/A',
         'icon' => 'fas fa-calendar fa-2x',
         'color' => 'primary'],
        ['key' => 'Max Marks',
         'value' => $homework->max_marks ?? 'N/A',
         'icon' => 'fas fa-list fa-2x',
         'color' => 'success'],
    ];
@endphp
<div class="row">
    @foreach($data as $datum)
        <div class="col-xl-4 col-md-6 mb-4">
            @component('classroom.classrooms.common.key_value_block', ['datum' => $datum])
            @endcomponent
        </div>
    @endforeach
</div>

@php
    $data = [
        // 'Given At' => $homework->created_at->format('M d, Y h:iA'),
        // 'Due Date' => $homework->due_at->format('M d, Y h:iA'),
        // 'Max Marks' => $homework->max_marks ?? 'N/A',
        'Instruction' => $homework->instruction ?? 'N/A',
        'Chapter' => $homework->chapter ? $homework->chapter->name : 'N/A',
    ];
    if( Gate::allows('submit', $homework) ) {
        $submission = $homework->getStudentSubmission( auth()->user()->student );
        if( !is_null($submission) && $submission->isMarked() ) {
            $data = array_merge($data, [
                'Percentage Marks' => $submission->marks_in_percentage ? $submission->marks_in_percentage.'%' : 'N/A',
                'Absolute Marks' => $submission->absolute_marks_to_max ?? 'N/A',
                'Teacher\'s Remarks' => $submission->teacher_remark ?? 'N/A',
            ]);
        }
    }
@endphp

@component('classroom.common.details', ['data' => $data, 'resourceable' => $homework])
@endcomponent

<div class="mt-4 mb-5 border-bottom"></div>

<div class="row">

    <div class="col-12 col-lg-8">

        @can('mark', $homework)
            @foreach( ['submitted', 'overdue', 'marked'] as $status )
                <div class="mb-5">
                    <h3 class="pb-2 mb-3 border-bottom">
                        <b>{{ ucfirst($status) }}</b>
                        <span class="gray2 font-size1">{{ $homework->getSubmissionCountByStatus( $status ) }} @if($homework->getSubmissionCountByStatus( $status ) > 1) submissions @else submission @endif</span>
                    </h3>
                    @if( count( $homework->getSubmissionsWithStatus( $status ) ) )
                            <div class="row">
                                @foreach($homework->getSubmissionsWithStatus( $status ) as $submission)
                                    <div class="col-12 col-md-6 mb-3">
                                        @component('classroom.homeworks.submissions.common.block', ['submission' => $submission, 'classroom' => $classroom, 'full_width' => true, 'limit' => true])
                                            @slot('cta')
                                                <a href="{{ route('classroom.lessons.homeworks.submissions.show', [$lesson->id, $submission->id]) }}" class="button--primary">View</a>
                                            @endslot
                                        @endcomponent
                                    </div>
                                @endforeach
                            </div>
                    @else
                        <div class="text-center gray2 py-2">
                            No submission
                        </div>
                    @endif
                </div>
            @endforeach

        @endcan

        @can('submit', $homework)
            @if( !$homework->hasSubmitted() )
                @component('classroom.common.page_subheading', ['title' => 'Submit Homework'])
                @endcomponent
                <div class="panel">
                    <form action="{{ route('classroom.lessons.homeworks.submissions.store', [$lesson->id, $homework->id]) }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="form-field">
                            <label>Submission File</label>
                            <input type="file" name="file" id="">
                            @error('file')
                                <span class="error-message">{{ $message }}</span>
                            @enderror
                            <p class="gray2">Accepted format: .pdf only</p>
                        </div>

                        <div class="form-field">
                            <label>Comment <span style="font-weight: 400"></span>(Optional)</label>
                            <textarea type="text" name="comment" id=""></textarea>
                            @error('comment')
                                <span class="error-message">{{ $message }}</span>
                            @enderror
                        </div>

                        <div class="text-right">
                            <button type="submit" class="button--primary disableOnSubmitButton">Submit Homework</button>
                        </div>
                    </form>
                </div>
            @else
                @component('classroom.common.page_subheading', ['title' => 'Homework Submission'])
                    @slot('button_group')
                        @can('submit', $homework)
                            @if($homework->hasStudentSubmission(auth()->user()->student) && $homework->getStudentSubmission(auth()->user()->student)->isMarked())
                                <a href="{{ route('classroom.lessons.homeworks.submissions.show', [$lesson->id, $homework->getStudentSubmission(auth()->user()->student)->id]) }}" class="button--primary sm mr-1"><span class="iconify" data-icon="mdi:magnify-plus-outline" data-inline="false"></span> View in Detail</a>
                            @endif
                        @endcan
                    @endslot
                @endcomponent
                @component('classroom.homeworks.submissions.common.block', ['submission' => $homework->getStudentSubmission( auth()->user()->student ), 'classroom' => $classroom, 'full_width' => true])
                @endcomponent
            @endif
        @endcan

    </div>

    <div class="col-12 col-lg-4">

        @can('mark', $homework)
            @component('classroom.classrooms.common.list', ['classroom' => $classroom, 'type' => 'student', 'title' => 'Students with no submission', 'collection' => $homework->unsubmittedStudents()])
            @endcomponent

            @php
                $data = [
                    ['status' => 'submitted',
                    'bg-color' => 'blue6'],
                    // ['status' => 'marked',
                    // 'bg-color' => 'green'],
                    ['status' => 'returned',
                    'bg-color' => 'green'],
                    ['status' => 'overdue',
                    'bg-color' => 'red'],
                ];
            @endphp
            <div class="panel p-0">
                <div class="panel-header">Metrics</div>
                <div class="panel-body">
                    @component('classroom.homeworks.common.metric', ['homework' => $homework, 'data' => $data])
                    @endcomponent
                </div>
            </div>
        @endcan
    </div>
</div>

<div class="py-2"></div>
@endsection

@section('scripts')
<script>
$(function(){
    $('.question__comment-button, .answer__comment-button').click(function(){
        $(this).next('.question__comment-list, .answer__comment-list').toggle();
    });
});
</script>
@endsection
