@extends('classroom.layouts.app')

@section('meta')
    <title>Create Classroom | {{ config('app.name') }} Classroom</title>
@endsection

@section('styles')
@endsection

@section('content')

@component('classroom.common.breadcrumbs')
    <a href="{{ route('classroom.landing') }}">Home</a> /
    Create
@endcomponent

@component('classroom.common.page_heading', ['title' => 'Create Classroom'])
@endcomponent

<div class="maxw-740px mx-auto my-5">
    <div class="panel mb-2">
        <form action="{{ route('classroom.classrooms.store') }}" method="POST">
            @csrf
            <div class="form-field">
                <label>{{ __('Class Name') }}</label>
                <input type="text" name="name" value="{{ old('name') }}" required autofocus autocomplete="name" placeholder="Class Name">
                @error('name')
                    <span class="error-message">{{ $message }}</span>
                @enderror
            </div>

            <div class="form-field">
                <label>Exam Board</label>
                <select class="exam-board-select" name="exam_board">
                    <option data-placeholder="true"></option>
                    @foreach($exam_boards as $exam_board)
                        <option value="{{ $exam_board->id }}" {{ old('exam_board') == $exam_board->id ? 'selected="selected"' : '' }}>{{ $exam_board->name }}</option>
                    @endforeach
                </select>
                @error('exam_board')
                    <span class="error-message">{{ $message }}</span>
                @enderror
            </div>

            <div class="form-field">
                <label>Subject</label>
                <select class="subject-select" name="subject" disabled>
                    <option data-placeholder="true"></option>
                    @foreach($exam_boards as $exam_board)
                        <optgroup label="{{ $exam_board->name }}">
                            @foreach($exam_board->subjects as $subject)
                                <option value="{{ $subject->id }}">
                                    {{ $subject->name }}
                                </option>
                            @endforeach
                        </optgroup>
                    @endforeach

                </select>
                @error('subject')
                    <span class="error-message">{{ $message }}</span>
                @enderror
            </div>

            <div class="text-center">
                <button type="submit" class="button--primary disableOnSubmitButton mb-2">Create Classroom</button>
            </div>
        </form>
    </div>
</div>

@endsection

@section('scripts')
<script>
$(function(){
    var subject_selectHTML = $('.subject-select').html();
    /*
     * Initialise subject select
     */
    new SlimSelect({
        select: '.exam-board-select',
        placeholder: 'Select Exam Board',
        allowDeselectOption: true,
        onChange: () => {
            var exam_board_selected = $('.exam-board-select :selected').text();
            var $subject_select = $('.subject-select');
            $subject_select.prop('disabled', false);
            // restore the full select list first.
            $subject_select.html( subject_selectHTML );

            var optGroup = $('optgroup[label="' + exam_board_selected + '"]').html();

            $subject_select.html(optGroup);
            new SlimSelect({
                select: '.subject-select',
                placeholder: 'Select Subject',
                allowDeselectOption: true
            });
        }
    });

    /*
    * For failed requestsw
    */

    var exam_board_selected = $('.exam-board-select :selected').text();
    var $subject_select = $('.subject-select');
    //Enable chapter select if old subject is selected
    if(exam_board_selected){
        $subject_select.prop('disabled', false);
    }

    var optGroup = $('optgroup[label="' + exam_board_selected + '"]').html();
    $subject_select.html(optGroup);

    new SlimSelect({
        select: '.subject-select',
        placeholder: 'Select Subject',
        allowDeselectOption: true
    });
});
</script>
@endsection
