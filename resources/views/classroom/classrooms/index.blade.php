@extends('classroom.layouts.app')

@section('meta')
    <title>Classrooms | {{ config('app.name') }} Classroom</title>
@endsection

@section('content')
@component('classroom.common.breadcrumbs')
    Home
@endcomponent

@component('classroom.common.page_heading', ['title' => 'Classrooms'])
@endcomponent

<div class="row justify-content-center align-items-start">
    <div class="col-12 col-lg-9">
        <div class="row">
            @foreach( $classrooms as $room )
                <div class="col-12 col-sm-6 col-lg-4 mb-3">
                    @component('classroom.classrooms.common.block', ['room' => $room])
                    @endcomponent
                </div>
            @endforeach

            @can('create', App\Classroom::class)
                <div class="col-12 col-sm-6 col-lg-4 mb-3">
                    @component('classroom.classrooms.common.block', ['title' => 'Create new classroom', 'url' => route('classroom.classrooms.create')])
                    @endcomponent
                </div>
            @endcan

            @can('join', App\Classroom::class)
                <div class="col-12 col-sm-6 col-lg-4 mb-3">
                    @component('classroom.classrooms.common.block', ['title' => 'Join new classroom', 'url' => route('classroom.classrooms.students.getJoinClassView')])
                    @endcomponent
                </div>
            @endcan

        </div>
    </div>

    <div class="col-12 col-lg-3">
        @component('classroom.common.timeline_block', ['timeline_items' => $timeline_items])
        @endcomponent
    </div>
</div>

<div class="py-2"></div>
@endsection
