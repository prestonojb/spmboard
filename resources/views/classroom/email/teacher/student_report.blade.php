@component('mail::message')
# Student Report
Hi {{ $student->parent->name ?? $student->parent->username }}, this email is to notify you that your child, {{ $student->name ?? $student->username }}'s progress report is ready.<br>
This progress report is meant for the period from {{ $report->start_date->toFormattedDateString() }} to {{ $report->end_date->toFormattedDateString() }} is ready.

@component('mail::button', ['url' => $url, 'color' => 'primary'])
View All Reports
@endcomponent

Best Regards,<br>
{{ $classroom->teacher->name ?? $classroom->teacher->username }}<br>
Teacher of {{ $classroom }}

@endcomponent
