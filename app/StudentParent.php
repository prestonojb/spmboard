<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class StudentParent extends Model
{
    use Notifiable;

    protected $guarded = [];
    protected $primaryKey = 'user_id';
    public $appends = ['email'];
    public $additional_attributes = ['email'];

    public function getRouteKeyName()
    {
        return 'user_id';
    }

    /**
     * --------------------
     * Relationships
     * --------------------
     */
    public function children()
    {
        return $this->hasMany(Student::class, 'parent_id', 'user_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * --------------------
     * Accessors
     * --------------------
     */

    public function getAccountTypeAttribute()
    {
        return 'parent';
    }

    public function getUsernameAttribute()
    {
        return $this->user->username;
    }

    public function getEmailAttribute()
    {
        return $this->user->email;
    }

    public function getNameAttribute()
    {
        return $this->user->name;
    }

    public function getAvatarAttribute()
    {
        return $this->user->avatar;
    }

    public function getLastOnlineAtAttribute()
    {
        return $this->user->last_online_at;
    }

    public function getAboutAttribute()
    {
        return $this->user->about;
    }

    public function getClassroomsAttribute()
    {
        return $this->classrooms();
    }


    /**
     * Returns profile color
     * @return string
     */
    public function getProfileColor()
    {
        return 'green';
    }

    /**
     * ------------------
     * Helpers
     * ------------------
     */
    /**
     * Returns true if parent linked to student account
     * @return boolean
     */
    public function hasChild(Student $student)
    {
        return in_array($student->user_id, $this->children_ids);
    }

    /**
     * Return all classrooms that all children are in (no duplicate)
     * @return Collection
     */
    public function classrooms()
    {
        $classroom_ids = collect();

        foreach( $this->children as $child ) {
            // Push each child's classroom IDs into array(2D)
            $child_classroom_ids = $child->classrooms()->pluck('id');
            // $classroom_ids = collect( (1, 2, 3), (4, 5, 1), ...)
            $classroom_ids->push( $child_classroom_ids );
        }

        $classroom_ids = $classroom_ids->flatten();
        return Classroom::find($classroom_ids);
    }

    public function getChildrenIdsAttribute()
    {
        return $this->children()->pluck('user_id')->toArray();
    }

    public function setNameAttribute($value)
    {
        $this->user()->update([
            'name' => $value
        ]);
    }

    /**
     * -------------------
     * Helpers
     * -------------------
     */

    /**
     * Get lesson IDs associated with parent
     * @param(optional) App\Classroom $classroom Filter lesson IDs by classroom
     * @return array
     */
    public function getLessonIds( Classroom $classroom = null )
    {
        if( !is_null( $classroom ) ) {
            if( !$classroom->hasParent($this) ) {
                return array();
            }
            // Get all lessons from classroom
            return $classroom->lessons()->pluck('id')->toArray();
        } else {
            $classroom_ids = $this->classrooms()->pluck('id')->toArray();
            // Get all lessons from the student
            return Lesson::whereHas('classroom', function($q) use ($classroom_ids){
                $q->whereIn('id', $classroom_ids);
            })->pluck('id')->toArray();
        }
    }

    /**
     * --------------------
     * Exam Builder
     * --------------------
     */

    /**
     * Returns true if parent has permission to generate exam builder MS, else false
     * @return boolean
     */
    public function canGenerateExamBuilderSheetMs()
    {
        return false;
    }

    /**
     * Returns true if parent can hide exam builder sheet watermark
     * @return boolean
     */
    public function canHideExamBuilderSheetWatermark()
    {
        return false;
    }

    /**
     * Returns true if parent can generate exam builder sheet with subchapter
     * @return boolean
     */
    public function canGenerateExamBuilderSheetWithSubchapter()
    {
        return false;
    }

    /**
     * Returns max number of questions parent can generate for exam builder sheet
     * @return int
     */
    public function getExamBuilderQuestionLimit()
    {
        return 10;
    }

    /**
     * Returns max number of chapters teacher can generate for exam builder sheet
     * @return int
     */
    public function getExamBuilderChapterLimit()
    {
        return 1;
    }

    /**
     * Returns true if parent has premium resource access
     * @return boolean
     */
    public function hasPremiumResourceAccess()
    {
        return false;
    }

    /**
     * Returns library limit of user
     * @return int
     */
    public function getLibraryLimit()
    {
        return 0;
    }

    /**
     * Returns true if parent passed all certification metrics, else returns false
     * @return boolean
     */
    public function passedAllCertificationMetrics()
    {
        return false;
    }
}
