<?php

namespace App;

use App\Traits\FullTextSearch;
use Illuminate\Database\Eloquent\Model;
use CyrildeWit\EloquentViewable\Viewable;
use CyrildeWit\EloquentViewable\Contracts\Viewable as ViewableContract;
use CyrildeWit\EloquentViewable\Support\Period;
use Carbon\Carbon;
use \Venturecraft\Revisionable\RevisionableTrait;
use App\Traits\QuestionSortable;
use Illuminate\Support\Facades\Storage;

class Question extends Model implements ViewableContract
{
    use FullTextSearch;
    use Viewable;
    use RevisionableTrait;
    use QuestionSortable;

    protected $revisionEnabled = true;
    protected $revisionCleanup = true; //Remove old revisions (works only when used with $historyLimit)
    protected $historyLimit = 20; //Stop tracking revisions after 500 changes have been made.

    protected $keepRevisionOf = ['question'];

    public function identifiableName()
    {
        return $this->question;
    }

    //Allow mass assignment to all variables in the model
    protected $guarded = [];

    //Full text index search
    protected $searchable = ['question', 'description'];

    protected $removeViewsOnDelete = true;

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'anonymous' => 'boolean',
    ];

    public static $valid_sorts = array('hot', 'top', 'new', 'answered');

    /**
     * ---------------
     * Relationships
     * ---------------
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function answers()
    {
        return $this->hasMany(Answer::class);
    }

    public function recommender()
    {
        return $this->belongsTo(User::class, 'recommended_by_user_id', 'id');
    }

    public function classrooms()
    {
        return $this->belongsToMany(Classroom::class, 'classroom_question');
    }

    public function question_upvotes()
    {
        return $this->hasMany(QuestionUpvote::class);
    }

    public function subject()
    {
        return $this->belongsTo(Subject::class);
    }

    public function chapters()
    {
        return $this->belongsToMany(Chapter::class);
    }

    public function comments()
    {
        return $this->hasMany(QuestionComment::class);
    }

    public function coin_action()
    {
        return $this->morphOne(CoinAction::class, 'coin_actionable');
    }

    public function sortedAnswers()
    {
        //Get answers to question
        return $this->answers()
                    ->withCount('answer_upvotes')
                    ->orderByDesc('checked')
                    ->orderByDesc('answer_upvotes_count')
                    ->get();
    }

    /**
     * ----------------
     * Accessors
     * ----------------
     */

     /**
      * Returns description without tags
      * @return string
      */
    public function getBriefDescriptionAttribute()
    {
        return str_limit(strip_tags($this->description), 350);
    }

    /**
     * Returns show URL
     * @return string
     */
    public function getShowUrlAttribute()
    {
        return route('questions.show', [$this->subject->exam_board->slug, $this->id]);
    }

    // Number of upvotes for this question
    public function getUpvoteCountAttribute()
    {
        return count($this->question_upvotes);
    }

    // Number of answers for this question
    public function getAnswerCountAttribute()
    {
        return $this->answers->count();
    }
    /**
     * Returns true if user has upvoted this question, else false
     * @return boolean
     */
    public function getUpvotedAttribute()
    {
        if( !auth()->check() ){
            return false;
        }
        return QuestionUpvote::where('question_id', $this->id)->where('user_id', auth()->user()->id)->exists();
    }

    /**
     * Returns true if question RP has been claimed, else false
     * @return boolean
     */
    public function getPointsClaimedAttribute()
    {
        return $this->reward_points == 0 || !is_null($this->points_awarded_at);
    }

    /**
     * Returns true if question has reward points
     * @return boolean
     */
    public function hasRewardPointBounty()
    {
        return $this->reward_points > 0;
    }

    /**
     * Returns true if question reward point bounty is available (not claimed yet)
     * @return boolean
     */
    public function rewardPointBountyAvailable()
    {
        return is_null($this->points_awarded_at);
    }


    /**
     * ------------------------------------------------------------------------------------
     * Recommendation state
     * 0 = NULL
     * 1 = NOT NULL
     * ------------------------------------------------------------------------------------
     * recommended_by_user_id |  recommended_at  | State
     *            0                    0           Not recommended
     *            0                    1           Not defined
     *            1                    0           Was recommendeded and then unrecommended
     *            1                    1           Recommended
     *
     * @return boolean $isRecommended
     */
    public function getRecommendedAttribute()
    {
        return !is_null($this->recommended_at) && !is_null($this->recommended_by_user_id);
    }

    public function getAnsweredAttribute()
    {
        foreach($this->answers as $answer) {
            if($answer->checked == 1){
                $answered = true;
                break;
            }
        }

        return $answered ?? false;
    }

    public function getChapterIdsAttribute()
    {
        return $this->chapters->pluck('id')->toArray();
    }

    public function setQuestionAttribute($value)
    {
        $this->attributes['question'] = $value;
        $this->attributes['slug'] = str_slug($value);
    }

    public function getImgAttribute($value)
    {
        if($value){
            if(strpos($value, 'amazonaws')){
                //Amazon link for image
                return $value;
            } else{
                //Amazon S3
                $updated_value = str_replace('\\', '/', $value);
                return Storage::cloud()->url($updated_value);
            }
        } else {
            return null;
        }
    }

    public function getIsQuestionOfTheDayAttribute()
    {
        return $this->created_at->isToday() && str_slug($this->flagged_as) == 'question-of-the-day';
    }

    /**
     * Get recommended questions for given user
     *
     *  @param App\User $user
     *  @return Illuminate\Database\Eloquent\Collection $recommended_questions
     */

    public static function recommendedQuestionsForUser($user) {
        $recommended_questions = Question::where('user_id', $user->id)
                                ->whereNotNull('recommended_at')
                                ->withCount('question_upvotes')
                                ->orderByDesc('question_upvotes_count')
                                ->get();

        foreach($recommended_questions as $question) {
            $question['recommended'] = true;
        }

        return $recommended_questions;
    }

    /**
     * Get most upvoted questions for given user
     *
     *  @param App\User $user
     *  @param array $except
     *  @param int $noOfQuestions
     *  @return Illuminate\Database\Eloquent\Collection $most_upvoted_questions
     */
    public static function mostUpvotedQuestionsForUser($user, $except = null, $noOfQuestions = 5) {
        $most_upvoted_questions = Question::where('user_id', $user->id)
                                    ->whereNotIn('id', $except)
                                    ->withCount('question_upvotes')
                                    ->orderByDesc('question_upvotes_count')
                                    ->take($noOfQuestions)
                                    ->get();

        foreach($most_upvoted_questions as $question) {
            $question['most_upvoted'] = true;
        }

        return $most_upvoted_questions;
    }

    /**
     * Get most viewed questions for given user
     *
     *  @param App\User $user
     *  @param array $except
     *  @param int $noOfQuestions
     *  @return Illuminate\Database\Eloquent\Collection $most_viewed_questions
     */
    public static function mostViewedQuestionsForUser($user, $except = null, $noOfQuestions = 5) {
        $most_viewed_questions = Question::where('user_id', $user->id)
                                        ->whereNotIn('questions.id', $except)
                                        ->orderByViews()
                                        ->take($noOfQuestions)
                                        ->get();

        foreach($most_viewed_questions as $question) {
            $question['most_viewed'] = true;
        }

        return $most_viewed_questions;
    }

    /**
     * Get top 5 questions for given user
     *  @param App\User $user
     *  @return Illuminate\Database\Eloquent\Collection $top_questions
     */
    public static function topQuestionsForUser($user, $noOfQuestionsToReturn = 5)
    {
        $top_questions = collect();

        $recommended_questions = Question::recommendedQuestionsForUser($user); // Get recommended questions for user
        $top_questions = $top_questions->merge($recommended_questions);

        $noOfQuestionsLeft = $noOfQuestionsToReturn - count($top_questions); // Get number of questions more required to return pre-defined $noOfQuestionsToReturn

        // Keep going until number of top questions reaches $noOfQuestionsToReturn...
        if($noOfQuestionsLeft > 0) {
            // Get most upvoted questions by user, excluding recommended questions
            $most_upvoted_questions = Question::mostUpvotedQuestionsForUser($user, $top_questions->pluck('id'), $noOfQuestionsLeft);

            $top_questions = $recommended_questions->merge($most_upvoted_questions);
        }

        return $top_questions;
    }

    /**
     * Returns all questions of user
     * @param App\User $user
     * @return Paginator
     */
    public static function allQuestionsForUser($user, $perPage = 10)
    {
        $questions = collect();

        $recommended_questions = Question::recommendedQuestionsForUser($user); // Get recommended questions for user
        $questions = $questions->merge($recommended_questions);

        $noOfQuestionsLeft = Question::all()->count() - count($questions);

        // Get most upvoted questions by user, excluding recommended questions
        $most_upvoted_questions = Question::mostUpvotedQuestionsForUser($user, $questions->pluck('id'), $noOfQuestionsLeft);

        $questions = $questions->merge($most_upvoted_questions);

        return collectionPaginate($questions, $perPage);
    }

    /**
     * Retuens related questions
     * @param int $no_of_questions_by_subject
     * @param int $no_of_questions_by_chapter
     * @return Collection
     */
    public function getRelatedQuestions($no_of_questions_by_subject = 2, $no_of_questions_by_chapter = 2)
    {
        $questions_by_subject = $this->getRelatedQuestionsBySubject($no_of_questions_by_subject);
        $questions_by_chapter = $this->getRelatedQuestionsByChapter($no_of_questions_by_chapter);
        return $questions_by_subject->merge($questions_by_chapter);
    }

    /**
     * Returns related questions by subject
     * @param int $take
     * @return Collection
     */
    public function getRelatedQuestionsBySubject($take = 2)
    {
        return self::where('questions.id', '!=', $this->id)->where('subject_id', $this->subject_id)->orderByViews()->take($take)->get();
    }

    /**
     * Returns related questions by chapter
     * @param App\Chapter $chapter
     * @param int $take
     * @return Collection
     */
    public function getRelatedQuestionsByChapter($take = 2)
    {
        $related_questions = collect();
        foreach($this->chapters as $chapter) {
            // Get questions from chapters
            $questions =  self::where('questions.id', '!=', $this->id)->whereHas('chapters', function ($q) use ($chapter) {
                return $q->where('chapters.id', $chapter->id);
            })->orderByViews()->take($take)->get();
            $related_questions = $related_questions->merge($questions);
        }
        return $related_questions;
    }

    /**
     * Save Image to S3 and DB
     * @param UploadedFile $img
     */
    public function saveImage($img)
    {
        //Current Month and Year
        $my = date('FY', time());

        //Store image to S3
        $path = $img->store('questions/'. $my. '/', 's3');
        $filename = $img->hashName();
        //Save image url in database
        $file_url = 'questions/'. $my. '/'. $filename;
        $this->update([
            'img' => $file_url,
        ]);
    }
}
