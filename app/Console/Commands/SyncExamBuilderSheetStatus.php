<?php

namespace App\Console\Commands;

use App\ExamBuilderSheet;
use Illuminate\Console\Command;

class SyncExamBuilderSheetStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:exam_builder_sheet';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync exam builder sheet statuses (QP/MS).';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $sheets = ExamBuilderSheet::all();
        foreach($sheets as $sheet) {
            $qp_status = $sheet->hasQp() ? 'available' : 'unavailable';
            $ms_status = $sheet->hasMs() ? 'available' : 'unavailable';
            // Update sheet
            $sheet->update([
                'qp_status' => $qp_status,
                'ms_status' => $ms_status,
            ]);
        }
    }
}
