<?php

namespace App\Http\Middleware;

use Closure;
use App\Subject;
use Illuminate\Support\Facades\View;

class LoadVariablestoViews
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        // $subjects = Subject::all();
        // View::share('subjects', $subjects);

        if(auth()->check()) {

            $user = auth()->user();
            // Temporary limit for notifications
            $unread_notifications = $user->unreadNotifications;
            $read_notifications = $user->readNotifications()->take(10)->get();

            View::share('unread_notifications', $unread_notifications);
            View::share('read_notifications', $read_notifications);
        }

        // View::share('a', 'a');
        return $next($request);
    }
}
