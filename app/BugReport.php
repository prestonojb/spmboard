<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BugReport extends Model
{
    protected $guarded = [];

    /**
     * --------------
     * ACCESSORS
     * --------------
     */
    /**
     * Save image to S3 and DB
     * @param UploadedFile $img
     */
    public function saveImage($img)
    {
        //Store image to S3
        $path = $img->store('bug_reports/', 's3');
        $filename = $img->hashName();
        //Save image url in database
        $file_url = 'bug_reports/'. $filename;
        $this->update([
            'img' => $file_url,
        ]);
    }

    /**
     * Returns all preset tags
     * @return array
     */
    public static function getTags()
    {
        return [
            'resource' => 'Resource',
            'bug' => 'Bug',
            'other' => 'Other',
        ];
    }

    /**
     * --------------
     * MUTATORS
     * --------------
     */
}
