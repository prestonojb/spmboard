@extends('classroom.layouts.app')

@section('meta')
    <title>Dashboard - {{ $classroom->name }} | {{ config('app.name') }} Classroom</title>
@endsection


@section('content')

@component('classroom.common.breadcrumbs')
    Dashboard
@endcomponent

@component('classroom.common.page_heading', ['title' => $classroom->name])
    @slot('button_group')
        @if(auth()->user()->is_teacher())
            <a href="@cannot('add_student', $classroom) javascript:void(0) @else {{ route('classroom.classrooms.students.get_add_view', $classroom->id) }} @endcannot" class="button--primary--inverse sm mr-1 @cannot('add_student', $classroom) disabled @endcannot"><i class="fas fa-plus"></i> Add student</a>
            <a href="@cannot('create_lesson', $classroom) javascript:void(0) @else {{ route('classroom.lessons.create', $classroom->id) }} @endcannot" class="button--primary sm mr-1 @cannot('create_lesson', $classroom) disabled @endcannot"><i class="fas fa-plus"></i> Add Lesson</a>
        @endif
        @can('update', $classroom)
            <div class="dropdown">
                <button class="button--primary--inverse sm" type="button" id="classroomDropdownButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fas fa-ellipsis-h"></i>
                </button>
                <div class="dropdown-menu" aria-labelledby="classroomDropdownButton">
                    <a class="dropdown-item" href="{{ route('classroom.classrooms.edit', $classroom->id) }}">Edit Classroom</a>
                </div>
            </div>
        @endcan
    @endslot
@endcomponent

{{-- Key Value Block --}}
@php
    $data = [
        ['key' => 'Number of Students',
         'value' => count($classroom->students ?? []),
         'icon' => 'fas fa-user fa-2x',
         'color' => 'primary'],
        ['key' => 'Subject',
         'value' => $classroom->subject->exam_board_and_name,
         'icon' => 'fas fa-list fa-2x',
         'color' => 'success'],
        ['key' => 'Code',
         'value' => $classroom->code,
         'icon' => 'fas fa-hashtag fa-2x',
         'color' => 'info'],
    ];
@endphp
<div class="row">
    @foreach($data as $datum)
        <div class="col-xl-4 col-md-6 mb-4">
            @component('classroom.classrooms.common.key_value_block', ['datum' => $datum])
            @endcomponent
        </div>
    @endforeach
</div>


<div class="row">
    {{-- Lessons --}}
    <div class="col-12 col-lg-8">
        @component('classroom.classrooms.common.list', ['classroom' => $classroom, 'type' => 'lesson', 'collection' => $classroom->getDashboardLessonItems(), 'title' => 'Recent Lessons', 'url' => route('classroom.lessons.index', $classroom->id)])
        @endcomponent
    </div>
    {{-- Students --}}
    <div class="col-12 col-lg-4">
        @component('classroom.classrooms.common.list', ['classroom' => $classroom, 'type' => 'student', 'title' => 'Student', 'collection' => $classroom->getDashboardStudentItems()])
        @endcomponent
    </div>
</div>

<hr>

@can('view_analytics', $classroom)
    <div class="row">
        {{-- Resource Post Graph --}}
        <div class="col-12 col-lg-8">
            <div class="panel p-0 mb-3">
                <div class="panel-header">Resource Posts By Month</div>
                <div class="panel-body p-4">
                    <canvas id="noOfResourcePostTime"></canvas>
                </div>
            </div>
        </div>
        {{-- Resource Post List --}}
        <div class="col-12 col-lg-4 mb-3">
            @component('classroom.classrooms.common.list', ['classroom' => $classroom, 'title' => 'Recent Resource Posts', 'type' => 'resource post', 'collection' => $classroom->getDashboardResourcePostItems()])
            @endcomponent
        </div>
    </div>

    <hr>

    <div class="row">
        {{-- Homework Graph --}}
        <div class="col-12 col-lg-8">
            <div class="panel p-0 mb-3">
                <div class="panel-header">Homework Volume By Month</div>
                <div class="panel-body p-4">
                    <canvas id="noOfHomeworkTime"></canvas>
                </div>
            </div>
        </div>
        {{-- Homeworks --}}
        <div class="col-12 col-lg-4 mb-3">
            @component('classroom.classrooms.common.list', ['classroom' => $classroom, 'type' => 'homework', 'collection' => $classroom->getDashboardHomeworkItems(), 'title' => 'Recent Homework'])
            @endcomponent
        </div>
    </div>
@else
    <div class="row">
        <div class="col-12 col-md-6 mb-3">
            @component('classroom.classrooms.common.list', ['classroom' => $classroom, 'title' => 'Recent Resource Posts', 'type' => 'resource post', 'collection' => $classroom->getDashboardResourcePostItems()])
            @endcomponent
        </div>

        <div class="col-12 col-md-6 mb-3">
            @component('classroom.classrooms.common.list', ['classroom' => $classroom, 'type' => 'homework', 'collection' => $classroom->getDashboardHomeworkItems(), 'title' => 'Recent Homework'])
            @endcomponent
        </div>
    </div>
@endcan

<div class="py-2"></div>
@endsection

@section('scripts')
<script>
// Resource Post Graph variables
var noOfResourcePostsTimeLabels = @json( array_keys($no_of_resource_posts_time) );
var noOfResourcePostsTimeData = @json( array_values($no_of_resource_posts_time) );

// Homework Graph variables
var noOfHomeworksTimeLabels = @json( array_keys($no_of_homeworks_time) );
var noOfHomeworksTimeData = @json( array_values($no_of_homeworks_time) );
</script>
@endsection
