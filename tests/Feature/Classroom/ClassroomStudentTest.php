<?php

namespace Tests\Feature\Classroom;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;
use App\Classroom;
use App\ExamBoard;
use App\Student;
use App\Subject;
use App\Teacher;
use App\User;

class ClassroomStudentTest extends TestCase
{
    use WithFaker;

    public function setUp() : void
    {
        parent::setUp();

        // Subscribed Teacher
        $user = factory(User::class)->states('subscribed_teacher')->create();
        $this->teacher = $user->teacher;

        // Students
        $user = factory(User::class)->states('student')->create();
        $this->student0 = $user->student;

        $user = factory(User::class)->states('student')->create();
        $this->student1 = $user->student;

        $user = factory(User::class)->states('student')->create();
        $this->student2 = $user->student;

        // Subject and classroom
        $this->subject = Subject::first();
        $this->classroom = factory(Classroom::class)->create([
                                'teacher_id' => $this->teacher->user_id,
                                'subject_id' => $this->subject->id,
                            ]);
    }

    /**
     * @group classroom_student
     * Test Get Add Student View
     */
    public function testGetAddView()
    {
        $response = $this->actingAs($this->teacher->user)
                         ->get(route('classroom.classrooms.students.get_add_view', $this->classroom->id));
        $response->assertSuccessful();
    }

    /**
     * @group classroom_student
     * Test Add Student View
     */
    public function testAdd()
    {
        $email_data = [$this->getStudentEmail(), $this->getStudentEmails(),
                       $this->getInvalidStudentEmail(), $this->getInvalidStudentEmails()];

        // Loop through all possible inputs
        for($i=0; $i < count($email_data); $i++) {
            // Create classroom for each instance
            $classroom = factory(Classroom::class)->create([
                            'teacher_id' => $this->teacher->user_id,
                            'subject_id' => $this->subject->id,
                        ]);
            $response = $this->actingAs($this->teacher->user)
                             ->post(route('classroom.classrooms.students.add', $classroom->id), [
                                'email' => $email_data[$i]
                             ]);
            // Valid data
            if($i <= 1) {
                $response->assertRedirect();
                $emails = commaSeperatedStringToArray($email_data[$i]);
                // Assert all students added to classroom
                $all_students_added = $classroom->hasStudents( Student::retrieveWithEmails( $emails ) );
                $this->assertTrue($all_students_added);
            } else {
                $response->assertSessionHasErrors('email');
                // Assert no student added to classroom
                $this->assertTrue( $classroom->isEmpty() );
            }
        }
    }

    /**
     * Returns student email
     * @return string
     */
    public function getStudentEmail()
    {
        return $this->student0->email;
    }

    /**
     * Returns students email
     * @return string
     */
    public function getStudentEmails()
    {
        return "{$this->student0->email}, {$this->student1->email}, {$this->student2->email}";
    }

    /**
     * Returns invalid student email string
     * @return array
     */
    public function getInvalidStudentEmail()
    {
        return 'asd';
    }

    /**
     * Returns invalid student emails
     * @return string
     */
    public function getInvalidStudentEmails()
    {
        return "{$this->student0->email}, {$this->student1->email}, asd";
    }
}
