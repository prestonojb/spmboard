@extends('classroom.layouts.app')

@section('meta')
    <title>Create Invoice | {{ config('app.name') }} Classroom</title>
@endsection

@section('styles')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.1.2/css/tempusdominus-bootstrap-4.min.css">
@endsection

@section('content')

@component('classroom.common.breadcrumbs')
    <a href="{{ route('classroom.landing') }}">Home</a> /
    <a href="{{ route('classroom.invoices.index') }}">Invoices</a> /
    Create
@endcomponent

@component('classroom.common.page_heading', ['title' => 'Create Invoice'])
@endcomponent

<div class="maxw-740px mx-auto my-5">

    @if( !is_null($setting) )
        <p class="font-size3 gray2 text-center mb-2">
            Make invoicing easier by configuring invoice settings <a style="font-size: inherit; font-weight: inherit;" href="{{ route('classroom.settings.teacher.invoice') }}">here</a>.
        </p>
    @endif

    <div class="panel">
        <form action="{{ route('classroom.invoices.store') }}" method="post" id="teacherCreateInvoiceForm">
            @csrf
            <input type="hidden" name="teacher_invoice_items">

            <div class="form-field">
                <label for="">Student</label>
                <select name="student" id="" required>
                    <option value="" disabled selected>Select student</option>

                    @foreach( $students as $student )
                        <option value="{{ $student->user_id }}" @if( request()->student == $student->user_id || old('student') == $student->user_id ) selected @endif>{{ $student->name }}</option>
                    @endforeach
                </select>
                @error('student')
                    <div class="error-message">{{ $message }}</div>
                @enderror
            </div>

            <div class="form-field">
                <label for="">Parent's Email</label>
                <input type="email" value="" name="parent_email" value="{{ old('parent_email') }}" readonly>
                <p class="mb-0 gray2 font-size2">
                    *Once the invoice is prepared, the invoice will automatically be sent to the student's parent email.
                </p>
            </div>

            <div class="form-field">
                <label for="" class="mb-3">Items</label>

                <div class="invoiceItemTableContainer mb-2">
                    <table class="table invoiceItemTable">
                        <thead>
                            <tr>
                                <th scope="col">Item</th>
                                <th>Qty</th>
                                <th>Unit Price(MYR)</th>
                                <th>Price(MYR)</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="lessonInvoiceItemRow">
                                <td>
                                    Lesson (per hour)<br>
                                    <button type="button" class="button-link blue4 underline-on-hover" data-toggle="modal" data-target="#lessonTableModal">Select</button>
                                </td>
                                <td><input type="number" name="no_of_units" min="0" max="99" id="" class="maxw-70px" value="0" readonly></td>
                                <td><input type="number" name="price_per_unit" id="" min="0" max="999.99" step="0.01" value="{{ !is_null($setting) ? $setting->default_lesson_rate_per_hour : 0 }}" class="maxw-100px"></td>
                                <td class="itemTotalPrice">0.00</td>
                                <td>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="my-2">
                        <button type="button" class="button--primary--inverse addItemButton"><i class="fas fa-plus"></i> Add Item</button>
                    </div>
                </div>
                @error('invoice_items')
                    <div class="error-message">{{ $message }}</div>
                @enderror
            </div>

            <div class="form-field">
                <label for="">Payment Due</label>
                <div class="d-flex align-items-center">
                    <div class="mr-2">
                        <input class="maxw-100px" type="number" step="1" name="due_in_days" value="{{ !is_null($setting) ? $setting->default_payment_due_in_days : '' }}">
                    </div>
                    <div class="">
                        days after invoice is sent
                    </div>
                </div>
            </div>

            <div class="text-center">
                <button class="button--primary" type="submit">Send Invoice</button>
            </div>

            {{-- Helper Invoice Item Row --}}
            <div class="d-none">
                <table>
                    <tr class="invoiceItemRowHelper">
                        <td><input type="text" name="invoice_item_title"></td>
                        <td><input type="number" name="no_of_units" min="0" max="99" id="" class="maxw-70px" value="1"></td>
                        <td><input type="number" name="price_per_unit" id="" min="0" max="999.99" step="0.01" value="0.00" class="maxw-100px"></td>
                        <td class="itemTotalPrice">0.00</td>
                        <td>
                            <button type="button" class="button-link red deleteItemButton"><i class="fas fa-times"></i></button>
                        </td>
                    </tr>
                </table>
            </div>

            @include('classroom.invoices.modal.lesson_table')
        </form>
    </div>
</div>

@endsection
