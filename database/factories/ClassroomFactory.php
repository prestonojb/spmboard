<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Classroom;
use App\Model;
use App\Teacher;
use Faker\Generator as Faker;

$factory->define(Classroom::class, function (Faker $faker) {
    return [
        'name' => $faker->company,
        'teacher_id' => function() {
            return factory(Teacher::class)->create()->id;
        },
        'subject_id' => 1,
        'code' => getRandomString(7),
    ];
});
