<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Library extends Model
{
    protected $guarded = [];

    public static $maxCapacity = 20;

    /**
     * ----------------
     * RELATIONSHIPS
     * ----------------
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function resources()
    {
        return $this->belongsToMany(Resource::class, 'library_resource', 'resource_id', 'library_id')->withTimestamps()->orderByDesc('library_resource.created_at');
    }

    public function custom_resources()
    {
        return $this->belongsToMany(CustomResource::class, 'library_resource', 'custom_resource_id', 'library_id')->withTimestamps()->orderByDesc('library_resource.created_at');
    }

    /**
     * ----------------
     * ACCESSORS
     * ----------------
     */

    public function getShowUrlAttribute()
    {
        return route('libraries.show', $this->id);
    }

    /**
     * ----------------
     * HELPERS
     * ----------------
     */

    /**
     * Returns true if library has resource, else false
     * @param App\Resource $resource
     * @return boolean
     */
    public function hasResource(Resource $resource)
    {
        return $this->resources()->where('resources.id', $resource->id)->exists();
    }

    /**
     * Returns true if library is full, else false
     * @return boolean
     */
    public function isFull()
    {
        return count($this->resources) >= self::$maxCapacity;
    }

    /**
     * Add (prebuilt) resource to library
     */
    public function toggleResource(Resource $resource)
    {
        $this->resources()->toggle($resource->id);
    }

    public function addCustomResource(CustomResource $custom_resource)
    {
        $this->custom_resource()->attach($custom_resource->id);
    }

    /**
     * Returns total number of resources (prebuilt and custom)
     * @return int
     */
    public function getTotalResourceCount()
    {
        return count($this->resources ?? []) + count($this->custom_resources ?? []);
    }

    /**
     * Returns Past Papers (prebuilt) resource of library
     * @return Collection
     */
    public function getPastPapers()
    {
        return $this->resources()->where('resourceable_type', PastPaper::class)->get();
    }

    /**
     * Returns Topical Past Papers (prebuilt) resource of library
     * @return Collection
     */
    public function getTopicalPastPapers()
    {
        return $this->resources()->where('resourceable_type', TopicalPastPaper::class)->get();
    }
    /**
     * Returns Notes (prebuilt) resource of library
     * @return Collection
     */
    public function getNotes()
    {
        return $this->resources()->where('resourceable_type', Notes::class)->get();
    }
    /**
     * Returns Guides (prebuilt) resource of library
     * @return Collection
     */
    public function getGuides()
    {
        return $this->resources()->where('resourceable_type', Guide::class)->get();
    }
    /**
     * Returns custom resource of library
     * @return Collection
     */
    public function getCustomResources()
    {
        return $this->custom_resources;
    }

    /**
     * Delete all related records in DB
     */
    public function cleanDelete()
    {
        // Delete library resource records
        $this->resources()->detach();
        $this->custom_resources()->detach();
        $this->delete();
    }

    /**
     * Returns true if library is user's most recent, else false
     * @return boolean
     */
    public function isMostRecent()
    {
        $most_recent_library = $this->user->getMostRecentLibrary();
        return $most_recent_library->id == $this->id;
    }

    /**
     * Returns true if library is disabled, else false
     * @return boolean
     */
    public function isDisabled()
    {
        $user_library_limit = $this->user->getLibraryLimit();
        $user_library_count = count($this->user->libraries ?? []);
        $exceeds_limit = $user_library_count > $user_library_limit;

        // If user exceed library limit, only most recent library is not disabled
        return $exceeds_limit ? !$this->isMostRecent()
                              : false;
    }
}
