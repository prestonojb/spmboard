<?php

namespace App\Notifications\Classroom;

use App\Mail\ReportPrepared as Mailable;
use App\StudentReport;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class ReportPrepared extends Notification implements ShouldQueue
{
    use Queueable;

    public $report;
    public $pdf;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct( $report, $pdf )
    {
        $this->report = $report;
        $this->pdf = $pdf;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    // /**
    //  * Get the mail representation of the notification.
    //  *
    //  * @param  mixed  $notifiable
    //  * @return \Illuminate\Notifications\Messages\MailMessage
    //  */
    // public function toMail($notifiable)
    // {
    //     return ( new Mailable($this->report, $this->pdf) )->to( $notifiable->email );
    // }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $report = $this->report;
        $teacher = $report->teacher;
        $student = $report->student;

        $title = "<b>{$teacher->name}</b> prepared a report for <b>{$student->name}</b>.";

        return [
            'type' => StudentReport::class,
            'target' => $report,
            'title' =>  $title,
        ];
    }
}
