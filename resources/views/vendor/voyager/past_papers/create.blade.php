@extends('voyager::master')

@section('page_title', 'Past Papers')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_header')
<div class="container-fluid">
    <h1 class="page-title">
        Past Papers
    </h1>
</div>
@stop

@section('content')
    <div class="page-content container-fluid">
        <form class="form-edit-add" role="form"
              action="{{ route('voyager.past-papers.store') }}"
              method="POST" enctype="multipart/form-data" autocomplete="off">
            {{ csrf_field() }}

            <div class="panel panel-bordered">
                <div class="panel">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <div class="panel-body">
                        {{-- Images --}}
                        <div class="form-group">
                            <label for="files">Files</label>
                            <input type="file" multiple class="form-control" id="imgs" name="files[]" placeholder="Images">
                            @error('files')
                                <div class="error-message">{{ $message }}</div>
                            @enderror
                            <p class="gray2">Only IGCSE Past Papers can be uploaded for now.<br>
                                GT and ER files cannot be uploaded.</p>
                        </div>

                        {{-- Subject --}}
                        <div class="form-group">
                            <label>Subject</label>
                            <select class="form-control" name="subject_id">
                                @foreach($subjects as $subject)
                                    <option value="{{ $subject->id }}" {{ old('subject') == $subject->id ? 'selected="selected"' : '' }}>{{ $subject->name_with_exam_board }}</option>
                                @endforeach
                            </select>
                            @error('subject')
                                <span class="error-message">{{ $message }}</span>
                            @enderror
                        </div>

                    </div>
                </div>
            </div>

            <button type="submit" class="btn btn-primary pull-right save">
                {{ __('voyager::generic.save') }}
            </button>
        </form>
    </div>
@stop
