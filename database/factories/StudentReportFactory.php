<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Student;
use App\Teacher;
use App\StudentReport;
use Faker\Generator as Faker;

$factory->define(StudentReport::class, function (Faker $faker) {
    return [
        'student_id' => function() {
            return factory(Student::class)->create()->id;
        },
        'teacher_id' => function() {
            return factory(Teacher::class)->create()->id;
        },
        'teacher_remark' => $this->faker->sentence,
        'start_date' => $faker->dateTimeBetween('-2 weeks', 'now'),
        'end_date' => $faker->dateTimeBetween('now', '+2 weeks'),
    ];
});
