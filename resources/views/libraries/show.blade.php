@extends('main.layouts.app')

@section('meta')
<title>{{ $title }} | {{ config('app.name') }}</title>
@endsection

@section('content')
@include('main.navs.header')
<div class="py-2"></div>

<div class="container">
    <a href="{{ route('libraries.index') }}" class="gray2 font-size2 underline-on-hover">< Back to Libraries</a>

    <div class="py-1"></div>

    <div class="d-flex flex-wrap justify-content-between align-items-center border-bottom pb-2 mb-4">
        <h1 class="font-size9">{{ $title }}</h1>
        @if(isset($library) && !is_null($library))
            {{-- Delete Library --}}
            <form action="{{ route('libraries.delete', $library->id) }}" method="POST">
                @csrf
                @method('delete')
                <button type="button" class="button--danger deleteButton">
                    <span class="iconify" data-icon="fluent:delete-24-regular" data-inline="false"></span> Delete Library</button>
            </form>
        @endif
    </div>

    @if($has_resources)
        {{-- Past Papers --}}
        @if(isset($past_papers) && count($past_papers ?? []))
            <div class="mb-3">
                <h2 class="font-size4 font-weight-normal mt-2 mb-3">Past Papers</h2>
                    <div class="row">
                        @foreach($past_papers as $past_paper)
                            <div class="col-12 col-md-6 col-lg-3 mb-2">
                                @component('libraries.common.resource_block', ['library' => $library, 'resource' => $past_paper])
                                @endcomponent
                            </div>
                        @endforeach
                    </div>
            </div>
        @endif

        {{-- Topical Past Papers --}}
        @if(isset($topical_past_papers) && count($topical_past_papers ?? []))
            <div class="mb-3">
                <h2 class="font-size4 font-weight-normal mt-2 mb-3">Topical Past Papers</h2>
                <div class="row">
                    @foreach($topical_past_papers as $topical_past_paper)
                        <div class="col-12 col-md-6 col-lg-3">
                            @component('libraries.common.resource_block', ['library' => $library, 'resource' => $topical_past_paper])
                            @endcomponent
                        </div>
                    @endforeach
                </div>
            </div>
        @endif

        {{-- Notes --}}
        @if(isset($notes) && count($notes ?? []))
            <div class="mb-3">
                <h2 class="font-size4 font-weight-normal mt-2 mb-3">Notes</h2>
                <div class="row">
                        @if(isset($type) && !is_null($type))
                            @foreach($notes as $note)
                                <div class="col-12 col-md-6 col-lg-3 mb-2">
                                    @component('libraries.common.resource_block', ['type' => $type, 'resource' => $note])
                                    @endcomponent
                                </div>
                            @endforeach
                        @else
                            @foreach($notes as $note)
                                <div class="col-12 col-md-6 col-lg-3 mb-2">
                                    @component('libraries.common.resource_block', ['library' => $library, 'resource' => $note])
                                    @endcomponent
                                </div>
                            @endforeach
                        @endif
                </div>
            </div>
        @endif

        {{-- Guides --}}
        @if(isset($guides) && count($guides ?? []))
            <div class="mb-3">
                <h2 class="font-size4 font-weight-normal mt-2 mb-3">Guides</h2>
                <div class="row">
                    @foreach($guides as $guide)
                        <div class="col-12 col-md-6 col-lg-3 mb-2">
                            @component('libraries.common.resource_block', ['library' => $library, 'resource' => $guide])
                            @endcomponent
                        </div>
                    @endforeach
                </div>
            </div>
        @endif

        {{-- Custom Resource --}}
        {{-- <div class="mb-3">
            <h2 class="font-size4 font-weight-normal mt-2 mb-3">Custom Resource</h2>
            <div class="row">
                @foreach($library->getCustomResources() as $custom_resource)
                    <div class="col-12 col-md-6 col-lg-4">
                        @component('libraries.common.resource_block', ['library' => $library, 'custom_resource' => $custom_resource])
                        @endcomponent
                    </div>
                @endforeach
                <div class="col-12 col-md-6 col-lg-4">
                    Add New
                    <button type="button" data-toggle="modal" data-target="#addCustomResourceModal" class="panel position-relative scale-on-hover fast border-left-blue5-3px h-100">
                        <div class="d-flex flex-column justify-content-center align-items-center mt-1 mb-1">
                            <span class="iconify blue5 mb-2" data-icon="bi:plus-circle-fill" data-inline="false" style="font-size: 48px;"></span>
                            <h4 class="gray2 font-size2 mb-0">Upload Custom Resource</h4>
                        </div>
                    </button>
                </div>
            </div>
        </div> --}}
    @else
        <div class="text-center my-4">
            <img src="{{ asset('img/essential/empty.svg') }}" alt="No resources Found" width="220" height="220" class="mb-2">
            <p class="gray2">No resources found.<br>
                Start browsing resources on Board and save your favourite resources here!</p>
        </div>
    @endif

    {{-- Add Custom Resource Modal --}}
    {{-- <div class="modal" tabindex="-1" role="dialog">
        <form action="{{  }}"></form>
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Add Custom Resource</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                  <p>Modal body text goes here.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary">Save changes</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div> --}}

</div>
@endsection

@section('scripts')
@endsection
