<div class="d-md-none mobile-body-overlay">
    <nav class="animated slideInLeft faster mobile-nav-container">
        <a class="mobile-nav-brand header2__sidenav-logo" href="{{ url('/') }}">
            <img class="" src="{{ asset('img/logo/logo_light_'.$exam_board->slug.'.png') }}">
        </a>
        <section class="mb-2">
            <h3 class="heading">Discover</h3>
            <ul class="mobile-nav-menu">
                <li class="header2__sidenav-item position-relative mobile-nav-item {{ request()->is('*') || request()->is('questions') ? 'active' : '' }}">
                    <span class="mobile-nav-icon"><span class="iconify" data-icon="cil:newspaper" data-inline="false"></span></span>
                    <a class="stretched-link mobile-nav-link" href="{{ route('questions.index', $exam_board->slug) }}">News Feed</a>
                </li>
                <li class="header2__sidenav-item position-relative mobile-nav-item {{ request()->is('libraries') ? 'active' : '' }}">
                    <span class="mobile-nav-icon"><span class="iconify" data-icon="codicon:library" data-inline="false"></span></span>
                    <a class="stretched-link mobile-nav-link" href="{{ route('libraries.index') }}">My Library</a>
                </li>
                {{-- <li class="position-relative mobile-nav-item {{ request()->is('/my-resources')? 'active' : '' }}">
                    <span class="mobile-nav-icon"><i class="fas fa-box-open"></i></span>
                    <a class="stretched-link mobile-nav-link" href="{{ route('resources.show') }}">My Resources</a>
                </li> --}}
            </ul>
        </section>
        @if(Gate::allows('access-resource'))
            <section>
                <h3 class="heading">Resources</h3>
                <ul class="mobile-nav-menu">
                    <li class="header2__sidenav-item position-relative mobile-nav-item">
                        <span class="mobile-nav-icon"><span class="iconify" data-icon="carbon:stacked-scrolling-1" data-inline="false"></span></span>
                        <a class="stretched-link mobile-nav-link" href="{{ route('past_papers.home', $exam_board->slug) }}">Past Papers</a>
                    </li>
                    @if(Gate::allows('access-marketplace'))
                        <li class="header2__sidenav-item position-relative mobile-nav-item {{ request()->is('topical_past_papers')? 'active' : '' }}">
                            <span class="mobile-nav-icon"><span class="iconify" data-icon="carbon:category-new-each" data-inline="false"></span></span>
                            <a class="stretched-link mobile-nav-link" href="{{ route('topical_past_papers.home', $exam_board->slug) }}">Topical Worksheets
                                @if( setting('beta.marketplace') ) <span class="badge badge-primary">BETA</span> @endif
                            </a>
                        </li>
                        <li class="header2__sidenav-item position-relative mobile-nav-item {{ request()->is('notes')? 'active' : '' }}">
                            <span class="mobile-nav-icon"><span class="iconify" data-icon="mdi-light:note-text" data-inline="false"></span></span>
                            <a class="stretched-link mobile-nav-link" href="{{ route('notes.home', $exam_board->slug) }}">Notes
                                @if( setting('beta.marketplace') ) <span class="badge badge-primary">BETA</span> @endif
                            </a>
                        </li>
                    @endif
                    <li class="header2__sidenav-item position-relative mobile-nav-item {{ request()->is('notes')? 'active' : '' }}">
                        <span class="mobile-nav-icon"><span class="iconify" data-icon="carbon:notebook-reference" data-inline="false"></span></span>
                        <a class="stretched-link mobile-nav-link" href="{{ route('notes.home', $exam_board->slug) }}">Guides</a>
                    </li>
                    @if(Gate::allows('access-marketplace'))
                        @if(Gate::allows('upload-resource'))
                            <li class="header2__sidenav-item position-relative mobile-nav-item red">
                                <span class="mobile-nav-icon"><span class="iconify" data-icon="carbon:add-alt" data-inline="false"></span></span>
                                <a class="stretched-link mobile-nav-link" href="{{ route('notes.create', $exam_board->slug) }}">
                                    Add Resource
                                    @if( setting('beta.marketplace') ) <span class="badge badge-primary">BETA</span> @endif
                                </a>
                            </li>
                        @endif
                    @endif
                    @if(Gate::allows('access-exam-builder'))
                        <li class="header2__sidenav-item position-relative mobile-nav-item {{ request()->is('*/exam-builder') || request()->is('*/exam-builder/*') ? 'active' : '' }}">
                            <span class="mobile-nav-icon"><span class="iconify" data-icon="carbon:model-builder" data-inline="false"></span></span>
                            <a class="stretched-link mobile-nav-link" href="{{ route('exam_builder.landing').'?exam_board='.$exam_board->slug }}">
                                Exam Builder
                                <span class="badge badge-danger">NEW</span>
                            </a>
                        </li>
                    @endif
                </ul>
            </section>
        @endif
        <section>
            <h3 class="heading">More</h3>
            <ul class="mobile-nav-menu">
                {{-- <li class="position-relative mobile-nav-item">
                    <span class="mobile-nav-icon"><span class="iconify" data-icon="mdi:treasure-chest" data-inline="false"></span></span>
                    <a class="stretched-link mobile-nav-link" href="{{ route('physical_resources') }}">Physical Resources</a>
                </li> --}}
                {{-- Top Up Coins --}}
                @auth
                    @if( auth()->user()->hasCoins() )
                        <li class="header2__sidenav-item position-relative mobile-nav-item {{ request()->is('*/store') || request()->is('*/store/*') ? 'active' : '' }}">
                            <span class="mobile-nav-icon"><span class="iconify" data-icon="la:coins-solid" data-inline="false"></span></span>
                            <a class="stretched-link mobile-nav-link" href="{{ route('products.coins') }}">Top Up Coins</a>
                        </li>
                    @endif
                @else
                    <li class="header2__sidenav-item position-relative mobile-nav-item {{ request()->is('*/store') || request()->is('*/store/*') ? 'active' : '' }}">
                        <span class="mobile-nav-icon"><span class="iconify" data-icon="la:coins-solid" data-inline="false"></span></span>
                        <a class="stretched-link mobile-nav-link" href="{{ route('products.coins') }}">Top Up Coins</a>
                    </li>
                @endauth
                @if(Gate::allows('access-classroom'))
                    <li class="header2__sidenav-item position-relative mobile-nav-item {{ request()->is('*/classroom') || request()->is('*/classroom/*') ? 'active' : '' }}">
                        <span class="mobile-nav-icon"><span class="iconify" data-icon="la:chalkboard-teacher-solid" data-inline="false"></span></span>
                        <a class="stretched-link mobile-nav-link" href="{{ route('classroom.landing') }}">
                            Classroom
                            @if( setting('beta.classroom') ) <span class="badge badge-primary">BETA</span> @endif
                        </a>
                    </li>
                @endif
                <li class="header2__sidenav-item position-relative mobile-nav-item">
                    <span class="mobile-nav-icon"><span class="iconify" data-icon="subway:world" data-inline="false"></span></span>
                    <a class="stretched-link mobile-nav-link" href="{{ route('/') }}">Back to Board</a>
                </li>
            </ul>
        </section>

        <section class="pl-2">
            <a href="{{ route('about') }}" class="inactive">About</a> ·
            <a href="{{ route('faq') }}" class="inactive">FAQ</a> ·
            <a href="{{ route('contactUs.createEmail') }}" class="inactive">Contact Us</a> ·
            <a href="https://forms.gle/15wUg56eDbMAe43q6" target="_blank" class="inactive">Give a Feedback</a>
            {{-- <a href="https://bit.ly/board_made_tutor" target="_blank" class="inactive">Become a Tutor</a> --}}
        </section>

        <section class="nav-footer mobile-nav-footer py-2">
            © Board {{ now()->year }}
        </section>
    </nav>
</div>


<nav class="d-none d-md-block nav-container header__sidenav">
    <section class="mb-2">
        <h3 class="heading">Discover</h3>
        <ul class="nav-menu">
            <li class="position-relative nav-item {{ request()->is('*') || request()->is('questions')? 'active' : '' }}">
                <span class="nav-icon"><span class="iconify" data-icon="cil:newspaper" data-inline="false"></span></span>
                <a class="stretched-link nav-link" href="{{ route('questions.index', $exam_board->slug) }}">News Feed</a>
            </li>
            @if(Gate::allows('access-resource'))
                <li class="position-relative nav-item {{ request()->is('libraries')? 'active' : '' }}">
                    <span class="nav-icon"><span class="iconify" data-icon="codicon:library" data-inline="false"></span></span>
                    <a class="stretched-link nav-link" href="{{ route('libraries.index') }}">My Library</a>
                </li>
            @endif
            {{-- <li class="position-relative nav-item {{ request()->is('/my-resources')? 'active' : '' }}">
                <span class="nav-icon"><i class="fas fa-box-open"></i></span>
                <a class="stretched-link nav-link" href="{{ route('resources.show') }}">My Resources</a>
            </li> --}}
        </ul>
    </section>
    @if(Gate::allows('access-resource'))
        <section>
            <h3 class="heading">Resources</h3>
            <ul class="nav-menu">
                <li class="position-relative nav-item">
                    <span class="nav-icon"><span class="iconify" data-icon="carbon:stacked-scrolling-1" data-inline="false"></span></span>
                    <a class="stretched-link nav-link" href="{{ route('past_papers.home', $exam_board->slug) }}">Past Papers</a>
                </li>
                @if(Gate::allows('access-marketplace'))
                    <li class="position-relative nav-item {{ request()->is('*/topical-past-papers') || request()->is('*/topical-past-papers/*') ? 'active' : '' }}">
                        <span class="nav-icon"><span class="iconify" data-icon="carbon:category-new-each" data-inline="false"></span></span>
                        <a class="stretched-link nav-link" href="{{ route('topical_past_papers.home', $exam_board->slug) }}">
                            Topical Worksheets
                            @if( setting('beta.marketplace') ) <span class="badge badge-primary">BETA</span> @endif
                        </a>
                    </li>
                    <li class="position-relative nav-item {{ request()->is('*/notes') || request()->is('*/notes/*') ? 'active' : '' }}">
                        <span class="nav-icon"><span class="iconify" data-icon="mdi-light:note-text" data-inline="false"></span></span>
                        <a class="stretched-link nav-link" href="{{ route('notes.home', $exam_board->slug) }}">
                            Notes
                            @if( setting('beta.marketplace') ) <span class="badge badge-primary">BETA</span> @endif
                        </a>
                    </li>
                @endif
                <li class="position-relative nav-item {{ request()->is('*/guides') || request()->is('*/guides/*') ? 'active' : '' }}">
                    <span class="nav-icon"><span class="iconify" data-icon="carbon:notebook-reference" data-inline="false"></span></span>
                    <a class="stretched-link nav-link" href="{{ route('guides.home', $exam_board->slug) }}">Guides</a>
                </li>
                @if(Gate::allows('access-marketplace'))
                    @if(Gate::allows('upload-resource'))
                        <li class="position-relative nav-item red">
                            <span class="nav-icon"><span class="iconify" data-icon="carbon:add-alt" data-inline="false"></span></span>
                            <a class="stretched-link nav-link" href="{{ route('notes.create', $exam_board->slug) }}">
                                Add Resource
                                @if( setting('beta.marketplace') ) <span class="badge badge-primary">BETA</span> @endif
                            </a>
                        </li>
                    @endif
                @endif

                {{-- Exam Builder --}}
                @if(Gate::allows('access-exam-builder'))
                    <li class="position-relative nav-item">
                        <span class="nav-icon"><span class="iconify" data-icon="carbon:model-builder" data-inline="false"></span></span>
                        <a class="stretched-link nav-link" href="{{ route('exam_builder.landing').'?exam_board='.$exam_board->slug }}">
                            Exam Builder
                            <span class="badge badge-danger">NEW</span>
                        </a>
                    </li>
                @endif
            </ul>
        </section>
    @endif
    <section>
        <h3 class="heading">More</h3>
        <ul class="nav-menu">
            {{-- <li class="position-relative nav-item">
                <span class="nav-icon"><span class="iconify" data-icon="mdi:treasure-chest" data-inline="false"></span></span>
                <a class="stretched-link nav-link" href="{{ route('physical_resources') }}">Physical Resources</a>
            </li> --}}

            {{-- Classroom --}}
            @if(Gate::allows('access-classroom'))
                <li class="position-relative nav-item">
                    <span class="nav-icon"><span class="iconify" data-icon="la:chalkboard-teacher-solid" data-inline="false"></span></span>
                    <a class="stretched-link nav-link" href="{{ route('classroom.landing') }}">
                        Classroom
                        @if( setting('beta.classroom') ) <span class="badge badge-primary">BETA</span> @endif
                    </a>
                </li>
            @endif

            {{-- Top Up Coins --}}
            @auth
                @if( auth()->user()->hasCoins() )
                    <li class="position-relative nav-item">
                        <span class="nav-icon"><span class="iconify" data-icon="la:coins-solid" data-inline="false"></span></span>
                        <a class="stretched-link nav-link" href="{{ route('products.coins') }}">Top Up Coins</a>
                    </li>
                @endif
            @else
                <li class="position-relative nav-item">
                    <span class="nav-icon"><span class="iconify" data-icon="la:coins-solid" data-inline="false"></span></span>
                    <a class="stretched-link nav-link" href="{{ route('products.coins') }}">Top Up Coins</a>
                </li>
            @endif
            <li class="position-relative nav-item">
                <span class="nav-icon"><span class="iconify" data-icon="subway:world" data-inline="false"></span></span>
                <a class="stretched-link nav-link" href="{{ route('/') }}">Back to Board</a>
            </li>
        </ul>
    </section>

    <section class="pl-2">
        <a href="{{ route('about') }}" class="inactive">About</a> ·
        <a href="{{ route('faq') }}" class="inactive">FAQ</a> ·
        <a href="{{ route('contactUs.createEmail') }}" class="inactive">Contact Us</a> ·
        <a href="https://forms.gle/15wUg56eDbMAe43q6" target="_blank" class="inactive">Give a Feedback</a>
        {{-- <a href="https://bit.ly/board_made_tutor" target="_blank" class="inactive">Become a Tutor</a> --}}
    </section>

    <section class="nav-footer py-2">
        © Board {{ now()->year }}
    </section>
</nav>
