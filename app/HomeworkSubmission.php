<?php

namespace App;

use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class HomeworkSubmission extends Model
{
    protected $guarded = [];
    public $additional_attributes = ['marks_to_max_ratio'];
    protected $dates = ['marked_at', 'returned_at', 'created_at', 'updated_at'];

    public function homework()
    {
        return $this->belongsTo( Homework::class );
    }

    public function student()
    {
        return $this->belongsTo(Student::class, 'student_id', 'user_id');
    }

    /**
     * ------------------
     * Accessors
     * ------------------
     */

    /**
     * Returns true if submission has marks, else false
     * @return boolean
     */
    public function hasMarks()
    {
        return !is_null($this->marks);
    }

    /**
     * Returns submission marks in percentage
     * @return int|null
     */
    public function getMarksInPercentageAttribute()
    {
        // Returns null if submission doesn't have marks, else return submission mark in percentage
        if( !$this->hasMarks() ) {
            return null;
        } else {
            try{
                return round(($this->marks/$this->homework->max_marks) * 100, 2);
            } catch(\Exception $e) {
                return null;
            }
        }
    }

    /**
     * Returns absolute marks to max in (xx/xx) format, else return null
     * @return string|null
     */
    public function getAbsoluteMarksToMaxAttribute()
    {
        return $this->hasMarks() ? "{$this->marks}/{$this->homework->max_marks}"
                                 : null;
    }

    public function getFileUrlAttribute($value)
    {
        if($value){
            if(strpos($value, 'amazonaws')){
                //Amazon link for image
                return $value;
            } else{
                //Amazon S3
                $updated_value = str_replace('\\', '/', $value);
                return Storage::cloud()->url($updated_value);
            }
        } else {
            return null;
        }
    }

    public function getMarkedFileUrlAttribute($value)
    {
        if($value){
            if(strpos($value, 'amazonaws')){
                //Amazon link for image
                return $value;
            } else{
                //Amazon S3
                $updated_value = str_replace('\\', '/', $value);
                return Storage::cloud()->url($updated_value);
            }
        } else {
            return null;
        }
    }

    /**
     * Returns marked file relative url 'homework/submissions/xxx'
     * @return string
     */
    public function getFileRelativeUrlAttribute()
    {
        return $this->getOriginal('file_url');
    }

    /**
     * Returns marked file relative url 'homework/submissions/xxx'
     * @return string
     */
    public function getMarkedFileRelativeUrlAttribute()
    {
        return $this->getOriginal('marked_file_url');
    }


    /**
     * Update marked file on S3 and DB
     */
    public function saveFile( $file )
    {
        //Store image to S3
        $path = $file->store('homework/submissions/','s3');
        $filename = $file->hashName();

        $this->update([
            'file_url' => 'homework/submissions/'.$filename,
        ]);
    }

    /**
     * Update marked file on S3 and DB
     */
    public function saveMarkedFile( $file )
    {
        //Store image to S3
        $path = $file->store('homework/submissions/marked','s3');
        $filename = $file->hashName();

        $this->update([
            'marked_file_url' => 'homework/submissions/marked/'.$filename,
        ]);
    }

    /**
     * Returns true if submission is markable, else false
     * @return boolean
     */
    public function isMarkable()
    {
        return in_array( $this->status, ['overdue', 'submitted'] );
    }

    /**
     * Returns true if submission is marked, else returns false
     * @return boolean
     */
    public function isMarked()
    {
        return $this->status == 'marked';
    }

    /**
     * Returns true if submission is graded, else false
     * @return boolean
     */
    public function isGraded()
    {
        return $this->isMarked();
    }

    public function getStatusTagHtmlAttribute()
    {
        return '<span class="status-tag '.$this->status .'">'.ucfirst($this->status).'</span>';
    }
}
