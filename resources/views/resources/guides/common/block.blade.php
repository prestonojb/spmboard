<div class="panel resource__block">
    <h3 class="">
        <a href="{{ $guide->url }}" class="font-size-inherit blue-underline">
            <b>{{ $guide->title }}</b>
        </a>
    </h3>
    <h4 class="gray2 font-size2 font-weight-normal mb-0">{{ str_limit($guide->description, 80) }}</h4>
</div>
