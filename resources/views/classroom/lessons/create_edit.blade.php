@extends('classroom.layouts.app')

@section('meta')
    <title>{{ $is_edit ? 'Edit Lesson - '.$lesson->name : 'Create Lesson' }} | {{ config('app.name') }} Classroom</title>
@endsection

@section('styles')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.1.2/css/tempusdominus-bootstrap-4.min.css">
@endsection

@section('content')
@component('classroom.common.breadcrumbs')
    <a href="{{ route('classroom.classrooms.show', $classroom->id) }}">Dashboard</a> /
    <a href="{{ route('classroom.lessons.index', $classroom->id) }}">Lessons</a> /
    @if($is_edit)
        <a href="{{ route('classroom.lessons.show', [$lesson->classroom->id, $lesson->id]) }}">{{ $lesson->name }}</a> /
        Edit
    @else
        Add
    @endif
@endcomponent

@component('classroom.common.page_heading', ['title' => $is_edit ? 'Edit Lesson' : 'Add Lesson'])
@endcomponent

<div class="panel maxw-740px mx-auto">
    @component('classroom.lessons.common.form', ['classroom' => $classroom, 'lesson' => $lesson, 'is_edit' => $is_edit])
    @endcomponent
</div>

<div class="py-2"></div>
@endsection
