<?php

namespace App\Http\Controllers\Voyager;

use App\ExamBuilderItem;
use \TCG\Voyager\Http\Controllers\VoyagerBaseController as VoyagerBaseController;
use Illuminate\Http\Request;
use Exception;
use App\Chapter;
use App\Exports\ExamBuilderItemExport;
use App\Subject;
use TCG\Voyager\Traits\AlertsMessages;
use App\Rules\ValidExamBuilderItemFormat;

class VoyagerExamBuilderItemController extends VoyagerBaseController
{
    protected $matching_seasons = ['FM', 'MJ', 'ON'];
    protected $matching_years = ['_199', '_20'];
    protected $matching_paper_numbers = ['_01_', '_02_', '_03_', '_04_', '_05_', '_06_',
                                         '_11_', '_12_', '_13_', '_14_', '_15_', '_16_',
                                         '_21_', '_22_', '_23_', '_24_', '_25_', '_26_',
                                         '_31_', '_32_', '_33_', '_34_', '_35_', '_36_',
                                         '_41_', '_42_', '_43_', '_44_', '_45_', '_46_',
                                         '_51_', '_52_', '_53_', '_54_', '_55_', '_56_',
                                         '_61_', '_62_', '_63_', '_64_', '_65_', '_66_',
                                         '_1_', '_2_', '_3_', '_4_', '_5_', '_6_'];
    protected $matching_answer = '_MS';
    protected $matching_variants = ['(1)', '(2)', '(3)', '(4)', '(5)'];

    public function create(Request $request)
    {
        $subjects = Subject::all();
        return view('vendor.voyager.exam_builder.items.create', compact('subjects'));
    }


    public function store(Request $request)
    {
        $files = $request->file('imgs');
        $request->validate([
            'subject_id' => 'required',
            'chapter_id' => 'required',
            'imgs.*' => 'required|mimes:png',
            'imgs' => 'required'
        ]);
        // Reject .PNG files (conflict with .png files)
        foreach($files as $file) {
            $ext = $file->getClientOriginalExtension();
            if($ext != 'png') {
                return redirect()->back()->with(['alert-type' => 'error', 'message' => "No '.PNG' files allowed. Rename to '.png' instead."]);
            }
        }


        $subject = Subject::find($request->subject_id);
        $chapter = Chapter::find($request->chapter_id);

        // Logic to fill in columns based on filenames
        foreach($files as $file) {
            $file_name = $file->getClientOriginalName();

            // Get Season
            $item_season = $this->getFileSeason($file_name);
            if( is_null($item_season) ) {
                return redirect()->back()->with(['alert-type' => 'error', 'message' => "Failed to find season for file with filename {$file_name}. Please rename the file and try again!"]);
            }

            // Get Year
            $item_year = $this->getFileYear($file_name);
            if( is_null($item_year) ) {
                return redirect()->back()->with(['alert-type' => 'error', 'message' => "Failed to find year for file with filename {$file_name}. Please rename the file and try again!"]);
            }

            // Get Paper Number
            $item_paper_number = $this->getPaperNumber($file_name);
            if( is_null($item_paper_number) ) {
                return redirect()->back()->with(['alert-type' => 'error', 'message' => "Failed to find paper number for file with filename {$file_name}. Please rename the file and try again!"]);
            }

            // Get Name
            $item_name = $this->getName($file_name);
            $item_label_number = $this->getLabelNumber($file_name);

            // Check if is Question/Answer
            $is_answer = $this->isAnswer($file_name);

            $item = ExamBuilderItem::where('subject_id', $subject->id)
                                    ->where('chapter_id', $chapter->id)
                                    ->where('name', $item_name)
                                    ->first();

            $path = "exam-builder/{$subject->exam_board->slug}/{$subject->name}/";
            // Insert record into database if there are no more files associated with the item, else
            if( $item ) {
                // Update existing record to 'exam_builder_items' table
                $item->addImage($file, $path, $is_answer);
            } else {
                // Insert new record to 'exam_builder_items' table'
                ExamBuilderItem::create([
                    'name' => $item_name,
                    'subject_id' => $subject->id,
                    'chapter_id' => $chapter->id,
                    'label_number' => $item_label_number,
                    'season' => $item_season,
                    'year' => $item_year,
                    'paper_number' => $item_paper_number,
                    'question_imgs' => $is_answer ? null : json_encode( array($path.$file_name) ),
                    'answer_imgs' => $is_answer ? json_encode( array($path.$file_name) ) : null,
                ]);
                ExamBuilderItem::uploadImage($file, $path);
            }
        }
        return redirect('admin/exam-builder-items')->with(['alert-type' => 'success', 'message' => 'Exam Builder Questions/Answers updated successfully!']);
    }

    /**
     * Deletes item from database and files on S3
     * @param \Illuminate\Http\Request
     * @param int $id
     */
    public function destroy(Request $request, $id)
    {
        $ids = request('ids') ?? [];
        // Init array of IDs
        if (empty($id)) {
            // Bulk delete, get IDs from POST
            $ids = explode(',', $request->ids);
        } else {
            // Single item delete, get ID from URL
            $ids[] = $id;
        }

        $items = ExamBuilderItem::find($ids);
        foreach($items as $item) {
            $item->deleteAllImages();
            $item->delete();
        }

        return redirect('admin/exam-builder-items')->with(['alert-type' => 'success', 'message' => 'Exam Builder deleted successfully!']);
    }

    /**
     * Get Generate Exam Builder Item view
     */
    public function generate()
    {
        $subjects = Subject::whereHas('exam_builder_items')->get();
        return view('vendor.voyager.exam_builder.items.generate', compact('subjects'));
    }

    public function result()
    {
        $items = ExamBuilderItem::where('chapter_id', request('chapter_id'))->where('paper_number', request('paper_number'))->get();
        $items = $items->sortBy('is_complete');
        return view('vendor.voyager.exam_builder.items.result', compact('items'));
    }

    /**
     * Get export Exam Builder Item view
     */
    public function getExportView()
    {
        $subjects = Subject::whereHas('exam_builder_items')->get();
        return view('vendor.voyager.exam_builder.items.export', compact('subjects'));
    }

    /**
     * Export Exam Builder Item data in excel file
     */
    public function export()
    {
        $subject = Subject::find(request('subject_id'));
        return (new ExamBuilderItemExport)->forSubject($subject)->download("exam_builder_{$subject->exam_board_and_name}.xlsx");
    }

    /**
     * Returns Season from file name, else return null
     * @param string $file_name
     * @return string|null
     */
    public function getFileSeason($file_name)
    {
        // Get Season
        foreach($this->matching_seasons as $season) {
            // $season = Algebra_***FM***_2016_42_Q1(1).JPEG;
            $start_pos = strpos($file_name, $season);
            if($start_pos !== false) {
                // $file_season = Algebra_***FM***_2016_42_Q1(1).JPEG;
                return substr($file_name, $start_pos, strlen($season));
            }
        }
        return null;
    }

    /**
     * Returns Year from file name, else return null
     * @param string $file_name
     * @return string|null
     */
    public function getFileYear($file_name)
    {
        foreach($this->matching_years as $year) {
            // $year = Algebra_FM***_20***16_42_Q1(1).JPEG;
            $start_pos = strpos($file_name, $year);

            if($start_pos !== false) {
                // $file_year = Algebra_FM_***2016***_42_Q1(1).JPEG;
                return substr($file_name, $start_pos + 1, 4);
            }
        }
        return null;
    }

    /**
     * Returns paper number from file name, else return null
     * @param string $file_name
     * @return string|null
     */
    public function getPaperNumber($file_name)
    {
        foreach($this->matching_paper_numbers as $paper_number) {
            // $paper_number = Algebra_FM_2016***_42_***Q1(1).JPEG;
            $start_pos = strpos($file_name, $paper_number);
            $len = 1;

            if($start_pos !== false) {
                // $file_paper_number = Algebra_FM_2016_***4***2_Q1(1).JPEG;
                $paper_number = substr($file_name, $start_pos + 1, $len);
                if ($paper_number == 0) {
                    // $file_paper_number = Algebra_FM_2016_0***4***_Q1(1).JPEG;
                    return substr($file_name, $start_pos + 2, $len);
                } else {
                    return $paper_number;
                }
            }
        }
        return null;
    }

    /**
     * Returns name from file name
     * @param string $file_name
     * @return string
     */
    public function getName($file_name)
    {
        $len = strlen($file_name);
        if( $this->isAnswer($file_name) ) {
            // $name = ***Algebra_FM_2016_42_Q1***_MS(1).JPEG;
            $name = substr($file_name, 0, -($len - strpos($file_name, $this->matching_answer)));

        } else {
            // $name = ***Algebra_FM_2016_42_Q1***(1).JPEG;

            // Position of file format(".JPEG")
            $pos = strpos($file_name, '.');
            // $name = ***Algebra_FM_2016_42_Q1(1)***.JPEG;
            $name = substr($file_name, 0, -($len - $pos));

            // Remove image variants(["(1)", "(2)", "(3)")
            foreach( $this->matching_variants as $variant ) {
                if( strpos($name, $variant) !== false ) {
                    $name = substr( $name, 0, strpos($name, $variant) );
                }
            }
        }
        return str_replace('_', ' ', $name);
    }

    /**
     * Returns label number from file name
     * @var string $file_name
     * @return int
     */
    public function getLabelNumber($file_name)
    {
        // $number = Algebra_FM_2016_42_Q***1***(1).JPEG;
        $name = $this->getName($file_name);
        // See https://stackoverflow.com/questions/4636166/only-variables-should-be-passed-by-reference
        $tmp = explode(' ', $name);
        // $label in format "QXX"
        $label = end( $tmp );
        return (int) substr($label, 1);
    }

    public function isAnswer($file_name)
    {
        return strpos($file_name, $this->matching_answer) !== false;
    }
}
