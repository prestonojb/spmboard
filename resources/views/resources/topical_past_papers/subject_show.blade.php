@extends('resources.layouts.app')
@section('meta')
    <title>{{ $subject->name }} - Topical Worksheets | {{ $exam_board->name }} {{ config('app.name') }} </title>
@endsection

@section('header')
    @component('resources.common.header', ['type' => 'topical_past_paper', 'exam_board' => $exam_board])
    @endcomponent
@endsection

@section('breadcrumbs')
    @component('resources.common.breadcrumbs')
        <a href="{{ route('topical_past_papers.home', $exam_board->slug) }}">Home</a> /
        {{ $subject->name }}
    @endcomponent
@endsection

@section('context_panel')
    @component('resources.common.context_panel', ['exam_board' => $exam_board->name, 'title' => $subject->name, 'description' => $subject->description])
        <h3 class="font-size3">
            Part of <a href="{{ route('topical_past_papers.home', [$subject->exam_board->slug]) }}" class="font-size-inherit blue-underline">{{ $subject->exam_board->name }}</a>
        </h3>
    @endcomponent
@endsection

@section('body')
    {{-- Filter --}}
    @include('resources.topical_past_papers.common.filter')

    @if(count($topical_past_papers ?? []))
        <p class="gray2">Showing {{ $topical_past_papers->firstItem() }}-{{ $topical_past_papers->lastItem() }} of {{ $topical_past_papers->total() }} results</p>
        <div class="form-row">
            {{-- Note list --}}
            @foreach($topical_past_papers as $topical_past_paper)
                <div class="col-6 col-md-4 col-lg-3 mb-2">
                    @component('resources.common.panel', ['resource' => $topical_past_paper->resource])
                    @endcomponent
                </div>
            @endforeach
        </div>

        {{ $topical_past_papers->links() }}
    @else
        <div class="py-3 text-center">
            <img src="{{ asset('img/essential/empty.svg') }}" alt="" width="300" height="300">
            <p class="mb-0">No topical worksheets available</p>
        </div>
    @endif
@endsection
