<?php

namespace App\Http\Controllers;

use App\User;
use App\Answer;
use App\ExamBoard;
use App\Question;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Group;
use App\Mail\EbooksPromotion;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use Session;

class UserController extends Controller
{
    public function show(User $user)
    {
        $answers = Answer::allAnswersForUser($user, 10);
        return view('forum.users.profile.answers', compact('user', 'answers'));
    }

    public function questions(User $user)
    {
        $questions = Question::allQuestionsForUser($user, 10);
        return view('forum.users.profile.questions', compact('user', 'questions'));
    }

    public function notes(User $user)
    {
        $notes = $user->owned_notes()->latest()->paginate(15);
        return view('forum.users.profile.notes', compact('user', 'notes'));
    }

    public function accountSettings(User $user)
    {
        // Update username if is different from existing one
        if(request('username') != $user->username){
            request()->validate([
                'username' => 'required|unique:users'
            ]);

            $user->update([
                'username' => request('username'),
            ]);
        }

        return back()->withSuccess('Account updated successfully!');
    }

    /**
     * User switch account type
     */
    public function switchAccountType(User $user)
    {
        $existing_account_type = $user->account_type;
        $new_account_type = request('account_type');

        if( $existing_account_type == $new_account_type ) {
            return redirect()->back()->with('error', 'New account type cannot be the same as existing account type.');
        }
        // Switch user account type from existing to new
        $user->switchAccountType( $existing_account_type, $new_account_type );

        // Refresh User
        $user = User::find($user->id);
        Session::flash('success', "The existing {$existing_account_type} account attached to this email is replaced with a new {$new_account_type} account!");
        return redirect($user->getGettingStartedPath());
    }
}
