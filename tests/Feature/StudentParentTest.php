<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;
use App\ExamBoard;
use App\State;
use App\StudentParent;
use App\User;

class StudentParentTest extends TestCase
{
    use WithFaker;

    public function setUp() : void
    {
        parent::setUp();

        // Teacher
        $user = factory(User::class)->states('parent')->create();
        $this->parent = $user->parent;
    }

    /**
     * @group parent
     * Test update parent profile
     */
    public function testUpdate()
    {
        Storage::fake('s3');

        // // Update arguments
        // $image = UploadedFile::fake()->image('avatar.png');

        // Data for POST request
        $name = $this->faker->word;
        $phone_number = $this->faker->phoneNumber;

        // POST request to update profile
        $response = $this->actingAs($this->parent->user)
                        ->patch( route('parent.update'), [
                            'name' => $name,
                            'phone_number' => $phone_number,
                        ]);

        // Refresh student model after update
        $this->parent = StudentParent::find( $this->parent->user_id );

        // //Current Month and Year
        // $my = date('FY', time());
        // $path = 'users/'. $my.'/'.$image->hashName();

        // // Assert student avatar not updated
        // Storage::disk('s3')->assertExists( $path, "Student 'avatar' image is not uploaded!" );
        // $this->assertEquals( $this->student->avatar_relative_url, $path, "Student 'avatar' is not updated!" );

        // Assert teacher is already updated
        $this->assertEquals( $this->parent->name, $name, "Parent 'name' is not updated!" );
        $this->assertEquals( $this->parent->phone_number, $phone_number, "Parent 'phone_number' is not updated!" );

        // Assert redirect
        $response->assertRedirect();
    }
}
