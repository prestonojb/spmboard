<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTopicalPastPapersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('topical_past_papers', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('subject_id');
            $table->unsignedInteger('chapter_id');
            $table->string('name'); // 2017 MRSM Paper 2 
            $table->unsignedInteger('paper_number');
            $table->string('qp_url')->nullable();
            $table->string('ms_url')->nullable();
            $table->unsignedInteger('coin')->default(2);
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('topical_past_papers');
    }
}
