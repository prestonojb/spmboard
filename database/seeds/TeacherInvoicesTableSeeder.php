<?php

use App\Teacher;
use Carbon\Carbon;
use App\TeacherInvoice;
use App\TeacherInvoiceItem;
use Illuminate\Database\Seeder;

class TeacherInvoicesTableSeeder extends Seeder
{
    protected $NO_OF_INVOICES = 10;
    protected $MIN_NO_OF_REPORT_ITEMS = 1;
    protected $MAX_NO_OF_REPORT_ITEMS = 4;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TeacherInvoice::truncate();
        TeacherInvoiceItem::truncate();
        $faker = \Faker\Factory::create();

        for($i = 0; $i < $this->NO_OF_INVOICES; $i++) {
            $teacher = Teacher::inRandomOrder()->first();
            $student = $teacher->students->random();

            $lesson_ids = $teacher->getUnpaidLessonIds( $student );

            if( count($lesson_ids ?? []) ) {
                // Create invoice and invoice items
                // Generate teacher's next invoice number
                $setting = $teacher->createOrGetInvoiceSetting();
                $invoice_number = $setting->getNextInvoiceNumber( true );

                $due_at = Carbon::createFromTimeStamp($faker->dateTimeBetween('now', '+1 month')->getTimestamp());
                $paid_at = rand(0, 1) ? $due_at->copy()->addDays( $faker->numberBetween( 1, 50 ) ) : null;

                // Generate invoice
                $invoice = TeacherInvoice::create([
                                'invoice_number' => $invoice_number,
                                'teacher_id' => $teacher->user_id,
                                'student_id' => $student->user_id,
                                'amount' => 0,
                                'due_at' => $due_at,
                                'paid_at' => $paid_at,
                            ]);


                // Generate invoice items and attach to invoice
                $no_of_items = rand($this->MIN_NO_OF_REPORT_ITEMS, $this->MAX_NO_OF_REPORT_ITEMS);
                for( $i=0; $i < $no_of_items; $i++ ) {
                    // 1/4 chance to be LESSON item
                    if( rand(0, 3) ) {
                        $lesson_id = $lesson_ids[ array_rand($lesson_ids) ];
                        $title = 'Lesson(per hour)';
                        $no_of_units = 1;
                    } else {
                        $lesson_id = null;
                        $title = $faker->sentence(3);
                        $no_of_units = rand(1, 3);
                    }
                    // Create report items
                    $invoice->items()->create([
                        'lesson_id' => $lesson_id, // Pop last value from lesson IDs array
                        'title' => $title,
                        'price_per_unit' => rand(0, 100),
                        'no_of_units' => $no_of_units,
                    ]);
                }

                // Update invoice 'amount' column
                $invoice->update([
                    'amount' => $invoice->calculateAmount(),
                ]);
            }
        }
    }
}
