<?php

namespace App\Policies;

use App\Student;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class StudentPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function classroom_index(User $user)
    {
        return $user->teacher;
    }

    /**
     * Permission to show student profile in class
     */
    public function show(User $user, Student $student)
    {
        if( $user->is_teacher() ) {
            return $user->teacher->hasStudent($student);
        } elseif( $user->is_student() ) {
            return $user->id == $student->user_id;
        } elseif( $user->is_parent() ) {
            return $user->parent->hasChild($student);
        }
    }

    public function view_progress(User $user)
    {
        return $user->student;
    }

    public function view_reports(User $user) {
        return $user->student;
    }

    public function view_children(User $user) {
        return $user->parent;
    }
}
