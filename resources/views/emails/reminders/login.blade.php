@component('mail::message')
# Hi {{ $user->username }}, we appreciate your time on Board!

@component('mail::panel')
You are on a <b>{{ $user->consecutive_login_days }} day login streak</b>! Continue your streak by logging in today and claim your
@if( is_integer( $reward[ $user->reward_day - 1 ] ) )
<b>{{ $reward[ $user->reward_day - 1 ] }} Reward Points</b>!
@else
<b>{{ $reward[ $user->reward_day - 1 ] }}</b>!
@endif
@endcomponent

**You can spend Reward Points on getting help for your homework, redeem coins for resources and many more!*

Hope to see you on Board!

@component('mail::button', ['url' => url('login'), 'color' => 'primary'])
Log In Now
@endcomponent

Regards,<br>
The Board Team

@endcomponent
