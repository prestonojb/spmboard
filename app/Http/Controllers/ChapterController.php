<?php

namespace App\Http\Controllers;

use App\Chapter;
use App\ExamBoard;
use App\Question;
use Illuminate\Http\Request;
use CyrildeWit\EloquentViewable\Support\Period;

class ChapterController extends Controller
{
    public $valid_sorts = array('hot', 'top', 'new', 'answered');

    public function show(ExamBoard $exam_board, Chapter $chapter)
    {
        $sort = request('sort');
        if(!is_null($sort) && !in_array($sort, $this->valid_sorts)) {
            $sort = 'hot';
        }

        $questions = Question::sortForChapter($chapter);
        return view('forum.questions.index', compact('exam_board', 'chapter', 'questions', 'sort'));
    }
}
