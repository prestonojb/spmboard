@extends('voyager::master')

@section('page_title', 'Certification Application (Teacher)')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_header')
    <h1>Certification Applications</h1>
@endsection

@section('content')

@component('certification_applications.teacher.index', ['applications' => $applications, 'is_admin_view' => true])
@endcomponent

@stop

@section('javascript')
<script>
$(function(){
    $('.actionButton').click(function(e){
        e.preventDefault();
        var adminComment = $('textarea[name=admin_comment]').val();
        var $form = $(this).closest('form');
        $form.find('[name=admin_comment]').val(adminComment);

        $form.submit();
    });
});
</script>
@stop
