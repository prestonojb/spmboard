<?php

use App\ExamBoard;
use App\Chapter;
use App\Subject;
use Illuminate\Database\Seeder;

class ExamBoardsTableSeeder extends Seeder
{
    protected $NO_OF_SUBJECTS_PER_BOARD = 5;
    protected $NO_OF_CHAPTERS_PER_SUBJECT = 15;
    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        \DB::table('exam_boards')->delete();
        Subject::truncate();
        Chapter::truncate();

        \DB::table('exam_boards')->insert(array (
            0 =>
            array (
                'id' => 1,
                'name' => 'SPM',
                'slug' => 'spm',
                'created_at' => '2020-05-20 15:04:45',
                'updated_at' => '2020-05-20 15:04:45',
            ),
            1 =>
            array (
                'id' => 2,
                'name' => 'IGCSE',
                'slug' => 'igcse',
                'created_at' => '2020-05-20 15:04:54',
                'updated_at' => '2020-05-20 15:04:55',
            ),
        ));

        $faker = \Faker\Factory::create();

        $exam_boards = ExamBoard::all();
        foreach($exam_boards as $exam_board) {
            // Generate subjects
            for($i = 0; $i < $this->NO_OF_SUBJECTS_PER_BOARD; $i++) {
                $exam_board->subjects()->create([
                    'name' => $faker->word,
                    'description' => $faker->sentence,
                    'icon' => '<span class="iconify" data-icon="la:cube-solid" data-inline="false"></span>',
                ]);
            }

            // Generate chapters
            $subjects = $exam_board->subjects;
            foreach($subjects as $subject) {
                for($i = 0; $i < $this->NO_OF_CHAPTERS_PER_SUBJECT; $i++) {
                    $subject->chapters()->create([
                        'name' => $faker->sentence(2),
                        'description' => $faker->sentence,
                        'chapter_number' => $i,
                    ]);
                }
            }
        }
    }
}
