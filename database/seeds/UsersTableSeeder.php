<?php

use App\ExamBoard;
use App\Question;
use App\StudentParent;
use App\Student;
use App\Teacher;
use App\User;
use Faker\Provider\bg_BG\PhoneNumber;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    protected $NO_OF_TEACHERS = 1;
    protected $NO_OF_STUDENTS = 10;
    protected $NO_OF_PARENTS = 1;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        User::truncate();
        Teacher::truncate();
        Student::truncate();
        StudentParent::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        /**
         * Seeds users first, then attach the users to different account types(student, teacher, parent)
         */

        $faker = \Faker\Factory::create();
        $genders = ['M', 'F'];

        for($i = 0; $i < $this->NO_OF_TEACHERS; $i++) {
            $user = User::create([
                    'username' => 'teacher'.strval($i),
                    'email' => 'teacher'.strval($i).'@gmail.com',
                    'password' => bcrypt('asdasdasd'),
                ]);
            $user->teacher()->create([
                'phone_number' => $faker->phoneNumber,
                'gender' => $genders[ rand(0,1) ],
                'school' => $faker->company,
                'year_of_birth' => rand(1940, 2000),
                'qualification' => $faker->sentence,
                'teaching_experience' => rand(0, 20),
            ]);
        }

        for($i = 0; $i < $this->NO_OF_PARENTS; $i++) {
            $user = User::create([
                    'username' => 'parent'.strval($i),
                    'email' => 'parent'.strval($i).'@gmail.com',
                    'password' => bcrypt('asdasdasd'),
                ]);
            $user->parent()->create([
                'phone_number' => $faker->phoneNumber,
            ]);
        }


        for($i = 0; $i < $this->NO_OF_STUDENTS; $i++) {
            $user = User::create([
                    'username' => 'student'.strval($i),
                    'email' => 'student'.strval($i).'@gmail.com',
                    'password' => bcrypt('asdasdasd'),
                ]);

            // 3/4 chance to have a parent account attached
            $parent = rand(0, 4) ? StudentParent::inRandomOrder()->first() : null;
            $user->student()->create([
                'coin_balance' => rand(0, 50),
                'reward_point_balance' => rand(0, 50),
                'school' => $faker->company,
                'parent_id' => $parent ? $parent->id : null,
                'exam_board_id' => ExamBoard::inRandomOrder()->first() ? ExamBoard::inRandomOrder()->first()->id : null,
            ]);
        }

        Teacher::first()->user->makeAdmin();
        Student::first()->user->makeAdmin();
        StudentParent::first()->user->makeAdmin();
    }
}
