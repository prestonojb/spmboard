<div class="modal" tabindex="-1" role="dialog" id="bugReportModal">
    <form action="{{ route('bug_reports.store') }}" method="post">
        @csrf
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Submit Bug Report</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    {{-- Tag --}}
                    <div class="form-field">
                        <label for="">Tag</label>
                        <select name="tag" required>
                            <option value="" disabled selected>Select tag</option>
                            @foreach(\App\BugReport::getTags() as $key=>$value)
                                <option value="{{ $key }}" @if($tag == $key) selected @endif>{{ $value }}</option>
                            @endforeach
                        </select>
                        @error('tag')
                            <p class="error-message">{{ $message }}</span>
                        @enderror
                    </div>
                    {{-- Description --}}
                    <div class="form-field">
                        <label for="">Description</label>
                        <textarea name="description" rows="3" placeholder="Include description of your report (if any)" required>{{ $description }}</textarea>
                        @error('description')
                            <p class="error-message">{{ $message }}</span>
                        @enderror
                    </div>

                    {{-- Email --}}
                    <div class="form-field">
                        <label for="">Email (Optional)</label>
                        <input type="email" name="email" value="{{ $email }}" placeholder="Email">
                        @error('email')
                            <p class="error-message">{{ $message }}</p>
                        @enderror
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="button--primary--inverse" data-dismiss="modal">Close</button>
                    <button type="submit" class="button--primary bugReportSubmitButton" style="width: 78px;">Submit</button>
                </div>
            </div>
        </div>
    </form>
</div>
