@extends('classroom.layouts.app')

@section('content')
@component('classroom.common.breadcrumbs')
    <a href="{{ route('classroom.landing') }}">Home</a> /
    <a href="{{ route('classroom.students.index') }}">Students</a> /
    <a href="{{ route('classroom.students.show', $student->user_id) }}">{{ $student->name }}</a> /
    Reports
@endcomponent

@component('classroom.common.page_heading', ['title' => 'Reports'])
@endcomponent

<div class="mb-2">
    @include('classroom.students.common.details')
</div>

<div class="table-responsive">
    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Report Period</th>
            <th scope="col">Remarks</th>
            <th scope="col">Created At</th>
            <th scope="col">Action</th>
        </tr>
        </thead>
        <tbody>
            <h3><b>Reports</b></h3>
            @foreach( $reports as $report )
                <tr>
                    <th scope="row">{{ $report->id }}</th>
                    <td>{{ $report->start_date->toFormattedDateString() }} - {{ $report->end_date->toFormattedDateString() }}</td>
                    <td>{{ $report->remark ?? 'N/A' }}</td>
                    <td>{{ $report->created_at->toFormattedDateString() }}</td>
                    <td>
                        <div class="d-flex">
                            <a href="{{ $report->url }}" target="_blank" class="button--primary mr-1">View PDF</a>
                            <form action="{{ route('classroom.students.reports.share', [$classroom->id, $report->id]) }}" method="post">
                                @csrf
                                <button class="button--danger mr-1 shareToParentButton" @if(!$student->has_parent()) disabled @endif data-shared-to-parent="{{ $report->shared_to_parent }}">Share with Parents</button>
                            </form>
                            <div class="dropdown">
                                <button class="" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <span class="iconify" data-icon="eva:more-horizontal-outline" data-inline="false"></span>
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <form action="{{ route('classroom.students.reports.destroy', [$classroom->id, $report->id]) }}" method="post">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="dropdown-item text-danger">Delete</button>
                                    </form>
                                </div>
                        </div>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>

    {{ $reports->links() }}
</div>


@endsection

@section('scripts')
<script>
$(function(){
   $('.shareToParentButton').click(function(e){
        e.preventDefault();
        var form = $(this).closest('form');
        if( $(this).data('shared-to-parent') == "1" ) {
            Swal.fire({
                title: 'Are you sure to resend this report?',
                text: "You have shared this report to the student's parent before!",
                type: 'warning',
                showCancelButton: true,
                reverseButtons: true,
                confirmButtonText: 'Confirm',
                cancelButtonText: 'Cancel',
            }).then(function(result){
                if(result.value){
                    form.submit();
                }
            });

        } else {
            $(this).attr('disabled', true);
            form.submit();
        }


   });
});
</script>
@endsection
