<?php

namespace App\Imports;

use App\Mail\AccountCredentials;
use App\Mail\SetupGuide;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\User;
use App\StudentParent;
use App\Student;

class StudentAndParentImport implements ToCollection, WithHeadingRow, WithValidation
{
    public function __construct()
    {
        // [["user" => \App\User instance, "account_type" => "student"], ...]
        $this->setupGuideData = [];
        // [["user" => \App\User instance, "password" => "asdasdasd"], ...]
        $this->accountCredentialsData = [];
    }

    /**
    * @param Collection $rows
    */
    public function collection(Collection $rows)
    {
        // $row[0] => student_email
        // $row[1] => parent_email
        foreach ($rows as $row)
        {
            // Create student
            // Get student credentials
            $student_email = $row->get('student_email');
            // Create student account if doesn't exist, else get student account
            $student_user = $this->createOrGetStudent($student_email);

            // Create Parent if parent email is included
            if( !empty($row['parent_email']) ) {
                // Get parent credentials
                $parent_email = $row->get('parent_email');
                $parent_user = $this->createOrGetParent($parent_email);

                // Update student parent
                if( $student_user->account_type == 'student' && $parent_user->account_type == 'parent' ) {
                    $student_user->student->updateParent($parent_user->parent);
                }
            }
        }
        $this->sendAccountCredentials();
        $this->sendSetupGuide();
    }

    /**
     * Create student account and send account credentials if account with email does not exist, else send email notification to update account status
     * @return App\User
     */
    public function createOrGetStudent($student_email)
    {
        $student_exists = User::exists($student_email);
        if( !$student_exists ) {
            $student_username = User::generateUsernameFromEmail( $student_email );
            $student_password = str_random(8);

            // Create student user
            $student_user = $this->createStudentUser($student_username, $student_email, $student_password);

            // Push data to account credentials data
            $data = [
                'user' => $student_user,
                'password' => $student_password,
            ];
            array_push( $this->accountCredentialsData, $data );
        } else {
            $student_user = User::where('email', $student_email)->first();

            // Push data to account credentials data
            $data = [
                'user' => $student_user,
                'account_type' => 'student',
            ];
            array_push( $this->setupGuideData, $data );
        }
        return $student_user;
    }

    /**
     * Create parent account and send account credentials if account with email does not exist, else send email notification to update account status
     * @return App\User
     */
    public function createOrGetParent($parent_email)
    {
        $parent_exists = User::exists($parent_email);

        if( !$parent_exists ) {
            $parent_username = User::generateUsernameFromEmail( $parent_email );
            $parent_password = str_random(8);

            $parent_user = $this->createParentUser($parent_username, $parent_email, $parent_password);

            // Push data to account credentials data
            $data = [
                'user' => $parent_user,
                'password' => $parent_password,
            ];
            array_push( $this->accountCredentialsData, $data );
        } else {
            $parent_user = User::where('email', $parent_email)->first();
            // Push data to account credentials data
            $data = [
                'user' => $parent_user,
                'account_type' => 'parent',
            ];
            array_push( $this->setupGuideData, $data );
        }
        return $parent_user;
    }

    public function rules(): array
    {
        return [
            'student_email' => 'required|email',
            'parent_email' => 'nullable|email',
        ];
    }

    /**
     * Create User with credentials
     * @param $username
     * @param $email
     * @param $password
     */
    public function createUser($username, $email, $password)
    {
        // Create User
        return User::create([
            'username' => $username,
            'email' => $email,
            'password' => Hash::make($password),
        ]);
    }

    /**
     * Create Student User with credentials
     * @param $username
     * @param $email
     * @param $password
     */
    public function createStudentUser($username, $email, $password)
    {
        $user = $this->createUser($username, $email, $password);
        $user->createAsStudent();
        return User::find($user->id);
    }

    /**
     * Create Parent User with credentials
     * @param $username
     * @param $email
     * @param $password
     */
    public function createParentUser($username, $email, $password)
    {
        // Create User for Parent
        $user = $this->createUser($username, $email, $password);
        $user->createAsParent();
        return User::find($user->id);
    }

    /**
     * Send account credentials to user via email
     * @param App\User $user
     * @param string $password
     */
    public function sendAccountCredentials()
    {
        foreach($this->accountCredentialsData as $data) {
            $user = $data['user'];
            $password = $data['password'];
            Mail::to( $user->email )->send( new AccountCredentials( $user, $password ) );
        }
    }

    /**
     * Send setup guide to user via email
     * @param App\User $user
     * @param string $account_type
     */
    public function sendSetupGuide()
    {
        foreach($this->setupGuideData as $data) {
            $user = $data['user'];
            $account_type = $data['account_type'];
            Mail::to( $user->email )->send( new SetupGuide( $user, $account_type ) );
        }
    }
}
