<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\UploadedFile;
use Tests\TestCase;
use App\ExamBoard;
use App\State;
use App\Teacher;
use App\User;

class TeacherTest extends TestCase
{
    use WithFaker;

    public function setUp() : void
    {
        parent::setUp();

        // Teacher
        $user = factory(User::class)->states('teacher')->create();
        $this->teacher = $user->teacher;
    }

    /**
     * @group teacher
     * Test update teacher profile
     */
    public function testUpdate()
    {
        Storage::fake('s3');

        // // Update arguments
        // $image = UploadedFile::fake()->image('avatar.png');

        // Data for POST request
        $name = $this->faker->word;
        $state_id = State::first()->id;
        $school = $this->faker->sentence;
        $phone_number = $this->faker->phoneNumber;
        $qualification = $this->faker->sentence;

        $genders = ['M', 'F'];
        $gender = $genders[ array_rand($genders) ];

        $year_of_birth = floor( rand(1950, 2000) );
        $teaching_experience = floor( rand(0, 20) );

        // POST request to update profile
        $response = $this->actingAs($this->teacher->user)
                        ->patch( route('teacher.update'), [
                            'name' => $name,
                            'state' => $state_id,
                            'phone_number' => $phone_number,
                            'qualification' => $qualification,
                            'gender' => $gender,
                            'year_of_birth' => $year_of_birth,
                            'teaching_experience' => $teaching_experience,
                            'school' => $school,
                        ]);

        // Refresh student model after update
        $this->teacher = Teacher::find( $this->teacher->user_id );

        // //Current Month and Year
        // $my = date('FY', time());
        // $path = 'users/'. $my.'/'.$image->hashName();

        // // Assert student avatar not updated
        // Storage::disk('s3')->assertExists( $path, "Student 'avatar' image is not uploaded!" );
        // $this->assertEquals( $this->student->avatar_relative_url, $path, "Student 'avatar' is not updated!" );

        // Assert teacher is already updated
        $this->assertEquals( $this->teacher->name, $name, "Teacher 'name' is not updated!" );
        $this->assertEquals( $this->teacher->state->id, $state_id, "Teacher 'state' is not updated!" );
        $this->assertEquals( $this->teacher->phone_number, $phone_number, "Teacher 'phone_number' is not updated!" );
        $this->assertEquals( $this->teacher->qualification, $qualification, "Teacher 'qualification' is not updated!" );
        $this->assertEquals( $this->teacher->gender, $gender, "Teacher 'gender' is not updated!" );
        $this->assertEquals( $this->teacher->year_of_birth, $year_of_birth, "Teacher 'year_of_birth' is not updated!" );
        $this->assertEquals( $this->teacher->teaching_experience, $teaching_experience, "Teacher 'teaching_experience' is not updated!" );
        $this->assertEquals( $this->teacher->school, $school, "Teacher 'school' is not updated!" );

        // Assert redirect
        $response->assertRedirect();
    }
}
