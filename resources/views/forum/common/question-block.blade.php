<div class="panel question--block position-relative mb-3 @if($question->is_question_of_the_day) border-gold border-width3px @endif">

    <div class="question-block__header">
        @component('questions.common.tags', ['question' => $question])
        @endcomponent

        @component('questions.common.icon_group', ['question' => $question])
        @endcomponent
    </div>

    <div class="question-block__user-info">
        @if( !is_null($question->user) && !$question->anonymous )
            <a href="{{ route('users.show', $question->user->username) }}">
                <img class="avatar" src="{{ $question->user->avatar ?? asset('img/essential/no-profile-picture.jpg') }}">
            </a>
        @else
            <a href="javascript:void(0)">
                <img class="avatar" src="{{ asset('img/essential/no-profile-picture.jpg') }}">
            </a>
        @endif
        <div class="mt-1 text">
            <div class="user-info">
                @if( !is_null($question->user) && !$question->anonymous )
                    <a class="username" href="{{ route('users.show', $question->user->username) }}">{!! $question->user->username_with_badge !!}</a>
                @else
                    <a class="username" href="javascript:void(0)"><i>Unknown User</i></a>
                @endif
                {!! $question->user->user_role_icon_popover_html !!}
            </div>
            <span class="time">Asked {{ $question->created_at->longRelativeDiffForHumans() }}</span>
        </div>
        @if( !empty($question->flagged_as) )
            <div class="question-block__flag-left {{ str_slug($question->flagged_as) }}" style="top:8px;">
                {{ $question->flagged_as }}
                <div class="triangle-left"></div>
            </div>
        @endif
    </div>

    {{-- Question title --}}
    <h1 class="question-block__title">
        <a href="{{ $question->show_url }}" class="stretched-link"></a>
        <strong>{{ $question->question }}</strong>
    </h1>

    {{-- Description --}}
    @if(!is_null($question->description))
        <div class="mb-2">
            <a href="{{ $question->show_url }}" class="inactive">
                {{ $question->brief_description }}
            </a>
        </div>
    @endif

    @if(!is_null($question->img))
        <div class="question-block__image-container">
            <img src="{{ $question->img }}" class="question-block__image">
        </div>
    @endif

    <div class="question-block__button-group">
        <button type="button" class="question-upvote-button @if($question->upvoted) upvoted @endif" data-questionid="{{ $question->id }}"><span class="icon"></span> <span class="text">Upvote</span> · <span class="upvote-count" data-questionid="{{ $question->id }}">{{ readableNumber($question->upvote_count) }}</span></button>
        <button type="button" class="answer-button" data-url="{{ route('questions.show', [$question->subject->exam_board->slug, $question->id]).'/'.$question->slug }}"><span class="icon"></span> <span class="text">Answer</span> · <span class="answer-count">{{ count($question->answers) }}</span></button>
    </div>
</div>
