var $form = $('#examBuilderFilterForm');
var $paperNumberSelect = $('[name=paper_number]');
var chapter_selectHTML = $('#examBuilderFilterForm .exam-builder__chapter-select').html();

// ------------------
// Filter
// ------------------
function updateChapterSelect()
{
    // Get select
    var $chapterSelect = $('#examBuilderFilterForm .exam-builder__chapter-select');
    var $originalChapterSelect = $('#examBuilderFilterForm select.exam-builder__chapter-select');

    // Reset Options
    var $options = $chapterSelect.find('option');
    $options.removeAttr('disabled');

    // Get options
    // Parent
    var $parentOptions = $options.filter('.parent');
    var $selectedParentOptions = $parentOptions.filter(':selected');
    // Child
    var $childOptions = $options.filter('.child');
    var $selectedChildOptions = $childOptions.filter(':selected');

    // Disable child chapters of selected parent chapter options
    $selectedParentOptions.each(function(i, $parent){
        $childOptions.filter('[data-parent-chapter-id='+ $parent.value +']').attr('disabled', 'disabled');
    });

    // Disable parent chapter of selected child chapter options
    $selectedChildOptions.each(function(i, $child){
        $parentOptions.filter('[value='+ $child.dataset.parentChapterId +']').attr('disabled', 'disabled');
    });

    // Sync wrapper select div element with original select element
    $originalChapterSelect.show();
    $originalChapterSelect.hide();
}


if( $form.find('.exam-builder__subject-select').length > 0 && $form.find('.exam-builder__chapter-select').length > 0) {
    new SlimSelect({
        select: '#examBuilderFilterForm .exam-builder__subject-select',
        placeholder: 'Select a subject',
        onChange: () => {
            var $subject_selected = $form.find('.exam-builder__subject-select :selected');
            var subject_id = $subject_selected.val();
            var subject_selected = $subject_selected.text();
            var $chapter_select = $('#examBuilderFilterForm .exam-builder__chapter-select');
            $chapter_select.prop('disabled', false);
            // restore the full select list first.
            $chapter_select.html(chapter_selectHTML);

            var optGroup = $('optgroup[label="' + subject_selected + '"]').html();

            $chapter_select.html(optGroup);

            new SlimSelect({
                select: '#examBuilderFilterForm .exam-builder__chapter-select',
                placeholder: 'Select Chapters',
                limit: examBuilderChapterLimit,
                closeOnSelect: false,
                onChange: function() {
                    updateChapterSelect();
                },
            });

            if( $paperNumberSelect.length > 0 ) {
                // Update Paper Number Select options
                $.ajax({
                    method: 'GET',
                    url: '/exam-builder/subjects/' + subject_id + '/paper_numbers',
                    success: function( paperNumbers ){
                        $paperNumberSelect.attr('disabled', false);
                        var optionsHtml = '<option value="" disabled selected>Select Paper Number</option>';
                        paperNumbers.forEach(function(paperNumber){
                            optionsHtml += '<option value="'+ paperNumber +'">'+ paperNumber +'</option>';
                        });
                        // Add Paper Number options on select element
                        $paperNumberSelect.html( optionsHtml );
                    }
                });
            }
        }
    });

    /*
    * For failed requests
    */

    var subject_selected = $('.exam-builder__subject-select :selected').text();
    var $subject_selected = $form.find('.exam-builder__subject-select :selected');
    var subject_id = $subject_selected.val();
    var $chapter_select = $('#examBuilderFilterForm .exam-builder__chapter-select');
    //Enable chapter select if old subject is selected
    if(subject_selected){
        $chapter_select.prop('disabled', false);
    }

    //Remove unrelated chapters
    var optGroup = $('optgroup[label="' + subject_selected + '"]').html();
    $chapter_select.html(optGroup);

    /*
    * Inititialise chapter select
    */

    new SlimSelect({
        select: '#examBuilderFilterForm .exam-builder__chapter-select',
        placeholder: 'Select Chapters',
        limit: examBuilderChapterLimit,
        closeOnSelect: false,
        onChange: function() {
            updateChapterSelect();
        },
    });

    if( $paperNumberSelect.length > 0 && !Number.isNaN( parseInt(subject_id) ) ) {
        // Enable Paper Select
        $paperNumberSelect.attr('disabled', false);
        var oldPaperNumber = $paperNumberSelect.val();
        // Update Paper Number Select options
        $.ajax({
            method: 'GET',
            url: '/exam-builder/subjects/' + subject_id + '/paper_numbers',
            success: function( paperNumbers ){
                var optionsHtml = '<option value="" disabled selected>Select Paper Number</option>';
                paperNumbers.forEach(function(paperNumber){
                    optionsHtml += '<option value="'+ paperNumber +'">'+ paperNumber +'</option>';
                });
                // Add Paper Number options on select element
                $paperNumberSelect.html( optionsHtml );
                $paperNumberSelect.find('option[value="'+ oldPaperNumber +'"]').prop('selected', true);

            }
        });
    }
}

// --------------
// Customize
// --------------
$(document).on('click', '.itemBlock .toggleAnswerButton', function(){
    $(this).next('.item__answer').toggleClass('d-none');
    if( $(this).text() == 'Show Answer' ) {
        $(this).text('Hide Answer');
    } else {
        $(this).text('Show Answer');
    }
});

// Stores current worksheet's item IDs
var $itemsInput = $('[name=item_ids]');
var $itemCount = $('span.itemCount');

// Replace current worksheet's item IDs with new item ID
// Reference: https://stackoverflow.com/a/5915824
function replaceItemId( id, new_id ) {
    var itemIds = $itemsInput.val();
    // Convert string into JS array
    itemIds = JSON.parse(itemIds);

    // Replace index number from old to new item ID
    var i = itemIds.indexOf(id);
    itemIds[i] = new_id;

    // Maintain the JSON string format of items input
    $itemsInput.val('[' + itemIds + ']');
}

// Remove item ID from item IDs array
// Reference: https://stackoverflow.com/a/5767357
function removeItemId( id ) {
    var itemIds = $itemsInput.val();

    // Convert string into JS array
    itemIds = JSON.parse(itemIds);

    var i = itemIds.indexOf(id);
    if( i > -1 ) {
        itemIds.splice(i, 1);
    }

    // Maintain the JSON string format of items input
    $itemsInput.val('[' + itemIds + ']');
}

$(document).on('click', '.itemBlock .regenerateItemButton', function(){
    var $t = $(this);
    var $initItemBlock = $t.parents('.itemBlock');
    var initHtml = $t.html();
    var itemId = $initItemBlock.data('item-id');
    var itemIds = $itemsInput.val();
    // Show loader in button
    window.showLoader( $t, 'in-button', 'white' );
    // Replace item
    $.ajax({
        method: 'POST',
        url: '/exam-builder/items/' + itemId + '/replace',
        data: { item_ids: itemIds },
        success: function (item) {
            // Get item block HTML with AJAX call
            $.ajax({
                method: 'POST',
                url: '/exam-builder/items/' + item.id + '/html', // Returns new item block html
                success: function (html) {
                    // Replace item block by appending new block to existing block, then deleting the existing block
                    $initItemBlock.after( html );
                    $initItemBlock.remove();

                    // Replace current worksheet's item IDs with new item ID
                    replaceItemId(itemId, item.id);


                    toastr.success("Item replaced successfully!");
                },
                error: function() {
                    toastr.error("Item cannot be replaced!");
                    window.hideLoader( $t, initHtml );
                }
            });
        },
        error: function() {
            toastr.error("Item cannot be replaced!");
            window.hideLoader( $t, initHtml );
        }
    });
});

$(document).on('click', '.itemBlock .removeItemButton', function(){
    var $t = $(this);
    var $initItemBlock = $t.parents('.itemBlock');
    var itemId = $initItemBlock.data('item-id');
    var itemName = $initItemBlock.data('item-name');

    var isLastItem = $('.itemBlock').length <= 1;
    if( !isLastItem ) {
        // Show delete alert
        Swal.fire({
            title: 'Are you sure?',
            type: 'error',
            html: '<b>' + itemName + '</b> will be removed from your current worksheet!',
            showCloseButton: true,
            showCancelButton: true,
            focusConfirm: false,
            reverseButtons: true,
            confirmButtonText: 'Confirm',
        }).then(function(result){
            if(result.value){
                // Remove item ID from item IDs array
                removeItemId(itemId);

                // Reflect -1 item count on front-end counter
                var val = parseInt($itemCount.text());
                $itemCount.text( val - 1 );

                // Remove Item Block
                $initItemBlock.remove();
            }
        });
    } else {
        Swal.fire({
            title: 'Exam Paper cannot be empty!',
            type: 'error',
            html: 'This is the last question for your exam paper!',
            showCloseButton: true,
            focusConfirm: true,
            confirmButtonText: 'Confirm',
        });
    }
});
