<!-- Sidebar -->
<ul class="navbar-nav sidebar accordion toggled" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{ route('classroom.classrooms.index') }}">
        <div class="sidebar-brand-icon p-2">
          <img class="w-100" src="{{ asset('img/logo/logo_light_icon.png') }}" alt="">
        </div>

        <div class="sidebar-brand-text mx-1">
            <img class="w-100" src="{{ asset('img/logo/logo_light.png') }}" alt="">
        </div>
    </a>

    <div class="text-white">
        <a href="{{ route('classroom.classrooms.index') }}"></a>
    </div>

    <!-- Nav Item - Dashboard -->
    <li class="nav-item mb-2">
        <a class="nav-link" href="{{ route('classroom.classrooms.index') }}">
          <span>< Back to Classes</span></a>
    </li>

    <!-- Heading -->
    <div class="sidebar-heading">
        {{ $classroom->name }}
    </div>

    <!-- Nav Item - Dashboard -->
    <li class="nav-item @if(request()->is('classroom/dashboard') || request()->is('classroom/*/dashboard')) active @endif">
      <a class="nav-link" href="{{ route('classroom.classrooms.show', $classroom->id) }}">
        <span class="iconify" data-icon="mdi-light:view-dashboard" data-inline="false"></span>
        <span>Dashboard</span></a>
    </li>

    <li class="nav-item @if(request()->is('classroom/*/discussion') || request()->is('classroom/*/posts/*')) active @endif">
      <a class="nav-link" href="{{ route('classroom.posts.index', $classroom->id) }}">
        <span class="iconify" data-icon="octicon:comment-discussion-24" data-inline="false"></span>
        <span>Discussion</span></a>
    </li>

    <li class="nav-item @if(request()->is('classroom/*/lessons') || request()->is('classroom/*/lessons/*')) active @endif">
      <a class="nav-link" href="{{ route('classroom.lessons.index', $classroom->id) }}">
        <span class="iconify" data-icon="ion:cube-outline" data-inline="false"></span>
        <span>Lessons</span></a>
    </li>


    {{-- <!-- Nav Item - Dashboard -->
    <li class="nav-item @if(request()->is('t/*/questions') || request()->is('t/*/questions/*')) active @endif">
        <a class="nav-link" href="{{ route('classroom.questions', $classroom->id) }}">
        <i class="fas fa-fw fa-question-circle"></i>
        <span>Questions</span></a>
    </li> --}}

    {{-- <!-- Nav Item - Dashboard -->
    <li class="nav-item @if(request()->is('t/*/questions') || request()->is('t/*/questions/*')) active @endif">
        <a class="nav-link" href="{{ route('classroom.questions', $classroom->id) }}">
          <i class="fas fa-fw fa-question-circle"></i>
          <span>Questions</span></a>
      </li>

    <!-- Nav Item - Dashboard -->
    <li class="nav-item @if(request()->is('t/*/resources') || request()->is('t/*/resources/*')) active @endif">
      <a class="nav-link" href="{{ route('classroom.lessons.resource_posts', $classroom->id) }}">
        <i class="fas fa-fw fa-book"></i>
        <span>Resource Post</span></a>
    </li>


    <!-- Nav Item - Dashboard -->
    <li class="nav-item @if(request()->is('t/*/homework') || request()->is('t/*/homework/*')) active @endif">
      <a class="nav-link" href="{{ route('classroom.homework', $classroom->id) }}">
        <i class="fas fa-fw fa-tasks"></i>
        <span>Homework</span></a>
    </li> --}}



    {{-- <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    <div class="sidebar-heading">
      Interface
    </div>

    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item">
      <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
        <i class="fas fa-fw fa-cog"></i>
        <span>Components</span>
      </a>
      <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
          <h6 class="collapse-header">Custom Components:</h6>
          <a class="collapse-item" href="buttons.html">Buttons</a>
          <a class="collapse-item" href="cards.html">Cards</a>
        </div>
      </div>
    </li>

    <!-- Nav Item - Utilities Collapse Menu -->
    <li class="nav-item">
      <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">
        <i class="fas fa-fw fa-wrench"></i>
        <span>Utilities</span>
      </a>
      <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
          <h6 class="collapse-header">Custom Utilities:</h6>
          <a class="collapse-item" href="utilities-color.html">Colors</a>
          <a class="collapse-item" href="utilities-border.html">Borders</a>
          <a class="collapse-item" href="utilities-animation.html">Animations</a>
          <a class="collapse-item" href="utilities-other.html">Other</a>
        </div>
      </div>
    </li>


    <!-- Heading -->
    <div class="sidebar-heading">
      Addons
    </div>

    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item">
      <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages" aria-expanded="true" aria-controls="collapsePages">
        <i class="fas fa-fw fa-folder"></i>
        <span>Pages</span>
      </a>
      <div id="collapsePages" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
          <h6 class="collapse-header">Login Screens:</h6>
          <a class="collapse-item" href="login.html">Login</a>
          <a class="collapse-item" href="register.html">Register</a>
          <a class="collapse-item" href="forgot-password.html">Forgot Password</a>
          <div class="collapse-divider"></div>
          <h6 class="collapse-header">Other Pages:</h6>
          <a class="collapse-item" href="404.html">404 Page</a>
          <a class="collapse-item" href="blank.html">Blank Page</a>
        </div>
      </div>
    </li>

    <!-- Nav Item - Charts -->
    <li class="nav-item">
      <a class="nav-link" href="charts.html">
        <i class="fas fa-fw fa-chart-area"></i>
        <span>Charts</span></a>
    </li>

    <!-- Nav Item - Tables -->
    <li class="nav-item">
      <a class="nav-link" href="tables.html">
        <i class="fas fa-fw fa-table"></i>
        <span>Tables</span></a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">

    --}}

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle" id="sidebarToggle"></button>
      </div>

  </ul>
  <!-- End of Sidebar -->
