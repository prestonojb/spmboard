@php
    // Declare variables
    if( !is_null($blog) ) {
        $title = old('title') ?? $blog->title;
        $description = old('description') ?? $blog->description;
        $body = old('body') ?? $blog->body;
        $blog_author_id = old('blog_author_id') ?? $blog->blog_author_id;
        $blog_category_id = old('blog_category_id') ?? $blog->blog_category_id;
    } else {
        $title = old('title');
        $description = old('description');
        $body = old('body');
        $blog_author_id = old('blog_author_id');
        $blog_category_id = old('blog_category_id');
    }
    $url = $is_edit ? route('voyager.blogs.update', $blog->id) : route('voyager.blogs.store');
@endphp

<form class="form-edit-add" role="form"
        action="{{ $url }}"
        method="POST" enctype="multipart/form-data" autocomplete="off">
        {{ csrf_field() }}
    @if($is_edit)
        @method('PATCH')
    @endif

    <div class="panel" style="max-width: 740px; padding: 12px; margin: 0 auto;">
        {{-- Title --}}
        <div class="form-group">
            <label for="">Title</label>
            <input type="text" name="title" class="form-control" placeholder="Title" id="" required value="{{ $title }}">
            @error('title')
                <div class="text-danger">{{ $message }}</div>
            @enderror
        </div>

        {{-- Description --}}
        <div class="form-group">
            <label for="">Description</label>
            <textarea name="description" class="form-control" id="" rows="3" required placeholder="Description">{{ $description }}</textarea>
            @error('description')
                <div class="text-danger">{{ $message }}</div>
            @enderror
            <p class="gray2">This field is not shown to the users and is only for SEO.</p>
        </div>

        {{-- Body --}}
        <div class="form-group">
            <label for="">Body</label>
            <div class="loading-icon loading in-editor blue"></div>
            <textarea class="tinymce-editor" name="body">{!! $body !!}</textarea>
            @error('body')
                <div class="error-message">{{ $message }}</div>
            @enderror
        </div>

        {{-- Image --}}
        <div class="form-group">
            <label for="">Cover Image</label>
            <input type="file" name="img" class="form-control" accept="image/*">
        </div>

        {{-- Blog Author --}}
        <div class="form-group">
            <label for="">Blog Author</label>
            <select name="blog_author_id" id="">
                <option value="" disabled selected>Select Blog Author</option>
                @foreach($blog_authors as $author)
                    <option value="{{ $author->id }}" @if($blog_author_id == $author->id) selected @endif>{{ $author->name }}</option>
                @endforeach
            </select>
            @error('blog_author')
                <div class="text-danger">{{ $message }}</div>
            @enderror
        </div>

        {{-- Blog Categories --}}
        <div class="form-group">
            <label for="">Blog Categories</label>
            <select name="blog_category_id" id="">
                <option value="" disabled selected>Select Blog Category</option>
                @foreach($blog_categories as $category)
                    <option value="{{ $category->id }}" @if($blog_category_id == $category->id) selected @endif>{{ $category->name }}</option>
                @endforeach
            </select>
            @error('blog_category')
                <div class="text-danger">{{ $message }}</div>
            @enderror
        </div>

        <div class="form-group"></div>

        <div class="text-right">
            <button type="submit" class="btn btn-primary">
                Save
            </button>
        </div>
    </div>
</form>
