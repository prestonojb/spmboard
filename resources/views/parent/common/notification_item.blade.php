@php
switch($notification->data['type']) {
    case 'App\StudentReport':
        $url = $notification->data['url'];
        break;
    case 'App\Homework':
        $url = 'javascript:void(0)';
        break;
    default:
        $url = 'javascript:void(0)';
}
@endphp

<div class="notification-list-item position-relative @if(isset($unread) && $unread) unread @endif">
    <p>{!! $notification->data['title'] !!}</p>
    <span><small>{{ $notification->created_at->diffForHumans() }}</small></span>
    @switch($notification->data['type'])
        @case('App\StudentReport')
            <a class="stretched-link" href="{{ $url }}" target="_blank"></a>
            @break
        @case('App\Homework')
            <a class="stretched-link" href="javascript:void(0)"></a>
            @break
    @endswitch
</div>

