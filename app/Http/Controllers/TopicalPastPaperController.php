<?php

namespace App\Http\Controllers;

use App\Chapter;
use App\TopicalPastPaper;
use App\ExamBoard;
use App\Subject;
use App\Resource;
use Amplitude;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Session;

class TopicalPastPaperController extends Controller
{
    /**
     * Get Home View
     */
    public function getHomeView(ExamBoard $exam_board)
    {
        $subjects = $exam_board->subjects()->whereHas('topical_past_papers')->get() ?? [];
        return view('resources.topical_past_papers.home', compact('exam_board', 'subjects'));
    }

    /**
     * Show topical past paper
     */
    public function show(ExamBoard $exam_board, TopicalPastPaper $topical_past_paper)
    {
        return redirect()->route('topical_past_papers.qp.show', [$exam_board->slug, $topical_past_paper]);
    }

    /**
     * Show subject topical past papers
     */
    public function subjectShow(ExamBoard $exam_board, Subject $subject)
    {
        $chapters = $subject->chapters()->whereHas('topical_past_papers')->get();

        $topical_past_papers = TopicalPastPaper::where('subject_id', $subject->id);

        if(request('chapter') == -1) {
           $topical_past_papers = $topical_past_papers->whereNull('chapter_id');
        } elseif(!is_null(request('chapter'))) {
           $topical_past_papers = $topical_past_papers->where('chapter_id', request('chapter'));
        }

        $topical_past_papers = $topical_past_papers->paginate(30) ?? [];

        return view('resources.topical_past_papers.subject_show', compact('exam_board', 'subject', 'chapters', 'topical_past_papers'));
    }

    /**
     * Show topical past paper question
     */
    public function questionShow(ExamBoard $exam_board, TopicalPastPaper $topical_past_paper)
    {
        // Record views
        if(!is_null($topical_past_paper->resource)) {
            views($topical_past_paper->resource)->record();
        }

        $is_question = true;
        return view('resources.topical_past_papers.show', compact('exam_board', 'topical_past_paper', 'is_question'));
    }

    /**
     * Show topical past paper answer
     */
    public function answerShow(ExamBoard $exam_board, TopicalPastPaper $topical_past_paper)
    {
        if( empty($topical_past_paper->ms_url) ) {
            abort(404);
        }

        // Record views
        if(!is_null($topical_past_paper->resource)) {
            views($topical_past_paper->resource)->record();
        }

        $is_question = false;
        return view('resources.topical_past_papers.show', compact('exam_board', 'topical_past_paper', 'is_question'));
    }


    /**
     * Create topical paper
     */
    public function create(ExamBoard $exam_board, TopicalPastPaper $topical_past_paper)
    {
        $is_edit = false;
        return view('resources.topical_past_papers.create_edit', compact('exam_board', 'topical_past_paper', 'is_edit'));
    }

    /**
     * Store topical paper
     */
    public function store(ExamBoard $exam_board)
    {
        // Validation
        request()->validate([
            'subject' => 'required|integer',
            'chapter' => 'nullable|integer',
            'name' => 'required|string',
            'description' => 'nullable|string|max:500',
            'paper_number' => 'nullable|integer',
            'qp_file' => 'required|file|mimes:pdf|max:50000',
            'ms_file' => 'nullable|file|mimes:pdf|max:50000',
            'coin_price' => 'nullable|integer|between:0,100',
            'confirm_tnc' => 'required',
        ]);

        $user = auth()->user();

        // Create topical paper
        $topical_past_paper = $user->topical_past_papers()->create([
                                'subject_id' => request('subject'),
                                'chapter_id' => request('chapter'),
                                'name' => request('name'),
                                'description' => request('description'),
                                'paper_number' => request('paper_number'),
                                'coin_price' => request('coin_price') ?? 0,
                            ]);

        // Add resource relationship to newly created topical paper
        Resource::create([
            'resourceable_type' => TopicalPastPaper::class,
            'resourceable_id' => $topical_past_paper->id
        ]);

        // Save files
        $topical_past_paper->saveQpFile(request('qp_file'));
        if(!is_null(request('ms_file'))) {
            $topical_past_paper->saveMsFile(request('ms_file'));
        }

        // Save Preview Image
        $topical_past_paper->savePreviewImage(request('qp_file'));

        Session::flash('success', $topical_past_paper->name . ' uploaded successfully!');
        return redirect()->route('topical_past_papers.show', [$exam_board->slug, $topical_past_paper->id]);
    }

    /**
     * Edit topical paper
     */
    public function edit(ExamBoard $exam_board, TopicalPastPaper $topical_past_paper)
    {
        $is_edit = true;
        return view('resources.topical_past_papers.create_edit', compact('exam_board', 'topical_past_paper', 'is_edit'));
    }

    /**
     * Update topical paper
     */
    public function update(ExamBoard $exam_board, TopicalPastPaper $topical_past_paper)
    {
        // Validation
        request()->validate([
            'subject' => 'required|integer',
            'chapter' => 'nullable|integer',
            'name' => 'required|string',
            'description' => 'nullable|string',
            'paper_number' => 'nullable|integer',
        ]);

        // Update topical paper
        $topical_past_paper->update([
            'subject_id' => request('subject'),
            'chapter_id' => request('chapter'),
            'name' => request('name'),
            'description' => request('description'),
            'paper_number' => request('paper_number'),
        ]);

        return redirect()->route('topical_past_papers.qp.show', [$exam_board->slug, $topical_past_paper->id]);
    }

    /**
     * Download Question Paper File
     */
    public function downloadQpFile(ExamBoard $exam_board, TopicalPastPaper $topical_past_paper)
    {
        $user = auth()->user();
        // Log event on Amplitude
        Amplitude::setUserId($user->id);
        Amplitude::queueEvent('download_resource', ['type' => "Topical Past Paper", 'resourceable_id' => $topical_past_paper->id, 'others' => "QP"]);

        try {
            return Storage::disk('s3')->download( $topical_past_paper->qp_url_relative_path );
        } catch (\Exception $e) {
            return redirect()->back()->with("error", "An error occured while downloading file, please try again later.");
        }
    }

    /**
     * Download MS File
     */
    public function downloadMsFile(ExamBoard $exam_board, TopicalPastPaper $topical_past_paper)
    {
        $user = auth()->user();
        // Log event on Amplitude
        Amplitude::setUserId($user->id);
        Amplitude::queueEvent('download_resource', ['type' => "Topical Past Paper", 'resourceable_id' => $topical_past_paper->id, 'others' => "MS"]);

        try {
            return Storage::disk('s3')->download( $topical_past_paper->ms_url_relative_path );
        } catch (\Exception $e) {
            return redirect()->back()->with("error", "An error occured while downloading file, please try again later.");
        }
    }

    /**
     * Unlock notes
     */
    public function unlock(ExamBoard $exam_board, TopicalPastPaper $topical_past_paper)
    {
        $user = auth()->user();
        $buyer = $user;
        // User does not have coins exception
        if(!$user->hasCoins()) {
            return redirect()->back()->withError('User cannot unlock resource without coins!');
        }

        // Topical paper is free
        if($topical_past_paper->isFree()) {
            return redirect()->back()->withError('Resource is already free.');
        }

        // User alraedy owns resource
        if($user->hasResource($topical_past_paper->resource)) {
            return redirect()->back()->withError('Resource is already unlocked!');
        }

        // User does not have sufficient coin balance
        $purchase_price = $topical_past_paper->getAdjustedCoinPrice($buyer);
        if($user->coin_balance < $purchase_price) {
            return redirect()->back()->withError('You have insufficient coin balance!');
        }

        try {
            $topical_past_paper->unlockFor($buyer);
        } catch(\Exception $e) {
            return redirect()->back()->withError('An error occurred! Please try again later.');
        }

        Session::flash('success', $topical_past_paper->name." unlocked successfully!");
        return redirect()->route('topical_past_papers.show', [$exam_board->slug, $topical_past_paper->id]);
    }

    /**
     * Download PDF
     */
    public function downloadPdf(ExamBoard $exam_board, TopicalPastPaper $topical_past_paper)
    {
        try {
            return Storage::disk('s3')->download( $topical_past_paper->url_relative_path );
        } catch(\Exception $e) {
            return redirect()->back()->withError('Download failed, please try again later.');
        }
    }

    public function delete(ExamBoard $exam_board, TopicalPastPaper $topical_past_paper)
    {
        $subject = $topical_past_paper->subject;
        $topical_past_paper->delete();

        Session::flash('success', 'Topical paper deleted successfully!');
        return redirect()->route('topical_past_papers.subject_show', [$exam_board->slug, $subject->id]);
    }
}
