<div class="filter">
    <h3 class="h3 filter__title">Filter</h3>

    <form class="filter__form" action="{{ url()->current() }}" method="GET">
        @csrf

        @if($exam_board->slug == 'igcse')
            <div class="form-field">
                <label class="filter__label">Year</label>
                <select name="year[]" class="year-select" multiple>
                    @foreach(range(2020, 2010, -1) as $i)
                        <option value="{{ $i }}" {{ is_array(request('year')) && in_array($i, request()->year) ? 'selected=selected' : '' }}>{{ $i }}</option>
                    @endforeach
                </select>
            </div>


            <div class="form-field">
                <label class="filter__label">Season</label>
                <select name="season[]" class="season-select" multiple>
                    @foreach(['Feb/March', 'May/June', 'Oct/Nov'] as $season)
                        <option value="{{ $season }}" {{ is_array(request('season')) && in_array($season, request()->season) ? 'selected=selected' : '' }}>{{ $season }}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-field">
                <label class="filter__label" for="">Paper</label>
                <select name="paper[]" class="paper-select" multiple>
                    <option data-placeholder="true"></option>
                    @foreach(range(1, 6) as $i)
                        <option value="{{ $i }}" {{ is_array(request('paper')) && in_array($i, request()->paper) ? 'selected=selected' : '' }}>{{ $i }}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-field">
                <label class="filter__label">Variant</label>
                <select name="variant[]" class="variant-select" multiple>
                    @foreach(range(1, 3) as $i)
                        <option value="{{ $i }}" {{ is_array(request('variant')) && in_array($i, request()->variant) ? 'selected=selected' : '' }}>{{ $i }}</option>
                    @endforeach
                </select>
            </div>
        @elseif($exam_board->slug == 'spm')

            <div class="form-field">
                <label class="filter__label">Year</label>
                <select name="year[]" class="year-select" multiple>
                    @foreach(range(2020, 2010, -1) as $i)
                        <option value="{{ $i }}" {{ is_array(request('year')) && in_array($i, request()->year) ? 'selected=selected' : '' }}>{{ $i }}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-field">
                <label class="filter__label" for="">Paper</label>
                <select name="paper_number[]" class="paper-select" multiple>
                    <option data-placeholder="true"></option>
                    @foreach(range(1, 6) as $i)
                        <option value="{{ $i }}" {{ is_array(request('paper_number')) && in_array($i, request('paper_number')) ? 'selected=selected' : '' }}>{{ $i }}</option>
                    @endforeach
                </select>
            </div>

        @endif

        <button type="submit" class="button--primary filter__button">Apply filter</button>
        <button type="button" class="filter__button clear-filter">Clear filter</button>
    </form>
</div>
