@extends('main.layouts.app')

@section('meta')
    <title>Classroom | {{ config('app.name') }}</title>
    <meta name="description" content="Board is an online community for students to learn from mentors. Share the joy of learning and teaching with the Board community now!">
@endsection

@section('content')
<div class="bg-white">
    {{-- Main background image --}}
    <section class="main__landing" style="background-image: url('{{ asset('img/phy_resources/phy_resources_wp.png') }}')">
        <div class="text-center">
            <h1 class="heading">Board Classroom</h1>
            <h1 class="sub-heading">Where learning happens</h1>

            <div class="py-2"></div>
            <a href="#learn_more" class="button--primary lg font-size5">Learn More</a>
        </div>
    </section>

    <div id="learn_more"></div>

    {{-- Why Board Classroom works? --}}
    @php
    $data = [
        ['title' => 'Personalised Learning', 'description' => 'Students learn at their own pace with the guidance of teacher in Board\'s virtual classroom!', 'icon' => 'noto-v1:glowing-star'],
        ['title' => 'Tools to empower teacher', 'description' => 'From study materials to administrative helper tools, teachers need not worry about anything besides teaching.', 'icon' => 'emojione:hand-with-fingers-splayed-medium-light-skin-tone'],
        ['title' => 'Child monitoring', 'description' => 'Teachers and parents can share child\'s learning insights to help the child improve academically together!', 'icon' => 'flat-ui:graph'],
    ];
    @endphp
    @component('main.section.icon_list', ['title' => 'Why Board Classrooom works?', 'data' => $data, 'bg_light' => true])
    @endcomponent


    {{-- Teachers --}}
    @component('main.section.with_side_image', ['subtitle' => 'Teachers', 'title' => 'Spend more time improving lesson quality, not on adminstrative tasks', 'description' => 'Devote your time into improving your students\' mastery of the subject, and prepare them for exams!', 'img' => asset('img/undraw/teaching.svg'),'cta_text' => 'Teachers, start here', 'cta_url' => route('register').'?account_type=teacher', 'reverse' => false])
        <ul class="checklist mb-2">
            <div class="row">
                <div class="col-6">
                    <li>Exam Builder</li>
                    <li>Invoicing</li>
                </div>
                <div class="col-6">
                    <li>Reporting</li>
                    <li>Analytics</li>
                </div>
            </div>
        </ul>
    @endcomponent

    {{-- Students --}}
    @component('main.section.with_side_image', ['subtitle' => 'Students', 'title' => 'Review lesson materials anytime, anywhere', 'description' => 'Study anywhere with your teacher, anytime.', 'img' => asset('img/undraw/bookshelves.svg'),'cta_text' => 'Students, start here', 'cta_url' => route('register').'?account_type=student', 'reverse' => true])
    @endcomponent

    {{-- Students --}}
    @component('main.section.with_side_image', ['subtitle' => 'Parents', 'title' => 'Receive updates from teachers on your child\'s academic progress', 'description' => 'Keep up with the updates from teacher about your child in class.', 'img' => asset('img/undraw/updates.svg'),'cta_text' => 'Parents, start here', 'cta_url' => route('register').'?account_type=parent', 'reverse' => false])
    @endcomponent


    {{-- Features --}}
    @php
        $data = [
            ['icon' => 'carbon:report', 'title' => 'Automated Reporting', 'description' => 'Leave what you need to report to parents to us and focus on delivering the next lessons!'],
            ['icon' => 'la:file-invoice-solid', 'title' => 'Invoice Tracking', 'description' => 'No need for excel sheets to collect your fees!'],
            ['icon' => 'bi:person', 'title' => 'Student Analytics', 'description' => 'Keep a close eye on your students\' learning progress!'],
            ['icon' => 'jam:write', 'title' => 'Homework marking', 'description' => 'Manage your marking in one place!'],
            ['icon' => 'carbon:category-new-each', 'title' => 'Ready-made resources', 'description' => 'Leverage on our study materials to enhance your teaching!'],
        ];
    @endphp
    @component('main.section.icon_list', ['title' => 'Features', 'description' => 'Check out Board Classroom\'s awesomeness!', 'data' => $data, 'bg_light' => true])
    @endcomponent

    {{-- FAQ --}}
    @php
        $qna = [
            'Is classroom free to use?' => 'Yes, it is free up until a certain limit! Then, you might want to consider subscribing to Board Premium to gain complete access to Board Classroom!',
        ];
    @endphp

    @component('main.section.faq', ['qna' => $qna])
    @endcomponent

    {{-- Join Board Classroom today --}}
    @component('main.section.cta_banner', ['title' => 'Join Board Classroom today'])
        <a href="{{ route('register').'?account_type=teacher' }}" class="primary mx-auto mb-2"><b>Teacher</b></a>
        <a href="{{ route('register').'?account_type=student' }}" class="primary mx-auto mb-2"><b>Student</b></a>
        <a href="{{ route('register').'?account_type=parent' }}" class="primary mx-auto mb-2"><b>Parent</b></a>
        <a href="{{ route('contactUs.createEmail') }}" class="secondary mx-auto mb-2">Contact Us</a>
    @endcomponent

    {{-- Footer --}}
    @include('main.navs.footer')
</div>
@endsection

@section('scripts')
<script>
    $(function(){
        $('a[href="#learn_more"]').click(function(e) {
            e.preventDefault();
            $('html, body').animate({
                scrollTop: $("#learn_more").offset().top - 68
            }, 500);
        });
    });
</script>
@endsection
