@extends('resources.layouts.app')
@section('meta')
<title>Exam Builder | {{ $exam_board->name }} {{ config('app.name') }} </title>
@endsection

@section('header')
    @component('resources.common.header', ['type' => 'exam_builder', 'exam_board' => $exam_board])
    @endcomponent
@endsection

@section('breadcrumbs')
    @component('resources.common.breadcrumbs')
        Home
    @endcomponent
@endsection

@section('context_panel')
    @component('resources.common.context_panel', ['exam_board' => $exam_board->name, 'title' => 'Exam Builder', 'description' => 'Practice makes perfect!'])
    @endcomponent
@endsection

@section('body')
    <div class="alert alert-primary">
        Check out the <a target="_blank" href="https://www.loom.com/share/bb07644054234c96914fdf27af53b5a7">tutorial video</a> if you are unsure of how Exam Builder works!
    </div>

    <div class="d-flex flex-wrap justify-content-between align-items-center mb-2">
        <div>
            <p class="mb-1">Number of worksheets available: <b>{{ auth()->user()->noOfAvailableWorksheetsLeft() == 999 ? '∞' : auth()->user()->noOfAvailableWorksheetsLeft() }}</b></p>
            <p>You can generate 3 worksheets/week with the free version. <a href="{{ route('board_pass') }}">Purchase Board Pass</a> to enjoy <b>unlimited</b> worksheets.</p>
        </div>
        <a href="@cannot('generate_qp', App\ExamBuilderSheet::class) javascript:void(0) @else {{ route('exam_builder.filter').'?exam_board='.$exam_board->slug }} @endcannot" class="button--primary lg @cannot('generate_qp', App\ExamBuilderSheet::class) disabled @endcannot">
            <i class="fas fa-plus"></i> Create Worksheet</a>
    </div>

    {{-- Table --}}
    @component('exam_builder.sheets.common.table', ['sheets' => $sheets, 'exam_board' => $exam_board])
    @endcomponent
@endsection
