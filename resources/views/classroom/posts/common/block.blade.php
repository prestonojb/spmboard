<div class="panel question--block position-relative mb-3">

    {{-- Post Type --}}
    <div class="question-block__header">
        <ul class="question-block__tags">
            <li class="text-uppercase">{{ $post->type }}</li>
        </ul>
    </div>

    <div class="question__user-header">
        {{-- User Info --}}
        <div class="question-block__user-info">
            {{-- Avatar --}}
            <a href="{{ $post->user ? route('users.show', $post->user->username) : 'javascript:void(0)' }}">
                <img class="avatar" src="{{ $post->user ? $post->user->avatar : asset('img/essential/no-profile-picture.jpg') }}">
            </a>
            {{-- Username --}}
            <div class="mt-1 text">
                <div class="user-info">
                    <a class="username" href="{{ route('users.show', $post->user->username) }}">{!! $post->user ? $post->user->username : '<i>Deleted Account</i>' !!}</a>
                    {!! $post->user->user_role_icon_popover_html !!}
                </div>
                <span class="time">Posted {{ $post->created_at->longRelativeDiffForHumans() }}</span>
            </div>
        </div>

        {{-- Action buttons --}}
        <div class="btn-group" role="group">
            <button type="button" class="more-button" data-toggle="dropdown">
                <i class="fa fa-ellipsis-h" aria-hidden="true"></i>
            </button>

            <div class="dropdown-menu dropdown-menu-right">
                @can('update', $post)
                    <a class="dropdown-item" href="{{ route('classroom.posts.edit', [$classroom->id, $post->id]) }}">Edit</a>
                @endcan
                @can('delete', $post)
                    <form method="POST" action="{{ route('classroom.posts.delete', [$classroom->id, $post->id]) }}">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="dropdown-item text-danger deleteButton">Delete</button>
                    </form>
                @endcan
            </div>
        </div>
    </div>

    {{-- Post Title --}}
    <h1 class="question-block__title">
        <strong>{{ $post->title }}</strong>
    </h1>

    {{-- Post Description --}}
    <div>{!! $post->description !!}</div>

    <div class="py-1"></div>

    {{-- Post Category --}}
    <div class="mb-1">
        {!! $post->category_tags_html !!}
    </div>
</div>
