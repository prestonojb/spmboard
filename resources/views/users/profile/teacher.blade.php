@component('forum.users.profile.common.main', ['user' => $teacher->user])
@endcomponent

<div class="py-1"></div>

<div class="d-block d-lg-none">
    @component('teacher.profile.more', ['teacher' => $teacher])
    @endcomponent
</div>

<div class="d-block d-lg-none py-1"></div>

@if(!is_null($teacher->user->about))
    @component('forum.users.profile.common.about', ['user' => $teacher->user])
    @endcomponent

    <div class="py-1"></div>
@endif


{{-- Teaching Summary --}}
@component('teacher.profile.teaching_summary', ['teacher' => $teacher])
@endcomponent

<div class="py-1"></div>

@component('forum.users.profile.common.recommendations', ['user' => $teacher->user])
@endcomponent

<div class="py-1"></div>

@component('forum.users.profile.common.pills', ['user' => $teacher->user])
@endcomponent
