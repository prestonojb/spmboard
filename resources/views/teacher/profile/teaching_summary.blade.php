<div class="panel p-0">
    <div class="panel-header">
        <h3 class="mb-0">Teaching</h3>
    </div>

    {{-- String data --}}
    @php
        $data = [
            'Years of Teaching Experience' => $teacher->years_of_teaching_experience,
            'Qualifications' => $teacher->qualifications,
            'Teaching Experience' => $teacher->teaching_experience,
            'Teaching Approach' => $teacher->teaching_approach,
        ];
    @endphp

    <div class="panel-body">
        @foreach($data as $key=>$value)
            @if(!is_null($value))
                @if(!$loop->first) <div class="py-3"></div> @endif
                <div class="">
                    <h4 class="mt-2 mb-1 text-uppercase font-size3 pb-2 mb-2 border-bottom font-weight-bold">{{ $key }}</h4>
                    <div class="gray2">{!! nl2br($value) !!}</div>
                </div>
            @endif
        @endforeach

        {{-- Subjects --}}
        @if(count($teacher->getSubjectsTaught() ?? []))
            <div class="py-2"></div>
            <div>
                <h4 class="mt-2 mb-2 text-uppercase font-size3 pb-2 mb-2 border-bottom font-weight-bold">Teaching Subjects</h4>
                @foreach($teacher->getSubjectsTaught() as $subject)
                    <span class="tag--primary mr-1">{{ $subject->exam_board_and_name }}</span>
                @endforeach
            </div>
        @endif
    </div>
</div>
