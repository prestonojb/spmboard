@extends('classroom.layouts.app')

@section('content')

@component('classroom.common.breadcrumbs')
    <a href="{{ route('classroom.landing') }}">Home</a> /
    <a href="{{ route('classroom.settings') }}">Settings</a> /
    Invoice
@endcomponent

@component('classroom.common.page_heading', ['title' => 'Invoice Setting'])
@endcomponent


<div class="maxw-740px mx-auto my-5">
    <form class="invoiceSettingForm" action="{{ route('classroom.settings.teacher.invoice.update') }}" method="post" enctype="multipart/form-data">
        @csrf
        @method('PATCH')
        <div class="mb-3 pb-3 border-bottom">
            <h3>Branding</h3>
            <div class="panel mb-2">
                <div class="form-field m-0">
                    <label>Logo</label>
                    <div class="row align-items-center">
                        <div class="col-12 col-lg-3">
                            @if( !is_null($setting) && !is_null($setting->logo_url) )
                                <img class="file-input-preview" src="{{ $setting->full_logo_url }}">
                            @else
                                <img class="file-input-preview" src="{{ $setting->logo_url ?? asset('img/essential/logo_placeholder.png') }}">
                            @endif
                        </div>
                        <div class="col-12 col-lg-9">
                            <div class="file-drop-area mb-1">
                                <span class="fake-btn">Choose files</span>

                                <span class="file-msg">or drag and drop files here</span>
                                <input class="file-input" name="logo" type="file">
                            </div>

                            <input type="hidden" name="remove_file">
                            <button type="button" class="button-link red underline-on-hover mr-2 removeFileButton">Remove Logo</button>
                            <button type="button" class="button-link blue4 underline-on-hover clearFileInputButton">Clear</button>

                            @error('logo')
                                <div class="error-message">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="mb-3 pb-3 border-bottom">
            <h3>Business Settings</h3>
            <div class="panel">
                <div class="form-field m-0">
                    <label for="">Address</label>
                    <input class="mb-1" type="text" name="from_address_1" placeholder="Address 1" value="{{ !is_null($setting) ? $setting->from_address_1 : '' }}">
                    @error('from_address_1')
                        <span class="error-message">{{ $message }}</span>
                    @enderror
                    <input class="mb-1" type="text" name="from_address_2" placeholder="Address 2" value="{{ !is_null($setting) ? $setting->from_address_2 : '' }}">
                    @error('from_address_2')
                        <span class="error-message">{{ $message }}</span>
                    @enderror
                    <select class="mb-1" name="state" id="">
                        <option value="" disabled selected>State</option>
                        @foreach( $states as $state )
                            <option value="{{ $state->id }}" @if(!is_null($setting) && $setting->state_id == $state->id) selected @endif>{{ $state->name }}</option>
                        @endforeach
                    </select>
                    @error('state')
                        <span class="error-message">{{ $message }}</span>
                    @enderror

                    <input class="mb-1" type="text" name="city" placeholder="City" value="{{ !is_null($setting) ? $setting->city : '' }}">
                    @error('city')
                        <span class="error-message">{{ $message }}</span>
                    @enderror
                    <input class="mb-1" type="text" name="postal_code" placeholder="Postal Code" value="{{ !is_null($setting) ? $setting->postal_code : '' }}">
                    @error('postal_code')
                        <span class="error-message">{{ $message }}</span>
                    @enderror
                </div>
            </div>
        </div>

        <div class="mb-3 pb-3 border-bottom">
            <h3>Sequencing</h3>
            <div class="panel">
                <div class="form-field">
                    <label for="">Invoice Prefix</label>
                    @if( old('prefix') )
                    <input class="mb-1" type="text" name="prefix" pattern=".{3, 8}" required value="{{ old('prefix') }}">
                    @else
                        <input class="mb-1" type="text" name="prefix" pattern=".{3, 8}" required value="{{ !is_null($setting) ? $setting->prefix : '' }}">
                    @enderror
                    <p class="mb-0 gray2 font-size3">*Prefix must be 3-8 characters long</p>

                    @error('prefix')
                        <span class="error-message">{{ $message }}</span>
                    @enderror
                </div>
                <div class="form-field m-0">
                    <label for="">
                        Next Invoice Sequence
                        <a tabindex="0" class="inactive" role="button" data-toggle="popover" data-trigger="focus" data-content="In the event that the expected invoice sequence is less than the configured invoice sequence, the invoice sequence will remain as the expected value."><i class="fa fa-info-circle" aria-hidden="true"></i></a>
                    </label>
                    <input type="number" name="next_invoice_sequence" min="1" id="" value="{{ $setting ? $setting->next_invoice_sequence : 1 }}">
                    @error('next_invoice_sequence')
                        <span class="error-message">{{ $message }}</span>
                    @enderror
                </div>
            </div>
        </div>

        <div class="mb-3 pb-3">
            <h3>Others</h3>
            <div class="panel">
                <div class="form-field">
                    <label for="">Default Lesson Rate per Hour</label>
                    <div class="d-flex align-items-center">
                        <div class="mr-2">
                            MYR
                        </div>
                        <div class="">
                            <input class="maxw-100px" type="number" name="default_lesson_rate_per_hour" value="{{ !is_null($setting) ? $setting->default_lesson_rate_per_hour : '' }}">
                        </div>
                    </div>
                    @error('default_lesson_rate_per_hour')
                        <span class="error-message">{{ $message }}</span>
                    @enderror
                </div>
                <div class="form-field">
                    <label for="">Default Payment Due</label>
                    <div class="d-flex align-items-center">
                        <div class="mr-2">
                            <input class="maxw-100px" type="number" step="1" name="default_payment_due_in_days" value="{{ !is_null($setting) ? $setting->default_payment_due_in_days : '' }}">
                        </div>
                        <div class="">
                            days
                        </div>
                    </div>
                    @error('default_payment_due_in_days')
                        <span class="error-message">{{ $message }}</span>
                    @enderror
                </div>
                {{-- <div class="form-field m-0">
                    <label for="">Memo</label>
                    <textarea name="memo" id="" cols="" rows="3" placeholder="Memo">{{ !is_null($setting) ? $setting->memo : '' }}</textarea>
                    @error('memo')
                        <span class="error-message">{{ $message }}</span>
                    @enderror
                </div> --}}
            </div>
        </div>

        <div class="text-right">
            <button type="submit" class="button--primary">Save Changes</button>
        </div>

    </form>
</div>
@endsection

@section('scripts')
<script>
var logoPlaceholderUrl = "{{ asset('img/essential/logo_placeholder.png') }}";
var logoUrl = "{{ $setting ? $setting->full_logo_url : '' }}";
</script>
<script>
$(function(){
    $('[name=default_lesson_rate_per_hour]').change(function(){
        var float = parseFloat($(this).val());
        var roundedFloat = Math.round( float * 100) / 100
        // Ensure that price input is 2 decimal places
        $(this).val( roundedFloat.toFixed(2) );
    });
    $('[name=default_payment_due_in_days]').change(function(){
        var val = parseInt($(this).val());
        $(this).val( val );
    });
    $('form.invoiceSettingForm').validate({
        errorClass: 'error-message',
        rules: {
            prefix: {
                required: true,
                minlength: 3,
                maxlength: 8
            },
            next_invoice_sequence: {
                min: 1
            }
        },
        submitHandler: function(form) {
            form.submit();
        }
    });
});
</script>
@endsection

