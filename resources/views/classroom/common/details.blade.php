<div class="panel p-0 mb-3">
    <div class="panel-header">Details</div>
    <div class="panel-body">
        @if( isset($resourceable) && !is_null($resourceable) )
            <div class="row">
                <div class="col-lg-8">
                    @foreach( $data as $key => $value )
                        @include('classroom.common.key_value_pair')
                    @endforeach
                </div>
                <div class="col-lg-4">
                    @foreach( $resourceable->resources as $resource )
                        @component('classroom.common.file_block', ['resource' => $resource, 'type' => $resource->pivot->type])
                        @endcomponent
                    @endforeach
                    @foreach( $resourceable->getCustomResources() as $custom_resource )
                        @component('classroom.common.file_block', ['custom_resource' => $custom_resource])
                        @endcomponent
                    @endforeach
                </div>
            </div>
        @else
            @foreach( $data as $key => $value )
                @include('classroom.common.key_value_pair')
            @endforeach
        @endif
    </div>
</div>
