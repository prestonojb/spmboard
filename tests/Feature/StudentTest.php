<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\UploadedFile;
use Tests\TestCase;
use App\ExamBoard;
use App\State;
use App\Student;
use App\User;

class StudentTest extends TestCase
{
    use WithFaker;

    public function setUp() : void
    {
        parent::setUp();

        // Student
        $user = factory(User::class)->states('student')->create();
        $this->student = $user->student;
    }

    /**
     * @group student
     * Test update student profile
     */
    public function testUpdate()
    {
        Storage::fake('s3');

        // Update arguments
        $image = UploadedFile::fake()->image('avatar.png');

        $name = $this->faker->word;
        $about = $this->faker->sentence;
        $state_id = State::first()->id;
        $school = $this->faker->sentence;
        $exam_board_id = ExamBoard::first()->id;

        // POST request to update profile
        $response = $this->actingAs($this->student->user)
                        ->patch( route('student.update'), [
                            'profile_img' => $image,
                            'name' => $name,
                            'about' => $about,
                            'state' => $state_id,
                            'school' => $school,
                            'exam_board' => $exam_board_id,
                        ]);

        // Refresh student model after update
        $this->student = Student::find( $this->student->user_id );

        //Current Month and Year
        $my = date('FY', time());
        $path = 'users/'. $my.'/'.$image->hashName();

        // Assert student avatar not updated
        Storage::disk('s3')->assertExists( $path, "Student 'avatar' image is not uploaded!" );
        $this->assertEquals( $this->student->avatar_relative_url, $path, "Student 'avatar' is not updated!" );

        // Assert student is already updated
        $this->assertEquals( $this->student->name, $name, "Student 'name' is not updated!" );
        $this->assertEquals( $this->student->about, $about, "Student 'about' is not updated!" );
        $this->assertEquals( $this->student->state->id, $state_id, "Student 'state' is not updated!" );
        $this->assertEquals( $this->student->school, $school, "Student 'school' is not updated!" );
        $this->assertEquals( $this->student->exam_board->id, $exam_board_id, "Student 'exam_board' is not updated!" );

        // Assert redirect
        $response->assertRedirect();
    }
}
