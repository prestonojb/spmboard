<?php

namespace App\Http\Controllers;

use App\Classroom;
use App\Lesson;
use App\Notifications\Classroom\ResourcePostPosted;
use App\PastPaper;
use App\ResourcePost;
use App\ResourcePostRating;
use App\Subject;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

class ClassroomResourcePostController extends Controller
{
    // public function index(Lesson $lesson)
    // {
    //     $classroom = $lesson->classroom;
    //     $chapters = $classroom->subject->chapters()->whereHas('resource_posts')->orderBy('chapter_number')->get();

    //     $subject = $classroom->subject;
    //     return view('classroom.resource_posts.index', compact('lesson', 'classroom', 'chapters', 'subject'));
    // }

    /**
     * Show Resource Post
     */
    public function show(Lesson $lesson, ResourcePost $resource_post)
    {
        $this->authorize('show', $lesson->classroom);

        $classroom = $lesson->classroom;
        $subject = $classroom->subject;

        return view('classroom.resource_posts.show', compact('lesson', 'classroom', 'resource_post', 'subject'));
    }

    /**
     * Create Resource Post
     */
    public function create(Lesson $lesson)
    {
        $this->authorize('update', $lesson);

        $classroom = $lesson->classroom;
        $subject = $classroom->subject;
        $chapters = $subject->chapters;

        return view('classroom.resource_posts.create', compact('lesson', 'classroom', 'subject', 'chapters'));
    }

    /**
     * Store Resource Post
     */
    public function store(Lesson $lesson)
    {
        $this->authorize('update', $lesson);
        request()->validate([
            'title' => 'required',
            'description' => 'nullable',
        ]);

        $classroom = $lesson->classroom;

        // Process data
        $resource_data = json_decode( request()->resource_data ) ?? [];
        $custom_resource_data = json_decode( request()->custom_resource_data ) ?? [];

        // Create Resource Post
        $resource_post = $lesson->resource_posts()->create([
                            'title' => request()->title,
                            'description' => request()->description,
                            'user_id' => auth()->user()->id,
                            'chapter_id' => request()->chapter,
                        ]);

        // Attach resources to resource post
        $resource_post->storeResources( $resource_data, $custom_resource_data );

        // Send Resource Post Posted to students in classroom
        Notification::send( $classroom->students, new ResourcePostPosted($lesson, $resource_post) );

        Session::flash('success', 'Resources posted successfully!');
        return redirect()->route('classroom.lessons.resource_posts.show', [$lesson->id, $resource_post->id]);
    }

    /**
     * Edit Resource Post
     */
    public function edit(Lesson $lesson, ResourcePost $resource_post)
    {
        $this->authorize('update', $resource_post);

        $classroom = $lesson->classroom;
        $chapters = $classroom->subject->chapters;

        return view('classroom.resource_posts.edit', compact('classroom', 'lesson', 'resource_post', 'chapters'));
    }

    /**
     * Update Resource Post
     */
    public function update(Lesson $lesson, ResourcePost $resource_post)
    {
        $this->authorize('update', $resource_post);
        request()->validate([
            'title' => 'required',
            'description' => 'nullable',
            'chapter' => 'nullable',
        ]);

        // Update resource post
        $resource_post->update([
            'title' => request()->title,
            'description' => request()->description,
            'chapter_id' => request()->chapter,
        ]);

        Session::flash('success', 'Resource Post updated successfully!');
        return redirect()->route('classroom.lessons.resource_posts.show', [$lesson->id, $resource_post->id]);
    }

    /**
     * Delete Resource Post
     */
    public function delete(Lesson $lesson, ResourcePost $resource_post)
    {
        $this->authorize('delete', $resource_post);

        // Delete resource post related relationships and itself
        $resource_post->cleanDelete();

        Session::flash( 'error', 'Resource Post deleted successfully!' );
        return redirect()->route('classroom.lessons.show', [$lesson->classroom->id, $lesson->id]);
    }

    /**
     * Rate Resource Post
     * Only students are able to rate resource post
     */
    public function rate( Lesson $lesson, ResourcePost $resource_post )
    {
        $this->authorize('rate', $resource_post);
        request()->validate([
            'rating' => 'required|integer|between:0,5',
        ]);
        $user = auth()->user();
        $student = $user->student;
        $rating = request('rating');

        // Update or add resource post rating by student
        $resource_post->updateOrAddRating($student, $rating);

        return $rating;
    }
}
