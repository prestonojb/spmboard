<div class="panel ml-2 mb-3 answerBlock" id="answer_{{ $answer->id }}">
    <div class="question-block__header">
        <label class="d-block">Answer</label>

        <ul class="nav question-block__icon-group mb-1">
            <button class="answer-block__award @if($answer->awarded_points) awarded @endif" data-answerid="{{ $answer->id }}" data-toggle="popover" data-trigger="focus" data-placement="top" data-content="Lights up when points are awarded to this answer.">
                <i class="fas fa-award"></i>
            </button>
            <button class="answer-block__check @if($answer->checked) checked @endif" data-answerid="{{ $answer->id }}" data-toggle="popover" data-trigger="focus" data-placement="top" data-content="Lights up when answer is accepted.">
                <i class="far fa-check-circle"></i>
            </button>
            <button class="answer-block__star @if($answer->recommended) recommend @endif" data-answerid="{{ $answer->id }}" data-toggle="popover" data-trigger="focus" data-placement="top" data-content="Lights up when answer is recommended by us!">
                <i class="fas fa-star"></i>
            </button>
        </ul>
    </div>

    <div class="answer__user-header">
        <div class="answer-block__user-info">
            @if( $answer->user )
                <a href="{{ route('users.show', $answer->user->username) }}">
                    <img class="avatar" src="{{ $answer->user->avatar ?? asset('img/essential/no-profile-picture.jpg') }}">
                </a>
            @else
                <a href="javascript:void(0)">
                    <img class="avatar" src="{{ asset('img/essential/no-profile-picture.jpg') }}">
                </a>
            @endif
            <div class="mt-1 text">
                <div class="user-info">
                    @if( $answer->user )
                        <a class="username" href="{{ route('users.show', $answer->user->username) }}">{!! $answer->user->username_with_badge !!}</a>
                    @else
                        <a class="username" href="javascript:void(0)"><i>Deleted account</i></a>
                    @endif
                    {!! $question->user->user_role_icon_popover_html !!}
                    <span class="about">@if($answer->user->headline), {{ $answer->user->headline }} @endif</span>
                </div>
                <span class="time">Answered {{ $answer->created_at->longRelativeDiffForHumans() }}</span>
            </div>
        </div>

        <div class="btn-group" role="group">
            <button type="button" class="more-button" data-toggle="dropdown">
                <i class="fa fa-ellipsis-h" aria-hidden="true"></i>
            </button>

            <div class="dropdown-menu dropdown-menu-right">
                @can('recommend', $answer)
                    <button type="button" class="dropdown-item answer-recommend-button" data-url="{{ route('answers.recommend', $answer->id) }}">@if(!$answer->recommended) Recommend @else Unrecommend @endif</button>
                @endcan
                @can('edit', $answer)
                    <a class="dropdown-item" href="{{ route('answers.edit', $answer->id) }}">Edit answer</a>
                @endcan
                @can('delete', $answer)
                    <form method="POST" action="{{ route('answers.delete', $answer->id) }}">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="dropdown-item text-danger delete-button">Delete answer</button>
                    </form>
                @endcan
            </div>
        </div>
    </div>

    <div class="answer">
        <div class="tinymce-editor-contents m-0">
            {!! $answer->answer !!}
            @if(!is_null($answer->img))
                <img src="{{ $answer->img }}" class="">
            @endif
        </div>
    </div>

    <div class="question-block__button-group">
        <button type="button" class="answer-upvote-button @if($answer->upvoted) upvoted @endif" data-answerid="{{ $answer->id }}"><span class="icon"></span> <span class="text">Upvote</span> · <span class="answer-upvote-count" data-answerid="{{ $answer->id }}">{{ readableNumber($answer->upvote_count) }}</span></button>
        @can('check', $answer)
            <button type="button" class="answer-check-button @if($answer->checked) checked @endif" data-answerid="{{ $answer->id }}"><span class="icon"><i class="fas fa-check"></i></span> <span class="text">Accept</span></button>
        @endcan
    </div>


    <section class="answer__comments">
        <button class="answer__comment-button">Comments({{ $answer->comments->count() }})</button>
        <ul class="list-group list-group-flush answer__comment-list">

            @foreach($answer->comments as $comment)
                <li class="list-group-item answer__comment-item">
                    <span class="comment">{{ $comment->comment }} - <a href="{{ route('users.show', $comment->user->username) }}" style="font-size: inherit;">{{ $comment->user->username }}</a></span> <span class="time">{{ $comment->created_at->diffForHumans() }}</span>
                    @can('delete', $comment)
                    <form method="POST" action="{{ route('answer_comments.delete', $comment->id) }}" class="d-inline">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="delete-button"><i class="fa fa-trash" aria-hidden="true"></i></button>
                    </form>
                    @endcan
                </li>
            @endforeach
            @auth
                @can('store', App\AnswerComment::class)
                    <li class="list-group-item answer__comment-item">
                        <form action="{{ route('answer_comments.store', $answer->id) }}" method="POST" class="answer-comment-form">
                            @csrf
                            <input type="text" class="d-inline" name="answer_comment" required>
                            <button type="submit" class="button--primary">Add Comment</button>
                        </form>
                        @error('answer_comment')
                        <span class="error-message"><small>{{ $message }}</small></span>
                        @enderror
                    </li>
                @endcan
            @else
            <li class="list-group-item answer__comment-item"><a href="{{ route('login') }}" style="font-size: inherit;">Log in</a> to comment.</li>
            @endauth
        </ul>

    </section>
</div>
