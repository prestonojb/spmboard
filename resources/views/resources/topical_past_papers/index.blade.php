@extends('forum.layouts.app')

@section('meta')
<title>Topical Past Papers | {{ $exam_board }} {{ config('app.name') }}</title>
@endsection

@section('styles')
{{-- Magnific Popup core CSS file --}}
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.css">
@endsection

@section('content')
@include('common.header2')
<section class="notes">
    <header class="notes__header">
        <div class="container">
            <h1><b>Topical Past Papers</b></h1>
            <h2 class="h4">Claim topical past papers with your coins!</h2>
        </div>
    </header>
    <section class="container notes__body">

        <div class="row justify-content-center overflow-auto resource-subject-menu">
            <div class="col-3 col-md-2 col-lg-1 p-2 d-flex flex-column justify-content-center resource-subject-item">
                <a href="{{ url('topical-past-papers') }}" class="stretched-link"></a>
                <span class="text-center icon {{ request()->is('topical-past-papers') ? 'active' : '' }}"><span class="iconify" data-icon="bx:bxs-grid" data-inline="false" style="font-size: 55px;"></span></span>
                <span class="text-center label {{ request()->is('topical-past-papers') ? 'active' : '' }}">All</span>
            </div>
            @foreach($subjects as $subject)
                <div class="col-3 col-md-2 col-lg-1 p-2 d-flex flex-column justify-content-center resource-subject-item">
                    <a href="{{ route('topical_past_papers.index', [$exam_board, $subject->id]) }}" class="stretched-link"></a>
                    <span class="text-center icon @if(request('subject') && $subject->id == request('subject')->id) active @endif">{!! $subject->icon !!}</span>
                    <span class="text-center label @if(request('subject') && $subject->id == request('subject')->id) active @endif">{{ $subject->name }}</span>
                </div>
            @endforeach
        </div>

        <div class="row flex-lg-row-reverse">
            <div class="col-12 col-lg-3 p-2">
                @include('resources.topical_past_papers.common.filter')
            </div>

            <div class="col-12 col-lg-9 p-2">
                <ul class="nav nav-tabs nav--tabs">
                    <li class="nav-item">
                      <a class="nav-link active" href="javascript:void(0)">Available</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="{{ url('topical-past-papers/owned') }}">Owned</a>
                    </li>
                </ul>
                <div>
                    <div>
                        @if($past_papers->count() > 0)

                        <div class="notes__results-count">Showing {{ $past_papers->firstItem() }} - {{ $past_papers->lastItem() }} of {{ $past_papers->total() }} results</div>

                        @foreach($past_papers as $paper)
                            @include('common.topical_past_paper')
                        @endforeach

                        {{ $past_papers->appends(request()->query())->links() }}

                        @else
                            <div class="note__empty">
                                <img src="{{ asset('img/essential/empty.svg') }}" class="mb-2" width="150">
                                <h4>No papers yet</h4>
                                <h5>Stay tuned for more.</h5>
                            </div>
                        @endif

                    </div>
                  </div>
            </div>
        </div>
    </section>
</section>
@endsection

@section('scripts')
{{-- Sweet Alert 2 --}}
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
{{-- Magnific Popup core JS file --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.js"></script>
<script src="{{ asset('js/slim_select.js') }}"></script>
<script src="{{ asset('js/topical-past-papers.js') }}"></script>
@endsection
