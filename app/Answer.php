<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\AnswerSortable;
use Illuminate\Support\Facades\Storage;

class Answer extends Model
{
    use AnswerSortable;

    protected $guarded = [];
    protected $appends = ['upvote_count'];

    /**
     * -----------------
     * Relationships
     * -----------------
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function answer_upvotes()
    {
        return $this->hasMany(AnswerUpvote::class);
    }

    public function question()
    {
        return $this->belongsTo(Question::class);
    }

    public function comments()
    {
        return $this->hasMany(AnswerComment::class);
    }

    public function recommender()
    {
        return $this->belongsTo(User::class, 'recommended_by_user_id', 'id');
    }

    public function coinActions()
    {
        return $this->morphOne(CoinAction::class, 'coin_actionable');
    }

    /**
     * -----------------
     * Accessors
     * -----------------
     */

    /**
     * Recommendation state
     * 0 = NULL
     * 1 = NOT NULL
     * ------------------------------------------------------------------------------------
     * recommended_by_user_id |  recommended_at  | State
     *            0                    0           Not recommended
     *            0                    1           Not defined
     *            1                    0           Was recommendeded and then unrecommended
     *            1                    1           Recommended
     *
     * @return boolean $isRecommended
     */
    public function getRecommendedAttribute(){
        return !is_null($this->recommended_at) && !is_null($this->recommended_by_user_id);
    }

    // Return boolean value of whether answer is awarded coins
    public function getAwardedPointsAttribute()
    {
        if(!$this->question->hasRewardPointBounty()) {
            return false;
        }
        return RewardPointAction::where([
                    'name' => 'Checked Answer',
                    'reward_point_actionable_id' => $this->id,
                    'reward_point_actionable_type' => Answer::class
                ])->exists();
    }

    public function getUpvoteCountAttribute()
    {
        return $this->answer_upvotes->count();
    }

    public function getUpvotedAttribute(){
        if(empty(auth()->user())){
            return false;
        }
        $upvoted = AnswerUpvote::where('answer_id', '=', $this->id)->where('user_id', '=', auth()->user()->id)->exists();
        return $upvoted;
    }

    public function getImgAttribute($value)
    {
        if($value){
            if(strpos($value, 'amazonaws')){
                //Amazon link for image
                return $value;
            } else{
                //Amazon S3
                $updated_value = str_replace('\\', '/', $value);
                return Storage::cloud()->url($updated_value);
            }
        } else {
            return null;
        }
    }

    /**
     * Returns show URL
     * @return string
     */
    public function getShowUrlAttribute()
    {
        return route('questions.show', [$this->question->subject->exam_board->slug, $this->question->id]).'#'.$this->id;
    }


    /**
     * Get recommended answers for given user
     *
     *  @param App\User $user
     *  @return Illuminate\Database\Eloquent\Collection $recommended_answers
     */

    public static function recommendedAnswersForUser($user) {
        $recommended_answers = Answer::where('user_id', $user->id)
                                ->whereNotNull('recommended_at')
                                ->withCount('answer_upvotes')
                                ->orderByDesc('answer_upvotes_count')
                                ->get();

        foreach($recommended_answers as $answer) {
            $answer['recommended'] = true;
        }

        return $recommended_answers;
    }

    /**
     * Get most upvoted answers for given user
     *
     *  @param App\User $user
     *  @param array $except
     *  @param int $noOfAnswers
     *  @return Illuminate\Database\Eloquent\Collection $most_upvoted_answers
     */
    public static function mostUpvotedAnswersForUser($user, $except = null, $noOfAnswers = 5) {
        $most_upvoted_answers = Answer::where('user_id', $user->id)
                                    ->whereNotIn('id', $except)
                                    ->withCount('answer_upvotes')
                                    ->orderByDesc('answer_upvotes_count')
                                    ->take($noOfAnswers)
                                    ->get();

        foreach($most_upvoted_answers as $answer) {
            $answer['most_upvoted'] = true;
        }

        return $most_upvoted_answers;
    }


    public static function topAnswersForUser($user, $noOfAnswersToReturn = 5)
    {
        $top_answers = collect();

        // Merge recommended and top answers
        $recommended_answers = Answer::recommendedAnswersForUser($user);
        $top_answers = $top_answers->merge($recommended_answers);

        // Calculate the number of answers left to be returned
        $noOfAnswersLeft = $noOfAnswersToReturn - count($top_answers);

        if($noOfAnswersLeft > 0) {
            $most_upvoted_answers = Answer::mostUpvotedAnswersForUser($user, $top_answers->pluck('id'), $noOfAnswersLeft);
            $top_answers = $recommended_answers->merge($most_upvoted_answers);
        }

        return $top_answers;
    }

    public static function allAnswersForUser($user, $perPage = 10)
    {
        $answers = collect();

        $recommended_answers = Answer::recommendedAnswersForUser($user); // Get recommended questions for user
        $answers = $answers->merge($recommended_answers);

        $noOfAnswersLeft = Answer::all()->count() - count($answers);

        // Get most upvoted questions by user, excluding recommended questions
        $most_upvoted_answers = Answer::mostUpvotedAnswersForUser($user, $answers->pluck('id'), $noOfAnswersLeft);

        $answers = $answers->merge($most_upvoted_answers);

        return collectionPaginate($answers, $perPage);
    }
}
