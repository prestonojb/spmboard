@component('mail::message')
# Homework Reminder
## Hi {{ $student->name ?? $student->username }}, just a friendly reminder to submit your homework before {{ $homework->due_at->toFormattedDateString() }}.

@component('mail::panel')
### Homework Details
Title: {{ $homework->title }}<br>
Instruction: {{ $homework->instruction }}<br>
Class: {{ $classroom->name }}
@endcomponent

@component('mail::button', ['url' => $homework_url, 'color' => 'primary'])
View homework
@endcomponent

Best regards,<br>
{{ $teacher->name ?? $teacher->username }}

@endcomponent
