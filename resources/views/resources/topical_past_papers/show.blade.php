@extends('resources.layouts.app')
@section('meta')
    <title>{{ $topical_past_paper->name }} - Topical Worksheets | {{ $exam_board->name }} {{ config('app.name') }} </title>
@endsection

@section('header')
    @component('resources.common.header', ['type' => 'topical_past_paper', 'exam_board' => $exam_board])
    @endcomponent
@endsection

@section('breadcrumbs')
    @component('resources.common.breadcrumbs')
    <a href="{{ route('topical_past_papers.home', $exam_board->slug) }}">Home</a> /
    <a href="{{ route('topical_past_papers.subject_show', [$exam_board->slug, $topical_past_paper->subject_id]) }}">{{ $topical_past_paper->subject->name }}</a> /
    {{ str_limit($topical_past_paper->name, 30) }}
    @endcomponent
@endsection

@section('context_panel')
    @component('resources.common.context_panel', ['exam_board' => $topical_past_paper->subject->exam_board->name, 'title' => $topical_past_paper->name, 'description' => $topical_past_paper->description, 'locked' => !Gate::allows('view_unlocked', $topical_past_paper->resource)])
        {{-- Ratings --}}
        <div class="position-relative d-inline-flex flex-wrap align-items-center mb-2">
            <div class="Stars sm mb-1 mr-2" style="--rating: {{ $topical_past_paper->resource->rating }};"></div>
            <a href="#reviews" class="blue5 underline-on-hover">{{ $topical_past_paper->resource->getTotalReviews() }} reviews</a>
        </div>

        <h3 class="font-size3">Part of <a href="{{ route('topical_past_papers.subject_show', [$topical_past_paper->subject->exam_board->slug, $topical_past_paper->subject->id]) }}" class="font-size-inherit blue-underline">{{ $topical_past_paper->subject->name }}</a> |
            <a href="javascript:void(0)" class="font-size-inherit blue-underline">{{ $topical_past_paper->chapter->name }}</a>
        </h3>

        {{-- Toolbar --}}
        <div class="py-1 my-1 border-left-gray5-3px pl-2">
            {{-- View as PDF --}}
            {{-- <a href="@can('download_pdf', $topical_past_paper) {{ $is_question ? $topical_past_paper->qp_url : $topical_past_paper->ms_url }} @else javascript:void(0) @endcan" @can('download_pdf', $topical_past_paper) target="_blank" @endcan class="gray-button sm mr-2 @cannot('download_pdf', $topical_past_paper) disabled @endcannot">
                <span class="iconify" data-icon="carbon:pdf-reference" data-inline="false"></span>View</a> --}}

            {{-- Download As PDF --}}
            <a href="@can('download_pdf', $topical_past_paper) {{ $is_question ? $topical_past_paper->qp_url : $topical_past_paper->ms_url }} @else javascript:void(0) @endcan" @can('download_pdf', $topical_past_paper) target="_blank" @endcan class="gray-button sm mr-2 @cannot('download_pdf', $topical_past_paper) disabled @endcannot">
                <span class="iconify" data-icon="carbon:generate-pdf" data-inline="false"></span>Download</a>
            {{-- Save --}}
            <button type="button" class="gray-button sm" data-toggle="modal" data-target="#saveToLibraryModal"  >
                <span class="iconify" data-icon="cil:playlist-add" data-inline="false"></span>Save</button>

            @component('resources.common.save_to_library', ['resource' => $topical_past_paper->resource])
            @endcomponent
        </div>
    @endcomponent
@endsection

@section('body')
    <div class="d-flex flex-wrap justify-content-between align-items-center mb-2">
        <span class="tag--primary lg text-uppercase mb-1">{{ $is_question ? 'Question Paper' : 'Mark Scheme' }}</span>

        @if($is_question)
            @if($topical_past_paper->hasMs())
                <a href="{{ $topical_past_paper->ms_route }}" class="button--primary lg mb-1">Switch to Mark Scheme
                    <span class="iconify" data-icon="codicon:arrow-right" data-inline="false"></span></a>
                @endif
        @else
            <a href="{{ $topical_past_paper->qp_route }}" class="button--primary--inverse lg mb-1">Switch to Question Paper
                <span class="iconify" data-icon="codicon:arrow-right" data-inline="false"></span></a>
        @endif
    </div>
@endsection

@section('side')
    <h4 class="mb-3 pb-2 border-bottom">More Worksheets from {{ $topical_past_paper->chapter->name }}</h4>
    @if(count($topical_past_paper->getRelatedPapersByChapter() ?? []) > 0)
        @foreach($topical_past_paper->getRelatedPapersByChapter() as $paper)
            <div class="mb-2">
                @component('resources.common.panel', ['resource' => $paper->resource])
                @endcomponent
            </div>
        @endforeach
    @else
        <div class="gray2 text-center ">No suggestion</div>
    @endif
@endsection

@section('main')
    {{-- Edit --}}
    @can('edit', $topical_past_paper->resource)
        <a href="{{ route('topical_past_papers.edit', [$exam_board->slug, $topical_past_paper->id]) }}" class="d-inline-block underline-on-hover mr-1 mb-2"><span class="iconify" data-icon="bx:bx-edit" data-inline="false"></span> Edit</a>
    @endcan

    {{-- Delete --}}
    @can('delete', $topical_past_paper->resource)
        <form action="{{ route('topical_past_papers.delete', [$exam_board->slug, $topical_past_paper->id]) }}" method="POST" class="d-inline">
            @csrf
            @method('delete')
            <button class="red button-link underline-on-hover deleteButton">
                <span class="iconify" data-icon="carbon:delete" data-inline="false"></span>
                Delete
            </button>
        </form>
    @endcan

    @php
        $tag = 'resource';
        $type = $is_question ? 'Question' : 'Answer';
        $description = '[Topical Worksheet ID: '.$topical_past_paper->id.'] '.$topical_past_paper->name.' ('.$type.')'.' is not working as expected.';
        $email = auth()->check() ? auth()->user()->email : '';
    @endphp
    @component('resources.common.share_and_report_toolbar', ['tag' => $tag, 'description' => $description, 'email' => $email])
    @endcomponent

    {{-- Tags --}}
    <div class="mb-1">
        @if($topical_past_paper->isFree())
            <a href="javascript:void(0)" class="status-tag lg red mr-1">Free</a>
        @endif
        <a href="javascript:void(0)" class="status-tag lg {{ $topical_past_paper->type_color }}">{{ $topical_past_paper->display_type }}</a>
    </div>

    {{-- Info --}}
    @component('resources.sections.info', ['resource' => $topical_past_paper->resource])
    @endcomponent

    <div class="py-1"></div>

    @can('view_unlocked', $topical_past_paper->resource)
        {{-- Full --}}
        <div class="w-100 custom_pdf_viewer" data-url="{{ $is_question ? $topical_past_paper->qp_url : $topical_past_paper->ms_url }}"></div>
    @else
        <div class="position-relative">
            {{-- Preview --}}
            <div class="w-100 custom_pdf_viewer_preview" data-max-page-preview="2" data-url="{{ $is_question ? $topical_past_paper->qp_url : $topical_past_paper->ms_url }}"></div>
            <div class="d-flex justify-content-between align-items-center text-white bg-blue6 custom-pdf-preview-prompt-block">
                <div>
                    <p class="mb-2 font-size2">End of preview</p>
                    <p class="font-size6 font-weight-bold">Unlock this study material with your coins!</p>
                    @if(auth()->user()->is_student())
                        <p class="font-size3"><i>You can save <b>{{ setting('board_pass.resource_discount_in_percentage') }}%</b> per unlock with Board Pass! <a target="_blank" href="{{ route('board_pass') }}" class="text-white"><u>Learn More</u></a></i></p>
                    @elseif(auth()->user()->is_teacher())
                        <p class="font-size3"><i>You can save <b>{{ setting('board_premium.resource_discount_in_percentage') }}%</b> per unlock with Board Premium! <a target="_blank" href="{{ route('classroom.prices.index') }}" class="text-white"><u>Learn More</u></a></i></p>
                    @endif
                </div>

                <div class="d-flex justify-content-center align-items-center">
                    <form action="{{ route('topical_past_papers.unlock', [$exam_board->slug, $topical_past_paper->id]) }}" method="post">
                        @csrf
                        <button type="submit" class="button--primary--inverse confirmButton" data-title="Are you sure?" data-text="You are unlocking {{ $topical_past_paper->name }} with {{ $topical_past_paper->getAdjustedCoinPrice(auth()->user()) }} coins!">
                            @if($topical_past_paper->getAdjustedCoinPrice(auth()->user()) != $topical_past_paper->coin_price)
                                <span class="d-inline-flex flex-column flex-md-row align-items-center">
                                    <img src="{{ asset('img/essential/coin.svg') }}" class="inline-image">
                                    <span>
                                        <span class="blue6 d-inline-flex flex-column mx-1">
                                            <span>
                                                <b class="font-size7">{{ $topical_past_paper->getAdjustedCoinPrice(auth()->user()) }}</b>
                                                <s>{{ $topical_past_paper->coin_price }}</s>
                                            </span>
                                            <span class="blue6">coins</span>
                                        </span>
                                    </span>
                                </span>
                            @else
                                <img src="{{ asset('img/essential/coin.svg') }}" class="inline-image">
                                <span class="blue6">
                                    <b>{{ $topical_past_paper->coin_price }} coins</b>
                                </span>
                            @endif
                        </button>
                    </form>
                </div>
            </div>
        </div>
    @endcan

    @component('resources.common.share_and_report_toolbar', ['tag' => $tag, 'description' => $description, 'email' => $email])
    @endcomponent

    <div class="py-3"></div>

    {{-- Author --}}
    <div class="border-top border-bottom">
        @component('resources.common.author_details', ['user' => $topical_past_paper->owner])
        @endcomponent
    </div>

    <div class="py-4"></div>

    {{-- Write a Review --}}
    @component('resources.sections.review', ['resource' => $topical_past_paper->resource, 'exam_board' => $exam_board])
    @endcomponent
@endsection
