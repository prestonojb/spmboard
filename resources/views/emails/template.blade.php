@component('mail::message')
# {{ $heading }}
@if(isset($subheading) && !empty($subheading))
## {{ $subheading }}
@endif

{{ $body }}


Regards,
The Board Team

** This email is auto-generated. If you need support, please email to hello@boardedu.org.*

@endcomponent
