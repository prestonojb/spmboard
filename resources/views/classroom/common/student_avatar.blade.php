<div class="d-flex justify-content-start align-items-center">
    @can('view_student', $classroom)
        @php $url = route('classroom.students.show', $student->user_id).(isset($classroom) ? '?class_selected='.$classroom->id : ''); @endphp
    @else
        @php $url = 'javascript:void(0)'; @endphp
    @endcan
    <a href="{{ $url }}">
        <img class="avatar mr-2" src="{{ $student->avatar }}">
    </a>
    <div class="d-flex flex-column justify-content-start align-items-start">
        <a class="inactive font-size3 font-weight500" href="{{ $url }}">{{ $student->username }}</a>
        @if( isset($subtitle) && !is_null($subtitle) )
            <span class="gray2 font-size1">{{ $subtitle }}</span>
        @endif
    </div>
</div>
