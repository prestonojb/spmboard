@extends('forum.layouts.app')

@section('meta')
    <title>Getting Started as a Teacher | {{ config('app.name') }}</title>
@endsection

@section('content')
@include('main.navs.header')

<div class="py-4"></div>

<section class="ask-wrapper">
    <div class="text-center">
        <h1 class="heading">Getting Started as a Teacher</h1>
        <h2 class="sub-heading mb-3">Tell us more about yourself so we can bring your teaching to the next level, together!</h2>
    </div>

    @component('teacher.forms.profile', ['teacher' => auth()->user()->teacher, 'states' => $states, 'getting_started' => true])
    @endcomponent
</section>

<div class="py-4"></div>

@endsection
