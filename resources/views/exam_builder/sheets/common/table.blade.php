@if(!is_null($sheets))
    <p class="gray2">Showing {{ $sheets->firstItem() ?? 0 }}-{{ $sheets->lastItem() ?? 0 }} of {{ $sheets->total() ?? 0 }} results</p>
@endif

<div class="table-responsive">
    <table class="table panel mb-2">
        <thead>
            <tr>
                <th scope="col">Name</th>
                <th scope="col">Generated At</th>
                <th scope="col">Subject</th>
                <th scope="col">Chapter(s)</th>
                <th scope="col">Actions</th>
            </tr>
        </thead>
        <tbody>
            @if( count($sheets ?? []))
                @foreach( $sheets as $sheet )
                    <tr>
                        <th scope="row">{{ $sheet->name }}</th>
                        <td>{{ $sheet->created_at->format('M d, Y h:iA') }}</td>
                        <td>{{ $sheet->subject->name }}</td>
                        <td>{{ str_limit($sheet->chapters->implode('name', ', '), 100) }}</td>
                        <td>
                            <div class="d-flex">
                                @can('show', $sheet)
                                    <a href="{{ route('exam_builder.sheets.show', $sheet->id) }}" class="button--primary--inverse sm mr-1"><span class="iconify" data-icon="mdi:magnify-plus-outline" data-inline="false"></span></a>
                                @endcan
                                @if($sheet->hasQp() && auth()->user()->can('download_qp', $sheet))
                                    <a href="{{ $sheet->qp_download_route }}" class="button--success--inverse sm mr-1 "><span class="iconify" data-icon="carbon:document-blank" data-inline="false"></span></a>
                                @else
                                    <a href="javascript:void(0)" class="button--success--inverse sm mr-1 disabled"><span class="iconify" data-icon="carbon:document-blank" data-inline="false"></span></a>
                                @endif
                                @if($sheet->hasQp() && auth()->user()->can('download_ms', $sheet))
                                    <a href="{{ $sheet->ms_download_route }}" class="button--success--inverse sm mr-1 "><span class="iconify" data-icon="carbon:document-blank" data-inline="false"></span></a>
                                @else
                                    <a href="javascript:void(0)" class="button--success--inverse sm mr-1 disabled"><span class="iconify" data-icon="carbon:document-blank" data-inline="false"></span></a>
                                @endif

                                <form action="{{ route('exam_builder.sheets.delete', $sheet->id) }}" method="post">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="button--danger--inverse sm deleteButton" @cannot('delete', $sheet) disabled @endcannot><span class="iconify" data-icon="carbon:delete" data-inline="false"></span></button>
                                </form>
                            </div>
                        </td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td class="text-center" colspan="6">No worksheets found</td>
                </tr>
            @endif
        </tbody>
    </table>
</div>

@if( !is_null($sheets) )
    {{ $sheets->appends(['exam_board' => $exam_board->slug])->links() }}
@endif

