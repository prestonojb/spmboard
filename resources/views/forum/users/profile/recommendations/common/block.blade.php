<div class="row">
    <div class="col-12 col-md-4 mb-2">
        <div class="d-flex justify-content-start align-items-start">
            <a href="{{ $recommendation->recommender->show_url }}">
                <img class="avatar mr-2" src="{{ $recommendation->recommender->avatar ?? asset('img/essential/no-profile-picture.jpg') }}">
            </a>
            <div>
                <div>
                    <a class="username" href="{{ $recommendation->recommender->show_url }}">{{ $recommendation->recommender->username }}</a>
                    {!! $recommendation->recommender->user_role_icon_popover_html !!}
                </div>
                @if(!is_null($recommendation->recommender->about))
                    <p class="font-size3">
                        {{ $recommendation->recommender->about }}
                    </p>
                @endif
                <p class="font-size1 gray2">{{ $recommendation->created_at->toFormattedDateString() }} · {{ $recommendation->relationship }}</p>
            </div>
        </div>
    </div>
    <div class="col-12 col-md-8 mb-2">
        {!! nl2br($recommendation->description) !!}
    </div>
</div>
