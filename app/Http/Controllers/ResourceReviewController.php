<?php

namespace App\Http\Controllers;

use App\ResourceReview;
use Illuminate\Http\Request;
use App\ExamBoard;
use App\Notifications\NewResourceReviewAdded;
use App\Resource;

class ResourceReviewController extends Controller
{
    public function store(ExamBoard $exam_board, Resource $resource)
    {
        // Validation
        request()->validate([
            'rating' => 'required|integer|between:1,5',
            'comment' => 'nullable|string',
        ]);

        $user = auth()->user();

        // Create resource review
        $resource_review = $resource->reviews()->create([
            'user_id' => $user->id,
            'rating' => request('rating'),
            'comment' => request('comment'),
        ]);

        // Notification
        $resource->owner()->notify(new NewResourceReviewAdded($resource_review));

        return redirect()->back()->withSuccess('Review added successfully!');
    }

    public function update(ExamBoard $exam_board, ResourceReview $resource_review)
    {
        // Validation
        request()->validate([
            'rating' => 'required|integer|between:1,5',
            'comment' => 'nullable|string',
        ]);

        $user = auth()->user();

        // Update resource review
        $resource_review->update([
            'rating' => request('rating'),
            'comment' => request('comment'),
        ]);

        return redirect()->back()->withSuccess('Review updated successfully!');
    }
}
