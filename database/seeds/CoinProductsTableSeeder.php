<?php

use App\CoinProduct;
use App\CoinProductType;
use Illuminate\Database\Seeder;

class CoinProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CoinProduct::truncate();
        CoinProductType::truncate();

        // Create Board Pass Products
        $coin_product = CoinProduct::create([
                            'name' => 'Board Pass',
                            'description' => 'Access all resources with this Pass',
                            'img' => asset('img/essential/coin.svg'),
                        ]);
        // Create Board Pass Types
        $types = [
            ['name' => '1 month', 'price_in_coins' => 50, 'discounted_price_in_coins' => 50, 'is_active' => 1],
            ['name' => '3 month', 'price_in_coins' => 150, 'discounted_price_in_coins' => 150, 'is_active' => 1],
            ['name' => '6 month', 'price_in_coins' => 300, 'discounted_price_in_coins' => 300, 'is_active' => 0],
            ['name' => '1 year', 'price_in_coins' => 600, 'discounted_price_in_coins' => 600, 'is_active' => 0],
        ];
        foreach( $types as $type ) {
            $coin_product->types()->create([
                'name' => $type['name'],
                'price_in_coins' => $type['price_in_coins'],
                'discounted_price_in_coins' => $type['discounted_price_in_coins'],
                'is_active' => $type['is_active'],
            ]);
        }
    }
}
