<?php

namespace App\Http\Controllers;

use App\Chapter;
use App\ExamBoard;
use App\ExamBuilderItem;
use App\ExamBuilderSheet;
use App\Subject;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class ExamBuilderController extends Controller
{
    /**
     * Get Exam Board from request
     * @return \App\ExamBoard
     */
    public function getExamBoard()
    {
        return ExamBoard::where('slug', request('exam_board') )->first() ?? ExamBoard::where('slug', 'igcse')->first();
    }
    /**
     * List all user's exam-board specific exam builder sheets
     */
    public function landing()
    {
        $user = auth()->user();
        $exam_board = $this->getExamBoard();

        if( !is_null($user) ) {
            $sheets = $user->getSheetsByExamBoard($exam_board, true, 30);
        } else {
            $sheets = null;
        }

        return view('exam_builder.landing', compact('exam_board', 'sheets'));
    }

    public function getFilterView()
    {
        $this->authorize('generate_qp', ExamBuilderSheet::class);

        $exam_board = $this->getExamBoard();
        $subjects = $exam_board->getSubjectsWithItems();
        return view('exam_builder.filter', compact('exam_board', 'subjects'));
    }

    /**
     * Process request from filter exam builder form
     */
    public function filter(Request $request)
    {
        $this->authorize('generate_qp', ExamBuilderSheet::class);

        $user = auth()->user();
        $request->validate([
            'no_of_questions' => 'required|integer|between:1,'.$user->getExamBuilderQuestionLimit(), // Validate number of questions within limit
            'subject' => 'required',
            'chapters' => 'required|array|between:1,'.$user->getExamBuilderChapterLimit(), // Validate number of questions within limit
            'paper_number' => 'required',
        ]);
        $total_no_of_items = $request->no_of_questions;

        $subject = Subject::find( $request->subject );
        $exam_board = $subject->exam_board;
        $chapters = Chapter::find( $request->chapters );
        $paper_number = request('paper_number');

        // Check permission of user for subchapter access
        if( $user->cannot('generate_with_subchapter', ExamBuilderSheet::class) ) {
            foreach($chapters as $chapter) {
                if($chapter->isSubChapter()) {
                    return redirect()->back()->with('error', 'No permission to generate worksheets with subchapters!');
                }
            }
        }

        // Validate chapters
        // When parent chapter is present, subchapter must be absent and vice versa
        if( !$this->areValidChapters($chapters) ) {
            return redirect()->back()->withInput()->with('error', 'Parent chapter and subchapter cannot be selected at the same time, please try again.');
        }

        try {
            $items = ExamBuilderItem::getRandomItems($total_no_of_items, $chapters, $paper_number);
        } catch(Exception $e) {
            return redirect()->back()->withInput()->with('error', 'There are not enough questions for your filter. Please try again by selecting more chapters or choosing less number of questions!');
        }


        // Ensure number of questions is reflected correctly
        if( count($items) != request()->no_of_questions ) {
            return redirect()->back()->with('error', 'Failed to build exam paper!');
        }

        return view('exam_builder.customize', compact('exam_board', 'subject', 'chapters', 'paper_number', 'items'));
    }


    /**
     * Replace passed item with similar item that is not in the worksheet yet
     */
    public function replaceItem(ExamBuilderItem $exam_builder_item)
    {
        // Get all item IDs already in the worksheet
        $item_ids = json_decode( request('item_ids') );
        return $exam_builder_item->replace( $item_ids );
    }

    /**
     * Returns Exam Builder Item HTML String
     * @return string
     */
    public function getBlockHtml(ExamBuilderItem $exam_builder_item)
    {
        return $exam_builder_item->html;
    }

    /**
     * Get available paper numbers by subject
     */
    public function getPaperNumbersBySubject(Subject $subject)
    {
        return $subject->getAvailableExamBuilderPaperNumbers();
    }

    /**
     * Returns true if chapters are valid, else false
     * When parent chapter is present, subchapter must be absent and vice versa
     * @param Collection $chapters
     * @return boolean
     */
    public function areValidChapters($chapters)
    {
        $chapter_ids = $chapters->pluck('id')->toArray();
        foreach($chapters as $chapter) {
            // Chapter is Parent Chapter
            // Ensure there is no child chapters of chapter present
            if($chapter->isParentChapter()) {
                // Get subchapter IDs
                $subchapter_ids = $chapter->getRelatedChapterIds(false);
                // If subchapter id is present in chapter IDs, return false
                if( count( array_intersect($subchapter_ids, $chapter_ids) ) > 0 ) {
                    return false;
                }
            } else {
                // Chapter is child chapter
                // Ensure parent chapter of chapter is not present
                // If parent chapter ID is present in chapter IDs, return false
                if( in_array($chapter->parent_chapter_id, $chapter_ids) ) {
                    return false;
                }
            }
        }
        return true;
    }
}
