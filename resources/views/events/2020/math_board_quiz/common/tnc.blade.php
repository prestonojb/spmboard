<div class="modal fade" id="quizTncModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Terms and Condition</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        <div class="modal-body">
            <div class="mb-3">
                <h3 class="mb-1">Eligibility</h3>
                <p>The 2020 Math Board Quiz is open to current SPM and IGCSE students, further verification might be required to verify the validity of the contestants.</p>
                <p>Failure to verify your student identity might cause your entry to be disqualified.</p>
            </div>

            <div class="mb-3">
                <h3 class="mb-1">Data Consent</h3>
                <p>On submission of your completed entry, you consent to your email address being added to the Board database and periodically will be sent newsletters from Board. For the avoidance of doubt, your email address will not be displayed or used for any other purpose; and
                    you will be automatically bound by these terms and conditions.</p>
                <p>By entering the 2020 Math Board Quiz, the winner also consents to the publishing of their name on Board website(<a href="https://boardedu.org" target="_blank">https://boardedu.org</a>).</p>
            </div>

            <div class="mb-3">
                <h3 class="mb-1">Entry</h3>
                <p>Entry into the 2020 Math Board Quiz is <b>FREE</b>.</p>
                <p>All entries must be received by 11:59pm (GMT) on 12th July 2020. Entries not submitted in accordance with these terms and conditions will not be accepted.
                    Entries that are incomplete or corrupted will be deemed invalid. No responsibility will be accepted by Board for corrupted entries.
                </p>
            </div>

            <div class="mb-3">
                <h3 class="mb-1">Winning Conditions</h3>
                <p>To stand a chance to become the winners, entrants must achieve a score of 5 / 5 in the 2020 Board Quiz. In the event that two or more entrants tie for the first prize for the 2020 Board Quiz, those entrants will be entered into a draw. The winner will be the first entry drawn at random by Board.</p>
            </div>

            <div class="mb-3">

                <h3 class="mb-1">Prize Disclaimer</h3>
                <p>The Board Quiz prizes cannot be redeemed for cash or benefits in kind such as gift vouchers. The prizes are non-transferable.
                    In the unlikely event that no entries are received before the closing date the prizes will not be awarded.</p>
            </div>

            <div class="mb-3">
                <h3 class="mb-1">Prize Giving Mechanims</h3>
                <p>The winners of the 2020 Board Quiz will be notified by Whatsapp after the closing date. The winner will be asked to provide proof of student ID within 14 days of being notified or they will forfeit the prize.
                    Method of despatch of prizes will be further clarified on Whatsapp.</p>
            </div>

            <div class="mb-3">
                <h3 class="mb-1">Legal</h3>
                <p>To the fullest extent permitted by law, Board hereby excludes all warranties, representations, covenants and liability (whether express or implied) regarding the prize or a prize draw.</p>
            </div>

            <div class="mb-3">
                <h3 class="mb-1">Disclaimer</h3>
                <p>In the event of any dispute regarding the 2020 Math Board Quiz, the decision of Board shall be final and binding and no correspondence or discussion shall be entered into.
                    At any time prior to the closing date, Board may withdraw or cancel the 2020 Board Quiz for any reason and may change the prize to a prize of a similar value. Any such cancellation or change shall be notified on Board website.</p>
                <p>Board reserves the right to cancel the Board Quiz and reject redemption of any Board Quiz Prizes if it reasonably believes that the Board Quiz or any prize is being used unlawfully or illegally.</p>
            </div>

            <div class="mb-3">
                <h3 class="mb-1">Change of Terms</h3>
                <p>Board reserves the right to amend these terms and conditions without prior notice. Any changes will be posted on Board website located at <a href="https://boardedu.org" target="_blank">https://boardedu.org</a></p>
            </div>

        </div>
        <div class="modal-footer">
            <button type="button" class="button--primary--inverse" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
</div>
