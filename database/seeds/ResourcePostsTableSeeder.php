<?php

use App\Lesson;
use App\Resource;
use App\ResourcePost;
use App\ResourcePostRating;
use App\Teacher;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ResourcePostsTableSeeder extends Seeder
{
    protected $NO_OF_RESOURCE_POSTS = 50;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ResourcePost::truncate();
        DB::table('resource_resource_post')->delete();
        $faker = \Faker\Factory::create();

        /**
         * Create resource posts, then attach 0-5 resources to it
         */

        $faker = \Faker\Factory::create();

        for($i = 0; $i < $this->NO_OF_RESOURCE_POSTS; $i++) {
            $teacher = Teacher::inRandomOrder()->first();
            $lesson = Lesson::inRandomOrder()->first();
            $classroom = $lesson->classroom;

            $subject = $lesson->classroom->subject;
            $chapter = $subject->chapters->random()->first();
            // Get shuffled resource IDs array
            $resource_ids = Resource::getIdsBySubject( $subject );

            // Create resource post
            $resource_post = ResourcePost::create([
                                'user_id' => $teacher ? $teacher->user_id : null,
                                'lesson_id' => $lesson ? $lesson->id : null,
                                'chapter_id' => $chapter ? $chapter->id : null,
                                'title' => $faker->sentence(3),
                                'description' => $faker->optional()->paragraph,
                            ]);

            // Attach resources
            for($j = 0; $j <= rand(0, 2); $j++) {
                $resource_id = $resource_ids[ array_rand($resource_ids) ];
                $resource_post->resources()->syncWithoutDetaching([
                    // Pop a random resource ID from resource IDs array
                    $resource_id
                ]);
                unset($resource_ids[ array_rand($resource_ids) ]);
            }

            // Generate resource post rating
            if( count($classroom->students ?? []) ) {
                foreach( $classroom->students as $student ) {
                    // 2/3 chance that student has rating
                    if( rand(0, 2) ) {
                        $student_has_rating = $resource_post->ratings()->where('student_id', $student->user_id)->exists();
                        if( !$student_has_rating ) {
                            $resource_post->ratings()->create([
                                'student_id' => $student->user_id,
                                'rating' => rand(0, 5)
                            ]);
                        }
                    }
                }
            }
        }
    }
}
