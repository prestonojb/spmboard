$(function(){
    require('./resource/exam_builder');
    require('./resource/filter');
    require('./resource/custom_pdf_viewer');
    require('./resource/library');

    // Coin Price Select
    var $coinPriceSelect = $('select[name=coin_price]');
    $('[name=coin_price_toggle]').change(function(e){
        var $t = $(this);
        if($t.val() == 'free') {
            // Reset Coin Price Select
            $coinPriceSelect.val(0);
            $coinPriceSelect.prop('disabled', true);
        } else if ($t.val() == 'paid') {
            $coinPriceSelect.prop('disabled', false);
        }
    });

    // Bug Report Submit Button
    $('.bugReportSubmitButton').click(function(e){
        e.preventDefault();
        var $t = $(this);
        var $modal = $('#bugReportModal');
        var $form = $t.closest('form');

        var initHtml = $(this).html();
        window.showLoader( $t, 'in-button', 'white' );

        $.ajax({
            url: $form.attr('action'),
            method: 'POST',
            data: $form.serialize(),
            success: function(response) {
                if( response == 'OK' ) {
                    toastr.success('Bug Report submitted successfully!');
                    $modal.modal('hide');
                } else {
                    toastr.error('An error occured!');
                }
                window.hideLoader($t, initHtml);
            },
            error: function(e) {
                console.log(e);
                toastr.error('An error occured!');
                window.hideLoader($t, initHtml);
            }
        });
    });

    $('.resource__panel-dropdown-header').click(function(e){
        $(this).toggleClass('active');
    });

    $(document).on('click', 'a[href^="#"]', function(e) {
        // target element id
        var id = $(this).attr('href');

        // target element
        var $id = $(id);
        if ($id.length === 0) {
            return;
        }

        // prevent standard hash navigation (avoid blinking in IE)
        e.preventDefault();

        // top position relative to the document
        var pos = $id.offset().top;

        // animated top scrolling
        $('body, html').animate({scrollTop: pos - 142});
    });
});
