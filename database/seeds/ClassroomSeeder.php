<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ClassroomSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');

        $this->call(UsersTableSeeder::class);
        $this->call(ResourcesTableSeeder::class);

        $this->call(ClassroomsTableSeeder::class);
        $this->call(LessonsTableSeeder::class);
        $this->call(ResourcePostsTableSeeder::class);
        $this->call(HomeworksTableSeeder::class);
        $this->call(StudentReportsTableSeeder::class);
        $this->call(TeacherInvoicesTableSeeder::class);

        DB::statement('SET FOREIGN_KEY_CHECKS = 1');

    }
}
