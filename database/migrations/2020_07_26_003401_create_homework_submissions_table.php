<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHomeworkSubmissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('homework_submissions', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('homework_id');
            $table->unsignedInteger('student_id');

            $table->string('status')->default('submitted');
            $table->string('file_url');
            $table->text('comment')->nullable();

            $table->unsignedInteger('marks')->nullable();
            $table->string('marked_file_url')->nullable();

            $table->text('teacher_remark')->nullable();

            $table->timestamp('graded_at')->nullable();
            $table->timestamp('returned_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('homework_submissions');
    }
}
