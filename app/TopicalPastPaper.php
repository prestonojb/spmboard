<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;
use Illuminate\Database\Eloquent\SoftDeletes;

class TopicalPastPaper extends Model
{
    use SoftDeletes;

    protected $guarded = [];

    public function subject()
    {
        return $this->belongsTo(Subject::class, 'subject_id', 'id');
    }

    public function chapter()
    {
        return $this->belongsTo(Chapter::class);
    }

    public function owner()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function exam_board()
    {
        return $this->hasOneThrough(ExamBoard::class, Subject::class);
    }

    public function resource()
    {
        return $this->morphOne('App\Resource', 'resourceable');
    }

    public function qpIsAvailable()
    {
        return true;
    }

    public function msIsAvailable()
    {
        if( !auth()->check() ) {
            return false;
        }
        $user = auth()->user();
        if( $teacher = $user->teacher ) {
            return $teacher->hasResourceAccess();
        }
        return $user->hasActiveBoardPass();
    }

    /**
     * Returns true if topical worksheet has mark scheme
     * @return boolean
     */
    public function hasMs()
    {
        return !is_null($this->ms_url);
    }

    /*
     |---------------------------------
     | Accessors
     |---------------------------------
    */
    /**
     * Returns primary attribute
     * @return string
     */
    public function getPrimaryAttribute()
    {
        return $this->name;
    }

    /**
     * Returns secondary attribute
     * @return string
     */
    public function getSecondaryAttribute()
    {
        return "Paper {$this->paper_number}";
    }

    /**
     * Returns preview image URL
     * @return string
     */
    public function getPreviewImgAttribute($value)
    {
        if($value){
            if(strpos($value, 'amazonaws')){
                //Amazon link for image
                return $value;
            } else{
                //Amazon S3
                $updated_value = str_replace('\\', '/', $value);
                return Storage::cloud()->url($updated_value);
            }
        } else {
            return asset('img/essential/note.svg');
        }
    }

    /**
     * Returns all tags HTML
     * @return string
     */
    public function getAllTagsHtml()
    {
        $html = '';
        if(!is_null($this->chapter)) {
            $html .= '<a class="status-tag sm gray mr-1 maxw-100px" href="javascript:void(0)">'.$this->chapter->name.'</a>';
        }
        return $html;
    }

    /**
     * Returns show URL
     * @return string
     */
    public function getShowUrlAttribute()
    {
        return $this->qp_route;
    }

    // URLs
    /* ---
     * Question Paper URLs
     * ---
     */
    public function getQpUrlAttribute($value)
    {
        if(strpos($value, 'http') !== false || strpos($value, 'https') !== false){
            return $value;
        }
        return $value ? Storage::cloud()->url($value) : null;
    }

    /**
     * Returns QP url relative path
     */
    public function getUrlRelativePathAttribute()
    {
        return $this->getOriginal('qp_url');
    }

    public function getQpUrlRelativePathAttribute()
    {
        return $this->getOriginal('qp_url');
    }

    public function getQpRouteAttribute()
    {
        return route('topical_past_papers.qp.show', [$this->subject->exam_board->slug, $this->id]);
    }

    /* ---
     * Mark Scheme URLs
     * ---
     */
    public function getMsUrlAttribute($value)
    {
        if(strpos($value, 'http') !== false || strpos($value, 'https') !== false){
            return $value;
        }
        return $value ? Storage::cloud()->url($value) : null;
    }

    public function getMsUrlRelativePathAttribute()
    {
        return $this->getOriginal('ms_url');
    }

    public function getMsRouteAttribute()
    {
        return route('topical_past_papers.ms.show', [$this->subject->exam_board->slug, $this->id]);
    }

    /**
     * Get question paper file download route
     */
    public function getQpDownloadRouteAttribute()
    {
        return route('topical_past_papers.qp.download', [$this->subject->exam_board->slug, $this->id]);
    }
    // --URLs

    /**
     * Inverse hasOneThrough Query Builder Reference: https://stackoverflow.com/questions/39159285/laravel-5-hasmanythrough-inverse-querying
     * Returns an Eloquent builder of Topical Past Paper by exam board
     *
     * @param string $exam_board Exam Board name('SPM', 'IGCSE'...)
     * @return Eloquent Builder
     */
    public static function getPastPaperByExamBoard($exam_board)
    {
        return TopicalPastPaper::whereHas('subject.exam_board', function($q) use ($exam_board) {
            return $q->where('name', $exam_board->name);
        });
    }

    /**
     * Returns related papers by chapter
     */
    public function getRelatedPapersByChapter()
    {
        return $this->chapter->topical_past_papers()->where('id', '!=', $this->id)->take(3)->get();
    }


    /**
     * Returns true if note is locked to user, else false
     * @param App\User $user
     * @return boolean
     */
    public function isLocked(?User $user)
    {
        if(!is_null($user)) {
            return $this->isPaid() && !$user->hasResource($this->resource);
        } else {
            return false;
        }
    }

        /**
     * Returns true if note is free
     * @return boolean
     */
    public function isFree()
    {
        return $this->coin_price == 0;
    }

    /**
     * Returns true if note is paid
     * @return boolean
     */
    public function isPaid()
    {
        return $this->coin_price > 0;
    }

    /**
     * Save QP PDF file to S3
     * @param UploadedFile $file
     */
    public function saveQpFile($file)
    {
        $dir = 'resources/topical_past_papers/qp';
        //Store file to S3
        $file->store($dir, 's3');
        $filename = $file->hashName();
        //Save image url in database
        $file_url = $dir.'/'.$filename;
        $this->update([
            'qp_url' => $file_url,
        ]);
    }

    /**
     * Save QP PDF file to S3
     * @param UploadedFile $file
     */
    public function saveMsFile($file)
    {
        $dir = 'resources/topical_past_papers/ms';
        //Store file to S3
        $file->store($dir, 's3');
        $filename = $file->hashName();
        //Save image url in database
        $file_url = $dir.'/'.$filename;
        $this->update([
            'ms_url' => $file_url,
        ]);
    }

    /**
     * Returns adjusted coin price (based on buyer's subscription)
     * @param App\User $buyer
     * @return int
     */
    public function getAdjustedCoinPrice($buyer)
    {
        // Get purchase price
        if($buyer->is_teacher() && $buyer->teacher->hasPremiumSubscription()) {
            return getCoinPriceAfterDiscount($this->coin_price, setting('board_premium.resource_discount_in_percentage'));
        } elseif($buyer->is_student() && $buyer->hasActiveBoardPass()) {
            return getCoinPriceAfterDiscount($this->coin_price, setting('board_pass.resource_discount_in_percentage'));
        } else {
            return $this->coin_price;
        }
    }

    /**
     * Returns coin discount
     * @return int
     */
    public function getCoinDiscount($buyer)
    {
        if(!is_int($this->getAdjustedCoinPrice($buyer))) {
            throw new \Exception("Coin price must be an integer.");
        }
        return $this->coin_price - $this->getAdjustedCoinPrice($buyer);
    }

    /**
     * Save preview image (first page of PDF file)
     * @param UploadedFile $file
     */
    public function savePreviewImage($file)
    {
        $this->resource->savePreviewImage($file);
    }

    /**
     * Update preview image (first page of PDF file)
     */
    public function updatePreviewImage()
    {
        $this->resource->updatePreviewImage();
    }

    /**
     * Unlock resource for user
     */
    public function unlockFor(User $buyer)
    {
        $this->resource->unlockFor($buyer);
    }
}
