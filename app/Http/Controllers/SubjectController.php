<?php

namespace App\Http\Controllers;

use App\ExamBoard;
use App\Question;
use App\Subject;
use Illuminate\Http\Request;
use CyrildeWit\EloquentViewable\Support\Period;
use Illuminate\Pagination\LengthAwarePaginator;

class SubjectController extends Controller
{
    public $valid_sorts = array('hot', 'top', 'new', 'answered');

    public function show(ExamBoard $exam_board, Subject $subject)
    {
        $sort = request('sort');
        if(!is_null($sort) && !in_array($sort, $this->valid_sorts)) {
            $sort = 'hot';
        }

        $questions = Question::sortForSubject($subject);

        return view('forum.questions.index', compact('exam_board', 'subject', 'questions', 'sort'));
    }
}
