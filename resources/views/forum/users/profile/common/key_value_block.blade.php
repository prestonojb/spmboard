<div class="card border-left-{{ $datum['color'] }}-3px h-100 py-2">
    <div class="card-body">
        <div class="row no-gutters align-items-center">
            <div class="col mr-2">
                <div class="text-xs font-weight-bold {{ $datum['color'] }} text-uppercase mb-1">{{ $datum['key'] }}</div>
                <div class="h5 mb-0 font-weight-bold font-size7">{{ $datum['value'] }}</div>
            </div>
            <div class="col-auto">
                <i class="{{ $datum['icon'] }} {{ $datum['color'] }}"></i>
            </div>
        </div>
    </div>
</div>
