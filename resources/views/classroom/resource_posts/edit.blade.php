@extends('classroom.layouts.app')

@section('meta')
    <title>Edit Resource - {{ $resource_post->title }} | {{ config('app.name') }} Classroom</title>
@endsection

@section('content')
    @component('classroom.common.breadcrumbs')
        <a href="{{ route('classroom.classrooms.show', $classroom->id) }}">Dashboard</a> /
        <a href="{{ route('classroom.lessons.index', $classroom->id) }}">Lessons</a> /
        <a href="{{ route('classroom.lessons.show', [$lesson->classroom->id, $lesson->id]) }}">{{ $lesson->name }}</a> /
        <a href="{{ route('classroom.lessons.resource_posts.show', [$lesson->id, $resource_post->id]) }}">{{ $resource_post->title }}</a> /
        Edit
    @endcomponent

    @component('classroom.common.page_heading', ['title' => "Edit {$resource_post->title}"])
    @endcomponent

<div class="maxw-740px mx-auto my-5">
    <div class="panel mb-2">
        <form action="{{ route('classroom.lessons.resource_posts.update', [$lesson->id, $resource_post->id]) }}" method="POST" id="postResourcesForm">
            @csrf
            @method('PATCH')
            <div class="form-field">
                <label>Title</label>
                <input type="text" name="title" placeholder="Title" value="{{ old('title') ? old('title') : $resource_post->title }}">
                @error('title')
                    <span class="error-message">{{ $message }}</span>
                @enderror
            </div>

            <div class="form-field">
                <label>Description(Optional)</label>
                <textarea name="description" id="" rows="3" placeholder="Description">{{ old('description') ? old('description') : $resource_post->description }}</textarea>
                @error('description')
                    <span class="error-message">{{ $message }}</span>
                @enderror
            </div>

            <div class="form-field">
                <div class="form-field">
                    <label>Chapter(Optional)</label>
                    <select class="chapter-select" name="chapter">
                        <option selected disabled>Select Chapter</option>
                        @foreach($chapters as $chapter)
                            <option value="{{ $chapter->id }}"
                                @if( old('chapter') == $chapter->id )
                                    selected
                                @elseif( $resource_post->chapter && $resource_post->chapter->id == $chapter->id )
                                    selected
                                @endif>
                                {{ $chapter->name }}
                            </option>
                        @endforeach
                    </select>

                    @error('chapter')
                        <span class="error-message">{{ $message }}</span>
                    @enderror
                </div>
            </div>

            @component('classroom.resource_posts.common.file_block_container', ['resource_post' => $resource_post])
            @endcomponent

            <div class="text-center">
                <button type="submit" class="button--primary disableOnSubmitButton">Update</button>
            </div>
        </form>
    </div>
</div>
@endsection

@section('scripts')
@endsection
