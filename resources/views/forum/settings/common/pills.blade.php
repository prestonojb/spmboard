<ul class="nav nav-tabs nav-stacked mb-3">
    <li class="position-relative nav-pills {{ request()->is('settings')? 'active' : '' }}">
        <a href="{{ route('settings') }}" class="stretched-link">Profile</a>
    </li>
    <li class="position-relative nav-pills {{ request()->is('settings/password')? 'active' : '' }}">
        <a href="{{ route('settings.password') }}" class="stretched-link">Password</a>
    </li>
    <li class="position-relative nav-pills {{ request()->is('settings/account')? 'active' : '' }}">
        <a href="{{ route('settings.account') }}" class="stretched-link">Account</a>
    </li>
</ul>