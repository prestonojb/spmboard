<form method="POST" action="{{ route('exam_builder.filter') }}" id="examBuilderFilterForm">
    @csrf
    <div class="form-field">
        <label>Number of Questions</label>
        <select name="no_of_questions">
            <option value="" disabled selected>Select number of questions</option>
            @foreach(range(5, auth()->user()->getExamBuilderQuestionLimit(), 5) as $number)
                <option value="{{ $number }}" {{ old('no_of_questions') == $number ? 'selected="selected"' : '' }}>{{ $number }}</option>
            @endforeach
        </select>
        @error('no_of_questions')
            <span class="error-message">{{ $message }}</span>
        @enderror
    </div>

    {{-- Subject --}}
    <div class="form-field">
        <label>Subject</label>
        <select class="exam-builder__subject-select" name="subject">
            <option data-placeholder="true"></option>
            @foreach($subjects as $subject)
                <option value="{{ $subject->id }}" {{ old('subject') == $subject->id ? 'selected="selected"' : '' }}>{{ $subject->name }}</option>
            @endforeach
        </select>
        @error('subject')
            <span class="error-message">{{ $message }}</span>
        @enderror
    </div>

    {{-- Chapters --}}
    <div class="form-field">
        <label>Chapters <span class="font-weight-normal"></span></label>
        <select class="exam-builder__chapter-select" name="chapters[]" multiple disabled>
            <option data-placeholder="true"></option>
            @foreach($subjects as $subject)
                <optgroup label="{{ $subject->name }}">
                    @foreach($subject->getChaptersWithItems() as $chapter)
                        <option value="{{ $chapter->id }}" class="parent"
                            @if(is_array(old('chapters')) && in_array($chapter->id, old('chapters'))) selected @endif>
                            {{ $chapter->name }}
                        </option>

                        @can('generate_with_subchapter', \App\ExamBuilderSheet::class)
                            @if($chapter->hasSubchapters())
                                @foreach($chapter->subchapters as $subchapter)
                                    <option value="{{ $subchapter->id }}" class="child" data-parent-chapter-id="{{ $subchapter->parent_chapter->id }}"
                                        @if(is_array(old('chapters')) && in_array($subchapter->id, old('chapters'))) selected @endif>
                                        >>> {{ $subchapter->name }}
                                    </option>
                                @endforeach
                            @endif
                        @endcan
                    @endforeach
                </optgroup>
            @endforeach
        </select>
        <p class="gray2 font-size1">Max {{ auth()->user()->getExamBuilderChapterLimit() }} chapters</p>
        @error('chapters')
            <span class="error-message">{{ $message }}</span>
        @enderror
    </div>

    {{-- Paper Number --}}
    <div class="form-field">
        <label for="">Paper Number</label>
        <select name="paper_number" id="" disabled>
            <option value="" disabled selected>Select Paper Number</option>
            @foreach(range(1, 6) as $paper_number)
                <option value="{{ $paper_number }}" @if(old('paper_number') && old('paper_number') == $paper_number) selected @endif>{{ $paper_number }}</option>
            @endforeach
        </select>
        @error('paper_number')
            <span class="error-message">{{ $message }}</span>
        @enderror
    </div>

    <div class="text-center">
        <button type="submit" class="button--primary disableOnSubmitButton">Generate</button>
    </div>
</form>
