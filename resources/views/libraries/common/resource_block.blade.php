<div class="file-block">
    {{-- Link --}}
    @if(isset($resource))
        <a target="_blank" href="{{ $resource->show_url }}" class="stretched-link"></a>
    @elseif(isset($custom_resource))
        <a target="_blank" href="{{ $custom_resource->file_url }}" class="stretched-link"></a>
    @endif

    {{-- Preview --}}
    <div class="file-preview">
        @if(isset($resource))
            <span class="iconify" data-icon="{{ $resource->data_icon }}" data-inline="false"></span>
        @elseif(isset($custom_resource))
            <span class="iconify" data-icon="{{ $custom_resource->data_icon }}" data-inline="false"></span>
        @endif
    </div>

    {{-- Content --}}
    <div class="file-content">
        @if( isset($resource) )
            <div class="file-title">{{ $resource->primary }}</div>
            <div class="file-type">
                {{ str_limit($resource->secondary, 30) }}
            </div>
        @elseif(isset($custom_resource))
            <div class="file-title">{{ $custom_resource->name }}</div>
            <div class="file-type">{{ $custom_resource->type }}</div>
        @endif
    </div>

    @if(!(isset($type) && $type == 'unlocked_resources'))
        {{-- Delete button --}}
        @if(isset($resource) && !is_null($library))
            <button type="button" class="file-cross-button deleteResourceFromLibraryButton" data-action="{{ route('libraries.toggle_resource', [$library->id, $resource->id]) }}" data-library="{{ $library }}">
                <span class="iconify" data-icon="gridicons:cross-small" data-inline="false"></span>
            </button>
        @elseif(isset($custom_resource))
            <button type="button" class="file-cross-button deleteButton" data-type="App\CustomResource" data-id="{{ $custom_resource->id }}">
                <span class="iconify" data-icon="gridicons:cross-small" data-inline="false"></span>
            </button>
        @endif
    @endif
</div>
