<?php

namespace App\Http\Middleware;

use App\CoinAction;
use App\RewardPointAction;
use Carbon\Carbon;
use Closure;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Amplitude;

class UpdateLastOnlineAt
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (auth()->guest()) {
            return $next($request);
        }

        $user = auth()->user();

        // Reward in RP/Coins
        $reward = [10, 20, 30, 40, 50, 50, 50];
        $coin_reward_for_day_7 = 5;

        # For newly registered users
        if ( is_null($user->last_online_at) ) {
            $user->update([
                'last_online_at' => Carbon::now(),
                'consecutive_login_days' => 1,
            ]);

            if( $user->hasRewardPoints() ) {
                // Gift day 1 reward to user
                $user->addRewardPointAction('Login Reward', $reward[0]);
                Session::flash('success', 'You are rewarded '. $reward[0] .' reward points for logging in on Day 1!');
            }
            return $next($request);
        }

        // User logs in for the first time today
        $first_daily_login = !$user->last_online_at->isToday();
        $streak_is_active = $user->last_online_at->isYesterday();
        // New login activity (cooldown: 30 minutes)
        $is_new_login = $user->last_online_at->diffInMinutes(Carbon::now()) >= 30;

        // Update last online at
        if( $is_new_login ) {
            // Update last online at
            $user->update([
                'last_online_at' => Carbon::now()
            ]);

            // Log event on Amplitude
            Amplitude::setUserId($user->id);
            Amplitude::queueEvent('login');
        }

        if( $first_daily_login ) {
            # User continues streak
            if( $streak_is_active ) {
                $user->consecutive_login_days += 1;
            } else {
                $user->consecutive_login_days = 1;
            }

            // Reward users RP/coins on logging in
            if( $user->hasRewardPoints() || $user->hasCoins() ) {
                if( $user->reward_day != 7 ) {
                    // When RP reward for the day is 0-6
                    if( $reward[$user->reward_day - 1] > 0 ) {

                        $amount = $reward[$user->reward_day - 1];
                        $user->addRewardPointAction('Login Reward', $amount);

                        Session::flash('success', 'You are rewarded '. $reward[$user->reward_day - 1] .' reward points for logging in on Day '. $user->reward_day .'!');
                    }
                } else {
                    // When RP reward for the day is 7
                    if( $user->hasCoins() ) {
                        // Add coins to user if has coins
                        $user->addCoinAction('Login Reward', $coin_reward_for_day_7);

                        Session::flash('success', 'You are rewarded '. $coin_reward_for_day_7 .' coins for logging in on Day '. $user->reward_day .'!');
                    } else {
                        // Add RP to user if does not have coins
                        $amount = $reward[$user->reward_day - 1];
                        $user->addRewardPointAction('Login Reward', $amount);

                        Session::flash('success', 'You are rewarded '. $reward[$user->reward_day - 1] .' coins for logging in on Day '. $user->reward_day .'!');
                    }
                }
            }
        }

        return $next($request);
    }
}
