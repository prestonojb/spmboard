@php
if($is_edit) {
    $answer_html = old('answer') ?? $answer->answer;
    $url = route('answers.update', $answer->id);
} else {
    $answer_html = old('answer');
    $url = route('answers.store', $question->id);
}
@endphp

<form method="POST" action="{{ $url }}" class="" enctype="multipart/form-data" id="answerForm">
    @csrf
    @if($is_edit)
        @method('PATCH')
    @endif
    <div class="form-group">
        <label for="">Answer</label>
        {{-- Answering guideline --}}
        <p class="gray2 font-size1">Read about our
            <button type="button" class="p-0 blue5 font-size-inherit underline-on-hover" data-target="#answeringGuidelineModal" data-toggle="modal">answering guidelines</button> before contributing an answer.</p>
        <div class="loading-icon loading in-editor blue"></div>
        <textarea class="answer-tinymce-editor" name="answer">{!! $answer_html !!}</textarea>
        @error('answer')
            <div class="error-message">{{ $message }}</div>
        @enderror
    </div>

    <div class="text-center">
        <button type="submit" class="button--primary w-250px loadOnSubmitButton">{{ $is_edit ? 'Update' : 'Answer' }}</button>
    </div>
</form>

@include('forum.answers.common.answering_guidelines_modal')
