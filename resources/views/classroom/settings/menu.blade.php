@extends('classroom.layouts.app')

@section('meta')
    <title>Settings | {{ config('app.name') }} Classroom</title>
@endsection

@section('content')

@component('classroom.common.breadcrumbs')
    <a href="{{ route('classroom.landing') }}">Home</a> /
    Settings
@endcomponent

@component('classroom.common.page_heading', ['title' => 'Settings'])
@endcomponent

<div class="maxw-740px mx-auto my-5">
    @if( auth()->user()->teacher )
        @component('classroom.settings.menu_items.teacher')
        @endcomponent
    @elseif( auth()->user()->student )
        @component('classroom.settings.menu_items.student')
        @endcomponent
    @elseif( auth()->user()->parent )
        @component('classroom.settings.menu_items.parent')
        @endcomponent
    @endif
</div>

@endsection

@section('scripts')
<script>
$(function(){
    $('#saveParentAccountButton').click(function(e){
        e.preventDefault();
        $(this).attr('disabled', true);
        $(this).closest('form').submit();
    });
});
</script>
@endsection
