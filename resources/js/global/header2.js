$('.header2__hamburger-button').click(function(e){
    var $overlay = $('.overlay');
    $overlay.show();
    e.stopPropagation();
});

$('.header2__mobile-search-icon').click(function(){
    $('.header2__mobile-search-form').toggleClass('show');
});

$('.overlay').click(function(){
    $('.overlay').hide();
});

$('.header2__sidenav-dropdown-arrow').click(function(e){
    e.stopPropagation();

    //Get child chapter menu
    var $chapterMenu = $(this).parent().next('.header2__sidenav-dropdown-menu');

    if($chapterMenu.css('display') == 'block'){
        //Hide if menu is on but is clicked again
        $chapterMenu.hide();
    } else{
        //Reset ALL subject menu//dropdown arrow
        $chapterMenu.show();
    }
    //Toggle up and down arrow of clicked list item
    $(this).find('i').toggleClass('fa-chevron-right fa-chevron-down');
});

//Show sidenav dropdown menu
$('.header2__sidenav-item.active').next('.header2__sidenav-dropdown-menu').show();
//Change arrow sign
$('.header2__sidenav-item.active').find('.header2__sidenav-dropdown-arrow i').toggleClass('fa-chevron-right fa-chevron-down');

//Add active class to parent of active link
$('.header2__sidenav-dropdown-menu').each(function(){
    var hasActiveLink = $(this).has('.header2__sidenav-dropdown-menu-item.active').length > 0;
    if(hasActiveLink){
        $(this).prev('.header2__sidenav-item').addClass('active');
        $(this).show();
    }
});

//Point mobile dropdown arrow to right direction
$('.header2__sidenav-item').each(function(){
    var siblingHasActiveLink = $(this).next('.header2__sidenav-dropdown-menu').has('.mobile-nav-dropdown-menu-item.active').length > 0;
    if(siblingHasActiveLink){
        $(this).find('.header2__sidenav-dropdown-arrow i').toggleClass('fa-chevron-right fa-chevron-down');
    }
});

//Point dropdown arrow to right direction
$('.header2__sidenav-item.active').next('.header2__sidenav-dropdown-menu').show();
$('.header2__sidenav-item.active').find('.header2__sidenav-dropdown-arrow i').toggleClass('fa-chevron-right fa-chevron-down');

//Show active subject when chapter is active
$('.header2__sidenav-dropdown-menu').each(function(){
    var hasActiveLink = $(this).has('.header2__sidenav-dropdown-menu-item.active').length > 0;
    if(hasActiveLink){
        $(this).prev('.header2__sidenav-item').addClass('active');
        $(this).show();
    }
});

//Dropdown menu arrow point to the right direction
$('.header2__sidenav-item').each(function(){
    var siblingHasActiveLink = $(this).next('.header2__sidenav-dropdown-menu').has('.header2__sidenav-dropdown-menu-item.active').length > 0;
    if(siblingHasActiveLink){
        $(this).find('.header2__sidenav-dropdown-arrow i').toggleClass('fa-chevron-right fa-chevron-down');
    }
});


$('.notification-badge').click(function(){
    $('.bell-icon').click();
});
