<?php

namespace Tests\Feature\Classroom;

use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Tests\TestCase;
use App\Classroom;
use App\Lesson;
use App\Subject;
use App\User;
use App\Notifications\Classroom\HomeworkSubmitted;
use App\Notifications\Classroom\HomeworkReturned;
use App\Homework;
use App\HomeworkSubmission;
use Illuminate\Support\Facades\Notification;
use Carbon\Carbon;

class HomeworkSubmissionTest extends TestCase
{
    use WithFaker;

    private $teacher;
    private $student;
    private $parent;
    private $subject;

    public function setUp() : void
    {
        parent::setUp();

        // Subscribed Teacher
        $user = factory(User::class)->states('subscribed_teacher')->create();
        $this->subscribed_teacher = $user->teacher;

        // Unsubscribed Teacher
        $user = factory(User::class)->states('teacher')->create();
        $this->teacher = $user->teacher;

        // Student with Board Pass
        $user = factory(User::class)->states('student')->create();
        $this->student = $user->student;

        // Parent
        $user = factory(User::class)->states('parent')->create();
        $this->parent = $user->parent;

        // Subject
        $this->subject = Subject::first();
        // Classroom
        $this->classroom = factory(Classroom::class)->create([
                                'teacher_id' => $this->teacher->user_id,
                                'subject_id' => $this->subject->id,
                            ]);
        // Lesson
        $this->lesson = factory(Lesson::class)->create([
            'classroom_id' => $this->classroom->id
        ]);

        // Homework
        $this->homework = factory(Homework::class)->states('with_submission')->create([
            'lesson_id' => $this->lesson->id
        ]);

        // Homework Submission
        $this->submission = $this->homework->submissions[0];

        // Student joins classroom
        $this->classroom->addStudent( $this->student );
        // Parent attached to student
        $this->student->updateParent($this->parent);

        // Submission belongs to student
        $this->submission->update(['student_id' => $this->student->user_id]);
    }


    /**
     * @group homework_submission
     * Test show homework submission
     */
    public function testShow()
    {
        // GET request to show homework submission
        $response = $this->actingAs($this->teacher->user)
                         ->get( route('classroom.lessons.homeworks.submissions.show', [$this->lesson->id, $this->submission->id]) );

        $response->assertSuccessful();
    }

    /**
     * @group homework_submission
     * Test submit homework
     */
    public function testStore()
    {
        Storage::fake('s3');
        Notification::fake();

        $init_count = HomeworkSubmission::count();

        $file = UploadedFile::fake()->create('document.pdf', 4096, 'application/pdf');

        // POST request to upload homework
        $response = $this->actingAs($this->student->user)
                         ->post( route('classroom.lessons.homeworks.submissions.store', [$this->lesson->id, $this->homework->id]), [
                            'file' => $file,
                            'comment' => $this->faker->optional->sentence,
                         ]);

        // Assert submission is stored
        $this->assertEquals( $init_count + 1, HomeworkSubmission::count() );

        // Assert the file was stored
        Storage::disk('s3')->assertExists( 'homework/submissions/'. $file->hashName() );

        // Assert homework submitted notification sent
        Notification::assertSentTo(
            [$this->classroom->teacher], HomeworkSubmitted::class
        );

        // Assert redirect
        $response->assertRedirect();
    }

    /**
     * @group homework_submission
     * Test submit non-PDF file for homework submission
     */
    public function testStoreNonPdf()
    {
        Storage::fake('s3');

        $images = ['image.png', 'image.jpg', 'image.jpeg'];
        foreach($images as $image) {
            $file = UploadedFile::fake()->image( $image );
            // POST request to upload homework
            $response = $this->actingAs($this->student->user)
                            ->post( route('classroom.lessons.homeworks.submissions.store', [$this->lesson->id, $this->homework->id]), [
                                'file' => $file,
                                'comment' => $this->faker->optional->sentence,
                            ]);

            // Assert file has error
            $response->assertSessionHasErrors('file');
        }
    }

    /**
     * @group homework_submission
     * Test mark and return homework submission
     */
    public function testMarkAndReturn()
    {
        Storage::fake('s3');
        Notification::fake();

        $file = UploadedFile::fake()->create('document.pdf', 4096, 'application/pdf');
        // POST request to mark homework
        $response = $this->actingAs($this->teacher->user)
                         ->post( route('classroom.lessons.homeworks.submissions.mark', [$this->lesson->id, $this->submission->id]), [
                            'marked_file' => $file,
                            'marks' => rand(0, $this->submission->homework->max_marks),
                            'teacher_remark' => $this->faker->optional->sentence,
                         ]);

        // Assert the file was stored
        Storage::disk('s3')->assertExists( 'homework/submissions/marked/'. $file->hashName() );

        // Assert homework returned notification sent to student ONLY
        // Student
        Notification::assertSentTo(
            [$this->submission->student], HomeworkReturned::class,
            function ($notification, $channels) {
                return $notification->via($this->submission->student) == ['mail', 'database'];
            }
        );

        // Parent
        Notification::assertNotSentTo(
            [$this->submission->student->parent], HomeworkReturned::class,
            function ($notification, $channels) {
                return $notification->via($this->submission->student) == ['mail', 'database'];
            }
        );

        // Assert redirect
        $response->assertRedirect();
    }

    /**
     * @group homework_submission
     * Test mark and return homework submission notify parent notification
     */
    public function testMarkAndReturnNotifyParents()
    {
        Notification::fake();
        $file = UploadedFile::fake()->create('document.pdf', 4096, 'application/pdf');

        // POST request to mark homework
        $response = $this->actingAs($this->teacher->user)
                         ->post( route('classroom.lessons.homeworks.submissions.mark', [$this->lesson->id, $this->submission->id]), [
                            'marked_file' => $file,
                            'marks' => rand(0, $this->submission->homework->max_marks),
                            'teacher_remark' => $this->faker->optional->sentence,
                            'notify_parents' => 1,
                         ]);

        // Assert homework returned notification sent to student and parent
        // Student
        Notification::assertSentTo(
            [$this->submission->student], HomeworkReturned::class,
            function ($notification, $channels) {
                return $notification->via($this->submission->student) == ['mail', 'database'];
            }
        );

        // Parent
        Notification::assertSentTo(
            [$this->submission->student->parent], HomeworkReturned::class,
            function ($notification, $channels) {
                return $notification->via($this->submission->student) == ['mail', 'database'];
            }
        );
    }


    /**
     * @group homework_submission
     * Mark submission for graded homework (Max marks: 50)
     */
    public function testMarkGradedHomework()
    {
        $marks = [null, -1, 0, 1, 49, 50, 51];

        foreach($marks as $mark) {
            // Homework with max marks: 50
            $homework = factory(Homework::class)->create([
                            'lesson_id' => $this->lesson->id,
                            'max_marks' => 50
                        ]);
            $submission = factory(HomeworkSubmission::class)->create([
                            'homework_id' => $homework->id,
                            'student_id' => $this->student->user_id,
                        ]);
            // POST request to upload homework
            $response = $this->actingAs($this->teacher->user)
                            ->post( route('classroom.lessons.homeworks.submissions.mark', [$this->lesson->id, $submission->id]), [
                                'marked_file' => UploadedFile::fake()->create('document.pdf', 4096, 'application/pdf'),
                                'marks' => $mark,
                                'teacher_remark' => $this->faker->optional->sentence,
                            ]);

            if( $mark >= 0 && $mark <= 50 ) {
                // Assert Redirect if marks is valid
                $response->assertSessionHas('success');
                $response->assertRedirect();
            } else {
                // Assert 'marks' have errors if invalid marks given
                $response->assertSessionHasErrors('marks');
            }
        }
    }


    /**
     * @group homework_submission
     * Mark submission for ungraded homework (Max marks: 50)
     */
    public function testMarkUngradedHomework()
    {
        $marks = [null, -1, 0, 1, 49, 50, 51];

        foreach($marks as $mark) {
            // Homework with max marks: 50
            $ungraded_homework = factory(Homework::class)->create([
                            'lesson_id' => $this->lesson->id,
                            'max_marks' => null
                        ]);
            $submission = factory(HomeworkSubmission::class)->create([
                'homework_id' => $ungraded_homework->id,
                'student_id' => $this->student->user_id,
            ]);
            $file = UploadedFile::fake()->create('document.pdf', 4096, 'application/pdf');
            // POST request to upload homework
            $response = $this->actingAs($this->teacher->user)
                            ->post( route('classroom.lessons.homeworks.submissions.mark', [$this->lesson->id, $submission->id]), [
                                'marked_file' => $file,
                                'marks' => $mark,
                                'teacher_remark' => $this->faker->optional->sentence,
                            ]);

            $submission = HomeworkSubmission::find($submission->id);
            $this->assertTrue( is_null($submission->marks) );
        }
    }
}
