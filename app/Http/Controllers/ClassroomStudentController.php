<?php

namespace App\Http\Controllers;

use App\Classroom;
use App\Question;
use App\User;
use App\Student;
use App\StudentParent;
use Illuminate\Http\Request;

class ClassroomStudentController extends Controller
{
    public function index()
    {
        $this->authorize('classroom_index', Student::class);
        $teacher = auth()->user()->teacher;

        // Classroom filter
        if( request()->class_selected ) {
            $class_selected = Classroom::where('id', request()->class_selected)->first();
        }
        if( isset($class_selected) && !is_null($class_selected) ) {
            // One class
            $students = $class_selected->students;
        } else {
            $class_selected = null;
            // All classes
            $students = $teacher->students;
        }

        // Understanding rating filter
        if( isset(request()->understanding_rating) ) {
            $und_rating = request()->understanding_rating;
            $students = $students->whereBetween('understanding_rating', [$und_rating, $und_rating + 1]);
        }

        // Unpaid Lesson filter
        if( isset(request()->unpaid_lessons) ) {
            $unpaid_lesson_count = request()->unpaid_lessons;
            $students = $students->whereBetween('unpaid_lesson_count', [$unpaid_lesson_count, $unpaid_lesson_count + 1]);
        }

        $students = $students->all();

        return view('classroom.students.index', compact('students', 'class_selected'));
    }

    public function show(Student $student)
    {
        $this->authorize('show', $student);
        $user = auth()->user();

        $userId = $student->user_id;

        $classrooms = $student->classrooms;

        // Invoices
        $invoices = $student->invoices()->orderByDesc('created_at')->take(5)->get();
        $reports = $student->reports()->orderByDesc('created_at')->take(5)->get();

        if( request('class_selected') ) {
            $class_selected = Classroom::find(request('class_selected'));
        } else {
            $class_selected = Classroom::whereHas('students', function ($q) use ($student) {
                $q->where('user_id', $student->user_id);
            })->first();
        }

        $homework_marks_data = $student->getHomeworkMarksData($class_selected);

        return view('classroom.students.show', compact('class_selected', 'classrooms', 'student', 'invoices', 'reports', 'homework_marks_data'));
    }

    public function update()
    {
        request()->validate([
            'name' => 'required',
            'profile_img' => 'nullable|mimes:jpeg,png,jpg,gif,svg',
            'about' => 'nullable|max:50',
            'state' => 'nullable',
            'school' => 'nullable|max:50',
            'group' => 'nullable',
        ]);

        $user = auth()->user();

        $user->update([
            'name' => request()->name,
            'about' => request()->about,
            'state_id' => request()->state,
        ]);

        $user->student->update([
            'school' => request()->school,
            'exam_board_id' => request()->exam_board,
        ]);

        return redirect()->back()->withSuccess('Profile updated successfully!');
    }

    public function editParentSettings()
    {
        $student = auth()->user()->student;
        $parent = $student->parent;

        return view('classroom.settings.student.parent', compact('parent'));
    }

    public function updateParentSettings()
    {
        request()->validate([
            'parent_email' => 'required|email',
        ]);

        $user = auth()->user();
        $student = $user->student;

        $parent = StudentParent::whereHas('user', function($q) {
            $q->where('email', request()->parent_email);
        })->first();


        if( $parent ){
            // Update parent_id if exists
            $student->update([
                'parent_id' => $parent->user_id
            ]);
        } else {
            return redirect()->back()->with( 'error', 'Parent account not found' );
        }

        return redirect()->back()->withSuccess('Parent account updated successfully!');
    }

}
