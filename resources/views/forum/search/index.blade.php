@extends('forum.layouts.app')

@section('meta')
<title>Search: {{ $query }} | {{ $exam_board->name }} {{ config('app.name') }}</title>

<meta property="og:image" content="{{ asset('img/essential/og-logo.png') }}" />
<meta property="og:type" content="website" />
@endsection

@section('styles')
@endsection

@section('content')
@include('forum.common.header')
@include('forum.common.sidenav')
<section class="content">
    <div class="qb-container">

        <div class="search__container">
            <p>Results for <span class="query">{{ $query }}</span></p>
            <form method="GET" action="{{ route('search.index', $exam_board) }}" class="search__form">
                <span class="icon"><i class="fa fa-search" aria-hidden="true"></i></span>
                <input name="q" class="search search-input" value="{{ $query }}" placeholder="e.g. What is the powerhouse of the cell?" required>
                <input type="hidden" name="sort">
            </form>
        </div>

        <button type="button" class="" data-toggle="modal" data-target="#sortSelect">Sort by: {{ isset($sort) ? ucfirst($sort) : 'Hot' }}</button>

        <div class="modal" id="sortSelect" tabindex="-1" role="dialog" aria-labelledby="sortSelect" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="list-group">
                        <a href="{{ url()->current() }}" data-sort="" class="list-group-item list-group-item-action @if(!isset($sort)) active @endif">Hot</a>
                        <a href="{{ url()->current() }}" data-sort="top" class="list-group-item list-group-item-action @if($sort=='top') active @endif">Top</a>
                        <a href="{{ url()->current() }}" data-sort="new" class="list-group-item list-group-item-action @if($sort=='new') active @endif">New</a>
                        <a href="{{ url()->current() }}" data-sort="answered" class="list-group-item list-group-item-action @if($sort=='answered') active @endif">Answered</a>
                    </div>
                </div>
            </div>
        </div>

        @if($questions->count() != 0)
        @foreach($questions as $question)
            @include('forum.common.question-block')
        @endforeach
        @else
        <section class="text-center my-4">
            <img src="{{ asset('img/essential/empty.svg') }}" alt="No Questions Found" width="220" height="220" class="mb-2">
            <h2 class="h3">No questions found. Try to ask a less specific question.</h2>
        </section>
        @endif

        {{ $questions->appends(['sort' => $sort])->links() }}
    </div>
</section>
@endsection


@section('scripts')
<script>
$(document).ready(function(){
    function getParameterByName(name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, '\\$&');
        var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, ' '));
    }


    $('.search__form').submit(function(e){
        $('input[name="sort"]').val('{{ $sort }}');
    });

    $('#sortSelect a').click(function(e){
        e.preventDefault();
        var baseUrl = '{{ url()->current() }}';
        var currentUrl = '{{ url()->full() }}';

        var q = getParameterByName('q', currentUrl);
        var sort = $(this).data('sort');

        var params = {
            'q': q,
            'sort': sort
        };

        location.href = baseUrl + '?' + jQuery.param(params);
    });
});
</script>
@endsection
