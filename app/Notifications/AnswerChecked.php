<?php

namespace App\Notifications;

use App\Answer;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\HtmlString;

class AnswerChecked extends Notification implements ShouldQueue
{
    use Queueable;

    public $answer;
    public $reward_points;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Answer $answer, $reward_points = 0)
    {
        $this->answer = $answer;
        $this->reward_points = $reward_points;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database'];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $answer = $this->answer;
        $question = $this->answer->question;

        $title = "<b>{$question->user->username}</b> accepted your answer for the question \"{$question->question}\"!";

        if ( $answer->awarded_points ) {
            $title = $title.' You are awarded <b>'.$question->reward_points. ' reward points</b>!';
        }

        return [
            'type' => Answer::class,
            'target' => $this->answer,
            'title' => $title,
        ];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $answer = $this->answer;
        $question = $this->answer->question;

        $line = "";

        if ( $answer->awarded_points ) {
            $line = $line.' You are awarded <b>'.$question->reward_points. ' reward points</b> for your awesome answer 🤩! <br><br>';
        }

        $line .= "The Board community needs more amazing answerers like you! <b>Click on the button below</b> to help the Board community answer more questions! <br><br>";
        $line .= "Earn more reward points whenever your answer gets accepted/recommended and use your points to claim <b>amazing study materials</b>!";

        return (new MailMessage)
                    ->subject("🎉 Hooray! Your answer is accepted!")
                    ->greeting("{$question->user->username} accepted your answer for the question \"{$question->question}\"!")
                    ->line(new HtmlString($line))
                    ->action('Answer More Questions', route('questions.index', $question->subject->exam_board->slug))
                    ->line("See you on Board! 🚀");
    }
}
