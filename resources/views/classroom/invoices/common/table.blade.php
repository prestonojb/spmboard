<table class="table panel p-0 invoiceTable" data-show-minimal="{{ isset($show_minimal) && $show_minimal }}">
    <thead>
    <tr>
        <th scope="col">Invoice Number</th>
        <th scope="col">Amount(MYR)</th>
        <th scope="col">Status</th>
        <th scope="col">Date Issued</th>
        <th scope="col">Due At</th>
        <th scope="col">Paid At</th>
        <th scope="col">Action</th>
    </tr>
    </thead>
    <tbody>
        @foreach( $invoices as $invoice )
            <tr>
                <th scope="row">{{ $invoice->invoice_number }}</th>
                <td>{{ $invoice->amount }}</td>
                <td>{!! $invoice->status_tag_html !!}</td>
                <td>{{ $invoice->created_at->toFormattedDateString() }}</td>
                <td>{{ $invoice->due_at->toFormattedDateString() }}</td>
                <td>{{ $invoice->paid_at ? $invoice->paid_at->toFormattedDateString() : 'N/A' }}</td>
                <td>
                    <div class="d-flex">
                        <a href="{{ route('classroom.invoices.show', $invoice->id) }}" class="button--primary--inverse sm mr-1"><span class="iconify" data-icon="mdi:magnify-plus-outline" data-inline="false"></span></a>
                        @can('mark_as_paid', $invoice)
                            <form action="{{ route('classroom.invoices.mark_as_paid', $invoice->id) }}" method="post">
                                @csrf
                                <button type="submit" class="sm mr-1 markAsPaidButton @if($invoice->status == 'paid') button--success disabled @else button--success--inverse @endif" @if($invoice->status == 'paid') disabled @endif><span class="iconify" data-icon="bi:check" data-inline="false"></span></button>
                            </form>
                        @endcan
                        @can('delete', $invoice)
                            <form action="{{ route('classroom.invoices.delete', $invoice->id) }}" method="post">
                                @csrf
                                @method('DELETE')
                                <button type="submit" @if($invoice->status == 'paid') disabled @endif class="button--danger--inverse sm deleteButton"><span class="iconify" data-icon="carbon:delete" data-inline="false"></span></button>
                            </form>
                        @endcan
                    </div>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>
