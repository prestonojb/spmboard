@extends('classroom.layouts.app')

@section('meta')
    <title>Post Resource | {{ config('app.name') }} Classroom</title>
@endsection

@section('content')
@component('classroom.common.breadcrumbs')
    <a href="{{ route('classroom.classrooms.show', $classroom->id) }}">Dashboard</a> /
    <a href="{{ route('classroom.lessons.index', $classroom->id) }}">Lessons</a> /
    <a href="{{ route('classroom.lessons.show', [$lesson->classroom->id, $lesson->id]) }}">{{ $lesson->name }}</a> /
    Post Resource
@endcomponent

@component('classroom.common.page_heading', ['title' => 'Post Resource'])
@endcomponent

<div class="maxw-740px mx-auto my-5">
    <div class="panel mb-2">
        <form action="{{ route('classroom.lessons.resource_posts.store', $lesson->id) }}" method="POST" id="postResourcesForm">
            @csrf
            <input type="hidden" name="resource_data">
            <input type="hidden" name="custom_resource_data">

            <div class="form-field">
                <label for="">Lesson</label>
                <input type="text" value="{{ $lesson->name }}" disabled>
            </div>

            <div class="form-field">
                <label>Title</label>
                <input type="text" name="title" placeholder="Title" required>
                @error('title')
                    <span class="error-message">{{ $message }}</span>
                @enderror
            </div>

            <div class="form-field">
                <label>Description(Optional)</label>
                <textarea name="description" id="" rows="3" placeholder="Description">{{ old('description') }}</textarea>
                @error('description')
                    <span class="error-message">{{ $message }}</span>
                @enderror
            </div>

            <div class="form-field">
                <div class="form-field">
                    <label>Chapter(Optional)</label>
                    <select class="chapter-select" name="chapter">
                        <option selected disabled>Select Chapter</option>
                        @foreach($subject->chapters as $chapter)
                            <option value="{{ $chapter->id }}" @if($chapter->id == old('chapter')) selected @endif>
                                {{ $chapter->name }}
                            </option>
                        @endforeach
                        </optgroup>

                    </select>
                    @error('chapter')
                        <span class="error-message">{{ $message }}</span>
                    @enderror
                </div>
            </div>

            <div class="file-block-container">
                <div class="row">
                </div>
            </div>

            <div class="d-flex justify-content-between align-items-center">
                <div class="dropdown">
                    <button type="button" class="button--primary--inverse" data-toggle="dropdown">Add</button>
                    <div class="dropdown-menu">
                        <button type="button" class="dropdown-item" data-toggle="modal" data-target=".prebuilt-pp-modal">Past Papers</button>
                        <button type="button" class="dropdown-item" data-toggle="modal" data-target=".prebuilt-notes-modal">Notes</button>
                        <button type="button" class="dropdown-item" data-toggle="modal" data-target=".prebuilt-tpp-modal">Topical Past Papers</button>
                        <button type="button" class="dropdown-item" id="customFileUploadButton">Custom File</button>
                    </div>
                </div>
                <button type="submit" class="button--primary disableOnSubmitButton">Post</button>
            </div>
        </form>

        <form id="customFileForm" enctype="multipart/form-data" method="POST">
            @csrf
            <input type="file" name="custom_file" hidden>
        </form>
    </div>
</div>

<div class="d-none">
    <div class="col-12 col-md-6 fileBlockCol">
        @component('classroom.common.file_block', ['deletable' => true])
        @endcomponent
    </div>
</div>

@include('classroom.common.prebuilt_resources_modal')
@endsection

@section('scripts')
<script>
var customFileUploadUrl = "{{ route('classroom.custom_file_upload') }}";
var storageUrl = "{{ Storage::url('/') }}";
storageUrl = storageUrl.slice(0, -1);
</script>
@endsection

