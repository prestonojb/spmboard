<?php

namespace App\Http\Controllers;

use App\ExamBoard;
use App\Subject;
use App\Notes;
use App\Resource;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Amplitude;

class NotesController extends Controller
{
    /**
     * Get Home View
     */
    public function getHomeView(ExamBoard $exam_board)
    {
        $subjects = $exam_board->subjects()->whereHas('notes')->get();
        return view('resources.notes.home', compact('exam_board', 'subjects'));
    }

    /**
     * Get note subject show page
     */
    public function subjectShow(ExamBoard $exam_board, Subject $subject)
    {
        $type = request('type') ?? null;

        // Get chapters with notes (of $type)
        $chapters = $subject->chapters()->whereHas('notes', function($q) use ($type) {
            $q->where('type', $type);
        })->get();

        // Get notes
        $notes = Notes::where('subject_id', $subject->id);
        // Chapter filter
        if(request('chapter') == -1) {
            $notes = $notes->whereNull('chapter_id');
        } elseif(!is_null(request('chapter'))) {
            $notes = $notes->where('chapter_id', request('chapter'));
        }

        // Type filter
        if(!is_null($type)) {
            $notes = $notes->where('type', $type);
        }

        $notes = $notes->paginate(30);

        return view('resources.notes.subject_show', compact('exam_board', 'subject', 'chapters', 'notes', 'type'));
    }

    /**
     * Get note show page
     */
    public function show(ExamBoard $exam_board, Notes $note)
    {
        // Record views
        if(!is_null($note->resource)) {
            views($note->resource)->record();
        }

        return view('resources.notes.show', compact('exam_board', 'note'));
    }

    /**
     * Unlock notes
     */
    public function unlock(ExamBoard $exam_board, Notes $note)
    {
        $user = auth()->user();
        $buyer = $user;
        // User does not have coins exception
        if(!$user->hasCoins()) {
            return redirect()->back()->withError('User cannot unlock resource without coins!');
        }

        // Note is free
        if($note->isFree()) {
            return redirect()->back()->withError('Resource is already free.');
        }

        // User alraedy owns resource
        if($user->hasResource($note->resource)) {
            return redirect()->back()->withError('Resource is already unlocked!');
        }

        // User does not have sufficient coin balance
        $purchase_price = $note->getAdjustedCoinPrice($buyer);
        if($user->coin_balance < $purchase_price) {
            return redirect()->back()->withError('You have insufficient coin balance!');
        }

        // Unlock resource for buyer
        try {
            $note->unlockFor($buyer);
        } catch(\Exception $e) {
            return redirect()->back()->withError('An error occurred! Please try again later.');
        }

        Session::flash('success', $note->name.' unlocked successfully!');
        return redirect()->route('notes.show', [$exam_board->slug, $note->id]);
    }

    /**
     * Create notes
     */
    public function create(ExamBoard $exam_board, Notes $note)
    {
        $is_edit = false;
        return view('resources.notes.create_edit', compact('exam_board', 'note', 'is_edit'));
    }

    /**
     * Store notes
     */
    public function store(ExamBoard $exam_board)
    {
        // Validation
        request()->validate([
            'subject' => 'required|integer',
            'chapter' => 'nullable|integer',
            'name' => 'required|string|max:191',
            'description' => 'nullable|string|max:500',
            'type' => 'required|string|in:comprehensive,concise',
            'file' => 'required|file|mimes:pdf|max:50000',
            'coin_price' => 'nullable|integer|between:0,100',
            'confirm_tnc' => 'required',
        ]);

        $user = auth()->user();

        // Create note
        $note = $user->owned_notes()->create([
            'subject_id' => request('subject'),
            'chapter_id' => request('chapter'),
            'name' => request('name'),
            'description' => request('description'),
            'type' => request('type'),
            'coin_price' => request('coin_price') ?? 0,
        ]);

        // Add resource relationship to newly created note
        Resource::create([
            'resourceable_type' => Notes::class,
            'resourceable_id' => $note->id
        ]);

        // Save PDF file
        $note->saveFile(request('file'));

        // Save Preview Image
        $note->savePreviewImage(request('file'));

        Session::flash($note->name. " uploaded successfully!");
        return redirect()->route('notes.show', [$exam_board->slug, $note->id]);
    }

    /**
     * Edit notes
     */
    public function edit(ExamBoard $exam_board, Notes $note)
    {
        $is_edit = true;
        return view('resources.notes.create_edit', compact('exam_board', 'note', 'is_edit'));
    }

    /**
     * Update notes
     */
    public function update(ExamBoard $exam_board, Notes $note)
    {
        // Validation
        request()->validate([
            'subject' => 'required|integer',
            'chapter' => 'nullable|integer',
            'description' => 'nullable|string',
            'type' => 'nullable|string|in:comprehensive,concise',
        ]);

        // Update notes
        $note->update([
            'subject_id' => request('subject'),
            'chapter_id' => request('chapter'),
            'description' => request('description'),
            'type' => request('type'),
        ]);

        Session::flash($note->name . " updated successfully!");
        return redirect()->route('notes.show', [$exam_board->slug, $note->id]);
    }

    /**
     * (Soft) delete note
     */
    public function delete(ExamBoard $exam_board, Notes $note)
    {
        $subject = $note->subject;
        $note->delete();

        Session::flash('success', 'Note deleted successfully!');
        return redirect()->route('notes.subject_show', [$exam_board->slug, $subject->id]);
    }

    /**
     * Download PDF
     */
    public function downloadPdf(ExamBoard $exam_board, Notes $note)
    {
        $user = auth()->user();
        // Log event on Amplitude
        Amplitude::setUserId($user->id);
        Amplitude::queueEvent('download_resource', ['type' => "Notes", 'resourceable_id' => $note->id]);

        try {
            return Storage::disk('s3')->download( $note->url_relative_path );
        } catch(\Exception $e) {
            return redirect()->back()->withError('Download failed, please try again later.');
        }
    }
}
