<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ParentHomeworkAssigned extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($student_parent, $student, $homework)
    {
        $this->student_parent = $student_parent;
        $this->student = $student;
        $this->homework = $homework;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('classroom.email.teacher.homework_assigned')
                    ->with([
                        'student_parent' => $this->student_parent,
                        'student' => $this->student,
                        'homework' => $this->homework,
                    ]);
    }
}
