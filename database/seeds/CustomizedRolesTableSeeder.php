<?php

use Illuminate\Database\Seeder;

class CustomizedRolesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('roles')->delete();
        
        \DB::table('roles')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'admin',
                'display_name' => 'Administrator',
                'created_at' => '2020-04-26 06:09:25',
                'updated_at' => '2020-04-26 06:09:25',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'moderator',
                'display_name' => 'Moderator',
                'created_at' => '2020-05-01 08:51:41',
                'updated_at' => '2020-05-01 08:51:41',
            ),
        ));
        
        
    }
}