<a class="dropdown-item d-flex align-items-center" href="{{ getNotificationUrl($notification) }}">
    <div class="mr-3">
        <div class="icon-circle bg-primary">
            <i class="fas fa-file-alt text-white"></i>
        </div>
    </div>
    <div>
        <div class="font-size1 text-gray-500">{{ $notification->created_at->diffForHumans() }}</div>
        <div class="font-size2 font-weight500">{!! $notification->data['title'] !!}</div>
    </div>
</a>
