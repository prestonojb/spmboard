<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LibraryResource extends Model
{
    protected $guarded = [];
    protected $table = "library_resource";
}
