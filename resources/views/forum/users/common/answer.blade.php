<div class="panel mb-3" id="answer__{{ $answer->id }}">
    <div class="question-block__header">
        <label class="d-block">Answer</label>

        <ul class="nav question-block__icon-group mb-1">
            <button class="answer-block__award @if($answer->awarded_points) awarded @endif" data-answerid="{{ $answer->id }}" data-toggle="popover" data-trigger="focus" data-placement="top" data-content="Lights up when points are awarded to this answer.">
                <i class="fas fa-award"></i>
            </button>
            <button class="answer-block__check @if($answer->checked) checked @endif" data-answerid="{{ $answer->id }}" data-toggle="popover" data-trigger="focus" data-placement="top" data-content="Lights up when answer is accepted.">
                <i class="far fa-check-circle"></i>
            </button>
            <button class="answer-block__star @if($answer->recommended) recommend @endif" data-answerid="{{ $answer->id }}" data-toggle="popover" data-trigger="focus" data-placement="top" data-content="Lights up when answer is recommended by us!">
                <i class="fas fa-star"></i>
            </button>
        </ul>
    </div>

    <div class="answer__user-header">
        <div class="answer-block__user-info">
            <a href="{{ route('users.show', $answer->user->username) }}">
                <img class="avatar" src="{{ $answer->user->avatar ?? asset('img/essential/no-profile-picture.jpg') }}">
            </a>
            <div class="mt-1 text">
                <div class="user-info"><a class="username" href="{{ route('users.show', $answer->user->username) }}">{!! $answer->user->username_with_badge !!}</a><span class="about">@if($answer->user->headline), {{ $answer->user->headline }} @endif</span></div>
                <span class="time">Answered {{ $answer->created_at->longRelativeDiffForHumans() }}</span>
            </div>
        </div>
    </div>


    <div class="answer">
        <div class="tinymce-editor-contents m-0">
            <a href="{{ $answer->show_url }}" class="inactive">
                {!! $answer->answer !!}
                @if(!is_null($answer->img))
                    <img src="{{ $answer->img }}" class="">
                @endif
            </a>
        </div>
    </div>

    <div class="question-block__button-group">
        <button type="button" class="answer-upvote-button @if($answer->upvoted) upvoted @endif" data-answerid="{{ $answer->id }}"><span class="icon"></span> <span class="text">Upvote</span> · <span class="answer-upvote-count" data-answerid="{{ $answer->id }}">{{ readableNumber($answer->upvote_count) }}</span></button>
    </div>

</div>
