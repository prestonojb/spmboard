@auth
    <div class="dropdown">
        <button type="button" class="bg-transparent mr-1 mr-md-2 p-0" data-toggle="dropdown">
            <img class="avatar" src="{{ auth()->user()->avatar }}">
            <span>
                <i class="fa fa-caret-down" aria-hidden="true"></i>
            </span>
        </button>
            <div class="dropdown-menu logout-dropdown-menu right">
                <a href="{{ route('users.show', auth()->user()->username) }}" class="dropdown-item">My Profile</a>
                <a href="{{ route('payment') }}" class="dropdown-item">Payment</a>
                @if( auth()->user()->hasCoins() )
                    <a href="{{ route('coins.show') }}" class="dropdown-item">My Coins</a>
                @endif
                <a href="{{ route('settings') }}" class="dropdown-item">Settings</a>

                @if( auth()->user()->is_student() )
                    <a href="{{ route('products.coins') }}" class="dropdown-item dark-yellow">
                        <span class="iconify" data-icon="ri:coin-fill" data-inline="false"></span>
                        <span>Buy Coins</span>
                    </a>
                @endif
                @can('index', \App\CertificationApplication::class)
                    <a href="{{ route('certification_applications.index') }}" class="dropdown-item blue5">
                        <span class="iconify" data-icon="ic:baseline-verified" data-inline="false"></span>
                        <span>Get Certified</span></a>
                @endcan
                @if( auth()->user()->hasRewardPoints() )
                    <a href="{{ route('referral.get_view') }}" class="dropdown-item red">
                        <span class="iconify" data-icon="carbon:gift" data-inline="false"></span>
                        <span class="">Refer a friend</span>
                    </a>
                @endif
                <div class="dropdown-divider"></div>
                <form method="POST" action="{{ route('logout') }}">
                    @csrf
                    <button type="submit" class="dropdown-item">Log Out</button>
                </form>
            </div>
    </div>

    @if(isset($hide_rp_coins) && !$hide_rp_coins)
        @if( auth()->user()->hasCoins() || auth()->user()->hasRewardPoints() )
            <div class="header__coins-points-container">
                @if( auth()->user()->hasCoins() )
                    <button type="button" class="header__coins-points-button" data-container="body" data-toggle="popover" data-placement="bottom" data-content="You can use coins to claim study resources!">
                        <img class="inline-image" src="{{ asset('img/essential/coin.svg') }}" alt="">
                        <span class="value">{{ auth()->user()->coin_balance }}</span>
                    </button>
                @endif
                @if( auth()->user()->hasRewardPoints() )
                    <button type="button" class="header__coins-points-button" data-container="body" data-toggle="popover" data-placement="bottom" data-content="Enjoy an array of rewards with reward points!(Coming soon)">
                        <img class="inline-image" src="{{ asset('img/essential/points.svg') }}" alt="">
                        <span class="value">{{ auth()->user()->reward_point_balance }}</span>
                    </button>
                @endif
            </div>
        @endif
    @endif
@else
    <li>
        <a class="header-link mr-2" href="{{ route('login') }}">Log in</a>
    </li>
    <li>
        <a class="button--primary" href="{{ route('register') }}">Join for Free</a>
    </li>
@endauth
