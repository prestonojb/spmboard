<ul class="nav question-block__icon-group mb-1">
    <button class="question-block__award @if(!$question->points_claimed) unawarded @endif" data-toggle="popover" data-trigger="focus" data-placement="top" data-content="Lights up when points offered for question is not yet awarded.">
        <i class="fas fa-award"></i>
    </button>
    <button class="question-block__check @if($question->answered) checked @endif" data-toggle="popover" data-trigger="focus" data-placement="top" data-content="Lights up when question has an accepted answer.">
        <i class="far fa-check-circle"></i>
    </button>
    <button class="question-block__star @if($question->recommended) recommend @endif" data-toggle="popover" data-trigger="focus" data-placement="top" data-content="Lights up when question is recommended by us!">
        <i class="fas fa-star"></i>
    </button>
</ul>
