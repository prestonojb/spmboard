<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;
use Illuminate\Database\Eloquent\SoftDeletes;

class Notes extends Model
{
    use SoftDeletes;

    protected $guarded = [];

    /**
     * -----------------
     * Relationships
     * -----------------
     */
    public function subject()
    {
        return $this->belongsTo(Subject::class);
    }

    public function chapter()
    {
        return $this->belongsTo(Chapter::class);
    }

    public function exam_board()
    {
        return $this->hasOneThrough(ExamBoard::class, Subject::class);
    }

    public function resource()
    {
        return $this->morphOne('App\Resource', 'resourceable');
    }

    /**
     * Returns owner of notes
     */
    public function owner()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    /**
     * -----------------
     * Accessors
     * -----------------
     */

    public function getPreviewImgAttribute($value)
    {
        if($value){
            if(strpos($value, 'amazonaws')){
                //Amazon link for image
                return $value;
            } else{
                //Amazon S3
                $updated_value = str_replace('\\', '/', $value);
                return Storage::cloud()->url($updated_value);
            }
        } else {
            return asset('img/essential/note.svg');
        }
    }

    /**
     * Returns type tag HTML
     * @return stirng
     */
    public function getTypeColorAttribute()
    {
        $dict = [
            'concise' => 'red',
            'comprehensive' => 'green',
        ];
        return $dict[$this->type];
    }

    public function getDisplayTypeAttribute()
    {
        $dict = [
            'one_page' => 'One Page',
            'concise' => 'Concise',
            'comprehensive' => 'Comprehensive',
        ];
        return $dict[ $this->type ];
    }

    /**
     * Returns primary attribute
     * @return string
     */
    public function getPrimaryAttribute()
    {
        return $this->name;
    }

    /**
     * Returns secondary attribute
     * @return string
     */
    public function getSecondaryAttribute()
    {
        return $this->description;
    }

    /**
     * Returns all tags HTML
     * @return string
     */
    public function getAllTagsHtml()
    {
        $html = '';
        $html .= '<a class="status-tag sm '.$this->type_color.' mr-1 maxw-100px" href="javascript:void(0)">'.$this->display_type.'</a>';
        if(!is_null($this->chapter)) {
            $html .= '<a class="status-tag sm gray mr-1 maxw-100px" href="javascript:void(0)">'.$this->chapter->name.'</a>';
        }
        return $html;
    }

    /**
     * Returns brief description
     * @return string
     */
    public function getBriefDescriptionAttribute()
    {
        return str_limit(strip_tags($this->description), 350);
    }

    /**
     * Returns show URL
     * @return string
     */
    public function getShowUrlAttribute()
    {
        return $this->route;
    }

    // URL
    public function getUrlAttribute($value)
    {
        if(strpos($value, 'http://') !== false || strpos($value, 'https://') !== false) {
            return $value;
        }
        return $value ? Storage::cloud()->url($value) : null;
    }

    /**
     * Get original "url" column value from DB
     * @return string
     */
    public function getUrlRelativePathAttribute()
    {
        return $this->getOriginal('url');
    }

    public function getRouteAttribute()
    {
        return route('notes.show', [$this->subject->exam_board->slug, $this->id]);
    }

    /**
     * Return route to download note PDF
     * @return string
     */
    public function getDownloadRouteAttribute()
    {
        return route('notes.download_pdf', [$this->subject->exam_board->slug, $this->id]);
    }

    // --URL

    /**
     * Inverse hasOneThrough Query Builder Reference: https://stackoverflow.com/questions/39159285/laravel-5-hasmanythrough-inverse-querying
     * Returns an Eloquent builder of Notes by exam board
     *
     * @param string $exam_board Exam Board name('SPM', 'IGCSE'...)
     * @return Eloquent Builder
     */
    public static function getNotesByExamBoard($exam_board)
    {
        $notes = Notes::whereHas('subject.exam_board', function($q) use ($exam_board) {
            return $q->where('name', $exam_board);
        });

        return $notes;
    }

    /**
     * Get all notes type key-value pairs
     * @return array
     */
    public static function getTypes()
    {
        return [
            'one_page' => 'One Page',
            'comprehensive' => 'Comprehensive',
        ];
    }

    /**
     * Returns related notes
     * @return Collection
     */
    public function getRelatedNotesByChapter($take = 3)
    {
        return $this->chapter->notes()->where('id', '!=', $this->id)->take($take)->get();
    }

    /**
     * ===================
     * HELPERS
     * ===================
     */
    /**
     * Returns true if note is free
     * @return boolean
     */
    public function isFree()
    {
        return $this->coin_price == 0;
    }

    /**
     * Returns true if note is paid
     * @return boolean
     */
    public function isPaid()
    {
        return $this->coin_price > 0;
    }

    /**
     * Save PDF file to S3
     * @param UploadedFile $file
     */
    public function saveFile($file)
    {
        //Store file to S3
        $path = $file->store('resources/notes/', 's3');
        $filename = $file->hashName();
        //Save image url in database
        $file_url = 'resources/notes/'.$filename;
        $this->update([
            'url' => $file_url,
        ]);
    }

    /**
     * Returns array of all available types
     * @return array
     */
    public static function getAllTypes()
    {
        return [
            'comprehensive' => 'Comprehensive',
            'concise' => 'Concise',
        ];
    }

    /**
     * Returns adjusted coin price (based on buyer's subscription)
     * @param App\User $buyer
     * @return int
     */
    public function getAdjustedCoinPrice($buyer)
    {
        // Get purchase price
        if($buyer->is_teacher() && $buyer->teacher->hasPremiumSubscription()) {
            return getCoinPriceAfterDiscount($this->coin_price, setting('board_premium.resource_discount_in_percentage'));
        } elseif($buyer->is_student() && $buyer->hasActiveBoardPass()) {
            return getCoinPriceAfterDiscount($this->coin_price, setting('board_pass.resource_discount_in_percentage'));
        } else {
            return $this->coin_price;
        }
    }

    /**
     * Returns coin discount
     * @return int
     */
    public function getCoinDiscount($buyer)
    {
        if(!is_int($this->getAdjustedCoinPrice($buyer))) {
            throw new \Exception("Coin price must be an integer.");
        }
        return $this->coin_price - $this->getAdjustedCoinPrice($buyer);
    }

    /**
     * Save preview image (first page of PDF file)
     * @param UploadedFile $file
     */
    public function savePreviewImage($file)
    {
        $this->resource->savePreviewImage($file);
    }

    /**
     * Update preview image (first page of PDF file)
     */
    public function updatePreviewImage()
    {
        $this->resource->updatePreviewImage();
    }

    /**
     * Unlock resource for user
     */
    public function unlockFor(User $buyer)
    {
        $this->resource->unlockFor($buyer);
    }
}
