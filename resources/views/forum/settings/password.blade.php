@extends('forum.layouts.app')

@section('meta')
<title>Password Settings | {{ config('app.name') }}</title>
@endsection

@section('content')
@include('main.navs.header')

<div class="py-2"></div>

<div class="container">
    <section class="row">
        <div class="col-12 col-lg-9">
            @include('forum.settings.common.pills')
            <div class="panel">
                <form method="POST" action="{{ route('users.changePassword', auth()->user()) }}">
                    @csrf

                    <div class="mt-2 mb-4">
                        <h3 class="">Password</h3>
                        <p class="gray2">Keep your passwords safe!</p>
                    </div>

                    @if(!is_null(auth()->user()->password))
                        <div class="form-field">
                            <label>Old password</label>
                            <input type="password" name="old_password">
                            @error('old_password')
                                <div class="error-message">{{ $message }}</div>
                            @enderror
                        </div>
                    @endif

                    <div class="form-field">
                        <label>New Password</label>
                        <input type="password" name="new_password">
                        @error('new_password')
                            <div class="error-message">{{ $message }}</div>
                        @enderror
                    </div>

                    <div class="form-field">
                        <label>Confirm New Password</label>
                        <input type="password" name="new_password_confirmation">
                        @error('new_password_confirmation')
                            <div class="error-message">{{ $message }}</div>
                        @enderror
                    </div>

                    <button type="submit" class="button--primary mb-1">Save Changes</button>
                </form>
            </div>
        </div>
    </section>
</div>

<div class="py-2"></div>
@endsection

@section('scripts')
@endsection
