{{-- Filter --}}
<form action="{{ url()->current() }}" method="get">
    @csrf
    <div class="filter-container">
        {{-- Chapter --}}
        <div class="select-container">
            <select name="chapter" id="" class="filter-select--lg">
                <option value="" disabled selected>Chapter</option>
                <option value="-1" @if(request('chapter') == -1) selected @endif>General</option>
                @foreach( $subject->chapters as $chapter )
                    <option value="{{ $chapter->id }}" @if(request('chapter') == $chapter->id) selected @endif>{{ $chapter->name }}</option>
                @endforeach
            </select>

            {{-- Sort By
            @php
                $sort_by_filters = [
                    'newest' => 'Newest',
                    'low_to_high_price' => 'Low to High (price)',
                    'high_to_low_price' => 'High to Low (price)',
                ];
            @endphp
            <select name="sort_by" id="" class="filter-select--lg">
                <option value="" selected>All</option>
                @foreach($sort_by_filters as $key => $value)
                    <option value="{{ $key }}">{{ $value }}</option>
                @endforeach
            </select> --}}
        </div>

        <div class="filter-buttons">
            <button type="button" class="button-link underline-on-hover mr-2 clearFilterButton">Clear</button>
            <button type="submit" class="button--primary">Apply Filter</button>
        </div>
    </div>
</form>
