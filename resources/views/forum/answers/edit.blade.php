    @extends('forum.layouts.app')

@section('meta')
<title>Edit Answer | {{ $exam_board->name }} {{ config('app.name') }}</title>

<meta property="og:image" content="{{ asset('img/logo/logo_sm.png') }}" />
<meta property="og:type" content="website" />
@endsection

@section('styles')
{{-- Quill  --}}
<link href="https://cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">
@endsection

@section('content')
@include('common.header2')

<div class="py-3"></div>

<div class="maxw-740px mx-auto">

    <div class="mb-2">
        <a class="inactive font-size2 gray2" href="{{ route('questions.show', [$exam_board->slug, $answer->question->id]) }}">< Back to question</a>
    </div>
    <h1 class="font-size7">Edit Answer</h1>

    <div class="panel">
        @component('forum.answers.common.form', ['question' => $answer->question, 'is_edit' => true, 'answer' => $answer])
        @endcomponent
    </div>
</div>
@endsection

@section('js_dependencies')
    {{-- TinyMCE --}}
    <script src="https://cdn.tiny.cloud/1/si4jvonmpvtlfypotvf9zu2vci2zsjavfd4v1oyvnzncszag/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>    {{-- KaTeX --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ" crossorigin="anonymous"></script>
@endsection
