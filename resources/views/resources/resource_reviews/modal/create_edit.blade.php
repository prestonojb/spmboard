@php
$is_edit = isset($is_edit) && $is_edit;
if($is_edit) {
    $rating = old('rating') ?? $note->rating;
    $comment = old('comment') ?? $note->comment;
    $url = route('resource_reviews.update', [$exam_board->slug, $resource_review->id]);
} else {
    $rating = old('rating');
    $comment = old('comment');
    $url = route('resource_reviews.store', [$exam_board->slug, $resource->id]);
}
@endphp


@if($errors->any())
<script>
    $(function(){
        console.log('hi');
        $('#createEditResourceReviewModal').modal('show');
    });
</script>
@endif


{{-- Create Resource Review Modal --}}
<div class="modal fade" id="createEditResourceReviewModal" tabindex="-1" role="dialog" aria-labelledby="createEditResourceReviewModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form method="POST" action="{{ $url }}" enctype="multipart/form-data">
                @csrf
                @if($is_edit)
                    @method('PATCH')
                @endif
                <div class="modal-header">
                    <h5 class="modal-title" id="createEditResourceReviewModal">Write a Review</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    {{-- Rating --}}
                    <div class="form-group">
                        <label for="rating">Rating<span class="red">*</span></label>
                        <div class="d-flex justify-content-start align-items-center">
                            <input class="mr-1" name="rating" type="number" min="1" max="5" step="1" required style="width: 60px;">
                            <span>/ 5</span>
                        </div>

                        @error('rating')
                            <div class="error-message">{{ $message }}</div>
                        @enderror
                    </div>

                    {{-- Comment --}}
                    <div class="form-group">
                        <label for="comment">Comment</label>
                        <textarea type="text" name="comment" placeholder="Comment" row="3" required>{{ $comment }}</textarea>
                        @error('comment')
                            <div class="error-message">{{ $message }}</div>
                        @enderror
                    </div>

                    <div class="form-field">
                        <span class="red">*</span>Required fields
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="button--primary--inverse" data-dismiss="modal">Close</button>
                    <button type="button" class="button--primary loadOnSubmitButton" style="width: 62px;">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
