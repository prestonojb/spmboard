<?php

namespace App\Http\Controllers\Voyager;

use \TCG\Voyager\Http\Controllers\VoyagerBaseController as VoyagerBaseController;
use Illuminate\Http\Request;
use App\Chapter;
use App\Subject;
use Excel;
use App\Imports\ChapterImport;

class VoyagerChapterController extends VoyagerBaseController
{
    public function index(Request $request)
    {
        $subjects = Subject::all();
        $selected_subject = isset(request()->subject) && !is_null(request()->subject) ? Subject::find(request('subject'))
                                                                                      : null;
        return view('vendor.voyager.chapters.index', compact('subjects', 'selected_subject'));
    }

    /**
     * Create
     */
    public function create(Request $request)
    {
        $subjects = Subject::all();
        $is_edit = false;
        return view('vendor.voyager.chapters.create_edit', compact('subjects', 'is_edit'));
    }

    /**
     * Bulk create
     */
    public function bulk_create(Request $request)
    {
        return view('vendor.voyager.chapters.bulk_create');
    }

    public function store(Request $request)
    {
        // Validation
        $request->validate([
            'name' => 'required',
            'description' => 'required',
            'subject' => 'required',
            'parent_chapter' => 'nullable'
        ]);

        // Get subject
        $subject = Subject::find(request('subject'));

        // Create chapter
        $chapter = $subject->chapters()->create([
            'name' => request('name'),
            'description' => request('description'),
            'subject_id' => request('subject'),
            'parent_chapter_id' => request('parent_chapter'),
        ]);

        return redirect()->route('voyager.chapters.show', $chapter->id)->with(['alert-type' => 'success', 'message' => 'Chapter added successfully!']);
    }

    /**
     * Bulk store
     */
    public function bulk_store(Request $request)
    {
        // Validation
        request()->validate([
            'file' => 'required'
        ]);
        // Import rows from excel sheet to DB
        Excel::import(new ChapterImport, request()->file('file'));

        return redirect()->back()->with(['alert-type' => 'success', 'message' => 'Chapters added successfully!']);
    }

    public function edit(Request $request, $id)
    {
        $subjects = Subject::all();
        $is_edit = true;
        $chapter = Chapter::find($id);
        return view('vendor.voyager.chapters.create_edit', compact('subjects', 'is_edit', 'chapter'));
    }

    public function update(Request $request, $id)
    {
        // Validation
        $request->validate([
            'name' => 'required',
            'description' => 'required',
            'subject' => 'required',
        ]);

        $chapter = Chapter::find($id);

        // Update chapter
        $chapter->update([
            'name' => request('name'),
            'description' => request('description'),
            'subject_id' => request('subject'),
            'parent_chapter_id' => request('parent_chapter'),
        ]);

        return redirect()->route('voyager.chapters.show', $chapter->id)->with(['alert-type' => 'success', 'message' => 'Chapter updated successfully!']);
    }

    /**
     * Order chapters
     */
    public function reorder()
    {
        $chapter_order = json_decode(request('chapter_order'));
        for($i=0; $i < count($chapter_order); $i++) {
            $chapter = Chapter::find($chapter_order[$i]);
            $chapter->update([
                'chapter_number' => $i + 1
            ]);
        }
        return redirect()->back()->with(['alert-type' => 'success', 'message' => 'Chapter order updated successfully!']);
    }
}
