@extends('main.layouts.app')

@section('meta')
<title>My Libraries | {{ config('app.name') }}</title>
@endsection

@section('content')
@include('main.navs.header')
<div class="py-2"></div>

<div class="container">
    <div class="d-flex flex-wrap justify-content-between align-items-center border-bottom pb-2 mb-2">
        <h1 class="font-size9">My Libraries</h1>
        {{-- Delete Library --}}
        <form action="{{ route('libraries.create') }}" method="POST">
            @csrf
            <a href="{{ route('libraries.create') }}" type="button" class="button--primary">
                <span class="iconify" data-icon="codicon:add" data-inline="false"></span> Add Library</a>
        </form>
    </div>

    <p class="gray2">Number of libraries remaining: {{ auth()->user()->getNoOfLibrariesRemaining() }}</p>
    <div class="row">
        <div class="col-12 col-md-6 col-lg-4 mb-2">
            @component('libraries.common.block', ['user' => auth()->user(), 'type' => 'unlocked_resources'])
            @endcomponent
        </div>
        @foreach($libraries as $library)
            <div class="col-12 col-md-6 col-lg-4 mb-2">
                @component('libraries.common.block', ['library' => $library])
                @endcomponent
            </div>
        @endforeach
    </div>
</div>

@endsection

@section('scripts')
@endsection
