<?php

namespace App;

use App\Question;
use App\Subject;
use Complex\Exception;
use Illuminate\Database\Eloquent\Model;

class Chapter extends Model
{
    public $guarded = [];
    public $additional_attributes = ['name_with_subject_and_exam_board'];

    /**
     * ------------------
     * Relationships
     * ------------------
     */
    public function subject()
    {
        return $this->belongsTo(Subject::class);
    }

    public function questions()
    {
        return $this->belongsToMany(Question::class);
    }

    public function notes()
    {
        return $this->hasMany(Notes::class);
    }

    public function free_notes()
    {
        return $this->hasMany(Notes::class)->where('coin_price', 0);
    }

    public function topical_past_papers()
    {
        return $this->hasMany(TopicalPastPaper::class);
    }

    public function free_topical_past_papers()
    {
        return $this->hasMany(TopicalPastPaper::class)->where('coin_price', 0);
    }

    public function guides()
    {
        return $this->hasMany(Guide::class);
    }

    public function resource_posts()
    {
        return $this->hasMany(ResourcePost::class);
    }

    public function exam_builder_items()
    {
        return $this->hasMany(ExamBuilderItem::class);
    }

    public function exam_builder_sheets()
    {
        return $this->belongsToMany(ExamBuilderSheet::class);
    }

    public function subchapters()
    {
        return $this->hasMany(self::class, 'parent_chapter_id', 'id')->orderBy('chapter_number');
    }

    public function parent_chapter()
    {
        return $this->belongsTo(self::class, 'parent_chapter_id', 'id');
    }

    /**
     * ------------
     * Accessors
     * ------------
     */

    public function getNameWithSubjectAndExamBoardAttribute()
    {
        return "[{$this->subject->exam_board->name} {$this->subject->name}] {$this->name}";
    }

    /**
     * Returns all chapters tag HTMLs
     */
    public static function getChapterTagsHtml( $chapters )
    {
        $html = '';
        foreach($chapters as $chapter) {
            $html .= "<span class='tag mr-1'>{$chapter->name}</span>";
        }
        return $html;
    }

    /**
     * Get Chapter Name
     * @return string
     */
    public function getNameAttribute($value)
    {
        return $this->isParentChapter() ? $value
                                        : "{$this->parent_chapter->name} - {$value}";
    }

    /**
     * Get Chapter original name
     * @return string
     */
    public function getOriginalNameAttribute($value)
    {
        return $this->getOriginal('name');
    }

    /**
     * ------------
     * Mutators
     * ------------
     */
    public function setNameAttribute($value)
    {
        $this->attributes['name'] = $value;
        $this->attributes['slug'] = str_slug($value);
    }

    /**
     * Returns true if chapter has notes
     * @return boolean
     */
    public function hasNotes()
    {
        return count($this->notes ?? []) > 0;
    }

    /**
     * Returns notes with type
     * @param string $type
     * @return Collection
     */
    public function getNotes( $type = null )
    {
        if(!is_null($type)) {
            return $this->notes()->where('type', $type)->get();
        } else {
            return $this->notes;
        }
    }

    /**
     * Returns index URL
     * @return string
     */
    public function getIndexUrlAttribute()
    {
        return route('chapter', [$this->subject->exam_board->slug, $this->id]);
    }

    /**
     * -----------------
     * Helper functions
     * -----------------
     */

    /**
     * Returns true if chapter has guide
     * @param App\Guide $guide
     * @return boolean
     */
    public function hasGuide(Guide $guide)
    {
        return $this->guides()->pluck('id')->contains($guide->id);
    }

    /**
     * Returns true if chapter is parent chapter (no subchapter)
     * @return boolean
     */
    public function isParentChapter()
    {
        return is_null($this->parent_chapter_id);
    }

    /**
     * Returns true if chapter is subchapter (has parent chapter)
     * @return boolean
     */
    public function isSubchapter()
    {
        return !is_null($this->parent_chapter_id);
    }

    /**
     * Returns true if chapter has subchapters
     * @return boolean
     */
    public function hasSubchapters()
    {
        return $this->getSubchaptersCount() > 0;
    }

    /**
     * Returns number of subchapters of chapter
     * @return int
     */
    public function getSubchaptersCount()
    {
        return count($this->subchapters ?? []);
    }

    /**
     * Returns all subchapter IDs, including self
     * @param boolean $include_self Set to true if to include self ID
     * @return array
     * @throws Exception Chapter is not a Parent Chapter
     */
    public function getRelatedChapterIds($include_self = true)
    {
        if($this->isParentChapter()) {
            $subchapter_ids = $this->subchapters()->pluck('id')->toArray();
            if($include_self) {
                array_push($subchapter_ids, $this->id);
            }
            return $subchapter_ids;
        } else {
            throw new Exception("Chapter is not a Parent Chapter");
        }
    }

    /**
      * Get previous chapter of subject based on chapter number
      * Returns last chapter if current chapter is the first chapter when loop is true, else return null
      * @return \App\Chapter|null
      */
    public function previous( $loop = false ){
        $chapter = self::where('subject_id', $this->subject_id)->where('chapter_number', '<', $this->chapter_number)->orderBy('chapter_number','desc')->first();

        if($loop) {
            return $chapter ? $chapter
                            : self::where('subject_id', $this->subject_id)->orderBy('chapter_number', 'desc')->first();
        } else {
            return $chapter;
        }
    }

     /**
      * Get next chapter based on chapter number
      * Return first chapter if current chapter is the last chapter of subject when loop is true, else return nullw1e
      */
    public function next( $loop = false ){
        $chapter = self::where('subject_id', $this->subject_id)->where('chapter_number', '>', $this->chapter_number)->orderBy('chapter_number','asc')->first();

        if($loop) {
            return $chapter ? $chapter
                            : self::where('subject_id', $this->subject_id)->orderBy('chapter_number', 'asc')->first();
        } else {
            return $chapter;
        }
    }

    /**
     * Returns all (available) topical papers paper numbers
     * Reference: https://stackoverflow.com/questions/42447682/distinct-values-with-pluck
     * @return array
     */
    public function getTopicalPastPapersPaperNumbers()
    {
        return $this->topical_past_papers()->groupBy('paper_number')->pluck('paper_number')->sortBy('paper_number')->toArray();
    }
}
