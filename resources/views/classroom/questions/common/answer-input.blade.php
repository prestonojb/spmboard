<div class="panel answer-input ml-2 mb-2">
    <form method="POST" action="{{ route('answers.store', $question->id) }}" class="" enctype="multipart/form-data">
        @csrf
        <div class="mb-2">
            <label>Answer</label>

            <div class="editor"></div>
            <input type="hidden" name="answer">

            @error('answer')
                <span class="error-message">{{ $message }}</span>
            @enderror
        </div>

        <div class="form-field">
            <label>Image<span style="font-weight:400;">(Optional)</span></label>

            <div class="file-drop-area">
                <span class="fake-btn">Choose files</span>
                <span class="file-msg">or drag and drop files here</span>
                <input class="file-input" name="img" type="file">
            </div>

            <img class="file-input-preview">

            @error('img')
                <span class="error-message">{{ $message }}</span>
            @enderror
        </div>

        <div class="text-center">
            <button type="submit" class="button--primary answer-input__answer-button answerButton">Answer</button>
        </div>
    </form>
</div>
