<?php

namespace Tests\Feature\Voyager;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\UploadedFile;
use Tests\TestCase;
use App\ExamBoard;
use App\Mail\AccountCredentials;
use App\Mail\SetupGuide;
use Illuminate\Support\Facades\Mail;
use App\State;
use App\Student;
use App\User;
use Excel;

class UserTest extends TestCase
{
    use WithFaker, WithoutMiddleware;

    public function setUp() : void
    {
        parent::setUp();

        // Student
        $this->user = factory(User::class)->states('teacher')->create();
        // Make user admin
        $this->user->makeAdmin();
    }

    /**
     * @group voyager_user
     * Test import student and parent user view
     */
    public function testGetStudentAndParentImportView()
    {
        $response = $this->actingAs($this->user)
                         ->get( route('voyager.users.import.get_student_and_parent_view'));
        $response->assertSuccessful();
    }

    /**
     * group voyager_user
     * Test import student and parent user
     * ** Failed to cast storage file to UploadedFile and validation fails
     * Works when file is uploaded from client
     */
    public function testStudentAndParentImport()
    {
        Excel::fake('s3');

        // ------------ Empty ------------ //
        Mail::fake();
        $this->clearDatabase();
        $init_count = User::count();
        $response = $this->actingAs($this->user)
                         ->post( route('voyager.users.import.student_and_parent'), [
                            'file' => $this->getEmptySheet(),
                         ]);
        $this->assertEquals($init_count, User::count());

        Mail::assertSent(AccountCredentials::class, 0);
        Mail::assertSent(SetupGuide::class, 0);

        // Assert redirect
        $response->assertRedirect();


        // ------------ Valid (No existing users) ------------ //
        Mail::fake();

        $this->clearDatabase();
        $init_count = User::count();
        $response = $this->actingAs($this->user)
                         ->post( route('voyager.users.import.student_and_parent'), [
                            'file' => $this->getValidSheet(),
                         ]);
        $this->assertEquals($init_count + 3, User::count());
        // First row
        $student = User::where('email', 'student_test@gmail.com')->first();
        $this->assertTrue( $student->has_parent() && $student->parent->email == 'parent_test@gmail.com' );

        // Second row
        $student0 = User::where('email', 'student_test0@gmail.com')->first();
        $this->assertFalse( !$student0->has_parent() );

        Mail::assertSent(AccountCredentials::class, 3);
        Mail::assertSent(SetupGuide::class, 0);


        // ------------ Valid (With existing users) ------------ //
        Mail::fake();

        $init_count = User::count();
        $response = $this->actingAs($this->user)
                         ->post( route('voyager.users.import.student_and_parent'), [
                            'file' => $this->getValidSheet(),
                         ]);
        $this->assertEquals($init_count, User::count());

        Mail::assertSent(AccountCredentials::class, 0);
        Mail::assertSent(SetupGuide::class, 3);
        $response->assertRedirect();


        // ------------ Invalid 1 ------------ //
        Mail::fake();

        $this->clearDatabase();
        $response = $this->actingAs($this->user)
                         ->post( route('voyager.users.import.student_and_parent'), [
                            'file' => $this->getInvalidOneSheet(),
                         ]);
        $response->assertSessionHasErrors();

        Mail::assertSent(AccountCredentials::class, 0);
        Mail::assertSent(SetupGuide::class, 0);


        // ------------ Invalid 2 ------------ //
        Mail::fake();

        $this->clearDatabase();
        $response = $this->actingAs($this->user)
                         ->post( route('voyager.users.import.student_and_parent'), [
                            'file' => $this->getInvalidTwoSheet(),
                         ]);
        $response->assertSessionHasErrors();

        Mail::assertSent(AccountCredentials::class, 0);
        Mail::assertSent(SetupGuide::class, 0);

    }

    /**
     * Delete all rows with users with emails involved in this test
     */
    public function clearDatabase()
    {
        User::whereIn('email', ['student_test@gmail.com', 'student_test0@gmail.com', 'teacher_test@gmail.com', 'teacher_test0@gmail.com', 'parent_test@gmail.com', 'parent_test0@gmail.com'])->delete();
    }

    /**
     * Get Empty Sheet
     * @return File
     */
    public function getEmptySheet()
    {
        $path = storage_path('tests/users/import/empty.xlsx');
        return new \Illuminate\Http\UploadedFile($path, 'empty.xlsx', 'xlsx');
    }

    /**
     * Get Valid Sheet
     * @return File
     */
    public function getValidSheet()
    {
        $path = storage_path('tests/users/import/valid.xlsx');
        return new \Illuminate\Http\UploadedFile($path, 'valid.xlsx', 'xlsx');
    }

    /**
     * Get Empty Sheet
     * @return File
     */
    public function getInvalidOneSheet()
    {
        $path = storage_path('tests/users/import/invalid_1.xlsx');
        return new \Illuminate\Http\UploadedFile($path, 'invalid_1.xlsx', 'xlsx');
    }

    /**
     * Get Empty Sheet
     * @return File
     */
    public function getInvalidTwoSheet()
    {
        $path = storage_path('tests/users/import/invalid_2.xlsx');
        return new \Illuminate\Http\UploadedFile($path, 'invalid_2.xlsx', 'xlsx');
    }
}
