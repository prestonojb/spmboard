@component('mail::message')
# Contact Us Form
## {{ $subject }}

@component('mail::panel')
{{ $body }}
@endcomponent

@endcomponent
