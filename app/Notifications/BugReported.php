<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use App\BugReport;

class BugReported extends Notification implements ShouldQueue
{
    use Queueable;

    public $bug_report;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(BugReport $bug_report)
    {
        $this->bug_report = $bug_report;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $description = $this->bug_report->description ?? 'N/A';
        return (new MailMessage)
                    ->greeting("Bug Reported")
                    ->line("Tag: {$this->bug_report->tag}")
                    ->line("Description: {$description}")
                    ->action('Learn More', route('voyager.bug-reports.show', $this->bug_report->id));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
