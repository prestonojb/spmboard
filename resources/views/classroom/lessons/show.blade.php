@extends('classroom.layouts.app')

@section('meta')
    <title>Lesson - {{ $lesson->name }} | {{ config('app.name') }} Classroom</title>
@endsection

@section('styles')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.1.2/css/tempusdominus-bootstrap-4.min.css">
@endsection

@section('content')

@component('classroom.common.breadcrumbs')
    <a href="{{ route('classroom.classrooms.show', $classroom->id) }}">Dashboard</a> /
    <a href="{{ route('classroom.lessons.index', $classroom->id) }}">Lessons</a> /
    {{ $lesson->name }}
@endcomponent

@component('classroom.common.page_heading', ['title' => $lesson->name, 'subtitle' => $lesson->classroom->name])
    <div class="mb-0 gray2">{!! $lesson->secondary !!}</b></div>

    @slot('button_group')
        <div class="d-flex justify-content-center align-items-center">
            @can('update', $lesson)
                <a href="{{ route('classroom.lessons.edit', [$lesson->classroom->id, $lesson->id]) }}" class="button--primary--inverse mr-1"><span class="iconify" data-icon="feather:edit" data-inline="false"></span> Edit</a>
            @endcan
            @can('delete', $lesson)
                <form action="{{ route('classroom.lessons.destroy', [$lesson->classroom->id, $lesson->id]) }}" method="post">
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="button--danger deleteButton"><span class="iconify" data-icon="emojione-monotone:cross-mark" data-inline="false"></span> Delete</button>
                </form>
            @endcan
        </div>
    @endslot
@endcomponent

<div class="panel maxw-740px mx-auto p-0 mt-2">
    <div class="panel-header">Description</div>
    <div class="panel-body">
        @if( !is_null($lesson->description) )
            <div class="tinymce-editor-contents">
                {!! $lesson->description !!}
            </div>
        @else
            <div class="text-center py-1 gray2">
                No description
            </div>
        @endif
    </div>
</div>

<div class="mt-5 mb-4"></div>

<div class="row">
    <div class="col-12 col-lg-6">
        {{-- Resource Posts --}}
        @component('classroom.common.page_subheading', ['title' => 'Resource Posts'])
            @slot('button_group')
                @can('update', $lesson)
                    <a href="{{ route('classroom.lessons.resource_posts.create', $lesson->id) }}" class="button--primary--inverse sm"><i class="fas fa-plus"></i> Add</a>
                @endcan
            @endslot
        @endcomponent
        @if( count($lesson->resource_posts ?? []) )
            @foreach( $lesson->resource_posts as $resource_post )
                @component('classroom.resource_posts.common.block', ['resource_post' => $resource_post])
                @endcomponent
            @endforeach
        @else
            <div class="text-center my-4">
                <img src="{{ asset('img/essential/empty.svg') }}" alt="No Resource Post" width="260" height="260" class="mb-2">
                <h4 class="font-size3">No Resource Post</h4>
            </div>
        @endif
    </div>

    <div class="col-12 col-lg-6">
        {{-- Homeworks --}}
        @component('classroom.common.page_subheading', ['title' => 'Homeworks'])
            @slot('button_group')
                @can('update', $lesson)
                    <a href="{{ route('classroom.lessons.homeworks.create', $lesson->id) }}" class="button--primary--inverse sm"><i class="fas fa-plus"></i>  Add</a>
                @endcan
            @endslot
        @endcomponent
        @if( count($lesson->homeworks ?? []) )
            @foreach( $lesson->homeworks as $homework )
                @component('classroom.homeworks.common.block', ['homework' => $homework])
                @endcomponent
            @endforeach
        @else
            <div class="text-center my-4">
                <img src="{{ asset('img/essential/no-notification.svg') }}" alt="No Homework" width="260" height="260" class="mb-2">
                <h4 class="font-size3">No Homework</h4>
            </div>
        @endif
    </div>
</div>

@endsection
