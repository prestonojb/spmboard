<div class="panel mb-2">
    <h3><b>Resources</b></h3>

    <form action="{{ route('classroom.lessons.resource_posts.store', $classroom->id) }}" method="POST" id="postResourcesForm">
        @csrf
        <input type="hidden" name="resource_data">
        <input type="hidden" name="custom_resource_data">

        <div class="form-field">
            <label>Title</label>
            <input type="text" name="title" placeholder="Title">
            @error('title')
                <span class="error-message">{{ $message }}</span>
            @enderror
        </div>

        <div class="form-field">
            <label>Description(Optional)</label>
            <textarea name="description" id="" rows="3" placeholder="Description"></textarea>
            @error('description')
                <span class="error-message">{{ $message }}</span>
            @enderror
        </div>

        <div class="form-field">
            <div class="form-field">
                <label>Chapters <span style="font-weight: 400">(Optional)</span></label>
                <select class="chapter-select" name="chapter">
                    <option selected disabled>Select Chapter</option>
                    @foreach($subject->chapters as $chapter)
                        <option value="{{ $chapter->id }}">
                            {{ $chapter->name }}
                        </option>
                    @endforeach
                    </optgroup>

                </select>
                @error('chapter')
                    <span class="error-message">{{ $message }}</span>
                @enderror
            </div>
        </div>

        <div class="file-block-container">
            <div class="row">
            </div>
        </div>

        <div class="d-flex justify-content-between align-items-center">
            <div class="dropdown">
                <button type="button" class="button--primary--inverse" data-toggle="dropdown">Add</button>
                <div class="dropdown-menu">
                    <button type="button" class="dropdown-item" data-toggle="modal" data-target=".prebuilt-notes-modal">Notes</button>
                    <button type="button" class="dropdown-item" data-toggle="modal" data-target=".prebuilt-pp-modal">Past Papers</button>
                    <button type="button" class="dropdown-item" data-toggle="modal" data-target=".prebuilt-tpp-modal">Topical Past Papers</button>
                    <button type="button" class="dropdown-item" id="customFileUploadButton">Custom File</button>
                </div>
            </div>
            <button type="submit" class="button--primary">Post</button>
        </div>
    </form>

    <form id="customFileForm" enctype="multipart/form-data" method="POST">
        @csrf
        <input type="file" name="custom_file" hidden>
    </form>
</div>
