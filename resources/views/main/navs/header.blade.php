@php
$exam_boards = App\ExamBoard::all()
@endphp

{{-- Desktop Top Navbar --}}
<nav class="navbar navbar-expand-lg header base-header">
<div class="container">
    <!-- Hamburger-icon -->
    <div class="base-header__left">
        <button class="hamburger-button" type="button" aria-expanded="false" aria-label="Toggle navigation">
            <i class="fa fa-bars" aria-hidden="true"></i>
        </button>

        <a class="base-header__brand" href="{{ route('/') }}">
            <img class="" src="{{ asset('img/logo/logo_light.png') }}">
        </a>
        <ul class="d-none d-lg-flex base-header__menu">
            {{-- Forum --}}
            <li class="base-header__menu-item">
                <a href="javascript:void(0)" class="">Forum <i class="fas fa-chevron-down"></i></a>
                <ul class="dropdown-menu">
                    @foreach($exam_boards as $exam_board)
                        <li class="">
                            <a class="stretched-link" href="{{ route('questions.index', $exam_board->slug) }}">{{ $exam_board->name }}</a>
                        </li>
                    @endforeach
                </ul>
            </li>

            @if(Gate::allows('access-resource'))
                {{-- Study Resources --}}
                <li class="base-header__menu-item">
                    <a href="javascript:void(0)" class="">Study Resources <i class="fas fa-chevron-down"></i></a>
                    <ul class="dropdown-menu">
                        @foreach($exam_boards as $exam_board)
                            <li class="">
                                <div class="d-flex justify-content-between align-items-center">
                                    <a href="javascript:void(0)" class="">
                                        {{ $exam_board->name }}
                                    </a>
                                    <i class="fas fa-chevron-right"></i>
                                </div>
                                <ul class="dropdown-menu submenu__level-2">
                                    <li>
                                        <a class="stretched-link" href="{{ route('past_papers.home', $exam_board->slug) }}">Past Papers</a>
                                    </li>
                                    @if(Gate::allows('access-marketplace'))
                                        <li>
                                            <a class="stretched-link" href="{{ route('topical_past_papers.home', $exam_board->slug) }}">Topical Worksheets
                                                @if(setting('beta.marketplace')) <span class="badge badge-primary">BETA</span> @endif
                                            </a>
                                        </li>
                                        <li>
                                            <a class="stretched-link" href="{{ route('notes.home', $exam_board->slug) }}">Notes
                                                @if(setting('beta.marketplace')) <span class="badge badge-primary">BETA</span> @endif
                                            </a>
                                        </li>
                                    @endif
                                    <li>
                                        <a class="stretched-link" href="{{ route('guides.home', $exam_board->slug) }}">Guides</a>
                                    </li>
                                </ul>
                            </li>
                        @endforeach
                    </ul>
                </li>
            @endif

            {{-- Exam Builder --}}
            @if(Gate::allows('access-exam-builder'))
                <li class="base-header__menu-item">
                    <a href="javascript:void(0)" class="">Exam Builder
                        <span class="badge badge-danger">NEW</span>
                        <i class="fas fa-chevron-down"></i></a>
                    <ul class="dropdown-menu">
                        @foreach( $exam_boards as $exam_board )
                            <li class="">
                                <a class="stretched-link" href="{{ route('exam_builder.landing').'?exam_board='.$exam_board->slug }}">{{ $exam_board->name }}</a>
                            </li>
                        @endforeach
                    </ul>
                </li>
            @endif

            {{-- Classroom --}}
            @if(Gate::allows('access-classroom'))
                <li class="base-header__menu-item">
                    <a href="{{ route('classroom.landing') }}" class="stretched-link @if(request()->is('classroom')) active @endif">Classroom
                        @if(setting('beta.classroom')) <span class="badge badge-primary">BETA</span> @endif
                    </a>
                </li>
            @endif

            <li class="base-header__menu-item">
                <a href="javascript:void(0)" class="">More <i class="fas fa-chevron-down"></i></a>
                <ul class="dropdown-menu">
                    <li class="{{ request()->is('about') ? 'active' : '' }}">
                        <a class="stretched-link" href="{{ route('about') }}">About</a>
                    </li>
                    <li class="{{ request()->is('blog/*') || request()->is('blog') ? 'active' : '' }}">
                        <a class="stretched-link" href="{{ route('blog.index') }}">Blog</a>
                    </li>
                </ul>
            </li>

        </ul>
    </div>

    <ul class="header-right-menu">
        @component('common.header.profile_dropdown', ['hide_rp_coins' => true])
        @endcomponent
    </ul>
    </div>
</nav>


{{-- ----------------------------------------------------------  MOBILE  ----------------------------------------------------------- --}}


{{-- Mobile Menu --}}
<div class="d-lg-none mobile-body-overlay">
    <nav class="animated slideInLeft faster mobile-nav-container">
        <a class="mobile-nav-brand mobile-base-nav-brand" href="{{ url('/') }}">
            <img class="" src="{{ asset('img/logo/logo_light.png') }}">
        </a>
        <div class="mb-2">
            <ul class="mobile-nav-menu">
                {{-- Forum --}}
                <li class="mobile-nav-item">
                    <span class="mobile-nav-icon"><span class="iconify" data-icon="cil:newspaper" data-inline="false"></span></span>
                    <a href="javascript:void(0)" class="mobile-nav-link">Forum</a>
                    <span class="mobile-nav-dropdown-arrow"><i class="fas fa-chevron-right"></i></span>
                </li>
                <ul class="mobile-nav-dropdown-menu">
                    @foreach($exam_boards as $board)
                        <li class="mobile-nav-dropdown-menu-item">
                            <a class="stretched-link mobile-nav-dropdown-menu-link" href="{{ route('questions.index', $board->slug) }}">{{ $board->name }}</a>
                        </li>
                    @endforeach
                </ul>

                {{-- Study Resources --}}
                <li class="mobile-nav-item">
                    <span class="mobile-nav-icon"><span class="iconify" data-icon="bx:bx-book" data-inline="false"></span></span>
                    <a href="javascript:void(0)" class="mobile-nav-link">Study Resources</a>
                    <span class="mobile-nav-dropdown-arrow"><i class="fas fa-chevron-right"></i></span>
                </li>

                {{-- Exam Builder --}}
                @if(Gate::allows('access-exam-builder'))
                    <li class="mobile-nav-item">
                        <span class="mobile-nav-icon"><span class="iconify" data-icon="carbon:model-builder" data-inline="false"></span></span>
                        <a href="javascript:void(0)" class="mobile-nav-link">
                            Exam Builder
                            <span class="badge badge-danger">NEW</span>
                        </a>
                        <span class="mobile-nav-dropdown-arrow"><i class="fas fa-chevron-right"></i></span>
                    </li>
                    <ul class="mobile-nav-dropdown-menu">
                        @foreach($exam_boards as $exam_board)
                            <li class="mobile-nav-dropdown-menu-item">
                                <a class="stretched-link mobile-nav-dropdown-menu-link" href="{{ route('exam_builder.landing').'?exam_board='.$exam_board->slug }}">{{ $exam_board->name }}</a>
                            </li>
                        @endforeach
                    </ul>
                @endif

                <ul class="mobile-nav-dropdown-menu">
                    @foreach($exam_boards as $board)
                        <li class="mobile-nav-dropdown-menu-item">
                            <a class="stretched-link mobile-nav-dropdown-menu-link" href="javascript:void(0)">{{ $board->name }}</a>
                            <span class="mobile-nav-dropdown-arrow"><i class="fas fa-chevron-right"></i></span>
                        </li>
                        <ul class="mobile-nav-dropdown-menu__level-2">
                            <li class="mobile-nav-dropdown-menu-item">
                                <a class="stretched-link mobile-nav-dropdown-menu-link" href="{{ route('past_papers.home', $board->slug) }}">Past Papers</a>
                            </li>
                            @if(Gate::allows('access-marketplace'))
                                <li class="mobile-nav-dropdown-menu-item">
                                    <a class="stretched-link mobile-nav-dropdown-menu-link" href="{{ route('topical_past_papers.home', $board->slug) }}">Topical Worksheets</a>
                                    @if(setting('beta.marketplace')) <span class="badge badge-primary">BETA</span> @endif
                                </li>
                                <li class="mobile-nav-dropdown-menu-item">
                                    <a class="stretched-link mobile-nav-dropdown-menu-link" href="{{ route('notes.home', $board->slug) }}">Notes</a>
                                    @if(setting('beta.marketplace')) <span class="badge badge-primary">BETA</span> @endif
                                </li>
                            @endif
                            <li class="mobile-nav-dropdown-menu-item">
                                <a class="stretched-link mobile-nav-dropdown-menu-link" href="{{ route('guides.home', $board->slug) }}">Guides</a>
                            </li>
                        </ul>
                    @endforeach
                </ul>

                {{-- Classroom --}}
                @if(Gate::allows('access-classroom'))
                    <li class="mobile-nav-item @if(request()->is('classroom')) active @endif">
                        <span class="mobile-nav-icon"><span class="iconify" data-icon="la:chalkboard-teacher-solid" data-inline="false"></span></span>
                        <a href="{{ route('classroom.landing') }}" class="stretched-link mobile-nav-link">
                            Classroom
                            @if(setting('beta.classroom')) <span class="badge badge-primary">BETA</span> @endif
                        </a>
                    </li>
                @endif

                {{-- More --}}
                <li class="mobile-nav-item">
                    <span class="mobile-nav-icon"><span class="iconify" data-icon="eva:more-horizontal-outline" data-inline="false"></span></span>
                    <a href="javascript:void(0)" class="mobile-nav-link">More</a>
                    <span class="mobile-nav-dropdown-arrow"><i class="fas fa-chevron-right"></i></span>
                </li>
                <ul class="mobile-nav-dropdown-menu">
                    <li class="mobile-nav-dropdown-menu-item">
                        <a class="stretched-link mobile-nav-dropdown-menu-link" href="{{ route('about') }}">About</a>
                    </li>
                    <li class="mobile-nav-dropdown-menu-item">
                        <a class="stretched-link mobile-nav-dropdown-menu-link" href="{{ route('blog.index') }}">Blog</a>
                    </li>
                </ul>


            </ul>
        </div>

        @guest
            <div class="base-header__login-links-container">
                <a class="header-link" href="{{ route('login') }}">Log In</a>
                <a class="button--primary py-2" href="{{ route('register') }}">Join For Free</a>
            </div>
        @endguest

        <section class="nav-footer mobile-nav-footer py-2">
            © Board {{ now()->year }}
        </section>
    </nav>
</div>
