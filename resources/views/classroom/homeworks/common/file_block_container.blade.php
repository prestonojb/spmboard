<div class="file-block-container">
    <div class="form-row">
        @foreach( $homework->resources as $resource )
            <div class="col-12 col-md-6">
                @include('classroom.common.file_block', ['resource' => $resource, 'type' => $resource->pivot->type])
            </div>
        @endforeach
        @foreach( $homework->getCustomResources() as $custom_resource )
            <div class="col-12 col-md-6">
                @component('classroom.common.file_block', ['custom_resource' => $custom_resource])
                @endcomponent
            </div>
        @endforeach
    </div>
</div>
