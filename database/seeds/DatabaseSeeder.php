<?php

use App\Answer;
use App\User;
use App\Question;
use App\Subject;
use App\Chapter;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        $this->call(ExamBoardsTableSeeder::class);
        $this->call(ClassroomSeeder::class);
        // $this->call(BlogCategoriesTableSeeder::class);
        // $this->call(StatesTableSeeder::class);
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }

}
