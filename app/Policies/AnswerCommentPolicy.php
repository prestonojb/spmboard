<?php

namespace App\Policies;

use App\User;
use App\AnswerComment;
use Illuminate\Auth\Access\HandlesAuthorization;

class AnswerCommentPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function store(User $user)
    {
        return !$user->parent;
    }

    public function delete(User $user, AnswerComment $answer_comment)
    {
        return $user->id == $answer_comment->user_id;
    }
}
