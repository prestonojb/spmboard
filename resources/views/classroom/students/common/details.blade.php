<div class="user-profile__header d-flex justify-content-between align-items-center py-2">
    <div class="d-flex">
        <img class="profile-img mr-2 mr-lg-3" src="{{ $student->avatar }}">
        <div class="mt-1 mt-md-2">
            <h1 class="h2 d-inline-block mb-1 mr-2"><strong>{{ $student->username }}</strong></h1>
            <h2 class="font-size3 gray2">{{ $student->secondary }}</h2>
        </div>
    </div>
</div>
