<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ExamBoard;
use App\Resource;
use Illuminate\Support\Facades\Session;

class ResourceController extends Controller
{
    public function updateAllPreviewImages()
    {
        $msg = Resource::updateAllPreviewImages();

        Session::flash('success', $msg);
        return redirect('/admin')->with(['alert-type' => 'success', 'message' => $msg]);
    }
}
