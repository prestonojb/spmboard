<?php

namespace App\Notifications;

use App\Answer;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\HtmlString;

class AnswerRecommended extends Notification implements ShouldQueue
{
    use Queueable;

    public $answer;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Answer $answer)
    {
        $this->answer = $answer;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database'];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $answer = $this->answer;
        $question = $this->answer->question;

        return [
            'type' => Answer::class,
            'target' => $answer,
            'title' => "<b>Hooray!</b> Your answer for question \"<b>{$question->question}</b>\" is recommended by the Board team! You are awarded <b>20 reward points</b>!",
        ];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $answer = $this->answer;
        $question = $this->answer->question;

        $line = "The Board community needs more amazing answerers like you! <b>Click on the button below</b> to help the Board community answer more questions! <br><br>";
        $line .= "Earn more reward points whenever your answer gets accepted/recommended and use your points to claim <b>amazing study materials</b>!";

        return (new MailMessage)
                    ->subject("🎉 Hooray! Your answer is recommended by Board!")
                    ->greeting("Your answer for question \"{$question->question}\" is recommended by the Board team! You are awarded 20 reward points! 🤩")
                    ->line(new HtmlString($line))
                    ->action('Answer More Questions', route('questions.index', $question->subject->exam_board->slug))
                    ->line("See you on Board! 🚀");
    }
}
