<?php

namespace App\Http\Controllers;

use App\Classroom;
use App\Lesson;
use App\Notifications\Classroom\InvoiceIssued;
use App\Student;
use App\Teacher;
use App\TeacherInvoice;
use App\TeacherInvoiceItem;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use PDF;

class ClassroomInvoiceController extends Controller
{
    /**
     * List all invoices
     */
    public function index()
    {
        $this->authorize('index', TeacherInvoice::class);

        $user = auth()->user();

        // Get invoices
        if( $user->is_teacher() ) {
            // Teacher
            $invoices = TeacherInvoice::where('teacher_id', $user->teacher->user_id);
        } elseif( $user->is_parent() ) {
            // Parent
            $invoices = TeacherInvoice::whereIn('student_id', $user->parent->children_ids);
        }

        // Issued days ago
        if( isset(request()->issued_days_ago) ) {
            $days_ago = request()->issued_days_ago;
            $from = Carbon::today()->subDays( $days_ago );
            $invoices = $invoices->where('created_at', '>', $from);
        }

        // Amount
        if( isset(request()->amount) ) {
            $amount = request()->amount;
            // 0 - 200
            if( $amount == 0 ) {
                $invoices = $invoices->whereBetween('amount', [0, 200]);
            } elseif( $amount <= 1000 ) {
                // 201 - 1000
                $invoices = $invoices->whereBetween('amount', [$amount + 1, $amount + 200]);
            } else {
                // > 1000
                $invoices = $invoices->where('amount', '>', 1000);
            }
        }

        $invoices = $invoices->get();

        return view('classroom.invoices.index', compact('invoices'));
    }

    /**
     * Create Invoice
     */
    public function create()
    {
        $this->authorize('store', TeacherInvoice::class);

        $teacher = auth()->user()->teacher;
        $students = $teacher->students();

        $setting = $teacher->invoice_setting;

        return view('classroom.invoices.create', compact('teacher', 'students', 'unpaid_lessons', 'setting'));
    }

    /**
     * Store Invoice
     */
    public function store()
    {
        $this->authorize('store', TeacherInvoice::class);
        request()->validate([
            'student' => 'required',
            'due_in_days' => 'required',
        ]);

        // Decode items JSON
        $invoice_items = json_decode( request('teacher_invoice_items') );

        $teacher = auth()->user()->teacher;
        $student = Student::where('user_id', request()->student)->first();

        $setting = $teacher->createOrGetInvoiceSetting();
        $invoice_number = $setting->getNextInvoiceNumber( true );

        $non_lesson_amount = 0;
        $lesson_amount = 0;

        // Validate for non-empty items
        if( is_null($invoice_items) ) {
            return redirect()->back()->withInput()->withErrors(['invoice_items' => 'Please fill up at least one invoice item!']);
        }

        // Calculate total for invoice
        if( !is_null($invoice_items) ) {
            foreach( $invoice_items as $item ) {
                // Back-end validation for invoice items field
                if( is_null($item->title) || is_null($item->no_of_units) || is_null($item->price_per_unit) ) {
                    return redirect()->back()->with('error', 'Oops! The invoice item field seem to not be filled up correctly, please double check whether all the required fields are filled up properly and try again.');
                }
                if( !is_null( $item->lesson_id ) ) {
                    $lesson = Lesson::find( $item->lesson_id );
                    $price_per_unit = $item->price_per_unit;
                    $lesson_hours = $lesson->duration_in_hours;
                    $lesson_amount += $lesson_hours * $price_per_unit;
                } else {
                    $non_lesson_amount += $item->no_of_units * $item->price_per_unit;
                }
            }
        }

        $amount = $lesson_amount + $non_lesson_amount;

        // Create Teacher Invoice
        $teacher_invoice = TeacherInvoice::create([
            'teacher_id' => $teacher->user_id,
            'student_id' => $student->user_id,
            'amount' => $amount,
            'due_at' => Carbon::today()->addDays( request()->due_in_days )->endOfDay(),
            'invoice_number' => $invoice_number,
        ]);

        if( !is_null($invoice_items) ) {
            foreach( $invoice_items as $item ) {
                $teacher_invoice->items()->create([
                    'lesson_id' => $item->lesson_id,
                    'title' => $item->title,
                    'no_of_units' => $item->no_of_units,
                    'price_per_unit' => $item->price_per_unit,
                ]);
            }
        }

        // Generate invoice PDF and send to parent
        if( $teacher_invoice->student->has_parent() ) {
            $teacher_invoice->student->parent->notify( new InvoiceIssued( $teacher_invoice, $teacher_invoice->generatePdf() ) );
        }

        Session::flash('success', 'Invoice created successfully!');
        return redirect()->route('classroom.invoices.show', $teacher_invoice);
    }

    public function show(TeacherInvoice $teacher_invoice)
    {
        $this->authorize('show', $teacher_invoice);
        $invoice = $teacher_invoice;
        return view('classroom.invoices.show', compact('invoice'));
    }

    public function delete( TeacherInvoice $teacher_invoice )
    {
        $this->authorize('delete', $teacher_invoice);
        if( $teacher_invoice->status == 'paid' ){
            return redirect()->back()->with('error', 'Paid invoice cannot be deleted!');
        }

        $teacher_invoice->items()->delete();
        $teacher_invoice->delete();

        return redirect()->route('classroom.invoices.index')->with('error', 'Invoice deleted successfully!');
    }

    public function mark_as_paid( TeacherInvoice $teacher_invoice )
    {
        $this->authorize('mark_as_paid', $teacher_invoice);
        $teacher_invoice->update([
            'paid_at' => Carbon::now(),
        ]);

        return redirect()->back()->with('success', 'Invoice marked as paid!');
    }

    public function download_pdf( TeacherInvoice $teacher_invoice )
    {
        $this->authorize('download_pdf', $teacher_invoice);
        $data = [
            'invoice' => $teacher_invoice
        ];
        $pdf = PDF::loadView('classroom.pdf.invoice', $data);
        return $pdf->download($teacher_invoice->invoice_number.'.pdf');
    }

    // Teacher Billing(Subscription)
    public function download($invoiceId)
    {
        return auth()->user()->downloadInvoice($invoiceId, [
            'vendor' => config('app.name'),
            'product' => 'Board for Teacher Classrooms'
        ]);
    }
}
