@php
    // Declare variables
    if( !is_null($guide) ) {
        $title = old('title') ?? $guide->title;
        $description = old('description') ?? $guide->description;
        $subject_id = old('subject') ?? $guide->subject_id;
        $chapter_id = old('chapter') ?? $guide->chapter_id;
        $body = old('body') ?? $guide->body;
        $author_id = old('author_id') ?? $guide->user_id;
    } else {
        $title = old('title');
        $description = old('description');
        $subject_id = old('subject');
        $chapter_id = old('chapter');
        $body = old('body');
        $author_id = old('author_id');
    }
    $url = $is_edit ? route('voyager.guides.update', $guide->id) : route('voyager.guides.store');
@endphp

<form class="form-edit-add" role="form"
        action="{{ $url }}"
        method="POST" enctype="multipart/form-data" autocomplete="off">
        {{ csrf_field() }}
    @if($is_edit)
        @method('PATCH')
    @endif

    <div class="panel" style="max-width: 740px; padding: 12px; margin: 0 auto;">
        {{-- Title --}}
        <div class="form-group">
            <label for="">Title</label>
            <input type="text" name="title" class="form-control" placeholder="Title" id="" required value="{{ $title }}">
            @error('title')
                <div class="text-danger">{{ $message }}</div>
            @enderror
        </div>

        {{-- Description --}}
        <div class="form-group">
            <label for="">Description</label>
            <textarea name="description" class="form-control" id="" rows="3" required placeholder="Description">{{ $description }}</textarea>
            @error('description')
                <div class="text-danger">{{ $message }}</div>
            @enderror
            <p class="gray2">This field is not shown to the users and is only for SEO.</p>
        </div>

        {{-- Subject --}}
        <div class="form-group">
            <label>Subject</label>
            <select class="subject-select" name="subject_id">
                <option data-placeholder="true"></option>
                @foreach($subjects as $subject)
                    <option value="{{ $subject->id }}" {{ $subject_id == $subject->id ? 'selected="selected"' : '' }}>{{ $subject->name_with_exam_board }}</option>
                @endforeach
            </select>
            @error('subject')
                <span class="error-message">{{ $message }}</span>
            @enderror
        </div>

        {{-- Chapters --}}
        <div class="form-group">
            <label>Chapters</label>
            <select class="chapter-select" name="chapter_id" disabled>
                <option></option>
                @foreach($subjects as $subject)
                    <optgroup label="{{ $subject->name_with_exam_board }}">
                        @foreach($subject->chapters as $chapter)
                            <option value="{{ $chapter->id }}"
                                {{ $chapter_id == $chapter->id ? 'selected="selected"' : '' }}>
                                {{ $chapter->name }}
                            </option>
                        @endforeach
                    </optgroup>
                @endforeach
            </select>
        </div>

        {{-- Body --}}
        <div class="form-group">
            <label for="">Body</label>
            <div class="loading-icon loading in-editor blue"></div>
            <textarea class="tinymce-editor" name="body">{!! $body !!}</textarea>
            @error('body')
                <div class="error-message">{{ $message }}</div>
            @enderror
        </div>

        {{-- Authored By --}}
        @php
            $user_emails = commaSeperatedStringToArray(setting('resources.guide_authors_email_list'));
            $users = App\User::whereIn('email', $user_emails)->get();
        @endphp
        <div class="form-group">
            <label for="">Authored By</label>
            <select name="author_id" id="">
                <option value="">Select Author</option>
                @foreach($users as $user)
                    <option value="{{ $user->id }}" @if($author_id == $user->id) selected @endif>{{ '@'.$user->username }} ({{ $user->email }})</option>
                @endforeach
            </select>
            @error('author_id')
                <div class="text-danger">{{ $message }}</div>
            @enderror
        </div>

        <div class="form-group"></div>

        <div class="text-right">
            <button type="submit" class="btn btn-primary">
                Save
            </button>
        </div>
    </div>
</form>
