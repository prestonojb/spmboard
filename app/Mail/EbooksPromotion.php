<?php

namespace App\Mail;

use App\Subject;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class EbooksPromotion extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($exam_board, $subjects)
    {
        $this->exam_board = $exam_board;
        $this->subjects = $subjects;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.ebooks_promotion')
                    ->with([
                        'subjects' => $this->subjects,
                        'url' => route('questions.index', $this->exam_board->slug)
                    ]);
    }
}
