<?php

namespace App\Http\Controllers;

use App\Recommendation;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Notifications\RecommendationGiven;
use Illuminate\Support\Facades\Notification;

class RecommendationController extends Controller
{
    public function store(User $recommendee)
    {
        // Validation
        request()->validate([
            'description' => 'required|string|min:20|max:500',
            'relationship' => ['required', 'string', Rule::in(['student_of', 'worked_with', 'parent_of_student_of'])],
        ]);

        $recommender = auth()->user();

        // Recommendee and recommender cannot be the same
        if($recommendee->id == $recommender->id) {
            return redirect()->back()->with('error', 'You cannot recommend yourself!');
        }

        // Throw error if recommender (user) already has already recommended the recommendee
        if( $recommendee->hasRecommendationFrom($recommender) ) {
            return redirect()->back()->with('error', 'You have already recommended this user. Consider updating your existing recommendation instead.');
        }

        // Create recommendation
        $recommendation = Recommendation::create([
                            'user_id' => $recommendee->id,
                            'recommended_by_user_id' => $recommender->id,
                            'description' => request('description'),
                            'relationship' => request('relationship'),
                        ]);

        Notification::send($recommendee, new RecommendationGiven($recommendation));
        return redirect()->back()->with('success', 'Recommendation added to profile successfully!');
    }

    public function update(Recommendation $recommendation)
    {
        // Validation
        request()->validate([
            'description' => 'required|string|min:20|max:500',
            'relationship' => ['required', 'string', Rule::in(['student_of', 'worked_with', 'parent_of_student_of'])],
        ]);

        // Update recommendation
        $recommendation->update([
            'description' => request('description'),
            'relationship' => request('relationship'),
        ]);

        return redirect()->back()->with('success', 'Recommendation updated successfully!');
    }
}
