<?php

namespace App\Http\Controllers;

use App\Article;
use App\User;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    /**
     * Index article
     */
    public function index(User $user)
    {
        if(!$user->is_teacher()) {
            return redirect()->back()->withError('User\'s account type cannot post articles!');
        }
        $articles = $user->articles()->latest()->paginate(10);
        return view('forum.users.profile.articles', compact('user', 'articles'));
    }

    /**
     * Show article
     */
    public function create(User $user, Article $article)
    {
        $this->authorize('create_store', Article::class);
        $is_edit = false;
        $article = null;
        return view('forum.users.profile.articles.create_edit', compact('user', 'is_edit', 'article'));
    }

    /**
     * Store article
     */
    public function store(User $user, Article $article)
    {
        $this->authorize('create_store', Article::class);
        // Validation
        request()->validate([
            'title' => 'required|string|max:100',
            'description' => 'required|string',
            'body' => 'required|string',
            'tags' => 'nullable',
            'cover_img' => 'nullable|mimes:jpeg,png,jpg,gif,svg',
        ]);

        $user = auth()->user();
        // Create Article
        $article = $user->articles()->create([
                        'title' => request('title'),
                        'description' => request('description'),
                        'body' => request('body'),
                        'tags' => request('tags'),
                    ]);
        return redirect()->route('users.articles.show', $article->id)->with('success', 'Article posted successfully!');
    }

    /**
     * Edit article
     */
    public function edit(User $user, Article $article)
    {
        $this->authorize('edit_update', $article);
        $is_edit = true;
        return view('forum.users.profile.articles.create_edit', compact('user', 'is_edit', 'article'));
    }

    public function update(Article $article)
    {
        $this->authorize('edit_update', $article);
        // Validation
        request()->validate([
            'title' => 'required|string|max:100',
            'description' => 'required|string',
            'body' => 'required|string',
            'tags' => 'nullable',
            'cover_img' => 'nullable|mimes:jpeg,png,jpg,gif,svg',
        ]);

        // Update article
        $article->update([
            'title' => request('title'),
            'description' => request('description'),
            'body' => request('body'),
            'tags' => request('tags'),
        ]);

        // Save cover image
        if(request()->has('cover_img') && !is_null(request()->file('cover_img'))) {
            $article->updateCoverImage( request()->file('cover_img') );
        }

        return redirect()->route('users.articles.show', $article->id)->withSuccess('Article updated successfully!');
    }

    /**
     * Show article
     */
    public function show(User $user, Article $article)
    {
        views($article)->record();
        return view('forum.users.profile.articles.show', compact('user', 'article'));
    }

    /**
     * Delete Article
     */
    public function delete(Article $article)
    {
        $this->authorize('delete', $article);
        // Delete article
        $article->delete();
        return redirect()->route('users.articles', $article->user->username)->with('error', 'Article deleted successfully!');
    }
}
