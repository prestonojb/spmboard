<header class="store__header">
    <div class="container">
        <h4></h4>
        <h1 class="mb-1"><b>{{ $title }}</b></h1>
        <h2 class="h4 gray2">{{ $description }}</h2>
    </div>
</header>
