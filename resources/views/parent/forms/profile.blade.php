<form method="POST" action="{{ route('parent.update') }}" enctype="multipart/form-data">
    @csrf
    @method('PATCH')
    @if( isset($getting_started) && $getting_started )
        <input type="hidden" name="getting_started" value="1">
    @endif
    <div class="mt-2 mb-4">
        <h3 class="">Basic Details</h3>
        <p class="gray2">Fill in your basic details.</p>
    </div>

    {{-- Name --}}
    <div class="form-field">
        <label>Name</label>
        <input type="text" name="name" placeholder="Name" value="{{ old('name') ?? $parent->name }}"/>
        @error('name')
            <div class="error-message">{{ $message }}</div>
        @enderror
    </div>

    {{-- Phone Number --}}
    <div class="form-field">
        <label>Phone Number</label>
        <input type="text" name="phone_number" placeholder="Phone Number" value="{{ old('phone_number') ?? $parent->phone_number }}"/>
        <p class="font-size1 gray2">This information will not be displayed publicly.</p>
        @error('phone_number')
            <div class="error-message">{{ $message }}</div>
        @enderror
    </div>

    <div class="text-center">
        <button type="submit" class="button--primary mb-1">Update</button>
    </div>
</form>
