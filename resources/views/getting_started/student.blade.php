@extends('forum.layouts.app')

@section('meta')
    <title>Getting Started as a Student | {{ config('app.name') }}</title>
@endsection

@section('content')
@include('main.navs.header')

<div class="py-4"></div>

<div class="ask-wrapper">
    <div class="text-center">
        <h1 class="heading">Getting Started as a Student</h1>
        <h2 class="sub-heading mb-3">Tell us more about yourself so we can cater your experience with us!</h2>
    </div>

    @component('student.forms.profile', ['student' => auth()->user()->student, 'exam_boards' => $exam_boards, 'states' => $states, 'getting_started' => true])
    @endcomponent
</div>

<div class="py-4"></div>

@endsection
