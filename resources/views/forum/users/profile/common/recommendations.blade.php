<div class="panel p-0">
    <div class="panel-header">
        <div class="d-flex justify-content-between align-items-center flex-wrap">
            <h3 class="mb-0">Recommendations</h3>

            @auth
                @if(auth()->user()->canRecommend($user))
                    <button type="button" class="button--primary--inverse sm" data-toggle="modal" data-target="#recommendationModal">{{ $user->hasRecommendationFrom(auth()->user()) ? 'Update Recommendation' : 'Give Recommendation' }}</button>
                    {{-- Recommendation form --}}
                    @component('forum.users.profile.recommendations.common.form', ['is_edit' => $user->hasRecommendationFrom(auth()->user()), 'recommendee' => $user])
                    @endcomponent
                @endif
            @endauth
        </div>
    </div>

    <div class="panel-body">
        @if(count($user->recommendations ?? []) > 0)
            @foreach($user->recommendations()->take(3)->get() as $recommendation)
                <div class="py-1"></div>

                @component('forum.users.profile.recommendations.common.block', ['recommendation' => $recommendation])
                @endcomponent

                <div class="border-bottom"></div>
            @endforeach

            <div class="pt-2 text-center">
                <button class="button-link blue4 underline-on-hover" data-toggle="modal" data-target="#allRecommendationsModal">
                    View All ({{ count($user->recommendations ?? []) }})
                </button>
            </div>

            @component('forum.users.profile.recommendations.common.all_modal', ['user' => $user])
            @endcomponent
        @else
            <div class="py-1 gray2 text-center">
                No recommendations yet
            </div>
        @endif
    </div>
</div>
