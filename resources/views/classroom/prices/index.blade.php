@extends('classroom.layouts.app')

@section('meta')
    <title>Plans | {{ config('app.name') }} Classroom</title>
@endsection

@section('content')
@component('classroom.common.page_heading', ['title' => 'Plans'])
@endcomponent


@if( auth()->user()->teacher )
    @component('classroom.navs.teacher.billing')
    @endcomponent

    @include('classroom.prices.main.teacher')

@endif

@endsection

@section('scripts')
<script>
$(function(){
    $('.subscribeNowButton').click(function(){
        var price = $(this).data('price');
        var pricePerInterval = $(this).data('price-per-interval');
        var price_id = $(this).data('price-id');
        $('[name=price]').val( price_id );

        $('#subscriptionModal span.price').text( price );
        $('#subscriptionModal span.price-per-interval').text( pricePerInterval );
    });

    var hasErrors = "{{ $errors->any() }}" == 1;
    if( hasErrors ) {
        $('#subscriptionModal').modal('show');
    }
});
</script>
@endsection

