<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePastPapersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('past_papers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name'); // 2017 MRSM Paper 2
            $table->unsignedInteger('subject_id');
            $table->unsignedInteger('year');
            $table->unsignedInteger('paper_number');
            $table->string('qp_url');
            $table->string('ms_url')->nullable();
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('past_papers');
    }
}
