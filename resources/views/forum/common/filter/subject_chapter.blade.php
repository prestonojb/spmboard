{{-- Filter Button --}}
<button class="button--primary sm" data-toggle="modal" data-target="#subjectListModal">
    Subject/Chapter <i class="fas fa-angle-down"></i>
</button>

{{-- Subject --}}
<div class="modal" tabindex="-1" id="subjectListModal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            {{-- Subjects --}}
            <div class="modal-header">
                <h5 class="modal-title">Subjects</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">
                <div class="" role="tablist">
                    {{-- Subject List --}}
                    @foreach($exam_board->subjects()->whereHas('questions')->get() as $subject)

                        <div class="@if(!$loop->last) border-bottom mb-2 @endif pb-2">

                            <div class="d-flex justify-content-start align-items-center">
                                {{-- Icon --}}
                                <div class="blue6 mr-2 font-size11">
                                    {!! $subject->icon !!}
                                </div>

                                {{-- Text --}}
                                <div>
                                    <a href="{{ $subject->index_url }}" class="blue-underline font-size4">
                                        <b>{{ $subject->name }}</b></a> <small class="gray2">({{ count($subject->questions ?? []) }})</small>
                                        <button type="button" class="inactive underline-on-hover font-size1 p-0" data-toggle="modal" data-target="#{{ str_slug($subject->name) }}_chapter_modal">{{ count($subject->chapters ) }} chapters</button>
                                    <p class="gray2 font-size2 font-weight-normal mb-0">{{ str_limit($subject->description, 50) }}</p>
                                </div>
                            </div>
                        </div>

                        {{-- Chapter --}}
                        <div class="modal" tabindex="-1" id="{{ str_slug($subject->name) }}_chapter_modal">
                            <div class="modal-dialog modal-xl modal-dialog-scrollable">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title">Chapters of {{ $subject->name }}</h5>
                                        <button type="button" class="close" onclick="$(this).closest('.modal').modal('hide');"  aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>

                                    {{-- Chapter List --}}
                                    <div class="modal-body">
                                        @foreach($subject->chapters()->whereHas('questions')->get() as $chapter)
                                            <div class="@if(!$loop->last) border-bottom mb-2 @endif pb-2">
                                                <a href="{{ $chapter->index_url }}" class="blue-underline font-size4">
                                                    <b>{{ $chapter->name }}</b></a>
                                                    <small class="gray2">({{ count($chapter->questions ?? []) }})</small>
                                                <p class="gray2 font-size2 font-weight-normal mb-0">{{ str_limit($chapter->description, 50) }}</p>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
