<?php

namespace App\Traits;
use App\Question;
use App\Subject;
use App\Chapter;
use App\ExamBoard;
use App\User;
use CyrildeWit\EloquentViewable\Support\Period;

trait QuestionSortable
{
    /**
     * Sort questions by exam board
     * @param App\ExamBoard $exam_board
     * @param int $perPage
     * @return Collection
     */
    public static function sortByExamBoard(ExamBoard $exam_board, $perPage=20)
    {
        // Inverse hasOneThrough Query Builder Reference: https://stackoverflow.com/questions/39159285/laravel-5-hasmanythrough-inverse-querying
        $questions = Question::whereHas('subject.exam_board', function($q) use ($exam_board) {
                                    return $q->where('id', $exam_board->id);
                                });
        $questions = self::sort($questions);
        return $questions->paginate($perPage);
    }

    /**
     * Sort questions by subject
     * @param App\Subject $subject
     * @param int $perPage
     * @return Collection
     */
    public static function sortForSubject(Subject $subject, $perPage=20)
    {
        $questions = Question::where('subject_id', $subject->id);
        $questions = self::sort($questions);
        return $questions->paginate($perPage);
    }

    /**
     * Sort questions by chapter
     * @param App\Chapter $chapter
     * @param int $perPage
     * @return Collection
     */
    public static function sortForChapter(Chapter $chapter, $perPage=20)
    {
        $questions = Question::whereHas('chapters', function($query) use ($chapter) {
                                    $query->where('chapters.id', '=', $chapter->id);
                                });
        $questions = self::sort($questions);
        return $questions->paginate($perPage);
    }

    /**
     * Sort Questiosn by 'sort'
     * @param Query Builder $questions
     * @return Query Builder
     */
    public static function sort($questions)
    {
        switch(request('sort')){
            case 'top':
                //withCount gets related table and counts number of rows in the related table
                return $questions->withCount('question_upvotes')
                                        ->orderByDesc('question_upvotes_count');
            case 'new':
                return $questions->orderByDesc('created_at');
            // Answered
            // Show answered questions
            // Ordered by number of views for the past 5 days
            case 'answered':
                $answered_questions = $questions->whereHas('answers', function($query){
                                            $query->where('checked', 1)->orWhere(function($q) {
                                                $q->whereNotNull('recommended_by_user_id')->whereNotNull('recommended_at');
                                            });;
                                        });

                return $answered_questions->orderByViews('desc', Period::pastDays(1));
            // Hot
            // Show unanswered questions
            // Ordered by the most RP offered, THEN oldest questions first
            default:
                $unanswered_questions = $questions->whereDoesntHave('answers', function($query){
                                            $query->where('checked', 1)->orWhere(function($q) {
                                                $q->whereNotNull('recommended_by_user_id')->whereNotNull('recommended_at');
                                            });
                                        });
                return $unanswered_questions->orderByDesc('reward_points')->orderBy('created_at');
        }
    }
}
