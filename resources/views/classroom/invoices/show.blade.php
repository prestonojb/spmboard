@extends('classroom.layouts.app')

@section('meta')
    <title>Invoice - {{ $invoice->invoice_number }} | {{ config('app.name') }} Classroom</title>
@endsection

@section('content')

{{-- Breadcrumbs --}}
@component('classroom.common.breadcrumbs')
    <a href="{{ route('classroom.landing') }}">Home</a> /
    <a href="{{ route('classroom.invoices.index') }}">Invoices</a> /
    {{ $invoice->invoice_number }}
@endcomponent

{{-- Page Heading --}}
@component('classroom.common.page_heading', ['title' => $invoice->title, 'subtitle' => $invoice->subtitle])
    <div class="mb-0 gray2">{!! $invoice->secondary !!}</b></div>
    @slot('button_group')
        @can('download_pdf', $invoice)
            <form action="{{ route('classroom.invoices.download_pdf', $invoice->id) }}" method="post">
                @csrf
                <button class="button--primary--inverse mr-1" type="submit"><i class="fas fa-arrow-down"></i> Download PDF</button>
            </form>
        @endcan
        @can('mark_as_paid', $invoice)
            <form action="{{ route('classroom.invoices.mark_as_paid', $invoice->id) }}" method="post">
                @csrf
                <button type="submit" class="button--success mr-1 markAsPaidButton" @if($invoice->status == 'paid') disabled @endif>Mark as Paid</button>
            </form>
        @endcan
        @can('delete', $invoice)
            <form action="{{ route('classroom.invoices.delete', $invoice->id) }}" method="post">
                @csrf
                @method('DELETE')
                <button type="submit" @if($invoice->status == 'paid') disabled @endif class="button--danger deleteButton">Delete</button>
            </form>
        @endcan
    @endslot
@endcomponent

@php
    $data = [
        'Student' => '<a href="'.route('classroom.students.show', $invoice->student->user_id).'"><b>'.$invoice->student->name.'</b></a>',
        'Due At' => $invoice->due_at->toDayDateTimeString(),
        'Paid At' => $invoice->paid_at ? $invoice->paid_at->toDayDateTimeString() : 'N/A',
        'Created At' => $invoice->created_at->toDayDateTimeString(),
        'Updated At' => $invoice->updated_at->toDayDateTimeString(),
    ];
    if( Gate::allows('mark_as_paid', $invoice) ) {
        $data = array_merge($data, [
            'Parent\'s Name' => $invoice->student->parent ? $invoice->student->parent->name : 'N/A',
            'Parent\'s Email' => $invoice->student->parent ? $invoice->student->parent->email : 'N/A',
        ]);
    }
@endphp
@component('classroom.common.details', ['data' => $data])
@endcomponent

<div class="panel p-0">
    <div class="panel-header">Summary</div>
    <div class="panel-body">

        <table class="table mb-0">
            <thead>
                <tr>
                    <th scope="col">Item</th>
                    <th scope="col">Qty</th>
                    <th scope="col">Unit Price(MYR)</th>
                    <th scope="col">Price(MYR)</th>
                </tr>
            </thead>
            <tbody>
                @if($invoice->no_of_lessons)
                    <tr>
                        <td>Lesson(per hour)</td>
                        <td>{{ $invoice->lesson_quantity }}</td>
                        <td>{{ $invoice->lesson_price_per_unit }}</td>
                        <td class="text-right">{{ $invoice->amount }}</td>
                    </tr>
                @endif
                @foreach( $invoice->non_lesson_items() as $item )
                    <tr>
                        <td>{{ $item->title }}</td>
                        <td>{{ $item->no_of_units }}</td>
                        <td>{{ $item->price_per_unit }}</td>
                        <td class="text-right">{{ $invoice->amount }}</td>
                    </tr>
                @endforeach
                <tr>
                    <td colspan="3">Total</td>
                    <td class="bg-light text-right"><b>{{ $invoice->amount }}</b></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
@endsection
