<div class="panel p-0 mb-3">
    <div class="panel-header">
        <a href="{{ route('classroom.lessons.homeworks.show', [$homework->lesson->id, $homework->id]) }}" class="underline-on-hover text-dark">
            <h3 class="position-relative font-size5 mb-1">
                {{ $homework->primary }}
            </h3>
        </a>
        <h4 class="font-size1 gray2 mb-0">
            {!! $homework->secondary !!}
        </h4>
    </div>

    @if( $homework->instruction || count($homework->resources ?? []) || count($homework->getCustomResources() ?? []) )
        <div class="panel-body">
            @if($homework->instruction)
                <p class="mb-2">{{ $homework->instruction }}</p>
            @endif

            @component('classroom.homeworks.common.file_block_container', ['homework' => $homework])
            @endcomponent
        </div>
    @else
        <div class="panel-body">
            <div class="text-center py-2 gray2">
                No data
            </div>
        </div>
    @endif

    @can('mark', $homework)
        @php
            $data = [
                ['status' => 'submitted',
                 'bg-color' => 'blue5'],
                // ['status' => 'marked',
                //  'bg-color' => 'orange'],
                ['status' => 'returned',
                 'bg-color' => 'green'],
                ['status' => 'overdue',
                 'bg-color' => 'red'],
            ];
        @endphp
        <div class="panel-footer border-top">
            @component('classroom.homeworks.common.metric', ['homework' => $homework, 'data' => $data])
            @endcomponent
        </div>
    @endcan

</div>
