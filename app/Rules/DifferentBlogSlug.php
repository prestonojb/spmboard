<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Blog;

class DifferentBlogSlug implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return !Blog::where('slug', str_slug($value))->exists();
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'This blog name has already been taken.';
    }
}
