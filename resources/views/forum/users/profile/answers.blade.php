@extends('forum.users.profile.layouts.app')

@section('meta')
    <title>{{ $user->username }} - {{ ucfirst($user->account_type) }} | {{ config('app.name') }}</title>
@endsection

@section('styles')
    {{-- Magnific Popup core CSS file --}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.css">
@endsection


@section('main')
    {{-- Metrics --}}
    @php
        $data = [
            ['key' => 'Upvotes',
            'value' => $user->getAnswerUpvoteCount(),
            'icon' => 'fas fa-arrow-up fa-2x',
            'color' => 'blue6'],
            ['key' => 'Recommended',
            'value' => $user->getAnswerRecommendationsCount(),
            'icon' => 'fas fa-star fa-2x',
            'color' => 'yellow'],
            ['key' => 'Checked',
            'value' => $user->getAnswerChecksCount(),
            'icon' => 'fas fa-check fa-2x',
            'color' => 'green'],
        ];
    @endphp
    <div class="form-row border-bottom pb-3 mb-3">
        @foreach($data as $datum)
            <div class="col-12 col-lg-4 mb-2">
                @component('forum.users.profile.common.key_value_block', ['datum' => $datum])
                @endcomponent
            </div>
        @endforeach
    </div>

    @if(count($answers ?? []))
        <div class="user-question-container user-questions">
            @foreach($answers as $answer)
                <div class="position-relative">
                    @component('forum.users.common.answer', ['answer' => $answer])
                    @endcomponent
                </div>
            @endforeach
        </div>
    @else
        <section class="text-center mt-4">
            <img src="{{ asset('img/essential/knowledge.svg') }}" alt="No Questions Found" width="220" height="220" class="mb-2">
            <h2 class="gray2 font-size4">No answers by user yet...</h2>
        </section>
    @endif

    {{ $answers->links() }}
@endsection
