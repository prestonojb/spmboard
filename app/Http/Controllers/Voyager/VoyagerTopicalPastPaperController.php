<?php

namespace App\Http\Controllers\Voyager;

use App\Imports\TopicalPastPapersImport;
use Illuminate\Http\Request;
use Excel;
use App\Subject;
use App\TopicalPastPaper as PastPaper;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Artisan;

class VoyagerTopicalPastPaperController extends \TCG\Voyager\Http\Controllers\VoyagerBaseController
{
    public function create(Request $request)
    {
        $subjects = Subject::all();
        return view('vendor.voyager.topical_past_papers.create', compact('subjects'));
    }

    /**
     * Stores only topical past papers records in DB (*doesn't store files on S3)
     */
    public function store(Request $request)
    {
        // Validation
        request()->validate([
            'file' => 'required|mimes:csv,xlsx,xls'
        ]);
        // Import rows from excel sheet to DB
        Excel::import(new TopicalPastPapersImport, request()->file('file'));

        // Sync Resources
        Artisan::call('sync:resources');

        return redirect()->back()->with(['alert-type' => 'success', 'message' => 'Updated Database successfully! Remember to upload your files on S3 too.']);
    }

    public function destroy(Request $request, $id)
    {
        $past_paper = PastPaper::find($id);
        $past_paper->delete();

        return redirect('admin/topical-past-papers')->with('error', 'Topical Past Year Paper deleted successfully!');
    }
}


