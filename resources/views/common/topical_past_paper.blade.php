<div class="position-relative note @if($paper->is_unavailable()) unavailable @endif">
    <div class="row">
        <div class="col-12 col-sm-9">
            <h3 class="note__title">{{ $paper->name }}</h3>
            <h4 class="note__description">{{ $paper->subject->name }}, {{ $paper->chapter->name }}, Paper {{ $paper->paper_number }}</h4>
            <div class="pp__tags">
                @if($paper->is_free())
                    <span class="tag free">Free</span>
                @endif
            </div>
        </div>

        <div class="col-12 col-sm-3 pp__cta">

            <a class="button--primary" href="{{ route('topical_past_papers.qp.show', [$exam_board, $paper->id]) }}" target="_blank">Question</a>

            @if(!is_null($paper->ms_url))
                @if()
                    <a class="button--primary--inverse @if($paper->is_free()) disabled @endif" href="{{ !$paper->is_free() ? route('topical_past_papers.ms.show', [$exam_board, $paper->id]) : 'javascript:void(0)' }}" target="_blank">Answer</a>
                @else
                    {{-- @if( $paper->is_owned() )
                        <a class="button--primary--inverse" href="{{ route('topical_past_papers.ms.show', [$exam_board, $paper->id]) }}" target="_blank">Answer</a>
                    @else
                        <form action="{{ route('topical_past_papers.claim', $paper->id) }}" method="POST">
                            @csrf
                            <button type="submit" class="button--primary--inverse"><img class="inline-image" src="{{ asset('img/essential/coin.svg') }}" alt="coin"> {{ $paper->coin }}</button>
                        </form>
                    @endif --}}

                @endif
            @endif
        </div>
    </div>
</div>

{{-- PDF Viewer Modal --}}
{{-- <div class="modal fade pdf-modal-container" tabindex="-1" role="dialog" aria-labelledby="pdfModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg pdf-modal">
        <div class="modal-content">
            <div id="my_pdf_viewer">
                <div id="canvas_container">
                    <canvas id="pdf_renderer"></canvas>
                    <div id="zoom_controls_container">
                        <div id="zoom_controls">
                            <button id="zoom_in">+</button>
                            <button id="zoom_out">-</button>
                        </div>
                    </div>
                </div>
                <div id="navigation_controls">
                    <button id="go_previous">Previous</button>
                    <input id="current_page" value="1" type="number"/>
                    <button id="go_next">Next</button>
                </div>
            </div>
        </div>
    </div>
</div> --}}
