<table class="table panel p-0 reportTable" data-show-minimal="{{ isset($show_minimal) && $show_minimal }}">
    <thead>
    <tr>
        <th scope="col">Student</th>
        <th scope="col">Period</th>
        <th scope="col">No. of lessons</th>
        <th scope="col">Prepared At</th>
        <th scope="col">Remark</th>
        <th scope="col">Action</th>
    </tr>
    </thead>
    <tbody>
        @foreach( $reports as $report )
            <tr>
                <td scope="row" class="text-center text-lg-left">
                    <img class="avatar" src="{{ $report->student->avatar }}" width="35" height="35">  {{ $report->student->username }}
                </td>
                <td>{{ $report->period }}</td>
                <td>{{ $report->no_of_lessons }}</td>
                <td>{{ $report->created_at->toFormattedDateString() }}</td>
                <td>{{ $report->teacher_remark ? str_limit($report->teacher_remark, 50) : 'N/A' }}</td>
                <td>
                    <div class="d-flex">
                        <a href="{{ route('classroom.reports.show', $report->id) }}" class="button--primary--inverse sm mr-1"><span class="iconify" data-icon="mdi:magnify-plus-outline" data-inline="false"></span></a>
                        @can('delete', $report)
                            <form action="{{ route('classroom.reports.delete', $report->id) }}" method="post">
                                @csrf
                                @method('DELETE')
                                <button type="button" class="button--danger--inverse sm deleteButton"><span class="iconify" data-icon="carbon:delete" data-inline="false"></span></button>
                            </form>
                        @endcan
                    </div>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>
