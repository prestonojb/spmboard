<form action="{{ url()->current() }}" method="get">
    @csrf
    <div class="filter-container">
        <div class="select-container">
            {{-- Student --}}
            <select name="student" id="" class="filter-select--lg">
                <option value="" disabled selected>Student</option>
                @foreach($students as $student)
                    <option value="{{ $student->user_id }}" @if(request()->student == $student->user_id) selected @endif>{{ $student->name }}</option>
                @endforeach
            </select>

            {{-- Classroom --}}
            <select name="prepared_days_ago" id="" class="filter-select--lg">
                <option value="" disabled selected>Prepared At</option>
                <option value="0" @if( isset(request()->prepared_days_ago) && request()->prepared_days_ago == 0 ) selected @endif>Today</option>
                <option value="1" @if( request()->prepared_days_ago == 1 ) selected @endif>Yesterday</option>
                @foreach([3, 7, 30, 90, 365] as $day)
                    <option value="{{ $day }}" @if( request()->prepared_days_ago == $day ) selected @endif>Past {{ $day }} days</option>
                @endforeach
            </select>
        </div>

        <div class="filter-buttons">
            <button type="button" class="button-link underline-on-hover mr-1 clearFilterButton">Clear</button>
            <button type="submit" class="button--primary">Apply Filter</button>
        </div>
    </div>
</form>
