<div class="tab-content">
    @foreach($subject->chapters as $chapter)
        <div class="tab-pane fade @if($loop->first) show active @endif" id="{{ str_slug($chapter->name) }}_chapter" role="tabpanel">
            @if(count($chapter->topical_past_papers ?? []))
                @foreach($chapter->topical_past_papers as $paper)
                    @component('resources.topical_past_papers.common.block', ['paper' => $paper])
                    @endcomponent
                    <div class="py-1"></div>
                @endforeach
            @else
                <div class="text-center p-5">
                    <img src="{{ asset('img/essential/empty.svg') }}" alt="" width="400" height="400" class="d-inline-block mb-2">
                    <p class="gray2">No guides available for this chapter yet</p>
                </div>
            @endif
        </div>
    @endforeach
</div>
