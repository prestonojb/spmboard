<?php

namespace App\Http\Controllers;

use App\Classroom;
use App\Student;
use App\StudentParent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class StudentParentController extends Controller
{
    public function home()
    {
        return view('parent.home');
    }

    public function show_student(Student $student)
    {
        $student_user_id = $student->user_id;
        $classroom = Classroom::whereHas('students', function($q) use ($student_user_id){
            $q->where('student_id', $student_user_id);
        })->first();

        return view('parent.students.show', compact('student', 'classroom'));
    }

    public function get_getting_started_view()
    {
        return view('getting_started.parent');
    }

    public function update()
    {
        request()->validate([
            'name' => 'required',
            'phone_number' => 'required',
        ]);

        $user = auth()->user();
        $parent = $user->parent;

        // User update
        $user->update([
            'name' => request('name'),
        ]);

        // Parent update
        $parent->update([
            'phone_number' => request('phone_number'),
        ]);

        Session::flash('success', 'Profile updated successfully!');
        return request()->getting_started ? redirect()->route('login_choose_redirect')
                                            : redirect()->back();
    }
}
