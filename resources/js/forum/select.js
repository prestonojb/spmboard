var chapter_selectHTML = $('.chapter-select').html();

/*
* Initialise subject select
*/
new SlimSelect({
    select: '.subject-select',
    placeholder: 'e.g. Biology',
    allowDeselectOption: true,
    onChange: () => {
        var subject_selected = $('.subject-select :selected').text();
        var $chapter_select = $('.chapter-select');
        $chapter_select.prop('disabled', false);
        // restore the full select list first.
        $chapter_select.html(chapter_selectHTML);

        var optGroup = $('optgroup[label="' + subject_selected + '"]').html();

        $chapter_select.html(optGroup);
        new SlimSelect({
            select: '.chapter-select',
            placeholder: 'Select Chapter',
            limit: 2,
            allowDeselectOption: true,
        });
    }
});

/*
* For failed requests
*/
var subject_selected = $('.subject-select :selected').text();
var $chapter_select = $('.chapter-select');
//Enable chapter select if old subject is selected
if(subject_selected){
    $chapter_select.prop('disabled', false);
}

//Remove unrelated chapters
var optGroup = $('optgroup[label="' + subject_selected + '"]').html();
$chapter_select.html(optGroup);

/*
 * Inititialise chapter select
 */
new SlimSelect({
    select: '.chapter-select',
    placeholder: 'e.g. Respiration',
    limit: 2,
    allowDeselectOption: true
});

var preferred_subjectsHTML = $('.preferred-subjects-select').html();

/*
 * Initialise subject select
 */
new SlimSelect({
    select: '.exam-board-select',
    placeholder: 'Select Exam Board',
    allowDeselectOption: true,
    onChange: () => {
        var exam_board_selected = $('.exam-board-select :selected').text();
        var $preferred_sujects_select = $('.preferred-subjects-select');
        $preferred_sujects_select.prop('disabled', false);
        // restore the full select list first.
        $preferred_sujects_select.html(preferred_subjectsHTML);

        var optGroup = $('optgroup[label="' + exam_board_selected + '"]').html();

        $preferred_sujects_select.html(optGroup);
        new SlimSelect({
            select: '.preferred-subjects-select',
            placeholder: 'Select Preferred Subjects',
            limit: 3,
            allowDeselectOption: true
        });
    }
});

/*
* For failed requests
*/

var subject_selected = $('.exam-board-select :selected').text();
var $preferred_sujects_select = $('.preferred-subjects-select');
//Enable chapter select if old subject is selected
if(subject_selected){
    $preferred_sujects_select.prop('disabled', false);
}

//Remove unrelated chapters
var optGroup = $('optgroup[label="' + subject_selected + '"]').html();
$preferred_sujects_select.html(optGroup);

/*
* Inititialise chapter select
*/
new SlimSelect({
    select: '.preferred-subjects-select',
    placeholder: 'e.g. Respiration',
    limit: 3,
    allowDeselectOption: true
});
