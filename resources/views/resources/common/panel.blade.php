<div class="card h-100">
    <img class="maxw-100 h-225px" src="{{ $resource->preview_img }}">
    <div class="card-body d-flex justify-content-between align-items-start flex-column p-2">
        <div>
            {{-- Name/Title --}}
            <h2 class="card-title font-weight500 font-size4 underline-on-hover mb-2">
                <a class="font-weight-inherit font-size-inherit color-inherit" href="{{ $resource->show_url }}">
                    {{ $resource->primary }}
                </a>
                @cannot('view_unlocked', $resource)
                    <span class="iconify red" data-icon="ant-design:lock-filled" data-inline="false"></span>
                @endcannot
            </h2>

            {{-- Tags --}}
            @if(!is_null($resource->getAllTagsHtml()))
                <div class="mb-0">
                    {!! $resource->getAllTagsHtml() !!}
                </div>
            @endif
        </div>

        {{-- Coin Price --}}
        <div>
            <img class="" width="20" height="20" src="{{ asset('img/essential/coin.svg') }}" alt="">
            <span class="font-size2">{{ $resource->coin_price }} coins</span>
        </div>

        <div class="align-self-end">
            <div class="Stars xs" style="--rating: {{ $resource->rating }};"></div><span class="gray2 font-size1">({{ $resource->getTotalReviews() }})</span> ·
            <span class="font-size1 gray2">{{ $resource->getNoOfUnlocks() }} @if($resource->getNoOfUnlocks() > 1) unlocks @else unlock @endif</span>
        </div>
    </div>
</div>
