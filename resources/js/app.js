/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

// window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

// Vue.component('example-component', require('./components/ExampleComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// const app = new Vue({
//     el: '#app',
// });

// require('./bootstrap');

$(function(){
    // Get URL query string value by key
    window.getParameterByName = function getParameterByName(name, url) {
            if (!url) url = window.location.href;
            name = name.replace(/[\[\]]/g, '\\$&');
            var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
                results = regex.exec(url);
            if (!results) return null;
            if (!results[2]) return '';
            return decodeURIComponent(results[2].replace(/\+/g, ' '));
        }

    // Show loader in element to indicate that it is loading
    window.showLoader =  function ( elem, type, loaderColor ) {
        var loader = '<div class="loading-icon loading ' + type + ' ' + loaderColor + '"></div>';
        elem.html( loader );
        elem.prop('disabled', true);
    }

    // Replace loader with new HTML
    window.hideLoader = function ( elem, newHtml ) {
        elem.html( newHtml );
        elem.prop('disabled', false);
    }

    //Loader
    $('html').css('visibility', '1');
    $('.loader').hide();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    // Bootstrap Modal
    $(document).on('show.bs.modal', '.modal', function () {
        var zIndex = 1040 + (10 * $('.modal:visible').length);
        $(this).css('z-index', zIndex);
        setTimeout(function() {
            $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
        }, 0);
    });

    toastr.options = {
        'timeOut' : '1500',
        "showMethod": "fadeIn",
        "showMethod": "fadeIn",
        'preventDuplicates': true,
        'positionClass': 'toast-top-right'
    };

    // Prevent closing from click inside dropdown
    $(document).on('click', '.dropdown-menu', function (e) {
        e.stopPropagation();
    });

    $('.disableOnSubmitButton').click(function(e){
        e.preventDefault();
        $(this).attr('disabled', true);
        $(this).closest('form').submit();
    });

    $('.loadOnSubmitButton').click(function(e){
        var $t = $(this);
        var initHtml = $(this).html();
        window.showLoader( $t, 'in-button', 'white' );

        // Validate Form
        var $form = $t.closest('form');
        // Validate inputs with required property
        $form.find('input').each(function(){
            // Adds error message if input is required but empty length
            if( $(this).prop('required') ) {
                if( !$.trim( $(this).val() ).length ) {
                    // Prevent adding more than one error message after input
                    if( $(this).next('.error-message').length == 0) {
                        $(this).after( '<span class="error-message">This field is required.</span>' );
                    }
                } else {
                    $(this).next('.error-message').remove();
                }
            }
        });
        // Disable modal if exists
        var $modal = $t.closest('.modal');
        if($modal.length > 0) {
            // Reference: https://stackoverflow.com/a/38502417
            $modal.data('bs.modal')._config.backdrop = 'static';
            $modal.data('bs.modal')._config.keyboard = false;
        }

        if( $form.find('.error-message').length > 0 ) {
            // Form is invalid
            window.hideLoader($t, initHtml);
            // Re-enable modal
            if($modal.length > 0) {
                $modal.data('bs.modal')._config.backdrop = true;
                $modal.data('bs.modal')._config.keyboard = true;
            }
        } else {
            // Form is valid
            $t.closest('form').submit();
        }
    });

    $('[data-toggle="popover"]').popover({
        trigger: 'focus',
    });

    $('[data-toggle="tooltip"]').tooltip()

    $('.hamburger-button').click(function(e){
        var $overlay = $('.mobile-body-overlay');
        $overlay.show();
        e.stopPropagation();
    });

    $('.mobile-body-overlay').click(function(){
        $('.mobile-body-overlay').hide();
    });

    $('.mobile-nav-container').click(function(e){
        e.stopPropagation();
    });

    $('.mobile-nav-item, .mobile-nav-dropdown-menu-item').click(function(e){
        $(this).find('.mobile-nav-dropdown-arrow').click();
    });

    $('.mobile-nav-dropdown-arrow').click(function(e){
        e.stopPropagation();
        //Get child chapter menu
        var $dropdownMenu = $(this).parent().next('.mobile-nav-dropdown-menu, .mobile-nav-dropdown-menu__level-2');
        if($dropdownMenu.css('display') == 'block'){
            //Hide if menu is on but is clicked again
            $dropdownMenu.hide();
        } else{
            //Reset ALL subject menu//dropdown arrow
            $dropdownMenu.show();
        }
        //Toggle up and down arrow of clicked list item
        $(this).find('i').toggleClass('fa-chevron-right fa-chevron-down');
    });

    $('.search-icon').click(function(){
        $('.mobile-search-form').toggle();
        $('.mobile-search-input-header').focus();
    });

    $('.notification-badge').click(function(){
        $('.bell-icon').click();
    });

    $('.clearFilterButton').click(function(e){
        e.preventDefault();
        var form = $(this).closest('form');
        form.find('select option:disabled').prop('selected', true);
    });

    $('.confirmButton').click(function(e){
        e.preventDefault();
        e.stopPropagation();
        var title = $(this).data('title');
        var text = $(this).data('text');
        var type = $(this).data('type') || 'warning';
        var form = $(this).closest('form');
        Swal.fire({
            title: title,
            text: text,
            type: type,
            showCancelButton: true,
            reverseButtons: true,
            confirmButtonText: 'Confirm',
        }).then(function(result){
            if(result.value){
                form.submit();
            }
        });
    });

    $('.deleteButton').click(function(e){
        e.preventDefault();
        e.stopPropagation();
        var form = $(this).closest('form');
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            reverseButtons: true,
            confirmButtonText: 'Confirm',
        }).then(function(result){
            if(result.value){
                form.submit();
            }
        });
    });

    $('.unlockResourceButton').click(function(e){
        e.preventDefault();
        e.stopPropagation();
        var resource_name = $(this).data('resource-name');
        var resource_coin_price = $(this).data('resource-coin-price');
        var form = $(this).closest('form');
        Swal.fire({
            title: 'Are you sure?',
            text: "You are unlocking " + resource_name + " with " + resource_coin_price + "!",
            type: 'warning',
            showCancelButton: true,
            reverseButtons: true,
            confirmButtonText: 'Confirm',
        }).then(function(result){
            if(result.value){
                form.submit();
            }
        });
    });

    // Switch Exam Board Select
    $('.switch-exam-board-select').change(function(e){
        e.preventDefault();
        // Redirect to url
        var url = $(this).find(':selected').data('url');
        window.location.href = url;
    });

    $(".shareIcons").jsSocials({
        showLabel: false,
        showCount: false,
        shareIn: "popup",
        shares: ["facebook", "twitter", "whatsapp"]
    });

    $('.copyButton').click(function(){
        var inputId = $(this).data('input-id');
        var $input = $('#'+inputId); // Get input by ID

        $input.select();
        document.execCommand("copy");

        toastr.success('Copied to clipboard!');
    });

    $('.notification-dropdown-menu').click(function(e){
        e.preventDefault();
        e.stopPropagation();
    });
});

require('./global/header2');
require('./global/file_input');
require('./global/smart_bar');
