<?php

namespace App\Policies;

use App\Notes;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class NotePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Permission to browse note
     */
    public function browse(User $user)
    {
        return $user->isAdmin();
    }

    /**
     * Permission to edit note
     */
    public function edit(User $user, Notes $note)
    {
        return $user->id == $note->user_id;
    }

    /**
     * Permission to show note
     */
    public function show(User $user, Notes $note)
    {
        return $user->hasPremiumResourceAccess();
    }

    /**
     * Permission to download PDF
     */
    public function download_pdf(User $user, Notes $note)
    {
        // Free
        if($note->isFree()) {
            return $user->hasPremiumResourceAccess();
        // Paid
        } else {
            return $user->hasResource($note->resource);
        }
    }
}
