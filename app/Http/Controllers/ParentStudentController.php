<?php

namespace App\Http\Controllers;

use App\Classroom;
use App\Question;
use App\StudentReport;
use App\Student;
use Illuminate\Http\Request;

class ParentStudentController extends Controller
{
    public function show(Student $student, Classroom $classroom = null) {

        if( is_null($classroom) ){
            $classroom = $student->classrooms()->first();
        }

        $classrooms = $student->classrooms;

        $reports = StudentReport::where('student_id', $student->user_id)->where('classroom_id', $classroom->id)->whereNotNull('shared_to_parent_at')->orderByDesc('created_at')->paginate(30);

        return view('parent.students.show', compact('classrooms', 'classroom', 'student', 'reports'));
    }
}
