<?php

namespace App\Http\Controllers;

use App\Question;
use App\ExamBoard;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function index(ExamBoard $exam_board)
    {
        $sort = request()->sort;
        if(isset($sort)) {
            $sort_type = array('hot', 'top', 'new', 'answered');
            if(!in_array($sort, $sort_type)) {
                return redirect('search?q='. request('q'));
            }
        }

        $query = request('q');

        $questions = Question::search($query);

        $questions = $questions->whereHas('subject.exam_board', function($q) use ($exam_board) {
            return $q->where('name', $exam_board->name);
        });

        switch(request()->sort){
            //Top
            case 'new':

                $questions = $questions->orderBy('created_at')->paginate(30);

                break;

            //Unanswered
            case 'answered':

                //Get answered questions
                $answered_questions = $questions->whereHas('answers', function($query){
                    $query->where('checked', '=', '1');
                });

                $questions = $answered_questions->paginate(30);

                break;

            //Hot
            default:
                $questions = $questions->paginate(30);
                break;
        }

        $sort = request()->sort;

        return view('forum.search.index', compact('exam_board', 'query', 'questions', 'sort'));
    }


}
