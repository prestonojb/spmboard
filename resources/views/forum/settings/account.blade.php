@extends('forum.layouts.app')

@section('meta')
<title>Account Settings | {{ config('app.name') }}</title>
@endsection

@section('content')
@include('main.navs.header')

<div class="py-2"></div>

<div class="container">
    <div class="row">
        <div class="col-12 col-lg-9">
            @include('forum.settings.common.pills')
            <div class="panel">
                <div class="mt-2 mb-4">
                    <h3 class="">Credentials</h3>
                    <p class="gray2">Sets how you are identified on Board.</p>
                </div>
                <form method="POST" action="{{ route('users.account.settings', auth()->user()) }}">
                    @csrf
                    @method('PATCH')
                    <div class="form-field">
                        <label>Username</label>
                        <input type="text" name="username" value="{{ auth()->user()->username }}">
                        @error('username')
                            <div class="error-message">{{ $message }}</div>
                        @enderror
                    </div>


                    <div class="form-field">
                        <label>Email</label>
                        <input type="email" name="email" value="{{ auth()->user()->email }}" placeholder="Email" readonly>
                        @error('email')
                            <div class="error-message">{{ $message }}</div>
                        @enderror

                        @if(auth()->user()->hasVerifiedEmail())
                            <span class="verified-text">Verified</span>
                        @else
                            <span class="d-inline-block">
                                <span class="">Email is not verified.</span><a href="{{ route('verification.notice') }}"> Click here to verify</a>.
                            </span>
                        @endif
                    </div>

                    <button type="submit" class="button--primary mb-1">Save Changes</button>
                </form>

                <hr>

                <div class="mt-2 mb-4">
                    <h3>Switch Account Type</h3>
                    <p class="gray2">Switching your account type means you will <b>lose ALL progress (including coins and Reward Points)</b> of the existing account (Note: this action is <b>IRREVERSIBLE</b>).</p>
                        <p class="gray2 font-size2">
                            <i>*If you wish to have two account types,
                            please consider registering with seperate emails.</i>
                        </p>
                </div>

                <form action="{{ route('users.account.switch_account_type', auth()->user()->username) }}" method="POST">
                    @csrf
                    <p class="gray2 mb-0"><small>Existing: {{ ucfirst( auth()->user()->account_type ) }} account</small></p>
                    <label for="" class="">New Account Type</label>
                    <div class="form-field">
                        <div class="form-row">
                            @foreach(['teacher', 'student', 'parent'] as $account_type)
                                <div class="col-4">
                                    <div class="styled-radio-input">
                                        <input type="radio" name="account_type" value="{{ $account_type }}" id="account_type_{{ $account_type }}" class="@if( $account_type == auth()->user()->account_type ) default @endif" @if( $account_type == auth()->user()->account_type ) checked @endif>
                                        <label for="account_type_{{ $account_type }}">{{ ucfirst($account_type) }}</label>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>

                    <button type="button" class="button--danger switchAccountTypeButton" disabled>Switch Account Type</button>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="py-2"></div>

@endsection

@section('scripts')
<script>
$(function(){
    var $switchAccountTypeButton = $('.switchAccountTypeButton');
    $('[name=account_type]').change(function(e){
        if( $('[name=account_type]:checked').hasClass('default') ) {
            $switchAccountTypeButton.prop('disabled', true);
        } else {
            $switchAccountTypeButton.prop('disabled', false);
        }
    });
    $switchAccountTypeButton.click(function(e){
        e.preventDefault();
        e.stopPropagation();
        var form = $(this).closest('form');
        Swal.fire({
            title: 'Are you sure?',
            html: "Changing your account type means that you will be losing <b>ALL PROGRESS</b> of your current account (including Reward Points/Coins)! Are you sure you want to continue?",
            type: 'warning',
            showCancelButton: true,
            reverseButtons: true,
            confirmButtonColor: '#e63946',
            confirmButtonText: 'Yes, this is what I want',
        }).then(function(result){
            if(result.value){
                form.submit();
            }
        });
    });
});
</script>
@endsection
