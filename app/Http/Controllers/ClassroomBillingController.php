<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ClassroomBillingController extends Controller
{
    public function history()
    {
        $user = auth()->user();
        if( $teacher = $user->teacher ) {
            $invoices = $user->invoices();
        } else {
            return redirect()->back()->with('error', 'Your account is not authorized to view this page!');
        }

        return view('classroom.billing.history', compact('invoices'));
    }
}
