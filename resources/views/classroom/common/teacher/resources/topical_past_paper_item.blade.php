<div class="resource-list-item"  data-toggle="collapse" href="#topical_past_paper_{{ $past_paper->id }}" role="button">
    <div class="d-flex justify-content-start align-items-center">
        <div class="title">{{ $past_paper->name }}</div>
    </div>
    <span class="">
        <i class="fas fa-angle-down"></i>
    </span>
</div>

<div class="collapse ml-2" id="topical_past_paper_{{ $past_paper->id }}">
    @foreach(['question', 'answer'] as $type)
        <div class="resource-list-item" data-type="{{ $type }}" data-resource-type="Topical Paper ({{ ucfirst($type) }})" data-resource-id="{{ $past_paper->resource->id }}" data-resource-name="{{ $past_paper->name }}">
            @if( $type == 'question' )
                <div class="d-flex justify-content-start align-items-center">
                    <input type="checkbox" class="checkbox-input mr-2" @cannot('question_show', $past_paper) disabled @endcannot>
                    <div class="title">{{ ucfirst($type) }}</div>
                </div>
                <a href="@cannot('question_show', $past_paper) javascript:void:(0) @else {{ $past_paper->qp_route }} @endcannot" class="button--primary sm @cannot('question_show', $past_paper) disabled @endcannot" @can('question_show') target="_blank" @endcan>View</a>
            @elseif ( $type == 'answer' )
                <div class="d-flex justify-content-start align-items-center">
                    <input type="checkbox" class="checkbox-input mr-2" @cannot('answer_show', $past_paper) disabled @endcannot>
                    <div class="title">{{ ucfirst($type) }}</div>
                </div>
                <a href="@cannot('answer_show', $past_paper) javascript:void:(0) @else {{ $past_paper->ms_route }} @endcannot" class="button--primary sm @cannot('answer_show', $past_paper) disabled @endcannot" @can('answer_show', $past_paper) target="_blank" @endcan>View</a>
            @endif
        </div>
    @endforeach
</div>
