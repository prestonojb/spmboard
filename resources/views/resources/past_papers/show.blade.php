@extends('resources.layouts.app')
@section('meta')
    <title>{{ $past_paper->name }} - Past Papers | {{ $exam_board->name }} {{ config('app.name') }} </title>
@endsection

@section('header')
    @component('resources.common.header', ['type' => 'past_paper', 'exam_board' => $exam_board])
    @endcomponent
@endsection

@section('breadcrumbs')
    @component('resources.common.breadcrumbs')
    <a href="{{ route('past_papers.home', $exam_board->slug) }}">Home</a> /
    <a href="{{ route('past_papers.subject_show', [$exam_board->slug, $past_paper->subject_id]) }}">{{ $past_paper->subject->name }}</a> /
    {{ str_limit($past_paper->name, 30) }}
    @endcomponent
@endsection

@section('context_panel')
    @component('resources.common.context_panel', ['exam_board' => $past_paper->subject->exam_board->name, 'title' => $past_paper->name, 'description' => $past_paper->description])
        <h3 class="font-size3">Part of <a href="{{ $past_paper->subject_route }}" class="font-size-inherit blue-underline">{{ $past_paper->subject->name }}</a></h3>

        {{-- Toolbar --}}
        <div class="py-1 my-1 border-left-gray5-3px pl-2">
            @auth
                {{-- View as PDF --}}
                <a href="{{ $is_question ? $past_paper->qp_url : $past_paper->ms_url }}" target="_blank" class="gray-button sm mr-2">
                    <span class="iconify" data-icon="carbon:pdf-reference" data-inline="false"></span>View</a>
                {{-- Download As PDF --}}
                <a href="{{ $is_question ? $past_paper->qp_download_route : $past_paper->ms_download_route }}" class="gray-button sm mr-2">
                    <span class="iconify" data-icon="carbon:generate-pdf" data-inline="false"></span>Download</a>
                {{-- Save --}}
                <button type="button" class="gray-button sm" data-toggle="modal" data-target="#saveToLibraryModal">
                    <span class="iconify" data-icon="cil:playlist-add" data-inline="false"></span>Save</button>
                @component('resources.common.save_to_library', ['resource' => $past_paper->resource])
                @endcomponent
            @else
                <a href="{{ route('login') }}" class="gray-button sm mr-2">
                    <span class="iconify" data-icon="carbon:pdf-reference" data-inline="false"></span>View</button></a>
                <a href="{{ route('login') }}" class="gray-button sm mr-2">
                    <span class="iconify" data-icon="carbon:generate-pdf" data-inline="false"></span>Download</button></a>
                <a href="{{ route('login') }}" class="gray-button sm">
                    <span class="iconify" data-icon="cil:playlist-add" data-inline="false"></span>Save</button></a>
            @endauth
        </div>
    @endcomponent
@endsection

@section('side')
    @component('resources.past_papers.common.paper_menu', ['current_paper' => $past_paper])
    @endcomponent
@endsection

@section('body')
    <div class="d-flex flex-wrap justify-content-between align-items-center mb-2">
        <span class="tag--primary lg text-uppercase mb-1">{{ $is_question ? 'Question Paper' : 'Mark Scheme' }}</span>

        @if($is_question)
            <a href="{{ $past_paper->ms_route }}" class="button--primary lg mb-1">Switch to Mark Scheme
                <span class="iconify" data-icon="codicon:arrow-right" data-inline="false"></span></a>
        @else
            <a href="{{ $past_paper->qp_route }}" class="button--primary--inverse lg mb-1">Switch to Question Paper
                <span class="iconify" data-icon="codicon:arrow-right" data-inline="false"></span></a>
        @endif
    </div>
@endsection

@section('main')
    {{-- Edit --}}
    @can('edit', $past_paper)
        <a href="{{ route('voyager.past-papers.edit', $past_paper->id) }}" class="d-inline-block underline-on-hover mr-1 mb-2"><span class="iconify" data-icon="bx:bx-edit" data-inline="false"></span> Edit</a>
    @endcan

    {{-- Delete --}}
    @can('delete', $past_paper)
        <form action="{{ route('past_papers.delete', [$exam_board->slug, $past_paper->id]) }}" method="POST" class="d-inline">
            @csrf
            @method('delete')
            <button class="red button-link underline-on-hover deleteButton">
                <span class="iconify" data-icon="carbon:delete" data-inline="false"></span>
                Delete
            </button>
        </form>
    @endcan

    @php
        $tag = 'resource';
        $type = $is_question ? 'Question' : 'Answer';
        $description = '[Past Paper ID: '.$past_paper->id.'] '.$past_paper->name.' ('.$type.')'.' is not working as expected.';
        $email = auth()->check() ? auth()->user()->email : '';
    @endphp
    @component('resources.common.share_and_report_toolbar', ['tag' => $tag, 'description' => $description, 'email' => $email])
    @endcomponent

    <div class="gray2 font-size2 mb-2"><b>{{ views($past_paper->resource)->count() }} views</b> · Updated {{ $past_paper->updated_at->diffForHumans() }}</div>

    <div class="w-100 custom_pdf_viewer" data-url="{{ $is_question ? $past_paper->qp_url : $past_paper->ms_url }}"></div>

    @component('resources.common.share_and_report_toolbar', ['tag' => $tag, 'description' => $description, 'email' => $email])
    @endcomponent

    <div class="py-2"></div>
@endsection
