<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RewardPointAction extends Model
{
    protected $guarded = [];

    public function reward_point_actionable()
    {
        return $this->morphTo();
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function link()
    {
        $exam_board = $this->reward_point_actionable->subject->exam_board;

        switch($this->coin_actionable_type) {
            case 'App\Notes':
                return route('notes.home', $exam_board->slug);
            case 'App\PastPaper':
                return route('past_papers.home', $exam_board->slug);
            case 'App\TopicalPastPaper':
                return route('topical_past_papers.home', $exam_board->slug);
            default:
                return 'javascript:void(0)';
        }
    }
}
