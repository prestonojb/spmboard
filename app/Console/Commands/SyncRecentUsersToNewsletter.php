<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Newsletter;
use App\User;

class SyncRecentUsersToNewsletter extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:newsletter';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync recently signed up users to Mailchimp audience';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $recent_day_range = 2;
        $recentDaysAgo = Carbon::now()->subDays( $recent_day_range )->format('Y-m-d');

        // Get users created for the past {recent_day_range} days
        $users = User::all()->where('created_at', '>=', $recentDaysAgo);

        $bar = $this->output->createProgressBar(count($users));
        $bar->start();

        foreach ($users as $user) {
            // Get relevant tag names
            $account_type = $user->getUserAccountType();

            // Update user's email to Mailchimp's contact list and append group tag to the contact
            Newsletter::subscribeOrUpdate( $user->email, [], 'users' );
            Newsletter::addTags( [$account_type], $user->email, 'users' );

            $bar->advance();
        }

        $bar->finish();

        $this->info('Mailchimp contacts synced successfully with recent users ('.$recent_day_range.' days)!');
    }
}
