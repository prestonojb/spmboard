<div class="panel p-0 mb-3">
    <div class="panel-header">
        Resource Posts
    </div>
    <div class="panel-body">
        <div class="row">

            <div class="col-12">
                <div class="py-2 text-center">
                    <p class="gray2 mb-1">Average understanding rating</p>
                    {{-- Reference: https://codepen.io/FredGenkin/pen/eaXYGV --}}
                    <div class="Stars mb-1" style="--rating: {{ $student->getAverageResourceRating( $class_selected ) }};"></div>

                    <p class="gray2 text-center mb-0">
                        <b>{{ $student->getAverageResourceRating( $class_selected ) }} stars</b>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
