<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTeacherInvoiceSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teacher_invoice_settings', function (Blueprint $table) {
            $table->unsignedInteger('teacher_id')->unique();
            $table->string('logo_url')->nullable();
            $table->unsignedInteger('default_payment_due_in_days')->default(14);
            $table->string('prefix')->default('INV-');
            $table->unsignedInteger('next_invoice_sequence')->default(1);

            $table->string('from_address_1')->nullable();
            $table->string('from_address_2')->nullable();
            $table->string('postal_code')->nullable();
            $table->string('city')->nullable();
            $table->unsignedInteger('state_id')->nullable();

            $table->char('currency')->default('MYR');
            $table->decimal('default_lesson_rate_per_hour', 8, 2)->default(50);

            $table->string('memo')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teacher_invoice_settings');
    }
}
