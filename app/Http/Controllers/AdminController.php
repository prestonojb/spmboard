<?php

namespace App\Http\Controllers;

use App\Answer;
use App\CoinAction;
use App\ExamBoard;
use App\Question;
use App\BoardPassSubscription;
use App\Product;
use App\Resource;
use App\ResourceUser;
use App\Student;
use App\StudentParent;
use App\Teacher;
use App\User;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use CyrildeWit\EloquentViewable\Support\Period;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AdminController extends Controller
{
    public function dashboard()
    {
        $week_start_date = request('week_start_date') ?? Carbon::today()->startOfWeek()->format('d-m-Y');
        $week_start_date = Carbon::createFromFormat('d-m-Y', $week_start_date)->startOfWeek();
        $week_end_date = $week_start_date->copy()->endOfWeek();

        // Get period (week)
        $period = CarbonPeriod::create($week_start_date->format('Y-m-d'), '1 day', $week_end_date->format('Y-m-d'));

        // General Data
        $general_data = $this->getGeneralData($week_start_date, $week_end_date, $period);
        $sales_data = $this->getSalesData($week_start_date, $week_end_date, $period);
        $marketplace_data = $this->getMarketplaceData($week_start_date, $week_end_date, $period);
        $board_pass_data = $this->getBoardPassData($week_start_date, $week_end_date, $period);
        $forum_data = $this->getForumData($week_start_date, $week_end_date, $period);

        return view('vendor.voyager.dashboard', compact('week_start_date', 'general_data', 'sales_data', 'marketplace_data', 'board_pass_data', 'forum_data'));
    }

    /**
     * Get General Data
     * @param Carbon $week_start_date
     * @param Carbon $week_end_date
     * @param CarbonPeriod $period
     * @return array
     */
    public function getGeneralData($week_start_date, $week_end_date, $period)
    {
        // Last Week
        $last_week_start_date = $week_start_date->copy()->subWeek();
        $last_week_end_date = $week_end_date->copy()->subWeek();

        $last_period = CarbonPeriod::create($last_week_start_date->format('Y-m-d'), '1 day', $last_week_end_date->format('Y-m-d'));

        // General
        $new_signups_data = array();
        foreach($period as $date) {
            $new_signups_data[$date->format('M d')] = User::whereDate('created_at', $date->format('Y-m-d'))->count();
        }

        // Total Users
        $general_data['total_users'] = User::count();
        $general_data['total_teachers'] = Teacher::count();
        $general_data['total_students'] = Student::count();
        $general_data['total_parents'] = StudentParent::count();

        // Total signups
        $total_signups = User::whereDate('created_at', '>=', $week_start_date->format('Y-m-d'))->whereDate('created_at', '<=', $week_end_date->format('Y-m-d'))->count();
        $last_total_signups = User::whereDate('created_at', '>=', $last_week_start_date->format('Y-m-d'))->whereDate('created_at', '<=', $last_week_end_date->format('Y-m-d'))->count();

        // New signups
        $general_data['new_signups_data'] = $new_signups_data;
        $general_data['total_signups'] = $total_signups;
        $general_data['total_signups_percentage_change'] = $last_total_signups > 0 ? number_format(($total_signups - $last_total_signups / $last_total_signups), 2)
                                                                                   : 100;
        return $general_data;
    }

    /**
     * Get Sales Data
     * @param Carbon $week_start_date
     * @param Carbon $week_end_date
     * @param CarbonPeriod $period
     * @return array
     */
    public function getSalesData($week_start_date, $week_end_date, $period)
    {
        // Coins transacted
        $sales_data['coins_purchased'] = CoinAction::whereDate('created_at', '>=', $week_start_date->format('Y-m-d'))->whereDate('created_at', '<=', $week_end_date->format('Y-m-d'))
                                                    ->where('coin_actionable_type', Product::class)
                                                    ->sum('amount');

        $sales_data['coin_purchases_in_period'] = CoinAction::whereDate('created_at', '>=', $week_start_date->format('Y-m-d'))->whereDate('created_at', '<=', $week_end_date->format('Y-m-d'))
                                                            ->where('coin_actionable_type', Product::class)->latest()->get();

        return $sales_data;
    }

    /**
     * Get Marketplace Data
     * @param Carbon $week_start_date
     * @param Carbon $week_end_date
     * @param CarbonPeriod $period
     * @return array
     */
    public function getMarketplaceData($week_start_date, $week_end_date, $period)
    {
        // Coins transacted
        $marketplace_data['coins_transacted'] = CoinAction::whereDate('created_at', '>=', $week_start_date->format('Y-m-d'))->whereDate('created_at', '<=', $week_end_date->format('Y-m-d'))
                                                            ->whereIn('coin_actionable_type', Resource::getSellableClasses())
                                                            ->where('amount', '<', 0)->sum('amount');

        $marketplace_data['coins_transacted'] = -($marketplace_data['coins_transacted']);

        // Total unlocks
        $marketplace_data['total_unlocks'] = CoinAction::whereDate('created_at', '>=', $week_start_date->format('Y-m-d'))->whereDate('created_at', '<=', $week_end_date->format('Y-m-d'))
                                                        ->whereIn('coin_actionable_type', Resource::getSellableClasses())
                                                        ->where('amount', '<', 0)->count();

        // Average coins/unlock
        $marketplace_data['average_coins_per_unlock'] = $marketplace_data['total_unlocks'] > 0 ? number_format(($marketplace_data['coins_transacted']/$marketplace_data['total_unlocks']), 1)
                                                                                               : 0;

        // Unlocks in period
        $marketplace_data['unlocks_in_period'] = ResourceUser::whereDate('created_at', '>=', $week_start_date->format('Y-m-d'))->whereDate('created_at', '<=', $week_end_date->format('Y-m-d'))
                                                            ->latest()->get();
        $marketplace_data['top_unlocked_resources'] = ResourceUser::select(['resource_id', 'user_id', 'created_at'])->selectRaw("COUNT(resource_id) AS unlock_count")->groupBy('resource_id')->orderByDesc('unlock_count')->limit(5)->get();
        return $marketplace_data;
    }

    /**
     * Get Board Pass Data
     * @param Carbon $week_start_date
     * @param Carbon $week_end_date
     * @param CarbonPeriod $period
     * @return array
     */
    public function getBoardPassData($week_start_date, $week_end_date, $period)
    {
        // Active
        $board_passes = BoardPassSubscription::all();
        $no_of_active_passes = 0;
        foreach($board_passes as $pass) {
            if($pass->isActive($week_start_date)) {
                $no_of_active_passes += 1;
            }
        }

        // Sales
        $coin_actions = CoinAction::whereDate('created_at', '>=', $week_start_date->format('Y-m-d'))->whereDate('created_at', '<=', $week_end_date->format('Y-m-d'))->where('coin_actionable_type', BoardPassSubscription::class)->where('amount', '<', 0)->get();
        $board_pass_sales = 0;
        foreach($coin_actions as $action) {
            $board_pass_sales -= $action->amount;
        }

        $board_pass_data['no_of_active_passes'] = $no_of_active_passes;
        $board_pass_data['board_pass_sales'] = $board_pass_sales;
        return $board_pass_data;
    }

    /**
     * Get Board Pass Data
     * @param Carbon $week_start_date
     * @param Carbon $week_end_date
     * @param CarbonPeriod $period
     * @return array
     */
    public function getForumData($week_start_date, $week_end_date, $period)
    {
        // Questions asked
        $forum_data['total_questions_asked'] = Question::whereDate('created_at', '>=', $week_start_date->format('Y-m-d'))->whereDate('created_at', '<=', $week_end_date->format('Y-m-d'))->count();
        // Answers given
        $forum_data['total_answers_given'] = Answer::whereDate('created_at', '>=', $week_start_date->format('Y-m-d'))->whereDate('created_at', '<=', $week_end_date->format('Y-m-d'))->count();

        return $forum_data;
    }

    /**
     * Returns the no of question answered by date
     * Question is considered answered on the date if the first answer is answered on the date
     * @param Carbon $date
     * @return int
     */
    public function getQuestionsAnsweredCount($date)
    {
        $question_answered_count = 0;
        $questions = Question::whereHas('answers')->get();
        foreach($questions as $question) {
            //  Returns true if first answer to question is answered today
            $is_answered_today = $question->answers()->orderBy('created_at')->first()->whereDate('created_at', $date->format('Y-m-d'))->count();
            if( $is_answered_today ) {
                $question_answered_count += 1;
            }
        }
        return $question_answered_count;
    }

    /**
     * Returns the no of question views by date
     * @param Carbon $date
     * @return int
     */
    public function getQuestionViewsCount($date)
    {
        $period = Period::create($date->startOfDay(), $date->endOfDay());
        return views(new Question())->period($period)->count();
    }

    /**
     * Returns the no of questions asked by date
     * @param Carbon $date
     * @return int
     */
    public function getQuestionsAskedCount($date)
    {
        return Question::whereDate('created_at', $date->format('Y-m-d'))->count();
    }

    /**
     * Returns the no of answers given by date
     * @param Carbon $date
     * @return int
     */
    public function getAnswersGivenCount($date)
    {
        return Answer::whereDate('created_at', $date->format('Y-m-d'))->count();
    }
}
