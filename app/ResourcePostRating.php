<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ResourcePostRating extends Model
{
    protected $guarded = [];
    /**
     * ------------------
     * Relationships
     * ------------------
     */
    public function resource_post()
    {
        return $this->belongsTo(ResourcePost::class);
    }

    public function student()
    {
        return $this->belongsTo( User::class, 'student_id' );
    }
}
