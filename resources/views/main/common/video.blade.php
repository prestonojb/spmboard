<div class="container py-4">
    <div class="text-center" id="video">
        <figcaption class="d-block font-size5 mb-2"><b>All about Board</b></figcaption>
        <div class="about__video-iframe-container">
            <iframe src="{{ $embed_url }}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
    </div>
</div>
