@extends('classroom.layouts.app')

@section('meta')
    <title>Edit Homework - {{ $homework->title }} | {{ config('app.name') }} Classroom</title>
@endsection

@section('content')
    @component('classroom.common.breadcrumbs')
        <a href="{{ route('classroom.classrooms.show', $classroom->id) }}">Dashboard</a> /
        <a href="{{ route('classroom.lessons.index', $classroom->id) }}">Lessons</a> /
        <a href="{{ route('classroom.lessons.show', [$lesson->classroom->id, $lesson->id]) }}">{{ $lesson->name }}</a> /
        <a href="{{ route('classroom.lessons.homeworks.show', [$lesson->id, $homework->id]) }}">{{ str_limit($homework->title, 50) }}</a> /
        Edit
    @endcomponent

    @component('classroom.common.page_heading', ['title' => 'Edit Homework'])
    @endcomponent

    <div class="maxw-740px mx-auto my-5">
        <div class="panel mb-2">
            <form action="{{ route('classroom.lessons.homeworks.update', [$homework->lesson->id, $homework->id]) }}" method="POST" id="postResourcesForm">
                @csrf
                @method('PATCH')
                <input type="hidden" name="resource_data">
                <input type="hidden" name="custom_resource_data">

                <div class="form-field">
                    <label>Title</label>
                    <input type="text" name="title" placeholder="Title" value="{{ old('title') ? old('title') : $homework->title }}">
                    @error('title')
                        <span class="error-message">{{ $message }}</span>
                    @enderror
                </div>

                <div class="form-field">
                    <label>Instruction(Optional)</label>
                    <textarea name="instruction" id="" rows="3" placeholder="Instruction">{{ old('instruction') ? old('instruction') : $homework->instruction }}</textarea>
                    @error('instruction')
                        <span class="error-message">{{ $message }}</span>
                    @enderror
                </div>

                <div class="form-field">
                    <div class="form-field">
                        <label>Chapter(Optional)</label>
                        <select class="chapter-select" name="chapter">
                            <option selected disabled>Select Chapter</option>
                            @foreach($chapters as $chapter)
                                <option value="{{ $chapter->id }}"
                                    @if( old('chapter') == $chapter->id )
                                        selected
                                    @elseif( $homework->chapter && $homework->chapter->id == $chapter->id )
                                        selected
                                    @endif>
                                    {{ $chapter->name }}
                                </option>
                            @endforeach
                        </select>
                        @error('chapter')
                            <span class="error-message">{{ $message }}</span>
                        @enderror
                    </div>
                </div>

                @component('classroom.homeworks.common.file_block_container', ['homework' => $homework])
                @endcomponent

                <div class="text-center">
                    <button type="submit" class="button--primary disableOnSubmitButton">Update</button>
                </div>
            </form>
        </div>
    </div>
@endsection
