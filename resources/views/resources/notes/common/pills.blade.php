<ul class="nav nav-tabs nav-stacked mb-3">
    <li class="nav-pills @if( !isset(request()->type) || request('type') == 'one_page' ) active @endif">
        <a href="{{ url()->current(). '?type=one_page' }}" class="@if( !isset(request()->type) || request('type') == 'one_page' ) active @endif">One Page</a>
    </li>
    <li class="nav-pills @if(request('type') == 'comprehensive') active @endif">
        <a href="{{ url()->current(). '?type=comprehensive' }}" class="@if(request('type') == 'comprehensive') active @endif">Comprehensive</a>
    </li>
    {{-- <li class="nav-pills @if(request('type') == 'detailed') active @endif">
        <a href="{{ url()->current(). '?type=detailed' }}" class="@if(request('type') == 'detailed') active @endif">Detailed</a>
    </li> --}}
</ul>
