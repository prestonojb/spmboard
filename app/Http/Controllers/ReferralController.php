<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class ReferralController extends Controller
{
    public function getUniqueCode( $length = 10 )
    {
        $code = getRandomString( $length );
        $code_exists = User::where('referral_code', $code)->exists();
        if($code_exists) {
            $code = $this->getUniqueCode( $length );
        } else {
            return $code;
        }
    }

    /**
     * Get View
     */
    public function getView()
    {
        $user = auth()->user();

        // User doesn't have RP
        if(!$user->hasRewardPoints()) {
            return redirect()->back()->with('error', 'Your account is not eligible to join our referral programme.');
        }
        
        return view('users.referral', compact('user'));
    }

    /**
     * Generate referral link
     */
    public function generateReferralLink()
    {
        $user = auth()->user();

        // User doesn't have RP
        if(!$user->hasRewardPoints()) {
            return redirect()->back()->with('error', 'Your account is not eligible to join our referral programme.');
        }

        // User has existing Referral Link
        if(!is_null($user->referral_code)) {
            return redirect()->back()->with('error', 'Your account already has an existing referral link.');
        }

        // Update user referral code
        $code = $this->getUniqueCode();
        $user->update(['referral_code' => $code]);

        return redirect()->back()->with('success', 'Referral Link is generated successfully!');
    }
}
