<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class StudentReport extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($student, $classroom, $report)
    {
        $this->student = $student;
        $this->classroom = $classroom;
        $this->report = $report;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('classroom.email.teacher.student_report')
                     ->from( $this->classroom->teacher->email )
                     ->with([
                         'student' => $this->student,
                         'classroom' => $this->classroom,
                         'report' => $this->report,
                         'url' => route('parent.students.show', $this->classroom->id),
                     ])
                     ->attach( $this->report->url );
    }
}
