@extends('classroom.layouts.app')

@section('meta')
    <title>Lessons | {{ config('app.name') }}</title>
@endsection


@section('content')

@component('classroom.common.breadcrumbs')
    <a href="{{ route('classroom.classrooms.show', $classroom->id) }}">Dashboard</a> /
    Lessons
@endcomponent

@component('classroom.common.page_heading', ['title' => 'Lessons'])
    @can('create_lesson', $classroom)
        @slot('button_group')
            <a href="{{ route('classroom.lessons.create', $classroom->id) }}" class="button--primary sm"><i class="fas fa-plus"></i> Add Lesson</a>
        @endslot
    @endcan
@endcomponent

@component('classroom.lessons.common.table', ['lessons' => $lessons])
@endcomponent

@endsection
