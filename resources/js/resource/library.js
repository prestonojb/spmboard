// Create new library Button
$('#createNewLibraryButton').click(function(){
    var $t = $(this);
    var initHtml = $(this).html();
    window.showLoader( $t, 'in-button', 'white' );

    // Validate Form
    var $form = $t.closest('form');
    var resource_id = $form.find('input[name=resource_id]').val();

    // Validate inputs with required property
    $form.find('input').each(function(){
        // Adds error message if input is required but empty length
        if( $(this).prop('required') ) {
            if( !$.trim( $(this).val() ).length ) {
                // Prevent adding more than one error message after input
                if( $(this).next('.error-message').length == 0) {
                    $(this).after( '<span class="error-message">This field is required.</span>' );
                }
            } else {
                $(this).next('.error-message').remove();
            }
        }
    });
    // Disable modal if exists
    var $modal = $t.closest('.modal');
    var $saveToLibraryModal = $('#saveToLibraryModal');
    if($modal.length > 0) {
        // Reference: https://stackoverflow.com/a/38502417
        $modal.data('bs.modal')._config.backdrop = 'static';
        $modal.data('bs.modal')._config.keyboard = false;
    }

    if( $form.find('.error-message').length > 0 ) {
        // Form is invalid
        window.hideLoader($t, initHtml);
        // Re-enable modal
        if($modal.length > 0) {
            $modal.data('bs.modal')._config.backdrop = true;
            $modal.data('bs.modal')._config.keyboard = true;
        }
    } else {
        // Form is valid
        $.ajax({
            method: "POST",
            url: "/libraries/store",
            data: $form.serialize(),
            success: function(response) {
                if( typeof (response) == 'string' ) {
                    // Reset Button
                    window.hideLoader($t, initHtml);

                    // Show error message
                    toastr.error(response);
                } else {
                    var library = response;
                    // Add library block to modal (#saveToLibraryModal)
                    $.ajax({
                        method: "POST",
                        url: "/libraries/"+ library.id +"/"+ resource_id +"/get_list_item_html",
                        success: function(newLibraryBlockHtml) {
                            $saveToLibraryModal.find('#createNewLibraryListItem').before(newLibraryBlockHtml);
                        }
                    });

                    // Reset Button
                    window.hideLoader($t, initHtml);

                    // Reset Modal
                    // Remove form input values
                    $form.find('input[type=text]').val('');
                    // Hide modal
                    $modal.modal('hide');
                    $modal.data('bs.modal')._config.backdrop = true;
                    $modal.data('bs.modal')._config.keyboard = true;

                    // Show toast message
                    toastr.success(library.name + ' added successfully!');
                }
            },
            error: function() {
                toastr.error('An error occured.');
            }
        });
    }
});

// Toggle save to library checkbox
$('body').on('change', '.toggleSaveToLibraryCheckBox', function(e){
    e.preventDefault();
    e.stopPropagation();
    var $t = $(this);
    var $form = $t.closest('form');
    var library = $t.data('library');
    var resource = $t.data('resource');
    // AJAX
    $.ajax({
        url: $form.attr('action'),
        method: 'POST',
        success: function(response){
            if(response == 'saved') {
                toastr.success('Saved to ' + library.name);
                $t.prop('checked', true);
            } else if(response == 'removed') {
                toastr.success('Removed from ' + library.name);
                $t.prop('checked', false);
            } else if(response == 'full') {
                toastr.error(library.name + ' is full!');
                $t.prop('checked', false);
            } else {
                toastr.error('An error occurred!');
            }
        },
        error: function(resposne) {
            toastr.error('An error occurred!');
        }
    });
});



// Remove resource from library button
$('.deleteResourceFromLibraryButton').click(function(e){
    e.preventDefault();
    e.stopPropagation();
    var $t = $(this);
    var url = $t.data('action');
    var library = $t.data('library');
    Swal.fire({
        title: 'Remove from ' + library.name + '?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        reverseButtons: true,
        confirmButtonText: 'Confirm',
    }).then(function(result){
        if(result.value){
            $.ajax({
                method: "POST",
                url: url,
                success: function(response) {
                    if(response == 'removed') {
                        $t.parents('.file-block').parent('.col-12').remove();
                        toastr.success('Removed from ' + library.name);
                    } else {
                        toastr.error("An error occured!");
                    }
                },
                error: function(response) {
                    toastr.error("An error occured!");
                }
            });
        }
    });
});
