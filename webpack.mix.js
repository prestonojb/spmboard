const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */


// SASS
mix.sass('resources/sass/app.scss', 'public/css')
    .sass('resources/sass/voyager/app.scss', 'public/css/voyager')
    .sass('resources/sass/main/app.scss', 'public/css/main')
    .sass('resources/sass/forum/app.scss', 'public/css/forum')
    .sass('resources/sass/classroom/app.scss', 'public/css/classroom');

mix.js('resources/js/app.js', 'public/js');
mix.js('resources/js/forum.js', 'public/js');
mix.js('resources/js/resource.js', 'public/js');
mix.js('resources/js/classroom.js', 'public/js');

if (mix.inProduction()) {
    mix.version();
}
