<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class ResourcePost extends Model
{
    protected $guarded = [];


    /**
     * ---------------------
     * Relationships
     * ---------------------
     */

    public function ratings()
    {
        return $this->hasMany(ResourcePostRating::class);
    }

    public function chapter()
    {
        return $this->belongsTo(Chapter::class);
    }

    public function lesson()
    {
        return $this->belongsTo(Lesson::class);
    }

    public function resources()
    {
        return $this->belongsToMany( Resource::class, 'resource_resource_post', 'resource_post_id', 'resource_id')->withPivot('type', 'created_at', 'updated_at');
    }

    public function questions()
    {
        return $this->belongsToMany(Question::class, 'classroom_question', 'question_id', 'classroom_id')->withTimestamps();
    }

    /**
     * ---------------------
     * Accessors
     * ---------------------
     */
    public function getPrimaryAttribute()
    {
        return $this->title;
    }

    public function getSecondaryAttribute()
    {
        $str = "";
        if( auth()->user()->student && $this->getStudentRating( auth()->user()->student ) ) {
            $str .= $this->getStudentRating( auth()->user()->student ). '<i class="fas fa-star yellow"></i> · ';
        } elseif( auth()->user()->teacher && !is_null($this->average_rating) ) {
            $str .= $this->average_rating. '<i class="fas fa-star yellow"></i> · ';
        }

        if($this->chapter) {
            $str .= "{$this->chapter->name} · ";
        }

        return $str."Posted {$this->created_at->shortRelativeDiffForHumans()}";
    }

    public function getUrlAttribute()
    {
        return route('classroom.lessons.resource_posts.show', [$this->lesson->id, $this->id]);
    }

    public function getAverageRatingAttribute()
    {
        $avg_rating = ResourcePostRating::where('resource_post_id', $this->id)->avg('rating');
        if( $avg_rating ) {
            return number_format( $avg_rating, 1 );
        } else {
            return null;
        }
    }

    /**
     * -------------------
     * Helpers
     * -------------------
     */

    /**
     * Get all Custom Resource URLs
     * @return Collection
     */
    public function custom_resource_urls()
    {
        // Returns all custom resource URLs
        $urls = $this->customResourceRelativeUrls( false );
        $full_urls = $urls->map(function($url){
            return Storage::cloud()->url($url);
        });

        return $full_urls;
    }

    /**
     * Returns all Custom Resource URLs
     * @param boolean (optional, default = true) $toArray
     * @return array|Collection
     */
    public function customResourceRelativeUrls( $toArray = true )
    {
        $urls = DB::table('resource_resource_post')->where('resource_post_id', $this->id)->whereNull('resource_id')->pluck('file_url');

        return $toArray ? $urls->toArray()
                        : $urls;
    }

    /**
     * Returns collection of custom resoures
     * @return Collection [{'file_name' => 'xxx', 'file_url' => 'xxx'},...]
     */
    public function getCustomResources()
    {
        $custom_resource_rows = DB::table('resource_resource_post')->select('file_name', 'file_url')->where('resource_post_id', $this->id)->whereNull('resource_id')->get();

        $custom_resources = collect();

        // Modify 'file_url' column to contain full path
        foreach( $custom_resource_rows as $row ) {
            $row->file_url = Storage::url( $row->file_url );
            $custom_resources->push( $row );
        }

        return $custom_resources;
    }

    /**
     * Attach prebuilt and custom resources to resource post
     * @param array $resource_data [{'resource_id' => 1, 'type' => null}, {'resource_id' => 2, 'type' => 'question'}, ...]
     * @param array $custom_resource_data [{'file_name' => 'respiration', 'file_url' => 'resources/xxxxx.pdf'}, ...]
     */
    public function storeResources( $resource_data, $custom_resource_data )
    {
        foreach( $resource_data as $resource_datum ) {
            $this->resources()->attach(
                $resource_datum->resource_id, [
                    'type' => $resource_datum->type,
                ]
            );
        }

        foreach( $custom_resource_data as $custom_resource_datum ) {
            DB::table('resource_resource_post')->insert([
                'file_name' => $custom_resource_datum->file_name,
                'file_url' => $custom_resource_datum->file_url,
                'resource_post_id' => $this->id,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]);
        }
    }

    public function getStudentCountByRating( $rating )
    {
        return ResourcePostRating::where('resource_post_id', $this->id)->where('rating', $rating)->count();
    }

    public function getStudentPercentageByRating( $rating )
    {
        $total_students = $this->lesson->classroom->students()->count();

        if($total_students) {
            return ($this->getStudentCountByRating($rating))/$total_students * 100;
        } else {
            return 0;
        }
    }


    public function getStudentsByRating( $rating ) {
        $student_id_arr = ResourcePostRating::where('resource_post_id', $this->id)->where('rating', $rating)->pluck('student_id');

        return Student::find($student_id_arr);
    }

    /**
     * Returns Resource Post Rating of resource post associated with student
     * @param App\Student $student
     * @return App\ResourcePostRating
     */
    public function getRatingByStudent( Student $student )
    {
        return $this->ratings()->where('student_id', $student->user_id)->first();
    }

    /**
     * Get rating by student
     * @param App\Student
     * @return float|null
     */
    public function getStudentRating( Student $student )
    {
        $rating_row = $this->getRatingByStudent( $student );
        return $rating_row ? number_format($rating_row->rating, 1)
                           : null;
    }

    /**
     * Get string representation of student rating on resource post
     * @param App\Student $student
     * @return string|null
     */
    public function metric( Student $student )
    {
        $rating = $this->getStudentRating( $student );
        return $rating ? "{$rating}/5" : null;
    }

    /**
     * Student add/update rating
     * @param App\Student
     * @param int $rating (0-5)
     * @return int $rating
     */
    public function updateOrAddRating( Student $student, $rating )
    {
        // Get rating
        $rating_row = $this->getStudentRating( $student );
        if( !is_null($rating_row)) {
            // Update rating
            $rating_row->update([
                'rating' => $rating
            ]);
        } else {
            // Add new rating
            $rating = $this->ratings()->create([
                            'student_id' => $student->user_id,
                            'rating' => $rating
                        ]);
        }
        return $rating;
    }

    /**
     * Delete resource post all related relationships and itself
     * :deleted: Ratings, Custom Resource Files, Resource Post
     * :detached: Resources
     */
    public function cleanDelete()
    {
        // Delete ALL custom files
        $urls = $this->customResourceRelativeUrls();
        Storage::cloud()->delete( $urls );

        // Delete all ratings and resources
        $this->ratings()->delete();
        $this->resources()->detach();
        // Delete resource post row
        $this->delete();
    }
}
