@if( isset($board_pass) && !is_null($board_pass) )
    <div class="board-pass">
        <div class="item">
            <div class="item-left text-center bg-blue6 white">
                {{-- Active Board Pass --}}
                @if( $board_pass->is_active )
                    <p class="blue6 mb-2 font-size2 color-inherit">Valid until</p>
                    <h2 class="font-size12 color-inherit"><b>{{ $board_pass->end_at->format('d') }}</b></h2>
                    <p class="day font-size4 font-weight500 color-inherit">{{ $board_pass->end_at->format('M Y') }}</p>
                @elseif( $board_pass->is_expired )
                    {{-- Expired Board Pass --}}
                    <p class="blue6 mb-2 font-size2 color-inherit">Expired on</p>
                    <h2 class="font-size12 color-inherit"><b>{{ $board_pass->end_at->format('d') }}</b></h2>
                    <p class="day font-size4 font-weight500 color-inherit">{{ $board_pass->end_at->format('M Y') }}</p>
                @endif

                <span class="up-border"></span>
                <span class="down-border"></span>
            </div> <!-- end item-right -->

            <div class="item-right">
                <div class="align-self-start">
                    {{-- <p class="gray2 font-size2 text-uppercase mb-1">{{ ucwords($board_pass) }}</p> --}}
                    <h2 class="font-size11 blue6"><b>Board Pass</b></h2>
                    <h3 class="font-size4 mb-2"><u>Exam Builder</u></h3>
                    <ul class="checklist">
                        <li class=""><b>Download</b> worksheets in PDF</li>
                        <li class=""><b>Unlimited</b> worksheets</li>
                        <li class=""><b>Up to 20 questions</b> per worksheet</li>
                    </ul>
                    @if( setting('board_pass.one_for_one_promotion') )
                        <p>
                            <span class="status-tag red text-uppercase mb-1">Buy 1 month, FREE 1 month</span>
                        </p>
                    @endif
                    <p class="gray2 font-size2">
                        <i>Activated on {{ $board_pass->created_at->toFormattedDateString() }}</i>
                    </p>
                </div>
                <div class="align-self-end">
                    @if( $board_pass->is_active )
                        <a class="cta-button button--primary lg" href="{{ route('board_pass') }}">Extend</a>
                    @elseif( $board_pass->is_expired )
                        <a class="cta-button button--success lg" href="{{ route('board_pass') }}">Renew</a>
                    @endif
                </div>
            </div>

        </div>
    </div>
@else
    <div class="board-pass">
        <div class="item">
            <div class="item-left text-center bg-blue6 white">

                <p class="blue6 mb-2 font-size2 color-inherit">Invalid</p>
                <h2 class="font-size12 color-inherit"><b>XX</b></h2>
                <p class="day font-size4 font-weight500 color-inherit">XX 20XX</p>

                <span class="up-border"></span>
                <span class="down-border"></span>
            </div> <!-- end item-right -->

            <div class="item-right">
                <div class="align-self-start">
                    <p class="gray2 font-size2 text-uppercase mb-1">XXX Pass</p>
                    <h2 class="font-size11 blue6"><b>Board Pass</b></h2>
                    <h3 class="font-size4 mb-2"><u>Exam Builder</u></h3>
                    <ul class="checklist">
                        <li class=""><b>Download</b> worksheets in PDF</li>
                        <li class=""><b>Unlimited</b> worksheets</li>
                        <li class=""><b>Up to 20 questions</b> per worksheet</li>
                    </ul>
                    @if( setting('board_pass.one_for_one_promotion') )
                        <p>
                            <span class="status-tag red text-uppercase mb-1">Buy 1 month, FREE 1 month</span>
                        </p>
                    @endif
                </div>
                <div class="align-self-end">
                    @if( setting('board_pass.one_for_one_promotion') )
                        <a class="cta-button button--danger lg" href="{{ route('board_pass') }}">Grab My Offer</a>
                    @else
                        <a class="cta-button button--danger lg" href="{{ route('board_pass') }}">Activate</a>
                    @endif
                </div>
            </div>

        </div>
    </div>
@endif
