@if( setting('marketing.ebooks_promotion') )
    <div class="d-none smart-bar-container ebooksPromotion">
        <div class="container">
            <div class="smart-bar">
                <div class="d-none d-lg-block"></div>
                <div class="text">
                    Get up to <b>3 FREE E-books</b> of your preferred subjects when you sign up now! | <b>Limited time only!</b>
                </div>
                <button class="dismiss-button dismissButton ebooksPromotion"><span class="iconify" data-icon="emojione-monotone:cross-mark" data-inline="false"></span></button>
            </div>
        </div>
    </div>
@endif
