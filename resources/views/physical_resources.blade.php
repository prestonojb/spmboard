@extends('main.layouts.app')

@section('meta')
<title>Board Resource | {{ config('app.name') }}</title>
@endsection

@section('styles')
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css"/>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css"/>
@endsection

@section('content')

<main class="phy-resources bg-white">
    <section class="main__landing phy-resources__main mb-4">
        <div class="text-center">
            {{-- Board Resources Poster --}}
            <img class="mb-3" width="500" src="{{ asset('img/phy_resources/spm_trial_papers_2019.jpg') }}" alt="Ascension Unlimited Pass Poster" class="phy-resources__cover-img">

            <h1 class="heading">Board Resource</h1>
            <h2 class="sub-heading">Need more physical resources to help facilitate your studies?</h2>
            <h3 class="gray2 font-size4 mb-4">
                What are you waiting for? Visit our <a class="font-size-inherit" href="https://shopee.com.my/resourcesboard" target="_blank">Shopee store</a> now and place your orders.
            </h3>

            <a class="button--primary lg" href="#learn_more">
                Learn More
            </a>
        </div>
    </section>


    <section class="container text-center about__is-board-for-me py-5" id="learn_more">
        <div class="mb-3 mb-md-4">
            <h4 class="heading">For SPM</h4>
            <h5 class="sub-heading">Subjects of state past papers available </h5>
        </div>
        @php
            $subjects = ['Maths', 'Add Maths', 'Chemistry', 'Biology', 'Physics'];
            $icons = ['<span class="iconify" data-icon="carbon:math-curve" data-inline="false"></span>',
                    '<span class="iconify" data-icon="mdi:function-variant" data-inline="false"></span>',
                    '<span class="iconify" data-icon="ri:test-tube-line" data-inline="false"></span>',
                    '<span class="iconify" data-icon="mdi:bacteria-outline" data-inline="false"></span>',
                    '<span class="iconify" data-icon="bx:bx-rocket" data-inline="false"></span>'
                    ];
            $states = [['Johor', 'MRSM', 'Kelantan', 'Pahang', 'Penang', 'Perlis', 'Selangor', 'Terengganu'],
                        ['Perak', 'MRSM', 'Terengganu', 'Negeri Sembilan', 'Kelantan', 'Johor', 'Selangor', 'Penang'],
                        ['Penang', 'MRSM', 'Terengganu', 'Perlis', 'Kelantan', 'Johor', 'Selangor', 'Pahang'],
                        ['Johor', 'MRSM', 'Kelantan', 'Negeri Sembilan', 'Pahang', 'Selangor', 'Penang'],
                        ['Johor', 'MRSM', 'Kelantan', 'Negeri Sembilan', 'Pahang', 'Selangor', 'Penang']
                        ];
        @endphp
        <div class="row justify-content-center">
            @for($i=0; $i < count($subjects); $i++)
                <div class="col-6 col-md-4 col-lg-2">
                    <div class="main__elements">
                        {!! $icons[$i] !!}
                        <h6 class="title"><strong>{{ $subjects[$i] }}</strong></h6>
                        <ol>
                            @for($j=0; $j < count($states[$i]); $j++)
                                <li>{{ $states[$i][$j] }}</li>
                            @endfor
                        </ol>
                    </div>
                </div>
            @endfor
        </div>
        <a class="button--primary lg mr-2" href="https://shopee.com.my/SPM-Trial-Papers-2019-Compilation-8-State-SPM-Kertas-Peperiksaan-Percubaan-2019-8-NEGERI--i.260501581.4241332688" target="_blank">
            <span class="iconify" data-icon="icons8:buy" data-inline="false"></span>
            Buy Now
        </a>
        <a class="underline-on-hover" href="{{ route('past_papers.home', 'spm') }}">
            More SPM Past Papers
        </a>
    </section>

    <section class="container text-center about__is-board-for-me py-5">
        <div class="mb-3 mb-md-4">
            <h4 class="heading">For IGCSE<h4>
            <h5 class="sub-heading">Past Papers available</h5>
        </div>
        @php
            $subjects = [];
            $icons = [];
            $states = [];
        @endphp
        @if(count($subjects))
            <div class="row justify-content-center">
                @for($i=0; $i < count($subjects); $i++)
                    <div class="col-6 col-md-4 col-lg-2">
                        <div class="main__elements">
                            {!! $icons[$i] !!}
                            <h6 class="title"><strong>{{ $subjects[$i] }}</strong></h6>
                            <ol>
                                @for($j=0; $j < count($states[$i]); $j++)
                                    <li>{{ $states[$i][$j] }}</li>
                                @endfor
                            </ol>
                        </div>
                    </div>
                @endfor
                <a class="button--primary lg" href="https://shopee.com.my/SPM-Trial-Papers-2019-Compilation-8-State-SPM-Kertas-Peperiksaan-Percubaan-2019-8-NEGERI--i.260501581.4241332688" target="_blank">
                    <span class="iconify" data-icon="icons8:buy" data-inline="false"></span>
                    Buy Now
                </a>
                <a class="underline-on-hover" href="{{ route('past_papers.home', 'spm') }}">
                    More IGCSE Past Papers
                </a>
            </div>
        @else
            <div>
                <img src="{{ asset('img/essential/empty.svg') }}" alt="Empty" class="mb-2" width="300" height="250">
            </div>
            <h4>No resources yet. Stay tuned for more.</h4>
            <a class="button--primary lg" href="{{ route('past_papers.home', 'spm') }}">
                IGCSE Past Papers
            </a>
        @endif
    </section>



    <section class="container ascension__faq mb-4">
        <h4 class="heading">FAQ</h4>
        <div id="ascension_faq_accordion">
            @php
                $questions = ['What is Board Resource?'];
                $answers = ['Board Resource is a platform where we manage your orders for our physical resources!'];
            @endphp

            @for($i=0; $i < count($questions); $i++)
                <div class="card faq__card">
                    <div class="card-header faq__card-header">
                        <h5 class="mb-0">
                            <button class="button-link" data-toggle="collapse" data-target="#resources{{ $i }}" aria-expanded="true" aria-controls="collapseOne">
                                {{ $questions[$i] }}
                            </button>
                        </h5>
                    </div>
                    <div id="resources{{ $i }}" class="collapse" aria-labelledby="headingOne" data-parent="#ascension_faq_accordion">
                        <div class="card-body faq__card-body">
                            {{ $answers[$i] }}
                        </div>
                    </div>
                </div>
            @endfor
        </div>
    </section>

    @include('main.navs.footer')
</main>
@endsection

@section('scripts')
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
<script>
$(function(){
    $('a[href="#learn_more"]').click(function(e) {
        e.preventDefault();
        $('html, body').animate({
            scrollTop: $("#learn_more").offset().top - 68
        }, 500);
});
});
</script>
@endsection
