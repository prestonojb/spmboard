@php
$is_beta = isset($is_beta) && $is_beta;
switch($type) {
    case 'guide':
        $title = 'Guides';
        break;
    case 'topical_past_paper':
        $title = 'Topical Worksheets';
        break;
    case 'past_paper':
        $title = 'Past Papers';
        break;
    case 'note':
        $title = 'Notes';
        break;
    case 'exam_builder':
        $title = 'Exam Builder';
        break;
}
@endphp

{{-- Header --}}
<div class="container">
    <div class="d-flex justify-content-between align-items-center">
        <p class="resource__h1 m-0">{{ $title }}</p>

        @php
            $exam_boards = \App\ExamBoard::all();
        @endphp

        <select class="switch-exam-board-select maxw-150px" name="exam_board" id="">
            @switch($type)
                @case('guide')
                    @foreach($exam_boards as $board)
                        <option value="{{ $board->slug }}" data-url="{{ route('guides.home', $board->slug) }}" @if($board->slug == $exam_board->slug) selected @endif>{{ $board->name }}</option>
                    @endforeach
                    @break
                @case('topical_past_paper')
                    @foreach($exam_boards as $board)
                        <option value="{{ $board->slug }}" data-url="{{ route('topical_past_papers.home', $board->slug) }}" @if($board->slug == $exam_board->slug) selected @endif>{{ $board->name }}</option>
                    @endforeach
                    @break
                @case('past_paper')
                    @foreach($exam_boards as $board)
                        <option value="{{ $board->slug }}" data-url="{{ route('past_papers.home', $board->slug) }}" @if($board->slug == $exam_board->slug) selected @endif>{{ $board->name }}</option>
                    @endforeach
                    @break
                @case('note')
                    @foreach($exam_boards as $board)
                        <option value="{{ $board->slug }}" data-url="{{ route('notes.home', $board->slug) }}" @if($board->slug == $exam_board->slug) selected @endif>{{ $board->name }}</option>
                    @endforeach
                    @break
                @case('exam_builder')
                    @foreach($exam_boards as $board)
                        <option value="{{ $board->slug }}" data-url="{{ route('exam_builder.landing').'?exam_board='.$board->slug }}" @if($board->slug == $exam_board->slug) selected @endif>{{ $board->name }}</option>
                    @endforeach
                    @break
            @endswitch
        </select>
    </div>
</div>
