<?php

namespace App\Http\Controllers\Voyager;

use App\Blog;
use App\BlogAuthor;
use App\BlogCategory;
use \TCG\Voyager\Http\Controllers\VoyagerBaseController as VoyagerBaseController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class VoyagerBlogController extends VoyagerBaseController
{
    /**
     * Create blog
     */
    public function create(Request $request)
    {
        $is_edit = false;
        $blog_authors = BlogAuthor::all();
        $blog_categories = BlogCategory::all();

        return view('vendor.voyager.blogs.create_edit', compact('is_edit', 'blog_authors', 'blog_categories'));
    }

    /**
     * Edit blog
     */
    public function edit(Request $request, $id)
    {
        $blog = Blog::find($id);
        $blog_authors = BlogAuthor::all();
        $blog_categories = BlogCategory::all();
        $is_edit = true;

        return view('vendor.voyager.blogs.create_edit', compact('is_edit', 'blog', 'blog_authors', 'blog_categories'));
    }
}
