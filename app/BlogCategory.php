<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Blog;

class BlogCategory extends Model
{
    protected $guarded = [];

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function blogs() 
    {
        return $this->hasMany(Blog::class);
    }

    public function setNameAttribute($value)
    {
        $this->attributes['name'] = $value;
        $this->attributes['slug'] = str_slug($value);
    }   
}
