@extends('forum.layouts.app')
@section('meta')
    <title>2020 Math Board Quiz | {{ config('app.name') }}</title>
    <meta name="description" content="Complete the quiz during the event and stand a chance to win RM50 Netflix gift cards!">
    <meta property="og:image" content="{{ asset('img/essential/2020_math_board_quiz_poster.jpeg') }}" />
    <meta property="og:type" content="website" />
@endsection

@section('content')
@include('main.navs.header')
<main class="quiz">
    <header class="quiz__header">
        <div class="container">
            {{-- Active --}}
            @if($status == 'active')
                <h1 class="heading">2020 Math Board Quiz</h1>
                <h2 class="sub-heading">Score the highest in the quiz and stand a chance to win <b>Netflix gift cards worth RM50</b>!</h2>
                <p class="body">By entering the Rules Quiz, each entrant agrees to be <br>
                    bound by these <button type="button" class="button-link blue4" data-toggle="modal" data-target="#quizTncModal" style="font-size: inherit;">terms and conditions</button>.</p>
                <div class="">
                    <p class="sub-heading mb-0"><b>Ends in</b></p>
                    <div class="quiz__countdown-timer mb-3">
                        <div class="unit-container"><span class="value" id="days"></span>days</div>
                        <div class="unit-container"><span class="value" id="hours"></span>Hours</div>
                        <div class="unit-container"><span class="value" id="minutes"></span>Minutes</div>
                        <div class="unit-container"><span class="value" id="seconds"></span>Seconds</div>
                    </div>
                </div>
                <button class="button--primary lg startQuizButton">
                    <span class="iconify" data-icon="entypo:controller-play" data-inline="false"></span>
                    Start Quiz
                </button>

            {{-- Pre --}}
            @elseif($status == 'pre')

                <div class="text-center">
                    <h3 class="sub-heading mb-1">Board Quiz will begin on <b>{{ $start_time->toDayDateTimeString() }}</b></h3>
                    <h3 class="sub-heading mb-3">Ends on <b>{{ $end_time->toDayDateTimeString() }}</b></h3>
                    <h2 class="quiz__half-border-blue heading">2020 Math Board Quiz will be starting in...</h2>
                </div>

                    <div class="quiz__countdown-timer mb-3">
                        <div class="unit-container"><span class="value" id="days"></span>days</div>
                        <div class="unit-container"><span class="value" id="hours"></span>Hours</div>
                        <div class="unit-container"><span class="value" id="minutes"></span>Minutes</div>
                        <div class="unit-container"><span class="value" id="seconds"></span>Seconds</div>
                    </div>

                {{-- <div>
                    <h5 class="gray2 mb-3">Need help on your homework? Ask away now on</h5>
                    <div class="d-flex justify-content-center align-items-center">
                        <a href="{{ route('questions.index', 'SPM').'?utm_campaign=board_quiz&utm_medium=event_page&utm_source=website' }}" class="button--primary lg mr-3">SPM Board</a>
                        <a href="{{ route('questions.index', 'IGCSE').'?utm_campaign=board_quiz&utm_medium=event_page&utm_source=website' }}" class="button--primary lg">IGCSE Board</a>
                    </div>
                </div> --}}

                <button class="button--primary lg startQuizButton">
                    Learn More
                </button>

            {{-- Post --}}
            @elseif($status == 'post')
                <div class="text-center mt-4">
                    <h2 class="quiz__half-border-blue heading">2020 Math Board Quiz has ended.</h2>
                    <h3 class="sub-heading mb-1">{{ setting('marketing.2020_math_board_quiz_no_of_entries') ?? 300 }} participants joined this Board Quiz.</h3>
                    <p class="gray2 mb-4">5 winners will be announced real soon! Stay tuned.</p>
                    <a href="{{ route('/') }}" class="button--primary lg">Back to Board</a>
                </div>
            @endif
        </div>
    </header>


    @if( $status == 'active' )

        <section class="quiz__body my-4" id="quizBody">
            <div class="container">

                <h2 class="heading">Board Quiz is live!</h2>
                <h3 class="sub-heading mb-1">Complete the quiz and stand a chance to win Netflix gift card worth RM50!</h3>
                <p class="gray2">If you are stuck with any questions in the quiz, feel free to search for answers on Board(<a href="{{ route('questions.index', 'spm') }}">SPM</a>, <a  href="{{ route('questions.index', 'igcse') }}">IGCSE</a>)!</p>

                <div class="row">

                    <div class="col-12 col-lg-8">

                        <div class="panel mt-2 mb-4">

                            <form action="{{ route('events.2020.math_board_quiz.quiz') }}" method="POST" id="mathQuizForm" class="steps-form">
                                @csrf
                                <h3>Personal Details</h3>
                                <fieldset>
                                    <h2>Personal Details</h2>
                                    <div class="form-field">
                                        <label>{{ __('Name') }}</label>
                                        <input type="text" name="name" value="{{ old('name') }}" required autocomplete="name" placeholder="Name">
                                        @error('name')
                                            <span class="error-message">{{ $message }}</span>
                                        @enderror
                                    </div>
                                    <div class="form-field">
                                        <label>{{ __('Email Address') }}</label>
                                        <input type="email" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="Email Address">
                                        @error('email')
                                            <span class="error-message">{{ $message }}</span>
                                        @enderror
                                    </div>
                                    <div class="form-field">
                                        <label>{{ __('Phone Number') }}</label>
                                        <input type="text" name="phone_number" value="{{ old('phone_number') }}" required autocomplete="phone_number" placeholder="Phone Number">
                                        <p><small>* This is how we can notify you if you win the quiz!</small></p>
                                        @error('phone_number')
                                            <span class="error-message">{{ $message }}</span>
                                        @enderror
                                    </div>

                                    <div class="form-field">
                                        <label for="exam_board">{{ __('Exam Board') }}</label>
                                        <select name="exam_board" id="exam_board" required>
                                            <option value="" disabled selected>Select Exam Board</option>
                                            @foreach($exam_boards as $board)
                                                <option value="{{ $board->name }}" @if(old('exam_board') == $board->name) selected @endif>{{ $board->name }}</option>
                                            @endforeach
                                        </select>
                                        @error('exam_board')
                                            <span class="error-message">{{ $message }}</span>
                                        @enderror
                                    </div>

                                    <div class="quiz__nav-button-group">
                                        <button class="button--primary next">Next</button>
                                    </div>
                                </fieldset>

                                <h3>Quiz</h3>
                                <fieldset>
                                    <h2>Quiz</h2>

                                    @for($i=0; $i < count($questions); $i++)
                                        <div class="form-field">
                                            <h4>{{ $questions[$i] }}</h4>
                                            @for($j=0; $j < count($options[$i]); $j++)
                                                <div class="styled-radio-input">
                                                    <input type="radio" name="question_{{ $i }}" id="question_{{ $i }}_{{ $j }}" value="{{ $j }}" class="" required @if( !is_null(old('question_'.$i, null)) && old('question_'.$i) == $j ) checked @endif>
                                                    <label for="question_{{ $i }}_{{ $j }}" class="">{{ $options[$i][$j] }}</label>
                                                </div>
                                            @endfor
                                            @error('question_{{ $i }}')
                                                <span class="error-message">{{ $message }}</span>
                                            @enderror
                                        </div>
                                    @endfor

                                    <div class="quiz__nav-button-group">
                                        <button class="button--primary--inverse prev">Previous</button>
                                        <button type="submit" class="button--primary">Submit</button>
                                    </div>

                                </fieldset>

                            </form>
                        </div>
                    </div>

                    {{-- Posters --}}
                    <div class="col-12 col-lg-4">
                        <div class="mb-3">
                            <h3 class="h4 mb-2">Seeking for online classes?</h3>
                            <a href="{{ route('ascension').'?utm_source=website&utm_campaign=board_quiz&utm_medium=quiz_poster_pre' }}" target="_blank">
                                <img src="{{ asset('img/ascension/up-poster.jpg') }}" alt="">
                            </a>
                        </div>
                        <div class="">
                            <h3 class="h4 mb-2">Need printed State Papers?</h3>
                            <a href="{{ route('physical_resources').'?utm_source=website&utm_campaign=board_quiz&utm_medium=quiz_poster_pre' }}" target="_blank">
                                <img src="{{ asset('img/phy_resources/ad_poster.png') }}" alt="">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    @elseif($status == 'pre')
        <section class="quiz__body my-4" id="quizBody">
            <div class="container">

                <h2 class="heading">Board Quiz is coming your way!</h2>
                <h3 class="sub-heading mb-1"> Complete the quiz during the event days and stand a chance to win RM50 Netflix gift cards!</h3>
                <p class="gray2">Before that, follow the steps below to be prepared for the quiz.</p>

                <div class="row">

                    <div class="col-12 col-lg-8">

                        <div class="panel mt-2 mb-4 p-3">

                            <div class="quiz-thank-you__steps">
                                {{-- Step # --}}
                                <div class="quiz-thank-you__step">
                                    <div class="step-number">1</div>
                                    <div class="step-body">
                                        <div class="step-text">
                                            <h2 class="heading">Like our Facebook Page to stay updated with contest!
                                                We will post any updates on the quiz on both our pages!</h2>
                                        </div>

                                        <div class="quiz__social-icons">
                                            <a href="https://facebook.com/spmboardmy" target="_blank" class="button--primary fb-button mr-1">
                                                <span class="iconify" data-icon="logos:facebook" data-inline="false"></span>
                                                <span class="text">SPM <span class="d-none d-md-inline">Board</span></span>
                                            </a>
                                            <a href="https://facebook.com/igcseboardmy" target="_blank" class="button--primary fb-button">
                                                <span class="iconify" data-icon="logos:facebook" data-inline="false"></span>
                                                <span class="text">IGCSE <span class="d-none d-md-inline">Board</span></span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                {{-- Step # --}}
                                <div class="quiz-thank-you__step">
                                    <div class="step-number">2</div>

                                    <div class="stepbody">
                                        <div class="step-text">
                                            <h2 class="heading">Share this contest with your friends!</h2>
                                        </div>
                                        <div id="shareIcons"></div>
                                    </div>
                                </div>
                                {{-- Step # --}}
                                <div class="quiz-thank-you__step">
                                    <div class="step-number">3</div>
                                    <div class="stepbody">
                                        <div class="step-text">
                                            <h2 class="heading">Start asking questions on Board!</h2>
                                            <p class="sub-heading">*Ask questions, get answered, get rewarded.</p>
                                        </div>

                                        <a href="{{ route('register').'?utm_source=website&utm_campaign=board_quiz&utm_medium=board_quiz_countdowm' }}" class="button--primary lg mr-2">Join for Free</a>
                                        <a class="underline-on-hover" href="{{ route('/').'?utm_source=website&utm_campaign=board_quiz&utm_medium=board_quiz_countdowm' }}">Learn More</a>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>

                    {{-- Posters --}}
                    <div class="col-12 col-lg-4">
                        <div class="mb-3">
                            <h3 class="h4 mb-2">Seeking for online classes?</h3>
                            <a href="{{ route('ascension').'?utm_source=website&utm_campaign=board_quiz&utm_medium=quiz_poster_pre' }}" target="_blank">
                                <img src="{{ asset('img/ascension/up-poster.jpg') }}" alt="">
                            </a>
                        </div>
                        <div class="">
                            <h3 class="h4 mb-2">Need printed State Papers?</h3>
                            <a href="{{ route('physical_resources').'?utm_source=website&utm_campaign=board_quiz&utm_medium=quiz_poster_pre' }}" target="_blank">
                                <img src="{{ asset('img/phy_resources/ad_poster.png') }}" alt="">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    @endif
</main>

@include('events.2020.math_board_quiz.common.tnc')
@endsection

@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-steps/1.1.0/jquery.steps.js"></script>
<script>
$(function(){

    function convertDateForIos(date) {
        var arr = date.split(/[- :]/);
        date = new Date(arr[0], arr[1]-1, arr[2], arr[3], arr[4], arr[5]);
        return date;
    }

    const second = 1000,
    minute = second * 60,
    hour = minute * 60,
    day = hour * 24;

    @if($status == 'pre')
        var countdownTime = convertDateForIos("{{ $start_time->toDateTimeString() }}");
    @elseif($status == 'active')
        var countdownTime = convertDateForIos("{{ $end_time->toDateTimeString() }}")
    @else
        var countdownTime = null;
    @endif


    if(countdownTime) {
        // countdownTime.setHours( countdownTime.getHours() - 8 );
        let countDown = countdownTime.getTime(),
        x = setInterval(function() {

            let now = new Date().getTime(),
                distance = countDown - now;

            document.getElementById('days').innerText = Math.floor(distance / (day)),
            document.getElementById('hours').innerText = Math.floor((distance % (day)) / (hour)),
            document.getElementById('minutes').innerText = Math.floor((distance % (hour)) / (minute)),
            document.getElementById('seconds').innerText = Math.floor((distance % (minute)) / second);

            // Refresh page when date is reached
            if (distance < 0) {
                location.reload()
            }

        }, second)
    }

    function prev() {
        $('div.actions > ul li a[href="#previous"]').click();
    }

    function next() {
        $('div.actions > ul li a[href="#next"]').click();
    }

    var form = $("#mathQuizForm").show();

    form.steps({
        headerTag: "h3",
        bodyTag: "fieldset",
        transitionEffect: 1,
        onInit: function ()
        {
            $prevButton = $('button.prev');
            $nextButton = $('button.next');

            $prevButton.click(function(e){
                e.preventDefault();
                prev();
            });

            $nextButton.click(function(e){
                e.preventDefault();
                next();
            });

            $('[type="submit"]').click(function(e){
                e.preventDefault();
                $(this).attr('disabled', true);
                $(this).closest('form').submit();
            });
        },
        onStepChanging: function (event, currentIndex, newIndex)
        {
            // Allways allow previous action even if the current form is not valid!
            if (currentIndex > newIndex)
            {
                return true;
            }

            // Needed in some cases if the user went back (clean up)
            if (currentIndex < newIndex)
            {
                // To remove error styles
                form.find(".body:eq(" + newIndex + ") label.error").remove();
                form.find(".body:eq(" + newIndex + ") .error").removeClass("error");
            }

            form.validate().settings.ignore = ":disabled,:hidden";

            return form.valid();

        },
        onStepChanged: function (event, currentIndex, priorIndex)
        {

        },
        onFinishing: function (event, currentIndex)
        {
            form.validate().settings.ignore = ":disabled";
            return form.valid();
        },
        onFinished: function (event, currentIndex)
        {
        }
    }).validate({
        messages:{
            // accept_tnc: 'Ensure that you have read the Terms & Condition before proceeding to the quiz :)',
        },
        errorPlacement: function errorPlacement(error, element) {
            if (element.attr('type') == 'checkbox') {
                element.next().after(error);
            } else if (element.attr('type') == 'radio') {
                element.parent().parent().append(error);
            } else {
                element.after(error);
            }
        },
    });

    // the following method must come AFTER .validate()
    $("input[name^='question_']").each(function() {
        $(this).rules('add', {
            required: true,
            messages: {
                required: "Oops! Seems like you missed out this question.",
            }
        });
    });

    $('.startQuizButton').click(function(e){
        e.preventDefault();
        $('html, body').animate({
            scrollTop: $("#quizBody").offset().top - 68
        }, 500);
    });

    jsSocials.shares.twitter = {
        logo: "fa fa-twitter",
        shareUrl: "https://twitter.com/share?url=" + "{{ route('events.2020.get_math_board_quiz_view') }}" + "&text=Join me for this Board Quiz and stand a chance to win Netflix Gift Cards worth RM50 now!",
    };

    jsSocials.shares.facebook = {
        logo: "fa fa-facebook",
        shareUrl: "https://www.facebook.com/share.php?u="+ "{{ route('events.2020.get_math_board_quiz_view') }}" +"&title=Join me for this Board Quiz and stand a chance to win Netflix Gift Cards worth RM50 now!",
    };

    jsSocials.shares.whatsapp = {
        logo: "fa fa-whatsapp",
        shareUrl: "https://api.whatsapp.com/send?text=Join me for this Board Quiz and stand a chance to win Netflix Gift Cards worth RM50 now! To join Board Quiz, click this link: "+ "{{ route('events.2020.get_math_board_quiz_view') }}",
    };

    $("#shareIcons").jsSocials({
        showLabel: false,
        showCount: false,
        shares: ["twitter", "facebook", "whatsapp"],
        shareIn: "popup",
    });
});
</script>
@endsection
