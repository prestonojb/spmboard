<?php

namespace App\Policies;

use App\ResourcePost;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ResourcePostPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function rate(User $user, ResourcePost $resource_post)
    {
        $classroom = $resource_post->lesson->classroom;
        return $user->student && $classroom->students->contains($user->student->user_id);
    }

    /**
     * Permission to update resource post
     */
    public function update(User $user, ResourcePost $resource_post)
    {
        $lesson = $resource_post->lesson;
        return $user->is_teacher() && $lesson->classroom->hasTeacher($user->teacher);
    }

    /**
     * Permission to delete resource post
     */
    public function delete(User $user, ResourcePost $resource_post)
    {
        $lesson = $resource_post->lesson;
        return $user->is_teacher() && $lesson->classroom->hasTeacher($user->teacher);
    }
}
