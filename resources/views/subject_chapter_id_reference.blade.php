@extends('main.layouts.app')

@section('meta')
<title>Subject Chapter ID Reference | {{ config('app.name') }}</title>
@endsection

@section('content')
<div class="container">
    <div class="py-2"></div>
    <div class="panel">
        <div class="form-group">
            <label for="">Subject ID References</label>
            <table class="table table-bordered">
                <tr>
                    <th>Subject</th>
                    <th>ID</th>
                    <th>Chapter ID Reference</th>
                </tr>
                @foreach(App\Subject::all() as $subject)
                    <tr>
                        <td>{{ $subject->exam_board_and_name }}</td>
                        <td>
                            {{ $subject->id }}
                        </td>
                        <td>
                            <button type="button" class="button--primary" data-toggle="modal" data-target="#{{ str_slug($subject->name_with_exam_board) }}_modal">View</button>
                        </td>
                    </tr>
                @endforeach
            </table>

            @foreach(App\Subject::all() as $subject)
                <div class="modal" tabindex="-1" role="dialog" id="{{ str_slug($subject->name_with_exam_board) }}_modal">
                    <div class="modal-dialog modal-xl" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">{{ $subject->exam_board_and_name }} Chapter ID References</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <table class="table table-bordered">
                                    <tr>
                                        <th>Chapter Name</th>
                                        <th>ID</th>
                                    </tr>

                                    @foreach($subject->chapters as $chapter)
                                        <tr>
                                            <td>{{ $chapter->name }}</td>
                                            <td>{{ $chapter->id }}</td>
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="button--primary" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>
@endsection
