<?php

namespace App\Http\Controllers;

use App\Classroom;
use App\ExamBoard;
use App\Question;
use App\ResourcePost;
use App\Homework;
use App\Lesson;
use App\User;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Exception;
use Illuminate\Support\Facades\Session;

class ClassroomController extends Controller
{
    public function getUniqueCode( $length = 7 )
    {
        $code = getRandomString( $length );
        $code_exists = Classroom::where('code', $code)->exists();
        if($code_exists) {
            $code = $this->getUniqueCode( $length );
        } else {
            return $code;
        }
    }

    public function getLandingView()
    {
        if(!auth()->check()) {
            return view('classroom.landing');
        }

        $user = auth()->user();
        if( $user->teacher ) {
            return redirect()->route('classroom.classrooms.index');
        } elseif( $user->student ) {
            return redirect()->route('classroom.classrooms.index');
        } elseif( $user->parent ) {
            return redirect()->route('classroom.children.index');
        }
    }

    public function index()
    {
        $this->authorize('index', Classroom::class);

        $user = auth()->user();

        $classrooms = $user->classrooms;
        $timeline_items = $user->getTimelineItems();

        return view('classroom.classrooms.index', compact('classrooms', 'timeline_items'));
    }

    public function create()
    {
        $this->authorize('create', Classroom::class);

        $user = auth()->user();
        $teacher = $user->teacher;

        $exam_boards = ExamBoard::all();
        return view('classroom.classrooms.create', compact('classroom', 'exam_boards'));
    }

    public function store()
    {
        $this->authorize('store', Classroom::class);

        // Validation
        request()->validate([
            'name' => 'required',
            'subject' => 'required',
        ]);

        // Create classroom
        $classroom = Classroom::create([
            'name' => request('name'),
            'teacher_id' => auth()->user()->id,
            'subject_id' => request('subject'),
            'code' => $this->getUniqueCode(),
        ]);

        Session::flash('success', 'Classroom '. $classroom->name .' created successfully!');
        return redirect()->route('classroom.classrooms.show', $classroom->id);
    }

    public function edit(Classroom $classroom)
    {
        $this->authorize('update', $classroom);
        return view('classroom.classrooms.edit', compact('classroom'));
    }

    public function update(Classroom $classroom)
    {
        $this->authorize('update', $classroom);

        // Validation
        request()->validate([
            'name' => 'required',
        ]);

        // Update classroom
        $classroom->update([
            'name' => request('name'),
        ]);

        Session::flash( 'success', 'Classroom updated successfully!' );
        return redirect()->route('classroom.classrooms.show', $classroom->id);
    }

    public function show(Classroom $classroom)
    {
        $this->authorize('show', $classroom);

        $classroom = $classroom ?? Classroom::where('teacher_id', auth()->user()->id )->first();
        $classroomId = $classroom->id;

        $no_of_questions_time = array();
        $no_of_resource_posts_time = array();
        $no_of_homeworks_time = array();

        $startDt = Carbon::now()->subMonths(6);
        $endDt = Carbon::now();

        $period = CarbonPeriod::create($startDt, '1 month', $endDt);

        foreach ($period as $date) {
            $month = $date->format('m');
            $year = $date->format('Y');
            // Questions
            $no_of_question = Question::whereHas('classrooms', function($q) use ($classroomId) {
                                    $q->where('classrooms.id', $classroomId);
                                })
                                ->whereMonth('created_at', $month)
                                ->whereYear('created_at', $year)
                                ->count();

            $no_of_questions_time[ $date->format('M Y') ] = $no_of_question;

            $lesson_ids = $classroom->lessons()->pluck('id')->toArray();

            // Resource Posts
            $no_of_resource_post = ResourcePost::whereIn('lesson_id', $lesson_ids)
                                    ->whereMonth('created_at', $month)
                                    ->whereYear('created_at', $year)
                                    ->count();

            $no_of_resource_posts_time[ $date->format('M Y') ] = $no_of_resource_post;


            // Homework
            $no_of_homework = Homework::whereIn('lesson_id', $lesson_ids)
                                    ->whereMonth('created_at', $month)
                                    ->whereYear('created_at', $year)
                                    ->count();

            $no_of_homeworks_time[ $date->format('M Y') ] = $no_of_homework;
        }

        return view('classroom.dashboard', compact('classroom', 'no_of_questions_time', 'no_of_resource_posts_time', 'no_of_homeworks_time'));
    }

    /**
     * Generate Classroom Calendar
     */
    public function calendar()
    {
        $this->authorize('view_calendar', Classroom::class);
        $user = auth()->user();
        // Redirect if user is parent
        if( $user->is_parent() ) {
            return redirect()->back()->with('error', 'Calendar is not supported for parent account.');
        }

        // Set date boundaries
        // Lower bound: -11 months, 3 weeks
        // Upper bound: +11 months, 3 weeks
        $from = Carbon::now()->subYear();
        $to = Carbon::now()->addYear();

        $lb_date = Carbon::now()->subYear()->addWeek();
        $ub_date = Carbon::now()->addYear()->subWeek();

        // Get Class selected and user classrooms
        $class_selected = request()->has('class_selected') ? Classroom::find(request('class_selected'))
                                                           : null;
        $classrooms = $user->classrooms;
        // Get Schedule Data (lessons, homeworks)
        $lessons = $user->account_variant->getLessons($class_selected)->whereBetween('start_at', [$from.' 00:00:00', $to.' 23:59:59']);
        $homeworks = $user->account_variant->getHomeworks($class_selected)->whereBetween('due_at', [$from.' 00:00:00', $to.' 23:59:59']);

        // dd($lessons);
        return view('classroom.calendar', compact('classrooms', 'class_selected', 'lessons', 'homeworks', 'lb_date', 'ub_date'));
    }

    public function custom_file_upload()
    {
        // Validation
        request()->validate([
            'custom_file' => 'required|mimes:pdf|max:10000',
        ]);
        $file = request()->file('custom_file');
        //Store image to S3
        $path = $file->store('custom_resources/', 's3');

        $filename = $file->hashName();
        //Save image url in database
        $file_url = 'custom_resources/'. $filename;

        return $file_url;
    }
}
