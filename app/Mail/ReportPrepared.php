<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ReportPrepared extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct( $report, $pdf )
    {
        $this->report = $report;
        $this->pdf = $pdf;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $report = $this->report;
        return $this->markdown('classroom.email.teacher.report_prepared')
                     ->from( $report->teacher->email )
                     ->with([
                        'report' => $report,
                     ])
                     ->attachData($this->pdf->output(), 'Report #'.$report->id.'.pdf', [
                        'mime' => 'application/pdf',
                    ]);
    }
}
