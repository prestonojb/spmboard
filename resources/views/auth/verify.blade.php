@extends('forum.layouts.app')

@section('meta')
<title>Verify Email Address | {{ config('app.name') }}</title>
@endsection

@section('content')
@include('main.navs.header')

<div class="py-2"></div>

<div class="wrapper">
    <h1 class="text-center heading mb-3">Verify Email Address</h1>

    <div class="text-center">
        <p class="mb-2">{{ __('A verification email has been sent to your email address, please click on the link provided in the email to verify your email address') }}.</p>
        <p><strong>{{ __('If you did not receive the email') }}, you may try to request another email by clicking the button below.</p>

        <form method="POST" action="{{ route('verification.resend') }}">
            @csrf
            <button type="submit" class="button--primary resendEmailButton">
                {{ __('Request another email') }}
            </button>
        </form>
    </div>

</div>

<div class="py-2"></div>

@endsection

@section('scripts')
<script>
$(function(){
    @if(session('resent'))
        toastr.success('Email resent successfully!');
    @endif
    $('.resendEmailButton').click(function(e){
        e.preventDefault();
        $(this).attr('disabled', true);
        $(this).closest('form').submit();
    });
});
</script>
@endsection
