@extends('forum.layouts.app')

@section('meta')
<title>Verify email address | {{ config('app.name') }}</title>
@endsection

@section('content')
@include('main.navs.header')
<div class="wrapper">
    <div class="text-center">
        <h1 class="heading">Forgot Password?</h1>
        <p class="sub-heading mb-3">Fret not, we got you!</p>
    </div>

    <form method="POST" action="{{ route('password.email') }}">
        @csrf
        <div class="form-field">
            <label>{{ __('Email Address') }}</label>
            <input type="email" name="email" required autofocus autocomplete="email" placeholder="e.g. hello@spmboard.com">

            @error('email')
                <div class="error-message" role="alert">
                    {{ $message }}
                </div>
            @enderror
        </div>

        <div class="text-center">
            <button type="submit" class="button--primary">{{ __('Send Me A Link') }}</button>
        </div>

    </form>


</div>

@endsection
