@component('mail::message')
# Error Feedback

@component('mail::panel')
{{ $feedback }}
@endcomponent

@endcomponent
