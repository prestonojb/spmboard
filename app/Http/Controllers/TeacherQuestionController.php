<?php

namespace App\Http\Controllers;

use App\Classroom;
use App\Http\Requests\Question\UpdateRequest;
use App\Notifications\ClassroomQuestionAsked;
use App\Notifications\QuestionAsked;
use App\Question;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

class TeacherQuestionController extends Controller
{
    public function index(Classroom $classroom)
    {
        $questions = $classroom->questions()->orderByDesc('created_at')->get();
        $chapters = $classroom->subject->chapters;
        return view('classroom.questions.index', compact('classroom', 'questions', 'chapters'));
    }

    public function show(Classroom $classroom, Question $question)
    {
        $answers = $question->get_answers_from_classroom( $classroom );
        return view('classroom.questions.show', compact('classroom', 'question', 'answers'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Classroom $classroom, Question $question)
    {
        $this->authorize('delete', $question);

        if($question->img){
            if(strpos($question->img, 'amazonaws.com')){
                //Delete file from S3
                $img_dir = substr($question->img, strpos($question->img, 'questions/'));
                Storage::cloud()->delete($img_dir);

            } else {
                //Delete file from local
                $img_path = storage_path('app/public/questions/'. $question->img);
                unlink($img_path);
            }
        }

        $answers = $question->answers;

        foreach($answers as $answer){

            if($answer->img){
                if(strpos($answer->img, 'amazonaws.com')){
                    //Delete file from S3
                    $img_dir = substr($answer->img, strpos($answer->img, 'answers/'));
                    Storage::cloud()->delete($img_dir);
                } else {
                    //Delete file from local
                    $img_path = storage_path('app/public/answers/'. $answer->img);
                    unlink($img_path);
                }
            }

        }

        $question->delete();

        Session::flash('error', 'Question deleted successfully!');
        return redirect()->route('classroom.questions.index', $classroom);

    }
}
