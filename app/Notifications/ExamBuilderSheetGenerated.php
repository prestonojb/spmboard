<?php

namespace App\Notifications;

use App\ExamBuilderSheet;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class ExamBuilderSheetGenerated extends Notification implements ShouldQueue
{
    use Queueable;

    public $sheet;
    public $is_qp;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(ExamBuilderSheet $sheet, $is_qp)
    {
        $this->sheet = $sheet;
        $this->is_qp = $is_qp;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    // /**
    //  * Get the mail representation of the notification.
    //  *
    //  * @param  mixed  $notifiable
    //  * @return \Illuminate\Notifications\Messages\MailMessage
    //  */
    // public function toMail($notifiable)
    // {
    //     return (new MailMessage)
    //                 ->line('The introduction to the notification.')
    //                 ->action('Notification Action', url('/'))
    //                 ->line('Thank you for using our application!');
    // }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $sheet_type = $this->is_qp ? 'Question Paper' : 'Mark Scheme';
        return [
            'type' => ExamBuilderSheet::class,
            'target' => $this->sheet,
            'title' => "<b>{$this->sheet->name}</b> ({$sheet_type}) is now available!",
            'is_qp' => $this->is_qp,
        ];
    }
}
