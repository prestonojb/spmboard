@component('mail::message')
# Account Credentials
## Your Board {{ $user->account_type }} account has been created with the following credentials:

@component('mail::panel')

**Username**: {{ $user->username }}

**Email**: {{ $user->email }}

**Password**: {{ $password }}

@endcomponent


@component('mail::button', ['url' => url('login'), 'color' => 'primary'])
Log In Now
@endcomponent


As a security measure, please reset your password with this <a href="{{ url('password/reset') }}">link</a> for your personal password.


Happy learning!


Regards,<br>
The Board Team


**This email is auto-generated. If you need support, please email to hello@boardedu.org.*

@endcomponent
