<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Product extends Model
{
    /**
     * ------------
     * ACCESSORS
     * ------------
     */
    /**
     * Returns price with currency
     * @return string
     */
    public function getPriceWithCurrencyAttribute()
    {
        return "{$this->currency} {$this->price}";
    }

    public function getInvolvesCashAttribute()
    {
        return $this->price != 0;
    }
}
