@php
    if($is_edit) {
        $title = old('title') ?? $article->title;
        $description = old('description') ?? $article->description;
        $body = old('body') ?? $article->body;
        $cover_img_url = old('cover_img') ?? $article->cover_img;
        $tags = old('tags') ?? $article->tags_comma_delimited_str;

        $url = route('users.articles.update', $article->id);
    } else {
        $title = old('title');
        $description = old('description');
        $body = old('body');
        $tags = old('tags');

        $url = route('users.articles.store');
    }
@endphp

<form method="POST" action="{{ $url }}" class="" enctype="multipart/form-data">
    @csrf
    @if($is_edit)
        @method('PATCH')
    @endif

    {{-- Title --}}
    <div class="form-field">
        <label>Title</label>
        <input name="title" placeholder="Title" value="{{ $title }}" required autocomplete="off">
        @error('title')
            <span class="error-message">{{ $message }}</span>
        @enderror
    </div>

    {{-- Description --}}
    <div class="form-field">
        <label>Description</label>
        <textarea name="description" placeholder="Description" required autocomplete="off">{{ $description }}</textarea>
        @error('description')
            <span class="error-message">{{ $message }}</span>
        @enderror
    </div>

    {{-- Body --}}
    <div class="form-group">
        <label for="">Body</label>
        <div class="loading-icon loading in-editor blue"></div>
        <textarea class="article-tinymce-editor" name="body">{!! $body !!}</textarea>
        @error('body')
            <div class="error-message">{{ $message }}</div>
        @enderror
    </div>

    {{-- Tags --}}
    <div class="form-field">
        <label>Tags</label>
        <input class="tags-input articleTagsInput" name="tags" placeholder="Tags" value="{{ $tags }}" autocomplete="off">
        @error('tags')
            <div class="error-message">{{ $message }}</div>
        @enderror
    </div>

    {{-- Image --}}
    @if($is_edit)
        <div class="form-field">
            <label for="">Cover Image</label>
            @if(!is_null($article->cover_img))
                <img class="file-input-preview" src="{{ $cover_img_url }}">
            @else
                <p class="py-1 gray2">No image</p>
            @endif
        </div>
    @else
        <div class="form-field">
            <label>Cover Image <span style="font-weight: 400">(Optional)</span></label>
            <div class="file-drop-area">
                <span class="fake-btn">Choose files</span>
                <span class="file-msg">or drag and drop files here</span>
                <input class="file-input" name="img" type="file">
            </div>
            <img class="file-input-preview">
            @error('img')
                <span class="error-message">{{ $message }}</span>
            @enderror
        </div>
    @endif

    <div class="text-center">
        <button type="submit" class="w-250px button--primary loadOnSubmitButton">{{ $is_edit ? 'Update' : 'Post' }}</button>
    </div>
</form>
