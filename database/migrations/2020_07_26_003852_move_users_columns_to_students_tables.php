<?php

use App\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class MoveUsersColumnsToStudentsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $users = User::all();
        foreach($users as $user) {
            DB::table('students')->insert([
                'user_id' => $user->id,
                'coin_balance' => $user->coin_balance ?? 0,
                'reward_point_balance' => $user->reward_point_balance ?? 0,
                'exam_board_id' => $user->exam_board_id,
                'school' => $user->school,
            ]);
        }

        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn([
                'reward_point_balance',
                'coin_balance',
                'exam_board_id',
                'race',
                'school'
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->unsignedInteger('reward_point_balance')->default(0);
            $table->unsignedInteger('coin_balance')->default(0);
            $table->unsignedInteger('exam_board_id')->nullable();

            $table->string('race')->nullable();
            $table->string('school')->nullable();
        });
    }
}
