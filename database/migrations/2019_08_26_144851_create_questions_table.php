<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::enableForeignKeyConstraints();

        Schema::create('questions', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->string('question')->unique();
            $table->text('description')->nullable();
            $table->unsignedInteger('subject_id');
            $table->string('img')->nullable();
            $table->string('slug');
            $table->unsignedInteger('score')->default(100);
            $table->timestamps();
        });

        // Full Text Index
        DB::statement('ALTER TABLE questions ADD FULLTEXT fulltext_index (question, description)');

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('questions');
    }
}
