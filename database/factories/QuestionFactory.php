<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Question;
use Faker\Generator as Faker;

$factory->define(Question::class, function (Faker $faker) {
    return [
        'question' => $faker->sentence,
        'description' => $faker->sentence,
        'subject_id' => mt_rand(1, 5),
        // 'chapter_id' => 1
    ];
});
