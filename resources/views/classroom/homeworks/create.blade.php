@extends('classroom.layouts.app')

@section('styles')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.1.2/css/tempusdominus-bootstrap-4.min.css">
@endsection

@section('meta')
    <title>Give Homework | {{ config('app.name') }} Classroom</title>
@endsection

@section('content')
@component('classroom.common.breadcrumbs')
    <a href="{{ route('classroom.classrooms.show', $classroom->id) }}">Dashboard</a> /
    <a href="{{ route('classroom.lessons.index', $classroom->id) }}">Lessons</a> /
    <a href="{{ route('classroom.lessons.show', [$lesson->classroom->id, $lesson->id]) }}">{{ $lesson->name }}</a> /
    Give Homework
@endcomponent

@component('classroom.common.page_heading', ['title' => 'Give Homework'])
@endcomponent

<div class="maxw-740px mx-auto my-5">
    <div class="panel mb-2">
        <form action="{{ route('classroom.lessons.homeworks.store', $lesson->id) }}" method="POST" id="postHomeworkForm">
            @csrf
            <input type="hidden" name="resource_data">
            <input type="hidden" name="custom_resource_data">

            <div class="form-field">
                <label for="">Lesson</label>
                <input type="text" value="{{ $lesson->name }}" disabled>
            </div>

            <div class="form-field">
                <label>Title</label>
                <input type="text" name="title" placeholder="Title" value="{{ old('title') }}">
                @error('title')
                    <span class="error-message">{{ $message }}</span>
                @enderror
            </div>

            <div class="form-field">
                <label>Instruction(Optional)</label>
                <textarea name="Instruction" id="" rows="3" placeholder="Instruction">{{ old('instruction') }}</textarea>
                @error('Instruction')
                    <span class="error-message">{{ $message }}</span>
                @enderror
            </div>

            <div class="form-field">
                <label for="">Due at(Optional)</label>
                <input type="text" name="due_at" class="datetimepicker-input" id="dueAtDtPicker" value="{{ old('due_at') }}" data-toggle="datetimepicker" data-target="#dueAtDtPicker"/>
            </div>

            <div class="form-field">
                <label>Total Marks(Optional)</label>
                <input name="max_marks" type="number" placeholder="Maximum Marks" value="{{ old('max_marks') }}">
                @error('max_marks')
                    <span class="error-message">{{ $message }}</span>
                @enderror
                <p class="gray2">Leave total marks empty if this homework will not be graded.<br>
                    Range of total marks: 1-500 marks
                </p>
            </div>

            <div class="d-flex align-items-center justify-content-start mb-3">
                <input type="checkbox" name="notify_parents" id="notify_parents" {{ old('notify_parents') ? 'checked' : '' }}>
                <label for="notify_parents">{{ __('Notify parents') }}</label>
            </div>

            <div class="file-block-container">
                <div class="row">
                </div>
            </div>

            <div class="d-flex justify-content-between align-items-center">
                <div class="dropdown">
                    <button type="button" class="button--primary--inverse" data-toggle="dropdown">Add</button>
                    <div class="dropdown-menu">
                        <button type="button" class="dropdown-item" data-toggle="modal" data-target=".prebuilt-pp-modal">Past Papers</button>
                        <button type="button" class="dropdown-item" data-toggle="modal" data-target=".prebuilt-notes-modal">Notes</button>
                        <button type="button" class="dropdown-item" data-toggle="modal" data-target=".prebuilt-tpp-modal">Topical Past Papers</button>
                        <button type="button" class="dropdown-item" id="customFileUploadButton">Custom File</button>
                    </div>
                </div>
                <button type="submit" class="button--primary disableOnSubmitButton">Post</button>
            </div>
        </form>

        <form id="customFileForm" enctype="multipart/form-data" method="POST">
            @csrf
            <input type="file" name="custom_file" hidden>
        </form>
    </div>
</div>

@include('classroom.common.prebuilt_resources_modal')

<div class="d-none">
    <div class="col-12 col-md-6 fileBlockCol">
        @component('classroom.common.file_block', ['deletable' => true])
        @endcomponent
    </div>
</div>
@endsection

@section('scripts')
<script>
var customFileUploadUrl = "{{ route('classroom.custom_file_upload') }}";
var storageUrl = "{{ Storage::url('/') }}";
storageUrl = storageUrl.slice(0, -1);
$(function(){
    var minDate = moment();
    $('#dueAtDtPicker').datetimepicker({
        useCurrent: false,
        format: 'DD/MM/YYYY hh:mm A',
        minDate: minDate,
    });
});
</script>
@endsection
