<div class="panel p-0">
    <div class="panel-header">
        {{ $title }}
    </div>
    <div class="panel-body p-0">
        @if(count($resources ?? []))
            @foreach($resources as $resource)
                <div class="position-relative @if(!$loop->last) border-bottom @endif white-hoverable p-2">
                    <a href="{{ $resource->url }}" class="stretched-link"></a>

                    @if( !is_null($resource->type) )
                        <div class="px-3">
                            {{-- @switch($resource->type)
                                @default --}}
                                <div class="icon-circle font-size7 bg-blue5 white">
                                    <span class="iconify" data-icon="ion:cube-outline" data-inline="false"></span>
                                </div>
                                {{-- @endswitch --}}
                            </div>
                        </div>
                    @endif

                    <h4 class="font-size3 font-weight500 mb-1">{{ $resource->primary }}</h4>
                    <p class="font-size1 gray2 mb-0">{{ $resource->secondary }}</p>
                </div>
            @endforeach
        @else
            <div class="text-center py-2">No resource</div>
        @endif
    </div>
</div>
