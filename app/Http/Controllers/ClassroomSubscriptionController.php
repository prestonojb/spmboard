<?php

namespace App\Http\Controllers;

use App\Classroom;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Stripe\Stripe;

class ClassroomSubscriptionController extends Controller
{
    public $stripe;

    public function __construct()
    {
        Stripe::setApiKey(config('services.stripe.secret'));
        $this->stripe = new \Stripe\StripeClient(
                            config('services.stripe.secret')
                        );
    }

    // Gets Stripe Promo Code object, PROMO CODE NEEDS TO BE UNIQUE
    public function getStripePromoCode($promo_code)
    {
        return $this->stripe->promotionCodes->all(['code' => $promo_code])->data[0];
    }

    public function show()
    {
        $this->authorize('view_classroom_billing', Classroom::class);

        $user = auth()->user();
        $stripe = $this->stripe;

        $active_price = null;
        $next_billing_date = null;

        if( $user->is_teacher() ) {
            // Get user active price
            if( $user->subscribed('premium') ) {
                $active_price_id = $user->subscription('premium')->stripe_plan;
                $active_price = $stripe->prices->retrieve(
                    $active_price_id
                );
                // Get next billing date
                $next_billing_date_timestamp = $user->subscription('premium')->asStripeSubscription()->current_period_end;
                $next_billing_date = Carbon::createFromTimeStamp( $next_billing_date_timestamp );
            }
        } else {
            return redirect()->back()->with('error', 'You are not eligible to subscribe to Board Premium with your'. auth()->user()->account_type .' account.');
        }

        return view('classroom.subscriptions.show', compact('active_price', 'next_billing_date'));
    }

    public function store()
    {
        $this->authorize('view_classroom_billing', Classroom::class);

        // Validation
        request()->validate([
            'payment_method' => 'required',
            'promo_code' => 'nullable'
        ]);

        $user = auth()->user();
        $price_id = request()->price;

        // **NOTE: Promo Codes MUST be UNIQUE across the entire application**
        // Retrieve coupon associated with Promo Code from Stripe
        if( isset(request()->promo_code) ) {
            try {
                $coupon_id = $this->getStripePromoCode(request()->promo_code)->coupon->id;
            } catch(Exception $e) {
                // Returns error if there is no Promo Code that matches any coupon
                return redirect()->back()->withInput()->withErrors(['promo_code' => 'The Promo Code provided is invalid!']);
            }
        }

        $payment_method = request()->payment_method;

        if( $user->is_teacher() ) {
            if( $user->subscribed('premium') ) {
                // Allow upgrade by proration(handled by Stripe)
                $user->subscription('premium')->swapAndInvoice( $price_id );
            } else {
                if( isset($coupon_id) && !is_null($coupon_id) ) {
                    // Subscribe teacher to classroom plan with valid coupon
                    $user->newSubscription('premium', $price_id)->withCoupon( $coupon_id )->create( $payment_method );
                } else {
                    $user->newSubscription('premium', $price_id)->create( $payment_method );
                }
            }
        } else {
            return redirect()->back()->with('error', 'You are not eligible to subscribe to Board Premium package with your'. auth()->user()->account_type .' account.');
        }

        return redirect()->back()->withSuccess('You have subscribed successfully!');
    }

    public function cancel()
    {
        $this->authorize('view_classroom_billing', Classroom::class);
        $user = auth()->user();
        $user->subscription('premium')->cancelNow();

        return redirect()->back()->with('error', 'Your subscription has been cancelled successfully!');
    }
}
