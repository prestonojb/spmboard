@extends('main.layouts.app')

@section('meta')
<title>Author - {{ $blog_author->name }} | {{ config('app.name') }}</title>
@endsection

@section('content')
<section class="container">
    <section class="row">
        <div class="col-12 p-2">
            <div class="panel">
                <div class="d-flex mb-1">
                    <img class="blog-author__avatar profile-img mr-3" src="{{ $blog_author->img }}">
                    <div class="d-flex flex-column mt-2 px-2">
                        <div class="d-flex">
                            <h1 class="d-inline-block m-0 mb-1 mr-2">{{ $blog_author->name }}</h1>
                        </div>
                        <h2 class="h4">{{ $blog_author->description }}</h3>
                    </div>
                </div>

                @if($blogs->count())
                    <div class="recommended-articles">
                        <h6 class="h3 mb-2">Articles by {{ $blog_author->name }}</h6>
                        <div class="row">
                            @foreach($blogs as $blog)
                                @include('blog.common.block')
                            @endforeach
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </section>
</section>
@endsection

