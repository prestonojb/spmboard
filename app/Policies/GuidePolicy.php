<?php

namespace App\Policies;

use App\Guide;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class GuidePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Edit guide
     */
    public function edit(?User $user, Guide $guide)
    {
        if(!is_null($user)) {
            return $user->isAdmin();
        }
        return false;
    }

    /**
     * Delete guide
     */
    public function delete(?User $user, Guide $guide)
    {
        if(!is_null($user)) {
            return $user->isAdmin();
        }
        return false;
    }
}
