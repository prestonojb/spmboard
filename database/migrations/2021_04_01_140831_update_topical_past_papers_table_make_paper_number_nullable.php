<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateTopicalPastPapersTableMakePaperNumberNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('topical_past_papers', function (Blueprint $table) {
            $table->unsignedInteger('paper_number')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('topical_past_papers', function (Blueprint $table) {
            $table->unsignedInteger('paper_number')->change();
        });
    }
}
