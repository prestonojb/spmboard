@extends('forum.layouts.app')

@section('meta')
<title>404 - Page not found | {{ config('app.name') }}</title>
@endsection

@section('content')

@include('main.navs.header')

<div class="error-page-container">
    <img src="{{ asset('img/essential/error_404.svg') }}" alt="error 404" class="main-img">
    <h3 class="description">Seems like you are on uncharted territory.<br>
        Do you wish to go back to the main page?</h3>
    <a href="{{ route('/') }}" class="button--primary lg link">Back to Board</a>
</div>

@endsection
