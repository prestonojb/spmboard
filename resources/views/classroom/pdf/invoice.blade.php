<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Invoice-{{ $invoice->invoice_number }} | {{ config('app.name') }}</title>

    <style>
    .invoice-box {
        max-width: 800px;
        margin: auto;
        padding: 30px;
        border: 1px solid #eee;
        box-shadow: 0 0 10px rgba(0, 0, 0, .15);
        font-size: 16px;
        line-height: 24px;
        font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
        color: #555;
    }

    .invoice-box table {
        width: 100%;
        line-height: inherit;
        text-align: left;
    }

    .invoice-box table td {
        padding: 5px;
        vertical-align: top;
    }

    .invoice-box table tr td:nth-child(2) {
        text-align: right;
    }

    .invoice-box table tr.top table td {
        padding-bottom: 20px;
    }

    .invoice-box table tr.top table td.title {
        font-size: 45px;
        line-height: 45px;
        color: #333;
    }

    .invoice-box table tr.information table td {
        padding-bottom: 40px;
    }

    .invoice-box table tr.heading td {
        background: #eee;
        border-bottom: 1px solid #ddd;
        font-weight: bold;
    }

    .invoice-box table tr.details td {
        padding-bottom: 20px;
    }

    .invoice-box table tr.item td{
        border-bottom: 1px solid #eee;
    }

    .invoice-box table tr.item.last td {
        border-bottom: none;
    }

    .invoice-box table tr.total td:nth-child(2) {
        border-top: 2px solid #eee;
        font-weight: bold;
    }

    @media only screen and (max-width: 600px) {
        .invoice-box table tr.top table td {
            width: 100%;
            display: block;
            text-align: center;
        }

        .invoice-box table tr.information table td {
            width: 100%;
            display: block;
            text-align: center;
        }
    }

    /** RTL **/
    .rtl {
        direction: rtl;
        font-family: Tahoma, 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
    }

    .rtl table {
        text-align: right;
    }

    .rtl table tr td:nth-child(2) {
        text-align: left;
    }

    .text-right {
        text-align: right;
    }

    .bordered-table {
        border-collapse: collapse;
    }

    table.lesson-table, .bordered-table th, .bordered-table td {
        border: 1px solid #eee;
    }
    .mb-0 {
        margin-bottom: 0;
    }
    .mb-1 {
        margin-bottom: 4px;
    }
    </style>
</head>

<body>
    <div class="invoice-box">
        <table cellpadding="0" cellspacing="0">
            <tr class="top">
                <td colspan="4">
                    <table>
                        <tr>
                            <td colspan="3" class="title">
                                <img src="{{ asset('img/logo/logo_light.png') }}" style="width: 250px;">
                            </td>

                            <td>
                                <p class="mb-0">Invoice Number: {{ $invoice->invoice_number }}</p>
                                <p class="mb-0">Date Issued: {{ $invoice->created_at->toFormattedDateString() }}</p>
                                <p class="mb-0">Date Due: {{ $invoice->due_at->toFormattedDateString() }}</p>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>

            <tr class="information" style="margin-bottom: 8px;">
                <td>
                    From: {{ $invoice->teacher->name }}<br>
                    {{ $invoice->teacher->email }}
                </td>

            </tr>

            <tr>
                <td>
                    Bill to: {{ $invoice->student->name }}<br>
                    {{ $invoice->student->email }}
                </td>
            </tr>

            <tr>
                <td colspan="4">
                    <table class="bordered-table">
                        <tr class="heading">
                            <td>Item</td>
                            <td>Qty</td>
                            <td>Unit Price(MYR)</td>
                            <td class="text-right">Price(MYR)</td>
                        </tr>

                        @if($invoice->no_of_lessons)
                            <tr class="item">
                                <td>Lesson(per hour)</td>
                                <td>{{ $invoice->lesson_quantity }}</td>
                                <td>{{ $invoice->lesson_price_per_unit }}</td>
                                <td class="text-right">{{ $invoice->amount }}</td>
                            </tr>
                        @endif
                        @foreach( $invoice->non_lesson_items as $item )
                            <tr class="item">
                                <td>{{ $item->title }}</td>
                                <td>{{ $item->no_of_units }}</td>
                                <td>{{ $item->price_per_unit }}</td>
                                <td class="text-right">{{ $invoice->amount }}</td>
                            </tr>
                        @endforeach

                        <tr class="total">
                            <td colspan="3">Total</td>
                            <td>{{ $invoice->amount }}</td>
                        </tr>
                    </table>
                </td>
            </tr>

            <tr>
                <td>
                    <span style="font-size: 12px;">Powered by Board</span>
                </td>
            </tr>
        </table>
    </div>
</body>
</html>
