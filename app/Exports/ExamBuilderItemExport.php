<?php

namespace App\Exports;

use App\ExamBuilderItem;
use App\Subject;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Illuminate\Support\Facades\Schema;

class ExamBuilderItemExport implements FromQuery, WithHeadings
{
    use Exportable;

    public function forSubject(Subject $subject)
    {
        $this->subject = $subject;
        return $this;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function query()
    {
        return ExamBuilderItem::query()->select(['name', 'question_imgs', 'answer_imgs'])->where('subject_id', $this->subject->id);
    }

    /**
     * Table columns for export
     */
    public function headings():array
    {
        return ['name', 'question_imgs', 'answer_imgs'];
    }
}
