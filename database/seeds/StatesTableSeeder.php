<?php

use App\State;
use Illuminate\Database\Seeder;

class StatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $states = array(
            0 => array('id' => '1', 'name' => 'Johor'),
            1 => array('id' => '2', 'name' => 'Kedah'),
            2 => array('id' => '3', 'name' => 'Kelantan'),
            3 => array('id' => '4', 'name' => 'Kuala Lumpur'),
            4 => array('id' => '5', 'name' => 'Melaka'),
            5 => array('id' => '6', 'name' => 'Negeri Sembilan'),
            6 => array('id' => '7', 'name' => 'Pahang'),
            7 => array('id' => '8', 'name' => 'Perlis'),
            8 => array('id' => '9', 'name' => 'Pulau Pinang'),
            9 => array('id' => '10', 'name' => 'Sabah'),
            10 => array('id' => '11', 'name' => 'Sarawak'),
            11 => array('id' => '12', 'name' => 'Selangor'),
            12 => array('id' => '13', 'name' => 'Terengganu'),
        );

        foreach ($states as $value) {

            $data = array(
                'id' => $value['id'],
                'name' => $value['name'],
            );

            $data_exists = State::find($value['id']);
            if (!is_null($data_exists)) {
                $data['updated_at'] = date("Y-m-d H:i:s");
                State::where('id', $value['id'])
                        ->update($data); //UPDATE
            } else {
                $data['created_at'] = date("Y-m-d H:i:s");
                $data['updated_at'] = date("Y-m-d H:i:s");
                State::create($data); //CREATE
            }
        }
    }
}
