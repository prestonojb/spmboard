<?php

namespace App\Notifications\Classroom;

use App\ClassroomPost;
use App\Student;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\HtmlString;

class NewClassroomPostAdded extends Notification implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * Must declare variables for queued notifications to work
     * Reference: https://stackoverflow.com/a/61973744
     */
    public $student;
    public $post;
    public $classroom;
    public $teacher;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Student $student, ClassroomPost $post)
    {
        $this->student = $student;
        $this->post = $post;

        $this->classroom = $post->classroom;
        $this->teacher = $this->classroom->teacher;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $student = $this->student;
        $post = $this->post;
        $classroom = $this->classroom;
        $teacher = $this->teacher;

        if( $notifiable->account_type == 'student' ) {
            $lineHtml = "<b>{$teacher->name}</b> posted in <b>{$classroom->name}</b>.";
        } elseif( $notifiable->account_type == 'parent' ) {
            $lineHtml = "<b>{$teacher->name}</b> posted in <b>{$classroom->name}</b>. (for {$this->student->name})";
        }
        $line2Html = "<b>{$post->title}</b>";
        $line3Html = str_limit( html_entity_decode( strip_tags( $post->description ) ), 200);

        return (new MailMessage)
                    ->line(new HtmlString($lineHtml))
                    ->line(new HtmlString($line2Html))
                    ->line(new HtmlString($line3Html))
                    ->action('Read More', route('classroom.posts.index', $classroom->id));
        }

    /**
     * Get the array representation of the notification.
     * @param mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $student = $this->student;
        $post = $this->post;
        $classroom = $this->classroom;
        $teacher = $this->teacher;

        if( $notifiable->account_type == 'student' ) {
            $title = "<b>{$teacher->name}</b> posted in <b>{$classroom->name}</b>.";
        } elseif( $notifiable->account_type == 'parent' ) {
            $title = "<b>{$teacher->name}</b> posted in <b>{$classroom->name}</b> (Classroom <b>{$student->name}</b> is in}).";
        }
        return [
            'type' => ClassroomPost::class,
            'target' => $post,
            'title' => $title,
        ];
    }
}
