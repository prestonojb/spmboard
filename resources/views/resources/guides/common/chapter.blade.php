<div class="learn__chapter">
    <div>
        <span class="name">
            {{ $chapter->name }}
        </span>
        <span class="count">
            {{ count($chapter->guides) }} @if(count($chapter->guides) > 1) guides @else guide @endif
        </span>
    </div>

    <span class="arrow-icon">
        <span class="iconify" data-icon="bx:bxs-down-arrow" data-inline="false"></span>
    </span>

</div>
<div class="learn__guides">
    @foreach($chapter->guides as $guide)
        <div class="learn__guide">
            <a href="{{ route('guides.show', [$exam_board, $guide->id]) }}">{{ $guide->title }}</a>
        </div>
    @endforeach
</div>
