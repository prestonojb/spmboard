<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\BlogCategory;
use App\BlogAuthor;
use Illuminate\Support\Facades\Storage;

class Blog extends Model
{

    protected $guarded = [];

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function blog_category()
    {
        return $this->belongsTo(BlogCategory::class);
    }

    public function blog_author()
    {
        return $this->belongsTo(BlogAuthor::class);
    }

    /**
     * ---------------------
     * Accessors
     * ---------------------
     */
    /**
     * Returns blog show URL
     * @return string
     */
    public function getShowUrlAttribute()
    {
        return route('blog.show', $this->id);
    }

    public function setTitleAttribute($value)
    {
        $this->attributes['title'] = $value;
        $this->attributes['slug'] = str_slug($value);
    }

    public function getImgAttribute($value)
    {
        if($value){
            if(strpos($value, 'amazonaws')){
                //Amazon link for image
                return $value;
            } else{
                //Amazon S3
                $updated_value = str_replace('\\', '/', $value);
                return Storage::cloud()->url($updated_value);
            }
        } else {
            return null;
        }
    }

}
