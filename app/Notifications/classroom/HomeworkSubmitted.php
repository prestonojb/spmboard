<?php

namespace App\Notifications\Classroom;

use App\Lesson;
use App\HomeworkSubmission;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class HomeworkSubmitted extends Notification implements ShouldQueue
{
    use Queueable;

    public $lesson;
    public $homework_submission;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Lesson $lesson, HomeworkSubmission $homework_submission)
    {
        $this->lesson = $lesson;
        $this->homework_submission = $homework_submission;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    // /**
    //  * Get the mail representation of the notification.
    //  *
    //  * @param  mixed  $notifiable
    //  * @return \Illuminate\Notifications\Messages\MailMessage
    //  */
    // public function toMail($notifiable)
    // {
    //     return (new MailMessage)
    //                 ->line('The introduction to the notification.')
    //                 ->action('Notification Action', url('/'))
    //                 ->line('Thank you for using our application!');
    // }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $lesson = $this->lesson;
        $homework_submission = $this->homework_submission;
        $homework = $homework_submission->homework;
        $student = $homework_submission->student;

        return [
            'type' => HomeworkSubmission::class,
            'target' => $homework_submission,
            'lesson_id' => $lesson->id,
            'title' => "<b>{$student->name}</b> made a submission on <b>{$homework->title}</b>.",
        ];
    }
}
