@extends('voyager::master')

@section('page_title', 'Student and Parent Import')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_header')
    <h1 class="page-title">
        Bulk Create Students and Parents
    </h1>
@stop

@section('content')
    <div class="page-content container-fluid">
        <div class="panel panel-bordered">
            <div class="panel-body">

                <form class="form-edit-add" role="form"
                    action="{{ route('voyager.users.import.student_and_parent') }}"
                    method="POST" enctype="multipart/form-data" autocomplete="off">

                    @csrf

                    <div class="form-group">
                        <label for="">Student and Parent List (Excel only)</label>
                        <input type="file" name="file" id="">
                        @error('file')
                            <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <p>Excel sheets uploaded must adhere to the Sheet Reference (refer below), else it will not work.</p>
                        <a target="_blank" href="https://docs.google.com/spreadsheets/d/1VF_CQEEpGcLrAv301krAZ22rPv9AUNAnMZiDpoDW6PI/edit?usp=sharing">Sheet Reference</a>
                    </div>

                    <button type="submit" class="btn btn-primary save">
                        {{ __('voyager::generic.save') }}
                    </button>
                </form>
            </div>
        </div>
    </div>
@stop
