<?php

namespace App\Http\Controllers;

use App\Answer;
use App\Notifications\AnswerChecked;
use App\CoinAction;
use App\RewardPointAction;
use Carbon\Carbon;
use Amplitude;

class AnswerCheckController extends Controller
{
    /**
     * Update answer check status
     */
    public function update(Answer $answer)
    {
        $this->authorize('check', $answer);

        $answerOwner = $answer->user;
        $question = $answer->question;

        // Update checked answer on DB
        if($answer->checked == 0){
            // Reset all checked values for answers associated with question
            Answer::where('question_id', $question->id)->update(['checked' => 0]);
            $answer->update([
                'checked' => 1,
                'checked_at' => Carbon::now(),
            ]);

            // Log event on Amplitude
            Amplitude::setUserId($answerOwner->id);
            Amplitude::queueEvent('accept_answer', ['question_id' => $answer->question->id, 'answer_id' => $answer->id]);

        } else{
            $answer->update([
                'checked_at' => null,
            ]);
        }

        // Reward RP if question has bounty & Question RP bounty still available
        if( $question->hasRewardPointBounty() && $question->rewardPointBountyAvailable() ) {
            // Prevent abuse of checking your own answer and receiving coins
            if($question->user->id != $answerOwner->id || auth()->user()->id != $question->user->id){
                // Add RP to user
                RewardPointAction::create([
                    'user_id' => $answerOwner->id,
                    'name' => 'Checked Answer',
                    'amount' => $question->reward_points,
                    'reward_point_actionable_id' => $answer->id,
                    'reward_point_actionable_type' => Answer::class,
                ]);

                $answerOwner->addRewardPoints($question->reward_points);

                // Update question
                $question->update([
                    'points_awarded_at' => Carbon::now(),
                ]);

                // Notify answer
                $answerOwner->notify(new AnswerChecked($answer, $question->reward_points));
                return 'Points awarded';
            }
        } else {
            $answerOwner->notify(new AnswerChecked($answer, 0));
            return;
        }
    }
}
