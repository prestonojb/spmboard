var $form = $('#teacherCreateInvoiceForm');
var $lessonTable = $form.find('.lessonTable');

function updateLessonInvoiceItem() {
    var totalDurationInHours = 0;
    // Loop through CHECKED lesson checkbox and add duration to totalDurationInHours
    $lessonTable.find('.lessonCheckbox:checked').each(function(){
        var durationInHours = parseFloat( $(this).parent().siblings('.durationInHoursData').text() );
        totalDurationInHours += durationInHours;
    });
    totalDurationInHours = Math.round( totalDurationInHours );
    // Update Lesson Invoice Item Qty field
    $('.lessonInvoiceItemRow [name=no_of_units]').val( totalDurationInHours );
}

function updateTotal() {
    $form.find('.lessonInvoiceItemRow, .invoiceItemRow').each(function(){
        var $noOfUnitsInput = $(this).find('[name=no_of_units]');
        var $pricePerUnitInput = $(this).find('[name=price_per_unit]');
        var $itemTotalPrice = $(this).find('.itemTotalPrice');

        var noOfUnits = parseInt( $noOfUnitsInput.val() || 0 );
        var pricePerUnit = parseFloat( $pricePerUnitInput.val() || 0 );
        var itemtotalPrice = noOfUnits * pricePerUnit;

        $itemTotalPrice.text( itemtotalPrice.toFixed(2) );
    });
}

function updateDom() {
    updateLessonInvoiceItem();
    updateTotal();
}

$form.find('select[name=student]').change(function(e){
    $.ajax({
        type: "post",
        url: "/s/parent",
        data: {
            student: e.target.value
        },
        success: function (response) {
            if( response ) {
                $('[name=parent_email]').val( response.email );
            } else {
                $('[name=parent_email]').val( 'No Parent found' );
            }
        }
    });
    $.ajax({
        type: "post",
        url: "/classroom/t/s/unpaid_lessons",
        data: {
            student: e.target.value
        },
        success: function (response) {
            var lessons = response;

            $lessonTable.find('tbody').html('');

            lessons.forEach(function(lesson){
                $newLessonRow = $('.lessonRowHelper').clone();
                $newLessonRow.toggleClass('lessonRowHelper lessonRow');
                $('.selectAllCheckbox').prop('checked', false);

                $newLessonRow.find('.lessonCheckbox').val( lesson.id );
                $newLessonRow.find('.lesson_name').text( lesson.name );
                $newLessonRow.find('.lesson_start_at').text( lesson.start_date );
                $newLessonRow.find('.lesson_duration_in_hours').text( lesson.duration_in_hours );

                $lessonTable.find('tbody').append( $newLessonRow );
            });
            updateDom();
        }
    });
});

$form.find('.selectAllCheckbox').change(function(e){
    var table = $(this).closest('table');
    if( $(this).is(':checked') ) {
        table.find('.lessonCheckbox').prop('checked', true);
    } else {
        table.find('.lessonCheckbox').prop('checked', false);
    }
    updateDom();
});

$form.find('.lessonTable').on('change', '.lessonCheckbox', function(){
    var $table = $(this).closest('table');
    // Boolean for ALL lesson checkboxes are checked
    var allChecked = $table.find('.lessonCheckbox:checked').length == $table.find('.lessonCheckbox').length;
    if( allChecked ) {
        $('.selectAllCheckbox').prop('checked', true);
    } else {
        $('.selectAllCheckbox').prop('checked', false);
    }
    updateDom();
});

$form.find('[name=due_in_days]').change(function(){
    var val = parseInt($(this).val());
    $(this).val( val );
});

$form.find('.addItemButton').click(function(e){
    var maxNoOfItems = 5;
    if( $('.lessonInvoiceItemRow, .invoiceItemRow').length <= maxNoOfItems ) {
        var innerHtml = $('.invoiceItemRowHelper').html();

        // Create new DOM object with constructed HTML string
        $('.invoiceItemTable tbody').append( '<tr class="invoiceItemRow">'+ innerHtml + '</tr>' );
    } else {
        if( $('.invoiceItemTableContainer .error-message').length == 0 ) {
            $('.invoiceItemTable').after(' <div class="error-message">Maximum '+ strval(maxNoOfItems) +' items can be included in an invoice.</div> ');
        }
    }
});

$form.find('.invoiceItemTable').on('click', '.deleteItemButton', function(e){
    e.preventDefault();
    var $row = $(this).parents('tr');
    $row.remove();

    $('.invoiceItemTableContainer .error-message').remove();
});

$form.find('.invoiceItemTable').on('change', 'input[name=price_per_unit]', function(e){
    var float = parseFloat($(this).val());
    var roundedFloat = Math.round( float * 100) / 100
    // Ensure that price input is 2 decimal places
    $(this).val( roundedFloat.toFixed(2) );
    updateDom();
});

$form.find('.invoiceItemTable').on('change', 'input[name=no_of_units]', function(e){
    updateDom();
});

$form.find('.invoiceItemTable').on('change', 'input', function(e){
    if( $.trim( $(this).val() ) !== '' ) {
        if( $(this).siblings('.error-message').length !== 0 ) {
            $(this).siblings('.error-message').remove();
        }
    }
});

function validateForm() {
    var lessonChecked = $('.lessonTable .lessonCheckbox:checked').length;
    var $invoiceItemRows = $form.find('.invoiceItemRow');
    if( !lessonChecked ) {
        // No lesson and no item
        if( $invoiceItemRows.length == 0 ) {
            if( !$('.invoiceItemTable').next().hasClass('error-message') ) {
                $('.invoiceItemTable').after(' <div class="error-message">Please fill up at least one invoice item!</div> ');
            }
        } else {
            // No lesson with item
            if( $('.invoiceItemTable').next().hasClass('error-message') ) {
                $('.invoiceItemTable').next().remove();
            }
        }
    }
    $invoiceItemRows.each(function() {
        $(this).find('input').each(function(){
            if( $.trim( $(this).val() ) == '' ) {
                if( $(this).siblings('.error-message').length == 0 ) {
                    $(this).after('<div class="error-message">Required</div>');
                }
            }
        });
    });
}

$form.find('[type=submit]').click(function(e){
    e.preventDefault();
    $(this).attr('disabled', true);
    // Validate all input fields
    validateForm();
    
    var invoiceItemData = [];
    if( $form.find('.invoiceItemTableContainer .error-message').length == 0 ) {
        // Lesson Invoice Item
        $('.lessonTable .lessonCheckbox:checked').each(function(){
            var invoiceItem = {};
            // Add Lesson Invoice Item to invoiceItem array
            invoiceItem.lesson_id = parseInt( $(this).val() );
            invoiceItem.title = "Lesson(per hour)";
            invoiceItem.no_of_units = 1;
            invoiceItem.price_per_unit = parseInt( $('.lessonInvoiceItemRow [name=price_per_unit]').val() );
            invoiceItemData.push( invoiceItem );
        });
        // Invoice Item
        $('.invoiceItemRow').each(function(){
            var invoiceItem = {};
            // Add Invoice Item to invoiceItem array
            invoiceItem.lesson_id = null;
            invoiceItem.title = $(this).find('[name=invoice_item_title]').val();
            invoiceItem.no_of_units = parseInt( $(this).find('[name=no_of_units]').val() );
            invoiceItem.price_per_unit = parseInt( $(this).find('[name=price_per_unit]').val() );
            invoiceItemData.push( invoiceItem );
        });
        // Submit teacher invoice items form field only if prescription Data is filled
        if( Array.isArray(invoiceItemData) && invoiceItemData.length ) {
            $('[name=teacher_invoice_items]').val( JSON.stringify( invoiceItemData ) );
        }
        $(this).closest('form').submit();
    } else {
        $(this).attr('disabled', false);
        toastr.info('Please fill up the required fields before proceeding.');
    }
});
