<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class WithdrawalRequest extends Model
{
    protected $guarded = [];

    /**
     * ============
     * RELATIONSHIPS
     * ============
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * ============
     * ACCESSORS
     * ============
     */
    /**
     * Returns status tag HTML according to status
     * @return string
     */
    public function getStatusTagHtmlAttribute()
    {
        $dict = [
            'pending' => 'yellow',
            'completed' => 'green',
            'cancelled' => 'red',
        ];
        return "<span class='status-tag {$dict[$this->status]}'>".ucfirst($this->status)."</span>";
    }

    /**
     * Returns amount transferred in USD
     * @return double
     */
    public function getAmountTransferredInUsdAttribute($value)
    {
        return number_format($value, 2);
    }

    /**
     * Returns receipt full URL
     * @return string
     */
    public function getReceiptUrlAttribute($value)
    {
        if($value){
            if(strpos($value, 'amazonaws')){
                //Amazon link for image
                return $value;
            } else{
                //Amazon S3
                $updated_value = str_replace('\\', '/', $value);
                return Storage::cloud()->url($updated_value);
            }
        } else {
            return null;
        }
    }

    /**
     * ============
     * HELPERS
     * ============
     */
    /**
     * Saves receipt on DB and S3
     * @param UploadedFile $file
     */
    public function saveReceipt($file)
    {
        //Store file to S3
        $path = $file->store('withdrawal_requests/receipts/', 's3');
        $filename = $file->hashName();
        //Save file url in database
        $receipt_url = 'withdrawal_requests/receipts/'. $filename;
        $this->update([
            'receipt_url' => $receipt_url,
        ]);
    }

    /**
     * Returns true if withdrawal request is completed, else false
     * @return boolean
     */
    public function isCompleted()
    {
        return $this->status == 'completed';
    }

    /**
     * Returns true if withdrawal request is complete, else false
     * @return boolean
     */
    public function isCancelled()
    {
        return $this->status == 'cancelled';
    }
}
