<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Question;
use App\Answer;
use App\Notifications\AnswerRecommended;
use App\Notifications\QuestionAnswered;
use App\Rules\QuillEmpty;
use Illuminate\Support\Facades\Storage;
use App\RewardPointAction;
use Amplitude;

class AnswerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->only(['store']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Question $question)
    {
        $this->authorize('create', Answer::class);
        $request->validate([
            'answer' => ['required', new QuillEmpty],
            'img' => 'nullable|mimes:jpeg,png,jpg,gif,svg'
        ]);
        $user = auth()->user();

        $answer = new Answer;
        $answer->question_id = $question->id;
        $answer->user_id = auth()->user()->id;
        $answer->answer = $request->answer;

        if($request->has('img')){

            //Current Month and Year
            $my = date('FY', time());

            $img = $request->file('img');

            //Store image to S3
            $path = $img->store('answers/'. $my. '/', 's3');
            $filename = $img->hashName();
            //Save image url in database
            $file_url = 'answers/'. $my. '/'. $filename;
            $answer->img = $file_url;

        }

        $answer->save();

        // Notify question owner that question is answered
        if(auth()->user()->id != $question->user->id){
            $question->user->notify(new QuestionAnswered($answer));
        }

        // Log event on Amplitude
        Amplitude::setUserId($user->id);
        Amplitude::queueEvent('answer_question', ['question_id' => $question->id, 'answer_id' => $answer->id]);

        return back()->withSuccess('Answer uploaded successfully!');
    }

    public function edit(Answer $answer)
    {
        $exam_board = $answer->question->subject->exam_board;
        return view('forum.answers.edit', compact('exam_board', 'answer'));
    }

    public function update(Answer $answer)
    {
        request()->validate([
            'answer' => ['required', new QuillEmpty],
        ]);

        $answer->update([
            'answer' => request('answer'),
        ]);

        $question = $answer->question;
        return redirect('/'.$question->subject->exam_board->slug.'/questions/'.$question->id.'/'.$question->slug)->withSuccess('Answer updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Answer  $answer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Answer $answer)
    {
        if($answer->img){
            if(strpos($answer->img, 'amazonaws.com')){
                //Delete file from S3
                $img_dir = substr($answer->img, strpos($answer->img, 'answers/'));
                Storage::cloud()->delete($img_dir);

            } else {
                //Delete file from local
                $file_path = storage_path('app/public/answers/'. $answer->img);
                unlink($file_path);
            }
        }

        $answer->delete();

        return back()->withError('Answer deleted');
    }

    public function recommend(Answer $answer)
    {
        $this->authorize('recommend', $answer);

        // Moderator
        $mod = auth()->user();

        if(!$answer->recommended) {
            // Only reward users RP for answer recommended for the first time
            if ( is_null($answer->recommended_by_user_id) && is_null($answer->recommended_at) ) {
                $reward_amount = 20; // How much is awarded for a recommended answer

                $answer->user->addRewardPoints( $reward_amount );

                // Track reward point transaction
                RewardPointAction::create([
                    'user_id' => $answer->user->id,
                    'name' => 'Recommended Answer',
                    'reward_point_actionable_type' => Answer::class,
                    'reward_point_actionable_id' => $answer->id,
                    'amount' => $reward_amount,
                ]);

                $answer->recommended_by_user_id = $mod->id;
                $answer->recommended_at = now();

                $answer->save();

                // Notify user about his/her RP!
                $answer->user->notify(new AnswerRecommended($answer));
            } else {
                $answer->recommended_by_user_id = $mod->id;
                $answer->recommended_at = now();

                $answer->save();
            }

            return 'recommended';
        } else {
            $answer->recommended_by_user_id = $mod->id;
            $answer->recommended_at = null;

            $answer->save();

            return 'unrecommended';
        }
    }

}
