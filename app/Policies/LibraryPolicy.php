<?php

namespace App\Policies;

use App\Library;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class LibraryPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Permission to list library
     */
    public function index(User $user)
    {
        return $user->is_teacher() || $user->is_student();
    }

    /**
     * Permission to create/show library
     */
    public function create_store(User $user)
    {
        return $user->is_teacher() || $user->is_student();
    }

    /**
     * Permission to edit/update library
     */
    public function edit_update(User $user, Library $library)
    {
        return $library->user_id == $user->id;
    }

    /**
     * Permission to show library
     */
    public function show(User $user, Library $library)
    {
        return $library->user_id == $user->id && !$library->isDisabled();
    }

    /**
     * Permission to add/remove library
     */
    public function toggle_resource(User $user, Library $library)
    {
        return $library->user_id == $user->id && !$library->isDisabled();
    }

    /**
     * Permission to delete library
     */
    public function delete(User $user, Library $library)
    {
        return $library->user_id == $user->id;
    }
}
