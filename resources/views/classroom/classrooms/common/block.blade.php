@if( isset($room) && !is_null($room) )
    <div class="panel position-relative scale-on-hover fast border-left-blue5-3px h-100">
        <a href="{{ route('classroom.classrooms.show', $room->id) }}" class="stretched-link"></a>
        <div class="mt-1">
            <h3 class="font-size5 font-weight500">{{ $room->name }}</h3>
            <div class="mb-1">
                <h4 class="gray2 font-size2 mb-1">{{ $room->subject->exam_board_and_name }}</h4>
                <h4 class="gray2 font-size2 mb-0">{{ count($room->students ?? []) }} @if(count($room->students ?? []) > 1) students @else student @endif</h4>
            </div>
        </div>
    </div>
@else
    <div class="panel position-relative scale-on-hover fast border-left-blue5-3px h-100">
        <a href="{{ $url }}" class="stretched-link"></a>

        <div class="d-flex flex-column justify-content-center align-items-center mt-1 mb-1">
            <span class="iconify blue5 mb-2" data-icon="bi:plus-circle-fill" data-inline="false" style="font-size: 48px;"></span>
            <h4 class="gray2 font-size2 mb-0">{{ $title }}</h4>
        </div>
    </div>
@endif
