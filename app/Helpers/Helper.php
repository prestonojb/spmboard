<?php

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Answer;
use App\AnswerComment;
use App\CertificationApplication;
use App\ExamBuilderSheet;
use App\QuestionComment;
use App\ResourceReview;
use App\WithdrawalRequest;
use App\User;

if(!function_exists('collectionPaginate')) {
    function collectionPaginate($items, $perPage = 15, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);

        $items = $items instanceof Collection ? $items : Collection::make($items);

        // Resolve page 2,3,4... path
        $options = ['path' => LengthAwarePaginator::resolveCurrentPath()];

        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }
}

if(!function_exists('readableNumber')) {
    function readableNumber($num)
    {
        $units = ['', 'K', 'M', 'B', 'T'];
        for ($i = 0; $num >= 1000; $i++) {
            $num /= 1000;
        }
        return round($num, 1) . $units[$i];
    }
}

if(!function_exists('getRandomString')) {
    function getRandomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randomString = '';

        for ($i = 0; $i < $length; $i++) {
            $index = rand(0, strlen($characters) - 1);
            $randomString .= $characters[$index];
        }

        return $randomString;
    }
}

if(!function_exists('getDayOfWeekName')) {
    function getDayOfWeekName($day_of_week)
    {
        $day_names = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
        return $day_names[ $day_of_week ];
    }
}

/**
 * Get notification URL by type
 */
if(!function_exists('getNotificationUrl')) {
    function getNotificationUrl($notification)
    {
        try {
            $target = $notification->data['target'];
            switch( $notification->data['type'] ) {
                // CLASSROOM
                case 'App\Lesson':
                    return route('classroom.lessons.show', [$target['classroom_id'], $target['id']]);
                case 'App\ResourcePost':
                    return route('classroom.lessons.resource_posts.show', [$target['lesson_id'], $target['id']]);
                case 'App\Homework':
                    return route('classroom.lessons.homeworks.show', [$target['lesson_id'], $target['id']]);
                case 'App\HomeworkSubmission':
                    return route('classroom.lessons.homeworks.submissions.show', [$notification->data['lesson_id'], $target['id']]);
                case 'App\TeacherInvoice':
                    return route('classroom.invoices.show', $target['id']);
                case 'App\StudentReport':
                    return route('classroom.reports.show', $target['id']);
                case 'App\ClassroomPost':
                    return route('classroom.posts.show', [$target['classroom_id'], $target['id']]);
                // FORUM
                case 'App\Recommendation':
                    $user = User::find($target['user_id']);
                    return $user->show_url;
                case 'App\User':
                    $user = User::find($target['id']);
                    return $user->show_url;
                case 'App\Answer':
                    $answer = Answer::find($target['id']);
                    return $answer->show_url;
                case 'App\QuestionComment':
                    $question_comment = QuestionComment::find($target['id']);
                    return $question_comment->question->show_url;
                case 'App\AnswerComment':
                    $answer_comment = AnswerComment::find($target['id']);
                    return $answer_comment->answer->show_url;
                case 'App\ExamBuilderSheet':
                    $is_qp = $notification->data['is_qp'];
                    $sheet = ExamBuilderSheet::find($target['id']);
                    return $is_qp ? $sheet->qp_route : $sheet->ms_route;
                case 'App\CertificationApplication':
                    $application = CertificationApplication::find($target['id']);
                    return $application->show_url;
                // Marketplace
                case 'App\ResourceReview':
                    $resource_review = ResourceReview::find($target['id']);
                    return $resource_review->resource->show_url;
                case 'App\WithdrawalRequest':
                    $withdrawal_request = WithdrawalRequest::find($target['id']);
                    return route('coins.show');
                default:
                    return 'javascript:void(0)';
            }
        } catch( Exception $e ) {
            return 'javascript:void(0)';
        }
    }
}

/**
 * Returns array from comma-seperated string
 * @param string
 * @return array
 */
if(!function_exists('commaSeperatedStringToArray')) {
    function commaSeperatedStringToArray($string)
    {
        $string = str_replace(' ', '', $string);
        return explode(',', $string);
    }
}

/**
 * Returns comma-seperated string from array
 * @param string
 * @return array
 */
if(!function_exists('arrayToCommaSeperatedString')) {
    function arrayToCommaSeperatedString($arr)
    {
        $string = '';
        for($i=0;$i<count($arr);$i++) {
            if($i == count($arr) - 1) {
                $string .= $arr[$i];
            } else {
                $string .= $arr[$i].',';
            }
        }
    }
}

/**
 * Get all months in the year
 * @return array
 */
if(!function_exists('getAllMonths')) {
    function getAllMonths()
    {
        return [
            1 => 'January',
            2 => 'February',
            3 => 'March',
            4 => 'April',
            5 => 'May',
            6 => 'June',
            7 => 'July',
            8 => 'August',
            9 => 'September',
            10 => 'October',
            11 => 'November',
            12 => 'December',
        ];
    }
};

/**
 * Returns coin price after discount
 * @param int $coin_price_before_discount
 * @param int $discount_in_percentage
 * @return int
 */
if(!function_exists('getCoinPriceAfterDiscount')) {
    function getCoinPriceAfterDiscount($coin_price_before_discount, $discount_in_percentage)
    {
        if($discount_in_percentage >= 0 && $discount_in_percentage <= 100) {
            return (int) ceil($coin_price_before_discount * (100 - $discount_in_percentage)/100);
        } else {
            throw new \Exception("Discount must be between 0% to 100%.");
        }
    }
}

if(!function_exists('convertUsdToCoinSell')) {
    function convertUsdToCoinSell($usd_amount) {
        return number_format($usd_amount* setting('conversion.coin_to_usd_sell'), 2);
    }
}

if(!function_exists('coinToUsd')) {
    function coinToUsd($coin_amount) {
        return number_format($coin_amount/10, 2);
    }
}

/**
 * Returns true if file is PDF, else false
 * @param UploadedFile
 * @return boolean
 */
if(!function_exists('isPdfFile')) {
    function isPdfFile($file) {
        return $file->extension() == 'pdf';
    }
}
