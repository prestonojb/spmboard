@extends('main.layouts.app')

@section('meta')
<title>Coins | {{ config('app.name') }}</title>
@endsection

@section('content')
<div class="bg-white">
    {{-- Main --}}
    @component('main.section.main', ['title' => 'Board Coins', 'description' => 'Coins are a virtual good you can use to claim amazing resources on Board.'])
        <div class="text-center">
            <a class="button--primary lg" href="#getCoins">
                Get Coins
            </a>
        </div>
    @endcomponent

    {{-- Here's what you can buy with coins --}}
    @php
    $data = [
        ['title' => 'Board Pass', 'description' => 'Unlimited access to Premium Exam Builder <a href="'.route('board_pass').'">Learn More ></a>', 'icon' => 'ion:ticket-outline'],
    ];
    @endphp
    @component('main.section.icon_list', ['light_bg' => false, 'title' => 'Here\'s what you can buy with coins', 'description' => 'Spend your coins on Board Pass to improve your grades with premium resources!', 'data' => $data])
    @endcomponent

    {{-- Products --}}
    @component('main.section.block_list', ['title' => 'Board Coins', 'description' => 'Spend your coins on Board Pass to improve your grades with premium resources!', 'items' => $cash_products, 'type' => 'coins'])
        @slot('current_balance')
            @auth
                @if(auth()->user()->hasCoins())
                    <div class="badge bg-white blue6 font-size5">Current Balance:
                        <img src="{{ asset('img/essential/coin.svg') }}" alt="coin" width="20" height="20" class="d-inline-block mr-1">
                        {{ auth()->user()->coin_balance }}
                    </div>
                @endif
            @endauth
        @endslot
    @endcomponent

    {{-- FAQ --}}
    @php
        $qna = [
            'What are coins?' => 'Coins are our virtual good, and you can use them to unlock amazing study materials on Board Marketplace. You also need coins to purchase <a href="' . route('board_pass') . '" target="_blank">Board Pass</a>! We\'ll be adding cool new ways to spend your coins in the future.',
            'Can I transfer coins between Board accounts?' => 'You cannot transfer your coins between accounts.',
        ];
    @endphp
    @component('main.section.faq', ['qna' => $qna])
    @endcomponent
</div>
@include('main.navs.footer')
@endsection

@section('scripts')
<script>
$(function(){
    @if(!is_null(auth()->user()->password))
        $('.purchaseWithCashButton').click(async function(e){
            e.preventDefault();
            var form = $(this).closest('form');
            var product = $(this).data('product');
            var price = $(this).data('price');
            const { value: password } = await Swal.fire({
                title: 'Confirm Purchase',
                html: "You are purchasing <b>" + product.coins_in_return + "</b> coins for <b>" + product.currency + product.price + "</b>.<br> Please enter your password to confirm payment.",
                type: 'info',
                showCancelButton: 'Cancel',
                input: 'password',
                inputLabel: 'Password',
                inputPlaceholder: 'Enter your password',
                inputValidator: (value) => {
                    if (!value) {
                        return 'Enter your password!'
                    }
                },
                inputAttributes: {
                    autocapitalize: 'off',
                    autocorrect: 'off'
                }
            });

            // Password is entered
            if(password) {
                form.find('[name=password]').val(password);
                form.submit();
            }
        });
    @else
        $('.purchaseWithCashButton').click(function(e){
            e.preventDefault();
            Swal.fire({
                title: 'Setup password to proceed with payment',
                icon: 'info',
                showCancelButton: true,
                confirmButtonText: 'Setup Password',
                cancelButtonText: 'Cancel',
            }).then((result) => {
                if (result.value) {
                    window.location.href = "{{ route('settings.password') }}";
                }
            })
        });
    @endif

    $('a[href="#getCoins"]').click(function(e) {
        e.preventDefault();
        $('html, body').animate({
            scrollTop: $("#getCoins").offset().top - 68
        }, 500);
    });
});
</script>
@endsection
