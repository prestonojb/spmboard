<div class="modal fade prebuilt-resources-modal prebuilt-notes-modal show" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-xl">
    <div class="modal-content">
        <div class="modal-header">
            Notes
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>

        <div class="modal-body">
            <div class="row">

                @foreach($chapters as $chapter)
                    @if(count($chapter->free_notes ?? []))
                        <div class="col-12">
                            <div class="mb-4">
                                <h4 class="h3 font-weight500 border-bottom border-gray3 pb-1 mb-3">{{ $chapter->name }}</h4>
                                <div class="form-row">
                                    @foreach($chapter->free_notes as $note)
                                        <div class="col-12 col-md-6">
                                            @component('classroom.common.teacher.resources.note_item', ['note' => $note])
                                            @endcomponent
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    @endif
                @endforeach
            </div>
        </div>

        <div class="modal-footer">
            <button class="button--primary--inverse" data-dismiss="modal">Close</button>
            <button class="button--primary addResourceButton">Add</button>
        </div>

    </div>
  </div>
</div>
