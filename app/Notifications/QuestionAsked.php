<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use NotificationChannels\Telegram\TelegramChannel;
use NotificationChannels\Telegram\TelegramMessage;
use NotificationChannels\Telegram\TelegramFile;
use App\Question;

class QuestionAsked extends Notification implements ShouldQueue
{
    use Queueable;

    public $question;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Question $question)
    {
        $this->question = $question;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [TelegramChannel::class];
    }

    public function toTelegram($notifiable)
    {
        $url = route('questions.show', [$this->question->subject->exam_board->slug, $this->question->id]);

        $username = !$this->question->anonymous ? $this->question->user->username : 'Unknown User';

        // [SPM Physics] prestonojb asked [What is the powerhouse of the cell?](https://)
        $message = "\[{$this->question->subject->exam_board->name} {$this->question->subject->name}] {$username} asked [{$this->question->question}]({$url})";
        $message = str_replace('_', '\_', $message);
        $message = str_replace('*', '\*', $message);

        $group_chat_id = config('services.telegram-bot-api.channel_id');

        if($this->question->img){
            $file_path = $this->question->img;
            return TelegramFile::create()
                    // Optional recipient user id.
                    ->to($group_chat_id)
                    // Markdown supported.
                    ->content($message)
                    ->photo($file_path);
        } else {
            return TelegramMessage::create()
                    // Optional recipient user id.
                    ->to($group_chat_id)
                    // Markdown supported.
                    ->content($message);
        }
    }

}
