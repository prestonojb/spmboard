<div class="panel blog-tags">
    <a class="blog-tag mr-1 {{ (request()->is('blog')) ? 'active' : '' }}" href="{{ route('blog.index') }}">All</a>
    @foreach($blog_categories as $blog_category)
        <a class="blog-tag mr-1 {{ request()->is('blog/categories/'. $blog_category->slug)? 'active' : '' }}" href="{{ route('blog_categories.show', $blog_category->slug) }}">{{ $blog_category->name }}</a>
    @endforeach
</div>
