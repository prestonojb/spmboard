@php
    $step_labels = ['Design', 'Customize', 'Result']
@endphp

<a class="font-size3 text-dark underline-on-hover" href="{{ route('exam_builder.landing').'?exam_board='.$exam_board->slug }}">< Back to Worksheets</a>

<nav class="form-steps">
    @foreach(range(1, 3) as $i)
        <div class="form-steps__item form-steps__item{{ $steps == $i ? '--active' : '' }} form-steps__item{{ $steps > $i ? '--active' : '' }}">
            <div class="form-steps__item-content">
                <span class="form-steps__item-icon">{{ $i }}</span>
                @if(!$loop->first)
                    <span class="form-steps__item-line"></span>
                @endif
                <span class="form-steps__item-text">{{ $step_labels[$i-1] }}</span>
            </div>
        </div>
    @endforeach
</nav>
