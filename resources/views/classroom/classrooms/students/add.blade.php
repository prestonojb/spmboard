@extends('classroom.layouts.app')

@section('meta')
    <title>Add Student | {{ config('app.name') }} Classroom</title>
@endsection

@section('styles')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.1.2/css/tempusdominus-bootstrap-4.min.css">
@endsection

@section('content')
@component('classroom.common.breadcrumbs')
    <a href="{{ route('classroom.classrooms.show', $classroom->id) }}">Dashboard</a> /
    <a href="{{ route('classroom.students.index', $classroom->id) }}">Students</a> /
    Add
@endcomponent

@component('classroom.common.page_heading', ['title' => 'Add Student'])
@endcomponent

<div class="maxw-460px mx-auto my-4">
    <div class="panel">
        <form action="{{ route('classroom.classrooms.students.add', $classroom->id) }}" method="post">
            @csrf
            <div class="form-field">
                <label for="">
                    Email
                    <button type="button" class="blue4 p-0" data-container="body" data-toggle="popover" data-placement="top" data-content="Key in your students' emails to add them into the classroom. Their email will have to be tied to a Board student account to be able to be added into the classroom.">
                        <i class="fas fa-info-circle"></i>
                    </button>
                </label>
                <input type="text" name="email" class="multiple-email-input" id="" value="{{ old('email') }}" placeholder="Email">
                @error('email')
                    <div class="error-message">{!! $message !!}</div>
                @enderror
            </div>

            <div class="text-center">
                <button type="submit" class="button--primary disableOnSubmitButton">Add</button>
            </div>
        </form>
    </div>
</div>
@endsection
