@auth
    @if( auth()->user()->hasCoins() )
        @component('resources.common.board_pass', ['board_pass' => auth()->user()->getLatestBoardPass()])
        @endcomponent
    @endif
@else
    @component('resources.common.board_pass')
    @endcomponent
@endif
