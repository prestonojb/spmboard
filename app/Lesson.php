<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Lesson extends Model
{
    protected $guarded = [];
    protected $dates = [
        'start_at',
        'end_at',
        'scheduled_at',
    ];
    public $additional_attributes = ['duration_in_hours', 'status', 'resource_post_count', 'homework_count', 'start_date'];
    protected $appends = ['duration_in_hours', 'status', 'resource_post_count', 'homework_count', 'start_date'];

    /*
     |---------------------------------
     | Relationships
     |---------------------------------
    */
    public function classroom()
    {
        return $this->belongsTo(Classroom::class);
    }

    public function resource_posts()
    {
        return $this->hasMany(ResourcePost::class);
    }

    public function homeworks()
    {
        return $this->hasMany(Homework::class);
    }

    public function invoice_items()
    {
        return $this->hasMany( TeacherInvoiceItem::class );
    }

    public function student_report_item()
    {
        return $this->hasMany( StudentReportItem::class );
    }

    /*
     |---------------------------------
     | Accessors
     |---------------------------------
    */

    public function getStartDateAttribute()
    {
        return $this->start_at->toFormattedDateString();
    }

    public function getDurationInHoursAttribute()
    {
        $float_duration_in_hours = $this->start_at->floatDiffInHours( $this->end_at );
        $int_duration_in_hours = floor($float_duration_in_hours);
        $decimal_duration_in_hours = $float_duration_in_hours - $int_duration_in_hours;

        if( $decimal_duration_in_hours <= 0.25 ) {
            $decimal_duration_in_hours = 0.25;
        } elseif( $decimal_duration_in_hours <= 0.5 ) {
            $decimal_duration_in_hours = 0.5;
        } elseif( $decimal_duration_in_hours <= 0.75 ) {
            $decimal_duration_in_hours = 0.75;
        } else {
            $decimal_duration_in_hours = 1;
        }

        return $int_duration_in_hours + $decimal_duration_in_hours;
    }

    public function getStatusAttribute()
    {
        $start_at = $this->start_at;
        $end_at = $this->end_at;
        if( Carbon::now()->lessThan( $start_at ) ) {
            return 'upcoming';
        } elseif( Carbon::now()->greaterThan( $start_at ) && Carbon::now()->lessThan( $end_at ) ) {
            return 'ongoing';
        } else {
            return 'completed';
        }
    }

    public function getStatusTagHtmlAttribute()
    {
        $dict = [
            'upcoming' => 'blue5',
            'ongoing' => 'yellow',
            'completed' => 'green',
        ];
        return "<span class='status-tag {$dict[$this->status]}'>".ucfirst($this->status)."</span>";
    }

    public function getPeriodAttribute()
    {
        if($this->start_at->isSameDay( $this->end_at )) {
            return "{$this->start_at->toFormattedDateString()} {$this->start_at->format('h:iA')}-{$this->end_at->format('h:iA')}";
        }
        return "{$this->start_at->format('M d h:iA') } - {$this->end_at->format('M d h:iA, Y')}";
    }

    public function getPrimaryAttribute()
    {
        return $this->name;
    }

    public function getSecondaryAttribute()
    {
        return "{$this->statusTagHtml} {$this->period}";
    }

    public function getUrlAttribute()
    {
        return route('classroom.lessons.show', [$this->classroom->id, $this->id]);
    }

    public function getResourcePostCountAttribute()
    {
        return $this->resource_posts->count() ?? 0;
    }

    public function getHomeworkCountAttribute()
    {
        return $this->homeworks->count() ?? 0;
    }
}
