@extends('resources.layouts.app')

@section('meta')
<title>Customize Worksheet | {{ $exam_board->name }} {{ config('app.name') }} </title>
@endsection


@section('header')
    @component('resources.common.header', ['type' => 'exam_builder', 'exam_board' => $exam_board])
    @endcomponent
@endsection

@section('breadcrumbs')
    @component('resources.common.breadcrumbs')
        <a href="{{ route('exam_builder.landing', $exam_board) }}">Home</a> /
        Customize Worksheet
    @endcomponent
@endsection

@section('context_panel')
    @component('resources.common.context_panel', ['exam_board' => $exam_board->name, 'title' => 'Customize Worksheet', 'description' => 'Replace/remove questions from your worksheet according to your needs.'])
    @endcomponent
@endsection

@section('body')
    {{-- Steps Nav --}}
    @component('exam_builder.common.steps', ['steps' => 2, 'exam_board' => $exam_board])
    @endcomponent
@endsection

@section('side')
    <div class="mb-2">
        <div class="border-bottom pb-2 mb-2">
            <h2 class="font-size7 mb-1"><b>{{ $subject->name }}</b></h2>
            <p class="mb-2">Paper {{ $paper_number }}</p>
            <div>
                @foreach($chapters as $chapter)
                    <span class="tag--primary">{{ $chapter->name }}</span>
                @endforeach
            </div>
        </div>
        <p class="gray2 mb-0">Total: <span class="itemCount">{{ count($items ?? []) }}</span> questions</p>
    </div>

    <button class="button--primary w-100 mb-3" id="generateExamPaperButton" data-toggle="modal" data-target="#generateExamPaperModal">Generate Exam Paper</button>

    {{-- Modal --}}
    @include('exam_builder.common.generate_paper_modal')
@endsection

@section('main')
    {{-- Body --}}
    <div class="container">
        {{-- Items --}}
        <div class="mb-3">
            @foreach($items as $item)
                @component('exam_builder.common.item_block', ['item' => $item])
                @endcomponent
            @endforeach
        </div>
    </div>
@endsection
