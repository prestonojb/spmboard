@extends('main.layouts.app')

@section('meta')
    <title>Home | {{ config('app.name') }}</title>
    <meta name="description" content="Board is an online community for students to learn from mentors. Share the joy of learning and teaching with the Board community now!">
@endsection

@section('content')
<div class="bg-white">
    {{-- Main --}}
    @component('main.section.main', ['title' => 'Our community of teachers, students and smartypants are ready to answer any and all. We love a challenge!', 'light_title' => true,'bg_img' => 'images/main_wp.png'])
    <div class="row">
        <div class="col-12 col-md-6">
            <div class="animated zoomInLeft fast panel landing__board-panel">
                <h2 class="heading">SPM</h2>
                <h3 class="sub-heading">Join the SPM community now!</h3>
                <a href="{{ route('questions.index', 'spm') }}" class="button--primary cta-button">Join now</a>
            </div>
        </div>
        <div class="col-12 col-md-6">
            <div class="animated zoomInRight fast panel landing__board-panel">
                <h2 class="heading">IGCSE</h2>
                <h3 class="mb-2 mb-md-3">Join the IGCSE community now!</h3>
                <a href="{{ route('questions.index', 'igcse') }}" class="button--primary cta-button">Join now</a>
            </div>
        </div>
    </div>
    @endcomponent

    {{-- What is Board? --}}
    @php
        $data = [
            ['icon' => 'ri:team-line', 'title' => 'Community Support', 'description' => 'Board is a learner’s sanctuary - no judgement, no competition and all fun! We believe this is the perfect formula for a learning environment.'],
            ['icon' => 'carbon:stacked-scrolling-1', 'title' => 'Comprehensive educational resources', 'description' => 'Our A* resources are made and compiled by teachers who know just what you might be lacking. And guess what? It is for free!'],
            ['icon' => 'mdi:lightbulb-on-outline', 'title' => 'Easy to use', 'description' => 'Our platform is straight forward - ask, get answered and score!'],
            ['icon' => 'ic:sharp-blur-linear', 'title' => 'Bridging the gap', 'description' => 'All our mentors are vetted and trained to make sure they give you the most accurate information. Through Board, you can connect with them to get the personalised help you need.'],
            ['icon' => 'clarity:world-line', 'title' => 'A new era of learning', 'description' => 'Introducing a new 21st-century style of learning - interactive, online and resourceful! Reach us anywhere you go, all you need is an internet connection and device.'],
            ['icon' => 'mdi:teach', 'title' => 'Regular live sessions', 'description' => 'Connect yourself with professionals in real time and get the personalised help you need.'],
        ];
    @endphp
    @component('main.section.icon_list', ['title' => 'What is Board?', 'description' => 'Learn, Unlearn and Relearn', 'data' => $data])
    @endcomponent


    {{-- Forum --}}
    @php
    $data = [
        ['img' => asset('img/landing/questions.svg'), 'heading' => 'Ask a question', 'subheading' => 'Tired of searching and Googling for hours on end? Just ask here on Board!'],
        ['img' => asset('img/landing/teacher.svg'), 'heading' => 'Get answered','subheading' => 'Our community of students, teachers and smarty-pants will answer your questions - they love being tested by hard questions!'],
        ['img' => asset('img/landing/exams.svg'), 'heading' => 'Get rewarded','subheading' => 'If you need another question answered, just do it again! Rinse and repeat.'],
    ];
    @endphp
    {{-- Header --}}
    @component('main.section.blue_header', ['title' => 'Forum', 'subtitle' => 'Hear from the best', 'description' => 'Professionals in their fields from all around the world are right at your fingertips.<br> No more searching and Googling for hours on end, all you need to do is ask away.', 'body' => 'Professionals in their fields from all around the world are right at your fingertips. No more searching and Googling for hours on end, all you need to do is ask away.'])
    @endcomponent
    {{-- Cards --}}
    @component('main.section.card_list', ['data' => $data])
    @endcomponent


    {{-- Resources --}}
    @php
    $data = [
        ['img' => asset('img/landing/gift.svg'), 'heading' => 'Redeem coins', 'subheading' => 'Every time you answer a question from another user, you get rewarded with points to redeem Board resources. It’s a win-win situation for everyone!'],
        ['img' => asset('img/landing/bibliophile.svg'), 'heading' => 'Study', 'subheading' => 'You can then redeem resources such as topical papers, notes and exercises with the coins you earned.'],
        ['img' => asset('img/landing/exams.svg'), 'heading' => 'Ace exams', 'subheading' => 'These resources will bring you closer to the top score. Woohoo!'],
    ];
    @endphp
    {{-- Header --}}
    @component('main.section.blue_header', ['title' => 'Resources', 'subtitle' => 'Learning has never been this rewarding', 'description' => 'By answering questions from other users, you can collect points to unlock more IGCSE Board resources.<br> It’s a win-win for everybody!', 'body' => 'By answering questions from other users, you can collect points to unlock more IGCSE Board resources. It’s a win-win for everybody!'])
    @endcomponent
    {{-- Cards --}}
    @component('main.section.card_list', ['data' => $data])
    @endcomponent


    {{-- CTA Banner --}}
    @component('main.section.cta_banner', ['title' => 'Want to be part of a community of passionate students and teachers?', 'align_buttons' => true])
        <a href="{{ url('register') }}" class="h3 button primary mb-2 mr-sm-2">Join Now</a>
        <a href="{{ url('contact-us') }}" class="h3 button secondary mb-2">Contact Us</a>
    @endcomponent

    {{-- Footer --}}
    @include('main.navs.footer')
</div>
@endsection
