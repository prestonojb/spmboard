<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class BoardPassSubscription extends Model
{
    protected $guarded = [];
    protected $dates = ['activated_at', 'created_at', 'updated_at'];
    protected $appends = ['end_at'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * -------------
     * ACCESSORS
     * -------------
     */
    public function getIsActiveAttribute()
    {
        return $this->isActive();
    }

    public function getIsExpiredAttribute()
    {
        return $this->isExpired();
    }

    /**
     * Returns Carbon timestamp of Board Pass end_at
     * @return Carbon\Carbon|null
     */
    public function getEndAtAttribute()
    {
        return !is_null($this->activated_at) ? $this->activated_at->addMonths($this->duration_in_month)
                                             : null;
    }

    /**
     * Returns true if subscription is activated, else false
     * @return boolean
     */
    public function isActivated()
    {
        return !is_null($this->activated_at);
    }

    /**
     * Returns true if subscription is active, else false
     * @return boolean
     */
    public function isActive($on = null)
    {
        if(is_null($on)) {
            $on = Carbon::now();
        }
        return !is_null($this->end_at) ? $this->activated_at->lessThanOrEqualTo($on) && $this->end_at->greaterThanOrEqualTo($on)
                                       : false;
    }

    /**
     * Returns true if subscription is expired, else false
     * @return boolean
     */
    public function isExpired($on = null)
    {
        if(is_null($on)) {
            $on = Carbon::now();
        }
        return !is_null($this->end_at) ? $this->end_at->lessThanOrEqualTo($on)
                                       : false;
    }

    /**
     * --------------
     * HELPERS
     * --------------
     */

    /**
     * Returns true if subscription can extend for months
     * @param integer $duration_in_month
     * @return boolean
     */
    public function canExtendFor($duration_in_month)
    {
        if(is_int($duration_in_month) && $duration_in_month > 0) {
            return $this->duration_in_month + $duration_in_month > 60;
        } else {
            throw new \Exception("Duration in month must be a positive integer");
        }
    }

    /**
     * Returns max number of months
     * @return integer
     */
    public function getMaxExtension()
    {
        return 60 - $this->duration_in_month;
    }

    /**
     * Extend subscription for months
     * @param integer $duration_in_month
     */
    public function extend($duration_in_month)
    {
        if(is_int($duration_in_month) && $duration_in_month > 0) {
            $init_duration_in_month = $this->duration_in_month;
            // Update duration_in_month of subscription
            $this->update([
                'duration_in_month' => $init_duration_in_month + $duration_in_month,
            ]);
        } else {
            throw new \Exception("Duration in month must be a positive integer");
        }
    }

    /**
     * Returns the end_at date
     * @param Carbon\Carbon $activated_at
     * @param Carbon\Carbon $end_at
     * @return string
     * @throws \Exception when Board Pass Subscription is no longer active
     */
    public static function getEndAtDate($end_at, $activated_at)
    {
        if(is_null($activated_at)) {
            $activated_at = Carbon::now();
        }
        // Get activated_at additional days from start of month
        $additional_days = $activated_at->day - 1;

        // From activated_at start of month, calculate the duration in month
        $start_of_month = $activated_at->copy()->startOfMonth();
        $duration_in_month = $end_at->diffInMonths($start_of_month);

        // Add duration in month to activated_at
        // Add additional days to activated_at (with no overflow in month)
        if($duration_in_month >= 0) {
            return $end_at->copy()->addUnitNoOverflow('day', $additional_days, 'month')->toFormattedDateString();
        } else {
            throw new \Exception("End at date cannot be in the past!");
        }
    }

    /**
     * Returns the active subscription end at date
     * @param Carbon\Carbon $end_at
     * @return string
     * @throws \Exception when Board Pass Subscription is no longer active
     */
    public function getActiveSubscriptionEndAtDate($end_at)
    {
        if(!$this->is_active) {
            throw new \Exception("Board Pass subscription must be active!");
        }
        return BoardPassSubscription::getEndAtDate($end_at, $this->activated_at);
    }

    /**
     * Returns the no of additional subscription months
     * @param Carbon\Carbon $activated_at
     * @param Carbon\Carbon $end_at
     * @param int $curr_duration_in_month
     * @return int
     * @throws \Exception when Board Pass Subscription is no longer active
     */
    public static function getNoOfAdditionalSubscriptionMonths($end_at, $activated_at, $curr_duration_in_month = 0)
    {
        if(is_null($activated_at)) {
            $activated_at = Carbon::now();
        }
        // From activated_at start of month, calculate the duration in month
        $start_of_month = $activated_at->copy()->startOfMonth();
        $duration_in_month = $end_at->diffInMonths($start_of_month);

        // Add duration in month to activated_at
        // Add additional days to activated_at (with no overflow in month)
        if($duration_in_month >= 0) {
            return $duration_in_month - $curr_duration_in_month;
        } else {
            throw new \Exception("End at date cannot be in the past!");
        }
    }

    /**
     * Returns the active subscription no of additional subscription months
     * @param Carbon\Carbon $end_at
     * @return int
     * @throws \Exception when Board Pass Subscription is no longer active
     */
    public function getActiveSubscriptionNoOfAdditionalSubscriptionMonths($end_at)
    {
        if(!$this->is_active) {
            throw new \Exception("Board Pass subscription must be active!");
        }
        return BoardPassSubscription::getNoOfAdditionalSubscriptionMonths($end_at, $this->activated_at, $this->duration_in_month);
    }

    /**
     * Returns the ammount of coins payable for purchase/upgrade
     * @param Carbon\Carbon $activated_at
     * @param Carbon\Carbon $end_at
     * @return string
     * @throws \Exception when Board Pass Subscription is no longer active
     */
    public static function getAmountOfCoins($end_at, $activated_at, $curr_duration_in_month = 0)
    {
        if(is_null($activated_at)) {
            $activated_at = Carbon::now();
        }
        // Get activated_at additional days from start of month
        $additional_days = $activated_at->day;

        // From activated_at start of month, calculate the duration in month
        $start_of_month = $activated_at->copy()->startOfMonth();
        $duration_in_month = $end_at->diffInMonths($start_of_month);

        // Add duration in month to activated_at
        // Add additional days to activated_at (with no overflow in month)
        if($duration_in_month >= 0) {
            $no_of_months_of_subscription = $duration_in_month - $curr_duration_in_month;
            return CoinProduct::getBoardPass()->getBoardPassPrice($no_of_months_of_subscription);
        } else {
            throw new \Exception("End at date cannot be in the past!");
        }
    }

    /**
     * Returns the active subscription end at date
     * @param Carbon\Carbon $end_at
     * @return string
     * @throws \Exception when Board Pass Subscription is no longer active
     */
    public function getActiveSubscriptionAmountOfCoins($end_at)
    {
        if(!$this->is_active) {
            throw new \Exception("Board Pass subscription must be active!");
        }
        return BoardPassSubscription::getAmountOfCoins($end_at, $this->activated_at, $this->duration_in_month);
    }
}
