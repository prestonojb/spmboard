<?php

namespace App\Http\Controllers;

use App\Classroom;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Stripe\Stripe;

class ClassroomPriceController extends Controller
{
    public $stripe;
    public function __construct()
    {
        Stripe::setApiKey(config('services.stripe.secret'));
        $this->stripe = new \Stripe\StripeClient(
            config('services.stripe.secret')
        );
    }

    public function guest_index()
    {
        if( auth()->check() ) {
            return $this->index();
        }

        $stripe = $this->stripe;

        $products = $stripe->products->all();

        foreach( $products['data'] as $curr_product ) {
            if( $curr_product->name == 'Premium' ) {
                $product = $curr_product;
            }
        }

        $prices = $stripe->prices->all([
            'product' => $product->id
        ]);

        return view('classroom.prices.guest_index', compact('prices', 'product'));
    }

    public function index()
    {
        $stripe = $this->stripe;

        $user = auth()->user();

        if( !$user->is_teacher() ) {
            return redirect()->back()->with('error', 'You are not eligible to subscribe for Premium package with your'. auth()->user()->account_type .' account.');
        }

        // Get Stripe Premium Product
        $product = $stripe->products->retrieve( setting('stripe.premium_product_id') );

        $monthly_price = $stripe->prices->retrieve( setting('stripe.premium_monthly_price_id') );
        $annual_price = $stripe->prices->retrieve( setting('stripe.premium_annual_price_id') );

        // Get user active price
        if( $user->subscribed('premium') ) {
            $active_price_id = $user->subscription('premium')->stripe_plan;
            $active_price = $stripe->prices->retrieve(
                $active_price_id
            );
        } else {
            $active_price = null;
        }

        $stripeCustomer = $user->createOrGetStripeCustomer();
        $payment_methods = $user->paymentMethods();
        $intent = $user->createSetupIntent();

        return view('classroom.prices.index', compact('product', 'monthly_price', 'annual_price', 'active_price', 'intent', 'payment_methods'));
    }
}
