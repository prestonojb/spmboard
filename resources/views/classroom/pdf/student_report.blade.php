<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Student Report | {{ config('app.name') }}</title>

    <style>
    .invoice-box {
        max-width: 800px;
        margin: auto;
        padding: 30px;
        border: 1px solid #eee;
        box-shadow: 0 0 10px rgba(0, 0, 0, .15);
        font-size: 16px;
        line-height: 24px;
        font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
        color: #555;
    }

    .invoice-box table {
        width: 100%;
        line-height: inherit;
        text-align: left;
    }

    .invoice-box table td {
        padding: 5px;
        vertical-align: top;
    }

    .invoice-box table tr.top table td {
        padding-bottom: 20px;
    }

    .invoice-box table tr.top table td.title {
        font-size: 45px;
        line-height: 45px;
        color: #333;
    }

    .invoice-box table tr.information table td {
        padding-bottom: 40px;
    }

    .invoice-box table tr.heading td {
        background: #eee;
        border-bottom: 1px solid #ddd;
        font-weight: bold;
    }

    .invoice-box table tr.details td {
        padding-bottom: 20px;
    }

    .invoice-box table tr.item td{
        border-bottom: 1px solid #eee;
    }

    .invoice-box table tr.item.last td {
        border-bottom: none;
    }

    .invoice-box table tr.total td:nth-child(2) {
        border-top: 2px solid #eee;
        font-weight: bold;
    }

    @media only screen and (max-width: 600px) {
        .invoice-box table tr.top table td {
            width: 100%;
            display: block;
            text-align: center;
        }

        .invoice-box table tr.information table td {
            width: 100%;
            display: block;
            text-align: center;
        }
    }

    .text-right {
        text-align: right;
    }

    .bordered-table {
        border-collapse: collapse;
    }

    table.bordered-table, .bordered-table th, .bordered-table td {
        border: 1px solid #eee;
    }
    .mb-0 {
        margin-bottom: 0;
    }
    .mb-1 {
        margin-bottom: 4px;
    }
    </style>
</head>

<body>
    <div class="invoice-box">
        <table cellpadding="0" cellspacing="0">
            <tr class="top">
                <td colspan="4">
                    <table>
                        <tr>
                            <td colspan="3" class="title">
                                <img src="{{ asset('img/logo/logo_light.png') }}" style="width: 250px;">
                            </td>

                            <td>
                                <p class="mb-0">Report: #{{ $report->id }}</p>
                                <p class="mb-0">Student: {{ $report->student->name }}</p>
                                <p class="mb-0">Prepared: {{ $report->created_at->toFormattedDateString() }}</p>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>

            <tr class="information" style="margin-bottom: 8px;">
                <td>
                    From: {{ $report->teacher->name }}<br>
                    {{ $report->teacher->email }}
                </td>
            </tr>

            @foreach($report->lesson_items as $item)
                <tr>
                    <td colspan="5">
                        <h3 style="margin-bottom: 0;">{{ $item->lesson->name }}</h3>
                        <p style="margin-bottom: 0;">{{ $item->lesson->period }}</p>
                    </td>
                </tr>

                <tr>
                    <td colspan="4">
                        <table class="bordered-table">
                            <tr class="heading">
                                <td colspan="2">
                                    Type
                                </td>

                                <td colspan="3">
                                    Title
                                </td>

                                <td colspan="1">
                                    Metric
                                </td>
                            </tr>

                            @if( $item->lesson->resource_post_count )
                                @foreach( $item->lesson->resource_posts as $post )
                                    <tr class="item">
                                        @if($loop->first)
                                            <td rowspan="{{ $item->lesson->resource_post_count }}" colspan="2">Resources</td>
                                        @endif
                                        <td colspan="3">{{ $post->title }}</td>
                                        <td colspan="1">{{ $post->metric( $report->student ) ?? 'N/A' }}</td>
                                    </tr>
                                @endforeach
                            @endif

                            @if( $item->lesson->homework_count )
                                @foreach( $item->lesson->homeworks as $homework )
                                    <tr class="item">
                                        @if($loop->first)
                                            <td rowspan="{{ $item->lesson->homework_count }}" colspan="2">Homework</td>
                                        @endif
                                        <td colspan="3">{{ $homework->title }}</td>
                                        <td colspan="1">{{ $homework->getStudentSubmission( $report->student ) && $homework->getStudentSubmission( $report->student )->marks_to_max_ratio ? $homework->getStudentSubmission( $report->student )->marks_to_max_ratio : 'N/A' }}</td>
                                    </tr>
                                @endforeach
                            @endif
                        </table>
                    </td>
                </tr>

                @if(!$loop->last)
                    <tr>
                        <td style="padding: 12px 0;"></td>
                    </tr>
                @endif
            @endforeach

            @if( $report->teacher_remark )
                <tr>
                    <td><h3>Remark</h3></td>
                    <td>{{ $report->teacher_remark }}</td>
                </tr>
            @endif

            <tr>
                <td colspan="4">
                    <span style="font-size: 12px;">Powered by Board</span>
                </td>
            </tr>
        </table>
    </div>
</body>
</html>
