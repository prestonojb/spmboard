@if(isset($type) && $type == 'unlocked_resources')
    <div class="panel position-relative scale-on-hover fast border-left-white-3px bg-blue6 text-white h-100">
        <a href="{{ route('libraries.show_unlocked_resources') }}" class="stretched-link"></a>
        <div class="mt-1">
            <h3 class="font-size5 font-weight500 color-inherit">
                Unlocked Resources
            </h3>
            <div class="mb-1">
                <h4 class="font-size1 mb-0 color-inherit">{{ $user->resources->count() }} @if($user->resources->count()) resources @else resource @endif</h4>
            </div>
        </div>
    </div>
@else
    <div class="panel position-relative scale-on-hover fast @if($library->isDisabled()) border-left-gray5-3px @else border-left-blue5-3px @endif h-100">
        <a href="@if($library->isDisabled()) javascript:void(0) @else {{ route('libraries.show', $library->id) }} @endif" class="stretched-link @if($library->isDisabled()) disabled @endif"></a>
        <div class="mt-1">
            <h3 class="font-size5 font-weight500">
                {{ $library->name }}
                @if($library->isDisabled())
                    <span class="badge badge-light text-uppercase">Disabled</span>
                @endif
            </h3>
            <div class="mb-1">
                <h4 class="gray2 font-size1 mb-0">{{ $library->getTotalResourceCount() }} @if($library->getTotalResourceCount() > 1) resources @else resource @endif</h4>
            </div>
        </div>
    </div>
@endif
