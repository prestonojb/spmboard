// Text Editor JS
if( $('.editor')[0] ) {
    // Declare DOMs
    var $editor = $('.editor')[0];
    var $input = $('input[data-editor-input=true]');
    var inputRequired = $input.prop('required');
    var $form = $input.closest('form');
    var $submitButton = $form.find('[type=submit]');
    var editorLabel = $input.attr('name');

    // Toolbar
    var toolbarOptions = [
        [{ 'header': [3, 4, false] }],
        ['bold', 'italic', 'underline'],
        [{ 'list': 'ordered'}, { 'list': 'bullet' }],
        [{ 'color': [] }, { 'background': [] }],
        ['link'],
    ];

    var quill = new Quill('.editor', {
        modules: {
            toolbar: toolbarOptions,
            clipboard: {
                matchVisual: false,
                matchSpacing: false,
            },
        },
        theme: 'snow',
    });

    console.log(quill);
    window.quill = quill;

    // Fill Text editor with existing data if any
    quill.root.innerHTML = $input.val();

    quill.on('text-change', function(){
        $input.val(quill.root.innerHTML);
    });

    $submitButton.click(function(e){
        e.preventDefault();
        var $t = $(this);
        var initHtml = $t.html();

        window.showLoader( $t, 'in-button', 'white' );

        var quillText = quill.getText();
            //remove white spaces and line breaks
            quillText = quillText.trim();

            // Get HTML in Quill editor
            var editorHTML = $('.ql-editor').html();
            // Trim all UNNECESSARY empty paragraphs("<p><br></p>") from Quill editor
            var lenOfEmptyParagraph = '<p><br></p>'.length;
            // Trim empty paragraphs at start
            while ( editorHTML.startsWith( '<p><br></p>' ) ){
                editorHTML = editorHTML.substr( lenOfEmptyParagraph );
            }
            // Trim empty paragraphs at end
            while ( editorHTML.endsWith( '<p><br></p>' ) ){
                editorHTML = editorHTML.substr( 0, editorHTML.length - lenOfEmptyParagraph );
            }
            // Remove remaining duplicate empty paragraphs
            while ( editorHTML.includes('<p><br></p><p><br></p>') ) {
                editorHTML = editorHTML.replace('<p><br></p><p><br></p>', '<p><br></p>');
            }

            if( editorHTML == '<p><br><p>' ) {
                editorHTML = '';
            }


            // Throw alert if input is required but text editor is empty
            if( inputRequired && editorHTML == '' ){
                //Throw an alert if quill input is empty
                window.hideLoader( $t, initHtml );
                alert('Please fill in the ' + editorLabel + ' field!');
                return;
            } else {
                // Update editor HTML to input
                $input.val(editorHTML);
                $form.submit();
            }
    });
}
