<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Session;

class CheckForClassroomAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if( Gate::denies('access-classroom') ) {
            Session::flash('error', 'Your account is not authorized to access Board Classroom.');
            return redirect('/');
        }

        return $next($request);
    }
}
