<div class="text-center mb-4">
    <h3 class="font-size7"><b>Plan Comparison</b></h3>
</div>

<div class="table-responsive">
    <table class="panel table table-bordered maxw-960px mx-auto">
        <thead>
            <tr>
                <th></th>
                <th scope="col">Free</th>
                <th scope="col">Board Premium</th>
            </tr>
        </thead>
        <tbody>
            {{-- Profiling --}}
            <tr>
                <th scope="row" colspan="3" class="bg-blue6 text-white text-uppercase">Profiling</th>
            </tr>

            <tr>
                <td>Social Links</td>
                <td><span class="red font-size7"><i class="fas fa-times-circle"></i></span></td>
                <td><span class="green font-size7"><i class="fas fa-check-circle"></i></span></td>
            </tr>

            <tr>
                <td>Premium Badge</td>
                <td><span class="red font-size7"><i class="fas fa-times-circle"></i></span></td>
                <td><span class="green font-size7"><i class="fas fa-check-circle"></i></span></td>
            </tr>

            {{-- Classroom --}}
            <tr>
                <th scope="row" colspan="3" class="bg-blue6 text-white text-uppercase">Classroom</th>
            </tr>

            <tr>
                <td>No. of Classrooms</td>
                <td>{{ setting('classroom.free_classroom_limit') }}</td>
                <td>{{ setting('classroom.premium_classroom_limit') }}</td>
            </tr>
            <tr>
                <td>Classroom Size</td>
                <td>Up to {{ setting('classroom.free_student_limit') }} students per classroom</td>
                <td>Up to {{ setting('classroom.premium_student_limit') }} students per classroom</td>
            </tr>
            <tr>
                <td>No. of Lessons</td>
                <td>Up to {{ setting('classroom.free_lesson_limit') }} per classroom</td>
                <td>Up to {{ setting('classroom.premium_lesson_limit') }} per classroom</td>
            </tr>

            <tr>
                <td>Reporting</td>
                <td><span class="red font-size7"><i class="fas fa-times-circle"></i></span></td>
                <td><span class="green font-size7"><i class="fas fa-check-circle"></i></span></td>
            </tr>

            {{-- Exam Builder --}}
            <tr>
                <th scope="row" colspan="3" class="bg-blue6 text-white text-uppercase">Exam Builder</th>
            </tr>
            <tr>
                <td>Remove Watermark</td>
                <td><span class="red font-size7"><i class="fas fa-times-circle"></i></span></td>
                <td><span class="green font-size7"><i class="fas fa-check-circle"></i></span></td>
            </tr>
            <tr>
                <td>Subtopic Worksheets</td>
                <td><span class="red font-size7"><i class="fas fa-times-circle"></i></span></td>
                <td><span class="green font-size7"><i class="fas fa-check-circle"></i></span></td>
            </tr>
            <tr>
                <td>Unlimited Worksheets</td>
                <td><span class="green font-size7"><i class="fas fa-check-circle"></i></span></td>
                <td><span class="green font-size7"><i class="fas fa-check-circle"></i></span></td>
            </tr>
            <tr>
                <td>Mark Scheme</td>
                <td>1 per week</td>
                <td>Unlimited</td>
            </tr>
            <tr>
                <td>Question limit per sheet</td>
                <td>10</td>
                <td>20</td>
            </tr>
            <tr>
                <td>Chapter limit per sheet</td>
                <td>1</td>
                <td>5</td>
            </tr>

            {{-- Resources --}}
            <tr>
                <th scope="row" colspan="3" class="bg-blue6 text-white text-uppercase">Resources</th>
            </tr>
            <tr>
                <td colspan="3" class="bg-blue1"><i>Past Papers</i></td>
            </tr>
            <tr>
                <td>Advanced Filtering</td>
                <td><span class="green font-size7"><i class="fas fa-check-circle"></i></span></td>
                <td><span class="green font-size7"><i class="fas fa-check-circle"></i></span></td>
            </tr>

            <tr>
                <td colspan="3" class="bg-blue1"><i>Topical Papers</i></td>
            </tr>
            <tr>
                <td>Question Paper</td>
                <td><span class="green font-size7"><i class="fas fa-check-circle"></i></span></td>
                <td><span class="green font-size7"><i class="fas fa-check-circle"></i></span></td>
            </tr>
            <tr>
                <td>Mark Scheme</td>
                <td><span class="red font-size7"><i class="fas fa-times-circle"></i></span></td>
                <td><span class="green font-size7"><i class="fas fa-check-circle"></i></span></td>
            </tr>

            <tr>
                <td colspan="3" class="bg-blue1"><i>Notes</i></td>
            </tr>
            <tr>
                <td>One-page</td>
                <td><span class="green font-size7"><i class="fas fa-check-circle"></i></span></td>
                <td><span class="green font-size7"><i class="fas fa-check-circle"></i></span></td>
            </tr>
            <tr>
                <td>Comprehensive</td>
                <td><span class="red font-size7"><i class="fas fa-times-circle"></i></span></td>
                <td><span class="green font-size7"><i class="fas fa-check-circle"></i></span></td>
            </tr>

            <tr>
                <td colspan="3" class="bg-blue1"><i>Guides</i></td>
            </tr>
            <tr>
                <td>Online Access</td>
                <td><span class="green font-size7"><i class="fas fa-check-circle"></i></span></td>
                <td><span class="green font-size7"><i class="fas fa-check-circle"></i></span></td>
            </tr>
            <tr>
                <td>Offline Access</td>
                <td><span class="red font-size7"><i class="fas fa-times-circle"></i></span></td>
                <td><span class="green font-size7"><i class="fas fa-check-circle"></i></span></td>
            </tr>

        </tbody>
    </table>
</div>
