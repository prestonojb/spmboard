<div class="panel p-0">
    <div class="panel-header">
        Homework
    </div>
    <div class="panel-body">
        <div class="text-center py-2">
            <p class="gray2 mb-3">Homework Marks</p>
            <canvas id="homeworkMarksTime"></canvas>
        </div>
    </div>
</div>
