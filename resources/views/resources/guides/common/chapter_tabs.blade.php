<div class="tab-content">
    <div class="tab-pane fade show active" id="general_chapter" role="tabpanel">
        <h2 class="font-size7 border-bottom py-2 mb-3">General</h2>
        @if($subject->hasGeneralGuides())
            @foreach($subject->general_guides as $guide)
                @component('resources.guides.common.block', ['guide' => $guide])
                @endcomponent
                <div class="py-1"></div>
            @endforeach
        @else
            <div class="text-center p-5">
                <img src="{{ asset('img/essential/empty.svg') }}" alt="" width="400" height="400" class="d-inline-block mb-2">
                <p class="gray2">No guides available</p>
            </div>
        @endif
    </div>
    @foreach($subject->chapters as $chapter)
        <div class="tab-pane fade" id="{{ str_slug($chapter->name) }}_chapter" role="tabpanel">
            <h2 class="font-size7 border-bottom py-2 mb-3">{{ $chapter->name }}</h2>
            @if(count($chapter->guides ?? []))
                @foreach($chapter->guides as $guide)
                    @component('resources.guides.common.block', ['guide' => $guide])
                    @endcomponent
                    <div class="py-1"></div>
                @endforeach
            @else
                <div class="text-center p-5">
                    <img src="{{ asset('img/essential/empty.svg') }}" alt="" width="400" height="400" class="d-inline-block mb-2">
                    <p class="gray2">No guides available</p>
                </div>
            @endif
        </div>
    @endforeach
</div>
