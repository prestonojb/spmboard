<!-- Topbar -->
<nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top">

    <!-- Sidebar Toggle (Topbar) -->
    <button id="sidebarToggleTop" class="btn btn-link text-dark d-md-none rounded-circle mr-3">
      <i class="fa fa-bars"></i>
    </button>

    {{-- <!-- Topbar Search -->
    <form class="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search">
      <div class="input-group">
        <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2" disabled>
        <div class="input-group-append">
          <button class="btn btn-primary" type="button">
            <i class="fas fa-search fa-sm"></i>
          </button>
        </div>
      </div>
    </form> --}}

    @auth
        <div class="white bg-blue5 px-3 py-2 text-uppercase ml-md-3"><b>{{ ucfirst(auth()->user()->account_type) }}</b></div>
    @endauth
<!-- Topbar Navbar -->
<ul class="navbar-nav ml-auto">

    {{-- <!-- Nav Item - Search Dropdown (Visible Only XS) -->
    <li class="nav-item dropdown no-arrow d-sm-none">
      <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="fas fa-search fa-fw"></i>
      </a>
      <!-- Dropdown - Messages -->
      <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchDropdown">
        <form class="form-inline mr-auto w-100 navbar-search">
          <div class="input-group">
            <input type="text" disabled class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
            <div class="input-group-append">
              <button class="btn btn-primary" type="button">
                <i class="fas fa-search fa-sm"></i>
              </button>
            </div>
          </div>
        </form>
      </div>
    </li> --}}

    @php
        $user_role = auth()->user()->user_role;
    @endphp

    <div class="nav-item mx-1">
        <a href="https://forms.gle/dPynpj4wvSJcgrEFA" target="_blank" class="nav-link font-size2 gray2"><span class="d-inline-block mr-1"><i class="fas fa-volume-down"></i></span>  Give Feedback</a>
    </div>

    <!-- Nav Item - Alerts -->
    <li class="nav-item dropdown no-arrow mx-1">
        <a class="nav-link dropdown-toggle notificationBellButton" href="#" id="alertsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-bell fa-fw"></i>
            <!-- Counter - Alerts -->
            @if( count($user_role->unreadNotifications ?? []) )
                <span class="badge badge-danger badge-counter">{{ count($user_role->unreadNotifications ?? []) }}</span>
            @endif
        </a>
        <!-- Dropdown - Alerts -->
        <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in overflow-y-auto h-500px" aria-labelledby="alertsDropdown">
            <h6 class="dropdown-header">Notifications</h6>

            @if( count($user_role->unreadNotifications ?? []) || count($user_role->readNotifications ?? []) )
                @foreach( $user_role->unreadNotifications as $notification )

                    @component('classroom.common.notification_item', ['notification' => $notification])
                    @endcomponent

                @endforeach

                @foreach( $user_role->readNotifications as $notification )

                    @component('classroom.common.notification_item', ['notification' => $notification])
                    @endcomponent

                @endforeach
            @else
                <a class="dropdown-item text-center small text-gray-500" href="#">No Notifications yet</a>
            @endif
        </div>
    </li>

    <div class="topbar-divider d-none d-sm-block"></div>

    <!-- Nav Item - User Information -->
    <li class="nav-item dropdown no-arrow">
      <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <span class="mr-2 d-none d-lg-inline text-gray-600 small">{{ auth()->user()->username }}</span>
        <img class="img-profile rounded-circle" src="{{ auth()->user()->avatar }}">
      </a>
      <!-- Dropdown - User Information -->
      <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">

        <a class="dropdown-item" href="{{ route('settings') }}">
        <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
            Profile
        </a>
        <a class="dropdown-item" href="{{ route('payment') }}">
        <i class="far fa-credit-card fa-sm fa-fw mr-2 text-gray-400"></i>
            Payment
        </a>
        @can('view_classroom_billing', App\User::class)
            <a class="dropdown-item" href="{{ route('classroom.subscriptions.show') }}">
            <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                Billing
            </a>
        @endcan

        <a class="dropdown-item" href="{{ route('classroom.settings') }}">
        <i class="fas fa-cog fa-sm fa-fw mr-2 text-gray-400"></i>
        Settings
        </a>
        {{-- <a class="dropdown-item" href="#">
          <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
          Settings
        </a>
        <a class="dropdown-item" href="#">
          <i class="fas fa-list fa-sm fa-fw mr-2 text-gray-400"></i>
          Activity Log
        </a> --}}
        <div class="dropdown-divider"></div>
        <form action="{{ route('logout') }}" method="POST">
            @csrf
            <button class="dropdown-item" href="{{ route('logout') }}">
                <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                Logout
            </button>
        </form>
      </div>
    </li>
  </ul>

</nav>
<!-- End of Topbar -->
