<?php

namespace App\Http\Controllers;

use App\Rules\ValidFacebookUrl;
use App\Rules\ValidInstagramUrl;
use App\Rules\ValidYoutubeUrl;
use App\State;
use Illuminate\Support\Facades\Session;

class TeacherController extends Controller
{
    /**
     * Returns social links in JSON format to be stored
     */
    public function socialLinksToJson($facebook_url, $instagram_url, $youtube_url)
    {
        return [
            'facebook' => $facebook_url,
            'instagram' => $instagram_url,
            'youtube' => $youtube_url,
        ];
    }

    /**
     * Teacher-specific controller functions
     */
    public function getGettingStartedView()
    {
        $states = State::all();
        return view('getting_started.teacher', compact('states'));
    }

    public function edit()
    {
        $states = State::all();
        return view('classroom.profile.edit', compact('states'));
    }

    /**
     * Update Teacher's profile
     */
    public function update()
    {
        // Validation
        request()->validate([
            'avatar' => 'nullable|mimes:jpeg,png,jpg,gif,svg',
            'name' => 'required',
            'headline' => 'required',
            'gender' => 'required',
            'year_of_birth' => 'required',
            'about' => 'nullable|string|min:50|max:1000',
            'state' => 'nullable',
            'phone_number' => 'nullable',
            'school' => 'nullable|max:191',
            'teaching_experience' => 'nullable|string|min:50|max:1000',
            'teaching_approach' => 'nullable|string|min:50|max:1000',
            'years_of_teaching_experience' => 'nullable',
            'qualifications' => 'nullable|max:191',
            'facebook' => ['nullable', new ValidFacebookUrl],
            'instagram' => ['nullable', new ValidInstagramUrl],
            'youtube' => ['nullable', new ValidYoutubeUrl],
        ]);

        $user = auth()->user();
        $teacher = $user->teacher;

        // User update
        $user->update([
            'name' => request('name'),
            'state_id' => request('state'),
            'headline' => request('headline'),
            'about' => request('about'),
        ]);

        // Update teacher's subjects
        $user->subjects()->sync(request('subjects'));

        // Update Avatar if has profile image in request
        $has_avatar = request()->has('avatar') && !is_null( request()->file('avatar') );
        if( $has_avatar ) {
            $user->updateAvatar( request()->file('avatar') );
        }

        // Get social links in JSON
        $social_links_json = $this->socialLinksToJson(request('facebook'), request('instagram'), request('youtube'));

        // Teacher update
        $teacher->update([
            'gender' => request('gender'),
            'year_of_birth' => request('year_of_birth'),
            'phone_number' => request('phone_number'),
            'school' => request('school'),
            'teaching_experience' => request('teaching_experience'),
            'teaching_approach' => request('teaching_approach'),
            'years_of_teaching_experience' => request('years_of_teaching_experience'),
            'qualifications' => request('qualifications'),
            'social_links' => $social_links_json,
        ]);

        Session::flash('success', 'Profile updated successfully!');
        return request()->getting_started ? redirect()->route('login_choose_redirect')
                                          : redirect()->back();
    }
}
