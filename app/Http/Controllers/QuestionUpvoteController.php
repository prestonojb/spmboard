<?php

namespace App\Http\Controllers;

use App\Question;
use App\QuestionUpvote;
use Amplitude;

class QuestionUpvoteController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function toggleUpvote(Question $question)
    {
        $user = auth()->user();
        $upvoted = QuestionUpvote::where('user_id', $user->id)->where('question_id', $question->id)->exists();
        if(!$upvoted){
            $this->store($question);

            // Log event on Amplitude
            Amplitude::setUserId($user->id);
            Amplitude::queueEvent('upvote_question', ['question_id' => $question->id]);

            return 'upvoted';
        } else{
            $this->destroy($question);
            return 'unupvoted';
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Question $question)
    {
        QuestionUpvote::create([
            'user_id' => auth()->user()->id,
            'question_id' => $question->id
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Question $question)
    {
        QuestionUpvote::where('user_id', auth()->user()->id)->where('question_id', $question->id)->delete();
    }
}
