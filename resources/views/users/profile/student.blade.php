@component('forum.users.profile.common.main', ['user' => $student->user])
@endcomponent

<div class="py-1"></div>

<div class="d-block d-lg-none">
    @component('student.profile.more', ['student' => $student])
    @endcomponent
</div>

<div class="d-block d-lg-none py-1"></div>

@component('forum.users.profile.common.pills', ['user' => $student->user])
@endcomponent
