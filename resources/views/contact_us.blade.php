@extends('main.layouts.app')

@section('meta')
<title>Contact Us | {{ config('app.name') }}</title>
<meta name="description" content="Write to us and we will reply to you ASAP! You can also email to us at hello@spmboard.com.">
@endsection

@section('content')

<div class="py-2"></div>

<div class="ask-wrapper">
    <h1 class="heading">Contact Us</h1>
    <h2 class="sub-heading">Write to us and we will reply to you ASAP!</h2>
    <h3 class="text-center font-size3 gray2">You can also email to us at <a href="javascript:void(0)" style="font-size: inherit;">hello@boardedu.org</a>.</h3>

    <form method="POST" action="{{ route('contactUs.createEmail') }}" class="form-container create-question-form" enctype="multipart/form-data">
        @csrf
        <div class="form-field">
            <label>Your Name</label>
            <input name="name" value="{{ old('name') }}" required autocomplete="off" placeholder="Name">

            @error('name')
                <span class="error-message">{{ $message }}</span>
            @enderror
        </div>

        <div class="form-field">
            <label>Your Email</label>
            <input type="email" name="email" value="{{ old('email') }}" required autocomplete="off" placeholder="Email">

            @error('email')
                <span class="error-message">{{ $message }}</span>
            @enderror
        </div>

        <div class="form-field">
            <label>Subject</label>
            <select name="subject" class="subject-select">
                <option value="" data-placeholder="true" selected></option>
                @foreach( ['Product Inquiries', 'Feature Request', 'Bug Report', 'Business Inquiries', 'Others'] as $i )
                    <option value="{{ $i }}" {{ old('subject') == $i ? 'selected="selected"' : '' }}>{{ $i }}</option>
                @endforeach
            </select>
            @error('subject')
                <span class="error-message">{{ $message }}</span>
            @enderror
        </div>

        <div class="form-field" id="othersSubjectFormField">
            <label for="">Please specify the subject:</label>
            <input type="text" placeholder="Subject" name="others_subject">
            @error('others_subject')
                <span class="error-message">{{ $message }}</span>
            @enderror
        </div>

        <div class="form-field">
            <label>Message</label>
            <textarea name="body" value="{{ old('body') }}" required autocomplete="off" placeholder="Message"></textarea>

            @error('body')
                <span class="error-message">{{ $message }}</span>
            @enderror
        </div>

        <button type="submit" class="button--primary">Send Message</button>
    </form>
</div>

<div class="py-2"></div>
@endsection

@section('scripts')
<script>
$(function(){
    $('[type="submit"]').click(function(e){
        e.preventDefault();
        $(this).attr('disabled', true);
        $(this).closest('form').submit();
    });
    $othersSubjectFormField = $('#othersSubjectFormField');
    $othersSubjectFormField.hide();

    new SlimSelect({
        select: '.subject-select',
        placeholder: 'Subject',
        allowDeselectOption: true,
        onChange: (e) => {
            // console.log(e.value);
            if ( e.value == 'Others' ) {
                $othersSubjectFormField.show();
            } else {
                $othersSubjectFormField.hide();
            }
        }
    });
});
</script>
@endsection
