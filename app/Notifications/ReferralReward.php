<?php

namespace App\Notifications;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class ReferralReward extends Notification implements ShouldQueue
{
    use Queueable;

    public $referral;
    public $referee;
    public $is_referral;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(User $referral, User $referee, $is_referral)
    {
        $this->referral = $referral;
        $this->referee = $referee;
        $this->is_referral = $is_referral;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    // /**
    //  * Get the mail representation of the notification.
    //  *
    //  * @param  mixed  $notifiable
    //  * @return \Illuminate\Notifications\Messages\MailMessage
    //  */
    // public function toMail($notifiable)
    // {
    //     return (new MailMessage)
    //                 ->line('The introduction to the notification.')
    //                 ->action('Notification Action', url('/'))
    //                 ->line('Thank you for using our application!');
    // }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $title = $this->is_referral ? "You are awarded ".setting('marketing.referral_rp_amount')." reward points for referring <b>{$this->referee->name}</b>!"
                                    : "You are awarded ".setting('marketing.referral_rp_amount')." reward points registering under the referral of <b>{$this->referral->name}</b>!";
        return [
            'type' => User::class,
            'target' => $this->referee,
            'title' => $title,
        ];
    }
}
