<?php

namespace App\Policies;

use App\Resource;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ResourcePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Permission to view unlocked resource
     */
    public function view_unlocked(?User $user, Resource $resource)
    {
        // Guest
        if(!is_null($user)) {
            return $resource->isFree() || $resource->owner()->id == $user->id || $user->hasResource($resource);
        } else {
            return false;
        }
    }

    /**
     * Permission to leave review
     */
    public function leave_review(User $user, Resource $resource)
    {
        // Cannot leave review on own resource
        if($user->id == $resource->resourceable->user_id) {
            return false;
        }
        if($resource->isFree()) {
            return !$resource->reviews()->where('user_id', $user->id)->exists();
        } else {
            return $user->hasResource($resource) && !$resource->reviews()->where('user_id', $user->id)->exists();
        }
    }

    /**
     * Permission to edit resource
     */
    public function edit(User $user, Resource $resource)
    {
        return $user->isAdmin() || $resource->resourceable->user_id == $user->id;
    }

    /**
     * Permission to delete resource
     */
    public function delete(User $user, Resource $resource)
    {
        return $user->isAdmin() || $resource->resourceable->user_id == $user->id;
    }
}
