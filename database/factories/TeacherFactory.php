<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Teacher;
use App\User;
use Laravel\Cashier\Subscription;
use Faker\Generator as Faker;


$factory->define(Teacher::class, function (Faker $faker) {
    return [
        'user_id' => function () {
            return factory(User::class)->create()->id;
        }, // Can be overriden
        'reward_point_balance' => 1000,
        'phone_number' => $faker->phoneNumber,
        'school' => $faker->company,
        'year_of_birth' => rand(1960, 2000),
        'qualification' => $faker->sentence,
        'teaching_experience' => rand(1, 20)
    ];
});

// Teacher subscribed to 'classrooms'
$factory->afterCreatingState(Teacher::class, 'with_classroom_subscription', function($teacher, $faker) {
    factory(Subscription::class)->create([
        'name' => 'classrooms',
        'user_id' => Teacher::latest()->first()->user_id, // Workaround for getting teacher's user_id, assuming that there are no external data flowing in when this factory is run
    ]);
});
