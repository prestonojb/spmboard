<div class="panel homework-submission-block">
    <div class="details">
        <div class="mb-2">
            @component('classroom.common.student_avatar', ['student' => $submission->student, 'classroom' => $submission->homework->lesson->classroom, 'subtitle' => "Submitted {$submission->created_at->format('M d, Y h:iA')}"])
            @endcomponent
        </div>

        <div class="mb-2">
            {{ isset($limit) && $limit ? str_limit($submission->comment, 100) : $submission->comment }}
        </div>

        @if( isset($full_width) && $full_width )
            @component('classroom.common.file_block', ['url' => $submission->file_url])
            @endcomponent
        @else
            <div class="row">
                <div class="col-12 col-lg-6">
                    @component('classroom.common.file_block', ['url' => $submission->file_url])
                    @endcomponent
                </div>
            </div>
        @endif

        <div class="d-flex justify-content-between align-items-center">
            {!! $submission->status_tag_html !!}
            <div>
                <div class="blue5 font-size5">
                    <b>
                        @if( $submission->hasMarks() )
                            {{ $submission->marks_in_percentage }}%
                        @else
                            Ungraded
                        @endif
                    </b>
                </div>
            </div>
            @if( isset($cta) )
                <div>
                    {{ $cta }}
                </div>
            @endif

        </div>
    </div>

</div>
