<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CoinProduct extends Model
{
    /**
     * --------------
     * ACCESSORS
     * --------------
     */
    /**
     * Returns Board Pass
     * @return App\CoinProduct
     */
    public static function getBoardPass()
    {
        return CoinProduct::where('name', 'Board Pass')->first();
    }

    /**
     * Returns true if coin product is Board Pass
     * @return boolean
     */
    public function isBoardPass()
    {
        return $this->name == 'Board Pass';
    }

    /**
     * ----------------------
     * BOARD PASS FUNCTIONS
     * ----------------------
     */
    /**
     * Returns Board Pass price (with discount) (rounded-up)
     * @param $discountInPercentage
     * @return int
     */
    public function getBoardPassPrice($no_of_months)
    {
        if(!$this->isBoardPass()) {
            throw new \Exception("Coin Product must be Board Pass!");
        }

        $base_price = $this->unit_price_in_coin;
        $discount_in_percent = $this->getBoardPassDiscount($no_of_months);

        // Get discounted price (in float)
        $discounted_price = $base_price * ((100-$discount_in_percent)/100);
        // Get discounted price (in int)
        return ceil($discounted_price * $no_of_months);
    }

    /**
     * Returns Board Pass discount (in %) based on number of months of subscription
     * @param int $no_of_months
     * @return int
     * Example:
     * Threshold 1: 6 mo
     * Threshold 2: 12 mo
     * Tier 1 discount: 10%
     * Tier 2 discount: 20%
     *
     * Therefore,
     * 1-6 mo: 0% discount
     * 7-12 mo: 10% discount
     * >12 mo: 20% discount
     *
     */
    public function getBoardPassDiscount($no_of_months)
    {
        // Pre-condition
        if($no_of_months < 1) {
            throw new \Exception("Number of months must be at least 1.");
        }

        $threshold_1 = setting('board_pass.tier_1_threshold_in_month');
        $threshold_2 = setting('board_pass.tier_2_threshold_in_month');

        if($no_of_months >= 1 && $no_of_months <= $threshold_1) {
            return 0;
        } elseif($no_of_months >= ($threshold_1 + 1) && $no_of_months <= $threshold_2) {
            return setting('board_pass.tier_1_discount');
        } else {
            return setting('board_pass.tier_2_discount');
        }
    }
}
