<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ErrorFeedback extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($email, $feedback)
    {
        $this->email = $email;
        $this->feedback = $feedback;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.error_feedback')
                    ->from( !is_null($this->email) ? $this->email : config('mail.from.address') )
                    ->subject('Error Feedback')
                    ->with([
                        'feedback' => $this->feedback,
                    ]);
    }
}
