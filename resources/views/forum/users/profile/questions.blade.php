@extends('forum.users.profile.layouts.app')

@section('meta')
    <title>Questions - {{ $user->username }} | {{ config('app.name') }}</title>
    <meta property="og:image" content="{{ $user->avatar }}" />
    <meta property="og:type" content="website" />
@endsection

@section('styles')
    {{-- Magnific Popup core CSS file --}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.css">
@endsection

@section('main')
    {{-- Metrics --}}
    @php
        $data = [
            ['key' => 'Upvotes',
            'value' => $user->getQuestionUpvoteCount(),
            'icon' => 'fas fa-arrow-up fa-2x',
            'color' => 'blue6'],
            ['key' => 'Recommended',
            'value' => $user->getQuestionRecommendationsCount(),
            'icon' => 'fas fa-star fa-2x',
            'color' => 'yellow']
        ];
    @endphp
    <div class="form-row border-bottom pb-3 mb-3">
        @foreach($data as $datum)
            <div class="col-12 col-lg-4 mb-2">
                @component('forum.users.profile.common.key_value_block', ['datum' => $datum])
                @endcomponent
            </div>
        @endforeach
    </div>

    @if(count($questions ?? []))
        <div class="user-question-container user-questions">
            @foreach($questions as $question)
                @include('forum.common.question-block')
            @endforeach
        </div>
    @else
        <section class="text-center mt-4">
            <img src="{{ asset('img/essential/reading.svg') }}" alt="No Questions Found" width="220" height="220" class="mb-2">
            <h2 class="gray2 font-size4">No questions by user yet...</h2>
        </section>
    @endif
    {{ $questions->links() }}
@endsection
