<!-- Sidebar -->
<ul class="navbar-nav sidebar accordion toggled" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{ route('classroom.classrooms.index') }}">
      <div class="sidebar-brand-icon p-2">
        <img class="w-100" src="{{ asset('img/logo/logo_light_icon.png') }}" alt="">
      </div>

      <div class="sidebar-brand-text mx-1">
          <img class="w-100" src="{{ asset('img/logo/logo_light.png') }}" alt="">
      </div>
    </a>

    <!-- Heading -->
    <div class="sidebar-heading">
        General
    </div>

    @can('index', App\Classroom::class)
        <!-- Nav Item - Dashboard -->
        <li class="nav-item @if(request()->is('classroom/classrooms')) active @endif">
        <a class="nav-link" href="{{ route('classroom.classrooms.index') }}">
            <span class="iconify" data-icon="mdi:teach" data-inline="false"></span>
            <span>Classes</span></a>
        </li>
    @endcan

    @can('view_children', App\Student::class)
        <li class="nav-item @if(request()->is('classroom/children') || request()->is('classroom/children/*')) active @endif">
            <a class="nav-link" href="{{ route('classroom.children.index') }}">
                <span class="iconify" data-icon="bi:person" data-inline="false"></span>
                <span>Children</span></a>
        </li>
    @endcan

    @can('view_calendar', App\Classroom::class)
        <!-- Nav Item - Dashboard -->
        <li class="nav-item @if(request()->is('classroom/calendar')) active @endif">
        <a class="nav-link" href="{{ route('classroom.calendar') }}">
            <span class="iconify" data-icon="carbon:calendar" data-inline="false"></span>
            <span>Calendar</span></a>
        </li>
    @endcan

    <hr class="sidebar-divider">

    <!-- Heading -->
    <div class="sidebar-heading">
      Manage
    </div>

    @can('classroom_index', App\Student::class)
        <!-- Nav Item - Dashboard -->
        <li class="nav-item @if(request()->is('classroom/students') || request()->is('classroom/students/*')) active @endif">
            <a class="nav-link" href="{{ route('classroom.students.index') }}">
                <span class="iconify" data-icon="bi:person" data-inline="false"></span>
                <span>Students</span></a>
        </li>
    @endcan

    @can('index', App\StudentReport::class)
        <li class="nav-item @if(request()->is('classroom/reports') || request()->is('classroom/reports/*')) active @endif">
            <a class="nav-link" href="{{ route('classroom.reports.index') }}">
                <span class="iconify" data-icon="carbon:report" data-inline="false"></span>
                <span>Reporting</span></a>
        </li>
    @endcan

    {{-- @can('index', App\TeacherInvoice::class)
        <li class="nav-item @if(request()->is('classroom/invoices') || request()->is('classroom/invoices/*')) active @endif">
            <a class="nav-link" href="{{ route('classroom.invoices.index') }}">
                <span class="iconify" data-icon="la:file-invoice-solid" data-inline="false"></span>
                <span>Invoicing</span></a>
        </li>
    @endcan --}}

    @can('view_progress', App\Student::class)
        <li class="nav-item @if(request()->is('classroom/students') || request()->is('classroom/students/*')) active @endif">
            <a class="nav-link" href="{{ route('classroom.students.show', auth()->user()->id) }}">
                <span class="iconify" data-icon="ant-design:line-chart-outlined" data-inline="false"></span>
                <span>My Progress</span></a>
        </li>
    @endcan

    <hr class="sidebar-divider">
    <div class="sidebar-heading">
        More
    </div>
    {{-- Forum & Resources --}}
    <li class="nav-item">
        <a class="nav-link" href="{{ route('forum.landing') }}">
            <span class="iconify" data-icon="cil:newspaper" data-inline="false"></span>
            <span>Forum and Resources</span></a>
    </li>
    {{-- Exam Builder --}}
    @if(Gate::allows('access-exam-builder'))
        <li class="nav-item">
        <a class="nav-link" href="{{ route('exam_builder.landing') }}">
                <span class="badge badge-primary mb-2">BETA</span>
                <span class="iconify" data-icon="carbon:model-builder" data-inline="false"></span>
                <span>Exam Builder</span>
            </a>
        </li>
    @endif
    <li class="nav-item">
        <a class="nav-link" href="{{ route('/') }}">
            <span class="iconify" data-icon="subway:world" data-inline="false"></span>
            <span>Back to Board</span></a>
    </li>


    {{-- <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    <div class="sidebar-heading">
      Interface
    </div>

    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item">
      <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
        <i class="fas fa-fw fa-cog"></i>
        <span>Components</span>
      </a>
      <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
          <h6 class="collapse-header">Custom Components:</h6>
          <a class="collapse-item" href="buttons.html">Buttons</a>
          <a class="collapse-item" href="cards.html">Cards</a>
        </div>
      </div>
    </li>

    <!-- Nav Item - Utilities Collapse Menu -->
    <li class="nav-item">
      <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">
        <i class="fas fa-fw fa-wrench"></i>
        <span>Utilities</span>
      </a>
      <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
          <h6 class="collapse-header">Custom Utilities:</h6>
          <a class="collapse-item" href="utilities-color.html">Colors</a>
          <a class="collapse-item" href="utilities-border.html">Borders</a>
          <a class="collapse-item" href="utilities-animation.html">Animations</a>
          <a class="collapse-item" href="utilities-other.html">Other</a>
        </div>
      </div>
    </li>


    <!-- Heading -->
    <div class="sidebar-heading">
      Addons
    </div>

    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item">
      <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages" aria-expanded="true" aria-controls="collapsePages">
        <i class="fas fa-fw fa-folder"></i>
        <span>Pages</span>
      </a>
      <div id="collapsePages" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
          <h6 class="collapse-header">Login Screens:</h6>
          <a class="collapse-item" href="login.html">Login</a>
          <a class="collapse-item" href="register.html">Register</a>
          <a class="collapse-item" href="forgot-password.html">Forgot Password</a>
          <div class="collapse-divider"></div>
          <h6 class="collapse-header">Other Pages:</h6>
          <a class="collapse-item" href="404.html">404 Page</a>
          <a class="collapse-item" href="blank.html">Blank Page</a>
        </div>
      </div>
    </li>

    <!-- Nav Item - Charts -->
    <li class="nav-item">
      <a class="nav-link" href="charts.html">
        <i class="fas fa-fw fa-chart-area"></i>
        <span>Charts</span></a>
    </li>

    <!-- Nav Item - Tables -->
    <li class="nav-item">
      <a class="nav-link" href="tables.html">
        <i class="fas fa-fw fa-table"></i>
        <span>Tables</span></a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">

    --}}

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle" id="sidebarToggle"></button>
      </div>

  </ul>
  <!-- End of Sidebar -->
