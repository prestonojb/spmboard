@extends('classroom.layouts.app')

@section('meta')
    <title>Children | {{ config('app.name') }} Classroom</title>
@endsection

@section('content')
@component('classroom.common.breadcrumbs')
    Home
@endcomponent

@component('classroom.common.page_heading', ['title' => 'Children'])
@endcomponent

<div class="row">
    <div class="col-12 col-lg-9 mb-3">

        <div class="row">
            @foreach( $children as $child )
                <div class="col-12 col-md-6 col-lg-4 mb-2">
                    @component('classroom.children.common.block', ['child' => $child])
                    @endcomponent
                </div>
            @endforeach
        </div>

    </div>

    <div class="col-12 col-lg-3">
        @component('classroom.common.timeline_block', ['timeline_items' => $timeline_items])
        @endcomponent
    </div>
</div>

<div class="py-1"></div>
@endsection
