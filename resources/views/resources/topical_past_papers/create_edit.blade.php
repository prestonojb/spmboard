@extends('forum.layouts.app')

@section('meta')
<title>{{ $is_edit ? 'Edit Topical Worksheets' : 'Upload Topical Worksheets' }} | {{ config('app.name') }}</title>
@endsection

@section('styles')
@endsection

@section('content')
@include('common.header2')

<div class="py-3"></div>

<div class="container">
    <div class="row">
        <div class="col-12 col-lg-9 mb-2">
            <div class="maxw-740px mx-auto">
                <a class="d-block mb-2 inactive font-size2 gray2" href="{{ route('/') }}">< Back to Home</a>

                <h1 class="font-size7">{{ $is_edit ? 'Edit Topical Worksheet' : 'Upload Topical Worksheet' }}</h1>

                {{-- Pills --}}
                @component('resources.common.pills', ['type' => 'topical_past_paper', 'exam_board' => $exam_board])
                @endcomponent

                <div class="panel">
                    {{-- Create --}}
                    @if(!$is_edit)
                        @component('resources.topical_past_papers.common.form', ['exam_board' => $exam_board, 'is_edit' => false])
                        @endcomponent
                    {{-- Edit --}}
                    @else
                        @component('resources.topical_past_papers.common.form', ['exam_board' => $exam_board, 'topical_past_paper' => $topical_past_paper, 'is_edit' => true])
                        @endcomponent
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

<div class="py-3"></div>
@endsection

@section('js_dependencies')
{{-- TinyMCE --}}
<script src="https://cdn.tiny.cloud/1/si4jvonmpvtlfypotvf9zu2vci2zsjavfd4v1oyvnzncszag/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
{{-- KaTeX --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ" crossorigin="anonymous"></script>
@endsection
