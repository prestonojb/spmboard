<section>
    <div class="mb-2">
        <h2 class="font-size5 font-weight500 mb-1">{{ $title }}</h2>
        @if(isset($description) && !is_null($description))
            <p class="gray2 font-size2">{{ $description }}</p>
        @endif
    </div>

    {{ $slot }}
</section>
