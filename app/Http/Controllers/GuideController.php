<?php

namespace App\Http\Controllers;

use App\Guide;
use App\ExamBoard;
use App\Subject;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class GuideController extends Controller
{
    public function getHomeView(ExamBoard $exam_board)
    {
        $subjects = $exam_board->subjects()->whereHas('guides')->get();
        return view('resources.guides.home', compact('exam_board', 'subjects'));
    }

    public function subjectShow(ExamBoard $exam_board, Subject $subject)
    {
        $chapters = $subject->chapters()->whereHas('guides')->orderBy('chapter_number')->get();
        return view('resources.guides.subject_show', compact('exam_board', 'subject', 'chapters'));
    }

    public function show(ExamBoard $exam_board, Guide $guide)
    {
        // Record views
        if(!is_null($guide->resource)) {
            views($guide->resource)->record();
        }
        return view('resources.guides.show', compact('exam_board', 'guide'));
    }

    /**
     * Delete guide
     */
    public function delete(ExamBoard $exam_board, Guide $guide)
    {
        $subject = $guide->subject;
        $guide->delete();

        Session::flash('success', 'Guide deleted successfully!');
        return redirect()->route('guides.subject_show', [$exam_board->slug, $subject->id]);
    }
}
