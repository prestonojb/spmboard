var $form = $('#createLessonForm');
// Variables to configure datepicker ranges
var minDate = moment();
var maxDate = minDate.clone();
maxDate.add(14, 'day');

var $startTimeDtPicker = $form.find('#startTimeDtPicker');
var $endTimeDtPicker = $form.find('#endTimeDtPicker');

// Initialise datepickers
$startTimeDtPicker.datetimepicker({
    useCurrent: false,
    format: 'DD/MM/YYYY hh:mm A',
    minDate: minDate,
    maxDate: maxDate,
});

$endTimeDtPicker.datetimepicker({
    useCurrent: false,
    format: 'DD/MM/YYYY hh:mm A',
    minDate: minDate,
    maxDate: maxDate,
});

// Ensure that endtime > starttime
$startTimeDtPicker.on("change.datetimepicker", function (e) {
    $endTimeDtPicker.datetimepicker('minDate', e.date);
});

$endTimeDtPicker.on("change.datetimepicker", function (e) {
    $startTimeDtPicker.datetimepicker('maxDate', e.date);
});
