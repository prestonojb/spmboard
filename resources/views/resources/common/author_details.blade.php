{{-- Resource Owner Credentials --}}
<div class="py-3 d-flex justify-content-start align-items-start">
    <a href="{{ $user->show_url }}">
        <img class="d-block profile-img mt-1 mr-2 mr-lg-3" src="{{ $user->avatar }}">
    </a>

    <div class="">
        <p class="gray2 font-size1 text-uppercase mb-1">Contributed By</p>
        <a href="{{ $user->show_url }}">
            <h4 class="font-size5 mb-2"><b>{!! $user->username_with_badge !!}</b></h4>
        </a>
        @if(!is_null($user->headline))
            <h5 class="font-weight-normal gray2 font-size3 mb-2">{{ $user->headline }}</h4>
        @endif

        @if(!is_null($user->about))
            {!! nl2br($user->about) !!}
        @endif
    </div>
</div>
