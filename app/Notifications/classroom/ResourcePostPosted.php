<?php

namespace App\Notifications\Classroom;

use App\Lesson;
use App\ResourcePost;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

/**
 * Notify users that resource post is posted in classroom
 *
 */
class ResourcePostPosted extends Notification implements ShouldQueue
{
    use Queueable;

    public $lesson;
    public $resource_post;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Lesson $lesson, ResourcePost $resource_post)
    {
        $this->lesson = $lesson;
        $this->resource_post = $resource_post;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $lesson = $this->lesson;
        $classroom = $lesson->classroom;
        $teacher = $classroom->teacher;
        $resource_post = $this->resource_post;

        $title = "<b>{$teacher->name}</b> added a new resource post in <b>{$classroom->name}</b>";

        return [
            'type' => ResourcePost::class,
            'target' => $resource_post,
            'title' => $title,
        ];
    }
}
