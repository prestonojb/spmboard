<div class="panel ml-2 mb-3" id="answer_{{ $answer->id }}">
    <div class="question-block__header">
        <label class="d-block">Answer</label>
    </div>

    <div class="answer__user-header">
        <div class="answer-block__user-info">
            <a href="{{ route('users.show', $answer->user->username) }}">
                <img class="avatar" src="{{ $answer->user->avatar ?? asset('img/essential/no-profile-picture.jpg') }}">
            </a>
            <div class="mt-1 text">
                <div class="user-info"><a class="username" href="{{ route('users.show', $answer->user->username) }}">{{ $answer->user->username }}</a><span class="about">@if($answer->user->about), {{ $answer->user->about }} @endif</span></div>
                <span class="time">Answered {{ $answer->created_at->longRelativeDiffForHumans() }}</span>
            </div>
        </div>

        <div class="btn-group" role="group">
            <button type="button" class="more-button" data-toggle="dropdown">
                <i class="fa fa-ellipsis-h" aria-hidden="true"></i>
            </button>

            <div class="dropdown-menu dropdown-menu-right">
                @can('edit', $answer)
                    <a class="dropdown-item" href="{{ route('classroom.answers.edit', [$classroom->id, $answer->id]) }}">Edit answer</a>
                @endcan
                @can('delete', $answer)
                    <form method="POST" action="{{ route('answers.delete', $answer->id) }}">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="dropdown-item text-danger delete-button">Delete answer</button>
                    </form>
                @endcan
            </div>
        </div>
    </div>

    <div class="answer">
        {!! $answer->answer !!}
        @if($answer->img)
            <div class="answer__image-container">
                <img src="{{ $answer->img }}" class="answer-img mb-2">
            </div>
        @endif
    </div>
</div>
