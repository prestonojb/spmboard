<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Classroom extends Model
{
    protected $guarded = [];

    /**
     * ---------------
     * Relationships
     * ---------------
     */
    public function subject()
    {
        return $this->belongsTo(Subject::class);
    }

    public function teacher()
    {
        return $this->belongsTo(Teacher::class, 'teacher_id');
    }

    public function students()
    {
        return $this->belongsToMany(Student::class, 'classroom_student', 'classroom_id', 'student_id', 'id', 'user_id')->withTimestamps();
    }

    public function posts()
    {
        return $this->hasMany(ClassroomPost::class);
    }

    /**
     * Returns all parents in classroom
     * @return Collection
     */
    public function getParentsAttribute()
    {
        return $this->parents();
    }

    /**
     * Returns all parents in classroom
     * @return Collection
     */
    public function parents()
    {
        // Retrieve all parent IDs, then filter out 'nulls' from array
        $parent_ids = $this->students()->pluck('parent_id')->toArray();
        $parent_ids = array_filter($parent_ids);

        return StudentParent::find($parent_ids);
    }

    public function lessons()
    {
        return $this->hasMany( Lesson::class )->latest();
    }

    public function getLessonIds()
    {
        return $this->lessons()->pluck('id')->toArray();
    }

    /**
     * ---------------
     * Accessors
     * ---------------
     */
    public function getResourcePostsAttribute()
    {
        return $this->resource_posts();
    }

    public function getHomeworksAttribute()
    {
        return $this->homeworks();
    }

    public function resource_posts()
    {
        $lesson_ids = $this->getLessonIds();
        return ResourcePost::whereIn('lesson_id', $lesson_ids)->get();
    }


    public function homeworks()
    {
        $lesson_ids = $this->getLessonIds();
        return Homework::whereIn('lesson_id', $lesson_ids)->get();
    }

    /**
     * ---------------
     * Helpers
     * ---------------
     */
    public function isTaughtBy(Teacher $teacher)
    {
        return $this->teacher_id == $teacher->user_id;
    }

    public function isEmpty()
    {
        return $this->students->count() == 0;
    }


    /**
     * Returns true if student is in classroom
     * @param App\Student
     * @return boolean
     */
    public function hasStudent( Student $student )
    {
        return $this->students->contains( $student->user_id );
    }

    /**
     * Returns true if all students in array is in classroom
     * @param Collection
     * @return boolean
     */
    public function hasStudents( $students )
    {
        foreach($students as $student) {
            if(!$this->hasStudent($student)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Add student to classroom
     * @param App\Student $student
     */
    public function addStudent( Student $student )
    {
        $this->students()->syncWithoutDetaching( $student->user_id );
    }

    /**
     * Returns all the parents in classroom
     * @return Collection
     */
    public function getParents()
    {
        $parents = collect();
        foreach( $this->students as $student ) {
            $parents->push( $student->parent );
        }
        return $parents;
    }

    /**
     * Returns true if classroom has parent, else false
     * @return boolean
     */
    public function hasParent( StudentParent $parent )
    {
        return $this->getParents()->contains( $parent );
    }

    public function get_parents_email_list()
    {
        $students = $this->students;
        foreach( $students as $student )
        {
            if( $student->parent && $student->parent->email ) {
                $parent_email = $student->parent->email;
            }
            array_push($email_list, $parent_email);
        }
        return $email_list;
    }

    /**
     * --------------
     * Dashboard
     * --------------
     */
    public function getDashboardLessonItems()
    {
        return $this->lessons()->orderByDesc('created_at')->take(5)->get();
    }

    public function getDashboardResourcePostItems()
    {
        $lesson_ids = $this->getLessonIds();
        return ResourcePost::whereIn('lesson_id', $lesson_ids)->orderByDesc('created_at')->take(5)->get();
    }

    public function getDashboardHomeworkItems()
    {
        $lesson_ids = $this->getLessonIds();
        return Homework::whereIn('lesson_id', $lesson_ids)->orderByDesc('created_at')->take(5)->get();
    }

    public function getDashboardStudentItems()
    {
        return $this->students;
    }

    /**
     * Returns true if classroom has teacher
     * @param App\Teacher $teacher
     * @return boolean
     */
    public function hasTeacher(Teacher $teacher)
    {
        return $this->teacher_id == $teacher->user_id;
    }
}
