<nav class="navbar header">
    <div class="container">
        <a class="header-brand" href="{{ route('parent.home') }}">
            <img src="{{ asset('img/logo/logo_light.png') }}">
        </a>

        <ul class="header-right-menu">
            {{-- Notifications --}}
            <div class="dropdown">
                <button type="button" class="header-icon bell-icon mr-1 mr-md-2" data-toggle="dropdown"><span class="iconify" data-icon="ic:baseline-notifications-none" data-inline="false"></span></button>
                <div class="dropdown-menu notification-dropdown-menu">
                    <h6 class="h3 dropdown-header">Notifications</h6>
                    <div class="notification-dropdown-list">
                        @if(auth()->user()->parent->notifications->count())
                            {{-- Unread Notification --}}
                            @foreach(auth()->user()->parent->unreadNotifications as $notification)
                                @component('parent.common.notification_item', ['notification' => $notification, 'unread' => true])
                                @endcomponent
                            @endforeach
                            {{-- Read Notification --}}
                            @foreach(auth()->user()->parent->readNotifications as $notification)
                                @component('parent.common.notification_item', ['notification' => $notification])
                                @endcomponent
                            @endforeach
                        @else
                            <div class="w-100 h-100 d-flex flex-column align-items-center justify-content-center">
                                <img src="{{ asset('img/essential/no-notification.svg') }}" class="empty">
                                <h6>No notifications yet</h6>
                            </div>
                        @endif
                    </div>
                </div>
            </div>

            <div class="dropdown">
                <button type="button" class="bg-transparent mr-1 mr-md-2 p-0" data-toggle="dropdown">
                    <img class="avatar" src="{{ auth()->user()->avatar }}">
                    <span>
                        <i class="fa fa-caret-down" aria-hidden="true"></i>
                    </span>
                </button>
                <div class="dropdown-menu right">
                    <form method="POST" action="{{ route('logout') }}">
                        @csrf
                        <button type="submit" class="dropdown-item">Log Out</button>
                    </form>
                </div>
            </div>

        </ul>
    </div>
</nav>

