<?php

namespace App\Policies;

use App\ClassroomPost;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ClassroomPostPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Permission to show post in classroom
     */
    public function show(User $user, ClassroomPost $post)
    {
        $classroom = $post->classroom;
        if( $user->is_teacher() ) {
            return $user->id == $classroom->teacher_id;
        }
        if( $user->is_student() ) {
            return $classroom->students->contains( $user->id );
        }
        return false;
    }

    /**
     * Permission to update post in classroom
     */
    public function update(User $user, ClassroomPost $post)
    {
        return $post->user_id == $user->id;
    }

    /**
     * Permission to delete post in classroom
     */
    public function delete(User $user, ClassroomPost $post)
    {
        return $post->user_id == $user->id;
    }
}
