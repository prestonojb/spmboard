<?php

namespace App\Policies;

use App\Article;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ArticlePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Permission to create/store article
     */
    public function create_store(User $user)
    {
        return $user->is_teacher();
    }

    /**
     * Permission to edit/update article
     */
    public function edit_update(User $user, Article $article)
    {
        return $user->is_teacher() && $article->user_id == $user->id;
    }

    /**
     * Permission to delete article
     */
    public function delete(User $user, Article $article)
    {
        return $user->is_teacher() && $article->user_id == $user->id;
    }
}
