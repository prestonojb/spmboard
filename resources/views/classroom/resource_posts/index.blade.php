@extends('classroom.layouts.app')

@section('meta')
    <title>Resources | {{ config('app.name') }} Classroom</title>
@endsection


@section('styles')
@endsection

@section('content')

@component('classroom.common.breadcrumbs')
    Resources
@endcomponent

@component('classroom.common.page_heading', ['title' => 'Resource Post', 'button' => 'Post Resources' ])
    @slot('button_url')
        {{ route('classroom.lessons.resource_posts.create', $classroom->id) }}
    @endslot
@endcomponent

@foreach( $chapters as $chapter )
    <h3 class="pb-2 border-bottom"><b>{{ $chapter->name }}</b></h3>
    <div class="row">
        @foreach($chapter->resource_posts as $resource_post)
            <div class="col-12 col-lg-6">
                @include('classroom.resource_posts.common.block')
            </div>
        @endforeach
    </div>
@endforeach

@endsection

@section('scripts')
@endsection
