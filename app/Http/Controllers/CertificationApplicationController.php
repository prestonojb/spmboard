<?php

namespace App\Http\Controllers;

use App\CertificationApplication;
use Illuminate\Http\Request;

class CertificationApplicationController extends Controller
{
    /**
     * List ALL applications
     */
    public function index()
    {
        $this->authorize('index', CertificationApplication::class);
        $user = auth()->user();
        $applications = $user->certification_applications()->paginate(5);

        return view('certification_applications.index', compact('applications'));
    }

    /**
     * Get Create view for certification application
     */
    public function create()
    {
        $this->authorize('create_store', CertificationApplication::class);
        return view('certification_applications.create');
    }

    /**
     * User submit application
     */
    public function store()
    {
        $this->authorize('create_store', CertificationApplication::class);
        // Validation
        request()->validate([
            'resume' => 'required|mimes:doc,pdf,docx', // Allow .doc, .docx and .pdf files
        ]);
        $user = auth()->user();
        // Check if user is certified
        if($user->is_certified) {
            return redirect()->back()->with('error', 'You are already certified.');
        }
        // Check if user has pending application
        if($user->hasPendingApplication()) {
            return redirect()->back()->with('error', 'Your existing application will be reviewed. At the mean time, you are not allowed to submit another application.');
        }
        // Create certification application
        $application = $user->certification_applications()->create([]);

        // Save resume
        $application->saveResume(request()->file('resume'));

        return redirect()->route('certification_applications.index')->with('success', 'Application submitted!');
    }

    // /**
    //  * User update application
    //  */
    // public function update(CertificationApplication $application)
    // {
    //     // Update resume
    //     $application->saveResume(request()->file('resume'));
    //     return redirect()->back()->with('success', 'Application updated!');
    // }
}
