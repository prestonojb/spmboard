<div class="position-relative note">
    <div class="row">
        <div class="col-12 col-sm-9">
            <h3 class="note__title">{{ $note->name }}</h3>
            <h4 class="note__description">{{ $note->subject->name }}, {{ $note->chapter->name ?? $note->description }}</h4>
        </div>

        <div class="col-12 col-sm-3 note__cta">

            @if(in_array($note->id, $owned_notes_ids))

                <a href="{{ route('notes.show', [$exam_board, $note->id]) }}" class="button--primary--inverse">Claimed</a>
                {{-- <button type="button" class="button--primary--inverse view-pdf-button" data-href="{{ $note->url }}" data-toggle="modal" data-target=".pdf-modal-container">Claimed</button> --}}

            @else

                <form action="{{ route('notes.claim', $note->id) }}" method="POST">
                    @csrf
                    <button type="submit" class="button--primary"><img class="inline-image" src="{{ asset('img/essential/coin.svg') }}" alt="coin"> {{ $note->coin }}</button>
                </form>

            @endif
        </div>
    </div>
</div>

{{-- PDF Viewer Modal --}}
{{-- <div class="modal fade pdf-modal-container" tabindex="-1" role="dialog" aria-labelledby="pdfModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg pdf-modal">
        <div class="modal-content">
            <div id="my_pdf_viewer">
                <div id="canvas_container">
                    <canvas id="pdf_renderer"></canvas>
                </div>
                <div id="zoom_controls_container">
                    <div id="zoom_controls">
                        <button id="zoom_in">+</button>
                        <button id="zoom_out">-</button>
                    </div>
                </div>
                <div id="navigation_controls">
                    <button id="go_previous">Previous</button>
                    <input id="current_page" value="1" type="number"/>
                    <button id="go_next">Next</button>
                </div>
            </div>
        </div>
    </div>
</div> --}}
