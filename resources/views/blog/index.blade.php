@extends('main.layouts.app')

@section('meta')
    <title>Blog | {{ config('app.name') }}</title>
    <meta name="description" content="A space for peers, seniors, teachers to share experience and opinions on SPM."/>

    <meta property="og:image" content="{{ asset('img/essential/og-logo.png') }}" />
    <meta property="og:type" content="article" />
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-12 p-2">
            <section class="text-center blog-index-header py-4">
                <h1><strong>Blog</strong></h1>
                <h2 class="h3">Welcome to Board Blog, where quality educational tips are shared.</h2>
                <p>If you are interested in blogging with us, please reach to us at <a style="font-size:inherit" href="mailto:hello@boardedu.org">hello@boardedu.org</a>.</p>
            </section>

            <div class="mb-2">
                @include('blog.common.blog-tags')
            </div>

            <div class="row mb-1">
                @foreach($blogs as $blog)
                    <div class="col-12 col-lg-4 mb-2">
                        @component('blog.common.block', ['blog' => $blog])
                        @endcomponent
                    </div>
                @endforeach
            </div>
        </div>
        @if($blogs instanceof \Illuminate\Pagination\LengthAwarePaginator)
            {{ $blogs->links() }}
        @endif
    </div>
</div>
@include('main.navs.footer')
@endsection
