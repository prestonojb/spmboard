<section class="">
    <div class="container">
        <div class="py-5">
            <div class="row @if($reverse) flex-md-row-reverse @endif">
                <div class="col-12 col-lg-6 mb-3">
                    <div class="px-3 mb-5">
                        <div class="px-5">
                            <img src="{{ $img }}" alt="">
                        </div>
                    </div>
                </div>

                <div class="col-12 col-lg-6 mb-3">
                    <h6 class="gray2 text-uppercase font-size3 mb-3">{{ $subtitle }}</h6>
                    <h5 class="font-size9">
                        <b>{{ $title }}</b>
                    </h5>
                    <h6 class="font-weight400 mb-3">
                        {{ $description }}
                    </h6>

                    {{ $slot }}
                    <a href="{{ $cta_url }}" class="button--primary lg">{{ $cta_text }}</a>
                </div>
            </div>
        </div>
    </div>
</section>
