@extends('main.layouts.app')

@section('meta')
<title>My Coins | {{ config('app.name') }}</title>
<meta name="description" content="Manage your coins here.">
@endsection


@section('content')
<div class="py-2"></div>

<div class="container">
    <div class="py-2"></div>

    @can('withdraw_coins', $user)

        <div class="alert alert-info font-size3 mb-0" role="alert">
            Ensure that the phone number attached to this account has DuitNow activated (Malaysia only). <br>
            Attached Phone Number: <u>{!! !is_null($user->phone_number) ? $user->phone_number : '<i>Unset</i>' !!}</u> <a class="blue5 underline-on-hover" href="{{ route('settings') }}">Change</a>
        </div>

        <div class="py-1"></div>

        @component('components.panel', ['title' => 'Coin Balance'])
            <div class="d-flex justify-content-start align-items-center">
                <img src="{{ asset('img/essential/coin.svg') }}" class="mr-2" alt="coin" width="40" height="40">
                <span class="mr-3"><span class="font-size7"><b>{{ $user->coin_balance }}</b></span> coins</span>

                <form action="{{ route('withdrawal_requests.store') }}" method="post">
                    @csrf
                    <button type="submit" class="button--danger lg confirmButton" data-title="Are you sure?" data-text="You are withdrawing {{ auth()->user()->coin_balance }} coins. Money will be transferred to your bank account associated with your phone number's Duitnow within 5 working days.">Withdraw</button>
                </form>
            </div>

            <div class="py-1"></div>

            <p class="gray2 mb-0">Estimated payout: <b>USD{{ convertUsdToCoinSell($user->coin_balance) }}</b></p>
        @endcomponent

        <div class="py-2"></div>
        <hr>

        {{-- Withdrawal History --}}
        @component('components.section', ['title' => 'Withdrawal History'])
            @component('users.withdrawal_requests.common.table', ['withdrawal_requests' => $withdrawal_requests])
            @endcomponent
        @endcomponent
    @else
        @component('components.panel', ['title' => 'Coin Balance'])
            <div class="d-flex justify-content-start align-items-center">
                <img src="{{ asset('img/essential/coin.svg') }}" class="mr-2" alt="coin" width="40" height="40">
                <span class="mr-3"><span class="font-size7"><b>{{ $user->coin_balance }}</b></span> coins</span>
            </div>
        @endcomponent
    @endcan

    <div class="py-2"></div>
    <hr>

    {{-- Coin Action --}}
    @component('components.section', ['title' => 'Coin History', 'description' => 'Showing 15 latest transactions'])
        @component('users.coins.common.history_table', ['coin_actions' => $coin_actions])
        @endcomponent
    @endcomponent

</div>

<div class="py-2"></div>
@endsection

