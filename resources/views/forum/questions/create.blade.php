@extends('forum.layouts.app')

@section('meta')
<title>Ask a question | {{ $exam_board->name }} {{ config('app.name') }}</title>
@endsection

@section('content')
@include('common.header2')

<div class="py-3"></div>

<div class="container">
    <div class="row">
        <div class="col-12 col-lg-9 mb-2">
            <div class="maxw-740px mx-auto">
                <a class="d-block mb-2 inactive font-size2 gray2" href="{{ route('questions.index', $exam_board->slug) }}">< Back to News Feed</a>

                <h1 class="font-size7">Ask a Question</h1>

                <div class="panel">
                    @component('forum.questions.common.form', ['is_edit' => false, 'subjects' => $subjects, 'exam_board' => $exam_board])
                    @endcomponent
                </div>
            </div>
        </div>
        <div class="col-12 col-lg-3 mb-2">
            @include('forum.questions.common.asking_guidelines')
        </div>
    </div>
</div>

<div class="py-3"></div>
@endsection

@section('js_dependencies')
{{-- TinyMCE --}}
<script src="https://cdn.tiny.cloud/1/si4jvonmpvtlfypotvf9zu2vci2zsjavfd4v1oyvnzncszag/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
{{-- KaTeX --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.9.0-alpha1/katex.min.js" integrity="sha384-GR8SEkOO1rBN/jnOcQDFcFmwXAevSLx7/Io9Ps1rkxWp983ZIuUGfxivlF/5f5eJ" crossorigin="anonymous"></script>
@endsection

@section('scripts')
<script>
$(function(){
    new SlimSelect({
        select: '.coin-select',
        minimumResultsForSearch: -1,
    });

    $('.createQuestionForm [type="submit"]').click(function(e) {
        e.preventDefault();
        $(this).attr('disabled', true);
        $(this).closest('form').submit();
    })
});
</script>
@endsection

