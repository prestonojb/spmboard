<?php

namespace App\Http\Controllers;

use App\CustomResource;
use App\Library;
use App\Resource;
use Amplitude;

class LibraryController extends Controller
{
    /**
     * List libraries
     */
    public function index()
    {
        $this->authorize('index', Library::class);

        $user = auth()->user();
        $libraries = $user->libraries;
        return view('libraries.index', compact('libraries'));
    }

    /**
     * Create library
     */
    public function create()
    {
        $user = auth()->user();
        // Check limit of libraries
        if(!$user->hasExtraLibrary()) {
            return redirect()->back()->with('error', 'Library limit exceeded!');
        }
        return view('libraries.create');
    }

    /**
     * Store library
     */
    public function store()
    {
        $this->authorize('create_store', Library::class);
        request()->validate([
            'name' => 'required|string',
        ]);

        $is_ajax = request('is_ajax') !== 'false';

        $user = auth()->user();
        // Check limit of libraries
        if(!$user->hasExtraLibrary()) {
            return $is_ajax ? 'Library limit exceeded!'
                            : redirect()->back()->withInput()->with('error', 'Library limit exceeded!');
        }

        // Create library
        $library = $user->libraries()->create([
                        'name' => request('name'),
                    ]);

        // Log event on Amplitude
        Amplitude::setUserId($user->id);
        Amplitude::queueEvent('create_library', ['library_id' => $library->id]);

        return $is_ajax ? $library
                        : redirect()->route('libraries.show', $library->id)->withSuccess("Library created successfully!");
    }

    /**
     * Show library
     */
    public function show(Library $library)
    {
        $this->authorize('show', $library);

        $has_resources = count($library->resources ?? []);
        // Get resources from library
        $past_papers = $library->getPastPapers();
        $topical_past_papers = $library->getTopicalPastPapers();
        $notes = $library->getNotes();
        $guides = $library->getGuides();

        $title = $library->name;

        return view('libraries.show', compact('library', 'has_resources', 'past_papers', 'topical_past_papers', 'notes', 'guides', 'title'));
    }

    /**
     * Show unlocked resources
     */
    public function showUnlockedResources()
    {
        $user = auth()->user();

        $has_resources = $user->hasUnlockedResources();
        // Get resources user owns
        $notes = $user->getOwnedNotes();
        $topical_past_papers = $user->getOwnedTopicalPastPapers();
        $type = 'unlocked_resources';
        $title = 'Unlocked Resources';
        $library = null;

        return view('libraries.show', compact('has_resources', 'library', 'notes', 'topical_past_papers', 'type', 'title'));
    }

    /**
     * Save/Remove resource from library
     * AJAX
     */
    public function toggleResource(Library $library, Resource $resource)
    {
        $this->authorize('toggle_resource', $library);

        $user = auth()->user();

        if(!$library->isFull()) {
            $library->toggleResource($resource);

            // Log event on Amplitude
            if($library->hasResource($resource)) {
                Amplitude::setUserId($user->id);
                Amplitude::queueEvent('save_to_library', ['library_id' => $library->id, 'type' => $resource->type, 'resourceable_id' => $resource->resourceable_id]);
            }
        } else {
            return 'full';
        }

        return $library->hasResource($resource) ? 'saved'
                                                : 'removed';
    }

    public function addCustomResource(Library $library)
    {
        // Validation
        request()->validate([
            'name' => 'required|string',
            'type' => 'required|string',
            'file' => 'required|mimes:pdf|max:10000',
        ]);
        // Create custom resource
        $custom_resource = CustomResource::create([
            'name' => request('name'),
            'type' => request('type'),
            'url' => CustomResource::saveFile(request('file')),
        ]);
        $library->addCustomResource($custom_resource);
        return 'OK';
    }

    /**
     * Delete library
     */
    public function delete(Library $library)
    {
        $this->authorize('delete', $library);

        $library->cleanDelete();
        return redirect()->route('libraries.index')->with('error', $library->name.' deleted successfully!');
    }

    /**
     * Get Library List Item HTML
     * @return string
     */
    public function getListItemHtml(Library $library, Resource $resource)
    {
        return view('libraries.common.list_item', compact('library', 'resource'))->render();
    }
}
