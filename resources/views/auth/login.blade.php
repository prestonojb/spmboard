@extends('main.layouts.app')

@section('meta')
    <title>Log in | {{ config('app.name') }}</title>
    <meta name="description" content="Sign in to engage with the community now!">
@endsection

@section('content')
@include('main.navs.header')

<div class="py-4"></div>

<div class="container wrapper">

    <div class="text-center">
        @if(session('error'))
            <div class="alert alert-danger" data-dismiss="alert" role="alert">
                {{ session('error') }}
            </div>
        @endif
        <h1 class="heading">Sign in</h1>
        <h2 class="sub-heading">Sign in to engage with the community now!</h2>

        <a class="mb-3 btn d-flex justify-content-center align-items-center google-btn" href="{{ route('google.redirect') }}">
            <img class="google-icon" src="{{ asset('img/essential/google.png') }}" alt="google-icon">
            SIGN IN WITH GOOGLE
        </a>

        <span class="d-inline-block mb-3"><strong>OR</strong></span>
    </div>

    <form method="POST" action="{{ route('login') }}">
        @csrf
        <div class="form-field">
            <label>{{ __('Email Address') }}</label>
            <input type="email" name="email" value="{{ old('email') }}" required autofocus autocomplete="email" placeholder="Email">
            @error('email')
                <span class="error-message" role="alert">{{ $message }}</span>
            @enderror
        </div>

        <div class="form-field">
            <label>{{ __('Password') }}</label>
            <input type="password" name="password" required placeholder="Password">
            @error('password')
                <span class="error-message" role="alert">{{ $message }}</span>
            @enderror
        </div>

        <div class="d-flex align-items-center justify-content-center mb-3">
            <input type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
            <label for="remember">{{ __('Remember Me') }}</label>
        </div>

        <div class="text-center">
            <button type="submit" class="button--primary mb-2">Log In</button>
            <p class="mb-0">Don't have an account? <a href="{{ route('register') }}">Sign up</a></p>
            <a href="{{ route('password.request') }}">Forgot password?</a>
        </div>
    </form>
</div>

<div class="py-4"></div>

@endsection
